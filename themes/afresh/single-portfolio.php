<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package afresh
 */

get_header();
$categories = get_the_terms(get_the_ID(),'portfolio_category');

?>

	<div id="primary" class="page-area container">
		<main id="main" class="site-main inner-wrapper">

		<?php
		while ( have_posts() ) :
			the_post();?>


<section class="showcase <?php echo($categories[0]->name == 'Web Development' || $categories[0]->name == 'Web Design' ) ? 'web-image' : '' ?>">
	<div class="post-photo">
	<img src="<?php echo get_template_directory_uri();?>/img/swatch.png" class="swirle">

		<?php if($categories[0]->name == 'Web Development' || $categories[0]->name == 'Web Design' ): ?>
		<?php the_post_thumbnail('full') ?>

		<?php else: ?>
		<?php the_post_thumbnail('full') ?>

		<?php endif; ?>

	</div>

<div class="row showcase-options">
	<div class="col-md-3">
		<h2><?php the_title() ?></h2>

		<?php 
$output = '';
$count = 0;
if ( ! empty( $categories ) ) {
    foreach( $categories as $category ) {
    	$count++;
    	$output .= '<h3><span> .0';
    	$output .=  $count;
    	$output .= '</span>';
        $output .= $category->name;
        $output .= '</h3>';
    }
    echo  $output ;
} ?>
		


		

	</div>
	<div class="col-md-9">
		<?php the_content() ?>



		<div class="share-block portfolio-share text-center"><span class="share-text">share</span>

		<ul class="post-socials">
	<li>
			<a href="#" data-type="twitter" data-url="<?php the_permalink() ?>" data-description="<?php the_title() ?>" data-via="afresh_creative" class="prettySocial fab fa-twitter"></a></li>

<li>
	<a href="#" data-type="facebook" data-url="<?php the_permalink() ?>" data-title="<?php the_title() ?>" data-description="<?php echo strip_tags( get_the_excerpt() ); ?>" data-media="<?php the_post_thumbnail_url() ?>" class="prettySocial fab fa-facebook"></a>
</li>
	<li>
		<a href="#" data-type="pinterest" data-url="<?php the_permalink() ?>" data-description="<?php the_title() ?>" data-media="<?php the_post_thumbnail_url() ?>" class="prettySocial fab fa-pinterest"></a>
</li>

</ul>
	</div>	


	</div>
</div>


</section>



		<?php endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
