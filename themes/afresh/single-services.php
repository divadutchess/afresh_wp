<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package afresh
 */

get_header();
?>

	<div class="portfolio-inner slanted">
		<div class="portfolio-header portfolio-header-works text-center">

			<div class="container">

		<h3><span><?php the_title(); ?></span></h3>
		<?php the_excerpt() ?>

	</div>
		

</div>
</div>



	<div id="primary" class="page-area container">
		<main id="main" class="site-main inner-wrapper">

		<?php
		while ( have_posts() ) :
			the_post();?>


<section class="showcase showcase-item">
<div class="row showcase-options">
						<div class="col-md-12 packages all-packages">
				

	<div class="row">


			<div class="page-header col-sm-6 col-xs-12">
			
				<div class="page-image">
					
					<?php the_post_thumbnail() ?>
				</div>
			</div>



				


					<div class="col-md-6 packages">

							<h3><span>.a</span>Duration:
						<b class="subsequent"><?php echo get_post_meta( $post->ID, 'service_duration', true ); ?></b></h3>

						<h3><span>.b</span>Investment:
						<b class="subsequent"><?php echo get_post_meta( $post->ID, 'service_investment', true ); ?></b></h3>



						<h3><span>.c</span>What's Included </h3>

						<ul>
							<?php 
							$includedDetails = get_post_meta($post->ID,'includedDetails',true); 
							foreach ($includedDetails as $item) {
								echo '<li>'.$item['name'].'</li>';
							}
							

							?>
						
							
						</ul>
						
					</div>		

				</div>

			</div>
</div>


</section>


<p class="text-center">
	
	<a href="#" data-toggle="modal" data-target="#contactModal" class="read-more"><span>Book a consult</span></a>
</p>
		<?php

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
