<?php get_header(); ?>


	<div id="primary" class="page-area">
		<main id="main" class="site-main">
<section class="portfolio">
	<div class="portfolio-inner slanted">

		<div class="portfolio-header text-center">

<?php
		while ( have_posts() ) : the_post();?>
<h3><span><?php the_title() ?></span></h3>
<?php the_content() ?>	

<?php endwhile;  ?>
</div></div>

<section class="move-up">

	<p class="showcase-blob text-center">The only one missing is your brand - <a href="/contact">Let's Chat?</a></p>


 <div class="loader-5 center"><span></span></div>



  <div class="segmented-control mb-5" style="opacity: 0; margin:1rem  auto; color: #272727">
                    <input type="radio" name="sc-2-1" id="sc-2-1-1" checked="">
                    <input type="radio" name="sc-2-1" id="sc-2-1-2">

                    <label for="sc-2-1-1" data-value="Web-esque" data-filter=".web">Web-esque</label>
                    <label for="sc-2-1-2" data-value="Graphic-esque" data-filter=".graphic">Graphic-esque</label>
                </div>

<div class="grid are-images-unloaded mt-5">




  <div class="grid__col-sizer"></div>
  <div class="grid__gutter-sizer"></div>

  <?php

global $post;
$count= 0; 
$args = array( 'posts_per_page' => -1, 'post_type'=>'portfolio' );

$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post ); $count++;  

$terms = get_the_terms( get_the_ID(), 'portfolio_category' );

  ?>

  <div class="grid__item transition <?php echo (has_term('web-design', 'portfolio_category', null) == 1)  ? 'web' : '' ?> <?php echo (has_term('graphic-design', 'portfolio_category', null) == 1)  ? 'graphic' : '' ?>">
      <a href="<?php echo get_the_permalink( $post->ID ); ?>" title="<?php echo $post->post_title ?>">


        <?php if(get_field('portfolio_img')): ?>

              <img src="<?php echo get_field('portfolio_img') ?>" alt="<?php the_title() ?>">

            <?php else: ?>

            <?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'class' => 'portfolio-img' ) ); ?>

          <?php endif; ?>



    </a>
      </div>
<?php endforeach; 
wp_reset_postdata();?>

 
</div>

</section>
</section>

</main></div>

<?php get_footer(); ?>