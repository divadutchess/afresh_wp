<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package afresh
 */

?>


		<article class="row content-area" id="post-<?php the_ID(); ?>">
			<span class="blog-heading"><?php echo get_the_date('d.m.Y')?></span>
			<div class="col-md-6">
				<a href="<?php the_permalink()?>" title="<?php the_title() ?>"><?php the_post_thumbnail('blog-image')?></a>
			</div>

			<div class="col-md-6">
				
				<div class="entry">
					<span class="beneath">
<?php 
$categories = get_the_category();
$separator = ' / ';
$output = '';
if ( ! empty( $categories ) ) {
    foreach( $categories as $category ) {
        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
    }
    echo trim( $output, $separator );
} ?>
					</span>
<h2><a href="<?php the_permalink()?>" title="<?php the_title() ?>"><?php the_title() ?></a></h2>

<?php echo excerpt(25); ?>
<a href="<?php the_permalink()?>" title="<?php the_title() ?>" class="read-more"><span>read more</span></a>




			</div>

</article><!-- #post-<?php the_ID(); ?> -->

