<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package afresh
 */

?>



	<div class="portfolio-inner slanted">
		<div class="portfolio-header text-center">

			<div class="container">

		<h3><span><?php the_title(); ?></span></h3>
		<?php the_excerpt() ?>

	</div>
		

</div>
</div>





