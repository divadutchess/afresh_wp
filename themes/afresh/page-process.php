<?php get_header() ?>


		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

		

		endwhile; // End of the loop.
		?>


<section class="page-inner" id="post-<?php the_ID(); ?>">

			<div class="container">

	<div class="page-wrapper">



<section class="process move-up">
				<img src="<?php echo get_template_directory_uri();?>/img/swatch.png" class="swirle">

	<div class="timeline-wrap">
		<?php 
$terms = get_terms( 'process', array(
	'depth'      => 1,
	'parent'     => 0,
	'orderby'    => 'order', // <--- Looky looky!
	'order'      => 'ASC',
	'hide_empty' => false,


) );
 ?>

   <ul class="timeline">
     <li class="timeline-dots"></li>
     <?php foreach ($terms as $term): ?>
     	<?php  $count++; $even_odd_class = ( ($count % 2) == 0 ) ? "entry--right" : "entry--left";  ?>
      <li class="timeline-entry <?php echo $even_odd_class; ?>">
         <div class="entry__content wow animated fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
         	<span></span>
            <h2><?php echo $term->name ?></h2>
            <p><?php echo $term->description ?></p>
         </div>
      </li>


  	<?php endforeach; ?>


     <li class="timeline-dots timeline-dots-end "></li>

   </ul>
</div>


</section>

	</div>

</div>

</section><!-- #post-<?php the_ID(); ?> -->

<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'afresh' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>



<?php get_footer() ?>