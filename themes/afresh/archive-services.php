<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package afresh
 */

get_header();
?>

	<div id="primary" class="page-area">
		<main id="main" class="site-main">


	<div class="portfolio-inner slanted">

		<div class="portfolio-header text-center">

<?php
		if ( have_posts() ) :?>
<h3><span><?php echo post_type_archive_title( '', false ); ?></span></h3>
<?php the_archive_description() ?>	

<?php endif;  ?>
</div></div>


			<section class="offered move-up">
							<img src="<?php echo get_template_directory_uri();?>/img/swatch.png" class="swirle">

	<div class="container">
		
<?php $count= 0; if ( have_posts() ) : ?>

		<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post(); $count++;?>
				<div class="row offer_row">

			<div class="col-md-6 img-package">
				<?php the_post_thumbnail('service-image') ?>

			</div>

			<div class="col-md-6 packages all-packages">
						<h3 class="service_offered"><span>.0<?php echo $count?></span><?php the_title() ?></h3>
				
				<?php the_excerpt() ?>


					<div class=" packages">
						<h3><span>.a</span>Duration</h3>
						<span class="subsequent"><?php echo get_post_meta( $post->ID, 'service_duration', true ); ?></span>
</div>

					<div class="investment-packages packages">

						<h3><span>.b</span>Investment</h3>
						<span class="subsequent"><?php echo get_post_meta( $post->ID, 'service_investment', true ); ?></span>

					</div>		

				
			</div>


								<div class="col-md-12 packages included">
						<h3><span>.c</span>What's Included </h3>

						<ul>
							<?php 
							$includedDetails = get_post_meta($post->ID,'includedDetails',true); 
							foreach ($includedDetails as $item) {
								echo '<li>'.$item['name'].'</li>';
							}?>
						
							
						</ul>

						<div class="package-content">
							<?php the_content() ?>
						</div>

							<p class="text-center"><a href="#" data-toggle="modal" data-target="#contactModal" class="read-more"><span>book a consult</span></a></p>

						
					</div>



		</div>

<?php endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</div>
<p><b>please note: </b>I also offer more services in addition to the packages listed above. I am also able to personalize options for your
business in particular; please inquire for a custom quote. If you do not see something below, <a href="/contact" class="beneath">please ask</a>! </p>

<!--div class="more-packages">
	<h4 class="beneath">add ons</h4>

	<ul>
		<li>business cards :: $100</li>
	</ul>
</div-->

</section>





		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
