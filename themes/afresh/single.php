<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package afresh
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();?>


<section class="blog-inner">
	<div class="container">
		<div class="inner-wrapper">
<div class="post-photo">
	<img src="<?php echo get_template_directory_uri();?>/img/swatch.png" class="swirle">
	<?php the_post_thumbnail() ?>

</div>

	<div class="post-area">			
<article class=" feed-area">
<h1 class="post-heading"> <span><?php the_title() ?></span></h1>

<div class="post-meta">
	

	<ul class="row">
		<li class="col-sm-4 col-4"><span class="beneath">written</span><a><?php the_date() ?></a></li>
		<li class="col-sm-4 col-4"><span class="beneath">filed under</span>
			<?php 
$categories = get_the_category();
$separator = ' / ';
$output = '';
if ( ! empty( $categories ) ) {
    foreach( $categories as $category ) {
        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
    }
    echo trim( $output, $separator );
} ?>
		

		</li>
		<li class="col-sm-4 col-4"><span class="beneath">written by</span>
			<a>HALEY</a></li>


	</ul>
</div>


		<div class="share-block text-center"><span class="share-text">share</span>

		<ul class="post-socials">
	<li>
			<a href="#" data-type="twitter" data-url="<?php the_permalink() ?>" data-description="<?php the_excerpt() ?>" data-via="afresh_creative" class="prettySocial fab fa-twitter"></a></li>

<li>
	<a href="#" data-type="facebook" data-url="<?php the_permalink() ?>" data-title="<?php the_title() ?>" data-description="<?php echo strip_tags( get_the_excerpt() ); ?>" data-media="<?php the_post_thumbnail_url() ?>" class="prettySocial fab fa-facebook"></a>
</li>
	<li>
		<a href="#" data-type="pinterest" data-url="<?php the_permalink() ?>" data-description="<?php the_title() ?>" data-media="<?php the_post_thumbnail_url() ?>" class="prettySocial fab fa-pinterest"></a>
</li>

</ul>
	</div>	




<div class="feed-entry">
<?php the_content() ?>

<img src="<?php echo site_url( '', null ); ?>/img/love_haley.png" width="70">
</div>



</article>

<div class="afresh-post-prev-next-buttons">
<div class="row">


<?php $prevPost = get_previous_post();
if($prevPost) {?>
<div class="nav-box col-sm-6 pagination-item prev">

<div class="pagination-img">
<?php echo get_the_post_thumbnail($prevPost->ID, array(150,150) );?>

</div>

<div class="pagination-content"><a href="<?php echo get_permalink($prevPost) ?>">
	<span class="afresh-nav-sub">Previous Post</span> 
	<span class="afresh-nav-title"> <?php echo $prevPost->post_title ?> </span></a> 

</div>



</div>


 
<?php } $nextPost = get_next_post();
if($nextPost) { ?>
<div class="nav-box col-sm-6 pagination-item next">

	<div class="pagination-content">
<a href="<?php echo get_permalink($nextPost) ?>">
		<span class="afresh-nav-sub">Next Post</span> 
		<span class="afresh-nav-title"> <?php echo $nextPost->post_title ?> </span>
	</a> 
</div>


<div class="pagination-img">
<?php echo get_the_post_thumbnail($nextPost->ID, array(150,150) );  ?>
</div>




</div>

<?php } ?>

</div>
</div><!--#cooler-nav div -->

<?php $image = get_post_meta( get_the_ID(), 'meta-image', false );  if($image[0]) { ?>
<div class="pin-for-later">
	<div class="row">
		<div class="col-md-7">
			<span class="beneath pin-beneath">save this article:</span>
		<p> This article could come in handy again in the future.  Pin this image on <a href="https://pinterest.ca/afreshcreative" target="_blank">Pinterest</a> so you’ll always find it when you need it!</p></div>
		<div class="col-md-5 pin-hover">
		<a href="#" data-type="pinterest" data-url="<?php the_permalink() ?>" data-description="<?php the_title() ?>" data-media="<?php echo $image[0]; ?>" class="prettySocial pin-style"> pin it </a>
			<img src="<?php echo $image[0]; ?>" title="<?php the_title(); ?>">
		</div>
	</div>
</div>

<?php } ?>

<?php 
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

 ?>
</div>



			</div>
		
</div>
		
</section>




			<?php 

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
