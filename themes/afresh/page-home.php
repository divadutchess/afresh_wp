<?php
/**
* Template Name: Page homepage
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header();
?>

<section class="welcome">


<div class="container">
	<div class="wrapper">

<div class="welcome-text">
				<img src="<?php echo get_template_directory_uri();?>/img/swatch.png" class="swirle">

	<h2 class="welcome-heading fadeIn wow">GIVE YOUR BRAND <span class="cursive">AFRESH</span><br> IMPRESSION<span class="in-heart"></span>
	</h2>
	<div class="welcome-blob pulse wow">
		<p><?php echo  $redux_demo['welcome-desc']?> </p>
			<a href="/contact">fancy a chitty chat?</a>

	</div>


</div>


</div>
</div>

</section>


<section class="services">
	<div class="container">
<div class="inner-wrapper">

<div class="row">

<?php $the_query = new WP_Query( array( 'posts_per_page' => 3, 'post_type' => 'services' ));
$count = 0;
 if ( $the_query->have_posts() ) : ?>


	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $count++; ?>
<div class="col-md-4 service-col">
<div class="module fadeInUp wow" >
<a href="<?php the_permalink(  ); ?>" title="<?php the_title( ); ?>"><?php the_post_thumbnail('home-service')?></a>
<div class="module-text">
	<h4><a href="<?php the_permalink(  ); ?>" title="<?php the_title( ); ?>"><span class="decorative">afresh</span><?php echo get_post_meta( $post->ID, 'afresh_what', true );?></a></h4>

	<?php the_excerpt()?>
</div>
</div>

<div class="numbering">
<span class="nombre"> .0<?php echo $count;?></span>
<span class="does"><?php the_title()?></span>
</div>
</div>


  	<?php endwhile; ?>
	<!-- end of the loop -->

	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>


<?php endif; ?>

</div>

<div class="testimonials">
	<span class="owl-nav float-right">
		<button class="prev-owl"><i class="fas fa-angle-left"></i></button>
		<button class="next-owl"><i class="fas fa-angle-right"></i></button>

	</span>
<div class="owl-carousel">


<?php $the_query = new WP_Query( array( 'posts_per_page' => -1, 'post_type' => 'testimonials' ));
 if ( $the_query->have_posts() ) : ?>


	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

  <div> 

  	<?php the_content()?><span class="made">- <?php the_title()?> </span> 

  </div>

  	<?php endwhile; ?>
	<!-- end of the loop -->

	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>


<?php endif; ?>


</div>

</div></div></div>
</section>


<section class="about reverse">
	
	<div class="container">
<div class="inner-wrapper">
		<div class="row align-md-items-center">
			<div class="col-md-6">
			<h2><span class="underneath"><?php echo $redux_demo['about-title']?></span></h2>

			<?php echo $redux_demo['about-desc']?>


			</div>

			<div class="col-md-6">
				<img src="<?php echo $redux_demo['about-img']['url']?>" width="700" height="635" loading="lazy">
			</div>	

		</div>
</div>
	</div>

</section>




<section class="brands">
	
	<div class="container">
		<div class="wrapper wow">
			<div class="row">

			<div class="col-md-12 brand-feature wow fadeIn">
			<span class="underneath"><?php echo $redux_demo['featured-title']?></span>
			<p><?php echo $redux_demo['featured-desc']?></p>
			</div>


			</div>

	<div class="row">

		  	<?php
		//portfolio_category
		global $post;
		$args = array( 'posts_per_page' => -1, 'post_type'=>'portfolio', 'tax_query' => array(
        array(
            'taxonomy' => 'portfolio_category',
            'field' => 'slug',
            'terms' => 'web-development'
        ),
    ) );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
		<div class="col-md-6 brand-img wow fadeInUp">
					<a href="<?php echo get_the_permalink( $post->ID ); ?>" title="<?php echo $post->post_title ?>">	

						<?php if(get_field('portfolio_img')): ?>

							<img src="<?php echo get_field('portfolio_img') ?>" alt="<?php the_title() ?>" loading="lazy" width="460" height="425">

						<?php else: ?>

						<?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'class' => 'portfolio-img' ) ); ?>

						<?php endif; ?>
				</a>
					</div>
		<?php endforeach; 
		wp_reset_postdata();?>


</div>
		</div>

	</div>

</section>


<section class="instagram-feed slightly">

	<div class="container">
		<div class="inner-wrapper">
			<div class="row instagram-row justify-content-center align-items-center">
				<div class="col-md-12 col-lg-9"><?php echo do_shortcode('[instagram-feed]')?></div>
		<div class="col-lg-3 col-md-12">
					<h2 class="follow-text">
						follow <a href="https://instagram.com/afresh.creative" target="_blank">
							<br><i class="fab fa-instagram"></i> afresh.creative</a>
					</h2>
				</div>		
			</div>
		</div>
	</div>
	
	
</section>

<?php
get_footer();
