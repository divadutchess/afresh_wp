<?php
/**
 * afresh functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package afresh
 */


require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

require_once (get_template_directory() .  '/redux-options.php'); 



if ( ! function_exists( 'afresh_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function afresh_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on afresh, use a find and replace
		 * to change 'afresh' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'afresh', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_post_type_support( 'page', 'excerpt' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Header - Left Menu', 'afresh' ),
			'menu-2' => esc_html__( 'Header - Right Menu', 'afresh' ),
			'menu-3' => esc_html__( 'Social Menu', 'afresh' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'afresh_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		add_image_size( 'home-blog-size', 500, 340, true ); 
		add_image_size( 'default-portfolio', 400, 400, true ); 
		add_image_size( 'extended-portfolio', 400, 600, true ); 
		add_image_size( 'home-service', 500, 360, true ); 
		add_image_size( 'blog-image', 600, 415, true ); 
		add_image_size( 'featured-image', 400, 435, true ); 
		add_image_size( 'service-image', 525, 400, true ); 
		add_image_size( 'custom-web-size', 950, 700, array( 'center', 'top' ) ); // Hard crop left top


	}
endif;
add_action( 'after_setup_theme', 'afresh_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function afresh_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'afresh_content_width', 640 );
}
add_action( 'after_setup_theme', 'afresh_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function afresh_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'afresh' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'afresh' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

		register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget', 'afresh' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'afresh' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'afresh_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function afresh_scripts() {


	$parent_style = 'afresh-style';

	 wp_enqueue_style( 'bootstrap',
        get_stylesheet_directory_uri() . '/css/bootstrap.min.css',
        false,
        wp_get_theme()->get('1.0.0')
    );
	
	 wp_enqueue_style( 'fontawesome',
        get_stylesheet_directory_uri() . '/css/fontawesome.min.css',
		false,
        wp_get_theme()->get('1.0.0')
    );


	 wp_enqueue_style( 'owl_carousel',
        get_stylesheet_directory_uri() . '/css/owl.carousel.min.css',
        false,
        wp_get_theme()->get('1.0.0')
    );
 

	 wp_enqueue_style( 'owl_carousel_theme',
        get_stylesheet_directory_uri() . '/css/owl.theme.default.min.css',
        false,
        wp_get_theme()->get('1.0.0')
    );


 wp_enqueue_style( 'segmented-controls',
        get_stylesheet_directory_uri() . '/assets/scss/segmented-controls.css',
        false,
        wp_get_theme()->get('1.0.0')
    );

	  wp_enqueue_style( 'animate',
        get_stylesheet_directory_uri() . '/css/animate.css',
        false,
        wp_get_theme()->get('1.0.0')
    );


	  wp_enqueue_style( 'fontawesome-all',
        get_stylesheet_directory_uri() . '/css/all.min.css',
        false,
        wp_get_theme()->get('1.0.0')
    );



	wp_enqueue_style( 'afresh-style', get_stylesheet_uri() );



	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), false, true );


	wp_enqueue_script( 'owl_carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), false, true );

	wp_enqueue_script( 'wow', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), false, true );

	wp_enqueue_script( 'prettySocial', get_template_directory_uri() . '/js/jquery.prettySocial.min.js', array('jquery'), false, true );


	wp_enqueue_script( 'imagesloaded' );

	wp_enqueue_script( 'isotype', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), false, true );


	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), false, true );


		// In most cases it is already included on the page and this line can be removed
 
	// register our main script but do not enqueue it yet
	wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/js/myloadmore.js', array('jquery') );
 
 global $wp_query;
	// now the most interesting part
	// we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
	wp_localize_script( 'my_loadmore', 'misha_loadmore_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $wp_query->max_num_pages
	) );
 
 	wp_enqueue_script( 'my_loadmore' );
}
add_action( 'wp_enqueue_scripts', 'afresh_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}





function excerpt($limit) {
$excerpt = explode(' ', get_the_excerpt(), $limit);
if (count($excerpt)>=$limit) {
array_pop($excerpt);
$excerpt = implode(" ",$excerpt).'...';
} else {
$excerpt = implode(" ",$excerpt);
}
$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
return $excerpt;
}
function content($limit) {
$content = explode(' ', get_the_content(), $limit);
if (count($content)>=$limit) {
array_pop($content);
$content = implode(" ",$content).'...';
} else {
$content = implode(" ",$content);
}
$content = preg_replace('/[.+]/','', $content);
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);
return $content;
}


// Register Custom Post Type
function custom_portfolio_type() {

	$labels = array(
		'name'                  => _x( 'Portfolio', 'Post Type General Name', 'afresh' ),
		'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'afresh' ),
		'menu_name'             => __( 'Portfolio', 'afresh' ),
		'name_admin_bar'        => __( 'Portfolio', 'afresh' ),
		'archives'              => __( 'Item Archives', 'afresh' ),
		'attributes'            => __( 'Item Attributes', 'afresh' ),
		'parent_item_colon'     => __( 'Parent Item:', 'afresh' ),
		'all_items'             => __( 'All Items', 'afresh' ),
		'add_new_item'          => __( 'Add New Item', 'afresh' ),
		'add_new'               => __( 'Add New', 'afresh' ),
		'new_item'              => __( 'New Item', 'afresh' ),
		'edit_item'             => __( 'Edit Item', 'afresh' ),
		'update_item'           => __( 'Update Item', 'afresh' ),
		'view_item'             => __( 'View Item', 'afresh' ),
		'view_items'            => __( 'View Items', 'afresh' ),
		'search_items'          => __( 'Search Item', 'afresh' ),
		'not_found'             => __( 'Not found', 'afresh' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'afresh' ),
		'featured_image'        => __( 'Featured Image', 'afresh' ),
		'set_featured_image'    => __( 'Set featured image', 'afresh' ),
		'remove_featured_image' => __( 'Remove featured image', 'afresh' ),
		'use_featured_image'    => __( 'Use as featured image', 'afresh' ),
		'insert_into_item'      => __( 'Insert into item', 'afresh' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'afresh' ),
		'items_list'            => __( 'Items list', 'afresh' ),
		'items_list_navigation' => __( 'Items list navigation', 'afresh' ),
		'filter_items_list'     => __( 'Filter items list', 'afresh' ),
	);
	$args = array(
		'label'                 => __( 'Portfolio', 'afresh' ),
		'description'           => __( 'Custom Portfolio Type', 'afresh' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments' ),
		'taxonomies'            => array( 'portfolio_category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 54,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'       => 'dashicons-images-alt2',

	);
	register_post_type( 'portfolio', $args );

}
add_action( 'init', 'custom_portfolio_type', 0 );



// Register Custom Taxonomy
function portfolio_kind_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'afresh' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'afresh' ),
		'menu_name'                  => __( 'Category', 'afresh' ),
		'all_items'                  => __( 'All Categories', 'afresh' ),
		'parent_item'                => __( 'Parent Category', 'afresh' ),
		'parent_item_colon'          => __( 'Parent Category:', 'afresh' ),
		'new_item_name'              => __( 'New Category Name', 'afresh' ),
		'add_new_item'               => __( 'Add New Category', 'afresh' ),
		'edit_item'                  => __( 'Edit Category', 'afresh' ),
		'update_item'                => __( 'Update Category', 'afresh' ),
		'view_item'                  => __( 'View Item', 'afresh' ),
		'separate_items_with_commas' => __( 'Separate genres with commas', 'afresh' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'afresh' ),
		'choose_from_most_used'      => __( 'Choose from the most used genres', 'afresh' ),
		'popular_items'              => __( 'Popular Items', 'afresh' ),
		'search_items'               => __( 'Search categories', 'afresh' ),
		'not_found'                  => __( 'Not Found', 'afresh' ),
		'no_terms'                   => __( 'No items', 'afresh' ),
		'items_list'                 => __( 'Items list', 'afresh' ),
		'items_list_navigation'      => __( 'Items list navigation', 'afresh' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'portfolio_category', array( 'portfolio' ), $args );

}
add_action( 'init', 'portfolio_kind_taxonomy', 0 );


function create_posttype() {
  $args = array(
    'labels' => array(
      'name' => __('Testimonials'),
      'singular_name' => __('Testimonials'),
      'all_items' => __('All Testimonials'),
      'add_new_item' => __('Add New Testimonial'),
      'edit_item' => __('Edit Testimonial'),
      'view_item' => __('View Testimonial'),
		'not_found' => __( 'Not Found', 'afresh' ),

    ),
    'public' => true,
    'has_archive' => true,
    'rewrite' => array('slug' => 'testimonials'),
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'capability_type' => 'page',
    'supports' => array('title', 'editor', 'thumbnail'),
    'exclude_from_search' => true,
    'menu_position' => 60,
    'has_archive' => true,
    'menu_icon' => 'dashicons-format-status'
    );
  register_post_type('testimonials', $args);
}
add_action( 'init', 'create_posttype');



// Register Custom Post Type
function services_post_type() {

	$labels = array(
		'name'                  => _x( 'Services', 'Post Type General Name', 'afresh' ),
		'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'afresh' ),
		'menu_name'             => __( 'Services', 'afresh' ),
		'name_admin_bar'        => __( 'Service', 'afresh' ),
		'archives'              => __( 'Item Archives', 'afresh' ),
		'attributes'            => __( 'Item Attributes', 'afresh' ),
		'parent_item_colon'     => __( 'Parent Service:', 'afresh' ),
		'all_items'             => __( 'All Services', 'afresh' ),
		'add_new_item'          => __( 'Add New Service', 'afresh' ),
		'add_new'               => __( 'New Service', 'afresh' ),
		'new_item'              => __( 'New Item', 'afresh' ),
		'edit_item'             => __( 'Edit Service', 'afresh' ),
		'update_item'           => __( 'Update Service', 'afresh' ),
		'view_item'             => __( 'View Service', 'afresh' ),
		'view_items'            => __( 'View Items', 'afresh' ),
		'search_items'          => __( 'Search services', 'afresh' ),
		'not_found'             => __( 'No services found', 'afresh' ),
		'not_found_in_trash'    => __( 'No services found in Trash', 'afresh' ),
		'featured_image'        => __( 'Featured Image', 'afresh' ),
		'set_featured_image'    => __( 'Set featured image', 'afresh' ),
		'remove_featured_image' => __( 'Remove featured image', 'afresh' ),
		'use_featured_image'    => __( 'Use as featured image', 'afresh' ),
		'insert_into_item'      => __( 'Insert into item', 'afresh' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'afresh' ),
		'items_list'            => __( 'Items list', 'afresh' ),
		'items_list_navigation' => __( 'Items list navigation', 'afresh' ),
		'filter_items_list'     => __( 'Filter items list', 'afresh' ),
	);
	$args = array(
		'label'                 => __( 'Service', 'afresh' ),
		'description'           => __( 'An overview of afresh packages available to you.', 'afresh' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 45,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'       => 'dashicons-exerpt-view',

	);
	register_post_type( 'services', $args );

}
add_action( 'init', 'services_post_type', 0 );


function add_services_afresh_meta_box() {
	add_meta_box(
		'services_meta_box', // $id
		'Extra Information', // $title
		'show_services_meta_box', // $callback
		'services', // $screen
		'normal', // $context
		'high' // $priority
	);
}
add_action( 'add_meta_boxes', 'add_services_afresh_meta_box' );


function show_services_meta_box() {
	global $post;  
	$meta = get_post_meta( $post->ID, 'your_fields', true ); ?>

	<input type="hidden" name="your_meta_box_nonce" value="<?php echo wp_create_nonce( basename(__FILE__) ); ?>">

    <!-- All fields will go here -->

    <p><label for="afresh_field">
    	afresh
    	<input id="afresh_field" type="text" name="afresh_what" value="<?php echo get_post_meta( $post->ID, 'afresh_what', true );?>" style="width: 80%">
    </label></p>

   <p> <label for="service_duration">
    	Duration
   <input id="service_duration" type="text" name="service_duration" value="<?php echo get_post_meta( $post->ID, 'service_duration', true );?>" style="width: 80%">
    </label></p>

      <p> <label for="service_investment">
    	Investment
   <input id="service_investment" type="text" name="service_investment" value="<?php echo get_post_meta( $post->ID, 'service_investment', true );?>" style="width: 80%">
    </label></p>

<?php }

function save_your_fields_meta( $post_id ) {   
	// verify nonce
	if ( !wp_verify_nonce( $_POST['your_meta_box_nonce'], basename(__FILE__) ) ) {
		return $post_id; 
	}
	// check autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}
	// check permissions
	if ( 'page' === $_POST['post_type'] ) {
		if ( !current_user_can( 'edit_page', $post_id ) ) {
			return $post_id;
		} elseif ( !current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}  
	}
	
	$old = get_post_meta( $post_id, 'afresh_what', true );
	$new = $_POST['afresh_what'];

	if ( $new && $new !== $old ) {
		update_post_meta( $post_id, 'afresh_what', $new );
	} elseif ( '' === $new && $old ) {
		delete_post_meta( $post_id, 'afresh_what', $old );
	}

	if ($_POST['service_investment']) {
		update_post_meta( $post_id, 'service_investment', $_POST['service_investment'] );
		
	}

		if ($_POST['service_duration']) {
		update_post_meta( $post_id, 'service_duration', $_POST['service_duration'] );
		
	}
}
add_action( 'save_post', 'save_your_fields_meta' );


function process_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Process', 'Taxonomy General Name', 'afresh' ),
		'singular_name'              => _x( 'Process', 'Taxonomy Singular Name', 'afresh' ),
		'menu_name'                  => __( 'Process', 'afresh' ),
		'all_items'                  => __( 'All Items', 'afresh' ),
		'parent_item'                => __( 'Parent Item', 'afresh' ),
		'parent_item_colon'          => __( 'Parent Item:', 'afresh' ),
		'new_item_name'              => __( 'New Item Name', 'afresh' ),
		'add_new_item'               => __( 'Add New Item', 'afresh' ),
		'edit_item'                  => __( 'Edit Item', 'afresh' ),
		'update_item'                => __( 'Update Item', 'afresh' ),
		'view_item'                  => __( 'View Item', 'afresh' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'afresh' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'afresh' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'afresh' ),
		'popular_items'              => __( 'Popular Items', 'afresh' ),
		'search_items'               => __( 'Search Items', 'afresh' ),
		'not_found'                  => __( 'Not Found', 'afresh' ),
		'no_terms'                   => __( 'No items', 'afresh' ),
		'items_list'                 => __( 'Items list', 'afresh' ),
		'items_list_navigation'      => __( 'Items list navigation', 'afresh' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'menu_icon' => 'dashicons-category',
		'menu_position'         => 55,

	);
	register_taxonomy( 'process', array( 'services' ), $args );

}
add_action( 'init', 'process_taxonomy', 0 );


function misha_loadmore_ajax_handler(){
 
	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';
 
	// it is always better to use WP_Query but not here
	query_posts( $args );
 
	if( have_posts() ) :
 
		// run the loop
		while( have_posts() ): the_post();
 
			// look into your theme code how the posts are inserted, but you can use your own HTML of course
			// do you remember? - my example is adapted for Twenty Seventeen theme
			get_template_part( 'template-parts/content', get_post_type() );
			// for the test purposes comment the line above and uncomment the below one
			// the_title();
 
 
		endwhile;
 
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}
 
 
 
add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}



// Adding the metaboxes
add_action( 'add_meta_boxes', 'add_included_meta' );

	/* Saving the data */
	add_action( 'save_post', 'included_meta_save' );

	/* Adding the main meta box container to the post editor screen */
	function add_included_meta() {
	    add_meta_box(
	        'included-details',
	       'What\'s Included Details',
	        'included_details_init',
	        'services',
	    	'normal',
			'high');
	}

	/*Printing the box content */
	function included_details_init() {
	    global $post;
	    // Use nonce for verification
	    wp_nonce_field( plugin_basename( __FILE__ ), 'included_nonce' );
	    ?>
	    <div id="included_meta_item">
	    <?php

	    //Obtaining the linked includeddetails meta values
	    $includedDetails = get_post_meta($post->ID,'includedDetails',true);
	    $c = 0;

	     if ( count( $includedDetails ) > 0 && is_array($includedDetails)) {
	        foreach( $includedDetails as $includedDetail ) {
	            if ( isset( $includedDetail['name'] ) || isset( $includedDetail['bio'] ) ) {
	                printf( '<p>Item <input type="text" name="includedDetails[%1$s][name]" value="%2$s"/>  <a href="#" class="remove-package">%3$s</a></p>', $c, $includedDetail['name'], 'Remove' );
	                $c = $c +1;



	            }
	        }
	    }



	    ?>
	<span id="output-package"></span>
	<a href="#" class="add_package"><?php _e('Add What\'s Included Details'); ?></a>
	<script>
	    var $ =jQuery.noConflict();
	    $(document).ready(function() {
	        var count = <?php echo $c; ?>;
	        $(".add_package").click(function() {
	            count = count + 1;

	            $('#output-package').append('<p> Item <input type="text" name="includedDetails['+count+'][name]" value="" /> <a href="#" class="remove-package"><?php echo "Remove"; ?></a></p>' );
	            return false;
	        });
	       $(document.body).on('click','.remove-package',function() {
	            $(this).parent().remove();
	        });
	    });
	    </script>
	</div><?php

	}

	/* Save function for the entered data */
	function included_meta_save( $post_id ) {
	    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
	        return;
	    // Verifying the nonce
	    if ( !isset( $_POST['included_nonce'] ) )
	        return;

	    if ( !wp_verify_nonce( $_POST['included_nonce'], plugin_basename( __FILE__ ) ) )
	        return;
	    // Updating the includedDetails meta data
	    $includedDetails = $_POST['includedDetails'];

	    update_post_meta($post_id,'includedDetails',$includedDetails);
	}





/**
 * Comment Form Fields Placeholder
 *
 */
function be_comment_form_fields( $fields ) {
	foreach( $fields as &$field ) {
		$field = str_replace( 'id="author"', 'id="author" placeholder="Name*"', $field );
		$field = str_replace( 'id="email"', 'id="email" placeholder="Email*"', $field );
		$field = str_replace( 'id="url"', 'id="url" placeholder="Website"', $field );
	}
	return $fields;
}
add_filter( 'comment_form_default_fields', 'be_comment_form_fields' );



//////////////////////////////////////////////////////////////////
// COMMENTS LAYOUT
//////////////////////////////////////////////////////////////////
	function better_comments($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;
		
		?>
		<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
			
			<div class="thecomment">
						
				<div class="author-img">
					<?php echo get_avatar($comment,$args['avatar_size']); ?>
				</div>
				
				<div class="comment-text">
					<span class="reply">
						<?php comment_reply_link(array_merge( $args, array('reply_text' => __('Reply', 'afresh'), 'depth' => $depth, 'max_depth' => $args['max_depth'])), $comment->comment_ID); ?>
						<?php edit_comment_link(__('Edit', 'afresh')); ?>
					</span>
					<span class="author"><?php echo get_comment_author(); ?></span>
					<span class="date"><?php printf(__('%1$s at %2$s', 'afresh'), get_comment_date(),  get_comment_time()) ?></span>
					<?php if ($comment->comment_approved == '0') : ?>
						<em><i class="icon-info-sign"></i> <?php _e('Comment awaiting approval', 'afresh'); ?></em>
						<br />
					<?php endif; ?>
					<?php comment_text(); ?>
				</div>
						
			</div>
			
			
		</li>

		<?php 
	}



function prfx_admin_styles(){
    global $typenow;
    if( $typenow == 'post' ) {
        wp_enqueue_style( 'prfx_meta_box_styles', get_template_directory_uri() . '/css/meta-box-styles.css' );
        wp_enqueue_style( 'admin-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
    }
}
add_action( 'admin_print_styles', 'prfx_admin_styles' );
function prfx_image_enqueue() {
    global $typenow;
    if( $typenow == 'post' ) {
        wp_enqueue_media();
 
        // Registers and enqueues the required javascript.
        wp_register_script( 'meta-box-image', get_template_directory_uri() . '/js/meta-box-image.js', array( 'jquery' ) );
        		wp_register_script( 'moment-js', get_template_directory_uri() . '/js/moment.min.js', array( 'jquery' ), '20170403', true );

        wp_localize_script( 'meta-box-image', 'meta_image',
            array(
                'title' => __( 'Select Image', 'trendio' ),
                'button' => __( 'Use this image', 'trendio' ),
            )
        );
        wp_enqueue_script( 'meta-box-image' );
        wp_enqueue_script( 'moment-js' );
    }
}
add_action( 'admin_enqueue_scripts', 'prfx_image_enqueue' );
add_action( 'add_meta_boxes', 'cd_meta_box_add' );


add_action( 'add_meta_boxes', 'cd_meta_box_add' );
function cd_meta_box_add()
{
	add_meta_box( 'save_pinterest', 'Save To Pinterest', 'pin_callback', 'post', 'normal', 'high' );

}


function pin_callback( $post )
{
	
 	wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    ?>

		<div>
	<p>
    <label for="meta-image" class="prfx-row-title"><?php _e( 'Pin Image', 'prfx-textdomain' )?></label>
    <input type="text" name="meta-image" id="meta-image" value="<?php if ( isset ( $prfx_stored_meta['meta-image'] ) ) echo $prfx_stored_meta['meta-image'][0]; ?>" />
    <input type="button" id="meta-image-button" class="button" value="<?php _e( 'Choose or Upload an Image', 'prfx-textdomain' )?>" />
</p>

		</div>
		<?php	
}


add_action("wp_ajax_photo_upload", "photo_upload");
add_action("wp_ajax_nopriv_photo_upload", "photo_upload");
function photo_upload() {
   if ( !wp_verify_nonce( $_POST['nonce'], "add_photo_nonce")) {
      exit("No naughty business please");
   }   
   $s = preg_replace('/[^a-z0-9]+/i', '', $_POST['songname']);
   $a = preg_replace('/[^a-z0-9]+/i', '', $_POST['artistname']);
$filename =  'post';
$filename .= '_';
$filename =  $s;
$filename .= '_';
$filename .= $a;
$filename .= '_';
$filename .= $_POST['filename'];
   $uploaddir = wp_upload_dir();
$uploadfile = $uploaddir['path'] . '/' . $filename;
   if(!empty($_POST['photo'])) {
$contents= file_get_contents($_POST['photo']);
$savefile = fopen($uploadfile, 'w');
fwrite($savefile, $contents);
fclose($savefile);
$wp_filetype = wp_check_filetype(basename($filename), null );
$attachment = array(
    'post_mime_type' => $wp_filetype['type'],
    'post_title' => $filename,
    'post_content' => '',
    'post_status' => 'inherit'
);
	$love = get_post_meta( $_POST['post_id'], 'wpcf-release-image', true );
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { 
		
/*$attach_id = wp_insert_attachment( $attachment, $uploadfile );
$imagenew = get_post( $attach_id );
$fullsizepath = get_attached_file( $imagenew->ID );
$attach_data = wp_generate_attachment_metadata( $attach_id, $fullsizepath );
wp_update_attachment_metadata( $attach_id, $attach_data );
*/
	$love =  $uploaddir['url'] . '/' . $filename;
update_post_meta( $_POST['post_id'], 'wpcf-release-image', $uploadfile );
		echo $love;
}
   }
   die();
}


add_action( 'save_post', 'cd_from_save' );


function cd_from_save( $post_id )
{
 
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'prfx_nonce' ] ) && wp_verify_nonce( $_POST[ 'prfx_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
 
// Checks for input and saves if needed
if( isset( $_POST[ 'meta-image' ] ) ) {
    update_post_meta( $post_id, 'meta-image', $_POST[ 'meta-image' ] );
}
}


