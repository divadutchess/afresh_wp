<?php get_header() ?>

<section class="blog-type">
	
	<?php
          $args = array( 'posts_per_page' => 3,
         'category_name' => 'featured' );
          $myposts = new WP_Query( $args ); ?>

          <?php if ( $myposts->have_posts() ) : ?>
	<div class="featured slanted">
		<div class="container">
			<div class="inner-wrapper">
			<span class="blog-heading">featured</span>
				<div class="row">


	
        <?php while ( $myposts->have_posts() ) : $myposts->the_post(); ?>
      

					<div class="col-md-4">
						<div class="feature">

						<a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_post_thumbnail('featured-image'); ?></a>
						<div class="feature-blob"><span class="beneath"><?php echo get_the_date('d.m.Y')?></span>
					<h4><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title(); ?></a></h4>
					</div></div>
						
				</div>


   <?php endwhile; 
          wp_reset_postdata();?>


			</div></div>

		</div>


		
	</div>

<?php endif; ?>


	<div class="recent-posts">
		<div class="container">
			<div class="inner-wrapper">
		<span class="underneath">latest posts</span>

<?php $the_query = new WP_Query( array( 'cat' => '-9', 'posts_per_page' => 8, 'post_type' => 'post', 'post_status' => 'publish' ));
 if ( $the_query->have_posts() ) : ?>

 	<div class="all-blogs">
	<?php while ( $the_query->have_posts() ) : $the_query->the_post();  ?>

		<?php get_template_part( 'template-parts/content', get_post_type() ); ?>

  	<?php endwhile; ?>
  </div>

	<?php wp_reset_postdata(); ?>


	<script>
var posts_afresh = '<?php echo serialize( $the_query->query_vars ) ?>',
    current_page_afresh = <?php echo $the_query->query_vars['paged'] ?>,
    max_page_afresh = <?php echo $the_query->max_num_pages ?>
</script>

<?php
 
// don't display the button if there are not enough posts
if (  $the_query->max_num_pages > 1 )
	echo '<p class="text-center"><button class="misha_loadmore read-more">load more</button> </p>'; // you can use <a> as well
?>
<?php endif; ?>





	</div>
	</div>
	</div>

</section>


<?php get_footer() ?>