<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package afresh
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/idc5coe.css">
  <link rel="shortcut icon" href="/img/favicon.png" type="image/png" />
  <link rel="apple-touch-icon" href="/img/apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="57x57" href="/img/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="/img/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="/img/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon" sizes="120x120" href="/img/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="/img/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon" sizes="152x152" href="/img/apple-touch-icon-152x152.png" />
  <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon-180x180.png" />

  <!-- Google Fonts -->

  <link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet">



  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <?php if(site_url() != 'http://afresh.local'): ?>

  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>
    window.fbAsyncInit = function() {
      FB.init({
        xfbml            : true,
        version          : 'v3.2'
      });
    };

    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your customer chat code -->
    <div class="fb-customerchat"
    attribution=setup_tool
    page_id="366455007461683"
    theme_color="#272727"
    logged_in_greeting="Hey ya! Looking to give your website afresh look? Let's chat 😊"
    logged_out_greeting="Hey ya! Looking to give your website afresh look? Let's chat 😊">
  </div>


<?php endif; ?>

  <header role="banner" id="masthead" class="site-header">

    <nav class="navbar navbar-expand-md navbar-light text-center align-items-center">
      <div class="container">
        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
         <img src="<?php echo get_template_directory_uri();?>/img/logo.svg" width="200" role="logo"></a>

         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse dual-collapse2">

         <?php
         wp_nav_menu( array(
          'theme_location' => 'menu-1',
          'depth'	          => 1,
          'menu_class'        => 'navbar-nav ml-auto',
          'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
          'walker'          => new WP_Bootstrap_Navwalker(),
        ) );
        ?>





        <div class="nav-item nav-socials">
          <span class="nav-link"><span class="decorative">socials</span>
          <?php
          wp_nav_menu( array(
            'theme_location' => 'menu-3',
            'menu_class'        => 'header-socials',
          ) );
          ?>

        </span>
      </div>
    </div>
  </div>

</nav>




</header><!-- #masthead -->

<div id="content" class="site-content">
