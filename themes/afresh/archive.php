<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package afresh
 */

get_header();
?>

	<div id="primary" class="page-area">
		<main id="main" class="site-main">



		<?php if ( have_posts() ) : ?>

	<div class="portfolio-inner slanted">
		<div class="portfolio-header text-center">

<?php
		if ( have_posts() ) :?>
<h3><span><?php echo the_archive_title( ); ?></span></h3>
<?php the_archive_description() ?>	

<?php endif;  ?>
</div></div><!-- .page-header -->

<div class="container">
<div class="inner-wrapper">

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );?>

				</div></div>


		<?php endif; ?>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
