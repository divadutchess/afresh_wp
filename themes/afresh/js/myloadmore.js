jQuery(function($){ // use jQuery code inside this to avoid "$ is not defined" error
	$('.misha_loadmore').click(function(){
 
		var button = $(this),
		    data = {
			'action': 'loadmore',
			'query': posts_afresh, // that's how we get params from wp_localize_script() function
			'page' : current_page_afresh
		};
 
		$.ajax({ // you can also use $.post here
			url : misha_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); // change the button text, you can also add a preloader image
			},
			success : function( data ){
				if( data ) { 
					button.text( 'Load posts' ); // insert new posts
					$('.all-blogs').html(data);
					current_page_afresh++;
 
					if ( current_page_afresh == max_page_afresh ) 
						button.remove(); // if last page, remove the button
 
				
				} else {
					button.remove(); // if no data, remove the button as well
				}
			}
		});
	});
});