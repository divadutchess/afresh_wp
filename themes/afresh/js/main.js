(function ($) {

    "use strict";


$(document).ready(function(){
  new WOW().init();
  $('.prettySocial').prettySocial();
  var admin = 0;

  if($('#wpadminbar').length > 0) {
    $('.site-header').css('top', $('#wpadminbar').height())

  }


    $('.site-content').css('margin-top', $('.site-header').height()  )

	var owl = $('.owl-carousel');


  owl.owlCarousel({

  	items: 1,
  	loop: false,
  	navigation: true,
    autoplay: true,
  	onTranslated: function(event){
                if (event.item.index == 0) jQuery(".prev-owl").addClass('disabled');
                else jQuery(".prev-owl").removeClass('disabled');

                if (event.item.index == (event.item.count - 1)) jQuery(".next-owl").addClass('disabled');
                else jQuery(".next-owl").removeClass('disabled');
            },

    onInitialized: function(event){
			if (event.item.index == 0) jQuery(".prev-owl").addClass('disabled');
                else jQuery(".prev-owl").removeClass('disabled');
    }




  });


  // Custom Navigation Events
$(".next-owl").on('click',function(){        
   owl.trigger('next.owl');
})      
$(".prev-owl").on('click',function(){        
   owl.trigger('prev.owl');
})

//-------------------------------------//


var grid = document.querySelector('.grid');

if(grid) {


var $grid = $('.grid').isotope({
  itemSelector: '.grid__item',
  percentPosition: true,
   filter: '.web',
  visibleStyle: { transform: 'translateY(0)', opacity: 1 },
 hiddenStyle: { transform: 'translateY(100px)', opacity: 0 },

  masonry: {
    // use outer width of grid-sizer for columnWidth
  columnWidth: '.grid__col-sizer',
    gutter: '.grid__gutter-sizer',

  }
});



$grid.on( 'layoutComplete', function() {
  $('.loader-5').hide();
  $('.segmented-control').css('opacity', '1');

});

$grid.imagesLoaded().progress( function() {
  grid.classList.remove('are-images-unloaded');
  $grid.isotope('layout');
});


     var $filterLinks = $('.segmented-control > label');


 $filterLinks.click(function(){
    var $this = $(this);
    var selector = $this.data('filter');
    console.log(selector)


     $grid.isotope({
      filter: selector
    });

  });


}




});


$(window).on('resize', function(){
    $('.site-content').css('margin-top', $('.site-header').height()  )


})


$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

     //>=, not <=
    if (scroll >= 500) {
        //clearHeader, not clearheader - caps H
        $(".site-header").addClass("header-fixed");
    }

    else {
        $(".site-header").removeClass("header-fixed");

    }
}); //missing );

})(jQuery);