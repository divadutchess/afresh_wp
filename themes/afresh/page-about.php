<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package afresh
 */

get_header();
?>

	<div id="primary" class="page-area">
		<main id="main" class="site-main">


		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );?>

<section class="move-up">

<div class="row">

		<div class="col-md-8">
<?php the_content() ?>	

</div>
	<div class="col-md-4">
<?php the_post_thumbnail(); ?>


<?php echo $redux_demo['about-fields']?>

	</div>



</div>
</section>


		<?php endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
