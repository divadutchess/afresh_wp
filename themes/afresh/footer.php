<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package afresh
 */

?>

	</div><!-- #content -->

<?php if(!is_front_page()) { ?>

<section class="got-mail slightly">
	
<div class="container">
	<div class="inner-wrapper">

<div class="mail-text">

<p>I would like <span class="decorative">afresh</span> ...</p>

<div class="row">
	<a class="footer-href col-lg-3 col-6" href="#"><span class="footer-button btn-11"  data-toggle="modal" data-target="#contactModal">Brand</span></a>
	<a class="footer-href col-lg-3 col-6" href="#"><span class="footer-button btn-11"  data-toggle="modal" data-target="#contactModal">website</span></a>
	<a class="footer-href col-lg-3 col-6" href="#"><span class="footer-button btn-11"  data-toggle="modal" data-target="#contactModal">feed (social media)</span></a>
	<a class="footer-href col-lg-3 col-6" href="#"><span class="footer-button btn-11"  data-toggle="modal" data-target="#contactModal">anything</span></a>



</div>
	

</div>

	</div>

</div>


</section>

<?php } ?>


<footer class="footer" id="colophon" role="footer">
	<div class="container">
		<div class="inner-wrapper">
			<div class="row footer-row">


				<div class="col-md-4">
					<div class="footer-block">
	<div class=" footer-nav"><?php dynamic_sidebar( 'sidebar-2' ); ?>

					</div>
				</div>

			</div>

							<div class="col-md-4">
					<div class="footer-block">
						<img src="<?php echo get_template_directory_uri();?>/img/logo-circle.svg" width="105px" role="logo">
						

				
					</div></div>


				<div class="col-md-4">
					<div class="footer-block no-border">
									<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-3',
				'menu_class'        => 'footer-socials',
			) );
			?>
					</div>
				</div>



			</div>
		</div>

			<section class="copyright">
	<p>&copy; <?php echo the_date('Y');?> afreshcreative.ca | made with <svg width="14" height="12"><path d="M1.014.994a3.335 3.335 0 0 0 0 4.789l5.982 5.884 5.99-5.877a3.334 3.334 0 0 0 0-4.789 3.49 3.49 0 0 0-4.88 0L7.002 2.083 5.894.995a3.49 3.49 0 0 0-4.88 0z" fill="#EF0107" fill-rule="evenodd"></path></svg> in hamilton, on</p>
</section>
	</div>
</footer>






</div><!-- #page -->


<!-- Modal -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">contact </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php echo do_shortcode( '[contact-form-7 id="14" title="Contact form 1"]' ); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn usual" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132752881-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-132752881-1');
</script>


<?php wp_footer(); ?>

</body>
</html>
