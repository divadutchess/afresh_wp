"use strict";

jQuery(document).ready(function ($) {
  scrollAnim();
});

function scrollAnim() {
  // init ScrollMagic
  var scrollMagic = new ScrollMagic.Controller(); // Scale Animation Setup
  // .to('@target', @length, {@object})
  // var scale_tween = TweenMax.to('#scale-animation', 1, {
  //   transform: 'scale(.75)',
  //   ease: Linear.easeNone
  // });
  // // BG Animation Setup
  // // .to('@target', @length, {@object})
  // var bg_tween = TweenMax.to('#bg-trigger', 1, {
  //   backgroundColor: '#FFA500',
  //   ease: Linear.easeNone
  // });
  // // YoYo Animation Setup
  // // .to(@target, @length, @object)
  // var yoyo_tween = TweenMax.to('#yoyo-animation', 1, {
  //   transform: 'scale(2)',
  //   ease: Cubic.easeOut,
  //   repeat: -1, // this negative value repeats the animation
  //   yoyo: true // make it bounce…yo!
  // });
  // Reveal

  var reveal = document.querySelectorAll('.reveal-effect');
  $.each(reveal, function (index, reveal_item) {
    // if (!isMobile) {
    new ScrollMagic.Scene({
      triggerElement: reveal_item,
      triggerHook: 'onEnter',
      reverse: false,
      duration: 0,
      offset: 2
    }).setClassToggle(reveal_item, 'animate').addTo(scrollMagic); // }
  }); // Unfold

  var unfold = document.querySelectorAll('.unfold');
  $.each(unfold, function (index, reveal_item) {
    // if (!isMobile) {
    new ScrollMagic.Scene({
      triggerElement: reveal_item,
      triggerHook: 'onEnter',
      reverse: false,
      offset: 0,
      duration: 0
    }).setClassToggle(reveal_item, 'animate').addTo(scrollMagic); // }
  });
  var $bg_switch = document.querySelectorAll('.bg-switch');
  $.each($bg_switch, function (index, reveal_item) {
    var child = $(this).attr("data-bg-color"); //  var prev = $bg_switch[index].previousSibling();

    var thistween = TweenMax.fromTo($bg_switch[index], 1, {
      backgroundColor: '#fff'
    }, {
      backgroundColor: child,
      immediateRender: false
    }); // make scene

    new ScrollMagic.Scene({
      triggerElement: this,
      offset: 0,
      duration: '100%',
      triggerHook: 'onEnter'
    }).setTween(thistween).addTo(scrollMagic);
  });
  var $heading = document.querySelectorAll('.section__heading');
  $.each($heading, function (i, reveal_item) {
    var splitone = $heading[i].querySelector('h3');
    var splittwo = $heading[i].querySelector('span#section-title');
    var splitthree = $heading[i].querySelector('div');
    var tweenLine = new TimelineMax({
      delay: 0.3
    }); //if (!isMobile) {

    var $word = $("path#h-line");
    var $dot = $("path#h-square"); // prepare SVG

    pathPrepare($word);
    pathPrepare($dot);
    tweenLine.fromTo($word, 0.9, {
      strokeDashoffset: 0
    }, {
      ease: Linear.easeNone,
      immediateRender: false
    }, 1);
    tweenLine.fromTo($dot, 0.1, {
      strokeDashoffset: 0
    }, {
      ease: Linear.easeNone,
      immediateRender: false
    }, 1); // tweenLine.staggerFrom(splitone, .6, {
    //   y: 100,
    //   opacity: 0,
    //   ease: 'Circ.easeOut'
    // }, 0.2);

    tweenLine.staggerFrom(splittwo, 1.6, {
      y: 50,
      opacity: 0,
      ease: 'Circ.easeOut'
    }, 0.72);
    tweenLine.staggerFrom(splitthree, .6, {
      y: 50,
      opacity: 0,
      ease: 'Elastic.easeOut'
    }, 0.92);
    new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 'onEnter',
      reverse: false,
      offset: 100,
      duration: 200,
      tweenChanges: true
    }).setTween(tweenLine).setClassToggle($heading[i], 'animate').addTo(scrollMagic);
  });
  /** Fade Animation **/

  var all_animations = ['fadeInUp', 'bounceInUp'];
  $.each(all_animations, function (index, animate) {
    var $fadeIn = document.querySelectorAll('.' + all_animations[index]);
    $.each($fadeIn, function (i, item) {
      new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: 'onEnter',
        reverse: false,
        offset: 0,
        duration: 20
      }).setClassToggle(item, 'animated').addTo(scrollMagic);
    });
  });
  var $slideRight = document.querySelectorAll('.slideInRight');
  $.each($slideRight, function (i, item) {
    var tweenFade = new TweenMax.fromTo(item, 1, {
      opacity: 0,
      y: 50
    }, {
      opacity: 1,
      y: 0,
      ease: Elastic.easeOut
    });
    new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 'onEnter',
      reverse: false,
      offset: 0,
      duration: 15
    }) //.setClassToggle(item, 'animated')
    .setTween(tweenFade).addTo(scrollMagic);
  });
  /** Sliding Text **/

  var $tt_h = document.querySelectorAll('.tt-h');
  var duration = 8000;
  var here = duration - window.innerWidth;
  $.each($tt_h, function (index, reveal_item) {
    new ScrollMagic.Scene({
      triggerElement: '.section-portfolio',
      triggerHook: 'onLeave',
      offset: 0,
      duration: 2500,
      reverse: true
    }).setTween(TweenMax.fromTo(reveal_item, 1, {
      right: '-150vw'
    }, {
      right: 30,
      immediateRender: false,
      ease: Power4.easeInOut
    })) //  .addIndicators(true)
    .addTo(scrollMagic);
  });
  /** SlideLeftIn **/

  var $slideIn = document.querySelectorAll('.slidein');
  $.each($slideIn, function (i, item) {
    new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 'onEnter',
      reverse: false,
      offset: 0,
      duration: 0
    }).setTween(TweenMax.fromTo(item, 1.2, {
      x: -100,
      opacity: 0
    }, {
      x: '0',
      opacity: 1,
      ease: Elastic.easeInOut,
      immediateRender: true
    }, '-=1.8')).addTo(scrollMagic);
  });
  /** Scroll Up / Down **/

  var scrollAnimations = ['scrollUp', 'scrollDown'];
  $.each(scrollAnimations, function (i, item) {
    var $effect = document.querySelector('.' + item);

    if ($effect) {
      var items = $effect.querySelectorAll('span');
      $.each(items, function (i, animate) {
        var config = {};

        if (item == 'scrollUp') {
          config.yStart = -60;
          config.yEnd = -500;
        } else {
          config.yStart = -30;
          config.yEnd = 500;
        }

        new ScrollMagic.Scene({
          triggerElement: '.segmented-control',
          triggerHook: 'onEnter',
          reverse: true,
          offset: 50,
          duration: '100%'
        }).setTween(TweenMax.fromTo(this, 0.5, {
          x: config.yStart
        }, {
          opacity: 1,
          x: config.yEnd,
          ease: Power2.easeInOut
        })) //  .setPin(this)
        .addIndicators({
          name: "Hi (duration: 500)" + config.offset
        }).addTo(scrollMagic);
      });
    }
  });
}

function pathPrepare($el) {
  var lineLength = $el[0].getTotalLength();
  $el.css("stroke-dasharray", lineLength);
  $el.css("stroke-dashoffset", lineLength);
}
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
  * Bootstrap v4.3.1 (https://getbootstrap.com/)
  * Copyright 2011-2020 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
!function (t, e) {
  "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = e(require("popper.js")) : "function" == typeof define && define.amd ? define(["popper.js"], e) : (t = t || self).bootstrap = e(t.Popper);
}(void 0, function (t) {
  "use strict";

  function e(t, e) {
    for (var n = 0; n < e.length; n++) {
      var i = e[n];
      i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
    }
  }

  function n(t, n, i) {
    return n && e(t.prototype, n), i && e(t, i), t;
  }

  function i(t, e, n) {
    return e in t ? Object.defineProperty(t, e, {
      value: n,
      enumerable: !0,
      configurable: !0,
      writable: !0
    }) : t[e] = n, t;
  }

  function o(t, e) {
    var n = Object.keys(t);

    if (Object.getOwnPropertySymbols) {
      var i = Object.getOwnPropertySymbols(t);
      e && (i = i.filter(function (e) {
        return Object.getOwnPropertyDescriptor(t, e).enumerable;
      })), n.push.apply(n, i);
    }

    return n;
  }

  function r(t) {
    for (var e = 1; e < arguments.length; e++) {
      var n = null != arguments[e] ? arguments[e] : {};
      e % 2 ? o(n, !0).forEach(function (e) {
        i(t, e, n[e]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n)) : o(n).forEach(function (e) {
        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e));
      });
    }

    return t;
  }

  t = t && t.hasOwnProperty("default") ? t["default"] : t;

  var s,
      a,
      l,
      c = "transitionend",
      u = function u(t) {
    do {
      t += ~~(1e6 * Math.random());
    } while (document.getElementById(t));

    return t;
  },
      f = function f(t) {
    var e = t.getAttribute("data-target");

    if (!e || "#" === e) {
      var n = t.getAttribute("href");
      e = n && "#" !== n ? n.trim() : null;
    }

    return e;
  },
      h = function h(t) {
    var e = f(t);
    return e && document.querySelector(e) ? e : null;
  },
      d = function d(t) {
    var e = f(t);
    return e ? document.querySelector(e) : null;
  },
      g = function g(t) {
    if (!t) return 0;
    var e = window.getComputedStyle(t),
        n = e.transitionDuration,
        i = e.transitionDelay,
        o = parseFloat(n),
        r = parseFloat(i);
    return o || r ? (n = n.split(",")[0], i = i.split(",")[0], 1e3 * (parseFloat(n) + parseFloat(i))) : 0;
  },
      p = function p(t) {
    var e = document.createEvent("HTMLEvents");
    e.initEvent(c, !0, !0), t.dispatchEvent(e);
  },
      _ = function _(t) {
    return (t[0] || t).nodeType;
  },
      m = function m(t, e) {
    var n = !1,
        i = e + 5;
    t.addEventListener(c, function e() {
      n = !0, t.removeEventListener(c, e);
    }), setTimeout(function () {
      n || p(t);
    }, i);
  },
      v = function v(t, e, n) {
    Object.keys(n).forEach(function (i) {
      var o,
          r = n[i],
          s = e[i],
          a = s && _(s) ? "element" : (o = s, {}.toString.call(o).match(/\s([a-z]+)/i)[1].toLowerCase());
      if (!new RegExp(r).test(a)) throw new Error(t.toUpperCase() + ': Option "' + i + '" provided type "' + a + '" but expected type "' + r + '".');
    });
  },
      E = function E(t) {
    return t ? [].slice.call(t) : [];
  },
      y = function y(t) {
    if (!t) return !1;

    if (t.style && t.parentNode && t.parentNode.style) {
      var e = getComputedStyle(t),
          n = getComputedStyle(t.parentNode);
      return "none" !== e.display && "none" !== n.display && "hidden" !== e.visibility;
    }

    return !1;
  },
      b = function b() {
    return function () {};
  },
      D = function D(t) {
    return t.offsetHeight;
  },
      T = function T() {
    var t = window.jQuery;
    return t && !document.body.hasAttribute("data-no-jquery") ? t : null;
  },
      I = (s = {}, a = 1, {
    set: function set(t, e, n) {
      "undefined" == typeof t.key && (t.key = {
        key: e,
        id: a
      }, a++), s[t.key.id] = n;
    },
    get: function get(t, e) {
      if (!t || "undefined" == typeof t.key) return null;
      var n = t.key;
      return n.key === e ? s[n.id] : null;
    },
    "delete": function _delete(t, e) {
      if ("undefined" != typeof t.key) {
        var n = t.key;
        n.key === e && (delete s[n.id], delete t.key);
      }
    }
  }),
      A = {
    setData: function setData(t, e, n) {
      I.set(t, e, n);
    },
    getData: function getData(t, e) {
      return I.get(t, e);
    },
    removeData: function removeData(t, e) {
      I["delete"](t, e);
    }
  },
      S = Element.prototype,
      w = S.matches,
      C = S.closest,
      L = Element.prototype.querySelectorAll,
      O = Element.prototype.querySelector,
      N = function N(t, e) {
    return new CustomEvent(t, e);
  };

  if ("function" != typeof window.CustomEvent && (N = function N(t, e) {
    e = e || {
      bubbles: !1,
      cancelable: !1,
      detail: null
    };
    var n = document.createEvent("CustomEvent");
    return n.initCustomEvent(t, e.bubbles, e.cancelable, e.detail), n;
  }), !((l = document.createEvent("CustomEvent")).initEvent("Bootstrap", !0, !0), l.preventDefault(), l.defaultPrevented)) {
    var k = Event.prototype.preventDefault;

    Event.prototype.preventDefault = function () {
      this.cancelable && (k.call(this), Object.defineProperty(this, "defaultPrevented", {
        get: function get() {
          return !0;
        },
        configurable: !0
      }));
    };
  }

  var P = function () {
    var t = N("Bootstrap", {
      cancelable: !0
    }),
        e = document.createElement("div");
    return e.addEventListener("Bootstrap", function () {
      return null;
    }), t.preventDefault(), e.dispatchEvent(t), t.defaultPrevented;
  }();

  w || (w = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector), C || (C = function C(t) {
    var e = this;

    do {
      if (w.call(e, t)) return e;
      e = e.parentElement || e.parentNode;
    } while (null !== e && 1 === e.nodeType);

    return null;
  });
  var H = /:scope\b/;
  (function () {
    var t = document.createElement("div");

    try {
      t.querySelectorAll(":scope *");
    } catch (t) {
      return !1;
    }

    return !0;
  })() || (L = function L(t) {
    if (!H.test(t)) return this.querySelectorAll(t);
    var e = Boolean(this.id);
    e || (this.id = u("scope"));
    var n = null;

    try {
      t = t.replace(H, "#" + this.id), n = this.querySelectorAll(t);
    } finally {
      e || this.removeAttribute("id");
    }

    return n;
  }, O = function O(t) {
    if (!H.test(t)) return this.querySelector(t);
    var e = L.call(this, t);
    return "undefined" != typeof e[0] ? e[0] : null;
  });
  var j = T(),
      M = /[^.]*(?=\..*)\.|.*/,
      R = /\..*/,
      x = /^key/,
      W = /::\d+$/,
      U = {},
      K = 1,
      V = {
    mouseenter: "mouseover",
    mouseleave: "mouseout"
  },
      B = ["click", "dblclick", "mouseup", "mousedown", "contextmenu", "mousewheel", "DOMMouseScroll", "mouseover", "mouseout", "mousemove", "selectstart", "selectend", "keydown", "keypress", "keyup", "orientationchange", "touchstart", "touchmove", "touchend", "touchcancel", "pointerdown", "pointermove", "pointerup", "pointerleave", "pointercancel", "gesturestart", "gesturechange", "gestureend", "focus", "blur", "change", "reset", "select", "submit", "focusin", "focusout", "load", "unload", "beforeunload", "resize", "move", "DOMContentLoaded", "readystatechange", "error", "abort", "scroll"];

  function F(t, e) {
    return e && e + "::" + K++ || t.uidEvent || K++;
  }

  function Q(t) {
    var e = F(t);
    return t.uidEvent = e, U[e] = U[e] || {}, U[e];
  }

  function Y(t, e) {
    null === t.which && x.test(t.type) && (t.which = null === t.charCode ? t.keyCode : t.charCode), t.delegateTarget = e;
  }

  function G(t, e, n) {
    void 0 === n && (n = null);

    for (var i = Object.keys(t), o = 0, r = i.length; o < r; o++) {
      var s = t[i[o]];
      if (s.originalHandler === e && s.delegationSelector === n) return s;
    }

    return null;
  }

  function X(t, e, n) {
    var i = "string" == typeof e,
        o = i ? n : e,
        r = t.replace(R, ""),
        s = V[r];
    return s && (r = s), B.indexOf(r) > -1 || (r = t), [i, o, r];
  }

  function q(t, e, n, i, o) {
    if ("string" == typeof e && t) {
      n || (n = i, i = null);
      var r = X(e, n, i),
          s = r[0],
          a = r[1],
          l = r[2],
          c = Q(t),
          u = c[l] || (c[l] = {}),
          f = G(u, a, s ? n : null);
      if (f) f.oneOff = f.oneOff && o;else {
        var h = F(a, e.replace(M, "")),
            d = s ? function (t, e, n) {
          return function i(o) {
            for (var r = t.querySelectorAll(e), s = o.target; s && s !== this; s = s.parentNode) {
              for (var a = r.length; a--;) {
                if (r[a] === s) return Y(o, s), i.oneOff && Z.off(t, o.type, n), n.apply(s, [o]);
              }
            }

            return null;
          };
        }(t, n, i) : function (t, e) {
          return function n(i) {
            return Y(i, t), n.oneOff && Z.off(t, i.type, e), e.apply(t, [i]);
          };
        }(t, n);
        d.delegationSelector = s ? n : null, d.originalHandler = a, d.oneOff = o, d.uidEvent = h, u[h] = d, t.addEventListener(l, d, s);
      }
    }
  }

  function z(t, e, n, i, o) {
    var r = G(e[n], i, o);
    r && (t.removeEventListener(n, r, Boolean(o)), delete e[n][r.uidEvent]);
  }

  var Z = {
    on: function on(t, e, n, i) {
      q(t, e, n, i, !1);
    },
    one: function one(t, e, n, i) {
      q(t, e, n, i, !0);
    },
    off: function off(t, e, n, i) {
      if ("string" == typeof e && t) {
        var o = X(e, n, i),
            r = o[0],
            s = o[1],
            a = o[2],
            l = a !== e,
            c = Q(t),
            u = "." === e.charAt(0);

        if ("undefined" == typeof s) {
          u && Object.keys(c).forEach(function (n) {
            !function (t, e, n, i) {
              var o = e[n] || {};
              Object.keys(o).forEach(function (r) {
                if (r.indexOf(i) > -1) {
                  var s = o[r];
                  z(t, e, n, s.originalHandler, s.delegationSelector);
                }
              });
            }(t, c, n, e.slice(1));
          });
          var f = c[a] || {};
          Object.keys(f).forEach(function (n) {
            var i = n.replace(W, "");

            if (!l || e.indexOf(i) > -1) {
              var o = f[n];
              z(t, c, a, o.originalHandler, o.delegationSelector);
            }
          });
        } else {
          if (!c || !c[a]) return;
          z(t, c, a, s, r ? n : null);
        }
      }
    },
    trigger: function trigger(t, e, n) {
      if ("string" != typeof e || !t) return null;
      var i,
          o = e.replace(R, ""),
          r = e !== o,
          s = B.indexOf(o) > -1,
          a = !0,
          l = !0,
          c = !1,
          u = null;
      return r && j && (i = j.Event(e, n), j(t).trigger(i), a = !i.isPropagationStopped(), l = !i.isImmediatePropagationStopped(), c = i.isDefaultPrevented()), s ? (u = document.createEvent("HTMLEvents")).initEvent(o, a, !0) : u = N(e, {
        bubbles: a,
        cancelable: !0
      }), "undefined" != typeof n && Object.keys(n).forEach(function (t) {
        Object.defineProperty(u, t, {
          get: function get() {
            return n[t];
          }
        });
      }), c && (u.preventDefault(), P || Object.defineProperty(u, "defaultPrevented", {
        get: function get() {
          return !0;
        }
      })), l && t.dispatchEvent(u), u.defaultPrevented && "undefined" != typeof i && i.preventDefault(), u;
    }
  },
      $ = {
    matches: function matches(t, e) {
      return w.call(t, e);
    },
    find: function find(t, e) {
      return void 0 === e && (e = document.documentElement), L.call(e, t);
    },
    findOne: function findOne(t, e) {
      return void 0 === e && (e = document.documentElement), O.call(e, t);
    },
    children: function children(t, e) {
      var n = this,
          i = E(t.children);
      return i.filter(function (t) {
        return n.matches(t, e);
      });
    },
    parents: function parents(t, e) {
      for (var n = [], i = t.parentNode; i && i.nodeType === Node.ELEMENT_NODE && 3 !== i.nodeType;) {
        this.matches(i, e) && n.push(i), i = i.parentNode;
      }

      return n;
    },
    closest: function closest(t, e) {
      return C.call(t, e);
    },
    prev: function prev(t, e) {
      for (var n = [], i = t.previousSibling; i && i.nodeType === Node.ELEMENT_NODE && 3 !== i.nodeType;) {
        this.matches(i, e) && n.push(i), i = i.previousSibling;
      }

      return n;
    }
  },
      J = "bs.alert",
      tt = "." + J,
      et = {
    CLOSE: "close" + tt,
    CLOSED: "closed" + tt,
    CLICK_DATA_API: "click" + tt + ".data-api"
  },
      nt = "alert",
      it = "fade",
      ot = "show",
      rt = function () {
    function t(t) {
      this._element = t, this._element && A.setData(t, J, this);
    }

    var e = t.prototype;
    return e.close = function (t) {
      var e = this._element;
      t && (e = this._getRootElement(t));

      var n = this._triggerCloseEvent(e);

      null === n || n.defaultPrevented || this._removeElement(e);
    }, e.dispose = function () {
      A.removeData(this._element, J), this._element = null;
    }, e._getRootElement = function (t) {
      var e = d(t);
      return e || (e = $.closest(t, "." + nt)), e;
    }, e._triggerCloseEvent = function (t) {
      return Z.trigger(t, et.CLOSE);
    }, e._removeElement = function (t) {
      var e = this;

      if (t.classList.remove(ot), t.classList.contains(it)) {
        var n = g(t);
        Z.one(t, c, function () {
          return e._destroyElement(t);
        }), m(t, n);
      } else this._destroyElement(t);
    }, e._destroyElement = function (t) {
      t.parentNode && t.parentNode.removeChild(t), Z.trigger(t, et.CLOSED);
    }, t.jQueryInterface = function (e) {
      return this.each(function () {
        var n = A.getData(this, J);
        n || (n = new t(this)), "close" === e && n[e](this);
      });
    }, t.handleDismiss = function (t) {
      return function (e) {
        e && e.preventDefault(), t.close(this);
      };
    }, t.getInstance = function (t) {
      return A.getData(t, J);
    }, n(t, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }]), t;
  }();

  Z.on(document, et.CLICK_DATA_API, '[data-dismiss="alert"]', rt.handleDismiss(new rt()));
  var st = T();

  if (st) {
    var at = st.fn.alert;
    st.fn.alert = rt.jQueryInterface, st.fn.alert.Constructor = rt, st.fn.alert.noConflict = function () {
      return st.fn.alert = at, rt.jQueryInterface;
    };
  }

  var lt = "bs.button",
      ct = "." + lt,
      ut = "active",
      ft = "btn",
      ht = "focus",
      dt = '[data-toggle^="button"]',
      gt = '[data-toggle="buttons"]',
      pt = 'input:not([type="hidden"])',
      _t = ".active",
      mt = ".btn",
      vt = {
    CLICK_DATA_API: "click" + ct + ".data-api",
    FOCUS_DATA_API: "focus" + ct + ".data-api",
    BLUR_DATA_API: "blur" + ct + ".data-api"
  },
      Et = function () {
    function t(t) {
      this._element = t, A.setData(t, lt, this);
    }

    var e = t.prototype;
    return e.toggle = function () {
      var t = !0,
          e = !0,
          n = $.closest(this._element, gt);

      if (n) {
        var i = $.findOne(pt, this._element);

        if (i && "radio" === i.type) {
          if (i.checked && this._element.classList.contains(ut)) t = !1;else {
            var o = $.findOne(_t, n);
            o && o.classList.remove(ut);
          }

          if (t) {
            if (i.hasAttribute("disabled") || n.hasAttribute("disabled") || i.classList.contains("disabled") || n.classList.contains("disabled")) return;
            i.checked = !this._element.classList.contains(ut), Z.trigger(i, "change");
          }

          i.focus(), e = !1;
        }
      }

      e && this._element.setAttribute("aria-pressed", !this._element.classList.contains(ut)), t && this._element.classList.toggle(ut);
    }, e.dispose = function () {
      A.removeData(this._element, lt), this._element = null;
    }, t.jQueryInterface = function (e) {
      return this.each(function () {
        var n = A.getData(this, lt);
        n || (n = new t(this)), "toggle" === e && n[e]();
      });
    }, t.getInstance = function (t) {
      return A.getData(t, lt);
    }, n(t, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }]), t;
  }();

  Z.on(document, vt.CLICK_DATA_API, dt, function (t) {
    t.preventDefault();
    var e = t.target;
    e.classList.contains(ft) || (e = $.closest(e, mt));
    var n = A.getData(e, lt);
    n || (n = new Et(e)), n.toggle();
  }), Z.on(document, vt.FOCUS_DATA_API, dt, function (t) {
    var e = $.closest(t.target, mt);
    e && e.classList.add(ht);
  }), Z.on(document, vt.BLUR_DATA_API, dt, function (t) {
    var e = $.closest(t.target, mt);
    e && e.classList.remove(ht);
  });
  var yt = T();

  if (yt) {
    var bt = yt.fn.button;
    yt.fn.button = Et.jQueryInterface, yt.fn.button.Constructor = Et, yt.fn.button.noConflict = function () {
      return yt.fn.button = bt, Et.jQueryInterface;
    };
  }

  function Dt(t) {
    return "true" === t || "false" !== t && (t === Number(t).toString() ? Number(t) : "" === t || "null" === t ? null : t);
  }

  function Tt(t) {
    return t.replace(/[A-Z]/g, function (t) {
      return "-" + t.toLowerCase();
    });
  }

  var It = {
    setDataAttribute: function setDataAttribute(t, e, n) {
      t.setAttribute("data-" + Tt(e), n);
    },
    removeDataAttribute: function removeDataAttribute(t, e) {
      t.removeAttribute("data-" + Tt(e));
    },
    getDataAttributes: function getDataAttributes(t) {
      if (!t) return {};
      var e = r({}, t.dataset);
      return Object.keys(e).forEach(function (t) {
        e[t] = Dt(e[t]);
      }), e;
    },
    getDataAttribute: function getDataAttribute(t, e) {
      return Dt(t.getAttribute("data-" + Tt(e)));
    },
    offset: function offset(t) {
      var e = t.getBoundingClientRect();
      return {
        top: e.top + document.body.scrollTop,
        left: e.left + document.body.scrollLeft
      };
    },
    position: function position(t) {
      return {
        top: t.offsetTop,
        left: t.offsetLeft
      };
    },
    toggleClass: function toggleClass(t, e) {
      t && (t.classList.contains(e) ? t.classList.remove(e) : t.classList.add(e));
    }
  },
      At = "carousel",
      St = "bs.carousel",
      wt = "." + St,
      Ct = {
    interval: 5e3,
    keyboard: !0,
    slide: !1,
    pause: "hover",
    wrap: !0,
    touch: !0
  },
      Lt = {
    interval: "(number|boolean)",
    keyboard: "boolean",
    slide: "(boolean|string)",
    pause: "(string|boolean)",
    wrap: "boolean",
    touch: "boolean"
  },
      Ot = "next",
      Nt = "prev",
      kt = "left",
      Pt = "right",
      Ht = {
    SLIDE: "slide" + wt,
    SLID: "slid" + wt,
    KEYDOWN: "keydown" + wt,
    MOUSEENTER: "mouseenter" + wt,
    MOUSELEAVE: "mouseleave" + wt,
    TOUCHSTART: "touchstart" + wt,
    TOUCHMOVE: "touchmove" + wt,
    TOUCHEND: "touchend" + wt,
    POINTERDOWN: "pointerdown" + wt,
    POINTERUP: "pointerup" + wt,
    DRAG_START: "dragstart" + wt,
    LOAD_DATA_API: "load" + wt + ".data-api",
    CLICK_DATA_API: "click" + wt + ".data-api"
  },
      jt = "carousel",
      Mt = "active",
      Rt = "slide",
      xt = "carousel-item-right",
      Wt = "carousel-item-left",
      Ut = "carousel-item-next",
      Kt = "carousel-item-prev",
      Vt = "pointer-event",
      Bt = {
    ACTIVE: ".active",
    ACTIVE_ITEM: ".active.carousel-item",
    ITEM: ".carousel-item",
    ITEM_IMG: ".carousel-item img",
    NEXT_PREV: ".carousel-item-next, .carousel-item-prev",
    INDICATORS: ".carousel-indicators",
    DATA_SLIDE: "[data-slide], [data-slide-to]",
    DATA_RIDE: '[data-ride="carousel"]'
  },
      Ft = {
    TOUCH: "touch",
    PEN: "pen"
  },
      Qt = function () {
    function t(t, e) {
      this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this.touchStartX = 0, this.touchDeltaX = 0, this._config = this._getConfig(e), this._element = t, this._indicatorsElement = $.findOne(Bt.INDICATORS, this._element), this._touchSupported = "ontouchstart" in document.documentElement || navigator.maxTouchPoints > 0, this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent), this._addEventListeners(), A.setData(t, St, this);
    }

    var e = t.prototype;
    return e.next = function () {
      this._isSliding || this._slide(Ot);
    }, e.nextWhenVisible = function () {
      !document.hidden && y(this._element) && this.next();
    }, e.prev = function () {
      this._isSliding || this._slide(Nt);
    }, e.pause = function (t) {
      t || (this._isPaused = !0), $.findOne(Bt.NEXT_PREV, this._element) && (p(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null;
    }, e.cycle = function (t) {
      t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config && this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval));
    }, e.to = function (t) {
      var e = this;
      this._activeElement = $.findOne(Bt.ACTIVE_ITEM, this._element);

      var n = this._getItemIndex(this._activeElement);

      if (!(t > this._items.length - 1 || t < 0)) if (this._isSliding) Z.one(this._element, Ht.SLID, function () {
        return e.to(t);
      });else {
        if (n === t) return this.pause(), void this.cycle();
        var i = t > n ? Ot : Nt;

        this._slide(i, this._items[t]);
      }
    }, e.dispose = function () {
      Z.off(this._element, wt), A.removeData(this._element, St), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null;
    }, e._getConfig = function (t) {
      return t = r({}, Ct, {}, t), v(At, t, Lt), t;
    }, e._handleSwipe = function () {
      var t = Math.abs(this.touchDeltaX);

      if (!(t <= 40)) {
        var e = t / this.touchDeltaX;
        this.touchDeltaX = 0, e > 0 && this.prev(), e < 0 && this.next();
      }
    }, e._addEventListeners = function () {
      var t = this;
      this._config.keyboard && Z.on(this._element, Ht.KEYDOWN, function (e) {
        return t._keydown(e);
      }), "hover" === this._config.pause && (Z.on(this._element, Ht.MOUSEENTER, function (e) {
        return t.pause(e);
      }), Z.on(this._element, Ht.MOUSELEAVE, function (e) {
        return t.cycle(e);
      })), this._config.touch && this._touchSupported && this._addTouchEventListeners();
    }, e._addTouchEventListeners = function () {
      var t = this,
          e = function e(_e2) {
        t._pointerEvent && Ft[_e2.pointerType.toUpperCase()] ? t.touchStartX = _e2.clientX : t._pointerEvent || (t.touchStartX = _e2.touches[0].clientX);
      },
          n = function n(e) {
        t._pointerEvent && Ft[e.pointerType.toUpperCase()] && (t.touchDeltaX = e.clientX - t.touchStartX), t._handleSwipe(), "hover" === t._config.pause && (t.pause(), t.touchTimeout && clearTimeout(t.touchTimeout), t.touchTimeout = setTimeout(function (e) {
          return t.cycle(e);
        }, 500 + t._config.interval));
      };

      E($.find(Bt.ITEM_IMG, this._element)).forEach(function (t) {
        Z.on(t, Ht.DRAG_START, function (t) {
          return t.preventDefault();
        });
      }), this._pointerEvent ? (Z.on(this._element, Ht.POINTERDOWN, function (t) {
        return e(t);
      }), Z.on(this._element, Ht.POINTERUP, function (t) {
        return n(t);
      }), this._element.classList.add(Vt)) : (Z.on(this._element, Ht.TOUCHSTART, function (t) {
        return e(t);
      }), Z.on(this._element, Ht.TOUCHMOVE, function (e) {
        return function (e) {
          e.touches && e.touches.length > 1 ? t.touchDeltaX = 0 : t.touchDeltaX = e.touches[0].clientX - t.touchStartX;
        }(e);
      }), Z.on(this._element, Ht.TOUCHEND, function (t) {
        return n(t);
      }));
    }, e._keydown = function (t) {
      if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {
        case 37:
          t.preventDefault(), this.prev();
          break;

        case 39:
          t.preventDefault(), this.next();
      }
    }, e._getItemIndex = function (t) {
      return this._items = t && t.parentNode ? E($.find(Bt.ITEM, t.parentNode)) : [], this._items.indexOf(t);
    }, e._getItemByDirection = function (t, e) {
      var n = t === Ot,
          i = t === Nt,
          o = this._getItemIndex(e),
          r = this._items.length - 1;

      if ((i && 0 === o || n && o === r) && !this._config.wrap) return e;
      var s = (o + (t === Nt ? -1 : 1)) % this._items.length;
      return -1 === s ? this._items[this._items.length - 1] : this._items[s];
    }, e._triggerSlideEvent = function (t, e) {
      var n = this._getItemIndex(t),
          i = this._getItemIndex($.findOne(Bt.ACTIVE_ITEM, this._element));

      return Z.trigger(this._element, Ht.SLIDE, {
        relatedTarget: t,
        direction: e,
        from: i,
        to: n
      });
    }, e._setActiveIndicatorElement = function (t) {
      if (this._indicatorsElement) {
        for (var e = $.find(Bt.ACTIVE, this._indicatorsElement), n = 0; n < e.length; n++) {
          e[n].classList.remove(Mt);
        }

        var i = this._indicatorsElement.children[this._getItemIndex(t)];

        i && i.classList.add(Mt);
      }
    }, e._slide = function (t, e) {
      var n,
          i,
          o,
          r = this,
          s = $.findOne(Bt.ACTIVE_ITEM, this._element),
          a = this._getItemIndex(s),
          l = e || s && this._getItemByDirection(t, s),
          u = this._getItemIndex(l),
          f = Boolean(this._interval);

      if (t === Ot ? (n = Wt, i = Ut, o = kt) : (n = xt, i = Kt, o = Pt), l && l.classList.contains(Mt)) this._isSliding = !1;else if (!this._triggerSlideEvent(l, o).defaultPrevented && s && l) {
        if (this._isSliding = !0, f && this.pause(), this._setActiveIndicatorElement(l), this._element.classList.contains(Rt)) {
          l.classList.add(i), D(l), s.classList.add(n), l.classList.add(n);
          var h = parseInt(l.getAttribute("data-interval"), 10);
          h ? (this._config.defaultInterval = this._config.defaultInterval || this._config.interval, this._config.interval = h) : this._config.interval = this._config.defaultInterval || this._config.interval;
          var d = g(s);
          Z.one(s, c, function () {
            l.classList.remove(n), l.classList.remove(i), l.classList.add(Mt), s.classList.remove(Mt), s.classList.remove(i), s.classList.remove(n), r._isSliding = !1, setTimeout(function () {
              Z.trigger(r._element, Ht.SLID, {
                relatedTarget: l,
                direction: o,
                from: a,
                to: u
              });
            }, 0);
          }), m(s, d);
        } else s.classList.remove(Mt), l.classList.add(Mt), this._isSliding = !1, Z.trigger(this._element, Ht.SLID, {
          relatedTarget: l,
          direction: o,
          from: a,
          to: u
        });

        f && this.cycle();
      }
    }, t.carouselInterface = function (e, n) {
      var i = A.getData(e, St),
          o = r({}, Ct, {}, It.getDataAttributes(e));
      "object" == _typeof(n) && (o = r({}, o, {}, n));
      var s = "string" == typeof n ? n : o.slide;
      if (i || (i = new t(e, o)), "number" == typeof n) i.to(n);else if ("string" == typeof s) {
        if ("undefined" == typeof i[s]) throw new TypeError('No method named "' + s + '"');
        i[s]();
      } else o.interval && o.ride && (i.pause(), i.cycle());
    }, t.jQueryInterface = function (e) {
      return this.each(function () {
        t.carouselInterface(this, e);
      });
    }, t.dataApiClickHandler = function (e) {
      var n = d(this);

      if (n && n.classList.contains(jt)) {
        var i = r({}, It.getDataAttributes(n), {}, It.getDataAttributes(this)),
            o = this.getAttribute("data-slide-to");
        o && (i.interval = !1), t.carouselInterface(n, i), o && A.getData(n, St).to(o), e.preventDefault();
      }
    }, t.getInstance = function (t) {
      return A.getData(t, St);
    }, n(t, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return Ct;
      }
    }]), t;
  }();

  Z.on(document, Ht.CLICK_DATA_API, Bt.DATA_SLIDE, Qt.dataApiClickHandler), Z.on(window, Ht.LOAD_DATA_API, function () {
    for (var t = E($.find(Bt.DATA_RIDE)), e = 0, n = t.length; e < n; e++) {
      Qt.carouselInterface(t[e], A.getData(t[e], St));
    }
  });
  var Yt = T();

  if (Yt) {
    var Gt = Yt.fn[At];
    Yt.fn[At] = Qt.jQueryInterface, Yt.fn[At].Constructor = Qt, Yt.fn[At].noConflict = function () {
      return Yt.fn[At] = Gt, Qt.jQueryInterface;
    };
  }

  var Xt = "collapse",
      qt = "bs.collapse",
      zt = "." + qt,
      Zt = {
    toggle: !0,
    parent: ""
  },
      $t = {
    toggle: "boolean",
    parent: "(string|element)"
  },
      Jt = {
    SHOW: "show" + zt,
    SHOWN: "shown" + zt,
    HIDE: "hide" + zt,
    HIDDEN: "hidden" + zt,
    CLICK_DATA_API: "click" + zt + ".data-api"
  },
      te = "show",
      ee = "collapse",
      ne = "collapsing",
      ie = "collapsed",
      oe = "width",
      re = "height",
      se = {
    ACTIVES: ".show, .collapsing",
    DATA_TOGGLE: '[data-toggle="collapse"]'
  },
      ae = function () {
    function t(t, e) {
      this._isTransitioning = !1, this._element = t, this._config = this._getConfig(e), this._triggerArray = E($.find('[data-toggle="collapse"][href="#' + t.id + '"],[data-toggle="collapse"][data-target="#' + t.id + '"]'));

      for (var n = E($.find(se.DATA_TOGGLE)), i = 0, o = n.length; i < o; i++) {
        var r = n[i],
            s = h(r),
            a = E($.find(s)).filter(function (e) {
          return e === t;
        });
        null !== s && a.length && (this._selector = s, this._triggerArray.push(r));
      }

      this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle(), A.setData(t, qt, this);
    }

    var e = t.prototype;
    return e.toggle = function () {
      this._element.classList.contains(te) ? this.hide() : this.show();
    }, e.show = function () {
      var e = this;

      if (!this._isTransitioning && !this._element.classList.contains(te)) {
        var n, i;
        this._parent && 0 === (n = E($.find(se.ACTIVES, this._parent)).filter(function (t) {
          return "string" == typeof e._config.parent ? t.getAttribute("data-parent") === e._config.parent : t.classList.contains(ee);
        })).length && (n = null);
        var o = $.findOne(this._selector);

        if (n) {
          var r = n.filter(function (t) {
            return o !== t;
          });
          if ((i = r[0] ? A.getData(r[0], qt) : null) && i._isTransitioning) return;
        }

        if (!Z.trigger(this._element, Jt.SHOW).defaultPrevented) {
          n && n.forEach(function (e) {
            o !== e && t.collapseInterface(e, "hide"), i || A.setData(e, qt, null);
          });

          var s = this._getDimension();

          this._element.classList.remove(ee), this._element.classList.add(ne), this._element.style[s] = 0, this._triggerArray.length && this._triggerArray.forEach(function (t) {
            t.classList.remove(ie), t.setAttribute("aria-expanded", !0);
          }), this.setTransitioning(!0);
          var a = "scroll" + (s[0].toUpperCase() + s.slice(1)),
              l = g(this._element);
          Z.one(this._element, c, function () {
            e._element.classList.remove(ne), e._element.classList.add(ee), e._element.classList.add(te), e._element.style[s] = "", e.setTransitioning(!1), Z.trigger(e._element, Jt.SHOWN);
          }), m(this._element, l), this._element.style[s] = this._element[a] + "px";
        }
      }
    }, e.hide = function () {
      var t = this;

      if (!this._isTransitioning && this._element.classList.contains(te) && !Z.trigger(this._element, Jt.HIDE).defaultPrevented) {
        var e = this._getDimension();

        this._element.style[e] = this._element.getBoundingClientRect()[e] + "px", D(this._element), this._element.classList.add(ne), this._element.classList.remove(ee), this._element.classList.remove(te);
        var n = this._triggerArray.length;
        if (n > 0) for (var i = 0; i < n; i++) {
          var o = this._triggerArray[i],
              r = d(o);
          r && !r.classList.contains(te) && (o.classList.add(ie), o.setAttribute("aria-expanded", !1));
        }
        this.setTransitioning(!0);
        this._element.style[e] = "";
        var s = g(this._element);
        Z.one(this._element, c, function () {
          t.setTransitioning(!1), t._element.classList.remove(ne), t._element.classList.add(ee), Z.trigger(t._element, Jt.HIDDEN);
        }), m(this._element, s);
      }
    }, e.setTransitioning = function (t) {
      this._isTransitioning = t;
    }, e.dispose = function () {
      A.removeData(this._element, qt), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null;
    }, e._getConfig = function (t) {
      return (t = r({}, Zt, {}, t)).toggle = Boolean(t.toggle), v(Xt, t, $t), t;
    }, e._getDimension = function () {
      return this._element.classList.contains(oe) ? oe : re;
    }, e._getParent = function () {
      var t = this,
          e = this._config.parent;
      _(e) ? "undefined" == typeof e.jquery && "undefined" == typeof e[0] || (e = e[0]) : e = $.findOne(e);
      var n = '[data-toggle="collapse"][data-parent="' + e + '"]';
      return E($.find(n, e)).forEach(function (e) {
        var n = d(e);

        t._addAriaAndCollapsedClass(n, [e]);
      }), e;
    }, e._addAriaAndCollapsedClass = function (t, e) {
      if (t) {
        var n = t.classList.contains(te);
        e.length && e.forEach(function (t) {
          n ? t.classList.remove(ie) : t.classList.add(ie), t.setAttribute("aria-expanded", n);
        });
      }
    }, t.collapseInterface = function (e, n) {
      var i = A.getData(e, qt),
          o = r({}, Zt, {}, It.getDataAttributes(e), {}, "object" == _typeof(n) && n ? n : {});

      if (!i && o.toggle && /show|hide/.test(n) && (o.toggle = !1), i || (i = new t(e, o)), "string" == typeof n) {
        if ("undefined" == typeof i[n]) throw new TypeError('No method named "' + n + '"');
        i[n]();
      }
    }, t.jQueryInterface = function (e) {
      return this.each(function () {
        t.collapseInterface(this, e);
      });
    }, t.getInstance = function (t) {
      return A.getData(t, qt);
    }, n(t, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return Zt;
      }
    }]), t;
  }();

  Z.on(document, Jt.CLICK_DATA_API, se.DATA_TOGGLE, function (t) {
    "A" === t.target.tagName && t.preventDefault();
    var e = It.getDataAttributes(this),
        n = h(this);
    E($.find(n)).forEach(function (t) {
      var n,
          i = A.getData(t, qt);
      i ? (null === i._parent && "string" == typeof e.parent && (i._config.parent = e.parent, i._parent = i._getParent()), n = "toggle") : n = e, ae.collapseInterface(t, n);
    });
  });
  var le = T();

  if (le) {
    var ce = le.fn[Xt];
    le.fn[Xt] = ae.jQueryInterface, le.fn[Xt].Constructor = ae, le.fn[Xt].noConflict = function () {
      return le.fn[Xt] = ce, ae.jQueryInterface;
    };
  }

  var ue = "dropdown",
      fe = "bs.dropdown",
      he = "." + fe,
      de = new RegExp("38|40|27"),
      ge = {
    HIDE: "hide" + he,
    HIDDEN: "hidden" + he,
    SHOW: "show" + he,
    SHOWN: "shown" + he,
    CLICK: "click" + he,
    CLICK_DATA_API: "click" + he + ".data-api",
    KEYDOWN_DATA_API: "keydown" + he + ".data-api",
    KEYUP_DATA_API: "keyup" + he + ".data-api"
  },
      pe = "disabled",
      _e = "show",
      me = "dropup",
      ve = "dropright",
      Ee = "dropleft",
      ye = "dropdown-menu-right",
      be = "position-static",
      De = '[data-toggle="dropdown"]',
      Te = ".dropdown form",
      Ie = ".dropdown-menu",
      Ae = ".navbar-nav",
      Se = ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)",
      we = "top-start",
      Ce = "top-end",
      Le = "bottom-start",
      Oe = "bottom-end",
      Ne = "right-start",
      ke = "left-start",
      Pe = {
    offset: 0,
    flip: !0,
    boundary: "scrollParent",
    reference: "toggle",
    display: "dynamic",
    popperConfig: null
  },
      He = {
    offset: "(number|string|function)",
    flip: "boolean",
    boundary: "(string|element)",
    reference: "(string|element)",
    display: "string",
    popperConfig: "(null|object)"
  },
      je = function () {
    function e(t, e) {
      this._element = t, this._popper = null, this._config = this._getConfig(e), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners(), A.setData(t, fe, this);
    }

    var i = e.prototype;
    return i.toggle = function () {
      if (!this._element.disabled && !this._element.classList.contains(pe)) {
        var t = this._menu.classList.contains(_e);

        e.clearMenus(), t || this.show();
      }
    }, i.show = function () {
      if (!(this._element.disabled || this._element.classList.contains(pe) || this._menu.classList.contains(_e))) {
        var n = e.getParentFromElement(this._element),
            i = {
          relatedTarget: this._element
        };

        if (!Z.trigger(n, ge.SHOW, i).defaultPrevented) {
          if (!this._inNavbar) {
            if ("undefined" == typeof t) throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org)");
            var o = this._element;
            "parent" === this._config.reference ? o = n : _(this._config.reference) && (o = this._config.reference, "undefined" != typeof this._config.reference.jquery && (o = this._config.reference[0])), "scrollParent" !== this._config.boundary && n.classList.add(be), this._popper = new t(o, this._menu, this._getPopperConfig());
          }

          "ontouchstart" in document.documentElement && !E($.closest(n, Ae)).length && E(document.body.children).forEach(function (t) {
            return Z.on(t, "mouseover", null, function () {});
          }), this._element.focus(), this._element.setAttribute("aria-expanded", !0), It.toggleClass(this._menu, _e), It.toggleClass(n, _e), Z.trigger(n, ge.SHOWN, i);
        }
      }
    }, i.hide = function () {
      if (!this._element.disabled && !this._element.classList.contains(pe) && this._menu.classList.contains(_e)) {
        var t = e.getParentFromElement(this._element),
            n = {
          relatedTarget: this._element
        };
        Z.trigger(t, ge.HIDE, n).defaultPrevented || (this._popper && this._popper.destroy(), It.toggleClass(this._menu, _e), It.toggleClass(t, _e), Z.trigger(t, ge.HIDDEN, n));
      }
    }, i.dispose = function () {
      A.removeData(this._element, fe), Z.off(this._element, he), this._element = null, this._menu = null, this._popper && (this._popper.destroy(), this._popper = null);
    }, i.update = function () {
      this._inNavbar = this._detectNavbar(), this._popper && this._popper.scheduleUpdate();
    }, i._addEventListeners = function () {
      var t = this;
      Z.on(this._element, ge.CLICK, function (e) {
        e.preventDefault(), e.stopPropagation(), t.toggle();
      });
    }, i._getConfig = function (t) {
      return t = r({}, this.constructor.Default, {}, It.getDataAttributes(this._element), {}, t), v(ue, t, this.constructor.DefaultType), t;
    }, i._getMenuElement = function () {
      var t = e.getParentFromElement(this._element);
      return $.findOne(Ie, t);
    }, i._getPlacement = function () {
      var t = this._element.parentNode,
          e = Le;
      return t.classList.contains(me) ? (e = we, this._menu.classList.contains(ye) && (e = Ce)) : t.classList.contains(ve) ? e = Ne : t.classList.contains(Ee) ? e = ke : this._menu.classList.contains(ye) && (e = Oe), e;
    }, i._detectNavbar = function () {
      return Boolean($.closest(this._element, ".navbar"));
    }, i._getOffset = function () {
      var t = this,
          e = {};
      return "function" == typeof this._config.offset ? e.fn = function (e) {
        return e.offsets = r({}, e.offsets, {}, t._config.offset(e.offsets, t._element) || {}), e;
      } : e.offset = this._config.offset, e;
    }, i._getPopperConfig = function () {
      var t = {
        placement: this._getPlacement(),
        modifiers: {
          offset: this._getOffset(),
          flip: {
            enabled: this._config.flip
          },
          preventOverflow: {
            boundariesElement: this._config.boundary
          }
        }
      };
      return "static" === this._config.display && (t.modifiers.applyStyle = {
        enabled: !1
      }), r({}, t, {}, this._config.popperConfig);
    }, e.dropdownInterface = function (t, n) {
      var i = A.getData(t, fe);

      if (i || (i = new e(t, "object" == _typeof(n) ? n : null)), "string" == typeof n) {
        if ("undefined" == typeof i[n]) throw new TypeError('No method named "' + n + '"');
        i[n]();
      }
    }, e.jQueryInterface = function (t) {
      return this.each(function () {
        e.dropdownInterface(this, t);
      });
    }, e.clearMenus = function (t) {
      if (!t || 3 !== t.which && ("keyup" !== t.type || 9 === t.which)) for (var n = E($.find(De)), i = 0, o = n.length; i < o; i++) {
        var r = e.getParentFromElement(n[i]),
            s = A.getData(n[i], fe),
            a = {
          relatedTarget: n[i]
        };

        if (t && "click" === t.type && (a.clickEvent = t), s) {
          var l = s._menu;
          if (r.classList.contains(_e)) if (!(t && ("click" === t.type && /input|textarea/i.test(t.target.tagName) || "keyup" === t.type && 9 === t.which) && r.contains(t.target))) Z.trigger(r, ge.HIDE, a).defaultPrevented || ("ontouchstart" in document.documentElement && E(document.body.children).forEach(function (t) {
            return Z.off(t, "mouseover", null, function () {});
          }), n[i].setAttribute("aria-expanded", "false"), s._popper && s._popper.destroy(), l.classList.remove(_e), r.classList.remove(_e), Z.trigger(r, ge.HIDDEN, a));
        }
      }
    }, e.getParentFromElement = function (t) {
      return d(t) || t.parentNode;
    }, e.dataApiKeydownHandler = function (t) {
      if ((/input|textarea/i.test(t.target.tagName) ? !(32 === t.which || 27 !== t.which && (40 !== t.which && 38 !== t.which || $.closest(t.target, Ie))) : de.test(t.which)) && (t.preventDefault(), t.stopPropagation(), !this.disabled && !this.classList.contains(pe))) {
        var n = e.getParentFromElement(this),
            i = n.classList.contains(_e);
        if (!i || i && (27 === t.which || 32 === t.which)) return 27 === t.which && $.findOne(De, n).focus(), void e.clearMenus();
        var o = E($.find(Se, n)).filter(y);

        if (o.length) {
          var r = o.indexOf(t.target);
          38 === t.which && r > 0 && r--, 40 === t.which && r < o.length - 1 && r++, r < 0 && (r = 0), o[r].focus();
        }
      }
    }, e.getInstance = function (t) {
      return A.getData(t, fe);
    }, n(e, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return Pe;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return He;
      }
    }]), e;
  }();

  Z.on(document, ge.KEYDOWN_DATA_API, De, je.dataApiKeydownHandler), Z.on(document, ge.KEYDOWN_DATA_API, Ie, je.dataApiKeydownHandler), Z.on(document, ge.CLICK_DATA_API, je.clearMenus), Z.on(document, ge.KEYUP_DATA_API, je.clearMenus), Z.on(document, ge.CLICK_DATA_API, De, function (t) {
    t.preventDefault(), t.stopPropagation(), je.dropdownInterface(this, "toggle");
  }), Z.on(document, ge.CLICK_DATA_API, Te, function (t) {
    return t.stopPropagation();
  });
  var Me = T();

  if (Me) {
    var Re = Me.fn[ue];
    Me.fn[ue] = je.jQueryInterface, Me.fn[ue].Constructor = je, Me.fn[ue].noConflict = function () {
      return Me.fn[ue] = Re, je.jQueryInterface;
    };
  }

  var xe = "bs.modal",
      We = "." + xe,
      Ue = {
    backdrop: !0,
    keyboard: !0,
    focus: !0,
    show: !0
  },
      Ke = {
    backdrop: "(boolean|string)",
    keyboard: "boolean",
    focus: "boolean",
    show: "boolean"
  },
      Ve = {
    HIDE: "hide" + We,
    HIDE_PREVENTED: "hidePrevented" + We,
    HIDDEN: "hidden" + We,
    SHOW: "show" + We,
    SHOWN: "shown" + We,
    FOCUSIN: "focusin" + We,
    RESIZE: "resize" + We,
    CLICK_DISMISS: "click.dismiss" + We,
    KEYDOWN_DISMISS: "keydown.dismiss" + We,
    MOUSEUP_DISMISS: "mouseup.dismiss" + We,
    MOUSEDOWN_DISMISS: "mousedown.dismiss" + We,
    CLICK_DATA_API: "click" + We + ".data-api"
  },
      Be = "modal-dialog-scrollable",
      Fe = "modal-scrollbar-measure",
      Qe = "modal-backdrop",
      Ye = "modal-open",
      Ge = "fade",
      Xe = "show",
      qe = "modal-static",
      ze = {
    DIALOG: ".modal-dialog",
    MODAL_BODY: ".modal-body",
    DATA_TOGGLE: '[data-toggle="modal"]',
    DATA_DISMISS: '[data-dismiss="modal"]',
    FIXED_CONTENT: ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
    STICKY_CONTENT: ".sticky-top"
  },
      Ze = function () {
    function t(t, e) {
      this._config = this._getConfig(e), this._element = t, this._dialog = $.findOne(ze.DIALOG, t), this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._isTransitioning = !1, this._scrollbarWidth = 0, A.setData(t, xe, this);
    }

    var e = t.prototype;
    return e.toggle = function (t) {
      return this._isShown ? this.hide() : this.show(t);
    }, e.show = function (t) {
      var e = this;

      if (!this._isShown && !this._isTransitioning) {
        this._element.classList.contains(Ge) && (this._isTransitioning = !0);
        var n = Z.trigger(this._element, Ve.SHOW, {
          relatedTarget: t
        });
        this._isShown || n.defaultPrevented || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), this._setEscapeEvent(), this._setResizeEvent(), Z.on(this._element, Ve.CLICK_DISMISS, ze.DATA_DISMISS, function (t) {
          return e.hide(t);
        }), Z.on(this._dialog, Ve.MOUSEDOWN_DISMISS, function () {
          Z.one(e._element, Ve.MOUSEUP_DISMISS, function (t) {
            t.target === e._element && (e._ignoreBackdropClick = !0);
          });
        }), this._showBackdrop(function () {
          return e._showElement(t);
        }));
      }
    }, e.hide = function (t) {
      var e = this;

      if ((t && t.preventDefault(), this._isShown && !this._isTransitioning) && !Z.trigger(this._element, Ve.HIDE).defaultPrevented) {
        this._isShown = !1;

        var n = this._element.classList.contains(Ge);

        if (n && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), Z.off(document, Ve.FOCUSIN), this._element.classList.remove(Xe), Z.off(this._element, Ve.CLICK_DISMISS), Z.off(this._dialog, Ve.MOUSEDOWN_DISMISS), n) {
          var i = g(this._element);
          Z.one(this._element, c, function (t) {
            return e._hideModal(t);
          }), m(this._element, i);
        } else this._hideModal();
      }
    }, e.dispose = function () {
      [window, this._element, this._dialog].forEach(function (t) {
        return Z.off(t, We);
      }), Z.off(document, Ve.FOCUSIN), A.removeData(this._element, xe), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._isTransitioning = null, this._scrollbarWidth = null;
    }, e.handleUpdate = function () {
      this._adjustDialog();
    }, e._getConfig = function (t) {
      return t = r({}, Ue, {}, t), v("modal", t, Ke), t;
    }, e._showElement = function (t) {
      var e = this,
          n = this._element.classList.contains(Ge),
          i = $.findOne(ze.MODAL_BODY, this._dialog);

      this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.setAttribute("aria-modal", !0), this._dialog.classList.contains(Be) && i ? i.scrollTop = 0 : this._element.scrollTop = 0, n && D(this._element), this._element.classList.add(Xe), this._config.focus && this._enforceFocus();

      var o = function o() {
        e._config.focus && e._element.focus(), e._isTransitioning = !1, Z.trigger(e._element, Ve.SHOWN, {
          relatedTarget: t
        });
      };

      if (n) {
        var r = g(this._dialog);
        Z.one(this._dialog, c, o), m(this._dialog, r);
      } else o();
    }, e._enforceFocus = function () {
      var t = this;
      Z.off(document, Ve.FOCUSIN), Z.on(document, Ve.FOCUSIN, function (e) {
        document === e.target || t._element === e.target || t._element.contains(e.target) || t._element.focus();
      });
    }, e._setEscapeEvent = function () {
      var t = this;
      this._isShown && this._config.keyboard ? Z.on(this._element, Ve.KEYDOWN_DISMISS, function (e) {
        27 === e.which && t._triggerBackdropTransition();
      }) : Z.off(this._element, Ve.KEYDOWN_DISMISS);
    }, e._setResizeEvent = function () {
      var t = this;
      this._isShown ? Z.on(window, Ve.RESIZE, function () {
        return t._adjustDialog();
      }) : Z.off(window, Ve.RESIZE);
    }, e._hideModal = function () {
      var t = this;
      this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._element.removeAttribute("aria-modal"), this._isTransitioning = !1, this._showBackdrop(function () {
        document.body.classList.remove(Ye), t._resetAdjustments(), t._resetScrollbar(), Z.trigger(t._element, Ve.HIDDEN);
      });
    }, e._removeBackdrop = function () {
      this._backdrop.parentNode.removeChild(this._backdrop), this._backdrop = null;
    }, e._showBackdrop = function (t) {
      var e = this,
          n = this._element.classList.contains(Ge) ? Ge : "";

      if (this._isShown && this._config.backdrop) {
        if (this._backdrop = document.createElement("div"), this._backdrop.className = Qe, n && this._backdrop.classList.add(n), document.body.appendChild(this._backdrop), Z.on(this._element, Ve.CLICK_DISMISS, function (t) {
          e._ignoreBackdropClick ? e._ignoreBackdropClick = !1 : t.target === t.currentTarget && e._triggerBackdropTransition();
        }), n && D(this._backdrop), this._backdrop.classList.add(Xe), !n) return void t();
        var i = g(this._backdrop);
        Z.one(this._backdrop, c, t), m(this._backdrop, i);
      } else if (!this._isShown && this._backdrop) {
        this._backdrop.classList.remove(Xe);

        var o = function o() {
          e._removeBackdrop(), t();
        };

        if (this._element.classList.contains(Ge)) {
          var r = g(this._backdrop);
          Z.one(this._backdrop, c, o), m(this._backdrop, r);
        } else o();
      } else t();
    }, e._triggerBackdropTransition = function () {
      var t = this;

      if ("static" === this._config.backdrop) {
        if (Z.trigger(this._element, Ve.HIDE_PREVENTED).defaultPrevented) return;

        this._element.classList.add(qe);

        var e = g(this._element);
        Z.one(this._element, c, function () {
          t._element.classList.remove(qe);
        }), m(this._element, e), this._element.focus();
      } else this.hide();
    }, e._adjustDialog = function () {
      var t = this._element.scrollHeight > document.documentElement.clientHeight;
      !this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px");
    }, e._resetAdjustments = function () {
      this._element.style.paddingLeft = "", this._element.style.paddingRight = "";
    }, e._checkScrollbar = function () {
      var t = document.body.getBoundingClientRect();
      this._isBodyOverflowing = t.left + t.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth();
    }, e._setScrollbar = function () {
      var t = this;

      if (this._isBodyOverflowing) {
        E($.find(ze.FIXED_CONTENT)).forEach(function (e) {
          var n = e.style.paddingRight,
              i = window.getComputedStyle(e)["padding-right"];
          It.setDataAttribute(e, "padding-right", n), e.style.paddingRight = parseFloat(i) + t._scrollbarWidth + "px";
        }), E($.find(ze.STICKY_CONTENT)).forEach(function (e) {
          var n = e.style.marginRight,
              i = window.getComputedStyle(e)["margin-right"];
          It.setDataAttribute(e, "margin-right", n), e.style.marginRight = parseFloat(i) - t._scrollbarWidth + "px";
        });
        var e = document.body.style.paddingRight,
            n = window.getComputedStyle(document.body)["padding-right"];
        It.setDataAttribute(document.body, "padding-right", e), document.body.style.paddingRight = parseFloat(n) + this._scrollbarWidth + "px";
      }

      document.body.classList.add(Ye);
    }, e._resetScrollbar = function () {
      E($.find(ze.FIXED_CONTENT)).forEach(function (t) {
        var e = It.getDataAttribute(t, "padding-right");
        "undefined" != typeof e && (It.removeDataAttribute(t, "padding-right"), t.style.paddingRight = e);
      }), E($.find("" + ze.STICKY_CONTENT)).forEach(function (t) {
        var e = It.getDataAttribute(t, "margin-right");
        "undefined" != typeof e && (It.removeDataAttribute(t, "margin-right"), t.style.marginRight = e);
      });
      var t = It.getDataAttribute(document.body, "padding-right");
      "undefined" == typeof t ? document.body.style.paddingRight = "" : (It.removeDataAttribute(document.body, "padding-right"), document.body.style.paddingRight = t);
    }, e._getScrollbarWidth = function () {
      var t = document.createElement("div");
      t.className = Fe, document.body.appendChild(t);
      var e = t.getBoundingClientRect().width - t.clientWidth;
      return document.body.removeChild(t), e;
    }, t.jQueryInterface = function (e, n) {
      return this.each(function () {
        var i = A.getData(this, xe),
            o = r({}, Ue, {}, It.getDataAttributes(this), {}, "object" == _typeof(e) && e ? e : {});

        if (i || (i = new t(this, o)), "string" == typeof e) {
          if ("undefined" == typeof i[e]) throw new TypeError('No method named "' + e + '"');
          i[e](n);
        } else o.show && i.show(n);
      });
    }, t.getInstance = function (t) {
      return A.getData(t, xe);
    }, n(t, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return Ue;
      }
    }]), t;
  }();

  Z.on(document, Ve.CLICK_DATA_API, ze.DATA_TOGGLE, function (t) {
    var e = this,
        n = d(this);
    "A" !== this.tagName && "AREA" !== this.tagName || t.preventDefault(), Z.one(n, Ve.SHOW, function (t) {
      t.defaultPrevented || Z.one(n, Ve.HIDDEN, function () {
        y(e) && e.focus();
      });
    });
    var i = A.getData(n, xe);

    if (!i) {
      var o = r({}, It.getDataAttributes(n), {}, It.getDataAttributes(this));
      i = new Ze(n, o);
    }

    i.show(this);
  });
  var $e = T();

  if ($e) {
    var Je = $e.fn.modal;
    $e.fn.modal = Ze.jQueryInterface, $e.fn.modal.Constructor = Ze, $e.fn.modal.noConflict = function () {
      return $e.fn.modal = Je, Ze.jQueryInterface;
    };
  }

  var tn = ["background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href"],
      en = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,
      nn = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i,
      on = function on(t, e) {
    var n = t.nodeName.toLowerCase();
    if (-1 !== e.indexOf(n)) return -1 === tn.indexOf(n) || Boolean(t.nodeValue.match(en) || t.nodeValue.match(nn));

    for (var i = e.filter(function (t) {
      return t instanceof RegExp;
    }), o = 0, r = i.length; o < r; o++) {
      if (n.match(i[o])) return !0;
    }

    return !1;
  },
      rn = {
    "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i],
    a: ["target", "href", "title", "rel"],
    area: [],
    b: [],
    br: [],
    col: [],
    code: [],
    div: [],
    em: [],
    hr: [],
    h1: [],
    h2: [],
    h3: [],
    h4: [],
    h5: [],
    h6: [],
    i: [],
    img: ["src", "alt", "title", "width", "height"],
    li: [],
    ol: [],
    p: [],
    pre: [],
    s: [],
    small: [],
    span: [],
    sub: [],
    sup: [],
    strong: [],
    u: [],
    ul: []
  };

  function sn(t, e, n) {
    if (!t.length) return t;
    if (n && "function" == typeof n) return n(t);

    for (var i = new window.DOMParser().parseFromString(t, "text/html"), o = Object.keys(e), r = E(i.body.querySelectorAll("*")), s = function s(t, n) {
      var i = r[t],
          s = i.nodeName.toLowerCase();
      if (-1 === o.indexOf(s)) return i.parentNode.removeChild(i), "continue";
      var a = E(i.attributes),
          l = [].concat(e["*"] || [], e[s] || []);
      a.forEach(function (t) {
        on(t, l) || i.removeAttribute(t.nodeName);
      });
    }, a = 0, l = r.length; a < l; a++) {
      s(a);
    }

    return i.body.innerHTML;
  }

  var an = "tooltip",
      ln = new RegExp("(^|\\s)bs-tooltip\\S+", "g"),
      cn = ["sanitize", "whiteList", "sanitizeFn"],
      un = {
    animation: "boolean",
    template: "string",
    title: "(string|element|function)",
    trigger: "string",
    delay: "(number|object)",
    html: "boolean",
    selector: "(string|boolean)",
    placement: "(string|function)",
    offset: "(number|string|function)",
    container: "(string|element|boolean)",
    fallbackPlacement: "(string|array)",
    boundary: "(string|element)",
    sanitize: "boolean",
    sanitizeFn: "(null|function)",
    whiteList: "object",
    popperConfig: "(null|object)"
  },
      fn = {
    AUTO: "auto",
    TOP: "top",
    RIGHT: "right",
    BOTTOM: "bottom",
    LEFT: "left"
  },
      hn = {
    animation: !0,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: "hover focus",
    title: "",
    delay: 0,
    html: !1,
    selector: !1,
    placement: "top",
    offset: 0,
    container: !1,
    fallbackPlacement: "flip",
    boundary: "scrollParent",
    sanitize: !0,
    sanitizeFn: null,
    whiteList: rn,
    popperConfig: null
  },
      dn = "show",
      gn = "out",
      pn = {
    HIDE: "hide.bs.tooltip",
    HIDDEN: "hidden.bs.tooltip",
    SHOW: "show.bs.tooltip",
    SHOWN: "shown.bs.tooltip",
    INSERTED: "inserted.bs.tooltip",
    CLICK: "click.bs.tooltip",
    FOCUSIN: "focusin.bs.tooltip",
    FOCUSOUT: "focusout.bs.tooltip",
    MOUSEENTER: "mouseenter.bs.tooltip",
    MOUSELEAVE: "mouseleave.bs.tooltip"
  },
      _n = "fade",
      mn = "show",
      vn = ".tooltip-inner",
      En = "hover",
      yn = "focus",
      bn = "click",
      Dn = "manual",
      Tn = function () {
    function e(e, n) {
      if ("undefined" == typeof t) throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org)");
      this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = e, this.config = this._getConfig(n), this.tip = null, this._setListeners(), A.setData(e, this.constructor.DATA_KEY, this);
    }

    var i = e.prototype;
    return i.enable = function () {
      this._isEnabled = !0;
    }, i.disable = function () {
      this._isEnabled = !1;
    }, i.toggleEnabled = function () {
      this._isEnabled = !this._isEnabled;
    }, i.toggle = function (t) {
      if (this._isEnabled) if (t) {
        var e = this.constructor.DATA_KEY,
            n = A.getData(t.delegateTarget, e);
        n || (n = new this.constructor(t.delegateTarget, this._getDelegateConfig()), A.setData(t.delegateTarget, e, n)), n._activeTrigger.click = !n._activeTrigger.click, n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n);
      } else {
        if (this.getTipElement().classList.contains(mn)) return void this._leave(null, this);

        this._enter(null, this);
      }
    }, i.dispose = function () {
      clearTimeout(this._timeout), A.removeData(this.element, this.constructor.DATA_KEY), Z.off(this.element, this.constructor.EVENT_KEY), Z.off($.closest(this.element, ".modal"), "hide.bs.modal", this._hideModalHandler), this.tip && this.tip.parentNode.removeChild(this.tip), this._isEnabled = null, this._timeout = null, this._hoverState = null, this._activeTrigger = null, this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null;
    }, i.show = function () {
      var e = this;
      if ("none" === this.element.style.display) throw new Error("Please use show on visible elements");

      if (this.isWithContent() && this._isEnabled) {
        var n = Z.trigger(this.element, this.constructor.Event.SHOW),
            i = function t(e) {
          if (!document.documentElement.attachShadow) return null;

          if ("function" == typeof e.getRootNode) {
            var n = e.getRootNode();
            return n instanceof ShadowRoot ? n : null;
          }

          return e instanceof ShadowRoot ? e : e.parentNode ? t(e.parentNode) : null;
        }(this.element),
            o = null === i ? this.element.ownerDocument.documentElement.contains(this.element) : i.contains(this.element);

        if (n.defaultPrevented || !o) return;
        var r = this.getTipElement(),
            s = u(this.constructor.NAME);
        r.setAttribute("id", s), this.element.setAttribute("aria-describedby", s), this.setContent(), this.config.animation && r.classList.add(_n);

        var a = "function" == typeof this.config.placement ? this.config.placement.call(this, r, this.element) : this.config.placement,
            l = this._getAttachment(a);

        this._addAttachmentClass(l);

        var f = this._getContainer();

        A.setData(r, this.constructor.DATA_KEY, this), this.element.ownerDocument.documentElement.contains(this.tip) || f.appendChild(r), Z.trigger(this.element, this.constructor.Event.INSERTED), this._popper = new t(this.element, r, this._getPopperConfig(l)), r.classList.add(mn), "ontouchstart" in document.documentElement && E(document.body.children).forEach(function (t) {
          Z.on(t, "mouseover", function () {});
        });

        var h = function h() {
          e.config.animation && e._fixTransition();
          var t = e._hoverState;
          e._hoverState = null, Z.trigger(e.element, e.constructor.Event.SHOWN), t === gn && e._leave(null, e);
        };

        if (this.tip.classList.contains(_n)) {
          var d = g(this.tip);
          Z.one(this.tip, c, h), m(this.tip, d);
        } else h();
      }
    }, i.hide = function () {
      var t = this,
          e = this.getTipElement(),
          n = function n() {
        t._hoverState !== dn && e.parentNode && e.parentNode.removeChild(e), t._cleanTipClass(), t.element.removeAttribute("aria-describedby"), Z.trigger(t.element, t.constructor.Event.HIDDEN), t._popper.destroy();
      };

      if (!Z.trigger(this.element, this.constructor.Event.HIDE).defaultPrevented) {
        if (e.classList.remove(mn), "ontouchstart" in document.documentElement && E(document.body.children).forEach(function (t) {
          return Z.off(t, "mouseover", b);
        }), this._activeTrigger[bn] = !1, this._activeTrigger[yn] = !1, this._activeTrigger[En] = !1, this.tip.classList.contains(_n)) {
          var i = g(e);
          Z.one(e, c, n), m(e, i);
        } else n();

        this._hoverState = "";
      }
    }, i.update = function () {
      null !== this._popper && this._popper.scheduleUpdate();
    }, i.isWithContent = function () {
      return Boolean(this.getTitle());
    }, i.getTipElement = function () {
      if (this.tip) return this.tip;
      var t = document.createElement("div");
      return t.innerHTML = this.config.template, this.tip = t.children[0], this.tip;
    }, i.setContent = function () {
      var t = this.getTipElement();
      this.setElementContent($.findOne(vn, t), this.getTitle()), t.classList.remove(_n), t.classList.remove(mn);
    }, i.setElementContent = function (t, e) {
      if (null !== t) return "object" == _typeof(e) && _(e) ? (e.jquery && (e = e[0]), void (this.config.html ? e.parentNode !== t && (t.innerHTML = "", t.appendChild(e)) : t.innerText = e.textContent)) : void (this.config.html ? (this.config.sanitize && (e = sn(e, this.config.whiteList, this.config.sanitizeFn)), t.innerHTML = e) : t.innerText = e);
    }, i.getTitle = function () {
      var t = this.element.getAttribute("data-original-title");
      return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t;
    }, i._getPopperConfig = function (t) {
      var e = this;
      return r({}, {
        placement: t,
        modifiers: {
          offset: this._getOffset(),
          flip: {
            behavior: this.config.fallbackPlacement
          },
          arrow: {
            element: "." + this.constructor.NAME + "-arrow"
          },
          preventOverflow: {
            boundariesElement: this.config.boundary
          }
        },
        onCreate: function onCreate(t) {
          t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t);
        },
        onUpdate: function onUpdate(t) {
          return e._handlePopperPlacementChange(t);
        }
      }, {}, this.config.popperConfig);
    }, i._addAttachmentClass = function (t) {
      this.getTipElement().classList.add("bs-tooltip-" + t);
    }, i._getOffset = function () {
      var t = this,
          e = {};
      return "function" == typeof this.config.offset ? e.fn = function (e) {
        return e.offsets = r({}, e.offsets, {}, t.config.offset(e.offsets, t.element) || {}), e;
      } : e.offset = this.config.offset, e;
    }, i._getContainer = function () {
      return !1 === this.config.container ? document.body : _(this.config.container) ? this.config.container : $.findOne(this.config.container);
    }, i._getAttachment = function (t) {
      return fn[t.toUpperCase()];
    }, i._setListeners = function () {
      var t = this;
      this.config.trigger.split(" ").forEach(function (e) {
        if ("click" === e) Z.on(t.element, t.constructor.Event.CLICK, t.config.selector, function (e) {
          return t.toggle(e);
        });else if (e !== Dn) {
          var n = e === En ? t.constructor.Event.MOUSEENTER : t.constructor.Event.FOCUSIN,
              i = e === En ? t.constructor.Event.MOUSELEAVE : t.constructor.Event.FOCUSOUT;
          Z.on(t.element, n, t.config.selector, function (e) {
            return t._enter(e);
          }), Z.on(t.element, i, t.config.selector, function (e) {
            return t._leave(e);
          });
        }
      }), this._hideModalHandler = function () {
        t.element && t.hide();
      }, Z.on($.closest(this.element, ".modal"), "hide.bs.modal", this._hideModalHandler), this.config.selector ? this.config = r({}, this.config, {
        trigger: "manual",
        selector: ""
      }) : this._fixTitle();
    }, i._fixTitle = function () {
      var t = _typeof(this.element.getAttribute("data-original-title"));

      (this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""));
    }, i._enter = function (t, e) {
      var n = this.constructor.DATA_KEY;
      (e = e || A.getData(t.delegateTarget, n)) || (e = new this.constructor(t.delegateTarget, this._getDelegateConfig()), A.setData(t.delegateTarget, n, e)), t && (e._activeTrigger["focusin" === t.type ? yn : En] = !0), e.getTipElement().classList.contains(mn) || e._hoverState === dn ? e._hoverState = dn : (clearTimeout(e._timeout), e._hoverState = dn, e.config.delay && e.config.delay.show ? e._timeout = setTimeout(function () {
        e._hoverState === dn && e.show();
      }, e.config.delay.show) : e.show());
    }, i._leave = function (t, e) {
      var n = this.constructor.DATA_KEY;
      (e = e || A.getData(t.delegateTarget, n)) || (e = new this.constructor(t.delegateTarget, this._getDelegateConfig()), A.setData(t.delegateTarget, n, e)), t && (e._activeTrigger["focusout" === t.type ? yn : En] = !1), e._isWithActiveTrigger() || (clearTimeout(e._timeout), e._hoverState = gn, e.config.delay && e.config.delay.hide ? e._timeout = setTimeout(function () {
        e._hoverState === gn && e.hide();
      }, e.config.delay.hide) : e.hide());
    }, i._isWithActiveTrigger = function () {
      for (var t in this._activeTrigger) {
        if (this._activeTrigger[t]) return !0;
      }

      return !1;
    }, i._getConfig = function (t) {
      var e = It.getDataAttributes(this.element);
      return Object.keys(e).forEach(function (t) {
        -1 !== cn.indexOf(t) && delete e[t];
      }), t && "object" == _typeof(t.container) && t.container.jquery && (t.container = t.container[0]), "number" == typeof (t = r({}, this.constructor.Default, {}, e, {}, "object" == _typeof(t) && t ? t : {})).delay && (t.delay = {
        show: t.delay,
        hide: t.delay
      }), "number" == typeof t.title && (t.title = t.title.toString()), "number" == typeof t.content && (t.content = t.content.toString()), v(an, t, this.constructor.DefaultType), t.sanitize && (t.template = sn(t.template, t.whiteList, t.sanitizeFn)), t;
    }, i._getDelegateConfig = function () {
      var t = {};
      if (this.config) for (var e in this.config) {
        this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
      }
      return t;
    }, i._cleanTipClass = function () {
      var t = this.getTipElement(),
          e = t.getAttribute("class").match(ln);
      null !== e && e.length && e.map(function (t) {
        return t.trim();
      }).forEach(function (e) {
        return t.classList.remove(e);
      });
    }, i._handlePopperPlacementChange = function (t) {
      var e = t.instance;
      this.tip = e.popper, this._cleanTipClass(), this._addAttachmentClass(this._getAttachment(t.placement));
    }, i._fixTransition = function () {
      var t = this.getTipElement(),
          e = this.config.animation;
      null === t.getAttribute("x-placement") && (t.classList.remove(_n), this.config.animation = !1, this.hide(), this.show(), this.config.animation = e);
    }, e.jQueryInterface = function (t) {
      return this.each(function () {
        var n = A.getData(this, "bs.tooltip"),
            i = "object" == _typeof(t) && t;

        if ((n || !/dispose|hide/.test(t)) && (n || (n = new e(this, i)), "string" == typeof t)) {
          if ("undefined" == typeof n[t]) throw new TypeError('No method named "' + t + '"');
          n[t]();
        }
      });
    }, e.getInstance = function (t) {
      return A.getData(t, "bs.tooltip");
    }, n(e, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return hn;
      }
    }, {
      key: "NAME",
      get: function get() {
        return an;
      }
    }, {
      key: "DATA_KEY",
      get: function get() {
        return "bs.tooltip";
      }
    }, {
      key: "Event",
      get: function get() {
        return pn;
      }
    }, {
      key: "EVENT_KEY",
      get: function get() {
        return ".bs.tooltip";
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return un;
      }
    }]), e;
  }(),
      In = T();

  if (In) {
    var An = In.fn.tooltip;
    In.fn.tooltip = Tn.jQueryInterface, In.fn.tooltip.Constructor = Tn, In.fn.tooltip.noConflict = function () {
      return In.fn.tooltip = An, Tn.jQueryInterface;
    };
  }

  var Sn = "popover",
      wn = new RegExp("(^|\\s)bs-popover\\S+", "g"),
      Cn = r({}, Tn.Default, {
    placement: "right",
    trigger: "click",
    content: "",
    template: '<div class="popover" role="tooltip"><div class="popover-arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
  }),
      Ln = r({}, Tn.DefaultType, {
    content: "(string|element|function)"
  }),
      On = "fade",
      Nn = "show",
      kn = ".popover-header",
      Pn = ".popover-body",
      Hn = {
    HIDE: "hide.bs.popover",
    HIDDEN: "hidden.bs.popover",
    SHOW: "show.bs.popover",
    SHOWN: "shown.bs.popover",
    INSERTED: "inserted.bs.popover",
    CLICK: "click.bs.popover",
    FOCUSIN: "focusin.bs.popover",
    FOCUSOUT: "focusout.bs.popover",
    MOUSEENTER: "mouseenter.bs.popover",
    MOUSELEAVE: "mouseleave.bs.popover"
  },
      jn = function (t) {
    var e, i;

    function o() {
      return t.apply(this, arguments) || this;
    }

    i = t, (e = o).prototype = Object.create(i.prototype), e.prototype.constructor = e, e.__proto__ = i;
    var r = o.prototype;
    return r.isWithContent = function () {
      return this.getTitle() || this._getContent();
    }, r.setContent = function () {
      var t = this.getTipElement();
      this.setElementContent($.findOne(kn, t), this.getTitle());

      var e = this._getContent();

      "function" == typeof e && (e = e.call(this.element)), this.setElementContent($.findOne(Pn, t), e), t.classList.remove(On), t.classList.remove(Nn);
    }, r._addAttachmentClass = function (t) {
      this.getTipElement().classList.add("bs-popover-" + t);
    }, r._getContent = function () {
      return this.element.getAttribute("data-content") || this.config.content;
    }, r._cleanTipClass = function () {
      var t = this.getTipElement(),
          e = t.getAttribute("class").match(wn);
      null !== e && e.length > 0 && e.map(function (t) {
        return t.trim();
      }).forEach(function (e) {
        return t.classList.remove(e);
      });
    }, o.jQueryInterface = function (t) {
      return this.each(function () {
        var e = A.getData(this, "bs.popover"),
            n = "object" == _typeof(t) ? t : null;

        if ((e || !/dispose|hide/.test(t)) && (e || (e = new o(this, n), A.setData(this, "bs.popover", e)), "string" == typeof t)) {
          if ("undefined" == typeof e[t]) throw new TypeError('No method named "' + t + '"');
          e[t]();
        }
      });
    }, o.getInstance = function (t) {
      return A.getData(t, "bs.popover");
    }, n(o, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return Cn;
      }
    }, {
      key: "NAME",
      get: function get() {
        return Sn;
      }
    }, {
      key: "DATA_KEY",
      get: function get() {
        return "bs.popover";
      }
    }, {
      key: "Event",
      get: function get() {
        return Hn;
      }
    }, {
      key: "EVENT_KEY",
      get: function get() {
        return ".bs.popover";
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return Ln;
      }
    }]), o;
  }(Tn),
      Mn = T();

  if (Mn) {
    var Rn = Mn.fn.popover;
    Mn.fn.popover = jn.jQueryInterface, Mn.fn.popover.Constructor = jn, Mn.fn.popover.noConflict = function () {
      return Mn.fn.popover = Rn, jn.jQueryInterface;
    };
  }

  var xn = "scrollspy",
      Wn = "bs.scrollspy",
      Un = "." + Wn,
      Kn = {
    offset: 10,
    method: "auto",
    target: ""
  },
      Vn = {
    offset: "number",
    method: "string",
    target: "(string|element)"
  },
      Bn = {
    ACTIVATE: "activate" + Un,
    SCROLL: "scroll" + Un,
    LOAD_DATA_API: "load" + Un + ".data-api"
  },
      Fn = {
    DROPDOWN_ITEM: "dropdown-item",
    ACTIVE: "active"
  },
      Qn = {
    DATA_SPY: '[data-spy="scroll"]',
    NAV_LIST_GROUP: ".nav, .list-group",
    NAV_LINKS: ".nav-link",
    NAV_ITEMS: ".nav-item",
    LIST_ITEMS: ".list-group-item",
    DROPDOWN: ".dropdown",
    DROPDOWN_TOGGLE: ".dropdown-toggle"
  },
      Yn = "offset",
      Gn = "position",
      Xn = function () {
    function t(t, e) {
      var n = this;
      this._element = t, this._scrollElement = "BODY" === t.tagName ? window : t, this._config = this._getConfig(e), this._selector = this._config.target + " " + Qn.NAV_LINKS + "," + this._config.target + " " + Qn.LIST_ITEMS + "," + this._config.target + " ." + Fn.DROPDOWN_ITEM, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, Z.on(this._scrollElement, Bn.SCROLL, function (t) {
        return n._process(t);
      }), this.refresh(), this._process(), A.setData(t, Wn, this);
    }

    var e = t.prototype;
    return e.refresh = function () {
      var t = this,
          e = this._scrollElement === this._scrollElement.window ? Yn : Gn,
          n = "auto" === this._config.method ? e : this._config.method,
          i = n === Gn ? this._getScrollTop() : 0;
      this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), E($.find(this._selector)).map(function (t) {
        var e,
            o = h(t);

        if (o && (e = $.findOne(o)), e) {
          var r = e.getBoundingClientRect();
          if (r.width || r.height) return [It[n](e).top + i, o];
        }

        return null;
      }).filter(function (t) {
        return t;
      }).sort(function (t, e) {
        return t[0] - e[0];
      }).forEach(function (e) {
        t._offsets.push(e[0]), t._targets.push(e[1]);
      });
    }, e.dispose = function () {
      A.removeData(this._element, Wn), Z.off(this._scrollElement, Un), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null;
    }, e._getConfig = function (t) {
      if ("string" != typeof (t = r({}, Kn, {}, "object" == _typeof(t) && t ? t : {})).target) {
        var e = t.target.id;
        e || (e = u(xn), t.target.id = e), t.target = "#" + e;
      }

      return v(xn, t, Vn), t;
    }, e._getScrollTop = function () {
      return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
    }, e._getScrollHeight = function () {
      return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
    }, e._getOffsetHeight = function () {
      return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
    }, e._process = function () {
      var t = this._getScrollTop() + this._config.offset,
          e = this._getScrollHeight(),
          n = this._config.offset + e - this._getOffsetHeight();

      if (this._scrollHeight !== e && this.refresh(), t >= n) {
        var i = this._targets[this._targets.length - 1];
        this._activeTarget !== i && this._activate(i);
      } else {
        if (this._activeTarget && t < this._offsets[0] && this._offsets[0] > 0) return this._activeTarget = null, void this._clear();

        for (var o = this._offsets.length; o--;) {
          this._activeTarget !== this._targets[o] && t >= this._offsets[o] && ("undefined" == typeof this._offsets[o + 1] || t < this._offsets[o + 1]) && this._activate(this._targets[o]);
        }
      }
    }, e._activate = function (t) {
      this._activeTarget = t, this._clear();

      var e = this._selector.split(",").map(function (e) {
        return e + '[data-target="' + t + '"],' + e + '[href="' + t + '"]';
      }),
          n = $.findOne(e.join(","));

      n.classList.contains(Fn.DROPDOWN_ITEM) ? ($.findOne(Qn.DROPDOWN_TOGGLE, $.closest(n, Qn.DROPDOWN)).classList.add(Fn.ACTIVE), n.classList.add(Fn.ACTIVE)) : (n.classList.add(Fn.ACTIVE), $.parents(n, Qn.NAV_LIST_GROUP).forEach(function (t) {
        $.prev(t, Qn.NAV_LINKS + ", " + Qn.LIST_ITEMS).forEach(function (t) {
          return t.classList.add(Fn.ACTIVE);
        }), $.prev(t, Qn.NAV_ITEMS).forEach(function (t) {
          $.children(t, Qn.NAV_LINKS).forEach(function (t) {
            return t.classList.add(Fn.ACTIVE);
          });
        });
      })), Z.trigger(this._scrollElement, Bn.ACTIVATE, {
        relatedTarget: t
      });
    }, e._clear = function () {
      E($.find(this._selector)).filter(function (t) {
        return t.classList.contains(Fn.ACTIVE);
      }).forEach(function (t) {
        return t.classList.remove(Fn.ACTIVE);
      });
    }, t.jQueryInterface = function (e) {
      return this.each(function () {
        var n = A.getData(this, Wn);

        if (n || (n = new t(this, "object" == _typeof(e) && e)), "string" == typeof e) {
          if ("undefined" == typeof n[e]) throw new TypeError('No method named "' + e + '"');
          n[e]();
        }
      });
    }, t.getInstance = function (t) {
      return A.getData(t, Wn);
    }, n(t, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return Kn;
      }
    }]), t;
  }();

  Z.on(window, Bn.LOAD_DATA_API, function () {
    E($.find(Qn.DATA_SPY)).forEach(function (t) {
      return new Xn(t, It.getDataAttributes(t));
    });
  });
  var qn = T();

  if (qn) {
    var zn = qn.fn[xn];
    qn.fn[xn] = Xn.jQueryInterface, qn.fn[xn].Constructor = Xn, qn.fn[xn].noConflict = function () {
      return qn.fn[xn] = zn, Xn.jQueryInterface;
    };
  }

  var Zn = "bs.tab",
      $n = "." + Zn,
      Jn = {
    HIDE: "hide" + $n,
    HIDDEN: "hidden" + $n,
    SHOW: "show" + $n,
    SHOWN: "shown" + $n,
    CLICK_DATA_API: "click" + $n + ".data-api"
  },
      ti = "dropdown-menu",
      ei = "active",
      ni = "disabled",
      ii = "fade",
      oi = "show",
      ri = ".dropdown",
      si = ".nav, .list-group",
      ai = ".active",
      li = ":scope > li > .active",
      ci = '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
      ui = ".dropdown-toggle",
      fi = ":scope > .dropdown-menu .active",
      hi = function () {
    function t(t) {
      this._element = t, A.setData(this._element, Zn, this);
    }

    var e = t.prototype;
    return e.show = function () {
      var t = this;

      if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && this._element.classList.contains(ei) || this._element.classList.contains(ni))) {
        var e,
            n = d(this._element),
            i = $.closest(this._element, si);

        if (i) {
          var o = "UL" === i.nodeName || "OL" === i.nodeName ? li : ai;
          e = (e = E($.find(o, i)))[e.length - 1];
        }

        var r = null;

        if (e && (r = Z.trigger(e, Jn.HIDE, {
          relatedTarget: this._element
        })), !(Z.trigger(this._element, Jn.SHOW, {
          relatedTarget: e
        }).defaultPrevented || null !== r && r.defaultPrevented)) {
          this._activate(this._element, i);

          var s = function s() {
            Z.trigger(e, Jn.HIDDEN, {
              relatedTarget: t._element
            }), Z.trigger(t._element, Jn.SHOWN, {
              relatedTarget: e
            });
          };

          n ? this._activate(n, n.parentNode, s) : s();
        }
      }
    }, e.dispose = function () {
      A.removeData(this._element, Zn), this._element = null;
    }, e._activate = function (t, e, n) {
      var i = this,
          o = (!e || "UL" !== e.nodeName && "OL" !== e.nodeName ? $.children(e, ai) : $.find(li, e))[0],
          r = n && o && o.classList.contains(ii),
          s = function s() {
        return i._transitionComplete(t, o, n);
      };

      if (o && r) {
        var a = g(o);
        o.classList.remove(oi), Z.one(o, c, s), m(o, a);
      } else s();
    }, e._transitionComplete = function (t, e, n) {
      if (e) {
        e.classList.remove(ei);
        var i = $.findOne(fi, e.parentNode);
        i && i.classList.remove(ei), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !1);
      }

      (t.classList.add(ei), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !0), D(t), t.classList.contains(ii) && t.classList.add(oi), t.parentNode && t.parentNode.classList.contains(ti)) && ($.closest(t, ri) && E($.find(ui)).forEach(function (t) {
        return t.classList.add(ei);
      }), t.setAttribute("aria-expanded", !0));
      n && n();
    }, t.jQueryInterface = function (e) {
      return this.each(function () {
        var n = A.getData(this, Zn) || new t(this);

        if ("string" == typeof e) {
          if ("undefined" == typeof n[e]) throw new TypeError('No method named "' + e + '"');
          n[e]();
        }
      });
    }, t.getInstance = function (t) {
      return A.getData(t, Zn);
    }, n(t, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }]), t;
  }();

  Z.on(document, Jn.CLICK_DATA_API, ci, function (t) {
    t.preventDefault(), (A.getData(this, Zn) || new hi(this)).show();
  });
  var di = T();

  if (di) {
    var gi = di.fn.tab;
    di.fn.tab = hi.jQueryInterface, di.fn.tab.Constructor = hi, di.fn.tab.noConflict = function () {
      return di.fn.tab = gi, hi.jQueryInterface;
    };
  }

  var pi = "bs.toast",
      _i = "." + pi,
      mi = {
    CLICK_DISMISS: "click.dismiss" + _i,
    HIDE: "hide" + _i,
    HIDDEN: "hidden" + _i,
    SHOW: "show" + _i,
    SHOWN: "shown" + _i
  },
      vi = "fade",
      Ei = "hide",
      yi = "show",
      bi = "showing",
      Di = {
    animation: "boolean",
    autohide: "boolean",
    delay: "number"
  },
      Ti = {
    animation: !0,
    autohide: !0,
    delay: 500
  },
      Ii = '[data-dismiss="toast"]',
      Ai = function () {
    function t(t, e) {
      this._element = t, this._config = this._getConfig(e), this._timeout = null, this._setListeners(), A.setData(t, pi, this);
    }

    var e = t.prototype;
    return e.show = function () {
      var t = this;

      if (!Z.trigger(this._element, mi.SHOW).defaultPrevented) {
        this._config.animation && this._element.classList.add(vi);

        var e = function e() {
          t._element.classList.remove(bi), t._element.classList.add(yi), Z.trigger(t._element, mi.SHOWN), t._config.autohide && (t._timeout = setTimeout(function () {
            t.hide();
          }, t._config.delay));
        };

        if (this._element.classList.remove(Ei), D(this._element), this._element.classList.add(bi), this._config.animation) {
          var n = g(this._element);
          Z.one(this._element, c, e), m(this._element, n);
        } else e();
      }
    }, e.hide = function () {
      var t = this;

      if (this._element.classList.contains(yi) && !Z.trigger(this._element, mi.HIDE).defaultPrevented) {
        var e = function e() {
          t._element.classList.add(Ei), Z.trigger(t._element, mi.HIDDEN);
        };

        if (this._element.classList.remove(yi), this._config.animation) {
          var n = g(this._element);
          Z.one(this._element, c, e), m(this._element, n);
        } else e();
      }
    }, e.dispose = function () {
      clearTimeout(this._timeout), this._timeout = null, this._element.classList.contains(yi) && this._element.classList.remove(yi), Z.off(this._element, mi.CLICK_DISMISS), A.removeData(this._element, pi), this._element = null, this._config = null;
    }, e._getConfig = function (t) {
      return t = r({}, Ti, {}, It.getDataAttributes(this._element), {}, "object" == _typeof(t) && t ? t : {}), v("toast", t, this.constructor.DefaultType), t;
    }, e._setListeners = function () {
      var t = this;
      Z.on(this._element, mi.CLICK_DISMISS, Ii, function () {
        return t.hide();
      });
    }, t.jQueryInterface = function (e) {
      return this.each(function () {
        var n = A.getData(this, pi);

        if (n || (n = new t(this, "object" == _typeof(e) && e)), "string" == typeof e) {
          if ("undefined" == typeof n[e]) throw new TypeError('No method named "' + e + '"');
          n[e](this);
        }
      });
    }, t.getInstance = function (t) {
      return A.getData(t, pi);
    }, n(t, null, [{
      key: "VERSION",
      get: function get() {
        return "4.3.1";
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return Di;
      }
    }, {
      key: "Default",
      get: function get() {
        return Ti;
      }
    }]), t;
  }(),
      Si = T();

  if (Si) {
    var wi = Si.fn.toast;
    Si.fn.toast = Ai.jQueryInterface, Si.fn.toast.Constructor = Ai, Si.fn.toast.noConflict = function () {
      return Si.fn.toast = wi, Ai.jQueryInterface;
    };
  }

  return {
    Alert: rt,
    Button: Et,
    Carousel: Qt,
    Collapse: ae,
    Dropdown: je,
    Modal: Ze,
    Popover: jn,
    ScrollSpy: Xn,
    Tab: hi,
    Toast: Ai,
    Tooltip: Tn
  };
});
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * demo.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2019, Codrops
 * http://www.codrops.com
 */
var Demo3 =
/*#__PURE__*/
function () {
  function Demo3() {
    _classCallCheck(this, Demo3);

    this.initCursor();
    this.initHovers();
  }

  _createClass(Demo3, [{
    key: "initCursor",
    value: function initCursor() {
      var _this = this;

      var _window = window,
          Back = _window.Back;
      this.outerCursor = document.querySelector(".circle-cursor--outer");
      this.innerCursor = document.querySelector(".circle-cursor--inner");
      this.outerCursorBox = this.outerCursor.getBoundingClientRect();
      this.outerCursorSpeed = 0;
      this.easing = Back.easeOut.config(1.7);
      this.clientX = -100;
      this.clientY = -100;
      this.showCursor = false;

      var unveilCursor = function unveilCursor() {
        TweenMax.set(_this.innerCursor, {
          x: _this.clientX,
          y: _this.clientY
        });
        TweenMax.set(_this.outerCursor, {
          x: _this.clientX - _this.outerCursorBox.width / 2,
          y: _this.clientY - _this.outerCursorBox.height / 2
        });
        setTimeout(function () {
          _this.outerCursorSpeed = 0.2;
        }, 100);
        _this.showCursor = true;
      };

      document.addEventListener("mousemove", unveilCursor);
      document.addEventListener("mousemove", function (e) {
        _this.clientX = e.clientX;
        _this.clientY = e.clientY;
      });

      var render = function render() {
        TweenMax.set(_this.innerCursor, {
          x: _this.clientX,
          y: _this.clientY
        });

        if (!_this.isStuck) {
          TweenMax.to(_this.outerCursor, _this.outerCursorSpeed, {
            x: _this.clientX - _this.outerCursorBox.width / 2,
            y: _this.clientY - _this.outerCursorBox.height / 2
          });
        }

        if (_this.showCursor) {
          document.removeEventListener("mousemove", unveilCursor);
        }

        requestAnimationFrame(render);
      };

      requestAnimationFrame(render);
    }
  }, {
    key: "initHovers",
    value: function initHovers() {
      var _this2 = this;

      var handleMouseEnter = function handleMouseEnter(e) {
        _this2.isStuck = true;
        var target = e.currentTarget;
        var box = target.getBoundingClientRect();
        _this2.outerCursorOriginals = {
          width: _this2.outerCursorBox.width,
          height: _this2.outerCursorBox.height
        };
        TweenMax.set(_this2.innerCursor, {
          opacity: 0.7
        });
        TweenMax.to(_this2.outerCursor, 0.2, {
          x: box.left + 40,
          y: box.left - 22,
          width: 40,
          height: 40,
          opacity: 1
        });
      };

      var handleMouseLeave = function handleMouseLeave() {
        _this2.isStuck = false;
        TweenMax.set(_this2.innerCursor, {
          opacity: 1
        });
        TweenMax.to(_this2.outerCursor, 0.2, {
          width: _this2.outerCursorOriginals.width,
          height: _this2.outerCursorOriginals.height,
          opacity: 0.2,
          borderColor: "#ffffff",
          borderWidth: 1,
          borderRadius: '50%',
          backgroundColor: "transparent"
        });
      };

      var linkItems = document.querySelectorAll(".nav-link");
      linkItems.forEach(function (item) {
        item.addEventListener("mouseenter", handleMouseEnter);
        item.addEventListener("mouseleave", handleMouseLeave);
      });
      var mainNavHoverTween = TweenMax.to(this.outerCursor, 0.3, {
        backgroundColor: "#081831",
        ease: this.easing,
        paused: true,
        width: 50,
        height: 50,
        opacity: 0.6,
        borderColor: "#081831"
      });

      var mainNavMouseEnter = function mainNavMouseEnter() {
        _this2.outerCursorSpeed = 0;
        TweenMax.set(_this2.innerCursor, {
          backgroundColor: '#081831',
          opacity: 0
        });
        mainNavHoverTween.play();
      };

      var mainNavMouseLeave = function mainNavMouseLeave() {
        _this2.outerCursorSpeed = 0.2;
        TweenMax.set(_this2.innerCursor, {
          opacity: 1
        });
        mainNavHoverTween.reverse();
      };

      var mainNavLinks = document.querySelectorAll(".ajax-link");
      mainNavLinks.forEach(function (item) {
        item.addEventListener("mouseenter", mainNavMouseEnter);
        item.addEventListener("mouseleave", mainNavMouseLeave);
      });
      var whiteHoverTween = TweenMax.to(this.outerCursor, 0.3, {
        opacity: 1,
        borderColor: "#081831",
        ease: this.easing,
        paused: true,
        backgroundColor: "rgba(0,0,0,0)"
      });

      var handleWhiteEnter = function handleWhiteEnter(e) {
        _this2.outerCursorSpeed = 0;
        TweenMax.set(_this2.innerCursor, {
          backgroundColor: '#081831',
          opacity: 1
        });
        whiteHoverTween.play();
      };

      var handleWhiteLeave = function handleWhiteLeave() {
        _this2.outerCursorSpeed = 0.2;
        TweenMax.set(_this2.innerCursor, {
          backgroundColor: '#fff',
          opacity: 1
        });
        whiteHoverTween.reverse();
      };

      var bright = document.querySelectorAll(".bg-bright");
      bright.forEach(function (item) {
        item.addEventListener("mouseenter", handleWhiteEnter);
        item.addEventListener("mouseleave", handleWhiteLeave);
      });
      var blueHoverTween = TweenMax.to(this.outerCursor, 0.3, {
        //  backgroundColor: "#ffffff",
        ease: this.easing,
        paused: true,
        opacity: 1,
        borderColor: "#fff"
      });

      var blueMouseEnter = function blueMouseEnter() {
        _this2.outerCursorSpeed = 0;
        TweenMax.set(_this2.innerCursor, {
          backgroundColor: '#fff',
          opacity: 1
        });
        blueHoverTween.play();
      };

      var blueMouseLeave = function blueMouseLeave() {
        _this2.outerCursorSpeed = 0.2;
        TweenMax.set(_this2.innerCursor, {
          backgroundColor: '#081831',
          opacity: 1
        });
        blueHoverTween.reverse();
      };

      var blue_bg = document.querySelectorAll(".bg-blue");
      blue_bg.forEach(function (item) {
        item.addEventListener("mouseenter", blueMouseEnter);
        item.addEventListener("mouseleave", blueMouseLeave);
      });
    }
  }]);

  return Demo3;
}();

var menu = new Demo3();
"use strict";

!function (t) {
  function e(e, n, i, r) {
    var a = e.text(),
        c = a.split(n),
        s = "";
    c.length && (t(c).each(function (t, e) {
      s += '<span class="' + i + (t + 1) + '" aria-hidden="true">' + e + "</span>" + r;
    }), e.attr("aria-label", a).empty().append(s));
  }

  var n = {
    init: function init() {
      return this.each(function () {
        e(t(this), "", "char", "");
      });
    },
    words: function words() {
      return this.each(function () {
        e(t(this), " ", "word", " ");
      });
    },
    lines: function lines() {
      return this.each(function () {
        var n = "eefec303079ad17405c889e092e105b0";
        e(t(this).children("br").replaceWith(n).end(), n, "line", "");
      });
    }
  };

  t.fn.lettering = function (e) {
    return e && n[e] ? n[e].apply(this, [].slice.call(arguments, 1)) : "letters" !== e && e ? (t.error("Method " + e + " does not exist on jQuery.lettering"), this) : n.init.apply(this, [].slice.call(arguments, 0));
  };
}(jQuery);
"use strict";

/**
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */
document.documentElement.className = "js";
TweenLite.defaultOverwrite = false;
var WebFontConfig = {
  typekit: {
    id: 'cim0eju'
  }
};
WebFont.load(WebFontConfig);
var lazyLoadInstance = new LazyLoad({
  elements_selector: ".lazyload" // ... more custom settings?

});

(function ($) {
  var headline = $("h1");
  var _char = '[class*="char"]';
  var button = $('.btn-quote');
  var $loader = document.querySelector('.loader'),
      mainContent = $('.site-content'),
      isAnimating = false;
  var $span = $('.js-span'); // Using lettering.js to wrap a <span> around each word and a <span> around each character.

  headline.lettering('words').children('span').lettering(); //EL's

  var $bg = $('.js-bg');
  var $heading = $('.js-heading');
  var preloader = $('#preloader'); //TIMELINE

  var plTl = new TimelineMax({
    paused: !0,
    //delay: 3,
    onComplete: function onComplete() {
      button.addClass('is-active'); //$("#preloader").remove()
    }
  });

  if ($('.page-header-inner').length > 0) {
    plTl.add('start').to(preloader, 1.2, {
      y: "-100%",
      ease: Circ.easeOut //CustomEase.create("custom", "M0,0 C0.86,0 0.07,1 1,1")

    }, "-=0").fromTo($bg, 0.4, {
      autoAlpha: 0
    }, {
      autoAlpha: 1,
      ease: Power3.easeOut
    }, 'start').fromTo($bg, 1.8, {
      scale: 2,
      x: '-70%'
    }, {
      scale: 1,
      x: '0%',
      ease: Power3.easeOut
    }, 'start') // .fromTo($heading, 0.4, {
    //   autoAlpha: 0,
    // }, {
    //   autoAlpha: 1,
    //   ease: Power3.easeOut
    // }, 'start')
    .staggerFrom($span, 0.4, {
      opacity: 0,
      ease: Power3.easeOut,
      y: '100%'
    }, '0.05').staggerFrom(_char, 0.5, {
      y: 80,
      opacity: 0,
      ease: Elastic.easeOut
    }, 0.01); // .staggerFrom(char, 1, {
    // 	  opacity: 0,
    // 	  ease: Elastic.easeOut,
    // 	  y: '100%'
    // 	}, '0.05');

    $('.wrapper').imagesLoaded({
      background: '.page-header-inner'
    }, function () {
      document.body.classList.remove('loading');
      plTl.play();
    });
  } else {
    new TweenMax.to(preloader, 1.2, {
      y: "-100%",
      ease: Circ.easeOut //CustomEase.create("custom", "M0,0 C0.86,0 0.07,1 1,1")

    }, "-=0");
  }

  jQuery(document).ready(function ($) {
    $('.navbar').on('click', '.nav-link', function (e) {
      e.preventDefault();
      var path = $(this).attr('href');
      var location = window.location.pathname.toString();
      var pathName = $(this).text().toString().toLowerCase(); // else check if we are inside a major section (i.e. blog)

      if (location.indexOf(pathName) !== -1) {
        console.log('somewhere inside ', pathName, path, location);
        window.location.reload(true);
        return false;
      }

      $loader.classList.add('loader--active');
      var $this = $(this);
      $this.parent().siblings().removeClass('active').end().addClass('active');
      var pageRef = $(this).attr('href');
      var targetTitle = $(this).attr('title');
      var pageSlug = $(this).attr('data-menu');
      callPage(pageRef, pageSlug, targetTitle);
    });

    window.onpopstate = function (e) {
      //$(".nav-link").fadeTo('fast', 1.0);
      callPage(e.state ? e.state.url : null);
    };

    function callPage(pageRefInput, pageSlug, targetTitleInput) {
      window.history.pushState({
        url: "" + pageRefInput + ""
      }, targetTitleInput, pageRefInput);
      var $this = $(this);
      $this.siblings().removeClass('active').end().addClass('active');
      var newSection = pageSlug;
      var primero = $('<div></div>');
      primero.load(newSection, function (e) {
        var t = document.createElement("div");
        var currentHTML = e;
        t.innerHTML = e;
        var i = t.querySelector("title");
        document.title = i.textContent;
      }); //create a new section element and insert it into the DOM

      var section = $('<main class="cd-main overflow-hidden ' + newSection + '"></main>').appendTo(mainContent); //load the new content from the proper html file

      section.load(newSection + ' .cd-main > *', function (event) {
        section.prev('.visible').removeClass('visible').end().addClass('visible');
        resetAfterAnimation(section);
      });
    }

    function resetAfterAnimation(newSection) {
      $('html, body').animate({
        scrollTop: 0
      }, 800); //once the new section animation is over, remove the old section and make the new one scrollable

      newSection.removeClass('overflow-hidden').prev('.cd-main').remove();
      isAnimating = false;
      init(); //reset your loading bar
      //EL's

      var $bg = $('.js-bg');
      var $heading = $('.js-heading');
      var $char = '[class*="word"]';
      var headline = $("h1");
      var $span = $('.js-span');
      headline.lettering('words'); //TIMELINE

      var plTl = new TimelineMax({
        //paused: true,
        //delay: 3,
        onComplete: function onComplete() {
          $('#preloader').remove();
        }
      });

      if ($('.page-header-inner').length > 0) {
        plTl.add('start').to(preloader, 1.2, {
          y: "-100%",
          ease: Circ.easeOut //CustomEase.create("custom", "M0,0 C0.86,0 0.07,1 1,1")

        }, "-=0").fromTo($bg, 0.4, {
          autoAlpha: 0
        }, {
          autoAlpha: 1,
          ease: Power3.easeOut
        }, 'start').fromTo($bg, 1.8, {
          scale: 2,
          x: '-70%'
        }, {
          scale: 1,
          x: '0%',
          ease: Power3.easeOut
        }, 'start').fromTo($heading, 0.4, {
          opacity: 0
        }, {
          opacity: 1,
          ease: Elastic.easeOut
        }, 'start').staggerFrom($span, 0.4, {
          opacity: 0,
          ease: Elastic.easeOut,
          y: '100%'
        }, '0.05').staggerFrom($char, 1, {
          opacity: 0,
          ease: Elastic.easeOut,
          y: '100%'
        }, '0.05');
        imagesLoaded(document.querySelector('.page-header-inner'), {
          background: true
        }, function (instance) {
          $loader.classList.remove('loader--active');
          plTl.play();
        });
      } else {
        $loader.classList.remove('loader--active');
      }
    }
  });
})(jQuery); //Img load


function imgLoad() {
  $('.portfolio-item').each(function () {
    var image = $(this).find('a').data('src');
    $(this).find('.portfolio-img-content').css({
      'background-image': 'url(' + image + ')',
      'background-size': 'cover',
      'background-position': 'center'
    });
  });
} //Parallax Effect


function parallaxbg() {
  if ($('.parallax-inner').length > 0) {
    $('.parallax-background').parallaxBackground({
      event: 'mouse_move',
      animation_type: 'shift',
      animate_duration: 3
    });
  }
}

function portfolioSettings() {
  var grid = document.querySelector('.grid');

  if (grid) {
    var $grid = $('.grid').isotope({
      itemSelector: '.grid__item',
      //   percentPosition: true,
      filter: '*',
      visibleStyle: {
        transform: 'translateY(0)',
        opacity: 1
      },
      hiddenStyle: {
        transform: 'translateY(100px)',
        opacity: 0
      },
      masonry: {
        columnWidth: '.grid__item'
      }
    });
    $grid.on('layoutComplete', function () {
      $('.loader-5').hide();
      $('.segmented-control').css('opacity', '1');
    });
    $grid.imagesLoaded().progress(function () {
      grid.classList.remove('are-images-unloaded');
      $grid.isotope('layout');
    });
    var $filterLinks = $('.segmented-control > li');
    $filterLinks.click(function () {
      var $this = $(this);
      $filterLinks.removeClass('active');
      $(this).addClass('active');
      var selector = $this.data('filter');
      $grid.isotope({
        filter: selector
      });
    });
  }
}

function inputEffects() {
  $('.btn-outline').lettering('letters');
  $.each($('.btn-outline'), function (index, reveal_item) {
    $(this).children().wrapAll("<span></span>");
    var span = $(this).children();
    span.clone().appendTo(this);
  }); // .children('span').lettering();

  var $tt_h = document.querySelectorAll(".wpcf7-form-control-wrap");
  $.each($tt_h, function (index, reveal_item) {
    var getLabel = $(this).next("label").detach();
    $(this).append(getLabel);
  });
  $(".input input").focusout(function () {
    if ($(this).val() != "") {
      $(this).addClass("not-empty");
    } else {
      $(this).removeClass("not-empty");
    }
  });
}

function serviceThings() {
  var e = document.getElementById("sv4");

  if (e) {
    var ctrl = new ScrollMagic.Controller();
    var tween = new TweenMax.fromTo(e, 1, {
      ease: Sine.easeOut,
      opacity: 0
    }, {
      ease: Sine.easeOut,
      opacity: 1,
      delay: .6
    }); // if (!isMobile) {

    new ScrollMagic.Scene({
      triggerElement: e,
      triggerHook: 'onEnter',
      reverse: false,
      duration: 120 // offset: 2

    }).setTween(tween).addTo(ctrl); // }

    var n = e.getBoundingClientRect().top,
        t = function t() {
      return e.parentElement.getBoundingClientRect().height > window.innerHeight && window.innerWidth > 992;
    };

    document.addEventListener("scroll", function () {
      var i = e.parentElement.getBoundingClientRect().height + n - window.innerHeight,
          d = window.pageYOffset || document.documentElement.scrollTop;
      t() ? d >= i ? (e.classList.add("pinned"), e.classList.remove("fixed")) : d >= n ? (e.classList.remove("pinned"), e.classList.add("fixed")) : (e.classList.remove("pinned"), e.classList.remove("fixed")) : e.classList.add("no-fixed");
    });
  }
}

function init() {
  inputEffects();
  portfolioSettings();
  imgLoad();
  serviceThings();
}

$(document).ready(function () {
  "use strict";

  init();
});
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * demo.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2018, Codrops
 * http://www.codrops.com
 */
{
  // Class Menu.
  var Menu =
  /*#__PURE__*/
  function () {
    function Menu(el) {
      var _this = this;

      _classCallCheck(this, Menu);

      this.DOM = {
        el: el
      }; // Open and close ctls.

      this.DOM.openCtrl = document.querySelector('.action--menu');
      this.DOM.closeCtrl = this.DOM.el.querySelector('.action--close');
      this.DOM.quoteCtrl = document.querySelector('.action--quote');
      this.DOM.openCtrl.addEventListener('click', function () {
        return _this.open();
      });

      if (this.DOM.quoteCtrl) {
        this.DOM.quoteCtrl.onclick = function () {
          this.open();
        };
      } // if($()){this.DOM.quoteCtrl.addEventListener('click', () => this.open());}


      this.DOM.closeCtrl.addEventListener('click', function () {
        return _this.close();
      });
      document.querySelector('.menu__item--2').addEventListener('click', function () {
        return _this.close();
      });
      this.DOM.mainCtrl = document.querySelector('nav.menu'); // The menu items.

      this.DOM.items = Array.from(this.DOM.el.querySelectorAll('.menu__item')); // The total number of items.

      this.itemsTotal = this.DOM.items.length; // Custom elements that will be animated.

      this.DOM.mainLinks = this.DOM.el.querySelectorAll('.mainmenu > a.mainmenu__item');
    } // Open the menu.


    _createClass(Menu, [{
      key: "open",
      value: function open() {
        this.toggle('open');
      } // Close the menu.

    }, {
      key: "close",
      value: function close() {
        this.toggle('close');
      }
    }, {
      key: "toggle",
      value: function toggle(action) {
        var _this2 = this;

        if (this.isAnimating) return; // (dis)allow the main image tilt effect.

        this.isAnimating = true; // Toggling the open state class.

        this.DOM.el.classList[action === 'open' ? 'add' : 'remove']('menu--open');
        document.body.classList[action === 'open' ? 'add' : 'remove']('menu--active'); // After all is animated..

        var animationEnd = function animationEnd(pos) {
          if (pos === _this2.itemsTotal - 1) {
            _this2.isAnimating = false;

            if (action === 'close') {
              TweenMax.to(_this2.DOM.mainCtrl, 0.6, {
                ease: Quart.easeInOut,
                y: '-101%' // onComplete: () => animationStart()

              });
            }
          }
        };

        if (action === 'open') {
          var configed = {};
          configed.y = '-101%';
          configed.x = '0%'; // Setting the initial values.

          TweenMax.set(this.DOM.mainCtrl, configed); // Animate.

          TweenMax.to(this.DOM.mainCtrl, .9, {
            ease: Quint.easeOut,
            x: '0%',
            y: '0%',
            onComplete: function onComplete() {
              return animationStart();
            }
          });
        } else {
          TweenMax.to(this.DOM.mainCtrl, 0.6, {
            ease: Quart.easeInOut,
            // x: config.x || 0,
            // y: '-101%',
            onComplete: function onComplete() {
              return animationStart();
            }
          });
        }

        var animationStart = function animationStart() {
          // Going through each menu´s item.
          _this2.DOM.items.forEach(function (el, pos) {
            // The inner wrapper.
            var innerEl = el.querySelector('.menu__item-inner'); // config and inner config will have the starting transform values (when opening) and the end ones (when closing) for both the item and its inner element.

            var config = {};
            var configInner = {}; // Direction defined in the HTML data-direction.
            // bt (bottom to top) || tb (top to bottom) || lr (left to right) || rl (right to left)

            var direction = el.dataset.direction; // Using 101% instead of 100% to avoid rendering problems.
            // In order to create the "reveal" effect, the item slides moves in one direction and its inner element in the opposite direction.

            if (direction === 'bt') {
              config.y = '101%';
              configInner.y = '-101%';
              configInner.x = '0%';
            } else if (direction === 'tb') {
              config.y = '-101%';
              configInner.y = '101%';
              configInner.x = '0%';
            } else if (direction === 'lr') {
              config.x = '-101%';
              configInner.x = '101%';
              config.y = '-101%';
            } else if (direction === 'rl') {
              config.x = '101%';
              configInner.x = '-101%';
              config.y = '-101%';
            } else {
              config.x = '101%';
              config.y = '101%';
              configInner.x = '-101%';
              configInner.y = '-101%';
            }

            if (action === 'open') {
              // Setting the initial values.
              TweenMax.set(el, config);
              TweenMax.set(innerEl, configInner); // Animate.

              TweenMax.to([el, innerEl], .9, {
                ease: Quint.easeOut,
                x: '0%',
                y: '0%',
                onComplete: function onComplete() {
                  return animationEnd(pos);
                }
              });
            } else {
              TweenMax.to(el, 0.6, {
                ease: Quart.easeInOut,
                x: config.x || 0,
                y: config.y || 0
              });
              TweenMax.to(innerEl, 0.6, {
                ease: Quart.easeInOut,
                x: configInner.x || 0,
                y: configInner.y || 0,
                onComplete: function onComplete() {
                  return animationEnd(pos);
                }
              });
            }
          });
        }; // Show/Hide open and close ctrls.


        TweenMax.to(this.DOM.closeCtrl, 0.6, {
          ease: action === 'open' ? Quint.easeOut : Quart.easeInOut,
          startAt: action === 'open' ? {
            rotation: 0
          } : null,
          opacity: action === 'open' ? 1 : 0,
          rotation: action === 'open' ? 180 : 270,
          delay: action === 'open' ? 1.5 : 0.3
        });
        TweenMax.to(this.DOM.openCtrl, action === 'open' ? 0.6 : 0.3, {
          //delay: action === 'open' ? 0 : 0.3,
          ease: action === 'open' ? Quint.easeOut : Quad.easeOut // opacity: action === 'open' ? 0 : 1

        }); // Main links animation.

        TweenMax.staggerTo(this.DOM.mainLinks, action === 'open' ? 0.9 : 0.2, {
          ease: action === 'open' ? Quint.easeOut : Quart.easeInOut,
          startAt: action === 'open' ? {
            y: '50%',
            opacity: 0
          } : null,
          y: action === 'open' ? '0%' : '50%',
          opacity: action === 'open' ? 1 : 0
        }, action === 'open' ? 0.1 : -0.1);
      }
    }]);

    return Menu;
  }(); // Initialize the Menu.


  var menu = new Menu(document.querySelector('nav.menu'));
  var allowTilt = true;
}
"use strict";

/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
(function () {
  var isIe = /(trident|msie)/i.test(navigator.userAgent);

  if (isIe && document.getElementById && window.addEventListener) {
    window.addEventListener('hashchange', function () {
      var id = location.hash.substring(1),
          element;

      if (!/^[A-z0-9_-]+$/.test(id)) {
        return;
      }

      element = document.getElementById(id);

      if (element) {
        if (!/^(?:a|select|input|button|textarea)$/i.test(element.tagName)) {
          element.tabIndex = -1;
        }

        element.focus();
      }
    }, false);
  }
})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFuaW1hdGlvbnMuanMiLCJib290c3RyYXAubWluLmpzIiwiY3Vyc29yLmpzIiwianF1ZXJ5LmxldHRlcmluZy5taW4uanMiLCJtYWluLmpzIiwibWVudS5qcyIsInNraXAtbGluay1mb2N1cy1maXguanMiXSwibmFtZXMiOlsialF1ZXJ5IiwiZG9jdW1lbnQiLCJyZWFkeSIsIiQiLCJzY3JvbGxBbmltIiwic2Nyb2xsTWFnaWMiLCJTY3JvbGxNYWdpYyIsIkNvbnRyb2xsZXIiLCJyZXZlYWwiLCJxdWVyeVNlbGVjdG9yQWxsIiwiZWFjaCIsImluZGV4IiwicmV2ZWFsX2l0ZW0iLCJTY2VuZSIsInRyaWdnZXJFbGVtZW50IiwidHJpZ2dlckhvb2siLCJyZXZlcnNlIiwiZHVyYXRpb24iLCJvZmZzZXQiLCJzZXRDbGFzc1RvZ2dsZSIsImFkZFRvIiwidW5mb2xkIiwiJGJnX3N3aXRjaCIsImNoaWxkIiwiYXR0ciIsInRoaXN0d2VlbiIsIlR3ZWVuTWF4IiwiZnJvbVRvIiwiYmFja2dyb3VuZENvbG9yIiwiaW1tZWRpYXRlUmVuZGVyIiwic2V0VHdlZW4iLCIkaGVhZGluZyIsImkiLCJzcGxpdG9uZSIsInF1ZXJ5U2VsZWN0b3IiLCJzcGxpdHR3byIsInNwbGl0dGhyZWUiLCJ0d2VlbkxpbmUiLCJUaW1lbGluZU1heCIsImRlbGF5IiwiJHdvcmQiLCIkZG90IiwicGF0aFByZXBhcmUiLCJzdHJva2VEYXNob2Zmc2V0IiwiZWFzZSIsIkxpbmVhciIsImVhc2VOb25lIiwic3RhZ2dlckZyb20iLCJ5Iiwib3BhY2l0eSIsInR3ZWVuQ2hhbmdlcyIsImFsbF9hbmltYXRpb25zIiwiYW5pbWF0ZSIsIiRmYWRlSW4iLCJpdGVtIiwiJHNsaWRlUmlnaHQiLCJ0d2VlbkZhZGUiLCJFbGFzdGljIiwiZWFzZU91dCIsIiR0dF9oIiwiaGVyZSIsIndpbmRvdyIsImlubmVyV2lkdGgiLCJyaWdodCIsIlBvd2VyNCIsImVhc2VJbk91dCIsIiRzbGlkZUluIiwieCIsInNjcm9sbEFuaW1hdGlvbnMiLCIkZWZmZWN0IiwiaXRlbXMiLCJjb25maWciLCJ5U3RhcnQiLCJ5RW5kIiwiUG93ZXIyIiwiYWRkSW5kaWNhdG9ycyIsIm5hbWUiLCIkZWwiLCJsaW5lTGVuZ3RoIiwiZ2V0VG90YWxMZW5ndGgiLCJjc3MiLCJ0IiwiZSIsImV4cG9ydHMiLCJtb2R1bGUiLCJyZXF1aXJlIiwiZGVmaW5lIiwiYW1kIiwic2VsZiIsImJvb3RzdHJhcCIsIlBvcHBlciIsIm4iLCJsZW5ndGgiLCJlbnVtZXJhYmxlIiwiY29uZmlndXJhYmxlIiwid3JpdGFibGUiLCJPYmplY3QiLCJkZWZpbmVQcm9wZXJ0eSIsImtleSIsInByb3RvdHlwZSIsInZhbHVlIiwibyIsImtleXMiLCJnZXRPd25Qcm9wZXJ0eVN5bWJvbHMiLCJmaWx0ZXIiLCJnZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IiLCJwdXNoIiwiYXBwbHkiLCJyIiwiYXJndW1lbnRzIiwiZm9yRWFjaCIsImdldE93blByb3BlcnR5RGVzY3JpcHRvcnMiLCJkZWZpbmVQcm9wZXJ0aWVzIiwiaGFzT3duUHJvcGVydHkiLCJzIiwiYSIsImwiLCJjIiwidSIsIk1hdGgiLCJyYW5kb20iLCJnZXRFbGVtZW50QnlJZCIsImYiLCJnZXRBdHRyaWJ1dGUiLCJ0cmltIiwiaCIsImQiLCJnIiwiZ2V0Q29tcHV0ZWRTdHlsZSIsInRyYW5zaXRpb25EdXJhdGlvbiIsInRyYW5zaXRpb25EZWxheSIsInBhcnNlRmxvYXQiLCJzcGxpdCIsInAiLCJjcmVhdGVFdmVudCIsImluaXRFdmVudCIsImRpc3BhdGNoRXZlbnQiLCJfIiwibm9kZVR5cGUiLCJtIiwiYWRkRXZlbnRMaXN0ZW5lciIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJzZXRUaW1lb3V0IiwidiIsInRvU3RyaW5nIiwiY2FsbCIsIm1hdGNoIiwidG9Mb3dlckNhc2UiLCJSZWdFeHAiLCJ0ZXN0IiwiRXJyb3IiLCJ0b1VwcGVyQ2FzZSIsIkUiLCJzbGljZSIsInN0eWxlIiwicGFyZW50Tm9kZSIsImRpc3BsYXkiLCJ2aXNpYmlsaXR5IiwiYiIsIkQiLCJvZmZzZXRIZWlnaHQiLCJUIiwiYm9keSIsImhhc0F0dHJpYnV0ZSIsIkkiLCJzZXQiLCJpZCIsImdldCIsIkEiLCJzZXREYXRhIiwiZ2V0RGF0YSIsInJlbW92ZURhdGEiLCJTIiwiRWxlbWVudCIsInciLCJtYXRjaGVzIiwiQyIsImNsb3Nlc3QiLCJMIiwiTyIsIk4iLCJDdXN0b21FdmVudCIsImJ1YmJsZXMiLCJjYW5jZWxhYmxlIiwiZGV0YWlsIiwiaW5pdEN1c3RvbUV2ZW50IiwicHJldmVudERlZmF1bHQiLCJkZWZhdWx0UHJldmVudGVkIiwiayIsIkV2ZW50IiwiUCIsImNyZWF0ZUVsZW1lbnQiLCJtc01hdGNoZXNTZWxlY3RvciIsIndlYmtpdE1hdGNoZXNTZWxlY3RvciIsInBhcmVudEVsZW1lbnQiLCJIIiwiQm9vbGVhbiIsInJlcGxhY2UiLCJyZW1vdmVBdHRyaWJ1dGUiLCJqIiwiTSIsIlIiLCJXIiwiVSIsIksiLCJWIiwibW91c2VlbnRlciIsIm1vdXNlbGVhdmUiLCJCIiwiRiIsInVpZEV2ZW50IiwiUSIsIlkiLCJ3aGljaCIsInR5cGUiLCJjaGFyQ29kZSIsImtleUNvZGUiLCJkZWxlZ2F0ZVRhcmdldCIsIkciLCJvcmlnaW5hbEhhbmRsZXIiLCJkZWxlZ2F0aW9uU2VsZWN0b3IiLCJYIiwiaW5kZXhPZiIsInEiLCJvbmVPZmYiLCJ0YXJnZXQiLCJaIiwib2ZmIiwieiIsIm9uIiwib25lIiwiY2hhckF0IiwidHJpZ2dlciIsImlzUHJvcGFnYXRpb25TdG9wcGVkIiwiaXNJbW1lZGlhdGVQcm9wYWdhdGlvblN0b3BwZWQiLCJpc0RlZmF1bHRQcmV2ZW50ZWQiLCJmaW5kIiwiZG9jdW1lbnRFbGVtZW50IiwiZmluZE9uZSIsImNoaWxkcmVuIiwicGFyZW50cyIsIk5vZGUiLCJFTEVNRU5UX05PREUiLCJwcmV2IiwicHJldmlvdXNTaWJsaW5nIiwiSiIsInR0IiwiZXQiLCJDTE9TRSIsIkNMT1NFRCIsIkNMSUNLX0RBVEFfQVBJIiwibnQiLCJpdCIsIm90IiwicnQiLCJfZWxlbWVudCIsImNsb3NlIiwiX2dldFJvb3RFbGVtZW50IiwiX3RyaWdnZXJDbG9zZUV2ZW50IiwiX3JlbW92ZUVsZW1lbnQiLCJkaXNwb3NlIiwiY2xhc3NMaXN0IiwicmVtb3ZlIiwiY29udGFpbnMiLCJfZGVzdHJveUVsZW1lbnQiLCJyZW1vdmVDaGlsZCIsImpRdWVyeUludGVyZmFjZSIsImhhbmRsZURpc21pc3MiLCJnZXRJbnN0YW5jZSIsInN0IiwiYXQiLCJmbiIsImFsZXJ0IiwiQ29uc3RydWN0b3IiLCJub0NvbmZsaWN0IiwibHQiLCJjdCIsInV0IiwiZnQiLCJodCIsImR0IiwiZ3QiLCJwdCIsIl90IiwibXQiLCJ2dCIsIkZPQ1VTX0RBVEFfQVBJIiwiQkxVUl9EQVRBX0FQSSIsIkV0IiwidG9nZ2xlIiwiY2hlY2tlZCIsImZvY3VzIiwic2V0QXR0cmlidXRlIiwiYWRkIiwieXQiLCJidCIsImJ1dHRvbiIsIkR0IiwiTnVtYmVyIiwiVHQiLCJJdCIsInNldERhdGFBdHRyaWJ1dGUiLCJyZW1vdmVEYXRhQXR0cmlidXRlIiwiZ2V0RGF0YUF0dHJpYnV0ZXMiLCJkYXRhc2V0IiwiZ2V0RGF0YUF0dHJpYnV0ZSIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsInRvcCIsInNjcm9sbFRvcCIsImxlZnQiLCJzY3JvbGxMZWZ0IiwicG9zaXRpb24iLCJvZmZzZXRUb3AiLCJvZmZzZXRMZWZ0IiwidG9nZ2xlQ2xhc3MiLCJBdCIsIlN0Iiwid3QiLCJDdCIsImludGVydmFsIiwia2V5Ym9hcmQiLCJzbGlkZSIsInBhdXNlIiwid3JhcCIsInRvdWNoIiwiTHQiLCJPdCIsIk50Iiwia3QiLCJQdCIsIkh0IiwiU0xJREUiLCJTTElEIiwiS0VZRE9XTiIsIk1PVVNFRU5URVIiLCJNT1VTRUxFQVZFIiwiVE9VQ0hTVEFSVCIsIlRPVUNITU9WRSIsIlRPVUNIRU5EIiwiUE9JTlRFUkRPV04iLCJQT0lOVEVSVVAiLCJEUkFHX1NUQVJUIiwiTE9BRF9EQVRBX0FQSSIsImp0IiwiTXQiLCJSdCIsInh0IiwiV3QiLCJVdCIsIkt0IiwiVnQiLCJCdCIsIkFDVElWRSIsIkFDVElWRV9JVEVNIiwiSVRFTSIsIklURU1fSU1HIiwiTkVYVF9QUkVWIiwiSU5ESUNBVE9SUyIsIkRBVEFfU0xJREUiLCJEQVRBX1JJREUiLCJGdCIsIlRPVUNIIiwiUEVOIiwiUXQiLCJfaXRlbXMiLCJfaW50ZXJ2YWwiLCJfYWN0aXZlRWxlbWVudCIsIl9pc1BhdXNlZCIsIl9pc1NsaWRpbmciLCJ0b3VjaFRpbWVvdXQiLCJ0b3VjaFN0YXJ0WCIsInRvdWNoRGVsdGFYIiwiX2NvbmZpZyIsIl9nZXRDb25maWciLCJfaW5kaWNhdG9yc0VsZW1lbnQiLCJfdG91Y2hTdXBwb3J0ZWQiLCJuYXZpZ2F0b3IiLCJtYXhUb3VjaFBvaW50cyIsIl9wb2ludGVyRXZlbnQiLCJQb2ludGVyRXZlbnQiLCJNU1BvaW50ZXJFdmVudCIsIl9hZGRFdmVudExpc3RlbmVycyIsIm5leHQiLCJfc2xpZGUiLCJuZXh0V2hlblZpc2libGUiLCJoaWRkZW4iLCJjeWNsZSIsImNsZWFySW50ZXJ2YWwiLCJzZXRJbnRlcnZhbCIsInZpc2liaWxpdHlTdGF0ZSIsImJpbmQiLCJ0byIsIl9nZXRJdGVtSW5kZXgiLCJfaGFuZGxlU3dpcGUiLCJhYnMiLCJfa2V5ZG93biIsIl9hZGRUb3VjaEV2ZW50TGlzdGVuZXJzIiwicG9pbnRlclR5cGUiLCJjbGllbnRYIiwidG91Y2hlcyIsImNsZWFyVGltZW91dCIsInRhZ05hbWUiLCJfZ2V0SXRlbUJ5RGlyZWN0aW9uIiwiX3RyaWdnZXJTbGlkZUV2ZW50IiwicmVsYXRlZFRhcmdldCIsImRpcmVjdGlvbiIsImZyb20iLCJfc2V0QWN0aXZlSW5kaWNhdG9yRWxlbWVudCIsInBhcnNlSW50IiwiZGVmYXVsdEludGVydmFsIiwiY2Fyb3VzZWxJbnRlcmZhY2UiLCJUeXBlRXJyb3IiLCJyaWRlIiwiZGF0YUFwaUNsaWNrSGFuZGxlciIsIll0IiwiR3QiLCJYdCIsInF0IiwienQiLCJadCIsInBhcmVudCIsIiR0IiwiSnQiLCJTSE9XIiwiU0hPV04iLCJISURFIiwiSElEREVOIiwidGUiLCJlZSIsIm5lIiwiaWUiLCJvZSIsInJlIiwic2UiLCJBQ1RJVkVTIiwiREFUQV9UT0dHTEUiLCJhZSIsIl9pc1RyYW5zaXRpb25pbmciLCJfdHJpZ2dlckFycmF5IiwiX3NlbGVjdG9yIiwiX3BhcmVudCIsIl9nZXRQYXJlbnQiLCJfYWRkQXJpYUFuZENvbGxhcHNlZENsYXNzIiwiaGlkZSIsInNob3ciLCJjb2xsYXBzZUludGVyZmFjZSIsIl9nZXREaW1lbnNpb24iLCJzZXRUcmFuc2l0aW9uaW5nIiwianF1ZXJ5IiwibGUiLCJjZSIsInVlIiwiZmUiLCJoZSIsImRlIiwiZ2UiLCJDTElDSyIsIktFWURPV05fREFUQV9BUEkiLCJLRVlVUF9EQVRBX0FQSSIsInBlIiwiX2UiLCJtZSIsInZlIiwiRWUiLCJ5ZSIsImJlIiwiRGUiLCJUZSIsIkllIiwiQWUiLCJTZSIsIndlIiwiQ2UiLCJMZSIsIk9lIiwiTmUiLCJrZSIsIlBlIiwiZmxpcCIsImJvdW5kYXJ5IiwicmVmZXJlbmNlIiwicG9wcGVyQ29uZmlnIiwiSGUiLCJqZSIsIl9wb3BwZXIiLCJfbWVudSIsIl9nZXRNZW51RWxlbWVudCIsIl9pbk5hdmJhciIsIl9kZXRlY3ROYXZiYXIiLCJkaXNhYmxlZCIsImNsZWFyTWVudXMiLCJnZXRQYXJlbnRGcm9tRWxlbWVudCIsIl9nZXRQb3BwZXJDb25maWciLCJkZXN0cm95IiwidXBkYXRlIiwic2NoZWR1bGVVcGRhdGUiLCJzdG9wUHJvcGFnYXRpb24iLCJjb25zdHJ1Y3RvciIsIkRlZmF1bHQiLCJEZWZhdWx0VHlwZSIsIl9nZXRQbGFjZW1lbnQiLCJfZ2V0T2Zmc2V0Iiwib2Zmc2V0cyIsInBsYWNlbWVudCIsIm1vZGlmaWVycyIsImVuYWJsZWQiLCJwcmV2ZW50T3ZlcmZsb3ciLCJib3VuZGFyaWVzRWxlbWVudCIsImFwcGx5U3R5bGUiLCJkcm9wZG93bkludGVyZmFjZSIsImNsaWNrRXZlbnQiLCJkYXRhQXBpS2V5ZG93bkhhbmRsZXIiLCJNZSIsIlJlIiwieGUiLCJXZSIsIlVlIiwiYmFja2Ryb3AiLCJLZSIsIlZlIiwiSElERV9QUkVWRU5URUQiLCJGT0NVU0lOIiwiUkVTSVpFIiwiQ0xJQ0tfRElTTUlTUyIsIktFWURPV05fRElTTUlTUyIsIk1PVVNFVVBfRElTTUlTUyIsIk1PVVNFRE9XTl9ESVNNSVNTIiwiQmUiLCJGZSIsIlFlIiwiWWUiLCJHZSIsIlhlIiwicWUiLCJ6ZSIsIkRJQUxPRyIsIk1PREFMX0JPRFkiLCJEQVRBX0RJU01JU1MiLCJGSVhFRF9DT05URU5UIiwiU1RJQ0tZX0NPTlRFTlQiLCJaZSIsIl9kaWFsb2ciLCJfYmFja2Ryb3AiLCJfaXNTaG93biIsIl9pc0JvZHlPdmVyZmxvd2luZyIsIl9pZ25vcmVCYWNrZHJvcENsaWNrIiwiX3Njcm9sbGJhcldpZHRoIiwiX2NoZWNrU2Nyb2xsYmFyIiwiX3NldFNjcm9sbGJhciIsIl9hZGp1c3REaWFsb2ciLCJfc2V0RXNjYXBlRXZlbnQiLCJfc2V0UmVzaXplRXZlbnQiLCJfc2hvd0JhY2tkcm9wIiwiX3Nob3dFbGVtZW50IiwiX2hpZGVNb2RhbCIsImhhbmRsZVVwZGF0ZSIsImFwcGVuZENoaWxkIiwiX2VuZm9yY2VGb2N1cyIsIl90cmlnZ2VyQmFja2Ryb3BUcmFuc2l0aW9uIiwiX3Jlc2V0QWRqdXN0bWVudHMiLCJfcmVzZXRTY3JvbGxiYXIiLCJfcmVtb3ZlQmFja2Ryb3AiLCJjbGFzc05hbWUiLCJjdXJyZW50VGFyZ2V0Iiwic2Nyb2xsSGVpZ2h0IiwiY2xpZW50SGVpZ2h0IiwicGFkZGluZ0xlZnQiLCJwYWRkaW5nUmlnaHQiLCJfZ2V0U2Nyb2xsYmFyV2lkdGgiLCJtYXJnaW5SaWdodCIsIndpZHRoIiwiY2xpZW50V2lkdGgiLCIkZSIsIkplIiwibW9kYWwiLCJ0biIsImVuIiwibm4iLCJub2RlTmFtZSIsIm5vZGVWYWx1ZSIsInJuIiwiYXJlYSIsImJyIiwiY29sIiwiY29kZSIsImRpdiIsImVtIiwiaHIiLCJoMSIsImgyIiwiaDMiLCJoNCIsImg1IiwiaDYiLCJpbWciLCJsaSIsIm9sIiwicHJlIiwic21hbGwiLCJzcGFuIiwic3ViIiwic3VwIiwic3Ryb25nIiwidWwiLCJzbiIsIkRPTVBhcnNlciIsInBhcnNlRnJvbVN0cmluZyIsImF0dHJpYnV0ZXMiLCJjb25jYXQiLCJpbm5lckhUTUwiLCJhbiIsImxuIiwiY24iLCJ1biIsImFuaW1hdGlvbiIsInRlbXBsYXRlIiwidGl0bGUiLCJodG1sIiwic2VsZWN0b3IiLCJjb250YWluZXIiLCJmYWxsYmFja1BsYWNlbWVudCIsInNhbml0aXplIiwic2FuaXRpemVGbiIsIndoaXRlTGlzdCIsIkFVVE8iLCJUT1AiLCJSSUdIVCIsIkJPVFRPTSIsIkxFRlQiLCJobiIsImRuIiwiZ24iLCJwbiIsIklOU0VSVEVEIiwiRk9DVVNPVVQiLCJfbiIsIm1uIiwidm4iLCJFbiIsInluIiwiYm4iLCJEbiIsIlRuIiwiX2lzRW5hYmxlZCIsIl90aW1lb3V0IiwiX2hvdmVyU3RhdGUiLCJfYWN0aXZlVHJpZ2dlciIsImVsZW1lbnQiLCJ0aXAiLCJfc2V0TGlzdGVuZXJzIiwiREFUQV9LRVkiLCJlbmFibGUiLCJkaXNhYmxlIiwidG9nZ2xlRW5hYmxlZCIsIl9nZXREZWxlZ2F0ZUNvbmZpZyIsImNsaWNrIiwiX2lzV2l0aEFjdGl2ZVRyaWdnZXIiLCJfZW50ZXIiLCJfbGVhdmUiLCJnZXRUaXBFbGVtZW50IiwiRVZFTlRfS0VZIiwiX2hpZGVNb2RhbEhhbmRsZXIiLCJpc1dpdGhDb250ZW50IiwiYXR0YWNoU2hhZG93IiwiZ2V0Um9vdE5vZGUiLCJTaGFkb3dSb290Iiwib3duZXJEb2N1bWVudCIsIk5BTUUiLCJzZXRDb250ZW50IiwiX2dldEF0dGFjaG1lbnQiLCJfYWRkQXR0YWNobWVudENsYXNzIiwiX2dldENvbnRhaW5lciIsIl9maXhUcmFuc2l0aW9uIiwiX2NsZWFuVGlwQ2xhc3MiLCJnZXRUaXRsZSIsInNldEVsZW1lbnRDb250ZW50IiwiaW5uZXJUZXh0IiwidGV4dENvbnRlbnQiLCJiZWhhdmlvciIsImFycm93Iiwib25DcmVhdGUiLCJvcmlnaW5hbFBsYWNlbWVudCIsIl9oYW5kbGVQb3BwZXJQbGFjZW1lbnRDaGFuZ2UiLCJvblVwZGF0ZSIsIl9maXhUaXRsZSIsImNvbnRlbnQiLCJtYXAiLCJpbnN0YW5jZSIsInBvcHBlciIsIkluIiwiQW4iLCJ0b29sdGlwIiwiU24iLCJ3biIsIkNuIiwiTG4iLCJPbiIsIk5uIiwia24iLCJQbiIsIkhuIiwiam4iLCJjcmVhdGUiLCJfX3Byb3RvX18iLCJfZ2V0Q29udGVudCIsIk1uIiwiUm4iLCJwb3BvdmVyIiwieG4iLCJXbiIsIlVuIiwiS24iLCJtZXRob2QiLCJWbiIsIkJuIiwiQUNUSVZBVEUiLCJTQ1JPTEwiLCJGbiIsIkRST1BET1dOX0lURU0iLCJRbiIsIkRBVEFfU1BZIiwiTkFWX0xJU1RfR1JPVVAiLCJOQVZfTElOS1MiLCJOQVZfSVRFTVMiLCJMSVNUX0lURU1TIiwiRFJPUERPV04iLCJEUk9QRE9XTl9UT0dHTEUiLCJZbiIsIkduIiwiWG4iLCJfc2Nyb2xsRWxlbWVudCIsIl9vZmZzZXRzIiwiX3RhcmdldHMiLCJfYWN0aXZlVGFyZ2V0IiwiX3Njcm9sbEhlaWdodCIsIl9wcm9jZXNzIiwicmVmcmVzaCIsIl9nZXRTY3JvbGxUb3AiLCJfZ2V0U2Nyb2xsSGVpZ2h0IiwiaGVpZ2h0Iiwic29ydCIsInBhZ2VZT2Zmc2V0IiwibWF4IiwiX2dldE9mZnNldEhlaWdodCIsImlubmVySGVpZ2h0IiwiX2FjdGl2YXRlIiwiX2NsZWFyIiwiam9pbiIsInFuIiwiem4iLCJabiIsIiRuIiwiSm4iLCJ0aSIsImVpIiwibmkiLCJpaSIsIm9pIiwicmkiLCJzaSIsImFpIiwiY2kiLCJ1aSIsImZpIiwiaGkiLCJfdHJhbnNpdGlvbkNvbXBsZXRlIiwiZGkiLCJnaSIsInRhYiIsInBpIiwiX2kiLCJtaSIsInZpIiwiRWkiLCJ5aSIsImJpIiwiRGkiLCJhdXRvaGlkZSIsIlRpIiwiSWkiLCJBaSIsIlNpIiwid2kiLCJ0b2FzdCIsIkFsZXJ0IiwiQnV0dG9uIiwiQ2Fyb3VzZWwiLCJDb2xsYXBzZSIsIkRyb3Bkb3duIiwiTW9kYWwiLCJQb3BvdmVyIiwiU2Nyb2xsU3B5IiwiVGFiIiwiVG9hc3QiLCJUb29sdGlwIiwiRGVtbzMiLCJpbml0Q3Vyc29yIiwiaW5pdEhvdmVycyIsIkJhY2siLCJvdXRlckN1cnNvciIsImlubmVyQ3Vyc29yIiwib3V0ZXJDdXJzb3JCb3giLCJvdXRlckN1cnNvclNwZWVkIiwiZWFzaW5nIiwiY2xpZW50WSIsInNob3dDdXJzb3IiLCJ1bnZlaWxDdXJzb3IiLCJyZW5kZXIiLCJpc1N0dWNrIiwicmVxdWVzdEFuaW1hdGlvbkZyYW1lIiwiaGFuZGxlTW91c2VFbnRlciIsImJveCIsIm91dGVyQ3Vyc29yT3JpZ2luYWxzIiwiaGFuZGxlTW91c2VMZWF2ZSIsImJvcmRlckNvbG9yIiwiYm9yZGVyV2lkdGgiLCJib3JkZXJSYWRpdXMiLCJsaW5rSXRlbXMiLCJtYWluTmF2SG92ZXJUd2VlbiIsInBhdXNlZCIsIm1haW5OYXZNb3VzZUVudGVyIiwicGxheSIsIm1haW5OYXZNb3VzZUxlYXZlIiwibWFpbk5hdkxpbmtzIiwid2hpdGVIb3ZlclR3ZWVuIiwiaGFuZGxlV2hpdGVFbnRlciIsImhhbmRsZVdoaXRlTGVhdmUiLCJicmlnaHQiLCJibHVlSG92ZXJUd2VlbiIsImJsdWVNb3VzZUVudGVyIiwiYmx1ZU1vdXNlTGVhdmUiLCJibHVlX2JnIiwibWVudSIsInRleHQiLCJlbXB0eSIsImFwcGVuZCIsImluaXQiLCJ3b3JkcyIsImxpbmVzIiwicmVwbGFjZVdpdGgiLCJlbmQiLCJsZXR0ZXJpbmciLCJlcnJvciIsIlR3ZWVuTGl0ZSIsImRlZmF1bHRPdmVyd3JpdGUiLCJXZWJGb250Q29uZmlnIiwidHlwZWtpdCIsIldlYkZvbnQiLCJsb2FkIiwibGF6eUxvYWRJbnN0YW5jZSIsIkxhenlMb2FkIiwiZWxlbWVudHNfc2VsZWN0b3IiLCJoZWFkbGluZSIsImNoYXIiLCIkbG9hZGVyIiwibWFpbkNvbnRlbnQiLCJpc0FuaW1hdGluZyIsIiRzcGFuIiwiJGJnIiwicHJlbG9hZGVyIiwicGxUbCIsIm9uQ29tcGxldGUiLCJhZGRDbGFzcyIsIkNpcmMiLCJhdXRvQWxwaGEiLCJQb3dlcjMiLCJzY2FsZSIsImltYWdlc0xvYWRlZCIsImJhY2tncm91bmQiLCJwYXRoIiwibG9jYXRpb24iLCJwYXRobmFtZSIsInBhdGhOYW1lIiwiY29uc29sZSIsImxvZyIsInJlbG9hZCIsIiR0aGlzIiwic2libGluZ3MiLCJyZW1vdmVDbGFzcyIsInBhZ2VSZWYiLCJ0YXJnZXRUaXRsZSIsInBhZ2VTbHVnIiwiY2FsbFBhZ2UiLCJvbnBvcHN0YXRlIiwic3RhdGUiLCJ1cmwiLCJwYWdlUmVmSW5wdXQiLCJ0YXJnZXRUaXRsZUlucHV0IiwiaGlzdG9yeSIsInB1c2hTdGF0ZSIsIm5ld1NlY3Rpb24iLCJwcmltZXJvIiwiY3VycmVudEhUTUwiLCJzZWN0aW9uIiwiYXBwZW5kVG8iLCJldmVudCIsInJlc2V0QWZ0ZXJBbmltYXRpb24iLCIkY2hhciIsImltZ0xvYWQiLCJpbWFnZSIsImRhdGEiLCJwYXJhbGxheGJnIiwicGFyYWxsYXhCYWNrZ3JvdW5kIiwiYW5pbWF0aW9uX3R5cGUiLCJhbmltYXRlX2R1cmF0aW9uIiwicG9ydGZvbGlvU2V0dGluZ3MiLCJncmlkIiwiJGdyaWQiLCJpc290b3BlIiwiaXRlbVNlbGVjdG9yIiwidmlzaWJsZVN0eWxlIiwidHJhbnNmb3JtIiwiaGlkZGVuU3R5bGUiLCJtYXNvbnJ5IiwiY29sdW1uV2lkdGgiLCJwcm9ncmVzcyIsIiRmaWx0ZXJMaW5rcyIsImlucHV0RWZmZWN0cyIsIndyYXBBbGwiLCJjbG9uZSIsImdldExhYmVsIiwiZGV0YWNoIiwiZm9jdXNvdXQiLCJ2YWwiLCJzZXJ2aWNlVGhpbmdzIiwiY3RybCIsInR3ZWVuIiwiU2luZSIsIk1lbnUiLCJlbCIsIkRPTSIsIm9wZW5DdHJsIiwiY2xvc2VDdHJsIiwicXVvdGVDdHJsIiwib3BlbiIsIm9uY2xpY2siLCJtYWluQ3RybCIsIkFycmF5IiwiaXRlbXNUb3RhbCIsIm1haW5MaW5rcyIsImFjdGlvbiIsImFuaW1hdGlvbkVuZCIsInBvcyIsIlF1YXJ0IiwiY29uZmlnZWQiLCJRdWludCIsImFuaW1hdGlvblN0YXJ0IiwiaW5uZXJFbCIsImNvbmZpZ0lubmVyIiwic3RhcnRBdCIsInJvdGF0aW9uIiwiUXVhZCIsInN0YWdnZXJUbyIsImFsbG93VGlsdCIsImlzSWUiLCJ1c2VyQWdlbnQiLCJoYXNoIiwic3Vic3RyaW5nIiwidGFiSW5kZXgiXSwibWFwcGluZ3MiOiI7O0FBQUFBLE1BQU0sQ0FBQ0MsUUFBRCxDQUFOLENBQWlCQyxLQUFqQixDQUF1QixVQUFTQyxDQUFULEVBQVk7QUFFakNDLEVBQUFBLFVBQVU7QUFDWCxDQUhEOztBQVFBLFNBQVNBLFVBQVQsR0FBc0I7QUFFbEI7QUFDQSxNQUFJQyxXQUFXLEdBQUcsSUFBSUMsV0FBVyxDQUFDQyxVQUFoQixFQUFsQixDQUhrQixDQUtsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7O0FBQ0EsTUFBSUMsTUFBTSxHQUFHUCxRQUFRLENBQUNRLGdCQUFULENBQTBCLGdCQUExQixDQUFiO0FBRUFOLEVBQUFBLENBQUMsQ0FBQ08sSUFBRixDQUFPRixNQUFQLEVBQWUsVUFBU0csS0FBVCxFQUFnQkMsV0FBaEIsRUFBNkI7QUFDeEM7QUFDRSxRQUFJTixXQUFXLENBQUNPLEtBQWhCLENBQXNCO0FBQ3BCQyxNQUFBQSxjQUFjLEVBQUVGLFdBREk7QUFFcEJHLE1BQUFBLFdBQVcsRUFBRSxTQUZPO0FBR3BCQyxNQUFBQSxPQUFPLEVBQUUsS0FIVztBQUlwQkMsTUFBQUEsUUFBUSxFQUFFLENBSlU7QUFLcEJDLE1BQUFBLE1BQU0sRUFBRTtBQUxZLEtBQXRCLEVBT0NDLGNBUEQsQ0FPZ0JQLFdBUGhCLEVBTzZCLFNBUDdCLEVBUUNRLEtBUkQsQ0FRT2YsV0FSUCxFQUZzQyxDQVd4QztBQUNELEdBWkgsRUEvQmtCLENBOENsQjs7QUFDQSxNQUFJZ0IsTUFBTSxHQUFHcEIsUUFBUSxDQUFDUSxnQkFBVCxDQUEwQixTQUExQixDQUFiO0FBRUFOLEVBQUFBLENBQUMsQ0FBQ08sSUFBRixDQUFPVyxNQUFQLEVBQWUsVUFBU1YsS0FBVCxFQUFnQkMsV0FBaEIsRUFBNkI7QUFDeEM7QUFDRSxRQUFJTixXQUFXLENBQUNPLEtBQWhCLENBQXNCO0FBQ3BCQyxNQUFBQSxjQUFjLEVBQUVGLFdBREk7QUFFcEJHLE1BQUFBLFdBQVcsRUFBRSxTQUZPO0FBR3BCQyxNQUFBQSxPQUFPLEVBQUUsS0FIVztBQUlwQkUsTUFBQUEsTUFBTSxFQUFFLENBSlk7QUFLcEJELE1BQUFBLFFBQVEsRUFBRTtBQUxVLEtBQXRCLEVBT0NFLGNBUEQsQ0FPZ0JQLFdBUGhCLEVBTzZCLFNBUDdCLEVBUUNRLEtBUkQsQ0FRT2YsV0FSUCxFQUZzQyxDQVd4QztBQUNELEdBWkg7QUFlQSxNQUFJaUIsVUFBVSxHQUFHckIsUUFBUSxDQUFDUSxnQkFBVCxDQUEwQixZQUExQixDQUFqQjtBQUVBTixFQUFBQSxDQUFDLENBQUNPLElBQUYsQ0FBT1ksVUFBUCxFQUFtQixVQUFTWCxLQUFULEVBQWdCQyxXQUFoQixFQUE2QjtBQUM5QyxRQUFJVyxLQUFLLEdBQUdwQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFxQixJQUFSLENBQWEsZUFBYixDQUFaLENBRDhDLENBRWxEOztBQUVBLFFBQUlDLFNBQVMsR0FBR0MsUUFBUSxDQUFDQyxNQUFULENBQWdCTCxVQUFVLENBQUNYLEtBQUQsQ0FBMUIsRUFBbUMsQ0FBbkMsRUFBc0M7QUFDckRpQixNQUFBQSxlQUFlLEVBQUM7QUFEcUMsS0FBdEMsRUFFYjtBQUNBQSxNQUFBQSxlQUFlLEVBQUNMLEtBRGhCO0FBRUFNLE1BQUFBLGVBQWUsRUFBRTtBQUZqQixLQUZhLENBQWhCLENBSmtELENBVTNDOztBQUNBLFFBQUl2QixXQUFXLENBQUNPLEtBQWhCLENBQXNCO0FBQ3BCQyxNQUFBQSxjQUFjLEVBQUcsSUFERztBQUVwQkksTUFBQUEsTUFBTSxFQUFFLENBRlk7QUFHcEJELE1BQUFBLFFBQVEsRUFBRSxNQUhVO0FBSXBCRixNQUFBQSxXQUFXLEVBQUU7QUFKTyxLQUF0QixFQU9DZSxRQVBELENBT1VMLFNBUFYsRUFRQ0wsS0FSRCxDQVFPZixXQVJQO0FBVUQsR0FyQko7QUF3QkEsTUFBSTBCLFFBQVEsR0FBRzlCLFFBQVEsQ0FBQ1EsZ0JBQVQsQ0FBMEIsbUJBQTFCLENBQWY7QUFDQU4sRUFBQUEsQ0FBQyxDQUFDTyxJQUFGLENBQU9xQixRQUFQLEVBQWlCLFVBQVNDLENBQVQsRUFBWXBCLFdBQVosRUFBeUI7QUFHeEMsUUFBSXFCLFFBQVEsR0FBR0YsUUFBUSxDQUFDQyxDQUFELENBQVIsQ0FBWUUsYUFBWixDQUEwQixJQUExQixDQUFmO0FBRUEsUUFBSUMsUUFBUSxHQUFHSixRQUFRLENBQUNDLENBQUQsQ0FBUixDQUFZRSxhQUFaLENBQTBCLG9CQUExQixDQUFmO0FBRUEsUUFBSUUsVUFBVSxHQUFHTCxRQUFRLENBQUNDLENBQUQsQ0FBUixDQUFZRSxhQUFaLENBQTBCLEtBQTFCLENBQWpCO0FBRUEsUUFBSUcsU0FBUyxHQUFHLElBQUlDLFdBQUosQ0FBZ0I7QUFDOUJDLE1BQUFBLEtBQUssRUFBRTtBQUR1QixLQUFoQixDQUFoQixDQVR3QyxDQWF4Qzs7QUFFRSxRQUFJQyxLQUFLLEdBQUdyQyxDQUFDLENBQUMsYUFBRCxDQUFiO0FBQ0EsUUFBSXNDLElBQUksR0FBR3RDLENBQUMsQ0FBQyxlQUFELENBQVosQ0FoQnNDLENBa0J0Qzs7QUFDQXVDLElBQUFBLFdBQVcsQ0FBQ0YsS0FBRCxDQUFYO0FBQ0FFLElBQUFBLFdBQVcsQ0FBQ0QsSUFBRCxDQUFYO0FBSUFKLElBQUFBLFNBQVMsQ0FBQ1YsTUFBVixDQUFpQmEsS0FBakIsRUFBd0IsR0FBeEIsRUFBNEI7QUFBQ0csTUFBQUEsZ0JBQWdCLEVBQUU7QUFBbkIsS0FBNUIsRUFBbUQ7QUFBRUMsTUFBQUEsSUFBSSxFQUFDQyxNQUFNLENBQUNDLFFBQWQ7QUFBd0JqQixNQUFBQSxlQUFlLEVBQUU7QUFBekMsS0FBbkQsRUFBb0csQ0FBcEc7QUFDQVEsSUFBQUEsU0FBUyxDQUFDVixNQUFWLENBQWlCYyxJQUFqQixFQUF1QixHQUF2QixFQUE0QjtBQUFDRSxNQUFBQSxnQkFBZ0IsRUFBRTtBQUFuQixLQUE1QixFQUFtRDtBQUFHQyxNQUFBQSxJQUFJLEVBQUNDLE1BQU0sQ0FBQ0MsUUFBZjtBQUF5QmpCLE1BQUFBLGVBQWUsRUFBRTtBQUExQyxLQUFuRCxFQUFxRyxDQUFyRyxFQXpCc0MsQ0EyQnRDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUFRLElBQUFBLFNBQVMsQ0FBQ1UsV0FBVixDQUFzQlosUUFBdEIsRUFBZ0MsR0FBaEMsRUFBcUM7QUFDbkNhLE1BQUFBLENBQUMsRUFBRSxFQURnQztBQUVuQ0MsTUFBQUEsT0FBTyxFQUFFLENBRjBCO0FBR25DTCxNQUFBQSxJQUFJLEVBQUU7QUFINkIsS0FBckMsRUFLRyxJQUxIO0FBT0FQLElBQUFBLFNBQVMsQ0FBQ1UsV0FBVixDQUFzQlgsVUFBdEIsRUFBa0MsRUFBbEMsRUFBc0M7QUFDcENZLE1BQUFBLENBQUMsRUFBRSxFQURpQztBQUVwQ0MsTUFBQUEsT0FBTyxFQUFFLENBRjJCO0FBR3BDTCxNQUFBQSxJQUFJLEVBQUU7QUFIOEIsS0FBdEMsRUFLRyxJQUxIO0FBU0EsUUFBSXRDLFdBQVcsQ0FBQ08sS0FBaEIsQ0FBc0I7QUFDcEJDLE1BQUFBLGNBQWMsRUFBRSxJQURJO0FBRXBCQyxNQUFBQSxXQUFXLEVBQUUsU0FGTztBQUdwQkMsTUFBQUEsT0FBTyxFQUFFLEtBSFc7QUFJcEJFLE1BQUFBLE1BQU0sRUFBRSxHQUpZO0FBS3BCRCxNQUFBQSxRQUFRLEVBQUUsR0FMVTtBQUtMaUMsTUFBQUEsWUFBWSxFQUFFO0FBTFQsS0FBdEIsRUFPQ3BCLFFBUEQsQ0FPVU8sU0FQVixFQVFDbEIsY0FSRCxDQVFnQlksUUFBUSxDQUFDQyxDQUFELENBUnhCLEVBUTZCLFNBUjdCLEVBU0NaLEtBVEQsQ0FTT2YsV0FUUDtBQVdELEdBNURIO0FBa0VBOztBQUVBLE1BQUk4QyxjQUFjLEdBQUcsQ0FBQyxVQUFELEVBQWEsWUFBYixDQUFyQjtBQUNBaEQsRUFBQUEsQ0FBQyxDQUFDTyxJQUFGLENBQU95QyxjQUFQLEVBQXVCLFVBQVN4QyxLQUFULEVBQWdCeUMsT0FBaEIsRUFBeUI7QUFDL0MsUUFBSUMsT0FBTyxHQUFHcEQsUUFBUSxDQUFDUSxnQkFBVCxDQUEwQixNQUFJMEMsY0FBYyxDQUFDeEMsS0FBRCxDQUE1QyxDQUFkO0FBQ0FSLElBQUFBLENBQUMsQ0FBQ08sSUFBRixDQUFPMkMsT0FBUCxFQUFnQixVQUFTckIsQ0FBVCxFQUFZc0IsSUFBWixFQUFrQjtBQUVqQyxVQUFJaEQsV0FBVyxDQUFDTyxLQUFoQixDQUFzQjtBQUNwQkMsUUFBQUEsY0FBYyxFQUFFLElBREk7QUFFcEJDLFFBQUFBLFdBQVcsRUFBRSxTQUZPO0FBR3BCQyxRQUFBQSxPQUFPLEVBQUUsS0FIVztBQUlwQkUsUUFBQUEsTUFBTSxFQUFFLENBSlk7QUFLcEJELFFBQUFBLFFBQVEsRUFBRTtBQUxVLE9BQXRCLEVBT0NFLGNBUEQsQ0FPZ0JtQyxJQVBoQixFQU9zQixVQVB0QixFQVFDbEMsS0FSRCxDQVFPZixXQVJQO0FBVUQsS0FaQTtBQWFELEdBZkE7QUFrQkEsTUFBSWtELFdBQVcsR0FBR3RELFFBQVEsQ0FBQ1EsZ0JBQVQsQ0FBMEIsZUFBMUIsQ0FBbEI7QUFDQU4sRUFBQUEsQ0FBQyxDQUFDTyxJQUFGLENBQU82QyxXQUFQLEVBQW9CLFVBQVN2QixDQUFULEVBQVlzQixJQUFaLEVBQWtCO0FBSXJDLFFBQUlFLFNBQVMsR0FBRyxJQUFJOUIsUUFBUSxDQUFDQyxNQUFiLENBQW9CMkIsSUFBcEIsRUFBMEIsQ0FBMUIsRUFBNkI7QUFBQ0wsTUFBQUEsT0FBTyxFQUFFLENBQVY7QUFBYUQsTUFBQUEsQ0FBQyxFQUFFO0FBQWhCLEtBQTdCLEVBQWtEO0FBQ2pFQyxNQUFBQSxPQUFPLEVBQUUsQ0FEd0Q7QUFFakVELE1BQUFBLENBQUMsRUFBRSxDQUY4RDtBQUdqRUosTUFBQUEsSUFBSSxFQUFFYSxPQUFPLENBQUNDO0FBSG1ELEtBQWxELENBQWhCO0FBTUEsUUFBSXBELFdBQVcsQ0FBQ08sS0FBaEIsQ0FBc0I7QUFDcEJDLE1BQUFBLGNBQWMsRUFBRSxJQURJO0FBRXBCQyxNQUFBQSxXQUFXLEVBQUUsU0FGTztBQUdwQkMsTUFBQUEsT0FBTyxFQUFFLEtBSFc7QUFJcEJFLE1BQUFBLE1BQU0sRUFBRSxDQUpZO0FBS3BCRCxNQUFBQSxRQUFRLEVBQUU7QUFMVSxLQUF0QixFQU9hO0FBUGIsS0FTZWEsUUFUZixDQVN3QjBCLFNBVHhCLEVBVWVwQyxLQVZmLENBVXFCZixXQVZyQjtBQVlhLEdBdEJkO0FBMkJBOztBQUVBLE1BQUlzRCxLQUFLLEdBQUcxRCxRQUFRLENBQUNRLGdCQUFULENBQTBCLE9BQTFCLENBQVo7QUFDQSxNQUFJUSxRQUFRLEdBQUcsSUFBZjtBQUNBLE1BQUkyQyxJQUFJLEdBQUkzQyxRQUFRLEdBQUc0QyxNQUFNLENBQUNDLFVBQTlCO0FBRUEzRCxFQUFBQSxDQUFDLENBQUNPLElBQUYsQ0FBT2lELEtBQVAsRUFBYyxVQUFTaEQsS0FBVCxFQUFnQkMsV0FBaEIsRUFBNkI7QUFFMUMsUUFBSU4sV0FBVyxDQUFDTyxLQUFoQixDQUFzQjtBQUNwQkMsTUFBQUEsY0FBYyxFQUFHLG9CQURHO0FBRXBCQyxNQUFBQSxXQUFXLEVBQUUsU0FGTztBQUdwQkcsTUFBQUEsTUFBTSxFQUFFLENBSFk7QUFJcEJELE1BQUFBLFFBQVEsRUFBRSxJQUpVO0FBS3BCRCxNQUFBQSxPQUFPLEVBQUU7QUFMVyxLQUF0QixFQVNDYyxRQVRELENBU1VKLFFBQVEsQ0FBQ0MsTUFBVCxDQUFnQmYsV0FBaEIsRUFBNkIsQ0FBN0IsRUFBZ0M7QUFBQ21ELE1BQUFBLEtBQUssRUFBRTtBQUFSLEtBQWhDLEVBQW1EO0FBQUNBLE1BQUFBLEtBQUssRUFBRSxFQUFSO0FBQVlsQyxNQUFBQSxlQUFlLEVBQUUsS0FBN0I7QUFBb0NlLE1BQUFBLElBQUksRUFBRW9CLE1BQU0sQ0FBQ0M7QUFBakQsS0FBbkQsQ0FUVixFQVdBO0FBWEEsS0FZQzdDLEtBWkQsQ0FZT2YsV0FaUDtBQWNELEdBaEJBO0FBbUJBOztBQUdBLE1BQUk2RCxRQUFRLEdBQUdqRSxRQUFRLENBQUNRLGdCQUFULENBQTBCLFVBQTFCLENBQWY7QUFDQU4sRUFBQUEsQ0FBQyxDQUFDTyxJQUFGLENBQU93RCxRQUFQLEVBQWlCLFVBQVNsQyxDQUFULEVBQVlzQixJQUFaLEVBQWtCO0FBR2pDLFFBQUloRCxXQUFXLENBQUNPLEtBQWhCLENBQXNCO0FBQ3BCQyxNQUFBQSxjQUFjLEVBQUUsSUFESTtBQUVwQkMsTUFBQUEsV0FBVyxFQUFFLFNBRk87QUFHcEJDLE1BQUFBLE9BQU8sRUFBRSxLQUhXO0FBSXBCRSxNQUFBQSxNQUFNLEVBQUUsQ0FKWTtBQUtwQkQsTUFBQUEsUUFBUSxFQUFFO0FBTFUsS0FBdEIsRUFTQ2EsUUFURCxDQVNVSixRQUFRLENBQUNDLE1BQVQsQ0FBZ0IyQixJQUFoQixFQUFzQixHQUF0QixFQUEyQjtBQUFDYSxNQUFBQSxDQUFDLEVBQUMsQ0FBQyxHQUFKO0FBQVNsQixNQUFBQSxPQUFPLEVBQUU7QUFBbEIsS0FBM0IsRUFBaUQ7QUFBQ2tCLE1BQUFBLENBQUMsRUFBQyxHQUFIO0FBQVFsQixNQUFBQSxPQUFPLEVBQUUsQ0FBakI7QUFBb0JMLE1BQUFBLElBQUksRUFBQ2EsT0FBTyxDQUFDUSxTQUFqQztBQUE0Q3BDLE1BQUFBLGVBQWUsRUFBRTtBQUE3RCxLQUFqRCxFQUFvSCxPQUFwSCxDQVRWLEVBVUNULEtBVkQsQ0FVT2YsV0FWUDtBQVlELEdBZkQ7QUFrQkE7O0FBRUEsTUFBSStELGdCQUFnQixHQUFHLENBQUMsVUFBRCxFQUFhLFlBQWIsQ0FBdkI7QUFFQWpFLEVBQUFBLENBQUMsQ0FBQ08sSUFBRixDQUFPMEQsZ0JBQVAsRUFBeUIsVUFBU3BDLENBQVQsRUFBWXNCLElBQVosRUFBa0I7QUFDekMsUUFBTWUsT0FBTyxHQUFHcEUsUUFBUSxDQUFDaUMsYUFBVCxDQUF1QixNQUFJb0IsSUFBM0IsQ0FBaEI7O0FBRUEsUUFBR2UsT0FBSCxFQUFZO0FBRVosVUFBTUMsS0FBSyxHQUFHRCxPQUFPLENBQUM1RCxnQkFBUixDQUF5QixNQUF6QixDQUFkO0FBR0FOLE1BQUFBLENBQUMsQ0FBQ08sSUFBRixDQUFPNEQsS0FBUCxFQUFjLFVBQVN0QyxDQUFULEVBQVlvQixPQUFaLEVBQXFCO0FBR2pDLFlBQU1tQixNQUFNLEdBQUcsRUFBZjs7QUFJQSxZQUFJakIsSUFBSSxJQUFJLFVBQVosRUFBdUI7QUFDckJpQixVQUFBQSxNQUFNLENBQUNDLE1BQVAsR0FBZ0IsQ0FBQyxFQUFqQjtBQUNBRCxVQUFBQSxNQUFNLENBQUNFLElBQVAsR0FBYyxDQUFDLEdBQWY7QUFDRCxTQUhELE1BSUs7QUFDSEYsVUFBQUEsTUFBTSxDQUFDQyxNQUFQLEdBQWdCLENBQUMsRUFBakI7QUFDQUQsVUFBQUEsTUFBTSxDQUFDRSxJQUFQLEdBQWMsR0FBZDtBQUVEOztBQU1ELFlBQUluRSxXQUFXLENBQUNPLEtBQWhCLENBQXNCO0FBQ2hCQyxVQUFBQSxjQUFjLEVBQUcsb0JBREQ7QUFFaEJDLFVBQUFBLFdBQVcsRUFBRyxTQUZFO0FBR2hCQyxVQUFBQSxPQUFPLEVBQUUsSUFITztBQUloQkUsVUFBQUEsTUFBTSxFQUFFLEVBSlE7QUFLaEJELFVBQUFBLFFBQVEsRUFBRTtBQUxNLFNBQXRCLEVBUUNhLFFBUkQsQ0FRV0osUUFBUSxDQUFDQyxNQUFULENBQWdCLElBQWhCLEVBQXNCLEdBQXRCLEVBQTJCO0FBQUV3QyxVQUFBQSxDQUFDLEVBQUVJLE1BQU0sQ0FBQ0M7QUFBWixTQUEzQixFQUFnRDtBQUN2RHZCLFVBQUFBLE9BQU8sRUFBRSxDQUQ4QztBQUV2RGtCLFVBQUFBLENBQUMsRUFBRUksTUFBTSxDQUFDRSxJQUY2QztBQUd2RDdCLFVBQUFBLElBQUksRUFBRThCLE1BQU0sQ0FBQ1Q7QUFIMEMsU0FBaEQsQ0FSWCxFQWFIO0FBYkcsU0FjRFUsYUFkQyxDQWNhO0FBQUVDLFVBQUFBLElBQUksRUFBRSx1QkFBc0JMLE1BQU0sQ0FBQ3JEO0FBQXJDLFNBZGIsRUFlREUsS0FmQyxDQWVLZixXQWZMO0FBbUJELE9BeENEO0FBMENMO0FBRUksR0FwREQ7QUEwREQ7O0FBSUQsU0FBU3FDLFdBQVQsQ0FBc0JtQyxHQUF0QixFQUEyQjtBQUN2QixNQUFJQyxVQUFVLEdBQUdELEdBQUcsQ0FBQyxDQUFELENBQUgsQ0FBT0UsY0FBUCxFQUFqQjtBQUNBRixFQUFBQSxHQUFHLENBQUNHLEdBQUosQ0FBUSxrQkFBUixFQUE0QkYsVUFBNUI7QUFDQUQsRUFBQUEsR0FBRyxDQUFDRyxHQUFKLENBQVEsbUJBQVIsRUFBNkJGLFVBQTdCO0FBQ0Q7Ozs7O0FDM1VMOzs7OztBQUtBLENBQUMsVUFBU0csQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxzQkFBaUJDLE9BQWpCLHlDQUFpQkEsT0FBakIsTUFBMEIsZUFBYSxPQUFPQyxNQUE5QyxHQUFxREEsTUFBTSxDQUFDRCxPQUFQLEdBQWVELENBQUMsQ0FBQ0csT0FBTyxDQUFDLFdBQUQsQ0FBUixDQUFyRSxHQUE0RixjQUFZLE9BQU9DLE1BQW5CLElBQTJCQSxNQUFNLENBQUNDLEdBQWxDLEdBQXNDRCxNQUFNLENBQUMsQ0FBQyxXQUFELENBQUQsRUFBZUosQ0FBZixDQUE1QyxHQUE4RCxDQUFDRCxDQUFDLEdBQUNBLENBQUMsSUFBRU8sSUFBTixFQUFZQyxTQUFaLEdBQXNCUCxDQUFDLENBQUNELENBQUMsQ0FBQ1MsTUFBSCxDQUFqTDtBQUE0TCxDQUExTSxTQUFpTixVQUFTVCxDQUFULEVBQVc7QUFBQzs7QUFBYSxXQUFTQyxDQUFULENBQVdELENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsU0FBSSxJQUFJUyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNULENBQUMsQ0FBQ1UsTUFBaEIsRUFBdUJELENBQUMsRUFBeEIsRUFBMkI7QUFBQyxVQUFJM0QsQ0FBQyxHQUFDa0QsQ0FBQyxDQUFDUyxDQUFELENBQVA7QUFBVzNELE1BQUFBLENBQUMsQ0FBQzZELFVBQUYsR0FBYTdELENBQUMsQ0FBQzZELFVBQUYsSUFBYyxDQUFDLENBQTVCLEVBQThCN0QsQ0FBQyxDQUFDOEQsWUFBRixHQUFlLENBQUMsQ0FBOUMsRUFBZ0QsV0FBVTlELENBQVYsS0FBY0EsQ0FBQyxDQUFDK0QsUUFBRixHQUFXLENBQUMsQ0FBMUIsQ0FBaEQsRUFBNkVDLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmhCLENBQXRCLEVBQXdCakQsQ0FBQyxDQUFDa0UsR0FBMUIsRUFBOEJsRSxDQUE5QixDQUE3RTtBQUE4RztBQUFDOztBQUFBLFdBQVMyRCxDQUFULENBQVdWLENBQVgsRUFBYVUsQ0FBYixFQUFlM0QsQ0FBZixFQUFpQjtBQUFDLFdBQU8yRCxDQUFDLElBQUVULENBQUMsQ0FBQ0QsQ0FBQyxDQUFDa0IsU0FBSCxFQUFhUixDQUFiLENBQUosRUFBb0IzRCxDQUFDLElBQUVrRCxDQUFDLENBQUNELENBQUQsRUFBR2pELENBQUgsQ0FBeEIsRUFBOEJpRCxDQUFyQztBQUF1Qzs7QUFBQSxXQUFTakQsQ0FBVCxDQUFXaUQsQ0FBWCxFQUFhQyxDQUFiLEVBQWVTLENBQWYsRUFBaUI7QUFBQyxXQUFPVCxDQUFDLElBQUlELENBQUwsR0FBT2UsTUFBTSxDQUFDQyxjQUFQLENBQXNCaEIsQ0FBdEIsRUFBd0JDLENBQXhCLEVBQTBCO0FBQUNrQixNQUFBQSxLQUFLLEVBQUNULENBQVA7QUFBU0UsTUFBQUEsVUFBVSxFQUFDLENBQUMsQ0FBckI7QUFBdUJDLE1BQUFBLFlBQVksRUFBQyxDQUFDLENBQXJDO0FBQXVDQyxNQUFBQSxRQUFRLEVBQUMsQ0FBQztBQUFqRCxLQUExQixDQUFQLEdBQXNGZCxDQUFDLENBQUNDLENBQUQsQ0FBRCxHQUFLUyxDQUEzRixFQUE2RlYsQ0FBcEc7QUFBc0c7O0FBQUEsV0FBU29CLENBQVQsQ0FBV3BCLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsUUFBSVMsQ0FBQyxHQUFDSyxNQUFNLENBQUNNLElBQVAsQ0FBWXJCLENBQVosQ0FBTjs7QUFBcUIsUUFBR2UsTUFBTSxDQUFDTyxxQkFBVixFQUFnQztBQUFDLFVBQUl2RSxDQUFDLEdBQUNnRSxNQUFNLENBQUNPLHFCQUFQLENBQTZCdEIsQ0FBN0IsQ0FBTjtBQUFzQ0MsTUFBQUEsQ0FBQyxLQUFHbEQsQ0FBQyxHQUFDQSxDQUFDLENBQUN3RSxNQUFGLENBQVUsVUFBU3RCLENBQVQsRUFBVztBQUFDLGVBQU9jLE1BQU0sQ0FBQ1Msd0JBQVAsQ0FBZ0N4QixDQUFoQyxFQUFrQ0MsQ0FBbEMsRUFBcUNXLFVBQTVDO0FBQXVELE9BQTdFLENBQUwsQ0FBRCxFQUF1RkYsQ0FBQyxDQUFDZSxJQUFGLENBQU9DLEtBQVAsQ0FBYWhCLENBQWIsRUFBZTNELENBQWYsQ0FBdkY7QUFBeUc7O0FBQUEsV0FBTzJELENBQVA7QUFBUzs7QUFBQSxXQUFTaUIsQ0FBVCxDQUFXM0IsQ0FBWCxFQUFhO0FBQUMsU0FBSSxJQUFJQyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUMyQixTQUFTLENBQUNqQixNQUF4QixFQUErQlYsQ0FBQyxFQUFoQyxFQUFtQztBQUFDLFVBQUlTLENBQUMsR0FBQyxRQUFNa0IsU0FBUyxDQUFDM0IsQ0FBRCxDQUFmLEdBQW1CMkIsU0FBUyxDQUFDM0IsQ0FBRCxDQUE1QixHQUFnQyxFQUF0QztBQUF5Q0EsTUFBQUEsQ0FBQyxHQUFDLENBQUYsR0FBSW1CLENBQUMsQ0FBQ1YsQ0FBRCxFQUFHLENBQUMsQ0FBSixDQUFELENBQVFtQixPQUFSLENBQWlCLFVBQVM1QixDQUFULEVBQVc7QUFBQ2xELFFBQUFBLENBQUMsQ0FBQ2lELENBQUQsRUFBR0MsQ0FBSCxFQUFLUyxDQUFDLENBQUNULENBQUQsQ0FBTixDQUFEO0FBQVksT0FBekMsQ0FBSixHQUFnRGMsTUFBTSxDQUFDZSx5QkFBUCxHQUFpQ2YsTUFBTSxDQUFDZ0IsZ0JBQVAsQ0FBd0IvQixDQUF4QixFQUEwQmUsTUFBTSxDQUFDZSx5QkFBUCxDQUFpQ3BCLENBQWpDLENBQTFCLENBQWpDLEdBQWdHVSxDQUFDLENBQUNWLENBQUQsQ0FBRCxDQUFLbUIsT0FBTCxDQUFjLFVBQVM1QixDQUFULEVBQVc7QUFBQ2MsUUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCaEIsQ0FBdEIsRUFBd0JDLENBQXhCLEVBQTBCYyxNQUFNLENBQUNTLHdCQUFQLENBQWdDZCxDQUFoQyxFQUFrQ1QsQ0FBbEMsQ0FBMUI7QUFBZ0UsT0FBMUYsQ0FBaEo7QUFBNk87O0FBQUEsV0FBT0QsQ0FBUDtBQUFTOztBQUFBQSxFQUFBQSxDQUFDLEdBQUNBLENBQUMsSUFBRUEsQ0FBQyxDQUFDZ0MsY0FBRixDQUFpQixTQUFqQixDQUFILEdBQStCaEMsQ0FBQyxXQUFoQyxHQUF5Q0EsQ0FBM0M7O0FBQTZDLE1BQUlpQyxDQUFKO0FBQUEsTUFBTUMsQ0FBTjtBQUFBLE1BQVFDLENBQVI7QUFBQSxNQUFVQyxDQUFDLEdBQUMsZUFBWjtBQUFBLE1BQTRCQyxDQUFDLEdBQUMsU0FBRkEsQ0FBRSxDQUFTckMsQ0FBVCxFQUFXO0FBQUMsT0FBRTtBQUFDQSxNQUFBQSxDQUFDLElBQUUsQ0FBQyxFQUFFLE1BQUlzQyxJQUFJLENBQUNDLE1BQUwsRUFBTixDQUFKO0FBQXlCLEtBQTVCLFFBQWtDdkgsUUFBUSxDQUFDd0gsY0FBVCxDQUF3QnhDLENBQXhCLENBQWxDOztBQUE4RCxXQUFPQSxDQUFQO0FBQVMsR0FBakg7QUFBQSxNQUFrSHlDLENBQUMsR0FBQyxTQUFGQSxDQUFFLENBQVN6QyxDQUFULEVBQVc7QUFBQyxRQUFJQyxDQUFDLEdBQUNELENBQUMsQ0FBQzBDLFlBQUYsQ0FBZSxhQUFmLENBQU47O0FBQW9DLFFBQUcsQ0FBQ3pDLENBQUQsSUFBSSxRQUFNQSxDQUFiLEVBQWU7QUFBQyxVQUFJUyxDQUFDLEdBQUNWLENBQUMsQ0FBQzBDLFlBQUYsQ0FBZSxNQUFmLENBQU47QUFBNkJ6QyxNQUFBQSxDQUFDLEdBQUNTLENBQUMsSUFBRSxRQUFNQSxDQUFULEdBQVdBLENBQUMsQ0FBQ2lDLElBQUYsRUFBWCxHQUFvQixJQUF0QjtBQUEyQjs7QUFBQSxXQUFPMUMsQ0FBUDtBQUFTLEdBQXJQO0FBQUEsTUFBc1AyQyxDQUFDLEdBQUMsU0FBRkEsQ0FBRSxDQUFTNUMsQ0FBVCxFQUFXO0FBQUMsUUFBSUMsQ0FBQyxHQUFDd0MsQ0FBQyxDQUFDekMsQ0FBRCxDQUFQO0FBQVcsV0FBT0MsQ0FBQyxJQUFFakYsUUFBUSxDQUFDaUMsYUFBVCxDQUF1QmdELENBQXZCLENBQUgsR0FBNkJBLENBQTdCLEdBQStCLElBQXRDO0FBQTJDLEdBQTFUO0FBQUEsTUFBMlQ0QyxDQUFDLEdBQUMsU0FBRkEsQ0FBRSxDQUFTN0MsQ0FBVCxFQUFXO0FBQUMsUUFBSUMsQ0FBQyxHQUFDd0MsQ0FBQyxDQUFDekMsQ0FBRCxDQUFQO0FBQVcsV0FBT0MsQ0FBQyxHQUFDakYsUUFBUSxDQUFDaUMsYUFBVCxDQUF1QmdELENBQXZCLENBQUQsR0FBMkIsSUFBbkM7QUFBd0MsR0FBNVg7QUFBQSxNQUE2WDZDLENBQUMsR0FBQyxTQUFGQSxDQUFFLENBQVM5QyxDQUFULEVBQVc7QUFBQyxRQUFHLENBQUNBLENBQUosRUFBTSxPQUFPLENBQVA7QUFBUyxRQUFJQyxDQUFDLEdBQUNyQixNQUFNLENBQUNtRSxnQkFBUCxDQUF3Qi9DLENBQXhCLENBQU47QUFBQSxRQUFpQ1UsQ0FBQyxHQUFDVCxDQUFDLENBQUMrQyxrQkFBckM7QUFBQSxRQUF3RGpHLENBQUMsR0FBQ2tELENBQUMsQ0FBQ2dELGVBQTVEO0FBQUEsUUFBNEU3QixDQUFDLEdBQUM4QixVQUFVLENBQUN4QyxDQUFELENBQXhGO0FBQUEsUUFBNEZpQixDQUFDLEdBQUN1QixVQUFVLENBQUNuRyxDQUFELENBQXhHO0FBQTRHLFdBQU9xRSxDQUFDLElBQUVPLENBQUgsSUFBTWpCLENBQUMsR0FBQ0EsQ0FBQyxDQUFDeUMsS0FBRixDQUFRLEdBQVIsRUFBYSxDQUFiLENBQUYsRUFBa0JwRyxDQUFDLEdBQUNBLENBQUMsQ0FBQ29HLEtBQUYsQ0FBUSxHQUFSLEVBQWEsQ0FBYixDQUFwQixFQUFvQyxPQUFLRCxVQUFVLENBQUN4QyxDQUFELENBQVYsR0FBY3dDLFVBQVUsQ0FBQ25HLENBQUQsQ0FBN0IsQ0FBMUMsSUFBNkUsQ0FBcEY7QUFBc0YsR0FBNWxCO0FBQUEsTUFBNmxCcUcsQ0FBQyxHQUFDLFNBQUZBLENBQUUsQ0FBU3BELENBQVQsRUFBVztBQUFDLFFBQUlDLENBQUMsR0FBQ2pGLFFBQVEsQ0FBQ3FJLFdBQVQsQ0FBcUIsWUFBckIsQ0FBTjtBQUF5Q3BELElBQUFBLENBQUMsQ0FBQ3FELFNBQUYsQ0FBWWxCLENBQVosRUFBYyxDQUFDLENBQWYsRUFBaUIsQ0FBQyxDQUFsQixHQUFxQnBDLENBQUMsQ0FBQ3VELGFBQUYsQ0FBZ0J0RCxDQUFoQixDQUFyQjtBQUF3QyxHQUE1ckI7QUFBQSxNQUE2ckJ1RCxDQUFDLEdBQUMsU0FBRkEsQ0FBRSxDQUFTeEQsQ0FBVCxFQUFXO0FBQUMsV0FBTSxDQUFDQSxDQUFDLENBQUMsQ0FBRCxDQUFELElBQU1BLENBQVAsRUFBVXlELFFBQWhCO0FBQXlCLEdBQXB1QjtBQUFBLE1BQXF1QkMsQ0FBQyxHQUFDLFNBQUZBLENBQUUsQ0FBUzFELENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsUUFBSVMsQ0FBQyxHQUFDLENBQUMsQ0FBUDtBQUFBLFFBQVMzRCxDQUFDLEdBQUNrRCxDQUFDLEdBQUMsQ0FBYjtBQUFlRCxJQUFBQSxDQUFDLENBQUMyRCxnQkFBRixDQUFtQnZCLENBQW5CLEVBQXNCLFNBQVNuQyxDQUFULEdBQVk7QUFBQ1MsTUFBQUEsQ0FBQyxHQUFDLENBQUMsQ0FBSCxFQUFLVixDQUFDLENBQUM0RCxtQkFBRixDQUFzQnhCLENBQXRCLEVBQXdCbkMsQ0FBeEIsQ0FBTDtBQUFnQyxLQUFuRSxHQUFzRTRELFVBQVUsQ0FBRSxZQUFVO0FBQUNuRCxNQUFBQSxDQUFDLElBQUUwQyxDQUFDLENBQUNwRCxDQUFELENBQUo7QUFBUSxLQUFyQixFQUF1QmpELENBQXZCLENBQWhGO0FBQTBHLEdBQTkyQjtBQUFBLE1BQSsyQitHLENBQUMsR0FBQyxTQUFGQSxDQUFFLENBQVM5RCxDQUFULEVBQVdDLENBQVgsRUFBYVMsQ0FBYixFQUFlO0FBQUNLLElBQUFBLE1BQU0sQ0FBQ00sSUFBUCxDQUFZWCxDQUFaLEVBQWVtQixPQUFmLENBQXdCLFVBQVM5RSxDQUFULEVBQVc7QUFBQyxVQUFJcUUsQ0FBSjtBQUFBLFVBQU1PLENBQUMsR0FBQ2pCLENBQUMsQ0FBQzNELENBQUQsQ0FBVDtBQUFBLFVBQWFrRixDQUFDLEdBQUNoQyxDQUFDLENBQUNsRCxDQUFELENBQWhCO0FBQUEsVUFBb0JtRixDQUFDLEdBQUNELENBQUMsSUFBRXVCLENBQUMsQ0FBQ3ZCLENBQUQsQ0FBSixHQUFRLFNBQVIsSUFBbUJiLENBQUMsR0FBQ2EsQ0FBRixFQUFJLEdBQUc4QixRQUFILENBQVlDLElBQVosQ0FBaUI1QyxDQUFqQixFQUFvQjZDLEtBQXBCLENBQTBCLGFBQTFCLEVBQXlDLENBQXpDLEVBQTRDQyxXQUE1QyxFQUF2QixDQUF0QjtBQUF3RyxVQUFHLENBQUMsSUFBSUMsTUFBSixDQUFXeEMsQ0FBWCxFQUFjeUMsSUFBZCxDQUFtQmxDLENBQW5CLENBQUosRUFBMEIsTUFBTSxJQUFJbUMsS0FBSixDQUFVckUsQ0FBQyxDQUFDc0UsV0FBRixLQUFnQixZQUFoQixHQUE2QnZILENBQTdCLEdBQStCLG1CQUEvQixHQUFtRG1GLENBQW5ELEdBQXFELHVCQUFyRCxHQUE2RVAsQ0FBN0UsR0FBK0UsSUFBekYsQ0FBTjtBQUFxRyxLQUEzUTtBQUE4USxHQUEvb0M7QUFBQSxNQUFncEM0QyxDQUFDLEdBQUMsU0FBRkEsQ0FBRSxDQUFTdkUsQ0FBVCxFQUFXO0FBQUMsV0FBT0EsQ0FBQyxHQUFDLEdBQUd3RSxLQUFILENBQVNSLElBQVQsQ0FBY2hFLENBQWQsQ0FBRCxHQUFrQixFQUExQjtBQUE2QixHQUEzckM7QUFBQSxNQUE0ckNqQyxDQUFDLEdBQUMsU0FBRkEsQ0FBRSxDQUFTaUMsQ0FBVCxFQUFXO0FBQUMsUUFBRyxDQUFDQSxDQUFKLEVBQU0sT0FBTSxDQUFDLENBQVA7O0FBQVMsUUFBR0EsQ0FBQyxDQUFDeUUsS0FBRixJQUFTekUsQ0FBQyxDQUFDMEUsVUFBWCxJQUF1QjFFLENBQUMsQ0FBQzBFLFVBQUYsQ0FBYUQsS0FBdkMsRUFBNkM7QUFBQyxVQUFJeEUsQ0FBQyxHQUFDOEMsZ0JBQWdCLENBQUMvQyxDQUFELENBQXRCO0FBQUEsVUFBMEJVLENBQUMsR0FBQ3FDLGdCQUFnQixDQUFDL0MsQ0FBQyxDQUFDMEUsVUFBSCxDQUE1QztBQUEyRCxhQUFNLFdBQVN6RSxDQUFDLENBQUMwRSxPQUFYLElBQW9CLFdBQVNqRSxDQUFDLENBQUNpRSxPQUEvQixJQUF3QyxhQUFXMUUsQ0FBQyxDQUFDMkUsVUFBM0Q7QUFBc0U7O0FBQUEsV0FBTSxDQUFDLENBQVA7QUFBUyxHQUFqNUM7QUFBQSxNQUFrNUNDLENBQUMsR0FBQyxTQUFGQSxDQUFFLEdBQVU7QUFBQyxXQUFPLFlBQVUsQ0FBRSxDQUFuQjtBQUFvQixHQUFuN0M7QUFBQSxNQUFvN0NDLENBQUMsR0FBQyxTQUFGQSxDQUFFLENBQVM5RSxDQUFULEVBQVc7QUFBQyxXQUFPQSxDQUFDLENBQUMrRSxZQUFUO0FBQXNCLEdBQXg5QztBQUFBLE1BQXk5Q0MsQ0FBQyxHQUFDLFNBQUZBLENBQUUsR0FBVTtBQUFDLFFBQUloRixDQUFDLEdBQUNwQixNQUFNLENBQUM3RCxNQUFiO0FBQW9CLFdBQU9pRixDQUFDLElBQUUsQ0FBQ2hGLFFBQVEsQ0FBQ2lLLElBQVQsQ0FBY0MsWUFBZCxDQUEyQixnQkFBM0IsQ0FBSixHQUFpRGxGLENBQWpELEdBQW1ELElBQTFEO0FBQStELEdBQXpqRDtBQUFBLE1BQTBqRG1GLENBQUMsSUFBRWxELENBQUMsR0FBQyxFQUFGLEVBQUtDLENBQUMsR0FBQyxDQUFQLEVBQVM7QUFBQ2tELElBQUFBLEdBQUcsRUFBQyxhQUFTcEYsQ0FBVCxFQUFXQyxDQUFYLEVBQWFTLENBQWIsRUFBZTtBQUFDLHFCQUFhLE9BQU9WLENBQUMsQ0FBQ2lCLEdBQXRCLEtBQTRCakIsQ0FBQyxDQUFDaUIsR0FBRixHQUFNO0FBQUNBLFFBQUFBLEdBQUcsRUFBQ2hCLENBQUw7QUFBT29GLFFBQUFBLEVBQUUsRUFBQ25EO0FBQVYsT0FBTixFQUFtQkEsQ0FBQyxFQUFoRCxHQUFvREQsQ0FBQyxDQUFDakMsQ0FBQyxDQUFDaUIsR0FBRixDQUFNb0UsRUFBUCxDQUFELEdBQVkzRSxDQUFoRTtBQUFrRSxLQUF2RjtBQUF3RjRFLElBQUFBLEdBQUcsRUFBQyxhQUFTdEYsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxVQUFHLENBQUNELENBQUQsSUFBSSxlQUFhLE9BQU9BLENBQUMsQ0FBQ2lCLEdBQTdCLEVBQWlDLE9BQU8sSUFBUDtBQUFZLFVBQUlQLENBQUMsR0FBQ1YsQ0FBQyxDQUFDaUIsR0FBUjtBQUFZLGFBQU9QLENBQUMsQ0FBQ08sR0FBRixLQUFRaEIsQ0FBUixHQUFVZ0MsQ0FBQyxDQUFDdkIsQ0FBQyxDQUFDMkUsRUFBSCxDQUFYLEdBQWtCLElBQXpCO0FBQThCLEtBQWpNO0FBQWtNLGNBQU8saUJBQVNyRixDQUFULEVBQVdDLENBQVgsRUFBYTtBQUFDLFVBQUcsZUFBYSxPQUFPRCxDQUFDLENBQUNpQixHQUF6QixFQUE2QjtBQUFDLFlBQUlQLENBQUMsR0FBQ1YsQ0FBQyxDQUFDaUIsR0FBUjtBQUFZUCxRQUFBQSxDQUFDLENBQUNPLEdBQUYsS0FBUWhCLENBQVIsS0FBWSxPQUFPZ0MsQ0FBQyxDQUFDdkIsQ0FBQyxDQUFDMkUsRUFBSCxDQUFSLEVBQWUsT0FBT3JGLENBQUMsQ0FBQ2lCLEdBQXBDO0FBQXlDO0FBQUM7QUFBM1MsR0FBWCxDQUEzakQ7QUFBQSxNQUFvM0RzRSxDQUFDLEdBQUM7QUFBQ0MsSUFBQUEsT0FBTyxFQUFDLGlCQUFTeEYsQ0FBVCxFQUFXQyxDQUFYLEVBQWFTLENBQWIsRUFBZTtBQUFDeUUsTUFBQUEsQ0FBQyxDQUFDQyxHQUFGLENBQU1wRixDQUFOLEVBQVFDLENBQVIsRUFBVVMsQ0FBVjtBQUFhLEtBQXRDO0FBQXVDK0UsSUFBQUEsT0FBTyxFQUFDLGlCQUFTekYsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxhQUFPa0YsQ0FBQyxDQUFDRyxHQUFGLENBQU10RixDQUFOLEVBQVFDLENBQVIsQ0FBUDtBQUFrQixLQUEvRTtBQUFnRnlGLElBQUFBLFVBQVUsRUFBQyxvQkFBUzFGLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUNrRixNQUFBQSxDQUFDLFVBQUQsQ0FBU25GLENBQVQsRUFBV0MsQ0FBWDtBQUFjO0FBQXZILEdBQXQzRDtBQUFBLE1BQSsrRDBGLENBQUMsR0FBQ0MsT0FBTyxDQUFDMUUsU0FBei9EO0FBQUEsTUFBbWdFMkUsQ0FBQyxHQUFDRixDQUFDLENBQUNHLE9BQXZnRTtBQUFBLE1BQStnRUMsQ0FBQyxHQUFDSixDQUFDLENBQUNLLE9BQW5oRTtBQUFBLE1BQTJoRUMsQ0FBQyxHQUFDTCxPQUFPLENBQUMxRSxTQUFSLENBQWtCMUYsZ0JBQS9pRTtBQUFBLE1BQWdrRTBLLENBQUMsR0FBQ04sT0FBTyxDQUFDMUUsU0FBUixDQUFrQmpFLGFBQXBsRTtBQUFBLE1BQWttRWtKLENBQUMsR0FBQyxXQUFTbkcsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxXQUFPLElBQUltRyxXQUFKLENBQWdCcEcsQ0FBaEIsRUFBa0JDLENBQWxCLENBQVA7QUFBNEIsR0FBOW9FOztBQUErb0UsTUFBRyxjQUFZLE9BQU9yQixNQUFNLENBQUN3SCxXQUExQixLQUF3Q0QsQ0FBQyxHQUFDLFdBQVNuRyxDQUFULEVBQVdDLENBQVgsRUFBYTtBQUFDQSxJQUFBQSxDQUFDLEdBQUNBLENBQUMsSUFBRTtBQUFDb0csTUFBQUEsT0FBTyxFQUFDLENBQUMsQ0FBVjtBQUFZQyxNQUFBQSxVQUFVLEVBQUMsQ0FBQyxDQUF4QjtBQUEwQkMsTUFBQUEsTUFBTSxFQUFDO0FBQWpDLEtBQUw7QUFBNEMsUUFBSTdGLENBQUMsR0FBQzFGLFFBQVEsQ0FBQ3FJLFdBQVQsQ0FBcUIsYUFBckIsQ0FBTjtBQUEwQyxXQUFPM0MsQ0FBQyxDQUFDOEYsZUFBRixDQUFrQnhHLENBQWxCLEVBQW9CQyxDQUFDLENBQUNvRyxPQUF0QixFQUE4QnBHLENBQUMsQ0FBQ3FHLFVBQWhDLEVBQTJDckcsQ0FBQyxDQUFDc0csTUFBN0MsR0FBcUQ3RixDQUE1RDtBQUE4RCxHQUE1TSxHQUE4TSxFQUFFLENBQUN5QixDQUFDLEdBQUNuSCxRQUFRLENBQUNxSSxXQUFULENBQXFCLGFBQXJCLENBQUgsRUFBd0NDLFNBQXhDLENBQWtELFdBQWxELEVBQThELENBQUMsQ0FBL0QsRUFBaUUsQ0FBQyxDQUFsRSxHQUFxRW5CLENBQUMsQ0FBQ3NFLGNBQUYsRUFBckUsRUFBd0Z0RSxDQUFDLENBQUN1RSxnQkFBNUYsQ0FBak4sRUFBK1Q7QUFBQyxRQUFJQyxDQUFDLEdBQUNDLEtBQUssQ0FBQzFGLFNBQU4sQ0FBZ0J1RixjQUF0Qjs7QUFBcUNHLElBQUFBLEtBQUssQ0FBQzFGLFNBQU4sQ0FBZ0J1RixjQUFoQixHQUErQixZQUFVO0FBQUMsV0FBS0gsVUFBTCxLQUFrQkssQ0FBQyxDQUFDM0MsSUFBRixDQUFPLElBQVAsR0FBYWpELE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQixJQUF0QixFQUEyQixrQkFBM0IsRUFBOEM7QUFBQ3NFLFFBQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsaUJBQU0sQ0FBQyxDQUFQO0FBQVMsU0FBekI7QUFBMEJ6RSxRQUFBQSxZQUFZLEVBQUMsQ0FBQztBQUF4QyxPQUE5QyxDQUEvQjtBQUEwSCxLQUFwSztBQUFxSzs7QUFBQSxNQUFJZ0csQ0FBQyxHQUFDLFlBQVU7QUFBQyxRQUFJN0csQ0FBQyxHQUFDbUcsQ0FBQyxDQUFDLFdBQUQsRUFBYTtBQUFDRyxNQUFBQSxVQUFVLEVBQUMsQ0FBQztBQUFiLEtBQWIsQ0FBUDtBQUFBLFFBQXFDckcsQ0FBQyxHQUFDakYsUUFBUSxDQUFDOEwsYUFBVCxDQUF1QixLQUF2QixDQUF2QztBQUFxRSxXQUFPN0csQ0FBQyxDQUFDMEQsZ0JBQUYsQ0FBbUIsV0FBbkIsRUFBZ0MsWUFBVTtBQUFDLGFBQU8sSUFBUDtBQUFZLEtBQXZELEdBQTBEM0QsQ0FBQyxDQUFDeUcsY0FBRixFQUExRCxFQUE2RXhHLENBQUMsQ0FBQ3NELGFBQUYsQ0FBZ0J2RCxDQUFoQixDQUE3RSxFQUFnR0EsQ0FBQyxDQUFDMEcsZ0JBQXpHO0FBQTBILEdBQTFNLEVBQU47O0FBQW1OYixFQUFBQSxDQUFDLEtBQUdBLENBQUMsR0FBQ0QsT0FBTyxDQUFDMUUsU0FBUixDQUFrQjZGLGlCQUFsQixJQUFxQ25CLE9BQU8sQ0FBQzFFLFNBQVIsQ0FBa0I4RixxQkFBNUQsQ0FBRCxFQUFvRmpCLENBQUMsS0FBR0EsQ0FBQyxHQUFDLFdBQVMvRixDQUFULEVBQVc7QUFBQyxRQUFJQyxDQUFDLEdBQUMsSUFBTjs7QUFBVyxPQUFFO0FBQUMsVUFBRzRGLENBQUMsQ0FBQzdCLElBQUYsQ0FBTy9ELENBQVAsRUFBU0QsQ0FBVCxDQUFILEVBQWUsT0FBT0MsQ0FBUDtBQUFTQSxNQUFBQSxDQUFDLEdBQUNBLENBQUMsQ0FBQ2dILGFBQUYsSUFBaUJoSCxDQUFDLENBQUN5RSxVQUFyQjtBQUFnQyxLQUEzRCxRQUFpRSxTQUFPekUsQ0FBUCxJQUFVLE1BQUlBLENBQUMsQ0FBQ3dELFFBQWpGOztBQUEyRixXQUFPLElBQVA7QUFBWSxHQUFuSSxDQUFyRjtBQUEwTixNQUFJeUQsQ0FBQyxHQUFDLFVBQU47QUFBaUIsR0FBQyxZQUFVO0FBQUMsUUFBSWxILENBQUMsR0FBQ2hGLFFBQVEsQ0FBQzhMLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBTjs7QUFBb0MsUUFBRztBQUFDOUcsTUFBQUEsQ0FBQyxDQUFDeEUsZ0JBQUYsQ0FBbUIsVUFBbkI7QUFBK0IsS0FBbkMsQ0FBbUMsT0FBTXdFLENBQU4sRUFBUTtBQUFDLGFBQU0sQ0FBQyxDQUFQO0FBQVM7O0FBQUEsV0FBTSxDQUFDLENBQVA7QUFBUyxHQUE5RyxRQUFvSGlHLENBQUMsR0FBQyxXQUFTakcsQ0FBVCxFQUFXO0FBQUMsUUFBRyxDQUFDa0gsQ0FBQyxDQUFDOUMsSUFBRixDQUFPcEUsQ0FBUCxDQUFKLEVBQWMsT0FBTyxLQUFLeEUsZ0JBQUwsQ0FBc0J3RSxDQUF0QixDQUFQO0FBQWdDLFFBQUlDLENBQUMsR0FBQ2tILE9BQU8sQ0FBQyxLQUFLOUIsRUFBTixDQUFiO0FBQXVCcEYsSUFBQUEsQ0FBQyxLQUFHLEtBQUtvRixFQUFMLEdBQVFoRCxDQUFDLENBQUMsT0FBRCxDQUFaLENBQUQ7QUFBd0IsUUFBSTNCLENBQUMsR0FBQyxJQUFOOztBQUFXLFFBQUc7QUFBQ1YsTUFBQUEsQ0FBQyxHQUFDQSxDQUFDLENBQUNvSCxPQUFGLENBQVVGLENBQVYsRUFBWSxNQUFJLEtBQUs3QixFQUFyQixDQUFGLEVBQTJCM0UsQ0FBQyxHQUFDLEtBQUtsRixnQkFBTCxDQUFzQndFLENBQXRCLENBQTdCO0FBQXNELEtBQTFELFNBQWlFO0FBQUNDLE1BQUFBLENBQUMsSUFBRSxLQUFLb0gsZUFBTCxDQUFxQixJQUFyQixDQUFIO0FBQThCOztBQUFBLFdBQU8zRyxDQUFQO0FBQVMsR0FBL04sRUFBZ093RixDQUFDLEdBQUMsV0FBU2xHLENBQVQsRUFBVztBQUFDLFFBQUcsQ0FBQ2tILENBQUMsQ0FBQzlDLElBQUYsQ0FBT3BFLENBQVAsQ0FBSixFQUFjLE9BQU8sS0FBSy9DLGFBQUwsQ0FBbUIrQyxDQUFuQixDQUFQO0FBQTZCLFFBQUlDLENBQUMsR0FBQ2dHLENBQUMsQ0FBQ2pDLElBQUYsQ0FBTyxJQUFQLEVBQVloRSxDQUFaLENBQU47QUFBcUIsV0FBTSxlQUFhLE9BQU9DLENBQUMsQ0FBQyxDQUFELENBQXJCLEdBQXlCQSxDQUFDLENBQUMsQ0FBRCxDQUExQixHQUE4QixJQUFwQztBQUF5QyxHQUEzYztBQUE2YyxNQUFJcUgsQ0FBQyxHQUFDdEMsQ0FBQyxFQUFQO0FBQUEsTUFBVXVDLENBQUMsR0FBQyxvQkFBWjtBQUFBLE1BQWlDQyxDQUFDLEdBQUMsTUFBbkM7QUFBQSxNQUEwQ3RJLENBQUMsR0FBQyxNQUE1QztBQUFBLE1BQW1EdUksQ0FBQyxHQUFDLFFBQXJEO0FBQUEsTUFBOERDLENBQUMsR0FBQyxFQUFoRTtBQUFBLE1BQW1FQyxDQUFDLEdBQUMsQ0FBckU7QUFBQSxNQUF1RUMsQ0FBQyxHQUFDO0FBQUNDLElBQUFBLFVBQVUsRUFBQyxXQUFaO0FBQXdCQyxJQUFBQSxVQUFVLEVBQUM7QUFBbkMsR0FBekU7QUFBQSxNQUF3SEMsQ0FBQyxHQUFDLENBQUMsT0FBRCxFQUFTLFVBQVQsRUFBb0IsU0FBcEIsRUFBOEIsV0FBOUIsRUFBMEMsYUFBMUMsRUFBd0QsWUFBeEQsRUFBcUUsZ0JBQXJFLEVBQXNGLFdBQXRGLEVBQWtHLFVBQWxHLEVBQTZHLFdBQTdHLEVBQXlILGFBQXpILEVBQXVJLFdBQXZJLEVBQW1KLFNBQW5KLEVBQTZKLFVBQTdKLEVBQXdLLE9BQXhLLEVBQWdMLG1CQUFoTCxFQUFvTSxZQUFwTSxFQUFpTixXQUFqTixFQUE2TixVQUE3TixFQUF3TyxhQUF4TyxFQUFzUCxhQUF0UCxFQUFvUSxhQUFwUSxFQUFrUixXQUFsUixFQUE4UixjQUE5UixFQUE2UyxlQUE3UyxFQUE2VCxjQUE3VCxFQUE0VSxlQUE1VSxFQUE0VixZQUE1VixFQUF5VyxPQUF6VyxFQUFpWCxNQUFqWCxFQUF3WCxRQUF4WCxFQUFpWSxPQUFqWSxFQUF5WSxRQUF6WSxFQUFrWixRQUFsWixFQUEyWixTQUEzWixFQUFxYSxVQUFyYSxFQUFnYixNQUFoYixFQUF1YixRQUF2YixFQUFnYyxjQUFoYyxFQUErYyxRQUEvYyxFQUF3ZCxNQUF4ZCxFQUErZCxrQkFBL2QsRUFBa2Ysa0JBQWxmLEVBQXFnQixPQUFyZ0IsRUFBNmdCLE9BQTdnQixFQUFxaEIsUUFBcmhCLENBQTFIOztBQUF5cEIsV0FBU0MsQ0FBVCxDQUFXaEksQ0FBWCxFQUFhQyxDQUFiLEVBQWU7QUFBQyxXQUFPQSxDQUFDLElBQUVBLENBQUMsR0FBQyxJQUFGLEdBQU8wSCxDQUFDLEVBQVgsSUFBZTNILENBQUMsQ0FBQ2lJLFFBQWpCLElBQTJCTixDQUFDLEVBQW5DO0FBQXNDOztBQUFBLFdBQVNPLENBQVQsQ0FBV2xJLENBQVgsRUFBYTtBQUFDLFFBQUlDLENBQUMsR0FBQytILENBQUMsQ0FBQ2hJLENBQUQsQ0FBUDtBQUFXLFdBQU9BLENBQUMsQ0FBQ2lJLFFBQUYsR0FBV2hJLENBQVgsRUFBYXlILENBQUMsQ0FBQ3pILENBQUQsQ0FBRCxHQUFLeUgsQ0FBQyxDQUFDekgsQ0FBRCxDQUFELElBQU0sRUFBeEIsRUFBMkJ5SCxDQUFDLENBQUN6SCxDQUFELENBQW5DO0FBQXVDOztBQUFBLFdBQVNrSSxDQUFULENBQVduSSxDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLGFBQU9ELENBQUMsQ0FBQ29JLEtBQVQsSUFBZ0JsSixDQUFDLENBQUNrRixJQUFGLENBQU9wRSxDQUFDLENBQUNxSSxJQUFULENBQWhCLEtBQWlDckksQ0FBQyxDQUFDb0ksS0FBRixHQUFRLFNBQU9wSSxDQUFDLENBQUNzSSxRQUFULEdBQWtCdEksQ0FBQyxDQUFDdUksT0FBcEIsR0FBNEJ2SSxDQUFDLENBQUNzSSxRQUF2RSxHQUFpRnRJLENBQUMsQ0FBQ3dJLGNBQUYsR0FBaUJ2SSxDQUFsRztBQUFvRzs7QUFBQSxXQUFTd0ksQ0FBVCxDQUFXekksQ0FBWCxFQUFhQyxDQUFiLEVBQWVTLENBQWYsRUFBaUI7QUFBQyxTQUFLLENBQUwsS0FBU0EsQ0FBVCxLQUFhQSxDQUFDLEdBQUMsSUFBZjs7QUFBcUIsU0FBSSxJQUFJM0QsQ0FBQyxHQUFDZ0UsTUFBTSxDQUFDTSxJQUFQLENBQVlyQixDQUFaLENBQU4sRUFBcUJvQixDQUFDLEdBQUMsQ0FBdkIsRUFBeUJPLENBQUMsR0FBQzVFLENBQUMsQ0FBQzRELE1BQWpDLEVBQXdDUyxDQUFDLEdBQUNPLENBQTFDLEVBQTRDUCxDQUFDLEVBQTdDLEVBQWdEO0FBQUMsVUFBSWEsQ0FBQyxHQUFDakMsQ0FBQyxDQUFDakQsQ0FBQyxDQUFDcUUsQ0FBRCxDQUFGLENBQVA7QUFBYyxVQUFHYSxDQUFDLENBQUN5RyxlQUFGLEtBQW9CekksQ0FBcEIsSUFBdUJnQyxDQUFDLENBQUMwRyxrQkFBRixLQUF1QmpJLENBQWpELEVBQW1ELE9BQU91QixDQUFQO0FBQVM7O0FBQUEsV0FBTyxJQUFQO0FBQVk7O0FBQUEsV0FBUzJHLENBQVQsQ0FBVzVJLENBQVgsRUFBYUMsQ0FBYixFQUFlUyxDQUFmLEVBQWlCO0FBQUMsUUFBSTNELENBQUMsR0FBQyxZQUFVLE9BQU9rRCxDQUF2QjtBQUFBLFFBQXlCbUIsQ0FBQyxHQUFDckUsQ0FBQyxHQUFDMkQsQ0FBRCxHQUFHVCxDQUEvQjtBQUFBLFFBQWlDMEIsQ0FBQyxHQUFDM0IsQ0FBQyxDQUFDb0gsT0FBRixDQUFVSSxDQUFWLEVBQVksRUFBWixDQUFuQztBQUFBLFFBQW1EdkYsQ0FBQyxHQUFDMkYsQ0FBQyxDQUFDakcsQ0FBRCxDQUF0RDtBQUEwRCxXQUFPTSxDQUFDLEtBQUdOLENBQUMsR0FBQ00sQ0FBTCxDQUFELEVBQVM4RixDQUFDLENBQUNjLE9BQUYsQ0FBVWxILENBQVYsSUFBYSxDQUFDLENBQWQsS0FBa0JBLENBQUMsR0FBQzNCLENBQXBCLENBQVQsRUFBZ0MsQ0FBQ2pELENBQUQsRUFBR3FFLENBQUgsRUFBS08sQ0FBTCxDQUF2QztBQUErQzs7QUFBQSxXQUFTbUgsQ0FBVCxDQUFXOUksQ0FBWCxFQUFhQyxDQUFiLEVBQWVTLENBQWYsRUFBaUIzRCxDQUFqQixFQUFtQnFFLENBQW5CLEVBQXFCO0FBQUMsUUFBRyxZQUFVLE9BQU9uQixDQUFqQixJQUFvQkQsQ0FBdkIsRUFBeUI7QUFBQ1UsTUFBQUEsQ0FBQyxLQUFHQSxDQUFDLEdBQUMzRCxDQUFGLEVBQUlBLENBQUMsR0FBQyxJQUFULENBQUQ7QUFBZ0IsVUFBSTRFLENBQUMsR0FBQ2lILENBQUMsQ0FBQzNJLENBQUQsRUFBR1MsQ0FBSCxFQUFLM0QsQ0FBTCxDQUFQO0FBQUEsVUFBZWtGLENBQUMsR0FBQ04sQ0FBQyxDQUFDLENBQUQsQ0FBbEI7QUFBQSxVQUFzQk8sQ0FBQyxHQUFDUCxDQUFDLENBQUMsQ0FBRCxDQUF6QjtBQUFBLFVBQTZCUSxDQUFDLEdBQUNSLENBQUMsQ0FBQyxDQUFELENBQWhDO0FBQUEsVUFBb0NTLENBQUMsR0FBQzhGLENBQUMsQ0FBQ2xJLENBQUQsQ0FBdkM7QUFBQSxVQUEyQ3FDLENBQUMsR0FBQ0QsQ0FBQyxDQUFDRCxDQUFELENBQUQsS0FBT0MsQ0FBQyxDQUFDRCxDQUFELENBQUQsR0FBSyxFQUFaLENBQTdDO0FBQUEsVUFBNkRNLENBQUMsR0FBQ2dHLENBQUMsQ0FBQ3BHLENBQUQsRUFBR0gsQ0FBSCxFQUFLRCxDQUFDLEdBQUN2QixDQUFELEdBQUcsSUFBVCxDQUFoRTtBQUErRSxVQUFHK0IsQ0FBSCxFQUFLQSxDQUFDLENBQUNzRyxNQUFGLEdBQVN0RyxDQUFDLENBQUNzRyxNQUFGLElBQVUzSCxDQUFuQixDQUFMLEtBQThCO0FBQUMsWUFBSXdCLENBQUMsR0FBQ29GLENBQUMsQ0FBQzlGLENBQUQsRUFBR2pDLENBQUMsQ0FBQ21ILE9BQUYsQ0FBVUcsQ0FBVixFQUFZLEVBQVosQ0FBSCxDQUFQO0FBQUEsWUFBMkIxRSxDQUFDLEdBQUNaLENBQUMsR0FBQyxVQUFTakMsQ0FBVCxFQUFXQyxDQUFYLEVBQWFTLENBQWIsRUFBZTtBQUFDLGlCQUFPLFNBQVMzRCxDQUFULENBQVdxRSxDQUFYLEVBQWE7QUFBQyxpQkFBSSxJQUFJTyxDQUFDLEdBQUMzQixDQUFDLENBQUN4RSxnQkFBRixDQUFtQnlFLENBQW5CLENBQU4sRUFBNEJnQyxDQUFDLEdBQUNiLENBQUMsQ0FBQzRILE1BQXBDLEVBQTJDL0csQ0FBQyxJQUFFQSxDQUFDLEtBQUcsSUFBbEQsRUFBdURBLENBQUMsR0FBQ0EsQ0FBQyxDQUFDeUMsVUFBM0Q7QUFBc0UsbUJBQUksSUFBSXhDLENBQUMsR0FBQ1AsQ0FBQyxDQUFDaEIsTUFBWixFQUFtQnVCLENBQUMsRUFBcEI7QUFBd0Isb0JBQUdQLENBQUMsQ0FBQ08sQ0FBRCxDQUFELEtBQU9ELENBQVYsRUFBWSxPQUFPa0csQ0FBQyxDQUFDL0csQ0FBRCxFQUFHYSxDQUFILENBQUQsRUFBT2xGLENBQUMsQ0FBQ2dNLE1BQUYsSUFBVUUsQ0FBQyxDQUFDQyxHQUFGLENBQU1sSixDQUFOLEVBQVFvQixDQUFDLENBQUNpSCxJQUFWLEVBQWUzSCxDQUFmLENBQWpCLEVBQW1DQSxDQUFDLENBQUNnQixLQUFGLENBQVFPLENBQVIsRUFBVSxDQUFDYixDQUFELENBQVYsQ0FBMUM7QUFBcEM7QUFBdEU7O0FBQW1LLG1CQUFPLElBQVA7QUFBWSxXQUFwTTtBQUFxTSxTQUFyTixDQUFzTnBCLENBQXROLEVBQXdOVSxDQUF4TixFQUEwTjNELENBQTFOLENBQUQsR0FBOE4sVUFBU2lELENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsaUJBQU8sU0FBU1MsQ0FBVCxDQUFXM0QsQ0FBWCxFQUFhO0FBQUMsbUJBQU9vTCxDQUFDLENBQUNwTCxDQUFELEVBQUdpRCxDQUFILENBQUQsRUFBT1UsQ0FBQyxDQUFDcUksTUFBRixJQUFVRSxDQUFDLENBQUNDLEdBQUYsQ0FBTWxKLENBQU4sRUFBUWpELENBQUMsQ0FBQ3NMLElBQVYsRUFBZXBJLENBQWYsQ0FBakIsRUFBbUNBLENBQUMsQ0FBQ3lCLEtBQUYsQ0FBUTFCLENBQVIsRUFBVSxDQUFDakQsQ0FBRCxDQUFWLENBQTFDO0FBQXlELFdBQTlFO0FBQStFLFNBQTdGLENBQThGaUQsQ0FBOUYsRUFBZ0dVLENBQWhHLENBQTVQO0FBQStWbUMsUUFBQUEsQ0FBQyxDQUFDOEYsa0JBQUYsR0FBcUIxRyxDQUFDLEdBQUN2QixDQUFELEdBQUcsSUFBekIsRUFBOEJtQyxDQUFDLENBQUM2RixlQUFGLEdBQWtCeEcsQ0FBaEQsRUFBa0RXLENBQUMsQ0FBQ2tHLE1BQUYsR0FBUzNILENBQTNELEVBQTZEeUIsQ0FBQyxDQUFDb0YsUUFBRixHQUFXckYsQ0FBeEUsRUFBMEVQLENBQUMsQ0FBQ08sQ0FBRCxDQUFELEdBQUtDLENBQS9FLEVBQWlGN0MsQ0FBQyxDQUFDMkQsZ0JBQUYsQ0FBbUJ4QixDQUFuQixFQUFxQlUsQ0FBckIsRUFBdUJaLENBQXZCLENBQWpGO0FBQTJHO0FBQUM7QUFBQzs7QUFBQSxXQUFTa0gsQ0FBVCxDQUFXbkosQ0FBWCxFQUFhQyxDQUFiLEVBQWVTLENBQWYsRUFBaUIzRCxDQUFqQixFQUFtQnFFLENBQW5CLEVBQXFCO0FBQUMsUUFBSU8sQ0FBQyxHQUFDOEcsQ0FBQyxDQUFDeEksQ0FBQyxDQUFDUyxDQUFELENBQUYsRUFBTTNELENBQU4sRUFBUXFFLENBQVIsQ0FBUDtBQUFrQk8sSUFBQUEsQ0FBQyxLQUFHM0IsQ0FBQyxDQUFDNEQsbUJBQUYsQ0FBc0JsRCxDQUF0QixFQUF3QmlCLENBQXhCLEVBQTBCd0YsT0FBTyxDQUFDL0YsQ0FBRCxDQUFqQyxHQUFzQyxPQUFPbkIsQ0FBQyxDQUFDUyxDQUFELENBQUQsQ0FBS2lCLENBQUMsQ0FBQ3NHLFFBQVAsQ0FBaEQsQ0FBRDtBQUFtRTs7QUFBQSxNQUFJZ0IsQ0FBQyxHQUFDO0FBQUNHLElBQUFBLEVBQUUsRUFBQyxZQUFTcEosQ0FBVCxFQUFXQyxDQUFYLEVBQWFTLENBQWIsRUFBZTNELENBQWYsRUFBaUI7QUFBQytMLE1BQUFBLENBQUMsQ0FBQzlJLENBQUQsRUFBR0MsQ0FBSCxFQUFLUyxDQUFMLEVBQU8zRCxDQUFQLEVBQVMsQ0FBQyxDQUFWLENBQUQ7QUFBYyxLQUFwQztBQUFxQ3NNLElBQUFBLEdBQUcsRUFBQyxhQUFTckosQ0FBVCxFQUFXQyxDQUFYLEVBQWFTLENBQWIsRUFBZTNELENBQWYsRUFBaUI7QUFBQytMLE1BQUFBLENBQUMsQ0FBQzlJLENBQUQsRUFBR0MsQ0FBSCxFQUFLUyxDQUFMLEVBQU8zRCxDQUFQLEVBQVMsQ0FBQyxDQUFWLENBQUQ7QUFBYyxLQUF6RTtBQUEwRW1NLElBQUFBLEdBQUcsRUFBQyxhQUFTbEosQ0FBVCxFQUFXQyxDQUFYLEVBQWFTLENBQWIsRUFBZTNELENBQWYsRUFBaUI7QUFBQyxVQUFHLFlBQVUsT0FBT2tELENBQWpCLElBQW9CRCxDQUF2QixFQUF5QjtBQUFDLFlBQUlvQixDQUFDLEdBQUN3SCxDQUFDLENBQUMzSSxDQUFELEVBQUdTLENBQUgsRUFBSzNELENBQUwsQ0FBUDtBQUFBLFlBQWU0RSxDQUFDLEdBQUNQLENBQUMsQ0FBQyxDQUFELENBQWxCO0FBQUEsWUFBc0JhLENBQUMsR0FBQ2IsQ0FBQyxDQUFDLENBQUQsQ0FBekI7QUFBQSxZQUE2QmMsQ0FBQyxHQUFDZCxDQUFDLENBQUMsQ0FBRCxDQUFoQztBQUFBLFlBQW9DZSxDQUFDLEdBQUNELENBQUMsS0FBR2pDLENBQTFDO0FBQUEsWUFBNENtQyxDQUFDLEdBQUM4RixDQUFDLENBQUNsSSxDQUFELENBQS9DO0FBQUEsWUFBbURxQyxDQUFDLEdBQUMsUUFBTXBDLENBQUMsQ0FBQ3FKLE1BQUYsQ0FBUyxDQUFULENBQTNEOztBQUF1RSxZQUFHLGVBQWEsT0FBT3JILENBQXZCLEVBQXlCO0FBQUNJLFVBQUFBLENBQUMsSUFBRXRCLE1BQU0sQ0FBQ00sSUFBUCxDQUFZZSxDQUFaLEVBQWVQLE9BQWYsQ0FBd0IsVUFBU25CLENBQVQsRUFBVztBQUFDLGFBQUMsVUFBU1YsQ0FBVCxFQUFXQyxDQUFYLEVBQWFTLENBQWIsRUFBZTNELENBQWYsRUFBaUI7QUFBQyxrQkFBSXFFLENBQUMsR0FBQ25CLENBQUMsQ0FBQ1MsQ0FBRCxDQUFELElBQU0sRUFBWjtBQUFlSyxjQUFBQSxNQUFNLENBQUNNLElBQVAsQ0FBWUQsQ0FBWixFQUFlUyxPQUFmLENBQXdCLFVBQVNGLENBQVQsRUFBVztBQUFDLG9CQUFHQSxDQUFDLENBQUNrSCxPQUFGLENBQVU5TCxDQUFWLElBQWEsQ0FBQyxDQUFqQixFQUFtQjtBQUFDLHNCQUFJa0YsQ0FBQyxHQUFDYixDQUFDLENBQUNPLENBQUQsQ0FBUDtBQUFXd0gsa0JBQUFBLENBQUMsQ0FBQ25KLENBQUQsRUFBR0MsQ0FBSCxFQUFLUyxDQUFMLEVBQU91QixDQUFDLENBQUN5RyxlQUFULEVBQXlCekcsQ0FBQyxDQUFDMEcsa0JBQTNCLENBQUQ7QUFBZ0Q7QUFBQyxlQUFwSDtBQUF1SCxhQUF4SixDQUF5SjNJLENBQXpKLEVBQTJKb0MsQ0FBM0osRUFBNkoxQixDQUE3SixFQUErSlQsQ0FBQyxDQUFDdUUsS0FBRixDQUFRLENBQVIsQ0FBL0osQ0FBRDtBQUE0SyxXQUFoTixDQUFIO0FBQXNOLGNBQUkvQixDQUFDLEdBQUNMLENBQUMsQ0FBQ0YsQ0FBRCxDQUFELElBQU0sRUFBWjtBQUFlbkIsVUFBQUEsTUFBTSxDQUFDTSxJQUFQLENBQVlvQixDQUFaLEVBQWVaLE9BQWYsQ0FBd0IsVUFBU25CLENBQVQsRUFBVztBQUFDLGdCQUFJM0QsQ0FBQyxHQUFDMkQsQ0FBQyxDQUFDMEcsT0FBRixDQUFVSyxDQUFWLEVBQVksRUFBWixDQUFOOztBQUFzQixnQkFBRyxDQUFDdEYsQ0FBRCxJQUFJbEMsQ0FBQyxDQUFDNEksT0FBRixDQUFVOUwsQ0FBVixJQUFhLENBQUMsQ0FBckIsRUFBdUI7QUFBQyxrQkFBSXFFLENBQUMsR0FBQ3FCLENBQUMsQ0FBQy9CLENBQUQsQ0FBUDtBQUFXeUksY0FBQUEsQ0FBQyxDQUFDbkosQ0FBRCxFQUFHb0MsQ0FBSCxFQUFLRixDQUFMLEVBQU9kLENBQUMsQ0FBQ3NILGVBQVQsRUFBeUJ0SCxDQUFDLENBQUN1SCxrQkFBM0IsQ0FBRDtBQUFnRDtBQUFDLFdBQTlJO0FBQWlKLFNBQWhaLE1BQW9aO0FBQUMsY0FBRyxDQUFDdkcsQ0FBRCxJQUFJLENBQUNBLENBQUMsQ0FBQ0YsQ0FBRCxDQUFULEVBQWE7QUFBT2lILFVBQUFBLENBQUMsQ0FBQ25KLENBQUQsRUFBR29DLENBQUgsRUFBS0YsQ0FBTCxFQUFPRCxDQUFQLEVBQVNOLENBQUMsR0FBQ2pCLENBQUQsR0FBRyxJQUFiLENBQUQ7QUFBb0I7QUFBQztBQUFDLEtBQWhvQjtBQUFpb0I2SSxJQUFBQSxPQUFPLEVBQUMsaUJBQVN2SixDQUFULEVBQVdDLENBQVgsRUFBYVMsQ0FBYixFQUFlO0FBQUMsVUFBRyxZQUFVLE9BQU9ULENBQWpCLElBQW9CLENBQUNELENBQXhCLEVBQTBCLE9BQU8sSUFBUDtBQUFZLFVBQUlqRCxDQUFKO0FBQUEsVUFBTXFFLENBQUMsR0FBQ25CLENBQUMsQ0FBQ21ILE9BQUYsQ0FBVUksQ0FBVixFQUFZLEVBQVosQ0FBUjtBQUFBLFVBQXdCN0YsQ0FBQyxHQUFDMUIsQ0FBQyxLQUFHbUIsQ0FBOUI7QUFBQSxVQUFnQ2EsQ0FBQyxHQUFDOEYsQ0FBQyxDQUFDYyxPQUFGLENBQVV6SCxDQUFWLElBQWEsQ0FBQyxDQUFoRDtBQUFBLFVBQWtEYyxDQUFDLEdBQUMsQ0FBQyxDQUFyRDtBQUFBLFVBQXVEQyxDQUFDLEdBQUMsQ0FBQyxDQUExRDtBQUFBLFVBQTREQyxDQUFDLEdBQUMsQ0FBQyxDQUEvRDtBQUFBLFVBQWlFQyxDQUFDLEdBQUMsSUFBbkU7QUFBd0UsYUFBT1YsQ0FBQyxJQUFFMkYsQ0FBSCxLQUFPdkssQ0FBQyxHQUFDdUssQ0FBQyxDQUFDVixLQUFGLENBQVEzRyxDQUFSLEVBQVVTLENBQVYsQ0FBRixFQUFlNEcsQ0FBQyxDQUFDdEgsQ0FBRCxDQUFELENBQUt1SixPQUFMLENBQWF4TSxDQUFiLENBQWYsRUFBK0JtRixDQUFDLEdBQUMsQ0FBQ25GLENBQUMsQ0FBQ3lNLG9CQUFGLEVBQWxDLEVBQTJEckgsQ0FBQyxHQUFDLENBQUNwRixDQUFDLENBQUMwTSw2QkFBRixFQUE5RCxFQUFnR3JILENBQUMsR0FBQ3JGLENBQUMsQ0FBQzJNLGtCQUFGLEVBQXpHLEdBQWlJekgsQ0FBQyxHQUFDLENBQUNJLENBQUMsR0FBQ3JILFFBQVEsQ0FBQ3FJLFdBQVQsQ0FBcUIsWUFBckIsQ0FBSCxFQUF1Q0MsU0FBdkMsQ0FBaURsQyxDQUFqRCxFQUFtRGMsQ0FBbkQsRUFBcUQsQ0FBQyxDQUF0RCxDQUFELEdBQTBERyxDQUFDLEdBQUM4RCxDQUFDLENBQUNsRyxDQUFELEVBQUc7QUFBQ29HLFFBQUFBLE9BQU8sRUFBQ25FLENBQVQ7QUFBV29FLFFBQUFBLFVBQVUsRUFBQyxDQUFDO0FBQXZCLE9BQUgsQ0FBL0wsRUFBNk4sZUFBYSxPQUFPNUYsQ0FBcEIsSUFBdUJLLE1BQU0sQ0FBQ00sSUFBUCxDQUFZWCxDQUFaLEVBQWVtQixPQUFmLENBQXdCLFVBQVM3QixDQUFULEVBQVc7QUFBQ2UsUUFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCcUIsQ0FBdEIsRUFBd0JyQyxDQUF4QixFQUEwQjtBQUFDc0YsVUFBQUEsR0FBRyxFQUFDLGVBQVU7QUFBQyxtQkFBTzVFLENBQUMsQ0FBQ1YsQ0FBRCxDQUFSO0FBQVk7QUFBNUIsU0FBMUI7QUFBeUQsT0FBN0YsQ0FBcFAsRUFBb1ZvQyxDQUFDLEtBQUdDLENBQUMsQ0FBQ29FLGNBQUYsSUFBbUJJLENBQUMsSUFBRTlGLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQnFCLENBQXRCLEVBQXdCLGtCQUF4QixFQUEyQztBQUFDaUQsUUFBQUEsR0FBRyxFQUFDLGVBQVU7QUFBQyxpQkFBTSxDQUFDLENBQVA7QUFBUztBQUF6QixPQUEzQyxDQUF6QixDQUFyVixFQUFzYm5ELENBQUMsSUFBRW5DLENBQUMsQ0FBQ3VELGFBQUYsQ0FBZ0JsQixDQUFoQixDQUF6YixFQUE0Y0EsQ0FBQyxDQUFDcUUsZ0JBQUYsSUFBb0IsZUFBYSxPQUFPM0osQ0FBeEMsSUFBMkNBLENBQUMsQ0FBQzBKLGNBQUYsRUFBdmYsRUFBMGdCcEUsQ0FBamhCO0FBQW1oQjtBQUExeEMsR0FBTjtBQUFBLE1BQWt5Q25ILENBQUMsR0FBQztBQUFDNEssSUFBQUEsT0FBTyxFQUFDLGlCQUFTOUYsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxhQUFPNEYsQ0FBQyxDQUFDN0IsSUFBRixDQUFPaEUsQ0FBUCxFQUFTQyxDQUFULENBQVA7QUFBbUIsS0FBMUM7QUFBMkMwSixJQUFBQSxJQUFJLEVBQUMsY0FBUzNKLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsYUFBTyxLQUFLLENBQUwsS0FBU0EsQ0FBVCxLQUFhQSxDQUFDLEdBQUNqRixRQUFRLENBQUM0TyxlQUF4QixHQUF5QzNELENBQUMsQ0FBQ2pDLElBQUYsQ0FBTy9ELENBQVAsRUFBU0QsQ0FBVCxDQUFoRDtBQUE0RCxLQUExSDtBQUEySDZKLElBQUFBLE9BQU8sRUFBQyxpQkFBUzdKLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsYUFBTyxLQUFLLENBQUwsS0FBU0EsQ0FBVCxLQUFhQSxDQUFDLEdBQUNqRixRQUFRLENBQUM0TyxlQUF4QixHQUF5QzFELENBQUMsQ0FBQ2xDLElBQUYsQ0FBTy9ELENBQVAsRUFBU0QsQ0FBVCxDQUFoRDtBQUE0RCxLQUE3TTtBQUE4TThKLElBQUFBLFFBQVEsRUFBQyxrQkFBUzlKLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsVUFBSVMsQ0FBQyxHQUFDLElBQU47QUFBQSxVQUFXM0QsQ0FBQyxHQUFDd0gsQ0FBQyxDQUFDdkUsQ0FBQyxDQUFDOEosUUFBSCxDQUFkO0FBQTJCLGFBQU8vTSxDQUFDLENBQUN3RSxNQUFGLENBQVUsVUFBU3ZCLENBQVQsRUFBVztBQUFDLGVBQU9VLENBQUMsQ0FBQ29GLE9BQUYsQ0FBVTlGLENBQVYsRUFBWUMsQ0FBWixDQUFQO0FBQXNCLE9BQTVDLENBQVA7QUFBc0QsS0FBdFQ7QUFBdVQ4SixJQUFBQSxPQUFPLEVBQUMsaUJBQVMvSixDQUFULEVBQVdDLENBQVgsRUFBYTtBQUFDLFdBQUksSUFBSVMsQ0FBQyxHQUFDLEVBQU4sRUFBUzNELENBQUMsR0FBQ2lELENBQUMsQ0FBQzBFLFVBQWpCLEVBQTRCM0gsQ0FBQyxJQUFFQSxDQUFDLENBQUMwRyxRQUFGLEtBQWF1RyxJQUFJLENBQUNDLFlBQXJCLElBQW1DLE1BQUlsTixDQUFDLENBQUMwRyxRQUFyRTtBQUErRSxhQUFLcUMsT0FBTCxDQUFhL0ksQ0FBYixFQUFla0QsQ0FBZixLQUFtQlMsQ0FBQyxDQUFDZSxJQUFGLENBQU8xRSxDQUFQLENBQW5CLEVBQTZCQSxDQUFDLEdBQUNBLENBQUMsQ0FBQzJILFVBQWpDO0FBQS9FOztBQUEySCxhQUFPaEUsQ0FBUDtBQUFTLEtBQWpkO0FBQWtkc0YsSUFBQUEsT0FBTyxFQUFDLGlCQUFTaEcsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxhQUFPOEYsQ0FBQyxDQUFDL0IsSUFBRixDQUFPaEUsQ0FBUCxFQUFTQyxDQUFULENBQVA7QUFBbUIsS0FBM2Y7QUFBNGZpSyxJQUFBQSxJQUFJLEVBQUMsY0FBU2xLLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsV0FBSSxJQUFJUyxDQUFDLEdBQUMsRUFBTixFQUFTM0QsQ0FBQyxHQUFDaUQsQ0FBQyxDQUFDbUssZUFBakIsRUFBaUNwTixDQUFDLElBQUVBLENBQUMsQ0FBQzBHLFFBQUYsS0FBYXVHLElBQUksQ0FBQ0MsWUFBckIsSUFBbUMsTUFBSWxOLENBQUMsQ0FBQzBHLFFBQTFFO0FBQW9GLGFBQUtxQyxPQUFMLENBQWEvSSxDQUFiLEVBQWVrRCxDQUFmLEtBQW1CUyxDQUFDLENBQUNlLElBQUYsQ0FBTzFFLENBQVAsQ0FBbkIsRUFBNkJBLENBQUMsR0FBQ0EsQ0FBQyxDQUFDb04sZUFBakM7QUFBcEY7O0FBQXFJLGFBQU96SixDQUFQO0FBQVM7QUFBN3BCLEdBQXB5QztBQUFBLE1BQW04RDBKLENBQUMsR0FBQyxVQUFyOEQ7QUFBQSxNQUFnOURDLEVBQUUsR0FBQyxNQUFJRCxDQUF2OUQ7QUFBQSxNQUF5OURFLEVBQUUsR0FBQztBQUFDQyxJQUFBQSxLQUFLLEVBQUMsVUFBUUYsRUFBZjtBQUFrQkcsSUFBQUEsTUFBTSxFQUFDLFdBQVNILEVBQWxDO0FBQXFDSSxJQUFBQSxjQUFjLEVBQUMsVUFBUUosRUFBUixHQUFXO0FBQS9ELEdBQTU5RDtBQUFBLE1BQXdpRUssRUFBRSxHQUFDLE9BQTNpRTtBQUFBLE1BQW1qRUMsRUFBRSxHQUFDLE1BQXRqRTtBQUFBLE1BQTZqRUMsRUFBRSxHQUFDLE1BQWhrRTtBQUFBLE1BQXVrRUMsRUFBRSxHQUFDLFlBQVU7QUFBQyxhQUFTN0ssQ0FBVCxDQUFXQSxDQUFYLEVBQWE7QUFBQyxXQUFLOEssUUFBTCxHQUFjOUssQ0FBZCxFQUFnQixLQUFLOEssUUFBTCxJQUFldkYsQ0FBQyxDQUFDQyxPQUFGLENBQVV4RixDQUFWLEVBQVlvSyxDQUFaLEVBQWMsSUFBZCxDQUEvQjtBQUFtRDs7QUFBQSxRQUFJbkssQ0FBQyxHQUFDRCxDQUFDLENBQUNrQixTQUFSO0FBQWtCLFdBQU9qQixDQUFDLENBQUM4SyxLQUFGLEdBQVEsVUFBUy9LLENBQVQsRUFBVztBQUFDLFVBQUlDLENBQUMsR0FBQyxLQUFLNkssUUFBWDtBQUFvQjlLLE1BQUFBLENBQUMsS0FBR0MsQ0FBQyxHQUFDLEtBQUsrSyxlQUFMLENBQXFCaEwsQ0FBckIsQ0FBTCxDQUFEOztBQUErQixVQUFJVSxDQUFDLEdBQUMsS0FBS3VLLGtCQUFMLENBQXdCaEwsQ0FBeEIsQ0FBTjs7QUFBaUMsZUFBT1MsQ0FBUCxJQUFVQSxDQUFDLENBQUNnRyxnQkFBWixJQUE4QixLQUFLd0UsY0FBTCxDQUFvQmpMLENBQXBCLENBQTlCO0FBQXFELEtBQTdKLEVBQThKQSxDQUFDLENBQUNrTCxPQUFGLEdBQVUsWUFBVTtBQUFDNUYsTUFBQUEsQ0FBQyxDQUFDRyxVQUFGLENBQWEsS0FBS29GLFFBQWxCLEVBQTJCVixDQUEzQixHQUE4QixLQUFLVSxRQUFMLEdBQWMsSUFBNUM7QUFBaUQsS0FBcE8sRUFBcU83SyxDQUFDLENBQUMrSyxlQUFGLEdBQWtCLFVBQVNoTCxDQUFULEVBQVc7QUFBQyxVQUFJQyxDQUFDLEdBQUM0QyxDQUFDLENBQUM3QyxDQUFELENBQVA7QUFBVyxhQUFPQyxDQUFDLEtBQUdBLENBQUMsR0FBQy9FLENBQUMsQ0FBQzhLLE9BQUYsQ0FBVWhHLENBQVYsRUFBWSxNQUFJMEssRUFBaEIsQ0FBTCxDQUFELEVBQTJCekssQ0FBbEM7QUFBb0MsS0FBbFQsRUFBbVRBLENBQUMsQ0FBQ2dMLGtCQUFGLEdBQXFCLFVBQVNqTCxDQUFULEVBQVc7QUFBQyxhQUFPaUosQ0FBQyxDQUFDTSxPQUFGLENBQVV2SixDQUFWLEVBQVlzSyxFQUFFLENBQUNDLEtBQWYsQ0FBUDtBQUE2QixLQUFqWCxFQUFrWHRLLENBQUMsQ0FBQ2lMLGNBQUYsR0FBaUIsVUFBU2xMLENBQVQsRUFBVztBQUFDLFVBQUlDLENBQUMsR0FBQyxJQUFOOztBQUFXLFVBQUdELENBQUMsQ0FBQ29MLFNBQUYsQ0FBWUMsTUFBWixDQUFtQlQsRUFBbkIsR0FBdUI1SyxDQUFDLENBQUNvTCxTQUFGLENBQVlFLFFBQVosQ0FBcUJYLEVBQXJCLENBQTFCLEVBQW1EO0FBQUMsWUFBSWpLLENBQUMsR0FBQ29DLENBQUMsQ0FBQzlDLENBQUQsQ0FBUDtBQUFXaUosUUFBQUEsQ0FBQyxDQUFDSSxHQUFGLENBQU1ySixDQUFOLEVBQVFvQyxDQUFSLEVBQVcsWUFBVTtBQUFDLGlCQUFPbkMsQ0FBQyxDQUFDc0wsZUFBRixDQUFrQnZMLENBQWxCLENBQVA7QUFBNEIsU0FBbEQsR0FBcUQwRCxDQUFDLENBQUMxRCxDQUFELEVBQUdVLENBQUgsQ0FBdEQ7QUFBNEQsT0FBM0gsTUFBZ0ksS0FBSzZLLGVBQUwsQ0FBcUJ2TCxDQUFyQjtBQUF3QixLQUFsakIsRUFBbWpCQyxDQUFDLENBQUNzTCxlQUFGLEdBQWtCLFVBQVN2TCxDQUFULEVBQVc7QUFBQ0EsTUFBQUEsQ0FBQyxDQUFDMEUsVUFBRixJQUFjMUUsQ0FBQyxDQUFDMEUsVUFBRixDQUFhOEcsV0FBYixDQUF5QnhMLENBQXpCLENBQWQsRUFBMENpSixDQUFDLENBQUNNLE9BQUYsQ0FBVXZKLENBQVYsRUFBWXNLLEVBQUUsQ0FBQ0UsTUFBZixDQUExQztBQUFpRSxLQUFscEIsRUFBbXBCeEssQ0FBQyxDQUFDeUwsZUFBRixHQUFrQixVQUFTeEwsQ0FBVCxFQUFXO0FBQUMsYUFBTyxLQUFLeEUsSUFBTCxDQUFXLFlBQVU7QUFBQyxZQUFJaUYsQ0FBQyxHQUFDNkUsQ0FBQyxDQUFDRSxPQUFGLENBQVUsSUFBVixFQUFlMkUsQ0FBZixDQUFOO0FBQXdCMUosUUFBQUEsQ0FBQyxLQUFHQSxDQUFDLEdBQUMsSUFBSVYsQ0FBSixDQUFNLElBQU4sQ0FBTCxDQUFELEVBQW1CLFlBQVVDLENBQVYsSUFBYVMsQ0FBQyxDQUFDVCxDQUFELENBQUQsQ0FBSyxJQUFMLENBQWhDO0FBQTJDLE9BQXpGLENBQVA7QUFBbUcsS0FBcHhCLEVBQXF4QkQsQ0FBQyxDQUFDMEwsYUFBRixHQUFnQixVQUFTMUwsQ0FBVCxFQUFXO0FBQUMsYUFBTyxVQUFTQyxDQUFULEVBQVc7QUFBQ0EsUUFBQUEsQ0FBQyxJQUFFQSxDQUFDLENBQUN3RyxjQUFGLEVBQUgsRUFBc0J6RyxDQUFDLENBQUMrSyxLQUFGLENBQVEsSUFBUixDQUF0QjtBQUFvQyxPQUF2RDtBQUF3RCxLQUF6MkIsRUFBMDJCL0ssQ0FBQyxDQUFDMkwsV0FBRixHQUFjLFVBQVMzTCxDQUFULEVBQVc7QUFBQyxhQUFPdUYsQ0FBQyxDQUFDRSxPQUFGLENBQVV6RixDQUFWLEVBQVlvSyxDQUFaLENBQVA7QUFBc0IsS0FBMTVCLEVBQTI1QjFKLENBQUMsQ0FBQ1YsQ0FBRCxFQUFHLElBQUgsRUFBUSxDQUFDO0FBQUNpQixNQUFBQSxHQUFHLEVBQUMsU0FBTDtBQUFlcUUsTUFBQUEsR0FBRyxFQUFDLGVBQVU7QUFBQyxlQUFNLE9BQU47QUFBYztBQUE1QyxLQUFELENBQVIsQ0FBNTVCLEVBQXE5QnRGLENBQTU5QjtBQUE4OUIsR0FBNWpDLEVBQTFrRTs7QUFBeW9HaUosRUFBQUEsQ0FBQyxDQUFDRyxFQUFGLENBQUtwTyxRQUFMLEVBQWNzUCxFQUFFLENBQUNHLGNBQWpCLEVBQWdDLHdCQUFoQyxFQUF5REksRUFBRSxDQUFDYSxhQUFILENBQWlCLElBQUliLEVBQUosRUFBakIsQ0FBekQ7QUFBbUYsTUFBSWUsRUFBRSxHQUFDNUcsQ0FBQyxFQUFSOztBQUFXLE1BQUc0RyxFQUFILEVBQU07QUFBQyxRQUFJQyxFQUFFLEdBQUNELEVBQUUsQ0FBQ0UsRUFBSCxDQUFNQyxLQUFiO0FBQW1CSCxJQUFBQSxFQUFFLENBQUNFLEVBQUgsQ0FBTUMsS0FBTixHQUFZbEIsRUFBRSxDQUFDWSxlQUFmLEVBQStCRyxFQUFFLENBQUNFLEVBQUgsQ0FBTUMsS0FBTixDQUFZQyxXQUFaLEdBQXdCbkIsRUFBdkQsRUFBMERlLEVBQUUsQ0FBQ0UsRUFBSCxDQUFNQyxLQUFOLENBQVlFLFVBQVosR0FBdUIsWUFBVTtBQUFDLGFBQU9MLEVBQUUsQ0FBQ0UsRUFBSCxDQUFNQyxLQUFOLEdBQVlGLEVBQVosRUFBZWhCLEVBQUUsQ0FBQ1ksZUFBekI7QUFBeUMsS0FBckk7QUFBc0k7O0FBQUEsTUFBSVMsRUFBRSxHQUFDLFdBQVA7QUFBQSxNQUFtQkMsRUFBRSxHQUFDLE1BQUlELEVBQTFCO0FBQUEsTUFBNkJFLEVBQUUsR0FBQyxRQUFoQztBQUFBLE1BQXlDQyxFQUFFLEdBQUMsS0FBNUM7QUFBQSxNQUFrREMsRUFBRSxHQUFDLE9BQXJEO0FBQUEsTUFBNkRDLEVBQUUsR0FBQyx5QkFBaEU7QUFBQSxNQUEwRkMsRUFBRSxHQUFDLHlCQUE3RjtBQUFBLE1BQXVIQyxFQUFFLEdBQUMsNEJBQTFIO0FBQUEsTUFBdUpDLEVBQUUsR0FBQyxTQUExSjtBQUFBLE1BQW9LQyxFQUFFLEdBQUMsTUFBdks7QUFBQSxNQUE4S0MsRUFBRSxHQUFDO0FBQUNuQyxJQUFBQSxjQUFjLEVBQUMsVUFBUTBCLEVBQVIsR0FBVyxXQUEzQjtBQUF1Q1UsSUFBQUEsY0FBYyxFQUFDLFVBQVFWLEVBQVIsR0FBVyxXQUFqRTtBQUE2RVcsSUFBQUEsYUFBYSxFQUFDLFNBQU9YLEVBQVAsR0FBVTtBQUFyRyxHQUFqTDtBQUFBLE1BQW1TWSxFQUFFLEdBQUMsWUFBVTtBQUFDLGFBQVMvTSxDQUFULENBQVdBLENBQVgsRUFBYTtBQUFDLFdBQUs4SyxRQUFMLEdBQWM5SyxDQUFkLEVBQWdCdUYsQ0FBQyxDQUFDQyxPQUFGLENBQVV4RixDQUFWLEVBQVlrTSxFQUFaLEVBQWUsSUFBZixDQUFoQjtBQUFxQzs7QUFBQSxRQUFJak0sQ0FBQyxHQUFDRCxDQUFDLENBQUNrQixTQUFSO0FBQWtCLFdBQU9qQixDQUFDLENBQUMrTSxNQUFGLEdBQVMsWUFBVTtBQUFDLFVBQUloTixDQUFDLEdBQUMsQ0FBQyxDQUFQO0FBQUEsVUFBU0MsQ0FBQyxHQUFDLENBQUMsQ0FBWjtBQUFBLFVBQWNTLENBQUMsR0FBQ3hGLENBQUMsQ0FBQzhLLE9BQUYsQ0FBVSxLQUFLOEUsUUFBZixFQUF3QjBCLEVBQXhCLENBQWhCOztBQUE0QyxVQUFHOUwsQ0FBSCxFQUFLO0FBQUMsWUFBSTNELENBQUMsR0FBQzdCLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVTRDLEVBQVYsRUFBYSxLQUFLM0IsUUFBbEIsQ0FBTjs7QUFBa0MsWUFBRy9OLENBQUMsSUFBRSxZQUFVQSxDQUFDLENBQUNzTCxJQUFsQixFQUF1QjtBQUFDLGNBQUd0TCxDQUFDLENBQUNrUSxPQUFGLElBQVcsS0FBS25DLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkUsUUFBeEIsQ0FBaUNjLEVBQWpDLENBQWQsRUFBbURwTSxDQUFDLEdBQUMsQ0FBQyxDQUFILENBQW5ELEtBQTREO0FBQUMsZ0JBQUlvQixDQUFDLEdBQUNsRyxDQUFDLENBQUMyTyxPQUFGLENBQVU2QyxFQUFWLEVBQWFoTSxDQUFiLENBQU47QUFBc0JVLFlBQUFBLENBQUMsSUFBRUEsQ0FBQyxDQUFDZ0ssU0FBRixDQUFZQyxNQUFaLENBQW1CZSxFQUFuQixDQUFIO0FBQTBCOztBQUFBLGNBQUdwTSxDQUFILEVBQUs7QUFBQyxnQkFBR2pELENBQUMsQ0FBQ21JLFlBQUYsQ0FBZSxVQUFmLEtBQTRCeEUsQ0FBQyxDQUFDd0UsWUFBRixDQUFlLFVBQWYsQ0FBNUIsSUFBd0RuSSxDQUFDLENBQUNxTyxTQUFGLENBQVlFLFFBQVosQ0FBcUIsVUFBckIsQ0FBeEQsSUFBMEY1SyxDQUFDLENBQUMwSyxTQUFGLENBQVlFLFFBQVosQ0FBcUIsVUFBckIsQ0FBN0YsRUFBOEg7QUFBT3ZPLFlBQUFBLENBQUMsQ0FBQ2tRLE9BQUYsR0FBVSxDQUFDLEtBQUtuQyxRQUFMLENBQWNNLFNBQWQsQ0FBd0JFLFFBQXhCLENBQWlDYyxFQUFqQyxDQUFYLEVBQWdEbkQsQ0FBQyxDQUFDTSxPQUFGLENBQVV4TSxDQUFWLEVBQVksUUFBWixDQUFoRDtBQUFzRTs7QUFBQUEsVUFBQUEsQ0FBQyxDQUFDbVEsS0FBRixJQUFVak4sQ0FBQyxHQUFDLENBQUMsQ0FBYjtBQUFlO0FBQUM7O0FBQUFBLE1BQUFBLENBQUMsSUFBRSxLQUFLNkssUUFBTCxDQUFjcUMsWUFBZCxDQUEyQixjQUEzQixFQUEwQyxDQUFDLEtBQUtyQyxRQUFMLENBQWNNLFNBQWQsQ0FBd0JFLFFBQXhCLENBQWlDYyxFQUFqQyxDQUEzQyxDQUFILEVBQW9GcE0sQ0FBQyxJQUFFLEtBQUs4SyxRQUFMLENBQWNNLFNBQWQsQ0FBd0I0QixNQUF4QixDQUErQlosRUFBL0IsQ0FBdkY7QUFBMEgsS0FBeGtCLEVBQXlrQm5NLENBQUMsQ0FBQ2tMLE9BQUYsR0FBVSxZQUFVO0FBQUM1RixNQUFBQSxDQUFDLENBQUNHLFVBQUYsQ0FBYSxLQUFLb0YsUUFBbEIsRUFBMkJvQixFQUEzQixHQUErQixLQUFLcEIsUUFBTCxHQUFjLElBQTdDO0FBQWtELEtBQWhwQixFQUFpcEI5SyxDQUFDLENBQUN5TCxlQUFGLEdBQWtCLFVBQVN4TCxDQUFULEVBQVc7QUFBQyxhQUFPLEtBQUt4RSxJQUFMLENBQVcsWUFBVTtBQUFDLFlBQUlpRixDQUFDLEdBQUM2RSxDQUFDLENBQUNFLE9BQUYsQ0FBVSxJQUFWLEVBQWV5RyxFQUFmLENBQU47QUFBeUJ4TCxRQUFBQSxDQUFDLEtBQUdBLENBQUMsR0FBQyxJQUFJVixDQUFKLENBQU0sSUFBTixDQUFMLENBQUQsRUFBbUIsYUFBV0MsQ0FBWCxJQUFjUyxDQUFDLENBQUNULENBQUQsQ0FBRCxFQUFqQztBQUF3QyxPQUF2RixDQUFQO0FBQWlHLEtBQWh4QixFQUFpeEJELENBQUMsQ0FBQzJMLFdBQUYsR0FBYyxVQUFTM0wsQ0FBVCxFQUFXO0FBQUMsYUFBT3VGLENBQUMsQ0FBQ0UsT0FBRixDQUFVekYsQ0FBVixFQUFZa00sRUFBWixDQUFQO0FBQXVCLEtBQWwwQixFQUFtMEJ4TCxDQUFDLENBQUNWLENBQUQsRUFBRyxJQUFILEVBQVEsQ0FBQztBQUFDaUIsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTSxPQUFOO0FBQWM7QUFBNUMsS0FBRCxDQUFSLENBQXAwQixFQUE2M0J0RixDQUFwNEI7QUFBczRCLEdBQXQ5QixFQUF0Uzs7QUFBK3ZDaUosRUFBQUEsQ0FBQyxDQUFDRyxFQUFGLENBQUtwTyxRQUFMLEVBQWM0UixFQUFFLENBQUNuQyxjQUFqQixFQUFnQzhCLEVBQWhDLEVBQW9DLFVBQVN2TSxDQUFULEVBQVc7QUFBQ0EsSUFBQUEsQ0FBQyxDQUFDeUcsY0FBRjtBQUFtQixRQUFJeEcsQ0FBQyxHQUFDRCxDQUFDLENBQUNnSixNQUFSO0FBQWUvSSxJQUFBQSxDQUFDLENBQUNtTCxTQUFGLENBQVlFLFFBQVosQ0FBcUJlLEVBQXJCLE1BQTJCcE0sQ0FBQyxHQUFDL0UsQ0FBQyxDQUFDOEssT0FBRixDQUFVL0YsQ0FBVixFQUFZME0sRUFBWixDQUE3QjtBQUE4QyxRQUFJak0sQ0FBQyxHQUFDNkUsQ0FBQyxDQUFDRSxPQUFGLENBQVV4RixDQUFWLEVBQVlpTSxFQUFaLENBQU47QUFBc0J4TCxJQUFBQSxDQUFDLEtBQUdBLENBQUMsR0FBQyxJQUFJcU0sRUFBSixDQUFPOU0sQ0FBUCxDQUFMLENBQUQsRUFBaUJTLENBQUMsQ0FBQ3NNLE1BQUYsRUFBakI7QUFBNEIsR0FBbEwsR0FBcUwvRCxDQUFDLENBQUNHLEVBQUYsQ0FBS3BPLFFBQUwsRUFBYzRSLEVBQUUsQ0FBQ0MsY0FBakIsRUFBZ0NOLEVBQWhDLEVBQW9DLFVBQVN2TSxDQUFULEVBQVc7QUFBQyxRQUFJQyxDQUFDLEdBQUMvRSxDQUFDLENBQUM4SyxPQUFGLENBQVVoRyxDQUFDLENBQUNnSixNQUFaLEVBQW1CMkQsRUFBbkIsQ0FBTjtBQUE2QjFNLElBQUFBLENBQUMsSUFBRUEsQ0FBQyxDQUFDbUwsU0FBRixDQUFZZ0MsR0FBWixDQUFnQmQsRUFBaEIsQ0FBSDtBQUF1QixHQUFwRyxDQUFyTCxFQUE0UnJELENBQUMsQ0FBQ0csRUFBRixDQUFLcE8sUUFBTCxFQUFjNFIsRUFBRSxDQUFDRSxhQUFqQixFQUErQlAsRUFBL0IsRUFBbUMsVUFBU3ZNLENBQVQsRUFBVztBQUFDLFFBQUlDLENBQUMsR0FBQy9FLENBQUMsQ0FBQzhLLE9BQUYsQ0FBVWhHLENBQUMsQ0FBQ2dKLE1BQVosRUFBbUIyRCxFQUFuQixDQUFOO0FBQTZCMU0sSUFBQUEsQ0FBQyxJQUFFQSxDQUFDLENBQUNtTCxTQUFGLENBQVlDLE1BQVosQ0FBbUJpQixFQUFuQixDQUFIO0FBQTBCLEdBQXRHLENBQTVSO0FBQXFZLE1BQUllLEVBQUUsR0FBQ3JJLENBQUMsRUFBUjs7QUFBVyxNQUFHcUksRUFBSCxFQUFNO0FBQUMsUUFBSUMsRUFBRSxHQUFDRCxFQUFFLENBQUN2QixFQUFILENBQU15QixNQUFiO0FBQW9CRixJQUFBQSxFQUFFLENBQUN2QixFQUFILENBQU15QixNQUFOLEdBQWFSLEVBQUUsQ0FBQ3RCLGVBQWhCLEVBQWdDNEIsRUFBRSxDQUFDdkIsRUFBSCxDQUFNeUIsTUFBTixDQUFhdkIsV0FBYixHQUF5QmUsRUFBekQsRUFBNERNLEVBQUUsQ0FBQ3ZCLEVBQUgsQ0FBTXlCLE1BQU4sQ0FBYXRCLFVBQWIsR0FBd0IsWUFBVTtBQUFDLGFBQU9vQixFQUFFLENBQUN2QixFQUFILENBQU15QixNQUFOLEdBQWFELEVBQWIsRUFBZ0JQLEVBQUUsQ0FBQ3RCLGVBQTFCO0FBQTBDLEtBQXpJO0FBQTBJOztBQUFBLFdBQVMrQixFQUFULENBQVl4TixDQUFaLEVBQWM7QUFBQyxXQUFNLFdBQVNBLENBQVQsSUFBWSxZQUFVQSxDQUFWLEtBQWNBLENBQUMsS0FBR3lOLE1BQU0sQ0FBQ3pOLENBQUQsQ0FBTixDQUFVK0QsUUFBVixFQUFKLEdBQXlCMEosTUFBTSxDQUFDek4sQ0FBRCxDQUEvQixHQUFtQyxPQUFLQSxDQUFMLElBQVEsV0FBU0EsQ0FBakIsR0FBbUIsSUFBbkIsR0FBd0JBLENBQXpFLENBQWxCO0FBQThGOztBQUFBLFdBQVMwTixFQUFULENBQVkxTixDQUFaLEVBQWM7QUFBQyxXQUFPQSxDQUFDLENBQUNvSCxPQUFGLENBQVUsUUFBVixFQUFvQixVQUFTcEgsQ0FBVCxFQUFXO0FBQUMsYUFBTSxNQUFJQSxDQUFDLENBQUNrRSxXQUFGLEVBQVY7QUFBMEIsS0FBMUQsQ0FBUDtBQUFvRTs7QUFBQSxNQUFJeUosRUFBRSxHQUFDO0FBQUNDLElBQUFBLGdCQUFnQixFQUFDLDBCQUFTNU4sQ0FBVCxFQUFXQyxDQUFYLEVBQWFTLENBQWIsRUFBZTtBQUFDVixNQUFBQSxDQUFDLENBQUNtTixZQUFGLENBQWUsVUFBUU8sRUFBRSxDQUFDek4sQ0FBRCxDQUF6QixFQUE2QlMsQ0FBN0I7QUFBZ0MsS0FBbEU7QUFBbUVtTixJQUFBQSxtQkFBbUIsRUFBQyw2QkFBUzdOLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUNELE1BQUFBLENBQUMsQ0FBQ3FILGVBQUYsQ0FBa0IsVUFBUXFHLEVBQUUsQ0FBQ3pOLENBQUQsQ0FBNUI7QUFBaUMsS0FBdEk7QUFBdUk2TixJQUFBQSxpQkFBaUIsRUFBQywyQkFBUzlOLENBQVQsRUFBVztBQUFDLFVBQUcsQ0FBQ0EsQ0FBSixFQUFNLE9BQU0sRUFBTjtBQUFTLFVBQUlDLENBQUMsR0FBQzBCLENBQUMsQ0FBQyxFQUFELEVBQUkzQixDQUFDLENBQUMrTixPQUFOLENBQVA7QUFBc0IsYUFBT2hOLE1BQU0sQ0FBQ00sSUFBUCxDQUFZcEIsQ0FBWixFQUFlNEIsT0FBZixDQUF3QixVQUFTN0IsQ0FBVCxFQUFXO0FBQUNDLFFBQUFBLENBQUMsQ0FBQ0QsQ0FBRCxDQUFELEdBQUt3TixFQUFFLENBQUN2TixDQUFDLENBQUNELENBQUQsQ0FBRixDQUFQO0FBQWMsT0FBbEQsR0FBcURDLENBQTVEO0FBQThELEtBQXhRO0FBQXlRK04sSUFBQUEsZ0JBQWdCLEVBQUMsMEJBQVNoTyxDQUFULEVBQVdDLENBQVgsRUFBYTtBQUFDLGFBQU91TixFQUFFLENBQUN4TixDQUFDLENBQUMwQyxZQUFGLENBQWUsVUFBUWdMLEVBQUUsQ0FBQ3pOLENBQUQsQ0FBekIsQ0FBRCxDQUFUO0FBQXlDLEtBQWpWO0FBQWtWaEUsSUFBQUEsTUFBTSxFQUFDLGdCQUFTK0QsQ0FBVCxFQUFXO0FBQUMsVUFBSUMsQ0FBQyxHQUFDRCxDQUFDLENBQUNpTyxxQkFBRixFQUFOO0FBQWdDLGFBQU07QUFBQ0MsUUFBQUEsR0FBRyxFQUFDak8sQ0FBQyxDQUFDaU8sR0FBRixHQUFNbFQsUUFBUSxDQUFDaUssSUFBVCxDQUFja0osU0FBekI7QUFBbUNDLFFBQUFBLElBQUksRUFBQ25PLENBQUMsQ0FBQ21PLElBQUYsR0FBT3BULFFBQVEsQ0FBQ2lLLElBQVQsQ0FBY29KO0FBQTdELE9BQU47QUFBK0UsS0FBcGQ7QUFBcWRDLElBQUFBLFFBQVEsRUFBQyxrQkFBU3RPLENBQVQsRUFBVztBQUFDLGFBQU07QUFBQ2tPLFFBQUFBLEdBQUcsRUFBQ2xPLENBQUMsQ0FBQ3VPLFNBQVA7QUFBaUJILFFBQUFBLElBQUksRUFBQ3BPLENBQUMsQ0FBQ3dPO0FBQXhCLE9BQU47QUFBMEMsS0FBcGhCO0FBQXFoQkMsSUFBQUEsV0FBVyxFQUFDLHFCQUFTek8sQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQ0QsTUFBQUEsQ0FBQyxLQUFHQSxDQUFDLENBQUNvTCxTQUFGLENBQVlFLFFBQVosQ0FBcUJyTCxDQUFyQixJQUF3QkQsQ0FBQyxDQUFDb0wsU0FBRixDQUFZQyxNQUFaLENBQW1CcEwsQ0FBbkIsQ0FBeEIsR0FBOENELENBQUMsQ0FBQ29MLFNBQUYsQ0FBWWdDLEdBQVosQ0FBZ0JuTixDQUFoQixDQUFqRCxDQUFEO0FBQXNFO0FBQXJuQixHQUFQO0FBQUEsTUFBOG5CeU8sRUFBRSxHQUFDLFVBQWpvQjtBQUFBLE1BQTRvQkMsRUFBRSxHQUFDLGFBQS9vQjtBQUFBLE1BQTZwQkMsRUFBRSxHQUFDLE1BQUlELEVBQXBxQjtBQUFBLE1BQXVxQkUsRUFBRSxHQUFDO0FBQUNDLElBQUFBLFFBQVEsRUFBQyxHQUFWO0FBQWNDLElBQUFBLFFBQVEsRUFBQyxDQUFDLENBQXhCO0FBQTBCQyxJQUFBQSxLQUFLLEVBQUMsQ0FBQyxDQUFqQztBQUFtQ0MsSUFBQUEsS0FBSyxFQUFDLE9BQXpDO0FBQWlEQyxJQUFBQSxJQUFJLEVBQUMsQ0FBQyxDQUF2RDtBQUF5REMsSUFBQUEsS0FBSyxFQUFDLENBQUM7QUFBaEUsR0FBMXFCO0FBQUEsTUFBNnVCQyxFQUFFLEdBQUM7QUFBQ04sSUFBQUEsUUFBUSxFQUFDLGtCQUFWO0FBQTZCQyxJQUFBQSxRQUFRLEVBQUMsU0FBdEM7QUFBZ0RDLElBQUFBLEtBQUssRUFBQyxrQkFBdEQ7QUFBeUVDLElBQUFBLEtBQUssRUFBQyxrQkFBL0U7QUFBa0dDLElBQUFBLElBQUksRUFBQyxTQUF2RztBQUFpSEMsSUFBQUEsS0FBSyxFQUFDO0FBQXZILEdBQWh2QjtBQUFBLE1BQWszQkUsRUFBRSxHQUFDLE1BQXIzQjtBQUFBLE1BQTQzQkMsRUFBRSxHQUFDLE1BQS8zQjtBQUFBLE1BQXM0QkMsRUFBRSxHQUFDLE1BQXo0QjtBQUFBLE1BQWc1QkMsRUFBRSxHQUFDLE9BQW41QjtBQUFBLE1BQTI1QkMsRUFBRSxHQUFDO0FBQUNDLElBQUFBLEtBQUssRUFBQyxVQUFRZCxFQUFmO0FBQWtCZSxJQUFBQSxJQUFJLEVBQUMsU0FBT2YsRUFBOUI7QUFBaUNnQixJQUFBQSxPQUFPLEVBQUMsWUFBVWhCLEVBQW5EO0FBQXNEaUIsSUFBQUEsVUFBVSxFQUFDLGVBQWFqQixFQUE5RTtBQUFpRmtCLElBQUFBLFVBQVUsRUFBQyxlQUFhbEIsRUFBekc7QUFBNEdtQixJQUFBQSxVQUFVLEVBQUMsZUFBYW5CLEVBQXBJO0FBQXVJb0IsSUFBQUEsU0FBUyxFQUFDLGNBQVlwQixFQUE3SjtBQUFnS3FCLElBQUFBLFFBQVEsRUFBQyxhQUFXckIsRUFBcEw7QUFBdUxzQixJQUFBQSxXQUFXLEVBQUMsZ0JBQWN0QixFQUFqTjtBQUFvTnVCLElBQUFBLFNBQVMsRUFBQyxjQUFZdkIsRUFBMU87QUFBNk93QixJQUFBQSxVQUFVLEVBQUMsY0FBWXhCLEVBQXBRO0FBQXVReUIsSUFBQUEsYUFBYSxFQUFDLFNBQU96QixFQUFQLEdBQVUsV0FBL1I7QUFBMlNuRSxJQUFBQSxjQUFjLEVBQUMsVUFBUW1FLEVBQVIsR0FBVztBQUFyVSxHQUE5NUI7QUFBQSxNQUFndkMwQixFQUFFLEdBQUMsVUFBbnZDO0FBQUEsTUFBOHZDQyxFQUFFLEdBQUMsUUFBandDO0FBQUEsTUFBMHdDQyxFQUFFLEdBQUMsT0FBN3dDO0FBQUEsTUFBcXhDQyxFQUFFLEdBQUMscUJBQXh4QztBQUFBLE1BQTh5Q0MsRUFBRSxHQUFDLG9CQUFqekM7QUFBQSxNQUFzMENDLEVBQUUsR0FBQyxvQkFBejBDO0FBQUEsTUFBODFDQyxFQUFFLEdBQUMsb0JBQWoyQztBQUFBLE1BQXMzQ0MsRUFBRSxHQUFDLGVBQXozQztBQUFBLE1BQXk0Q0MsRUFBRSxHQUFDO0FBQUNDLElBQUFBLE1BQU0sRUFBQyxTQUFSO0FBQWtCQyxJQUFBQSxXQUFXLEVBQUMsdUJBQTlCO0FBQXNEQyxJQUFBQSxJQUFJLEVBQUMsZ0JBQTNEO0FBQTRFQyxJQUFBQSxRQUFRLEVBQUMsb0JBQXJGO0FBQTBHQyxJQUFBQSxTQUFTLEVBQUMsMENBQXBIO0FBQStKQyxJQUFBQSxVQUFVLEVBQUMsc0JBQTFLO0FBQWlNQyxJQUFBQSxVQUFVLEVBQUMsK0JBQTVNO0FBQTRPQyxJQUFBQSxTQUFTLEVBQUM7QUFBdFAsR0FBNTRDO0FBQUEsTUFBNHBEQyxFQUFFLEdBQUM7QUFBQ0MsSUFBQUEsS0FBSyxFQUFDLE9BQVA7QUFBZUMsSUFBQUEsR0FBRyxFQUFDO0FBQW5CLEdBQS9wRDtBQUFBLE1BQXlyREMsRUFBRSxHQUFDLFlBQVU7QUFBQyxhQUFTMVIsQ0FBVCxDQUFXQSxDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLFdBQUswUixNQUFMLEdBQVksSUFBWixFQUFpQixLQUFLQyxTQUFMLEdBQWUsSUFBaEMsRUFBcUMsS0FBS0MsY0FBTCxHQUFvQixJQUF6RCxFQUE4RCxLQUFLQyxTQUFMLEdBQWUsQ0FBQyxDQUE5RSxFQUFnRixLQUFLQyxVQUFMLEdBQWdCLENBQUMsQ0FBakcsRUFBbUcsS0FBS0MsWUFBTCxHQUFrQixJQUFySCxFQUEwSCxLQUFLQyxXQUFMLEdBQWlCLENBQTNJLEVBQTZJLEtBQUtDLFdBQUwsR0FBaUIsQ0FBOUosRUFBZ0ssS0FBS0MsT0FBTCxHQUFhLEtBQUtDLFVBQUwsQ0FBZ0JuUyxDQUFoQixDQUE3SyxFQUFnTSxLQUFLNkssUUFBTCxHQUFjOUssQ0FBOU0sRUFBZ04sS0FBS3FTLGtCQUFMLEdBQXdCblgsQ0FBQyxDQUFDMk8sT0FBRixDQUFVaUgsRUFBRSxDQUFDTSxVQUFiLEVBQXdCLEtBQUt0RyxRQUE3QixDQUF4TyxFQUErUSxLQUFLd0gsZUFBTCxHQUFxQixrQkFBaUJ0WCxRQUFRLENBQUM0TyxlQUExQixJQUEyQzJJLFNBQVMsQ0FBQ0MsY0FBVixHQUF5QixDQUF4VyxFQUEwVyxLQUFLQyxhQUFMLEdBQW1CdEwsT0FBTyxDQUFDdkksTUFBTSxDQUFDOFQsWUFBUCxJQUFxQjlULE1BQU0sQ0FBQytULGNBQTdCLENBQXBZLEVBQWliLEtBQUtDLGtCQUFMLEVBQWpiLEVBQTJjck4sQ0FBQyxDQUFDQyxPQUFGLENBQVV4RixDQUFWLEVBQVkyTyxFQUFaLEVBQWUsSUFBZixDQUEzYztBQUFnZTs7QUFBQSxRQUFJMU8sQ0FBQyxHQUFDRCxDQUFDLENBQUNrQixTQUFSO0FBQWtCLFdBQU9qQixDQUFDLENBQUM0UyxJQUFGLEdBQU8sWUFBVTtBQUFDLFdBQUtkLFVBQUwsSUFBaUIsS0FBS2UsTUFBTCxDQUFZekQsRUFBWixDQUFqQjtBQUFpQyxLQUFuRCxFQUFvRHBQLENBQUMsQ0FBQzhTLGVBQUYsR0FBa0IsWUFBVTtBQUFDLE9BQUMvWCxRQUFRLENBQUNnWSxNQUFWLElBQWtCalYsQ0FBQyxDQUFDLEtBQUsrTSxRQUFOLENBQW5CLElBQW9DLEtBQUsrSCxJQUFMLEVBQXBDO0FBQWdELEtBQWpJLEVBQWtJNVMsQ0FBQyxDQUFDaUssSUFBRixHQUFPLFlBQVU7QUFBQyxXQUFLNkgsVUFBTCxJQUFpQixLQUFLZSxNQUFMLENBQVl4RCxFQUFaLENBQWpCO0FBQWlDLEtBQXJMLEVBQXNMclAsQ0FBQyxDQUFDZ1AsS0FBRixHQUFRLFVBQVNqUCxDQUFULEVBQVc7QUFBQ0EsTUFBQUEsQ0FBQyxLQUFHLEtBQUs4UixTQUFMLEdBQWUsQ0FBQyxDQUFuQixDQUFELEVBQXVCNVcsQ0FBQyxDQUFDMk8sT0FBRixDQUFVaUgsRUFBRSxDQUFDSyxTQUFiLEVBQXVCLEtBQUtyRyxRQUE1QixNQUF3QzFILENBQUMsQ0FBQyxLQUFLMEgsUUFBTixDQUFELEVBQWlCLEtBQUttSSxLQUFMLENBQVcsQ0FBQyxDQUFaLENBQXpELENBQXZCLEVBQWdHQyxhQUFhLENBQUMsS0FBS3RCLFNBQU4sQ0FBN0csRUFBOEgsS0FBS0EsU0FBTCxHQUFlLElBQTdJO0FBQWtKLEtBQTVWLEVBQTZWM1IsQ0FBQyxDQUFDZ1QsS0FBRixHQUFRLFVBQVNqVCxDQUFULEVBQVc7QUFBQ0EsTUFBQUEsQ0FBQyxLQUFHLEtBQUs4UixTQUFMLEdBQWUsQ0FBQyxDQUFuQixDQUFELEVBQXVCLEtBQUtGLFNBQUwsS0FBaUJzQixhQUFhLENBQUMsS0FBS3RCLFNBQU4sQ0FBYixFQUE4QixLQUFLQSxTQUFMLEdBQWUsSUFBOUQsQ0FBdkIsRUFBMkYsS0FBS08sT0FBTCxJQUFjLEtBQUtBLE9BQUwsQ0FBYXJELFFBQTNCLElBQXFDLENBQUMsS0FBS2dELFNBQTNDLEtBQXVELEtBQUtGLFNBQUwsR0FBZXVCLFdBQVcsQ0FBQyxDQUFDblksUUFBUSxDQUFDb1ksZUFBVCxHQUF5QixLQUFLTCxlQUE5QixHQUE4QyxLQUFLRixJQUFwRCxFQUEwRFEsSUFBMUQsQ0FBK0QsSUFBL0QsQ0FBRCxFQUFzRSxLQUFLbEIsT0FBTCxDQUFhckQsUUFBbkYsQ0FBakYsQ0FBM0Y7QUFBMFEsS0FBM25CLEVBQTRuQjdPLENBQUMsQ0FBQ3FULEVBQUYsR0FBSyxVQUFTdFQsQ0FBVCxFQUFXO0FBQUMsVUFBSUMsQ0FBQyxHQUFDLElBQU47QUFBVyxXQUFLNFIsY0FBTCxHQUFvQjNXLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVWlILEVBQUUsQ0FBQ0UsV0FBYixFQUF5QixLQUFLbEcsUUFBOUIsQ0FBcEI7O0FBQTRELFVBQUlwSyxDQUFDLEdBQUMsS0FBSzZTLGFBQUwsQ0FBbUIsS0FBSzFCLGNBQXhCLENBQU47O0FBQThDLFVBQUcsRUFBRTdSLENBQUMsR0FBQyxLQUFLMlIsTUFBTCxDQUFZaFIsTUFBWixHQUFtQixDQUFyQixJQUF3QlgsQ0FBQyxHQUFDLENBQTVCLENBQUgsRUFBa0MsSUFBRyxLQUFLK1IsVUFBUixFQUFtQjlJLENBQUMsQ0FBQ0ksR0FBRixDQUFNLEtBQUt5QixRQUFYLEVBQW9CMkUsRUFBRSxDQUFDRSxJQUF2QixFQUE2QixZQUFVO0FBQUMsZUFBTzFQLENBQUMsQ0FBQ3FULEVBQUYsQ0FBS3RULENBQUwsQ0FBUDtBQUFlLE9BQXZELEVBQW5CLEtBQWlGO0FBQUMsWUFBR1UsQ0FBQyxLQUFHVixDQUFQLEVBQVMsT0FBTyxLQUFLaVAsS0FBTCxJQUFhLEtBQUssS0FBS2dFLEtBQUwsRUFBekI7QUFBc0MsWUFBSWxXLENBQUMsR0FBQ2lELENBQUMsR0FBQ1UsQ0FBRixHQUFJMk8sRUFBSixHQUFPQyxFQUFiOztBQUFnQixhQUFLd0QsTUFBTCxDQUFZL1YsQ0FBWixFQUFjLEtBQUs0VSxNQUFMLENBQVkzUixDQUFaLENBQWQ7QUFBOEI7QUFBQyxLQUFwOUIsRUFBcTlCQyxDQUFDLENBQUNrTCxPQUFGLEdBQVUsWUFBVTtBQUFDbEMsTUFBQUEsQ0FBQyxDQUFDQyxHQUFGLENBQU0sS0FBSzRCLFFBQVgsRUFBb0I4RCxFQUFwQixHQUF3QnJKLENBQUMsQ0FBQ0csVUFBRixDQUFhLEtBQUtvRixRQUFsQixFQUEyQjZELEVBQTNCLENBQXhCLEVBQXVELEtBQUtnRCxNQUFMLEdBQVksSUFBbkUsRUFBd0UsS0FBS1EsT0FBTCxHQUFhLElBQXJGLEVBQTBGLEtBQUtySCxRQUFMLEdBQWMsSUFBeEcsRUFBNkcsS0FBSzhHLFNBQUwsR0FBZSxJQUE1SCxFQUFpSSxLQUFLRSxTQUFMLEdBQWUsSUFBaEosRUFBcUosS0FBS0MsVUFBTCxHQUFnQixJQUFySyxFQUEwSyxLQUFLRixjQUFMLEdBQW9CLElBQTlMLEVBQW1NLEtBQUtRLGtCQUFMLEdBQXdCLElBQTNOO0FBQWdPLEtBQTFzQyxFQUEyc0NwUyxDQUFDLENBQUNtUyxVQUFGLEdBQWEsVUFBU3BTLENBQVQsRUFBVztBQUFDLGFBQU9BLENBQUMsR0FBQzJCLENBQUMsQ0FBQyxFQUFELEVBQUlrTixFQUFKLEVBQU8sRUFBUCxFQUFVN08sQ0FBVixDQUFILEVBQWdCOEQsQ0FBQyxDQUFDNEssRUFBRCxFQUFJMU8sQ0FBSixFQUFNb1AsRUFBTixDQUFqQixFQUEyQnBQLENBQWxDO0FBQW9DLEtBQXh3QyxFQUF5d0NDLENBQUMsQ0FBQ3VULFlBQUYsR0FBZSxZQUFVO0FBQUMsVUFBSXhULENBQUMsR0FBQ3NDLElBQUksQ0FBQ21SLEdBQUwsQ0FBUyxLQUFLdkIsV0FBZCxDQUFOOztBQUFpQyxVQUFHLEVBQUVsUyxDQUFDLElBQUUsRUFBTCxDQUFILEVBQVk7QUFBQyxZQUFJQyxDQUFDLEdBQUNELENBQUMsR0FBQyxLQUFLa1MsV0FBYjtBQUF5QixhQUFLQSxXQUFMLEdBQWlCLENBQWpCLEVBQW1CalMsQ0FBQyxHQUFDLENBQUYsSUFBSyxLQUFLaUssSUFBTCxFQUF4QixFQUFvQ2pLLENBQUMsR0FBQyxDQUFGLElBQUssS0FBSzRTLElBQUwsRUFBekM7QUFBcUQ7QUFBQyxLQUFoNkMsRUFBaTZDNVMsQ0FBQyxDQUFDMlMsa0JBQUYsR0FBcUIsWUFBVTtBQUFDLFVBQUk1UyxDQUFDLEdBQUMsSUFBTjtBQUFXLFdBQUttUyxPQUFMLENBQWFwRCxRQUFiLElBQXVCOUYsQ0FBQyxDQUFDRyxFQUFGLENBQUssS0FBSzBCLFFBQVYsRUFBbUIyRSxFQUFFLENBQUNHLE9BQXRCLEVBQStCLFVBQVMzUCxDQUFULEVBQVc7QUFBQyxlQUFPRCxDQUFDLENBQUMwVCxRQUFGLENBQVd6VCxDQUFYLENBQVA7QUFBcUIsT0FBaEUsQ0FBdkIsRUFBMEYsWUFBVSxLQUFLa1MsT0FBTCxDQUFhbEQsS0FBdkIsS0FBK0JoRyxDQUFDLENBQUNHLEVBQUYsQ0FBSyxLQUFLMEIsUUFBVixFQUFtQjJFLEVBQUUsQ0FBQ0ksVUFBdEIsRUFBa0MsVUFBUzVQLENBQVQsRUFBVztBQUFDLGVBQU9ELENBQUMsQ0FBQ2lQLEtBQUYsQ0FBUWhQLENBQVIsQ0FBUDtBQUFrQixPQUFoRSxHQUFtRWdKLENBQUMsQ0FBQ0csRUFBRixDQUFLLEtBQUswQixRQUFWLEVBQW1CMkUsRUFBRSxDQUFDSyxVQUF0QixFQUFrQyxVQUFTN1AsQ0FBVCxFQUFXO0FBQUMsZUFBT0QsQ0FBQyxDQUFDaVQsS0FBRixDQUFRaFQsQ0FBUixDQUFQO0FBQWtCLE9BQWhFLENBQWxHLENBQTFGLEVBQWdRLEtBQUtrUyxPQUFMLENBQWFoRCxLQUFiLElBQW9CLEtBQUttRCxlQUF6QixJQUEwQyxLQUFLcUIsdUJBQUwsRUFBMVM7QUFBeVUsS0FBcnhELEVBQXN4RDFULENBQUMsQ0FBQzBULHVCQUFGLEdBQTBCLFlBQVU7QUFBQyxVQUFJM1QsQ0FBQyxHQUFDLElBQU47QUFBQSxVQUFXQyxDQUFDLEdBQUMsV0FBU0EsR0FBVCxFQUFXO0FBQUNELFFBQUFBLENBQUMsQ0FBQ3lTLGFBQUYsSUFBaUJsQixFQUFFLENBQUN0UixHQUFDLENBQUMyVCxXQUFGLENBQWN0UCxXQUFkLEVBQUQsQ0FBbkIsR0FBaUR0RSxDQUFDLENBQUNpUyxXQUFGLEdBQWNoUyxHQUFDLENBQUM0VCxPQUFqRSxHQUF5RTdULENBQUMsQ0FBQ3lTLGFBQUYsS0FBa0J6UyxDQUFDLENBQUNpUyxXQUFGLEdBQWNoUyxHQUFDLENBQUM2VCxPQUFGLENBQVUsQ0FBVixFQUFhRCxPQUE3QyxDQUF6RTtBQUErSCxPQUF4SjtBQUFBLFVBQXlKblQsQ0FBQyxHQUFDLFNBQUZBLENBQUUsQ0FBU1QsQ0FBVCxFQUFXO0FBQUNELFFBQUFBLENBQUMsQ0FBQ3lTLGFBQUYsSUFBaUJsQixFQUFFLENBQUN0UixDQUFDLENBQUMyVCxXQUFGLENBQWN0UCxXQUFkLEVBQUQsQ0FBbkIsS0FBbUR0RSxDQUFDLENBQUNrUyxXQUFGLEdBQWNqUyxDQUFDLENBQUM0VCxPQUFGLEdBQVU3VCxDQUFDLENBQUNpUyxXQUE3RSxHQUEwRmpTLENBQUMsQ0FBQ3dULFlBQUYsRUFBMUYsRUFBMkcsWUFBVXhULENBQUMsQ0FBQ21TLE9BQUYsQ0FBVWxELEtBQXBCLEtBQTRCalAsQ0FBQyxDQUFDaVAsS0FBRixJQUFValAsQ0FBQyxDQUFDZ1MsWUFBRixJQUFnQitCLFlBQVksQ0FBQy9ULENBQUMsQ0FBQ2dTLFlBQUgsQ0FBdEMsRUFBdURoUyxDQUFDLENBQUNnUyxZQUFGLEdBQWVuTyxVQUFVLENBQUUsVUFBUzVELENBQVQsRUFBVztBQUFDLGlCQUFPRCxDQUFDLENBQUNpVCxLQUFGLENBQVFoVCxDQUFSLENBQVA7QUFBa0IsU0FBaEMsRUFBa0MsTUFBSUQsQ0FBQyxDQUFDbVMsT0FBRixDQUFVckQsUUFBaEQsQ0FBNUcsQ0FBM0c7QUFBa1IsT0FBemI7O0FBQTBidkssTUFBQUEsQ0FBQyxDQUFDckosQ0FBQyxDQUFDeU8sSUFBRixDQUFPbUgsRUFBRSxDQUFDSSxRQUFWLEVBQW1CLEtBQUtwRyxRQUF4QixDQUFELENBQUQsQ0FBcUNqSixPQUFyQyxDQUE4QyxVQUFTN0IsQ0FBVCxFQUFXO0FBQUNpSixRQUFBQSxDQUFDLENBQUNHLEVBQUYsQ0FBS3BKLENBQUwsRUFBT3lQLEVBQUUsQ0FBQ1csVUFBVixFQUFzQixVQUFTcFEsQ0FBVCxFQUFXO0FBQUMsaUJBQU9BLENBQUMsQ0FBQ3lHLGNBQUYsRUFBUDtBQUEwQixTQUE1RDtBQUErRCxPQUF6SCxHQUE0SCxLQUFLZ00sYUFBTCxJQUFvQnhKLENBQUMsQ0FBQ0csRUFBRixDQUFLLEtBQUswQixRQUFWLEVBQW1CMkUsRUFBRSxDQUFDUyxXQUF0QixFQUFtQyxVQUFTbFEsQ0FBVCxFQUFXO0FBQUMsZUFBT0MsQ0FBQyxDQUFDRCxDQUFELENBQVI7QUFBWSxPQUEzRCxHQUE4RGlKLENBQUMsQ0FBQ0csRUFBRixDQUFLLEtBQUswQixRQUFWLEVBQW1CMkUsRUFBRSxDQUFDVSxTQUF0QixFQUFpQyxVQUFTblEsQ0FBVCxFQUFXO0FBQUMsZUFBT1UsQ0FBQyxDQUFDVixDQUFELENBQVI7QUFBWSxPQUF6RCxDQUE5RCxFQUEwSCxLQUFLOEssUUFBTCxDQUFjTSxTQUFkLENBQXdCZ0MsR0FBeEIsQ0FBNEJ5RCxFQUE1QixDQUE5SSxLQUFnTDVILENBQUMsQ0FBQ0csRUFBRixDQUFLLEtBQUswQixRQUFWLEVBQW1CMkUsRUFBRSxDQUFDTSxVQUF0QixFQUFrQyxVQUFTL1AsQ0FBVCxFQUFXO0FBQUMsZUFBT0MsQ0FBQyxDQUFDRCxDQUFELENBQVI7QUFBWSxPQUExRCxHQUE2RGlKLENBQUMsQ0FBQ0csRUFBRixDQUFLLEtBQUswQixRQUFWLEVBQW1CMkUsRUFBRSxDQUFDTyxTQUF0QixFQUFpQyxVQUFTL1AsQ0FBVCxFQUFXO0FBQUMsZUFBTyxVQUFTQSxDQUFULEVBQVc7QUFBQ0EsVUFBQUEsQ0FBQyxDQUFDNlQsT0FBRixJQUFXN1QsQ0FBQyxDQUFDNlQsT0FBRixDQUFVblQsTUFBVixHQUFpQixDQUE1QixHQUE4QlgsQ0FBQyxDQUFDa1MsV0FBRixHQUFjLENBQTVDLEdBQThDbFMsQ0FBQyxDQUFDa1MsV0FBRixHQUFjalMsQ0FBQyxDQUFDNlQsT0FBRixDQUFVLENBQVYsRUFBYUQsT0FBYixHQUFxQjdULENBQUMsQ0FBQ2lTLFdBQW5GO0FBQStGLFNBQTNHLENBQTRHaFMsQ0FBNUcsQ0FBUDtBQUFzSCxPQUFuSyxDQUE3RCxFQUFtT2dKLENBQUMsQ0FBQ0csRUFBRixDQUFLLEtBQUswQixRQUFWLEVBQW1CMkUsRUFBRSxDQUFDUSxRQUF0QixFQUFnQyxVQUFTalEsQ0FBVCxFQUFXO0FBQUMsZUFBT1UsQ0FBQyxDQUFDVixDQUFELENBQVI7QUFBWSxPQUF4RCxDQUFuWixDQUE1SDtBQUEya0IsS0FBaDBGLEVBQWkwRkMsQ0FBQyxDQUFDeVQsUUFBRixHQUFXLFVBQVMxVCxDQUFULEVBQVc7QUFBQyxVQUFHLENBQUMsa0JBQWtCb0UsSUFBbEIsQ0FBdUJwRSxDQUFDLENBQUNnSixNQUFGLENBQVNnTCxPQUFoQyxDQUFKLEVBQTZDLFFBQU9oVSxDQUFDLENBQUNvSSxLQUFUO0FBQWdCLGFBQUssRUFBTDtBQUFRcEksVUFBQUEsQ0FBQyxDQUFDeUcsY0FBRixJQUFtQixLQUFLeUQsSUFBTCxFQUFuQjtBQUErQjs7QUFBTSxhQUFLLEVBQUw7QUFBUWxLLFVBQUFBLENBQUMsQ0FBQ3lHLGNBQUYsSUFBbUIsS0FBS29NLElBQUwsRUFBbkI7QUFBckU7QUFBcUcsS0FBMStGLEVBQTIrRjVTLENBQUMsQ0FBQ3NULGFBQUYsR0FBZ0IsVUFBU3ZULENBQVQsRUFBVztBQUFDLGFBQU8sS0FBSzJSLE1BQUwsR0FBWTNSLENBQUMsSUFBRUEsQ0FBQyxDQUFDMEUsVUFBTCxHQUFnQkgsQ0FBQyxDQUFDckosQ0FBQyxDQUFDeU8sSUFBRixDQUFPbUgsRUFBRSxDQUFDRyxJQUFWLEVBQWVqUixDQUFDLENBQUMwRSxVQUFqQixDQUFELENBQWpCLEdBQWdELEVBQTVELEVBQStELEtBQUtpTixNQUFMLENBQVk5SSxPQUFaLENBQW9CN0ksQ0FBcEIsQ0FBdEU7QUFBNkYsS0FBcG1HLEVBQXFtR0MsQ0FBQyxDQUFDZ1UsbUJBQUYsR0FBc0IsVUFBU2pVLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsVUFBSVMsQ0FBQyxHQUFDVixDQUFDLEtBQUdxUCxFQUFWO0FBQUEsVUFBYXRTLENBQUMsR0FBQ2lELENBQUMsS0FBR3NQLEVBQW5CO0FBQUEsVUFBc0JsTyxDQUFDLEdBQUMsS0FBS21TLGFBQUwsQ0FBbUJ0VCxDQUFuQixDQUF4QjtBQUFBLFVBQThDMEIsQ0FBQyxHQUFDLEtBQUtnUSxNQUFMLENBQVloUixNQUFaLEdBQW1CLENBQW5FOztBQUFxRSxVQUFHLENBQUM1RCxDQUFDLElBQUUsTUFBSXFFLENBQVAsSUFBVVYsQ0FBQyxJQUFFVSxDQUFDLEtBQUdPLENBQWxCLEtBQXNCLENBQUMsS0FBS3dRLE9BQUwsQ0FBYWpELElBQXZDLEVBQTRDLE9BQU9qUCxDQUFQO0FBQVMsVUFBSWdDLENBQUMsR0FBQyxDQUFDYixDQUFDLElBQUVwQixDQUFDLEtBQUdzUCxFQUFKLEdBQU8sQ0FBQyxDQUFSLEdBQVUsQ0FBWixDQUFGLElBQWtCLEtBQUtxQyxNQUFMLENBQVloUixNQUFwQztBQUEyQyxhQUFNLENBQUMsQ0FBRCxLQUFLc0IsQ0FBTCxHQUFPLEtBQUswUCxNQUFMLENBQVksS0FBS0EsTUFBTCxDQUFZaFIsTUFBWixHQUFtQixDQUEvQixDQUFQLEdBQXlDLEtBQUtnUixNQUFMLENBQVkxUCxDQUFaLENBQS9DO0FBQThELEtBQTUyRyxFQUE2MkdoQyxDQUFDLENBQUNpVSxrQkFBRixHQUFxQixVQUFTbFUsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxVQUFJUyxDQUFDLEdBQUMsS0FBSzZTLGFBQUwsQ0FBbUJ2VCxDQUFuQixDQUFOO0FBQUEsVUFBNEJqRCxDQUFDLEdBQUMsS0FBS3dXLGFBQUwsQ0FBbUJyWSxDQUFDLENBQUMyTyxPQUFGLENBQVVpSCxFQUFFLENBQUNFLFdBQWIsRUFBeUIsS0FBS2xHLFFBQTlCLENBQW5CLENBQTlCOztBQUEwRixhQUFPN0IsQ0FBQyxDQUFDTSxPQUFGLENBQVUsS0FBS3VCLFFBQWYsRUFBd0IyRSxFQUFFLENBQUNDLEtBQTNCLEVBQWlDO0FBQUN5RSxRQUFBQSxhQUFhLEVBQUNuVSxDQUFmO0FBQWlCb1UsUUFBQUEsU0FBUyxFQUFDblUsQ0FBM0I7QUFBNkJvVSxRQUFBQSxJQUFJLEVBQUN0WCxDQUFsQztBQUFvQ3VXLFFBQUFBLEVBQUUsRUFBQzVTO0FBQXZDLE9BQWpDLENBQVA7QUFBbUYsS0FBN2pILEVBQThqSFQsQ0FBQyxDQUFDcVUsMEJBQUYsR0FBNkIsVUFBU3RVLENBQVQsRUFBVztBQUFDLFVBQUcsS0FBS3FTLGtCQUFSLEVBQTJCO0FBQUMsYUFBSSxJQUFJcFMsQ0FBQyxHQUFDL0UsQ0FBQyxDQUFDeU8sSUFBRixDQUFPbUgsRUFBRSxDQUFDQyxNQUFWLEVBQWlCLEtBQUtzQixrQkFBdEIsQ0FBTixFQUFnRDNSLENBQUMsR0FBQyxDQUF0RCxFQUF3REEsQ0FBQyxHQUFDVCxDQUFDLENBQUNVLE1BQTVELEVBQW1FRCxDQUFDLEVBQXBFO0FBQXVFVCxVQUFBQSxDQUFDLENBQUNTLENBQUQsQ0FBRCxDQUFLMEssU0FBTCxDQUFlQyxNQUFmLENBQXNCa0YsRUFBdEI7QUFBdkU7O0FBQWlHLFlBQUl4VCxDQUFDLEdBQUMsS0FBS3NWLGtCQUFMLENBQXdCdkksUUFBeEIsQ0FBaUMsS0FBS3lKLGFBQUwsQ0FBbUJ2VCxDQUFuQixDQUFqQyxDQUFOOztBQUE4RGpELFFBQUFBLENBQUMsSUFBRUEsQ0FBQyxDQUFDcU8sU0FBRixDQUFZZ0MsR0FBWixDQUFnQm1ELEVBQWhCLENBQUg7QUFBdUI7QUFBQyxLQUExekgsRUFBMnpIdFEsQ0FBQyxDQUFDNlMsTUFBRixHQUFTLFVBQVM5UyxDQUFULEVBQVdDLENBQVgsRUFBYTtBQUFDLFVBQUlTLENBQUo7QUFBQSxVQUFNM0QsQ0FBTjtBQUFBLFVBQVFxRSxDQUFSO0FBQUEsVUFBVU8sQ0FBQyxHQUFDLElBQVo7QUFBQSxVQUFpQk0sQ0FBQyxHQUFDL0csQ0FBQyxDQUFDMk8sT0FBRixDQUFVaUgsRUFBRSxDQUFDRSxXQUFiLEVBQXlCLEtBQUtsRyxRQUE5QixDQUFuQjtBQUFBLFVBQTJENUksQ0FBQyxHQUFDLEtBQUtxUixhQUFMLENBQW1CdFIsQ0FBbkIsQ0FBN0Q7QUFBQSxVQUFtRkUsQ0FBQyxHQUFDbEMsQ0FBQyxJQUFFZ0MsQ0FBQyxJQUFFLEtBQUtnUyxtQkFBTCxDQUF5QmpVLENBQXpCLEVBQTJCaUMsQ0FBM0IsQ0FBM0Y7QUFBQSxVQUF5SEksQ0FBQyxHQUFDLEtBQUtrUixhQUFMLENBQW1CcFIsQ0FBbkIsQ0FBM0g7QUFBQSxVQUFpSk0sQ0FBQyxHQUFDMEUsT0FBTyxDQUFDLEtBQUt5SyxTQUFOLENBQTFKOztBQUEySyxVQUFHNVIsQ0FBQyxLQUFHcVAsRUFBSixJQUFRM08sQ0FBQyxHQUFDZ1EsRUFBRixFQUFLM1QsQ0FBQyxHQUFDNFQsRUFBUCxFQUFVdlAsQ0FBQyxHQUFDbU8sRUFBcEIsS0FBeUI3TyxDQUFDLEdBQUMrUCxFQUFGLEVBQUsxVCxDQUFDLEdBQUM2VCxFQUFQLEVBQVV4UCxDQUFDLEdBQUNvTyxFQUFyQyxHQUF5Q3JOLENBQUMsSUFBRUEsQ0FBQyxDQUFDaUosU0FBRixDQUFZRSxRQUFaLENBQXFCaUYsRUFBckIsQ0FBL0MsRUFBd0UsS0FBS3dCLFVBQUwsR0FBZ0IsQ0FBQyxDQUFqQixDQUF4RSxLQUFnRyxJQUFHLENBQUMsS0FBS21DLGtCQUFMLENBQXdCL1IsQ0FBeEIsRUFBMEJmLENBQTFCLEVBQTZCc0YsZ0JBQTlCLElBQWdEekUsQ0FBaEQsSUFBbURFLENBQXRELEVBQXdEO0FBQUMsWUFBRyxLQUFLNFAsVUFBTCxHQUFnQixDQUFDLENBQWpCLEVBQW1CdFAsQ0FBQyxJQUFFLEtBQUt3TSxLQUFMLEVBQXRCLEVBQW1DLEtBQUtxRiwwQkFBTCxDQUFnQ25TLENBQWhDLENBQW5DLEVBQXNFLEtBQUsySSxRQUFMLENBQWNNLFNBQWQsQ0FBd0JFLFFBQXhCLENBQWlDa0YsRUFBakMsQ0FBekUsRUFBOEc7QUFBQ3JPLFVBQUFBLENBQUMsQ0FBQ2lKLFNBQUYsQ0FBWWdDLEdBQVosQ0FBZ0JyUSxDQUFoQixHQUFtQitILENBQUMsQ0FBQzNDLENBQUQsQ0FBcEIsRUFBd0JGLENBQUMsQ0FBQ21KLFNBQUYsQ0FBWWdDLEdBQVosQ0FBZ0IxTSxDQUFoQixDQUF4QixFQUEyQ3lCLENBQUMsQ0FBQ2lKLFNBQUYsQ0FBWWdDLEdBQVosQ0FBZ0IxTSxDQUFoQixDQUEzQztBQUE4RCxjQUFJa0MsQ0FBQyxHQUFDMlIsUUFBUSxDQUFDcFMsQ0FBQyxDQUFDTyxZQUFGLENBQWUsZUFBZixDQUFELEVBQWlDLEVBQWpDLENBQWQ7QUFBbURFLFVBQUFBLENBQUMsSUFBRSxLQUFLdVAsT0FBTCxDQUFhcUMsZUFBYixHQUE2QixLQUFLckMsT0FBTCxDQUFhcUMsZUFBYixJQUE4QixLQUFLckMsT0FBTCxDQUFhckQsUUFBeEUsRUFBaUYsS0FBS3FELE9BQUwsQ0FBYXJELFFBQWIsR0FBc0JsTSxDQUF6RyxJQUE0RyxLQUFLdVAsT0FBTCxDQUFhckQsUUFBYixHQUFzQixLQUFLcUQsT0FBTCxDQUFhcUMsZUFBYixJQUE4QixLQUFLckMsT0FBTCxDQUFhckQsUUFBOUs7QUFBdUwsY0FBSWpNLENBQUMsR0FBQ0MsQ0FBQyxDQUFDYixDQUFELENBQVA7QUFBV2dILFVBQUFBLENBQUMsQ0FBQ0ksR0FBRixDQUFNcEgsQ0FBTixFQUFRRyxDQUFSLEVBQVcsWUFBVTtBQUFDRCxZQUFBQSxDQUFDLENBQUNpSixTQUFGLENBQVlDLE1BQVosQ0FBbUIzSyxDQUFuQixHQUFzQnlCLENBQUMsQ0FBQ2lKLFNBQUYsQ0FBWUMsTUFBWixDQUFtQnRPLENBQW5CLENBQXRCLEVBQTRDb0YsQ0FBQyxDQUFDaUosU0FBRixDQUFZZ0MsR0FBWixDQUFnQm1ELEVBQWhCLENBQTVDLEVBQWdFdE8sQ0FBQyxDQUFDbUosU0FBRixDQUFZQyxNQUFaLENBQW1Ca0YsRUFBbkIsQ0FBaEUsRUFBdUZ0TyxDQUFDLENBQUNtSixTQUFGLENBQVlDLE1BQVosQ0FBbUJ0TyxDQUFuQixDQUF2RixFQUE2R2tGLENBQUMsQ0FBQ21KLFNBQUYsQ0FBWUMsTUFBWixDQUFtQjNLLENBQW5CLENBQTdHLEVBQW1JaUIsQ0FBQyxDQUFDb1EsVUFBRixHQUFhLENBQUMsQ0FBakosRUFBbUpsTyxVQUFVLENBQUUsWUFBVTtBQUFDb0YsY0FBQUEsQ0FBQyxDQUFDTSxPQUFGLENBQVU1SCxDQUFDLENBQUNtSixRQUFaLEVBQXFCMkUsRUFBRSxDQUFDRSxJQUF4QixFQUE2QjtBQUFDd0UsZ0JBQUFBLGFBQWEsRUFBQ2hTLENBQWY7QUFBaUJpUyxnQkFBQUEsU0FBUyxFQUFDaFQsQ0FBM0I7QUFBNkJpVCxnQkFBQUEsSUFBSSxFQUFDblMsQ0FBbEM7QUFBb0NvUixnQkFBQUEsRUFBRSxFQUFDalI7QUFBdkMsZUFBN0I7QUFBd0UsYUFBckYsRUFBdUYsQ0FBdkYsQ0FBN0o7QUFBdVAsV0FBN1EsR0FBZ1JxQixDQUFDLENBQUN6QixDQUFELEVBQUdZLENBQUgsQ0FBalI7QUFBdVIsU0FBenJCLE1BQThyQlosQ0FBQyxDQUFDbUosU0FBRixDQUFZQyxNQUFaLENBQW1Ca0YsRUFBbkIsR0FBdUJwTyxDQUFDLENBQUNpSixTQUFGLENBQVlnQyxHQUFaLENBQWdCbUQsRUFBaEIsQ0FBdkIsRUFBMkMsS0FBS3dCLFVBQUwsR0FBZ0IsQ0FBQyxDQUE1RCxFQUE4RDlJLENBQUMsQ0FBQ00sT0FBRixDQUFVLEtBQUt1QixRQUFmLEVBQXdCMkUsRUFBRSxDQUFDRSxJQUEzQixFQUFnQztBQUFDd0UsVUFBQUEsYUFBYSxFQUFDaFMsQ0FBZjtBQUFpQmlTLFVBQUFBLFNBQVMsRUFBQ2hULENBQTNCO0FBQTZCaVQsVUFBQUEsSUFBSSxFQUFDblMsQ0FBbEM7QUFBb0NvUixVQUFBQSxFQUFFLEVBQUNqUjtBQUF2QyxTQUFoQyxDQUE5RDs7QUFBeUlJLFFBQUFBLENBQUMsSUFBRSxLQUFLd1EsS0FBTCxFQUFIO0FBQWdCO0FBQUMsS0FBOStKLEVBQSsrSmpULENBQUMsQ0FBQ3lVLGlCQUFGLEdBQW9CLFVBQVN4VSxDQUFULEVBQVdTLENBQVgsRUFBYTtBQUFDLFVBQUkzRCxDQUFDLEdBQUN3SSxDQUFDLENBQUNFLE9BQUYsQ0FBVXhGLENBQVYsRUFBWTBPLEVBQVosQ0FBTjtBQUFBLFVBQXNCdk4sQ0FBQyxHQUFDTyxDQUFDLENBQUMsRUFBRCxFQUFJa04sRUFBSixFQUFPLEVBQVAsRUFBVWxCLEVBQUUsQ0FBQ0csaUJBQUgsQ0FBcUI3TixDQUFyQixDQUFWLENBQXpCO0FBQTRELDBCQUFpQlMsQ0FBakIsTUFBcUJVLENBQUMsR0FBQ08sQ0FBQyxDQUFDLEVBQUQsRUFBSVAsQ0FBSixFQUFNLEVBQU4sRUFBU1YsQ0FBVCxDQUF4QjtBQUFxQyxVQUFJdUIsQ0FBQyxHQUFDLFlBQVUsT0FBT3ZCLENBQWpCLEdBQW1CQSxDQUFuQixHQUFxQlUsQ0FBQyxDQUFDNE4sS0FBN0I7QUFBbUMsVUFBR2pTLENBQUMsS0FBR0EsQ0FBQyxHQUFDLElBQUlpRCxDQUFKLENBQU1DLENBQU4sRUFBUW1CLENBQVIsQ0FBTCxDQUFELEVBQWtCLFlBQVUsT0FBT1YsQ0FBdEMsRUFBd0MzRCxDQUFDLENBQUN1VyxFQUFGLENBQUs1UyxDQUFMLEVBQXhDLEtBQXFELElBQUcsWUFBVSxPQUFPdUIsQ0FBcEIsRUFBc0I7QUFBQyxZQUFHLGVBQWEsT0FBT2xGLENBQUMsQ0FBQ2tGLENBQUQsQ0FBeEIsRUFBNEIsTUFBTSxJQUFJeVMsU0FBSixDQUFjLHNCQUFvQnpTLENBQXBCLEdBQXNCLEdBQXBDLENBQU47QUFBK0NsRixRQUFBQSxDQUFDLENBQUNrRixDQUFELENBQUQ7QUFBTyxPQUF6RyxNQUE4R2IsQ0FBQyxDQUFDME4sUUFBRixJQUFZMU4sQ0FBQyxDQUFDdVQsSUFBZCxLQUFxQjVYLENBQUMsQ0FBQ2tTLEtBQUYsSUFBVWxTLENBQUMsQ0FBQ2tXLEtBQUYsRUFBL0I7QUFBMEMsS0FBbDJLLEVBQW0yS2pULENBQUMsQ0FBQ3lMLGVBQUYsR0FBa0IsVUFBU3hMLENBQVQsRUFBVztBQUFDLGFBQU8sS0FBS3hFLElBQUwsQ0FBVyxZQUFVO0FBQUN1RSxRQUFBQSxDQUFDLENBQUN5VSxpQkFBRixDQUFvQixJQUFwQixFQUF5QnhVLENBQXpCO0FBQTRCLE9BQWxELENBQVA7QUFBNEQsS0FBNzdLLEVBQTg3S0QsQ0FBQyxDQUFDNFUsbUJBQUYsR0FBc0IsVUFBUzNVLENBQVQsRUFBVztBQUFDLFVBQUlTLENBQUMsR0FBQ21DLENBQUMsQ0FBQyxJQUFELENBQVA7O0FBQWMsVUFBR25DLENBQUMsSUFBRUEsQ0FBQyxDQUFDMEssU0FBRixDQUFZRSxRQUFaLENBQXFCZ0YsRUFBckIsQ0FBTixFQUErQjtBQUFDLFlBQUl2VCxDQUFDLEdBQUM0RSxDQUFDLENBQUMsRUFBRCxFQUFJZ00sRUFBRSxDQUFDRyxpQkFBSCxDQUFxQnBOLENBQXJCLENBQUosRUFBNEIsRUFBNUIsRUFBK0JpTixFQUFFLENBQUNHLGlCQUFILENBQXFCLElBQXJCLENBQS9CLENBQVA7QUFBQSxZQUFrRTFNLENBQUMsR0FBQyxLQUFLc0IsWUFBTCxDQUFrQixlQUFsQixDQUFwRTtBQUF1R3RCLFFBQUFBLENBQUMsS0FBR3JFLENBQUMsQ0FBQytSLFFBQUYsR0FBVyxDQUFDLENBQWYsQ0FBRCxFQUFtQjlPLENBQUMsQ0FBQ3lVLGlCQUFGLENBQW9CL1QsQ0FBcEIsRUFBc0IzRCxDQUF0QixDQUFuQixFQUE0Q3FFLENBQUMsSUFBRW1FLENBQUMsQ0FBQ0UsT0FBRixDQUFVL0UsQ0FBVixFQUFZaU8sRUFBWixFQUFnQjJFLEVBQWhCLENBQW1CbFMsQ0FBbkIsQ0FBL0MsRUFBcUVuQixDQUFDLENBQUN3RyxjQUFGLEVBQXJFO0FBQXdGO0FBQUMsS0FBOXNMLEVBQStzTHpHLENBQUMsQ0FBQzJMLFdBQUYsR0FBYyxVQUFTM0wsQ0FBVCxFQUFXO0FBQUMsYUFBT3VGLENBQUMsQ0FBQ0UsT0FBRixDQUFVekYsQ0FBVixFQUFZMk8sRUFBWixDQUFQO0FBQXVCLEtBQWh3TCxFQUFpd0xqTyxDQUFDLENBQUNWLENBQUQsRUFBRyxJQUFILEVBQVEsQ0FBQztBQUFDaUIsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTSxPQUFOO0FBQWM7QUFBNUMsS0FBRCxFQUErQztBQUFDckUsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBT3VKLEVBQVA7QUFBVTtBQUF4QyxLQUEvQyxDQUFSLENBQWx3TCxFQUFxMkw3TyxDQUE1Mkw7QUFBODJMLEdBQTMzTSxFQUE1ckQ7O0FBQTBqUWlKLEVBQUFBLENBQUMsQ0FBQ0csRUFBRixDQUFLcE8sUUFBTCxFQUFjeVUsRUFBRSxDQUFDaEYsY0FBakIsRUFBZ0NxRyxFQUFFLENBQUNPLFVBQW5DLEVBQThDSyxFQUFFLENBQUNrRCxtQkFBakQsR0FBc0UzTCxDQUFDLENBQUNHLEVBQUYsQ0FBS3hLLE1BQUwsRUFBWTZRLEVBQUUsQ0FBQ1ksYUFBZixFQUE4QixZQUFVO0FBQUMsU0FBSSxJQUFJclEsQ0FBQyxHQUFDdUUsQ0FBQyxDQUFDckosQ0FBQyxDQUFDeU8sSUFBRixDQUFPbUgsRUFBRSxDQUFDUSxTQUFWLENBQUQsQ0FBUCxFQUE4QnJSLENBQUMsR0FBQyxDQUFoQyxFQUFrQ1MsQ0FBQyxHQUFDVixDQUFDLENBQUNXLE1BQTFDLEVBQWlEVixDQUFDLEdBQUNTLENBQW5ELEVBQXFEVCxDQUFDLEVBQXREO0FBQXlEeVIsTUFBQUEsRUFBRSxDQUFDK0MsaUJBQUgsQ0FBcUJ6VSxDQUFDLENBQUNDLENBQUQsQ0FBdEIsRUFBMEJzRixDQUFDLENBQUNFLE9BQUYsQ0FBVXpGLENBQUMsQ0FBQ0MsQ0FBRCxDQUFYLEVBQWUwTyxFQUFmLENBQTFCO0FBQXpEO0FBQXVHLEdBQWhKLENBQXRFO0FBQXlOLE1BQUlrRyxFQUFFLEdBQUM3UCxDQUFDLEVBQVI7O0FBQVcsTUFBRzZQLEVBQUgsRUFBTTtBQUFDLFFBQUlDLEVBQUUsR0FBQ0QsRUFBRSxDQUFDL0ksRUFBSCxDQUFNNEMsRUFBTixDQUFQO0FBQWlCbUcsSUFBQUEsRUFBRSxDQUFDL0ksRUFBSCxDQUFNNEMsRUFBTixJQUFVZ0QsRUFBRSxDQUFDakcsZUFBYixFQUE2Qm9KLEVBQUUsQ0FBQy9JLEVBQUgsQ0FBTTRDLEVBQU4sRUFBVTFDLFdBQVYsR0FBc0IwRixFQUFuRCxFQUFzRG1ELEVBQUUsQ0FBQy9JLEVBQUgsQ0FBTTRDLEVBQU4sRUFBVXpDLFVBQVYsR0FBcUIsWUFBVTtBQUFDLGFBQU80SSxFQUFFLENBQUMvSSxFQUFILENBQU00QyxFQUFOLElBQVVvRyxFQUFWLEVBQWFwRCxFQUFFLENBQUNqRyxlQUF2QjtBQUF1QyxLQUE3SDtBQUE4SDs7QUFBQSxNQUFJc0osRUFBRSxHQUFDLFVBQVA7QUFBQSxNQUFrQkMsRUFBRSxHQUFDLGFBQXJCO0FBQUEsTUFBbUNDLEVBQUUsR0FBQyxNQUFJRCxFQUExQztBQUFBLE1BQTZDRSxFQUFFLEdBQUM7QUFBQ2xJLElBQUFBLE1BQU0sRUFBQyxDQUFDLENBQVQ7QUFBV21JLElBQUFBLE1BQU0sRUFBQztBQUFsQixHQUFoRDtBQUFBLE1BQXNFQyxFQUFFLEdBQUM7QUFBQ3BJLElBQUFBLE1BQU0sRUFBQyxTQUFSO0FBQWtCbUksSUFBQUEsTUFBTSxFQUFDO0FBQXpCLEdBQXpFO0FBQUEsTUFBc0hFLEVBQUUsR0FBQztBQUFDQyxJQUFBQSxJQUFJLEVBQUMsU0FBT0wsRUFBYjtBQUFnQk0sSUFBQUEsS0FBSyxFQUFDLFVBQVFOLEVBQTlCO0FBQWlDTyxJQUFBQSxJQUFJLEVBQUMsU0FBT1AsRUFBN0M7QUFBZ0RRLElBQUFBLE1BQU0sRUFBQyxXQUFTUixFQUFoRTtBQUFtRXhLLElBQUFBLGNBQWMsRUFBQyxVQUFRd0ssRUFBUixHQUFXO0FBQTdGLEdBQXpIO0FBQUEsTUFBbU9TLEVBQUUsR0FBQyxNQUF0TztBQUFBLE1BQTZPQyxFQUFFLEdBQUMsVUFBaFA7QUFBQSxNQUEyUEMsRUFBRSxHQUFDLFlBQTlQO0FBQUEsTUFBMlFDLEVBQUUsR0FBQyxXQUE5UTtBQUFBLE1BQTBSQyxFQUFFLEdBQUMsT0FBN1I7QUFBQSxNQUFxU0MsRUFBRSxHQUFDLFFBQXhTO0FBQUEsTUFBaVRDLEVBQUUsR0FBQztBQUFDQyxJQUFBQSxPQUFPLEVBQUMsb0JBQVQ7QUFBOEJDLElBQUFBLFdBQVcsRUFBQztBQUExQyxHQUFwVDtBQUFBLE1BQTBYQyxFQUFFLEdBQUMsWUFBVTtBQUFDLGFBQVNuVyxDQUFULENBQVdBLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsV0FBS21XLGdCQUFMLEdBQXNCLENBQUMsQ0FBdkIsRUFBeUIsS0FBS3RMLFFBQUwsR0FBYzlLLENBQXZDLEVBQXlDLEtBQUttUyxPQUFMLEdBQWEsS0FBS0MsVUFBTCxDQUFnQm5TLENBQWhCLENBQXRELEVBQXlFLEtBQUtvVyxhQUFMLEdBQW1COVIsQ0FBQyxDQUFDckosQ0FBQyxDQUFDeU8sSUFBRixDQUFPLHFDQUFtQzNKLENBQUMsQ0FBQ3FGLEVBQXJDLEdBQXdDLDRDQUF4QyxHQUFxRnJGLENBQUMsQ0FBQ3FGLEVBQXZGLEdBQTBGLElBQWpHLENBQUQsQ0FBN0Y7O0FBQXNNLFdBQUksSUFBSTNFLENBQUMsR0FBQzZELENBQUMsQ0FBQ3JKLENBQUMsQ0FBQ3lPLElBQUYsQ0FBT3FNLEVBQUUsQ0FBQ0UsV0FBVixDQUFELENBQVAsRUFBZ0NuWixDQUFDLEdBQUMsQ0FBbEMsRUFBb0NxRSxDQUFDLEdBQUNWLENBQUMsQ0FBQ0MsTUFBNUMsRUFBbUQ1RCxDQUFDLEdBQUNxRSxDQUFyRCxFQUF1RHJFLENBQUMsRUFBeEQsRUFBMkQ7QUFBQyxZQUFJNEUsQ0FBQyxHQUFDakIsQ0FBQyxDQUFDM0QsQ0FBRCxDQUFQO0FBQUEsWUFBV2tGLENBQUMsR0FBQ1csQ0FBQyxDQUFDakIsQ0FBRCxDQUFkO0FBQUEsWUFBa0JPLENBQUMsR0FBQ3FDLENBQUMsQ0FBQ3JKLENBQUMsQ0FBQ3lPLElBQUYsQ0FBTzFILENBQVAsQ0FBRCxDQUFELENBQWFWLE1BQWIsQ0FBcUIsVUFBU3RCLENBQVQsRUFBVztBQUFDLGlCQUFPQSxDQUFDLEtBQUdELENBQVg7QUFBYSxTQUE5QyxDQUFwQjtBQUFxRSxpQkFBT2lDLENBQVAsSUFBVUMsQ0FBQyxDQUFDdkIsTUFBWixLQUFxQixLQUFLMlYsU0FBTCxHQUFlclUsQ0FBZixFQUFpQixLQUFLb1UsYUFBTCxDQUFtQjVVLElBQW5CLENBQXdCRSxDQUF4QixDQUF0QztBQUFrRTs7QUFBQSxXQUFLNFUsT0FBTCxHQUFhLEtBQUtwRSxPQUFMLENBQWFnRCxNQUFiLEdBQW9CLEtBQUtxQixVQUFMLEVBQXBCLEdBQXNDLElBQW5ELEVBQXdELEtBQUtyRSxPQUFMLENBQWFnRCxNQUFiLElBQXFCLEtBQUtzQix5QkFBTCxDQUErQixLQUFLM0wsUUFBcEMsRUFBNkMsS0FBS3VMLGFBQWxELENBQTdFLEVBQThJLEtBQUtsRSxPQUFMLENBQWFuRixNQUFiLElBQXFCLEtBQUtBLE1BQUwsRUFBbkssRUFBaUx6SCxDQUFDLENBQUNDLE9BQUYsQ0FBVXhGLENBQVYsRUFBWWdWLEVBQVosRUFBZSxJQUFmLENBQWpMO0FBQXNNOztBQUFBLFFBQUkvVSxDQUFDLEdBQUNELENBQUMsQ0FBQ2tCLFNBQVI7QUFBa0IsV0FBT2pCLENBQUMsQ0FBQytNLE1BQUYsR0FBUyxZQUFVO0FBQUMsV0FBS2xDLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkUsUUFBeEIsQ0FBaUNvSyxFQUFqQyxJQUFxQyxLQUFLZ0IsSUFBTCxFQUFyQyxHQUFpRCxLQUFLQyxJQUFMLEVBQWpEO0FBQTZELEtBQWpGLEVBQWtGMVcsQ0FBQyxDQUFDMFcsSUFBRixHQUFPLFlBQVU7QUFBQyxVQUFJMVcsQ0FBQyxHQUFDLElBQU47O0FBQVcsVUFBRyxDQUFDLEtBQUttVyxnQkFBTixJQUF3QixDQUFDLEtBQUt0TCxRQUFMLENBQWNNLFNBQWQsQ0FBd0JFLFFBQXhCLENBQWlDb0ssRUFBakMsQ0FBNUIsRUFBaUU7QUFBQyxZQUFJaFYsQ0FBSixFQUFNM0QsQ0FBTjtBQUFRLGFBQUt3WixPQUFMLElBQWMsTUFBSSxDQUFDN1YsQ0FBQyxHQUFDNkQsQ0FBQyxDQUFDckosQ0FBQyxDQUFDeU8sSUFBRixDQUFPcU0sRUFBRSxDQUFDQyxPQUFWLEVBQWtCLEtBQUtNLE9BQXZCLENBQUQsQ0FBRCxDQUFtQ2hWLE1BQW5DLENBQTJDLFVBQVN2QixDQUFULEVBQVc7QUFBQyxpQkFBTSxZQUFVLE9BQU9DLENBQUMsQ0FBQ2tTLE9BQUYsQ0FBVWdELE1BQTNCLEdBQWtDblYsQ0FBQyxDQUFDMEMsWUFBRixDQUFlLGFBQWYsTUFBZ0N6QyxDQUFDLENBQUNrUyxPQUFGLENBQVVnRCxNQUE1RSxHQUFtRm5WLENBQUMsQ0FBQ29MLFNBQUYsQ0FBWUUsUUFBWixDQUFxQnFLLEVBQXJCLENBQXpGO0FBQWtILFNBQXpLLENBQUgsRUFBZ0xoVixNQUFsTSxLQUEyTUQsQ0FBQyxHQUFDLElBQTdNO0FBQW1OLFlBQUlVLENBQUMsR0FBQ2xHLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVSxLQUFLeU0sU0FBZixDQUFOOztBQUFnQyxZQUFHNVYsQ0FBSCxFQUFLO0FBQUMsY0FBSWlCLENBQUMsR0FBQ2pCLENBQUMsQ0FBQ2EsTUFBRixDQUFVLFVBQVN2QixDQUFULEVBQVc7QUFBQyxtQkFBT29CLENBQUMsS0FBR3BCLENBQVg7QUFBYSxXQUFuQyxDQUFOO0FBQTRDLGNBQUcsQ0FBQ2pELENBQUMsR0FBQzRFLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBSzRELENBQUMsQ0FBQ0UsT0FBRixDQUFVOUQsQ0FBQyxDQUFDLENBQUQsQ0FBWCxFQUFlcVQsRUFBZixDQUFMLEdBQXdCLElBQTNCLEtBQWtDalksQ0FBQyxDQUFDcVosZ0JBQXZDLEVBQXdEO0FBQU87O0FBQUEsWUFBRyxDQUFDbk4sQ0FBQyxDQUFDTSxPQUFGLENBQVUsS0FBS3VCLFFBQWYsRUFBd0J1SyxFQUFFLENBQUNDLElBQTNCLEVBQWlDNU8sZ0JBQXJDLEVBQXNEO0FBQUNoRyxVQUFBQSxDQUFDLElBQUVBLENBQUMsQ0FBQ21CLE9BQUYsQ0FBVyxVQUFTNUIsQ0FBVCxFQUFXO0FBQUNtQixZQUFBQSxDQUFDLEtBQUduQixDQUFKLElBQU9ELENBQUMsQ0FBQzRXLGlCQUFGLENBQW9CM1csQ0FBcEIsRUFBc0IsTUFBdEIsQ0FBUCxFQUFxQ2xELENBQUMsSUFBRXdJLENBQUMsQ0FBQ0MsT0FBRixDQUFVdkYsQ0FBVixFQUFZK1UsRUFBWixFQUFlLElBQWYsQ0FBeEM7QUFBNkQsV0FBcEYsQ0FBSDs7QUFBMEYsY0FBSS9TLENBQUMsR0FBQyxLQUFLNFUsYUFBTCxFQUFOOztBQUEyQixlQUFLL0wsUUFBTCxDQUFjTSxTQUFkLENBQXdCQyxNQUF4QixDQUErQnNLLEVBQS9CLEdBQW1DLEtBQUs3SyxRQUFMLENBQWNNLFNBQWQsQ0FBd0JnQyxHQUF4QixDQUE0QndJLEVBQTVCLENBQW5DLEVBQW1FLEtBQUs5SyxRQUFMLENBQWNyRyxLQUFkLENBQW9CeEMsQ0FBcEIsSUFBdUIsQ0FBMUYsRUFBNEYsS0FBS29VLGFBQUwsQ0FBbUIxVixNQUFuQixJQUEyQixLQUFLMFYsYUFBTCxDQUFtQnhVLE9BQW5CLENBQTRCLFVBQVM3QixDQUFULEVBQVc7QUFBQ0EsWUFBQUEsQ0FBQyxDQUFDb0wsU0FBRixDQUFZQyxNQUFaLENBQW1Cd0ssRUFBbkIsR0FBdUI3VixDQUFDLENBQUNtTixZQUFGLENBQWUsZUFBZixFQUErQixDQUFDLENBQWhDLENBQXZCO0FBQTBELFdBQWxHLENBQXZILEVBQTROLEtBQUsySixnQkFBTCxDQUFzQixDQUFDLENBQXZCLENBQTVOO0FBQXNQLGNBQUk1VSxDQUFDLEdBQUMsWUFBVUQsQ0FBQyxDQUFDLENBQUQsQ0FBRCxDQUFLcUMsV0FBTCxLQUFtQnJDLENBQUMsQ0FBQ3VDLEtBQUYsQ0FBUSxDQUFSLENBQTdCLENBQU47QUFBQSxjQUErQ3JDLENBQUMsR0FBQ1csQ0FBQyxDQUFDLEtBQUtnSSxRQUFOLENBQWxEO0FBQWtFN0IsVUFBQUEsQ0FBQyxDQUFDSSxHQUFGLENBQU0sS0FBS3lCLFFBQVgsRUFBb0IxSSxDQUFwQixFQUF1QixZQUFVO0FBQUNuQyxZQUFBQSxDQUFDLENBQUM2SyxRQUFGLENBQVdNLFNBQVgsQ0FBcUJDLE1BQXJCLENBQTRCdUssRUFBNUIsR0FBZ0MzVixDQUFDLENBQUM2SyxRQUFGLENBQVdNLFNBQVgsQ0FBcUJnQyxHQUFyQixDQUF5QnVJLEVBQXpCLENBQWhDLEVBQTZEMVYsQ0FBQyxDQUFDNkssUUFBRixDQUFXTSxTQUFYLENBQXFCZ0MsR0FBckIsQ0FBeUJzSSxFQUF6QixDQUE3RCxFQUEwRnpWLENBQUMsQ0FBQzZLLFFBQUYsQ0FBV3JHLEtBQVgsQ0FBaUJ4QyxDQUFqQixJQUFvQixFQUE5RyxFQUFpSGhDLENBQUMsQ0FBQzZXLGdCQUFGLENBQW1CLENBQUMsQ0FBcEIsQ0FBakgsRUFBd0k3TixDQUFDLENBQUNNLE9BQUYsQ0FBVXRKLENBQUMsQ0FBQzZLLFFBQVosRUFBcUJ1SyxFQUFFLENBQUNFLEtBQXhCLENBQXhJO0FBQXVLLFdBQXpNLEdBQTRNN1IsQ0FBQyxDQUFDLEtBQUtvSCxRQUFOLEVBQWUzSSxDQUFmLENBQTdNLEVBQStOLEtBQUsySSxRQUFMLENBQWNyRyxLQUFkLENBQW9CeEMsQ0FBcEIsSUFBdUIsS0FBSzZJLFFBQUwsQ0FBYzVJLENBQWQsSUFBaUIsSUFBdlE7QUFBNFE7QUFBQztBQUFDLEtBQS93QyxFQUFneENqQyxDQUFDLENBQUN5VyxJQUFGLEdBQU8sWUFBVTtBQUFDLFVBQUkxVyxDQUFDLEdBQUMsSUFBTjs7QUFBVyxVQUFHLENBQUMsS0FBS29XLGdCQUFOLElBQXdCLEtBQUt0TCxRQUFMLENBQWNNLFNBQWQsQ0FBd0JFLFFBQXhCLENBQWlDb0ssRUFBakMsQ0FBeEIsSUFBOEQsQ0FBQ3pNLENBQUMsQ0FBQ00sT0FBRixDQUFVLEtBQUt1QixRQUFmLEVBQXdCdUssRUFBRSxDQUFDRyxJQUEzQixFQUFpQzlPLGdCQUFuRyxFQUFvSDtBQUFDLFlBQUl6RyxDQUFDLEdBQUMsS0FBSzRXLGFBQUwsRUFBTjs7QUFBMkIsYUFBSy9MLFFBQUwsQ0FBY3JHLEtBQWQsQ0FBb0J4RSxDQUFwQixJQUF1QixLQUFLNkssUUFBTCxDQUFjbUQscUJBQWQsR0FBc0NoTyxDQUF0QyxJQUF5QyxJQUFoRSxFQUFxRTZFLENBQUMsQ0FBQyxLQUFLZ0csUUFBTixDQUF0RSxFQUFzRixLQUFLQSxRQUFMLENBQWNNLFNBQWQsQ0FBd0JnQyxHQUF4QixDQUE0QndJLEVBQTVCLENBQXRGLEVBQXNILEtBQUs5SyxRQUFMLENBQWNNLFNBQWQsQ0FBd0JDLE1BQXhCLENBQStCc0ssRUFBL0IsQ0FBdEgsRUFBeUosS0FBSzdLLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkMsTUFBeEIsQ0FBK0JxSyxFQUEvQixDQUF6SjtBQUE0TCxZQUFJaFYsQ0FBQyxHQUFDLEtBQUsyVixhQUFMLENBQW1CMVYsTUFBekI7QUFBZ0MsWUFBR0QsQ0FBQyxHQUFDLENBQUwsRUFBTyxLQUFJLElBQUkzRCxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUMyRCxDQUFkLEVBQWdCM0QsQ0FBQyxFQUFqQixFQUFvQjtBQUFDLGNBQUlxRSxDQUFDLEdBQUMsS0FBS2lWLGFBQUwsQ0FBbUJ0WixDQUFuQixDQUFOO0FBQUEsY0FBNEI0RSxDQUFDLEdBQUNrQixDQUFDLENBQUN6QixDQUFELENBQS9CO0FBQW1DTyxVQUFBQSxDQUFDLElBQUUsQ0FBQ0EsQ0FBQyxDQUFDeUosU0FBRixDQUFZRSxRQUFaLENBQXFCb0ssRUFBckIsQ0FBSixLQUErQnRVLENBQUMsQ0FBQ2dLLFNBQUYsQ0FBWWdDLEdBQVosQ0FBZ0J5SSxFQUFoQixHQUFvQnpVLENBQUMsQ0FBQytMLFlBQUYsQ0FBZSxlQUFmLEVBQStCLENBQUMsQ0FBaEMsQ0FBbkQ7QUFBdUY7QUFBQSxhQUFLMkosZ0JBQUwsQ0FBc0IsQ0FBQyxDQUF2QjtBQUEwQixhQUFLaE0sUUFBTCxDQUFjckcsS0FBZCxDQUFvQnhFLENBQXBCLElBQXVCLEVBQXZCO0FBQTBCLFlBQUlnQyxDQUFDLEdBQUNhLENBQUMsQ0FBQyxLQUFLZ0ksUUFBTixDQUFQO0FBQXVCN0IsUUFBQUEsQ0FBQyxDQUFDSSxHQUFGLENBQU0sS0FBS3lCLFFBQVgsRUFBb0IxSSxDQUFwQixFQUF1QixZQUFVO0FBQUNwQyxVQUFBQSxDQUFDLENBQUM4VyxnQkFBRixDQUFtQixDQUFDLENBQXBCLEdBQXVCOVcsQ0FBQyxDQUFDOEssUUFBRixDQUFXTSxTQUFYLENBQXFCQyxNQUFyQixDQUE0QnVLLEVBQTVCLENBQXZCLEVBQXVENVYsQ0FBQyxDQUFDOEssUUFBRixDQUFXTSxTQUFYLENBQXFCZ0MsR0FBckIsQ0FBeUJ1SSxFQUF6QixDQUF2RCxFQUFvRjFNLENBQUMsQ0FBQ00sT0FBRixDQUFVdkosQ0FBQyxDQUFDOEssUUFBWixFQUFxQnVLLEVBQUUsQ0FBQ0ksTUFBeEIsQ0FBcEY7QUFBb0gsU0FBdEosR0FBeUovUixDQUFDLENBQUMsS0FBS29ILFFBQU4sRUFBZTdJLENBQWYsQ0FBMUo7QUFBNEs7QUFBQyxLQUF2aUUsRUFBd2lFaEMsQ0FBQyxDQUFDNlcsZ0JBQUYsR0FBbUIsVUFBUzlXLENBQVQsRUFBVztBQUFDLFdBQUtvVyxnQkFBTCxHQUFzQnBXLENBQXRCO0FBQXdCLEtBQS9sRSxFQUFnbUVDLENBQUMsQ0FBQ2tMLE9BQUYsR0FBVSxZQUFVO0FBQUM1RixNQUFBQSxDQUFDLENBQUNHLFVBQUYsQ0FBYSxLQUFLb0YsUUFBbEIsRUFBMkJrSyxFQUEzQixHQUErQixLQUFLN0MsT0FBTCxHQUFhLElBQTVDLEVBQWlELEtBQUtvRSxPQUFMLEdBQWEsSUFBOUQsRUFBbUUsS0FBS3pMLFFBQUwsR0FBYyxJQUFqRixFQUFzRixLQUFLdUwsYUFBTCxHQUFtQixJQUF6RyxFQUE4RyxLQUFLRCxnQkFBTCxHQUFzQixJQUFwSTtBQUF5SSxLQUE5dkUsRUFBK3ZFblcsQ0FBQyxDQUFDbVMsVUFBRixHQUFhLFVBQVNwUyxDQUFULEVBQVc7QUFBQyxhQUFNLENBQUNBLENBQUMsR0FBQzJCLENBQUMsQ0FBQyxFQUFELEVBQUl1VCxFQUFKLEVBQU8sRUFBUCxFQUFVbFYsQ0FBVixDQUFKLEVBQWtCZ04sTUFBbEIsR0FBeUI3RixPQUFPLENBQUNuSCxDQUFDLENBQUNnTixNQUFILENBQWhDLEVBQTJDbEosQ0FBQyxDQUFDaVIsRUFBRCxFQUFJL1UsQ0FBSixFQUFNb1YsRUFBTixDQUE1QyxFQUFzRHBWLENBQTVEO0FBQThELEtBQXQxRSxFQUF1MUVDLENBQUMsQ0FBQzRXLGFBQUYsR0FBZ0IsWUFBVTtBQUFDLGFBQU8sS0FBSy9MLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkUsUUFBeEIsQ0FBaUN3SyxFQUFqQyxJQUFxQ0EsRUFBckMsR0FBd0NDLEVBQS9DO0FBQWtELEtBQXA2RSxFQUFxNkU5VixDQUFDLENBQUN1VyxVQUFGLEdBQWEsWUFBVTtBQUFDLFVBQUl4VyxDQUFDLEdBQUMsSUFBTjtBQUFBLFVBQVdDLENBQUMsR0FBQyxLQUFLa1MsT0FBTCxDQUFhZ0QsTUFBMUI7QUFBaUMzUixNQUFBQSxDQUFDLENBQUN2RCxDQUFELENBQUQsR0FBSyxlQUFhLE9BQU9BLENBQUMsQ0FBQzhXLE1BQXRCLElBQThCLGVBQWEsT0FBTzlXLENBQUMsQ0FBQyxDQUFELENBQW5ELEtBQXlEQSxDQUFDLEdBQUNBLENBQUMsQ0FBQyxDQUFELENBQTVELENBQUwsR0FBc0VBLENBQUMsR0FBQy9FLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVTVKLENBQVYsQ0FBeEU7QUFBcUYsVUFBSVMsQ0FBQyxHQUFDLDJDQUF5Q1QsQ0FBekMsR0FBMkMsSUFBakQ7QUFBc0QsYUFBT3NFLENBQUMsQ0FBQ3JKLENBQUMsQ0FBQ3lPLElBQUYsQ0FBT2pKLENBQVAsRUFBU1QsQ0FBVCxDQUFELENBQUQsQ0FBZTRCLE9BQWYsQ0FBd0IsVUFBUzVCLENBQVQsRUFBVztBQUFDLFlBQUlTLENBQUMsR0FBQ21DLENBQUMsQ0FBQzVDLENBQUQsQ0FBUDs7QUFBV0QsUUFBQUEsQ0FBQyxDQUFDeVcseUJBQUYsQ0FBNEIvVixDQUE1QixFQUE4QixDQUFDVCxDQUFELENBQTlCO0FBQW1DLE9BQWxGLEdBQXFGQSxDQUE1RjtBQUE4RixLQUF2c0YsRUFBd3NGQSxDQUFDLENBQUN3Vyx5QkFBRixHQUE0QixVQUFTelcsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxVQUFHRCxDQUFILEVBQUs7QUFBQyxZQUFJVSxDQUFDLEdBQUNWLENBQUMsQ0FBQ29MLFNBQUYsQ0FBWUUsUUFBWixDQUFxQm9LLEVBQXJCLENBQU47QUFBK0J6VixRQUFBQSxDQUFDLENBQUNVLE1BQUYsSUFBVVYsQ0FBQyxDQUFDNEIsT0FBRixDQUFXLFVBQVM3QixDQUFULEVBQVc7QUFBQ1UsVUFBQUEsQ0FBQyxHQUFDVixDQUFDLENBQUNvTCxTQUFGLENBQVlDLE1BQVosQ0FBbUJ3SyxFQUFuQixDQUFELEdBQXdCN1YsQ0FBQyxDQUFDb0wsU0FBRixDQUFZZ0MsR0FBWixDQUFnQnlJLEVBQWhCLENBQXpCLEVBQTZDN1YsQ0FBQyxDQUFDbU4sWUFBRixDQUFlLGVBQWYsRUFBK0J6TSxDQUEvQixDQUE3QztBQUErRSxTQUF0RyxDQUFWO0FBQW1IO0FBQUMsS0FBMzRGLEVBQTQ0RlYsQ0FBQyxDQUFDNFcsaUJBQUYsR0FBb0IsVUFBUzNXLENBQVQsRUFBV1MsQ0FBWCxFQUFhO0FBQUMsVUFBSTNELENBQUMsR0FBQ3dJLENBQUMsQ0FBQ0UsT0FBRixDQUFVeEYsQ0FBVixFQUFZK1UsRUFBWixDQUFOO0FBQUEsVUFBc0I1VCxDQUFDLEdBQUNPLENBQUMsQ0FBQyxFQUFELEVBQUl1VCxFQUFKLEVBQU8sRUFBUCxFQUFVdkgsRUFBRSxDQUFDRyxpQkFBSCxDQUFxQjdOLENBQXJCLENBQVYsRUFBa0MsRUFBbEMsRUFBcUMsb0JBQWlCUyxDQUFqQixLQUFvQkEsQ0FBcEIsR0FBc0JBLENBQXRCLEdBQXdCLEVBQTdELENBQXpCOztBQUEwRixVQUFHLENBQUMzRCxDQUFELElBQUlxRSxDQUFDLENBQUM0TCxNQUFOLElBQWMsWUFBWTVJLElBQVosQ0FBaUIxRCxDQUFqQixDQUFkLEtBQW9DVSxDQUFDLENBQUM0TCxNQUFGLEdBQVMsQ0FBQyxDQUE5QyxHQUFpRGpRLENBQUMsS0FBR0EsQ0FBQyxHQUFDLElBQUlpRCxDQUFKLENBQU1DLENBQU4sRUFBUW1CLENBQVIsQ0FBTCxDQUFsRCxFQUFtRSxZQUFVLE9BQU9WLENBQXZGLEVBQXlGO0FBQUMsWUFBRyxlQUFhLE9BQU8zRCxDQUFDLENBQUMyRCxDQUFELENBQXhCLEVBQTRCLE1BQU0sSUFBSWdVLFNBQUosQ0FBYyxzQkFBb0JoVSxDQUFwQixHQUFzQixHQUFwQyxDQUFOO0FBQStDM0QsUUFBQUEsQ0FBQyxDQUFDMkQsQ0FBRCxDQUFEO0FBQU87QUFBQyxLQUFyckcsRUFBc3JHVixDQUFDLENBQUN5TCxlQUFGLEdBQWtCLFVBQVN4TCxDQUFULEVBQVc7QUFBQyxhQUFPLEtBQUt4RSxJQUFMLENBQVcsWUFBVTtBQUFDdUUsUUFBQUEsQ0FBQyxDQUFDNFcsaUJBQUYsQ0FBb0IsSUFBcEIsRUFBeUIzVyxDQUF6QjtBQUE0QixPQUFsRCxDQUFQO0FBQTRELEtBQWh4RyxFQUFpeEdELENBQUMsQ0FBQzJMLFdBQUYsR0FBYyxVQUFTM0wsQ0FBVCxFQUFXO0FBQUMsYUFBT3VGLENBQUMsQ0FBQ0UsT0FBRixDQUFVekYsQ0FBVixFQUFZZ1YsRUFBWixDQUFQO0FBQXVCLEtBQWwwRyxFQUFtMEd0VSxDQUFDLENBQUNWLENBQUQsRUFBRyxJQUFILEVBQVEsQ0FBQztBQUFDaUIsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTSxPQUFOO0FBQWM7QUFBNUMsS0FBRCxFQUErQztBQUFDckUsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTzRQLEVBQVA7QUFBVTtBQUF4QyxLQUEvQyxDQUFSLENBQXAwRyxFQUF1NkdsVixDQUE5Nkc7QUFBZzdHLEdBQTVpSSxFQUE3WDs7QUFBNDZJaUosRUFBQUEsQ0FBQyxDQUFDRyxFQUFGLENBQUtwTyxRQUFMLEVBQWNxYSxFQUFFLENBQUM1SyxjQUFqQixFQUFnQ3VMLEVBQUUsQ0FBQ0UsV0FBbkMsRUFBZ0QsVUFBU2xXLENBQVQsRUFBVztBQUFDLFlBQU1BLENBQUMsQ0FBQ2dKLE1BQUYsQ0FBU2dMLE9BQWYsSUFBd0JoVSxDQUFDLENBQUN5RyxjQUFGLEVBQXhCO0FBQTJDLFFBQUl4RyxDQUFDLEdBQUMwTixFQUFFLENBQUNHLGlCQUFILENBQXFCLElBQXJCLENBQU47QUFBQSxRQUFpQ3BOLENBQUMsR0FBQ2tDLENBQUMsQ0FBQyxJQUFELENBQXBDO0FBQTJDMkIsSUFBQUEsQ0FBQyxDQUFDckosQ0FBQyxDQUFDeU8sSUFBRixDQUFPakosQ0FBUCxDQUFELENBQUQsQ0FBYW1CLE9BQWIsQ0FBc0IsVUFBUzdCLENBQVQsRUFBVztBQUFDLFVBQUlVLENBQUo7QUFBQSxVQUFNM0QsQ0FBQyxHQUFDd0ksQ0FBQyxDQUFDRSxPQUFGLENBQVV6RixDQUFWLEVBQVlnVixFQUFaLENBQVI7QUFBd0JqWSxNQUFBQSxDQUFDLElBQUUsU0FBT0EsQ0FBQyxDQUFDd1osT0FBVCxJQUFrQixZQUFVLE9BQU90VyxDQUFDLENBQUNrVixNQUFyQyxLQUE4Q3BZLENBQUMsQ0FBQ29WLE9BQUYsQ0FBVWdELE1BQVYsR0FBaUJsVixDQUFDLENBQUNrVixNQUFuQixFQUEwQnBZLENBQUMsQ0FBQ3daLE9BQUYsR0FBVXhaLENBQUMsQ0FBQ3laLFVBQUYsRUFBbEYsR0FBa0c5VixDQUFDLEdBQUMsUUFBdEcsSUFBZ0hBLENBQUMsR0FBQ1QsQ0FBbkgsRUFBcUhrVyxFQUFFLENBQUNTLGlCQUFILENBQXFCNVcsQ0FBckIsRUFBdUJVLENBQXZCLENBQXJIO0FBQStJLEtBQXpNO0FBQTRNLEdBQTlWO0FBQWlXLE1BQUlzVyxFQUFFLEdBQUNoUyxDQUFDLEVBQVI7O0FBQVcsTUFBR2dTLEVBQUgsRUFBTTtBQUFDLFFBQUlDLEVBQUUsR0FBQ0QsRUFBRSxDQUFDbEwsRUFBSCxDQUFNaUosRUFBTixDQUFQO0FBQWlCaUMsSUFBQUEsRUFBRSxDQUFDbEwsRUFBSCxDQUFNaUosRUFBTixJQUFVb0IsRUFBRSxDQUFDMUssZUFBYixFQUE2QnVMLEVBQUUsQ0FBQ2xMLEVBQUgsQ0FBTWlKLEVBQU4sRUFBVS9JLFdBQVYsR0FBc0JtSyxFQUFuRCxFQUFzRGEsRUFBRSxDQUFDbEwsRUFBSCxDQUFNaUosRUFBTixFQUFVOUksVUFBVixHQUFxQixZQUFVO0FBQUMsYUFBTytLLEVBQUUsQ0FBQ2xMLEVBQUgsQ0FBTWlKLEVBQU4sSUFBVWtDLEVBQVYsRUFBYWQsRUFBRSxDQUFDMUssZUFBdkI7QUFBdUMsS0FBN0g7QUFBOEg7O0FBQUEsTUFBSXlMLEVBQUUsR0FBQyxVQUFQO0FBQUEsTUFBa0JDLEVBQUUsR0FBQyxhQUFyQjtBQUFBLE1BQW1DQyxFQUFFLEdBQUMsTUFBSUQsRUFBMUM7QUFBQSxNQUE2Q0UsRUFBRSxHQUFDLElBQUlsVCxNQUFKLENBQVcsVUFBWCxDQUFoRDtBQUFBLE1BQXVFbVQsRUFBRSxHQUFDO0FBQUM5QixJQUFBQSxJQUFJLEVBQUMsU0FBTzRCLEVBQWI7QUFBZ0IzQixJQUFBQSxNQUFNLEVBQUMsV0FBUzJCLEVBQWhDO0FBQW1DOUIsSUFBQUEsSUFBSSxFQUFDLFNBQU84QixFQUEvQztBQUFrRDdCLElBQUFBLEtBQUssRUFBQyxVQUFRNkIsRUFBaEU7QUFBbUVHLElBQUFBLEtBQUssRUFBQyxVQUFRSCxFQUFqRjtBQUFvRjNNLElBQUFBLGNBQWMsRUFBQyxVQUFRMk0sRUFBUixHQUFXLFdBQTlHO0FBQTBISSxJQUFBQSxnQkFBZ0IsRUFBQyxZQUFVSixFQUFWLEdBQWEsV0FBeEo7QUFBb0tLLElBQUFBLGNBQWMsRUFBQyxVQUFRTCxFQUFSLEdBQVc7QUFBOUwsR0FBMUU7QUFBQSxNQUFxUk0sRUFBRSxHQUFDLFVBQXhSO0FBQUEsTUFBbVNDLEVBQUUsR0FBQyxNQUF0UztBQUFBLE1BQTZTQyxFQUFFLEdBQUMsUUFBaFQ7QUFBQSxNQUF5VEMsRUFBRSxHQUFDLFdBQTVUO0FBQUEsTUFBd1VDLEVBQUUsR0FBQyxVQUEzVTtBQUFBLE1BQXNWQyxFQUFFLEdBQUMscUJBQXpWO0FBQUEsTUFBK1dDLEVBQUUsR0FBQyxpQkFBbFg7QUFBQSxNQUFvWUMsRUFBRSxHQUFDLDBCQUF2WTtBQUFBLE1BQWthQyxFQUFFLEdBQUMsZ0JBQXJhO0FBQUEsTUFBc2JDLEVBQUUsR0FBQyxnQkFBemI7QUFBQSxNQUEwY0MsRUFBRSxHQUFDLGFBQTdjO0FBQUEsTUFBMmRDLEVBQUUsR0FBQyw2REFBOWQ7QUFBQSxNQUE0aEJDLEVBQUUsR0FBQyxXQUEvaEI7QUFBQSxNQUEyaUJDLEVBQUUsR0FBQyxTQUE5aUI7QUFBQSxNQUF3akJDLEVBQUUsR0FBQyxjQUEzakI7QUFBQSxNQUEwa0JDLEVBQUUsR0FBQyxZQUE3a0I7QUFBQSxNQUEwbEJDLEVBQUUsR0FBQyxhQUE3bEI7QUFBQSxNQUEybUJDLEVBQUUsR0FBQyxZQUE5bUI7QUFBQSxNQUEybkJDLEVBQUUsR0FBQztBQUFDM2MsSUFBQUEsTUFBTSxFQUFDLENBQVI7QUFBVTRjLElBQUFBLElBQUksRUFBQyxDQUFDLENBQWhCO0FBQWtCQyxJQUFBQSxRQUFRLEVBQUMsY0FBM0I7QUFBMENDLElBQUFBLFNBQVMsRUFBQyxRQUFwRDtBQUE2RHBVLElBQUFBLE9BQU8sRUFBQyxTQUFyRTtBQUErRXFVLElBQUFBLFlBQVksRUFBQztBQUE1RixHQUE5bkI7QUFBQSxNQUFndUJDLEVBQUUsR0FBQztBQUFDaGQsSUFBQUEsTUFBTSxFQUFDLDBCQUFSO0FBQW1DNGMsSUFBQUEsSUFBSSxFQUFDLFNBQXhDO0FBQWtEQyxJQUFBQSxRQUFRLEVBQUMsa0JBQTNEO0FBQThFQyxJQUFBQSxTQUFTLEVBQUMsa0JBQXhGO0FBQTJHcFUsSUFBQUEsT0FBTyxFQUFDLFFBQW5IO0FBQTRIcVUsSUFBQUEsWUFBWSxFQUFDO0FBQXpJLEdBQW51QjtBQUFBLE1BQTYzQkUsRUFBRSxHQUFDLFlBQVU7QUFBQyxhQUFTalosQ0FBVCxDQUFXRCxDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLFdBQUs2SyxRQUFMLEdBQWM5SyxDQUFkLEVBQWdCLEtBQUttWixPQUFMLEdBQWEsSUFBN0IsRUFBa0MsS0FBS2hILE9BQUwsR0FBYSxLQUFLQyxVQUFMLENBQWdCblMsQ0FBaEIsQ0FBL0MsRUFBa0UsS0FBS21aLEtBQUwsR0FBVyxLQUFLQyxlQUFMLEVBQTdFLEVBQW9HLEtBQUtDLFNBQUwsR0FBZSxLQUFLQyxhQUFMLEVBQW5ILEVBQXdJLEtBQUszRyxrQkFBTCxFQUF4SSxFQUFrS3JOLENBQUMsQ0FBQ0MsT0FBRixDQUFVeEYsQ0FBVixFQUFZbVgsRUFBWixFQUFlLElBQWYsQ0FBbEs7QUFBdUw7O0FBQUEsUUFBSXBhLENBQUMsR0FBQ2tELENBQUMsQ0FBQ2lCLFNBQVI7QUFBa0IsV0FBT25FLENBQUMsQ0FBQ2lRLE1BQUYsR0FBUyxZQUFVO0FBQUMsVUFBRyxDQUFDLEtBQUtsQyxRQUFMLENBQWMwTyxRQUFmLElBQXlCLENBQUMsS0FBSzFPLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkUsUUFBeEIsQ0FBaUNvTSxFQUFqQyxDQUE3QixFQUFrRTtBQUFDLFlBQUkxWCxDQUFDLEdBQUMsS0FBS29aLEtBQUwsQ0FBV2hPLFNBQVgsQ0FBcUJFLFFBQXJCLENBQThCcU0sRUFBOUIsQ0FBTjs7QUFBd0MxWCxRQUFBQSxDQUFDLENBQUN3WixVQUFGLElBQWV6WixDQUFDLElBQUUsS0FBSzJXLElBQUwsRUFBbEI7QUFBOEI7QUFBQyxLQUE5SixFQUErSjVaLENBQUMsQ0FBQzRaLElBQUYsR0FBTyxZQUFVO0FBQUMsVUFBRyxFQUFFLEtBQUs3TCxRQUFMLENBQWMwTyxRQUFkLElBQXdCLEtBQUsxTyxRQUFMLENBQWNNLFNBQWQsQ0FBd0JFLFFBQXhCLENBQWlDb00sRUFBakMsQ0FBeEIsSUFBOEQsS0FBSzBCLEtBQUwsQ0FBV2hPLFNBQVgsQ0FBcUJFLFFBQXJCLENBQThCcU0sRUFBOUIsQ0FBaEUsQ0FBSCxFQUFzRztBQUFDLFlBQUlqWCxDQUFDLEdBQUNULENBQUMsQ0FBQ3laLG9CQUFGLENBQXVCLEtBQUs1TyxRQUE1QixDQUFOO0FBQUEsWUFBNEMvTixDQUFDLEdBQUM7QUFBQ29YLFVBQUFBLGFBQWEsRUFBQyxLQUFLcko7QUFBcEIsU0FBOUM7O0FBQTRFLFlBQUcsQ0FBQzdCLENBQUMsQ0FBQ00sT0FBRixDQUFVN0ksQ0FBVixFQUFZNFcsRUFBRSxDQUFDaEMsSUFBZixFQUFvQnZZLENBQXBCLEVBQXVCMkosZ0JBQTNCLEVBQTRDO0FBQUMsY0FBRyxDQUFDLEtBQUs0UyxTQUFULEVBQW1CO0FBQUMsZ0JBQUcsZUFBYSxPQUFPdFosQ0FBdkIsRUFBeUIsTUFBTSxJQUFJMFUsU0FBSixDQUFjLGlFQUFkLENBQU47QUFBdUYsZ0JBQUl0VCxDQUFDLEdBQUMsS0FBSzBKLFFBQVg7QUFBb0IseUJBQVcsS0FBS3FILE9BQUwsQ0FBYTRHLFNBQXhCLEdBQWtDM1gsQ0FBQyxHQUFDVixDQUFwQyxHQUFzQzhDLENBQUMsQ0FBQyxLQUFLMk8sT0FBTCxDQUFhNEcsU0FBZCxDQUFELEtBQTRCM1gsQ0FBQyxHQUFDLEtBQUsrUSxPQUFMLENBQWE0RyxTQUFmLEVBQXlCLGVBQWEsT0FBTyxLQUFLNUcsT0FBTCxDQUFhNEcsU0FBYixDQUF1QmhDLE1BQTNDLEtBQW9EM1YsQ0FBQyxHQUFDLEtBQUsrUSxPQUFMLENBQWE0RyxTQUFiLENBQXVCLENBQXZCLENBQXRELENBQXJELENBQXRDLEVBQTZLLG1CQUFpQixLQUFLNUcsT0FBTCxDQUFhMkcsUUFBOUIsSUFBd0NwWSxDQUFDLENBQUMwSyxTQUFGLENBQVlnQyxHQUFaLENBQWdCNEssRUFBaEIsQ0FBck4sRUFBeU8sS0FBS21CLE9BQUwsR0FBYSxJQUFJblosQ0FBSixDQUFNb0IsQ0FBTixFQUFRLEtBQUtnWSxLQUFiLEVBQW1CLEtBQUtPLGdCQUFMLEVBQW5CLENBQXRQO0FBQWtTOztBQUFBLDRCQUFpQjNlLFFBQVEsQ0FBQzRPLGVBQTFCLElBQTJDLENBQUNyRixDQUFDLENBQUNySixDQUFDLENBQUM4SyxPQUFGLENBQVV0RixDQUFWLEVBQVkwWCxFQUFaLENBQUQsQ0FBRCxDQUFtQnpYLE1BQS9ELElBQXVFNEQsQ0FBQyxDQUFDdkosUUFBUSxDQUFDaUssSUFBVCxDQUFjNkUsUUFBZixDQUFELENBQTBCakksT0FBMUIsQ0FBbUMsVUFBUzdCLENBQVQsRUFBVztBQUFDLG1CQUFPaUosQ0FBQyxDQUFDRyxFQUFGLENBQUtwSixDQUFMLEVBQU8sV0FBUCxFQUFtQixJQUFuQixFQUF5QixZQUFVLENBQUUsQ0FBckMsQ0FBUDtBQUErQyxXQUE5RixDQUF2RSxFQUF3SyxLQUFLOEssUUFBTCxDQUFjb0MsS0FBZCxFQUF4SyxFQUE4TCxLQUFLcEMsUUFBTCxDQUFjcUMsWUFBZCxDQUEyQixlQUEzQixFQUEyQyxDQUFDLENBQTVDLENBQTlMLEVBQTZPUSxFQUFFLENBQUNjLFdBQUgsQ0FBZSxLQUFLMkssS0FBcEIsRUFBMEJ6QixFQUExQixDQUE3TyxFQUEyUWhLLEVBQUUsQ0FBQ2MsV0FBSCxDQUFlL04sQ0FBZixFQUFpQmlYLEVBQWpCLENBQTNRLEVBQWdTMU8sQ0FBQyxDQUFDTSxPQUFGLENBQVU3SSxDQUFWLEVBQVk0VyxFQUFFLENBQUMvQixLQUFmLEVBQXFCeFksQ0FBckIsQ0FBaFM7QUFBd1Q7QUFBQztBQUFDLEtBQXJvQyxFQUFzb0NBLENBQUMsQ0FBQzJaLElBQUYsR0FBTyxZQUFVO0FBQUMsVUFBRyxDQUFDLEtBQUs1TCxRQUFMLENBQWMwTyxRQUFmLElBQXlCLENBQUMsS0FBSzFPLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkUsUUFBeEIsQ0FBaUNvTSxFQUFqQyxDQUExQixJQUFnRSxLQUFLMEIsS0FBTCxDQUFXaE8sU0FBWCxDQUFxQkUsUUFBckIsQ0FBOEJxTSxFQUE5QixDQUFuRSxFQUFxRztBQUFDLFlBQUkzWCxDQUFDLEdBQUNDLENBQUMsQ0FBQ3laLG9CQUFGLENBQXVCLEtBQUs1TyxRQUE1QixDQUFOO0FBQUEsWUFBNENwSyxDQUFDLEdBQUM7QUFBQ3lULFVBQUFBLGFBQWEsRUFBQyxLQUFLcko7QUFBcEIsU0FBOUM7QUFBNEU3QixRQUFBQSxDQUFDLENBQUNNLE9BQUYsQ0FBVXZKLENBQVYsRUFBWXNYLEVBQUUsQ0FBQzlCLElBQWYsRUFBb0I5VSxDQUFwQixFQUF1QmdHLGdCQUF2QixLQUEwQyxLQUFLeVMsT0FBTCxJQUFjLEtBQUtBLE9BQUwsQ0FBYVMsT0FBYixFQUFkLEVBQXFDak0sRUFBRSxDQUFDYyxXQUFILENBQWUsS0FBSzJLLEtBQXBCLEVBQTBCekIsRUFBMUIsQ0FBckMsRUFBbUVoSyxFQUFFLENBQUNjLFdBQUgsQ0FBZXpPLENBQWYsRUFBaUIyWCxFQUFqQixDQUFuRSxFQUF3RjFPLENBQUMsQ0FBQ00sT0FBRixDQUFVdkosQ0FBVixFQUFZc1gsRUFBRSxDQUFDN0IsTUFBZixFQUFzQi9VLENBQXRCLENBQWxJO0FBQTRKO0FBQUMsS0FBditDLEVBQXcrQzNELENBQUMsQ0FBQ29PLE9BQUYsR0FBVSxZQUFVO0FBQUM1RixNQUFBQSxDQUFDLENBQUNHLFVBQUYsQ0FBYSxLQUFLb0YsUUFBbEIsRUFBMkJxTSxFQUEzQixHQUErQmxPLENBQUMsQ0FBQ0MsR0FBRixDQUFNLEtBQUs0QixRQUFYLEVBQW9Cc00sRUFBcEIsQ0FBL0IsRUFBdUQsS0FBS3RNLFFBQUwsR0FBYyxJQUFyRSxFQUEwRSxLQUFLc08sS0FBTCxHQUFXLElBQXJGLEVBQTBGLEtBQUtELE9BQUwsS0FBZSxLQUFLQSxPQUFMLENBQWFTLE9BQWIsSUFBdUIsS0FBS1QsT0FBTCxHQUFhLElBQW5ELENBQTFGO0FBQW1KLEtBQWhwRCxFQUFpcERwYyxDQUFDLENBQUM4YyxNQUFGLEdBQVMsWUFBVTtBQUFDLFdBQUtQLFNBQUwsR0FBZSxLQUFLQyxhQUFMLEVBQWYsRUFBb0MsS0FBS0osT0FBTCxJQUFjLEtBQUtBLE9BQUwsQ0FBYVcsY0FBYixFQUFsRDtBQUFnRixLQUFydkQsRUFBc3ZEL2MsQ0FBQyxDQUFDNlYsa0JBQUYsR0FBcUIsWUFBVTtBQUFDLFVBQUk1UyxDQUFDLEdBQUMsSUFBTjtBQUFXaUosTUFBQUEsQ0FBQyxDQUFDRyxFQUFGLENBQUssS0FBSzBCLFFBQVYsRUFBbUJ3TSxFQUFFLENBQUNDLEtBQXRCLEVBQTZCLFVBQVN0WCxDQUFULEVBQVc7QUFBQ0EsUUFBQUEsQ0FBQyxDQUFDd0csY0FBRixJQUFtQnhHLENBQUMsQ0FBQzhaLGVBQUYsRUFBbkIsRUFBdUMvWixDQUFDLENBQUNnTixNQUFGLEVBQXZDO0FBQWtELE9BQTNGO0FBQThGLEtBQS8zRCxFQUFnNERqUSxDQUFDLENBQUNxVixVQUFGLEdBQWEsVUFBU3BTLENBQVQsRUFBVztBQUFDLGFBQU9BLENBQUMsR0FBQzJCLENBQUMsQ0FBQyxFQUFELEVBQUksS0FBS3FZLFdBQUwsQ0FBaUJDLE9BQXJCLEVBQTZCLEVBQTdCLEVBQWdDdE0sRUFBRSxDQUFDRyxpQkFBSCxDQUFxQixLQUFLaEQsUUFBMUIsQ0FBaEMsRUFBb0UsRUFBcEUsRUFBdUU5SyxDQUF2RSxDQUFILEVBQTZFOEQsQ0FBQyxDQUFDb1QsRUFBRCxFQUFJbFgsQ0FBSixFQUFNLEtBQUtnYSxXQUFMLENBQWlCRSxXQUF2QixDQUE5RSxFQUFrSGxhLENBQXpIO0FBQTJILEtBQXBoRSxFQUFxaEVqRCxDQUFDLENBQUNzYyxlQUFGLEdBQWtCLFlBQVU7QUFBQyxVQUFJclosQ0FBQyxHQUFDQyxDQUFDLENBQUN5WixvQkFBRixDQUF1QixLQUFLNU8sUUFBNUIsQ0FBTjtBQUE0QyxhQUFPNVAsQ0FBQyxDQUFDMk8sT0FBRixDQUFVc08sRUFBVixFQUFhblksQ0FBYixDQUFQO0FBQXVCLEtBQXJuRSxFQUFzbkVqRCxDQUFDLENBQUNvZCxhQUFGLEdBQWdCLFlBQVU7QUFBQyxVQUFJbmEsQ0FBQyxHQUFDLEtBQUs4SyxRQUFMLENBQWNwRyxVQUFwQjtBQUFBLFVBQStCekUsQ0FBQyxHQUFDdVksRUFBakM7QUFBb0MsYUFBT3hZLENBQUMsQ0FBQ29MLFNBQUYsQ0FBWUUsUUFBWixDQUFxQnNNLEVBQXJCLEtBQTBCM1gsQ0FBQyxHQUFDcVksRUFBRixFQUFLLEtBQUtjLEtBQUwsQ0FBV2hPLFNBQVgsQ0FBcUJFLFFBQXJCLENBQThCeU0sRUFBOUIsTUFBb0M5WCxDQUFDLEdBQUNzWSxFQUF0QyxDQUEvQixJQUEwRXZZLENBQUMsQ0FBQ29MLFNBQUYsQ0FBWUUsUUFBWixDQUFxQnVNLEVBQXJCLElBQXlCNVgsQ0FBQyxHQUFDeVksRUFBM0IsR0FBOEIxWSxDQUFDLENBQUNvTCxTQUFGLENBQVlFLFFBQVosQ0FBcUJ3TSxFQUFyQixJQUF5QjdYLENBQUMsR0FBQzBZLEVBQTNCLEdBQThCLEtBQUtTLEtBQUwsQ0FBV2hPLFNBQVgsQ0FBcUJFLFFBQXJCLENBQThCeU0sRUFBOUIsTUFBb0M5WCxDQUFDLEdBQUN3WSxFQUF0QyxDQUF0SSxFQUFnTHhZLENBQXZMO0FBQXlMLEtBQTkyRSxFQUErMkVsRCxDQUFDLENBQUN3YyxhQUFGLEdBQWdCLFlBQVU7QUFBQyxhQUFPcFMsT0FBTyxDQUFDak0sQ0FBQyxDQUFDOEssT0FBRixDQUFVLEtBQUs4RSxRQUFmLEVBQXdCLFNBQXhCLENBQUQsQ0FBZDtBQUFtRCxLQUE3N0UsRUFBODdFL04sQ0FBQyxDQUFDcWQsVUFBRixHQUFhLFlBQVU7QUFBQyxVQUFJcGEsQ0FBQyxHQUFDLElBQU47QUFBQSxVQUFXQyxDQUFDLEdBQUMsRUFBYjtBQUFnQixhQUFNLGNBQVksT0FBTyxLQUFLa1MsT0FBTCxDQUFhbFcsTUFBaEMsR0FBdUNnRSxDQUFDLENBQUM2TCxFQUFGLEdBQUssVUFBUzdMLENBQVQsRUFBVztBQUFDLGVBQU9BLENBQUMsQ0FBQ29hLE9BQUYsR0FBVTFZLENBQUMsQ0FBQyxFQUFELEVBQUkxQixDQUFDLENBQUNvYSxPQUFOLEVBQWMsRUFBZCxFQUFpQnJhLENBQUMsQ0FBQ21TLE9BQUYsQ0FBVWxXLE1BQVYsQ0FBaUJnRSxDQUFDLENBQUNvYSxPQUFuQixFQUEyQnJhLENBQUMsQ0FBQzhLLFFBQTdCLEtBQXdDLEVBQXpELENBQVgsRUFBd0U3SyxDQUEvRTtBQUFpRixPQUF6SSxHQUEwSUEsQ0FBQyxDQUFDaEUsTUFBRixHQUFTLEtBQUtrVyxPQUFMLENBQWFsVyxNQUFoSyxFQUF1S2dFLENBQTdLO0FBQStLLEtBQXJwRixFQUFzcEZsRCxDQUFDLENBQUM0YyxnQkFBRixHQUFtQixZQUFVO0FBQUMsVUFBSTNaLENBQUMsR0FBQztBQUFDc2EsUUFBQUEsU0FBUyxFQUFDLEtBQUtILGFBQUwsRUFBWDtBQUFnQ0ksUUFBQUEsU0FBUyxFQUFDO0FBQUN0ZSxVQUFBQSxNQUFNLEVBQUMsS0FBS21lLFVBQUwsRUFBUjtBQUEwQnZCLFVBQUFBLElBQUksRUFBQztBQUFDMkIsWUFBQUEsT0FBTyxFQUFDLEtBQUtySSxPQUFMLENBQWEwRztBQUF0QixXQUEvQjtBQUEyRDRCLFVBQUFBLGVBQWUsRUFBQztBQUFDQyxZQUFBQSxpQkFBaUIsRUFBQyxLQUFLdkksT0FBTCxDQUFhMkc7QUFBaEM7QUFBM0U7QUFBMUMsT0FBTjtBQUF1SyxhQUFNLGFBQVcsS0FBSzNHLE9BQUwsQ0FBYXhOLE9BQXhCLEtBQWtDM0UsQ0FBQyxDQUFDdWEsU0FBRixDQUFZSSxVQUFaLEdBQXVCO0FBQUNILFFBQUFBLE9BQU8sRUFBQyxDQUFDO0FBQVYsT0FBekQsR0FBdUU3WSxDQUFDLENBQUMsRUFBRCxFQUFJM0IsQ0FBSixFQUFNLEVBQU4sRUFBUyxLQUFLbVMsT0FBTCxDQUFhNkcsWUFBdEIsQ0FBOUU7QUFBa0gsS0FBNzhGLEVBQTg4Ri9ZLENBQUMsQ0FBQzJhLGlCQUFGLEdBQW9CLFVBQVM1YSxDQUFULEVBQVdVLENBQVgsRUFBYTtBQUFDLFVBQUkzRCxDQUFDLEdBQUN3SSxDQUFDLENBQUNFLE9BQUYsQ0FBVXpGLENBQVYsRUFBWW1YLEVBQVosQ0FBTjs7QUFBc0IsVUFBR3BhLENBQUMsS0FBR0EsQ0FBQyxHQUFDLElBQUlrRCxDQUFKLENBQU1ELENBQU4sRUFBUSxvQkFBaUJVLENBQWpCLElBQW1CQSxDQUFuQixHQUFxQixJQUE3QixDQUFMLENBQUQsRUFBMEMsWUFBVSxPQUFPQSxDQUE5RCxFQUFnRTtBQUFDLFlBQUcsZUFBYSxPQUFPM0QsQ0FBQyxDQUFDMkQsQ0FBRCxDQUF4QixFQUE0QixNQUFNLElBQUlnVSxTQUFKLENBQWMsc0JBQW9CaFUsQ0FBcEIsR0FBc0IsR0FBcEMsQ0FBTjtBQUErQzNELFFBQUFBLENBQUMsQ0FBQzJELENBQUQsQ0FBRDtBQUFPO0FBQUMsS0FBMXBHLEVBQTJwR1QsQ0FBQyxDQUFDd0wsZUFBRixHQUFrQixVQUFTekwsQ0FBVCxFQUFXO0FBQUMsYUFBTyxLQUFLdkUsSUFBTCxDQUFXLFlBQVU7QUFBQ3dFLFFBQUFBLENBQUMsQ0FBQzJhLGlCQUFGLENBQW9CLElBQXBCLEVBQXlCNWEsQ0FBekI7QUFBNEIsT0FBbEQsQ0FBUDtBQUE0RCxLQUFydkcsRUFBc3ZHQyxDQUFDLENBQUN3WixVQUFGLEdBQWEsVUFBU3paLENBQVQsRUFBVztBQUFDLFVBQUcsQ0FBQ0EsQ0FBRCxJQUFJLE1BQUlBLENBQUMsQ0FBQ29JLEtBQU4sS0FBYyxZQUFVcEksQ0FBQyxDQUFDcUksSUFBWixJQUFrQixNQUFJckksQ0FBQyxDQUFDb0ksS0FBdEMsQ0FBUCxFQUFvRCxLQUFJLElBQUkxSCxDQUFDLEdBQUM2RCxDQUFDLENBQUNySixDQUFDLENBQUN5TyxJQUFGLENBQU9zTyxFQUFQLENBQUQsQ0FBUCxFQUFvQmxiLENBQUMsR0FBQyxDQUF0QixFQUF3QnFFLENBQUMsR0FBQ1YsQ0FBQyxDQUFDQyxNQUFoQyxFQUF1QzVELENBQUMsR0FBQ3FFLENBQXpDLEVBQTJDckUsQ0FBQyxFQUE1QyxFQUErQztBQUFDLFlBQUk0RSxDQUFDLEdBQUMxQixDQUFDLENBQUN5WixvQkFBRixDQUF1QmhaLENBQUMsQ0FBQzNELENBQUQsQ0FBeEIsQ0FBTjtBQUFBLFlBQW1Da0YsQ0FBQyxHQUFDc0QsQ0FBQyxDQUFDRSxPQUFGLENBQVUvRSxDQUFDLENBQUMzRCxDQUFELENBQVgsRUFBZW9hLEVBQWYsQ0FBckM7QUFBQSxZQUF3RGpWLENBQUMsR0FBQztBQUFDaVMsVUFBQUEsYUFBYSxFQUFDelQsQ0FBQyxDQUFDM0QsQ0FBRDtBQUFoQixTQUExRDs7QUFBK0UsWUFBR2lELENBQUMsSUFBRSxZQUFVQSxDQUFDLENBQUNxSSxJQUFmLEtBQXNCbkcsQ0FBQyxDQUFDMlksVUFBRixHQUFhN2EsQ0FBbkMsR0FBc0NpQyxDQUF6QyxFQUEyQztBQUFDLGNBQUlFLENBQUMsR0FBQ0YsQ0FBQyxDQUFDbVgsS0FBUjtBQUFjLGNBQUd6WCxDQUFDLENBQUN5SixTQUFGLENBQVlFLFFBQVosQ0FBcUJxTSxFQUFyQixDQUFILEVBQTRCLElBQUcsRUFBRTNYLENBQUMsS0FBRyxZQUFVQSxDQUFDLENBQUNxSSxJQUFaLElBQWtCLGtCQUFrQmpFLElBQWxCLENBQXVCcEUsQ0FBQyxDQUFDZ0osTUFBRixDQUFTZ0wsT0FBaEMsQ0FBbEIsSUFBNEQsWUFBVWhVLENBQUMsQ0FBQ3FJLElBQVosSUFBa0IsTUFBSXJJLENBQUMsQ0FBQ29JLEtBQXZGLENBQUQsSUFBZ0d6RyxDQUFDLENBQUMySixRQUFGLENBQVd0TCxDQUFDLENBQUNnSixNQUFiLENBQWxHLENBQUgsRUFBMkhDLENBQUMsQ0FBQ00sT0FBRixDQUFVNUgsQ0FBVixFQUFZMlYsRUFBRSxDQUFDOUIsSUFBZixFQUFvQnRULENBQXBCLEVBQXVCd0UsZ0JBQXZCLEtBQTBDLGtCQUFpQjFMLFFBQVEsQ0FBQzRPLGVBQTFCLElBQTJDckYsQ0FBQyxDQUFDdkosUUFBUSxDQUFDaUssSUFBVCxDQUFjNkUsUUFBZixDQUFELENBQTBCakksT0FBMUIsQ0FBbUMsVUFBUzdCLENBQVQsRUFBVztBQUFDLG1CQUFPaUosQ0FBQyxDQUFDQyxHQUFGLENBQU1sSixDQUFOLEVBQVEsV0FBUixFQUFvQixJQUFwQixFQUEwQixZQUFVLENBQUUsQ0FBdEMsQ0FBUDtBQUFnRCxXQUEvRixDQUEzQyxFQUE2SVUsQ0FBQyxDQUFDM0QsQ0FBRCxDQUFELENBQUtvUSxZQUFMLENBQWtCLGVBQWxCLEVBQWtDLE9BQWxDLENBQTdJLEVBQXdMbEwsQ0FBQyxDQUFDa1gsT0FBRixJQUFXbFgsQ0FBQyxDQUFDa1gsT0FBRixDQUFVUyxPQUFWLEVBQW5NLEVBQXVOelgsQ0FBQyxDQUFDaUosU0FBRixDQUFZQyxNQUFaLENBQW1Cc00sRUFBbkIsQ0FBdk4sRUFBOE9oVyxDQUFDLENBQUN5SixTQUFGLENBQVlDLE1BQVosQ0FBbUJzTSxFQUFuQixDQUE5TyxFQUFxUTFPLENBQUMsQ0FBQ00sT0FBRixDQUFVNUgsQ0FBVixFQUFZMlYsRUFBRSxDQUFDN0IsTUFBZixFQUFzQnZULENBQXRCLENBQS9TO0FBQXlVO0FBQUM7QUFBQyxLQUE5OUgsRUFBKzlIakMsQ0FBQyxDQUFDeVosb0JBQUYsR0FBdUIsVUFBUzFaLENBQVQsRUFBVztBQUFDLGFBQU82QyxDQUFDLENBQUM3QyxDQUFELENBQUQsSUFBTUEsQ0FBQyxDQUFDMEUsVUFBZjtBQUEwQixLQUE1aEksRUFBNmhJekUsQ0FBQyxDQUFDNmEscUJBQUYsR0FBd0IsVUFBUzlhLENBQVQsRUFBVztBQUFDLFVBQUcsQ0FBQyxrQkFBa0JvRSxJQUFsQixDQUF1QnBFLENBQUMsQ0FBQ2dKLE1BQUYsQ0FBU2dMLE9BQWhDLElBQXlDLEVBQUUsT0FBS2hVLENBQUMsQ0FBQ29JLEtBQVAsSUFBYyxPQUFLcEksQ0FBQyxDQUFDb0ksS0FBUCxLQUFlLE9BQUtwSSxDQUFDLENBQUNvSSxLQUFQLElBQWMsT0FBS3BJLENBQUMsQ0FBQ29JLEtBQXJCLElBQTRCbE4sQ0FBQyxDQUFDOEssT0FBRixDQUFVaEcsQ0FBQyxDQUFDZ0osTUFBWixFQUFtQm1QLEVBQW5CLENBQTNDLENBQWhCLENBQXpDLEdBQTZIZCxFQUFFLENBQUNqVCxJQUFILENBQVFwRSxDQUFDLENBQUNvSSxLQUFWLENBQTlILE1BQWtKcEksQ0FBQyxDQUFDeUcsY0FBRixJQUFtQnpHLENBQUMsQ0FBQytaLGVBQUYsRUFBbkIsRUFBdUMsQ0FBQyxLQUFLUCxRQUFOLElBQWdCLENBQUMsS0FBS3BPLFNBQUwsQ0FBZUUsUUFBZixDQUF3Qm9NLEVBQXhCLENBQTFNLENBQUgsRUFBME87QUFBQyxZQUFJaFgsQ0FBQyxHQUFDVCxDQUFDLENBQUN5WixvQkFBRixDQUF1QixJQUF2QixDQUFOO0FBQUEsWUFBbUMzYyxDQUFDLEdBQUMyRCxDQUFDLENBQUMwSyxTQUFGLENBQVlFLFFBQVosQ0FBcUJxTSxFQUFyQixDQUFyQztBQUE4RCxZQUFHLENBQUM1YSxDQUFELElBQUlBLENBQUMsS0FBRyxPQUFLaUQsQ0FBQyxDQUFDb0ksS0FBUCxJQUFjLE9BQUtwSSxDQUFDLENBQUNvSSxLQUF4QixDQUFSLEVBQXVDLE9BQU8sT0FBS3BJLENBQUMsQ0FBQ29JLEtBQVAsSUFBY2xOLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVW9PLEVBQVYsRUFBYXZYLENBQWIsRUFBZ0J3TSxLQUFoQixFQUFkLEVBQXNDLEtBQUtqTixDQUFDLENBQUN3WixVQUFGLEVBQWxEO0FBQWlFLFlBQUlyWSxDQUFDLEdBQUNtRCxDQUFDLENBQUNySixDQUFDLENBQUN5TyxJQUFGLENBQU8wTyxFQUFQLEVBQVUzWCxDQUFWLENBQUQsQ0FBRCxDQUFnQmEsTUFBaEIsQ0FBdUJ4RCxDQUF2QixDQUFOOztBQUFnQyxZQUFHcUQsQ0FBQyxDQUFDVCxNQUFMLEVBQVk7QUFBQyxjQUFJZ0IsQ0FBQyxHQUFDUCxDQUFDLENBQUN5SCxPQUFGLENBQVU3SSxDQUFDLENBQUNnSixNQUFaLENBQU47QUFBMEIsaUJBQUtoSixDQUFDLENBQUNvSSxLQUFQLElBQWN6RyxDQUFDLEdBQUMsQ0FBaEIsSUFBbUJBLENBQUMsRUFBcEIsRUFBdUIsT0FBSzNCLENBQUMsQ0FBQ29JLEtBQVAsSUFBY3pHLENBQUMsR0FBQ1AsQ0FBQyxDQUFDVCxNQUFGLEdBQVMsQ0FBekIsSUFBNEJnQixDQUFDLEVBQXBELEVBQXVEQSxDQUFDLEdBQUMsQ0FBRixLQUFNQSxDQUFDLEdBQUMsQ0FBUixDQUF2RCxFQUFrRVAsQ0FBQyxDQUFDTyxDQUFELENBQUQsQ0FBS3VMLEtBQUwsRUFBbEU7QUFBK0U7QUFBQztBQUFDLEtBQTFtSixFQUEybUpqTixDQUFDLENBQUMwTCxXQUFGLEdBQWMsVUFBUzNMLENBQVQsRUFBVztBQUFDLGFBQU91RixDQUFDLENBQUNFLE9BQUYsQ0FBVXpGLENBQVYsRUFBWW1YLEVBQVosQ0FBUDtBQUF1QixLQUE1cEosRUFBNnBKelcsQ0FBQyxDQUFDVCxDQUFELEVBQUcsSUFBSCxFQUFRLENBQUM7QUFBQ2dCLE1BQUFBLEdBQUcsRUFBQyxTQUFMO0FBQWVxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU0sT0FBTjtBQUFjO0FBQTVDLEtBQUQsRUFBK0M7QUFBQ3JFLE1BQUFBLEdBQUcsRUFBQyxTQUFMO0FBQWVxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU9zVCxFQUFQO0FBQVU7QUFBeEMsS0FBL0MsRUFBeUY7QUFBQzNYLE1BQUFBLEdBQUcsRUFBQyxhQUFMO0FBQW1CcUUsTUFBQUEsR0FBRyxFQUFDLGVBQVU7QUFBQyxlQUFPMlQsRUFBUDtBQUFVO0FBQTVDLEtBQXpGLENBQVIsQ0FBOXBKLEVBQSt5SmhaLENBQXR6SjtBQUF3ekosR0FBNWhLLEVBQWg0Qjs7QUFBKzVMZ0osRUFBQUEsQ0FBQyxDQUFDRyxFQUFGLENBQUtwTyxRQUFMLEVBQWNzYyxFQUFFLENBQUNFLGdCQUFqQixFQUFrQ1MsRUFBbEMsRUFBcUNpQixFQUFFLENBQUM0QixxQkFBeEMsR0FBK0Q3UixDQUFDLENBQUNHLEVBQUYsQ0FBS3BPLFFBQUwsRUFBY3NjLEVBQUUsQ0FBQ0UsZ0JBQWpCLEVBQWtDVyxFQUFsQyxFQUFxQ2UsRUFBRSxDQUFDNEIscUJBQXhDLENBQS9ELEVBQThIN1IsQ0FBQyxDQUFDRyxFQUFGLENBQUtwTyxRQUFMLEVBQWNzYyxFQUFFLENBQUM3TSxjQUFqQixFQUFnQ3lPLEVBQUUsQ0FBQ08sVUFBbkMsQ0FBOUgsRUFBNkt4USxDQUFDLENBQUNHLEVBQUYsQ0FBS3BPLFFBQUwsRUFBY3NjLEVBQUUsQ0FBQ0csY0FBakIsRUFBZ0N5QixFQUFFLENBQUNPLFVBQW5DLENBQTdLLEVBQTROeFEsQ0FBQyxDQUFDRyxFQUFGLENBQUtwTyxRQUFMLEVBQWNzYyxFQUFFLENBQUM3TSxjQUFqQixFQUFnQ3dOLEVBQWhDLEVBQW9DLFVBQVNqWSxDQUFULEVBQVc7QUFBQ0EsSUFBQUEsQ0FBQyxDQUFDeUcsY0FBRixJQUFtQnpHLENBQUMsQ0FBQytaLGVBQUYsRUFBbkIsRUFBdUNiLEVBQUUsQ0FBQzBCLGlCQUFILENBQXFCLElBQXJCLEVBQTBCLFFBQTFCLENBQXZDO0FBQTJFLEdBQTNILENBQTVOLEVBQTBWM1IsQ0FBQyxDQUFDRyxFQUFGLENBQUtwTyxRQUFMLEVBQWNzYyxFQUFFLENBQUM3TSxjQUFqQixFQUFnQ3lOLEVBQWhDLEVBQW9DLFVBQVNsWSxDQUFULEVBQVc7QUFBQyxXQUFPQSxDQUFDLENBQUMrWixlQUFGLEVBQVA7QUFBMkIsR0FBM0UsQ0FBMVY7QUFBd2EsTUFBSWdCLEVBQUUsR0FBQy9WLENBQUMsRUFBUjs7QUFBVyxNQUFHK1YsRUFBSCxFQUFNO0FBQUMsUUFBSUMsRUFBRSxHQUFDRCxFQUFFLENBQUNqUCxFQUFILENBQU1vTCxFQUFOLENBQVA7QUFBaUI2RCxJQUFBQSxFQUFFLENBQUNqUCxFQUFILENBQU1vTCxFQUFOLElBQVVnQyxFQUFFLENBQUN6TixlQUFiLEVBQTZCc1AsRUFBRSxDQUFDalAsRUFBSCxDQUFNb0wsRUFBTixFQUFVbEwsV0FBVixHQUFzQmtOLEVBQW5ELEVBQXNENkIsRUFBRSxDQUFDalAsRUFBSCxDQUFNb0wsRUFBTixFQUFVakwsVUFBVixHQUFxQixZQUFVO0FBQUMsYUFBTzhPLEVBQUUsQ0FBQ2pQLEVBQUgsQ0FBTW9MLEVBQU4sSUFBVThELEVBQVYsRUFBYTlCLEVBQUUsQ0FBQ3pOLGVBQXZCO0FBQXVDLEtBQTdIO0FBQThIOztBQUFBLE1BQUl3UCxFQUFFLEdBQUMsVUFBUDtBQUFBLE1BQWtCQyxFQUFFLEdBQUMsTUFBSUQsRUFBekI7QUFBQSxNQUE0QkUsRUFBRSxHQUFDO0FBQUNDLElBQUFBLFFBQVEsRUFBQyxDQUFDLENBQVg7QUFBYXJNLElBQUFBLFFBQVEsRUFBQyxDQUFDLENBQXZCO0FBQXlCN0IsSUFBQUEsS0FBSyxFQUFDLENBQUMsQ0FBaEM7QUFBa0N5SixJQUFBQSxJQUFJLEVBQUMsQ0FBQztBQUF4QyxHQUEvQjtBQUFBLE1BQTBFMEUsRUFBRSxHQUFDO0FBQUNELElBQUFBLFFBQVEsRUFBQyxrQkFBVjtBQUE2QnJNLElBQUFBLFFBQVEsRUFBQyxTQUF0QztBQUFnRDdCLElBQUFBLEtBQUssRUFBQyxTQUF0RDtBQUFnRXlKLElBQUFBLElBQUksRUFBQztBQUFyRSxHQUE3RTtBQUFBLE1BQTZKMkUsRUFBRSxHQUFDO0FBQUM5RixJQUFBQSxJQUFJLEVBQUMsU0FBTzBGLEVBQWI7QUFBZ0JLLElBQUFBLGNBQWMsRUFBQyxrQkFBZ0JMLEVBQS9DO0FBQWtEekYsSUFBQUEsTUFBTSxFQUFDLFdBQVN5RixFQUFsRTtBQUFxRTVGLElBQUFBLElBQUksRUFBQyxTQUFPNEYsRUFBakY7QUFBb0YzRixJQUFBQSxLQUFLLEVBQUMsVUFBUTJGLEVBQWxHO0FBQXFHTSxJQUFBQSxPQUFPLEVBQUMsWUFBVU4sRUFBdkg7QUFBMEhPLElBQUFBLE1BQU0sRUFBQyxXQUFTUCxFQUExSTtBQUE2SVEsSUFBQUEsYUFBYSxFQUFDLGtCQUFnQlIsRUFBM0s7QUFBOEtTLElBQUFBLGVBQWUsRUFBQyxvQkFBa0JULEVBQWhOO0FBQW1OVSxJQUFBQSxlQUFlLEVBQUMsb0JBQWtCVixFQUFyUDtBQUF3UFcsSUFBQUEsaUJBQWlCLEVBQUMsc0JBQW9CWCxFQUE5UjtBQUFpU3pRLElBQUFBLGNBQWMsRUFBQyxVQUFReVEsRUFBUixHQUFXO0FBQTNULEdBQWhLO0FBQUEsTUFBd2VZLEVBQUUsR0FBQyx5QkFBM2U7QUFBQSxNQUFxZ0JDLEVBQUUsR0FBQyx5QkFBeGdCO0FBQUEsTUFBa2lCQyxFQUFFLEdBQUMsZ0JBQXJpQjtBQUFBLE1BQXNqQkMsRUFBRSxHQUFDLFlBQXpqQjtBQUFBLE1BQXNrQkMsRUFBRSxHQUFDLE1BQXprQjtBQUFBLE1BQWdsQkMsRUFBRSxHQUFDLE1BQW5sQjtBQUFBLE1BQTBsQkMsRUFBRSxHQUFDLGNBQTdsQjtBQUFBLE1BQTRtQkMsRUFBRSxHQUFDO0FBQUNDLElBQUFBLE1BQU0sRUFBQyxlQUFSO0FBQXdCQyxJQUFBQSxVQUFVLEVBQUMsYUFBbkM7QUFBaURyRyxJQUFBQSxXQUFXLEVBQUMsdUJBQTdEO0FBQXFGc0csSUFBQUEsWUFBWSxFQUFDLHdCQUFsRztBQUEySEMsSUFBQUEsYUFBYSxFQUFDLG1EQUF6STtBQUE2TEMsSUFBQUEsY0FBYyxFQUFDO0FBQTVNLEdBQS9tQjtBQUFBLE1BQTAwQkMsRUFBRSxHQUFDLFlBQVU7QUFBQyxhQUFTM2MsQ0FBVCxDQUFXQSxDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLFdBQUtrUyxPQUFMLEdBQWEsS0FBS0MsVUFBTCxDQUFnQm5TLENBQWhCLENBQWIsRUFBZ0MsS0FBSzZLLFFBQUwsR0FBYzlLLENBQTlDLEVBQWdELEtBQUs0YyxPQUFMLEdBQWExaEIsQ0FBQyxDQUFDMk8sT0FBRixDQUFVd1MsRUFBRSxDQUFDQyxNQUFiLEVBQW9CdGMsQ0FBcEIsQ0FBN0QsRUFBb0YsS0FBSzZjLFNBQUwsR0FBZSxJQUFuRyxFQUF3RyxLQUFLQyxRQUFMLEdBQWMsQ0FBQyxDQUF2SCxFQUF5SCxLQUFLQyxrQkFBTCxHQUF3QixDQUFDLENBQWxKLEVBQW9KLEtBQUtDLG9CQUFMLEdBQTBCLENBQUMsQ0FBL0ssRUFBaUwsS0FBSzVHLGdCQUFMLEdBQXNCLENBQUMsQ0FBeE0sRUFBME0sS0FBSzZHLGVBQUwsR0FBcUIsQ0FBL04sRUFBaU8xWCxDQUFDLENBQUNDLE9BQUYsQ0FBVXhGLENBQVYsRUFBWWliLEVBQVosRUFBZSxJQUFmLENBQWpPO0FBQXNQOztBQUFBLFFBQUloYixDQUFDLEdBQUNELENBQUMsQ0FBQ2tCLFNBQVI7QUFBa0IsV0FBT2pCLENBQUMsQ0FBQytNLE1BQUYsR0FBUyxVQUFTaE4sQ0FBVCxFQUFXO0FBQUMsYUFBTyxLQUFLOGMsUUFBTCxHQUFjLEtBQUtwRyxJQUFMLEVBQWQsR0FBMEIsS0FBS0MsSUFBTCxDQUFVM1csQ0FBVixDQUFqQztBQUE4QyxLQUFuRSxFQUFvRUMsQ0FBQyxDQUFDMFcsSUFBRixHQUFPLFVBQVMzVyxDQUFULEVBQVc7QUFBQyxVQUFJQyxDQUFDLEdBQUMsSUFBTjs7QUFBVyxVQUFHLENBQUMsS0FBSzZjLFFBQU4sSUFBZ0IsQ0FBQyxLQUFLMUcsZ0JBQXpCLEVBQTBDO0FBQUMsYUFBS3RMLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkUsUUFBeEIsQ0FBaUM0USxFQUFqQyxNQUF1QyxLQUFLOUYsZ0JBQUwsR0FBc0IsQ0FBQyxDQUE5RDtBQUFpRSxZQUFJMVYsQ0FBQyxHQUFDdUksQ0FBQyxDQUFDTSxPQUFGLENBQVUsS0FBS3VCLFFBQWYsRUFBd0J3USxFQUFFLENBQUNoRyxJQUEzQixFQUFnQztBQUFDbkIsVUFBQUEsYUFBYSxFQUFDblU7QUFBZixTQUFoQyxDQUFOO0FBQXlELGFBQUs4YyxRQUFMLElBQWVwYyxDQUFDLENBQUNnRyxnQkFBakIsS0FBb0MsS0FBS29XLFFBQUwsR0FBYyxDQUFDLENBQWYsRUFBaUIsS0FBS0ksZUFBTCxFQUFqQixFQUF3QyxLQUFLQyxhQUFMLEVBQXhDLEVBQTZELEtBQUtDLGFBQUwsRUFBN0QsRUFBa0YsS0FBS0MsZUFBTCxFQUFsRixFQUF5RyxLQUFLQyxlQUFMLEVBQXpHLEVBQWdJclUsQ0FBQyxDQUFDRyxFQUFGLENBQUssS0FBSzBCLFFBQVYsRUFBbUJ3USxFQUFFLENBQUNJLGFBQXRCLEVBQW9DVyxFQUFFLENBQUNHLFlBQXZDLEVBQXFELFVBQVN4YyxDQUFULEVBQVc7QUFBQyxpQkFBT0MsQ0FBQyxDQUFDeVcsSUFBRixDQUFPMVcsQ0FBUCxDQUFQO0FBQWlCLFNBQWxGLENBQWhJLEVBQXFOaUosQ0FBQyxDQUFDRyxFQUFGLENBQUssS0FBS3dULE9BQVYsRUFBa0J0QixFQUFFLENBQUNPLGlCQUFyQixFQUF3QyxZQUFVO0FBQUM1UyxVQUFBQSxDQUFDLENBQUNJLEdBQUYsQ0FBTXBKLENBQUMsQ0FBQzZLLFFBQVIsRUFBaUJ3USxFQUFFLENBQUNNLGVBQXBCLEVBQXFDLFVBQVM1YixDQUFULEVBQVc7QUFBQ0EsWUFBQUEsQ0FBQyxDQUFDZ0osTUFBRixLQUFXL0ksQ0FBQyxDQUFDNkssUUFBYixLQUF3QjdLLENBQUMsQ0FBQytjLG9CQUFGLEdBQXVCLENBQUMsQ0FBaEQ7QUFBbUQsV0FBcEc7QUFBdUcsU0FBMUosQ0FBck4sRUFBa1gsS0FBS08sYUFBTCxDQUFvQixZQUFVO0FBQUMsaUJBQU90ZCxDQUFDLENBQUN1ZCxZQUFGLENBQWV4ZCxDQUFmLENBQVA7QUFBeUIsU0FBeEQsQ0FBdFo7QUFBa2Q7QUFBQyxLQUExdEIsRUFBMnRCQyxDQUFDLENBQUN5VyxJQUFGLEdBQU8sVUFBUzFXLENBQVQsRUFBVztBQUFDLFVBQUlDLENBQUMsR0FBQyxJQUFOOztBQUFXLFVBQUcsQ0FBQ0QsQ0FBQyxJQUFFQSxDQUFDLENBQUN5RyxjQUFGLEVBQUgsRUFBc0IsS0FBS3FXLFFBQUwsSUFBZSxDQUFDLEtBQUsxRyxnQkFBNUMsS0FBK0QsQ0FBQ25OLENBQUMsQ0FBQ00sT0FBRixDQUFVLEtBQUt1QixRQUFmLEVBQXdCd1EsRUFBRSxDQUFDOUYsSUFBM0IsRUFBaUM5TyxnQkFBcEcsRUFBcUg7QUFBQyxhQUFLb1csUUFBTCxHQUFjLENBQUMsQ0FBZjs7QUFBaUIsWUFBSXBjLENBQUMsR0FBQyxLQUFLb0ssUUFBTCxDQUFjTSxTQUFkLENBQXdCRSxRQUF4QixDQUFpQzRRLEVBQWpDLENBQU47O0FBQTJDLFlBQUd4YixDQUFDLEtBQUcsS0FBSzBWLGdCQUFMLEdBQXNCLENBQUMsQ0FBMUIsQ0FBRCxFQUE4QixLQUFLaUgsZUFBTCxFQUE5QixFQUFxRCxLQUFLQyxlQUFMLEVBQXJELEVBQTRFclUsQ0FBQyxDQUFDQyxHQUFGLENBQU1sTyxRQUFOLEVBQWVzZ0IsRUFBRSxDQUFDRSxPQUFsQixDQUE1RSxFQUF1RyxLQUFLMVEsUUFBTCxDQUFjTSxTQUFkLENBQXdCQyxNQUF4QixDQUErQjhRLEVBQS9CLENBQXZHLEVBQTBJbFQsQ0FBQyxDQUFDQyxHQUFGLENBQU0sS0FBSzRCLFFBQVgsRUFBb0J3USxFQUFFLENBQUNJLGFBQXZCLENBQTFJLEVBQWdMelMsQ0FBQyxDQUFDQyxHQUFGLENBQU0sS0FBSzBULE9BQVgsRUFBbUJ0QixFQUFFLENBQUNPLGlCQUF0QixDQUFoTCxFQUF5Tm5iLENBQTVOLEVBQThOO0FBQUMsY0FBSTNELENBQUMsR0FBQytGLENBQUMsQ0FBQyxLQUFLZ0ksUUFBTixDQUFQO0FBQXVCN0IsVUFBQUEsQ0FBQyxDQUFDSSxHQUFGLENBQU0sS0FBS3lCLFFBQVgsRUFBb0IxSSxDQUFwQixFQUF1QixVQUFTcEMsQ0FBVCxFQUFXO0FBQUMsbUJBQU9DLENBQUMsQ0FBQ3dkLFVBQUYsQ0FBYXpkLENBQWIsQ0FBUDtBQUF1QixXQUExRCxHQUE2RDBELENBQUMsQ0FBQyxLQUFLb0gsUUFBTixFQUFlL04sQ0FBZixDQUE5RDtBQUFnRixTQUF0VSxNQUEyVSxLQUFLMGdCLFVBQUw7QUFBa0I7QUFBQyxLQUF6d0MsRUFBMHdDeGQsQ0FBQyxDQUFDa0wsT0FBRixHQUFVLFlBQVU7QUFBQyxPQUFDdk0sTUFBRCxFQUFRLEtBQUtrTSxRQUFiLEVBQXNCLEtBQUs4UixPQUEzQixFQUFvQy9hLE9BQXBDLENBQTZDLFVBQVM3QixDQUFULEVBQVc7QUFBQyxlQUFPaUosQ0FBQyxDQUFDQyxHQUFGLENBQU1sSixDQUFOLEVBQVFrYixFQUFSLENBQVA7QUFBbUIsT0FBNUUsR0FBK0VqUyxDQUFDLENBQUNDLEdBQUYsQ0FBTWxPLFFBQU4sRUFBZXNnQixFQUFFLENBQUNFLE9BQWxCLENBQS9FLEVBQTBHalcsQ0FBQyxDQUFDRyxVQUFGLENBQWEsS0FBS29GLFFBQWxCLEVBQTJCbVEsRUFBM0IsQ0FBMUcsRUFBeUksS0FBSzlJLE9BQUwsR0FBYSxJQUF0SixFQUEySixLQUFLckgsUUFBTCxHQUFjLElBQXpLLEVBQThLLEtBQUs4UixPQUFMLEdBQWEsSUFBM0wsRUFBZ00sS0FBS0MsU0FBTCxHQUFlLElBQS9NLEVBQW9OLEtBQUtDLFFBQUwsR0FBYyxJQUFsTyxFQUF1TyxLQUFLQyxrQkFBTCxHQUF3QixJQUEvUCxFQUFvUSxLQUFLQyxvQkFBTCxHQUEwQixJQUE5UixFQUFtUyxLQUFLNUcsZ0JBQUwsR0FBc0IsSUFBelQsRUFBOFQsS0FBSzZHLGVBQUwsR0FBcUIsSUFBblY7QUFBd1YsS0FBdm5ELEVBQXduRGhkLENBQUMsQ0FBQ3lkLFlBQUYsR0FBZSxZQUFVO0FBQUMsV0FBS04sYUFBTDtBQUFxQixLQUF2cUQsRUFBd3FEbmQsQ0FBQyxDQUFDbVMsVUFBRixHQUFhLFVBQVNwUyxDQUFULEVBQVc7QUFBQyxhQUFPQSxDQUFDLEdBQUMyQixDQUFDLENBQUMsRUFBRCxFQUFJd1osRUFBSixFQUFPLEVBQVAsRUFBVW5iLENBQVYsQ0FBSCxFQUFnQjhELENBQUMsQ0FBQyxPQUFELEVBQVM5RCxDQUFULEVBQVdxYixFQUFYLENBQWpCLEVBQWdDcmIsQ0FBdkM7QUFBeUMsS0FBMXVELEVBQTJ1REMsQ0FBQyxDQUFDdWQsWUFBRixHQUFlLFVBQVN4ZCxDQUFULEVBQVc7QUFBQyxVQUFJQyxDQUFDLEdBQUMsSUFBTjtBQUFBLFVBQVdTLENBQUMsR0FBQyxLQUFLb0ssUUFBTCxDQUFjTSxTQUFkLENBQXdCRSxRQUF4QixDQUFpQzRRLEVBQWpDLENBQWI7QUFBQSxVQUFrRG5mLENBQUMsR0FBQzdCLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVXdTLEVBQUUsQ0FBQ0UsVUFBYixFQUF3QixLQUFLSyxPQUE3QixDQUFwRDs7QUFBMEYsV0FBSzlSLFFBQUwsQ0FBY3BHLFVBQWQsSUFBMEIsS0FBS29HLFFBQUwsQ0FBY3BHLFVBQWQsQ0FBeUJqQixRQUF6QixLQUFvQ3VHLElBQUksQ0FBQ0MsWUFBbkUsSUFBaUZqUCxRQUFRLENBQUNpSyxJQUFULENBQWMwWSxXQUFkLENBQTBCLEtBQUs3UyxRQUEvQixDQUFqRixFQUEwSCxLQUFLQSxRQUFMLENBQWNyRyxLQUFkLENBQW9CRSxPQUFwQixHQUE0QixPQUF0SixFQUE4SixLQUFLbUcsUUFBTCxDQUFjekQsZUFBZCxDQUE4QixhQUE5QixDQUE5SixFQUEyTSxLQUFLeUQsUUFBTCxDQUFjcUMsWUFBZCxDQUEyQixZQUEzQixFQUF3QyxDQUFDLENBQXpDLENBQTNNLEVBQXVQLEtBQUt5UCxPQUFMLENBQWF4UixTQUFiLENBQXVCRSxRQUF2QixDQUFnQ3dRLEVBQWhDLEtBQXFDL2UsQ0FBckMsR0FBdUNBLENBQUMsQ0FBQ29SLFNBQUYsR0FBWSxDQUFuRCxHQUFxRCxLQUFLckQsUUFBTCxDQUFjcUQsU0FBZCxHQUF3QixDQUFwVSxFQUFzVXpOLENBQUMsSUFBRW9FLENBQUMsQ0FBQyxLQUFLZ0csUUFBTixDQUExVSxFQUEwVixLQUFLQSxRQUFMLENBQWNNLFNBQWQsQ0FBd0JnQyxHQUF4QixDQUE0QitPLEVBQTVCLENBQTFWLEVBQTBYLEtBQUtoSyxPQUFMLENBQWFqRixLQUFiLElBQW9CLEtBQUswUSxhQUFMLEVBQTlZOztBQUFtYSxVQUFJeGMsQ0FBQyxHQUFDLFNBQUZBLENBQUUsR0FBVTtBQUFDbkIsUUFBQUEsQ0FBQyxDQUFDa1MsT0FBRixDQUFVakYsS0FBVixJQUFpQmpOLENBQUMsQ0FBQzZLLFFBQUYsQ0FBV29DLEtBQVgsRUFBakIsRUFBb0NqTixDQUFDLENBQUNtVyxnQkFBRixHQUFtQixDQUFDLENBQXhELEVBQTBEbk4sQ0FBQyxDQUFDTSxPQUFGLENBQVV0SixDQUFDLENBQUM2SyxRQUFaLEVBQXFCd1EsRUFBRSxDQUFDL0YsS0FBeEIsRUFBOEI7QUFBQ3BCLFVBQUFBLGFBQWEsRUFBQ25VO0FBQWYsU0FBOUIsQ0FBMUQ7QUFBMkcsT0FBNUg7O0FBQTZILFVBQUdVLENBQUgsRUFBSztBQUFDLFlBQUlpQixDQUFDLEdBQUNtQixDQUFDLENBQUMsS0FBSzhaLE9BQU4sQ0FBUDtBQUFzQjNULFFBQUFBLENBQUMsQ0FBQ0ksR0FBRixDQUFNLEtBQUt1VCxPQUFYLEVBQW1CeGEsQ0FBbkIsRUFBcUJoQixDQUFyQixHQUF3QnNDLENBQUMsQ0FBQyxLQUFLa1osT0FBTixFQUFjamIsQ0FBZCxDQUF6QjtBQUEwQyxPQUF0RSxNQUEyRVAsQ0FBQztBQUFHLEtBQS84RSxFQUFnOUVuQixDQUFDLENBQUMyZCxhQUFGLEdBQWdCLFlBQVU7QUFBQyxVQUFJNWQsQ0FBQyxHQUFDLElBQU47QUFBV2lKLE1BQUFBLENBQUMsQ0FBQ0MsR0FBRixDQUFNbE8sUUFBTixFQUFlc2dCLEVBQUUsQ0FBQ0UsT0FBbEIsR0FBMkJ2UyxDQUFDLENBQUNHLEVBQUYsQ0FBS3BPLFFBQUwsRUFBY3NnQixFQUFFLENBQUNFLE9BQWpCLEVBQTBCLFVBQVN2YixDQUFULEVBQVc7QUFBQ2pGLFFBQUFBLFFBQVEsS0FBR2lGLENBQUMsQ0FBQytJLE1BQWIsSUFBcUJoSixDQUFDLENBQUM4SyxRQUFGLEtBQWE3SyxDQUFDLENBQUMrSSxNQUFwQyxJQUE0Q2hKLENBQUMsQ0FBQzhLLFFBQUYsQ0FBV1EsUUFBWCxDQUFvQnJMLENBQUMsQ0FBQytJLE1BQXRCLENBQTVDLElBQTJFaEosQ0FBQyxDQUFDOEssUUFBRixDQUFXb0MsS0FBWCxFQUEzRTtBQUE4RixPQUFwSSxDQUEzQjtBQUFrSyxLQUF4cEYsRUFBeXBGak4sQ0FBQyxDQUFDb2QsZUFBRixHQUFrQixZQUFVO0FBQUMsVUFBSXJkLENBQUMsR0FBQyxJQUFOO0FBQVcsV0FBSzhjLFFBQUwsSUFBZSxLQUFLM0ssT0FBTCxDQUFhcEQsUUFBNUIsR0FBcUM5RixDQUFDLENBQUNHLEVBQUYsQ0FBSyxLQUFLMEIsUUFBVixFQUFtQndRLEVBQUUsQ0FBQ0ssZUFBdEIsRUFBdUMsVUFBUzFiLENBQVQsRUFBVztBQUFDLGVBQUtBLENBQUMsQ0FBQ21JLEtBQVAsSUFBY3BJLENBQUMsQ0FBQzZkLDBCQUFGLEVBQWQ7QUFBNkMsT0FBaEcsQ0FBckMsR0FBd0k1VSxDQUFDLENBQUNDLEdBQUYsQ0FBTSxLQUFLNEIsUUFBWCxFQUFvQndRLEVBQUUsQ0FBQ0ssZUFBdkIsQ0FBeEk7QUFBZ0wsS0FBajNGLEVBQWszRjFiLENBQUMsQ0FBQ3FkLGVBQUYsR0FBa0IsWUFBVTtBQUFDLFVBQUl0ZCxDQUFDLEdBQUMsSUFBTjtBQUFXLFdBQUs4YyxRQUFMLEdBQWM3VCxDQUFDLENBQUNHLEVBQUYsQ0FBS3hLLE1BQUwsRUFBWTBjLEVBQUUsQ0FBQ0csTUFBZixFQUF1QixZQUFVO0FBQUMsZUFBT3piLENBQUMsQ0FBQ29kLGFBQUYsRUFBUDtBQUF5QixPQUEzRCxDQUFkLEdBQTRFblUsQ0FBQyxDQUFDQyxHQUFGLENBQU10SyxNQUFOLEVBQWEwYyxFQUFFLENBQUNHLE1BQWhCLENBQTVFO0FBQW9HLEtBQTkvRixFQUErL0Z4YixDQUFDLENBQUN3ZCxVQUFGLEdBQWEsWUFBVTtBQUFDLFVBQUl6ZCxDQUFDLEdBQUMsSUFBTjtBQUFXLFdBQUs4SyxRQUFMLENBQWNyRyxLQUFkLENBQW9CRSxPQUFwQixHQUE0QixNQUE1QixFQUFtQyxLQUFLbUcsUUFBTCxDQUFjcUMsWUFBZCxDQUEyQixhQUEzQixFQUF5QyxDQUFDLENBQTFDLENBQW5DLEVBQWdGLEtBQUtyQyxRQUFMLENBQWN6RCxlQUFkLENBQThCLFlBQTlCLENBQWhGLEVBQTRILEtBQUsrTyxnQkFBTCxHQUFzQixDQUFDLENBQW5KLEVBQXFKLEtBQUttSCxhQUFMLENBQW9CLFlBQVU7QUFBQ3ZpQixRQUFBQSxRQUFRLENBQUNpSyxJQUFULENBQWNtRyxTQUFkLENBQXdCQyxNQUF4QixDQUErQjRRLEVBQS9CLEdBQW1DamMsQ0FBQyxDQUFDOGQsaUJBQUYsRUFBbkMsRUFBeUQ5ZCxDQUFDLENBQUMrZCxlQUFGLEVBQXpELEVBQTZFOVUsQ0FBQyxDQUFDTSxPQUFGLENBQVV2SixDQUFDLENBQUM4SyxRQUFaLEVBQXFCd1EsRUFBRSxDQUFDN0YsTUFBeEIsQ0FBN0U7QUFBNkcsT0FBNUksQ0FBcko7QUFBb1MsS0FBdDBHLEVBQXUwR3hWLENBQUMsQ0FBQytkLGVBQUYsR0FBa0IsWUFBVTtBQUFDLFdBQUtuQixTQUFMLENBQWVuWSxVQUFmLENBQTBCOEcsV0FBMUIsQ0FBc0MsS0FBS3FSLFNBQTNDLEdBQXNELEtBQUtBLFNBQUwsR0FBZSxJQUFyRTtBQUEwRSxLQUE5NkcsRUFBKzZHNWMsQ0FBQyxDQUFDc2QsYUFBRixHQUFnQixVQUFTdmQsQ0FBVCxFQUFXO0FBQUMsVUFBSUMsQ0FBQyxHQUFDLElBQU47QUFBQSxVQUFXUyxDQUFDLEdBQUMsS0FBS29LLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkUsUUFBeEIsQ0FBaUM0USxFQUFqQyxJQUFxQ0EsRUFBckMsR0FBd0MsRUFBckQ7O0FBQXdELFVBQUcsS0FBS1ksUUFBTCxJQUFlLEtBQUszSyxPQUFMLENBQWFpSixRQUEvQixFQUF3QztBQUFDLFlBQUcsS0FBS3lCLFNBQUwsR0FBZTdoQixRQUFRLENBQUM4TCxhQUFULENBQXVCLEtBQXZCLENBQWYsRUFBNkMsS0FBSytWLFNBQUwsQ0FBZW9CLFNBQWYsR0FBeUJqQyxFQUF0RSxFQUF5RXRiLENBQUMsSUFBRSxLQUFLbWMsU0FBTCxDQUFlelIsU0FBZixDQUF5QmdDLEdBQXpCLENBQTZCMU0sQ0FBN0IsQ0FBNUUsRUFBNEcxRixRQUFRLENBQUNpSyxJQUFULENBQWMwWSxXQUFkLENBQTBCLEtBQUtkLFNBQS9CLENBQTVHLEVBQXNKNVQsQ0FBQyxDQUFDRyxFQUFGLENBQUssS0FBSzBCLFFBQVYsRUFBbUJ3USxFQUFFLENBQUNJLGFBQXRCLEVBQXFDLFVBQVMxYixDQUFULEVBQVc7QUFBQ0MsVUFBQUEsQ0FBQyxDQUFDK2Msb0JBQUYsR0FBdUIvYyxDQUFDLENBQUMrYyxvQkFBRixHQUF1QixDQUFDLENBQS9DLEdBQWlEaGQsQ0FBQyxDQUFDZ0osTUFBRixLQUFXaEosQ0FBQyxDQUFDa2UsYUFBYixJQUE0QmplLENBQUMsQ0FBQzRkLDBCQUFGLEVBQTdFO0FBQTRHLFNBQTdKLENBQXRKLEVBQXNUbmQsQ0FBQyxJQUFFb0UsQ0FBQyxDQUFDLEtBQUsrWCxTQUFOLENBQTFULEVBQTJVLEtBQUtBLFNBQUwsQ0FBZXpSLFNBQWYsQ0FBeUJnQyxHQUF6QixDQUE2QitPLEVBQTdCLENBQTNVLEVBQTRXLENBQUN6YixDQUFoWCxFQUFrWCxPQUFPLEtBQUtWLENBQUMsRUFBYjtBQUFnQixZQUFJakQsQ0FBQyxHQUFDK0YsQ0FBQyxDQUFDLEtBQUsrWixTQUFOLENBQVA7QUFBd0I1VCxRQUFBQSxDQUFDLENBQUNJLEdBQUYsQ0FBTSxLQUFLd1QsU0FBWCxFQUFxQnphLENBQXJCLEVBQXVCcEMsQ0FBdkIsR0FBMEIwRCxDQUFDLENBQUMsS0FBS21aLFNBQU4sRUFBZ0I5ZixDQUFoQixDQUEzQjtBQUE4QyxPQUFqZixNQUFzZixJQUFHLENBQUMsS0FBSytmLFFBQU4sSUFBZ0IsS0FBS0QsU0FBeEIsRUFBa0M7QUFBQyxhQUFLQSxTQUFMLENBQWV6UixTQUFmLENBQXlCQyxNQUF6QixDQUFnQzhRLEVBQWhDOztBQUFvQyxZQUFJL2EsQ0FBQyxHQUFDLFNBQUZBLENBQUUsR0FBVTtBQUFDbkIsVUFBQUEsQ0FBQyxDQUFDK2QsZUFBRixJQUFvQmhlLENBQUMsRUFBckI7QUFBd0IsU0FBekM7O0FBQTBDLFlBQUcsS0FBSzhLLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkUsUUFBeEIsQ0FBaUM0USxFQUFqQyxDQUFILEVBQXdDO0FBQUMsY0FBSXZhLENBQUMsR0FBQ21CLENBQUMsQ0FBQyxLQUFLK1osU0FBTixDQUFQO0FBQXdCNVQsVUFBQUEsQ0FBQyxDQUFDSSxHQUFGLENBQU0sS0FBS3dULFNBQVgsRUFBcUJ6YSxDQUFyQixFQUF1QmhCLENBQXZCLEdBQTBCc0MsQ0FBQyxDQUFDLEtBQUttWixTQUFOLEVBQWdCbGIsQ0FBaEIsQ0FBM0I7QUFBOEMsU0FBL0csTUFBb0hQLENBQUM7QUFBRyxPQUF6TyxNQUE4T3BCLENBQUM7QUFBRyxLQUEzdUksRUFBNHVJQyxDQUFDLENBQUM0ZCwwQkFBRixHQUE2QixZQUFVO0FBQUMsVUFBSTdkLENBQUMsR0FBQyxJQUFOOztBQUFXLFVBQUcsYUFBVyxLQUFLbVMsT0FBTCxDQUFhaUosUUFBM0IsRUFBb0M7QUFBQyxZQUFHblMsQ0FBQyxDQUFDTSxPQUFGLENBQVUsS0FBS3VCLFFBQWYsRUFBd0J3USxFQUFFLENBQUNDLGNBQTNCLEVBQTJDN1UsZ0JBQTlDLEVBQStEOztBQUFPLGFBQUtvRSxRQUFMLENBQWNNLFNBQWQsQ0FBd0JnQyxHQUF4QixDQUE0QmdQLEVBQTVCOztBQUFnQyxZQUFJbmMsQ0FBQyxHQUFDNkMsQ0FBQyxDQUFDLEtBQUtnSSxRQUFOLENBQVA7QUFBdUI3QixRQUFBQSxDQUFDLENBQUNJLEdBQUYsQ0FBTSxLQUFLeUIsUUFBWCxFQUFvQjFJLENBQXBCLEVBQXVCLFlBQVU7QUFBQ3BDLFVBQUFBLENBQUMsQ0FBQzhLLFFBQUYsQ0FBV00sU0FBWCxDQUFxQkMsTUFBckIsQ0FBNEIrUSxFQUE1QjtBQUFnQyxTQUFsRSxHQUFxRTFZLENBQUMsQ0FBQyxLQUFLb0gsUUFBTixFQUFlN0ssQ0FBZixDQUF0RSxFQUF3RixLQUFLNkssUUFBTCxDQUFjb0MsS0FBZCxFQUF4RjtBQUE4RyxPQUFoUixNQUFxUixLQUFLd0osSUFBTDtBQUFZLEtBQWhrSixFQUFpa0p6VyxDQUFDLENBQUNtZCxhQUFGLEdBQWdCLFlBQVU7QUFBQyxVQUFJcGQsQ0FBQyxHQUFDLEtBQUs4SyxRQUFMLENBQWNxVCxZQUFkLEdBQTJCbmpCLFFBQVEsQ0FBQzRPLGVBQVQsQ0FBeUJ3VSxZQUExRDtBQUF1RSxPQUFDLEtBQUtyQixrQkFBTixJQUEwQi9jLENBQTFCLEtBQThCLEtBQUs4SyxRQUFMLENBQWNyRyxLQUFkLENBQW9CNFosV0FBcEIsR0FBZ0MsS0FBS3BCLGVBQUwsR0FBcUIsSUFBbkYsR0FBeUYsS0FBS0Ysa0JBQUwsSUFBeUIsQ0FBQy9jLENBQTFCLEtBQThCLEtBQUs4SyxRQUFMLENBQWNyRyxLQUFkLENBQW9CNlosWUFBcEIsR0FBaUMsS0FBS3JCLGVBQUwsR0FBcUIsSUFBcEYsQ0FBekY7QUFBbUwsS0FBdDFKLEVBQXUxSmhkLENBQUMsQ0FBQzZkLGlCQUFGLEdBQW9CLFlBQVU7QUFBQyxXQUFLaFQsUUFBTCxDQUFjckcsS0FBZCxDQUFvQjRaLFdBQXBCLEdBQWdDLEVBQWhDLEVBQW1DLEtBQUt2VCxRQUFMLENBQWNyRyxLQUFkLENBQW9CNlosWUFBcEIsR0FBaUMsRUFBcEU7QUFBdUUsS0FBNzdKLEVBQTg3SnJlLENBQUMsQ0FBQ2lkLGVBQUYsR0FBa0IsWUFBVTtBQUFDLFVBQUlsZCxDQUFDLEdBQUNoRixRQUFRLENBQUNpSyxJQUFULENBQWNnSixxQkFBZCxFQUFOO0FBQTRDLFdBQUs4TyxrQkFBTCxHQUF3Qi9jLENBQUMsQ0FBQ29PLElBQUYsR0FBT3BPLENBQUMsQ0FBQ2xCLEtBQVQsR0FBZUYsTUFBTSxDQUFDQyxVQUE5QyxFQUF5RCxLQUFLb2UsZUFBTCxHQUFxQixLQUFLc0Isa0JBQUwsRUFBOUU7QUFBd0csS0FBL21LLEVBQWduS3RlLENBQUMsQ0FBQ2tkLGFBQUYsR0FBZ0IsWUFBVTtBQUFDLFVBQUluZCxDQUFDLEdBQUMsSUFBTjs7QUFBVyxVQUFHLEtBQUsrYyxrQkFBUixFQUEyQjtBQUFDeFksUUFBQUEsQ0FBQyxDQUFDckosQ0FBQyxDQUFDeU8sSUFBRixDQUFPMFMsRUFBRSxDQUFDSSxhQUFWLENBQUQsQ0FBRCxDQUE0QjVhLE9BQTVCLENBQXFDLFVBQVM1QixDQUFULEVBQVc7QUFBQyxjQUFJUyxDQUFDLEdBQUNULENBQUMsQ0FBQ3dFLEtBQUYsQ0FBUTZaLFlBQWQ7QUFBQSxjQUEyQnZoQixDQUFDLEdBQUM2QixNQUFNLENBQUNtRSxnQkFBUCxDQUF3QjlDLENBQXhCLEVBQTJCLGVBQTNCLENBQTdCO0FBQXlFME4sVUFBQUEsRUFBRSxDQUFDQyxnQkFBSCxDQUFvQjNOLENBQXBCLEVBQXNCLGVBQXRCLEVBQXNDUyxDQUF0QyxHQUF5Q1QsQ0FBQyxDQUFDd0UsS0FBRixDQUFRNlosWUFBUixHQUFxQnBiLFVBQVUsQ0FBQ25HLENBQUQsQ0FBVixHQUFjaUQsQ0FBQyxDQUFDaWQsZUFBaEIsR0FBZ0MsSUFBOUY7QUFBbUcsU0FBN04sR0FBZ08xWSxDQUFDLENBQUNySixDQUFDLENBQUN5TyxJQUFGLENBQU8wUyxFQUFFLENBQUNLLGNBQVYsQ0FBRCxDQUFELENBQTZCN2EsT0FBN0IsQ0FBc0MsVUFBUzVCLENBQVQsRUFBVztBQUFDLGNBQUlTLENBQUMsR0FBQ1QsQ0FBQyxDQUFDd0UsS0FBRixDQUFRK1osV0FBZDtBQUFBLGNBQTBCemhCLENBQUMsR0FBQzZCLE1BQU0sQ0FBQ21FLGdCQUFQLENBQXdCOUMsQ0FBeEIsRUFBMkIsY0FBM0IsQ0FBNUI7QUFBdUUwTixVQUFBQSxFQUFFLENBQUNDLGdCQUFILENBQW9CM04sQ0FBcEIsRUFBc0IsY0FBdEIsRUFBcUNTLENBQXJDLEdBQXdDVCxDQUFDLENBQUN3RSxLQUFGLENBQVErWixXQUFSLEdBQW9CdGIsVUFBVSxDQUFDbkcsQ0FBRCxDQUFWLEdBQWNpRCxDQUFDLENBQUNpZCxlQUFoQixHQUFnQyxJQUE1RjtBQUFpRyxTQUExTixDQUFoTztBQUE2YixZQUFJaGQsQ0FBQyxHQUFDakYsUUFBUSxDQUFDaUssSUFBVCxDQUFjUixLQUFkLENBQW9CNlosWUFBMUI7QUFBQSxZQUF1QzVkLENBQUMsR0FBQzlCLE1BQU0sQ0FBQ21FLGdCQUFQLENBQXdCL0gsUUFBUSxDQUFDaUssSUFBakMsRUFBdUMsZUFBdkMsQ0FBekM7QUFBaUcwSSxRQUFBQSxFQUFFLENBQUNDLGdCQUFILENBQW9CNVMsUUFBUSxDQUFDaUssSUFBN0IsRUFBa0MsZUFBbEMsRUFBa0RoRixDQUFsRCxHQUFxRGpGLFFBQVEsQ0FBQ2lLLElBQVQsQ0FBY1IsS0FBZCxDQUFvQjZaLFlBQXBCLEdBQWlDcGIsVUFBVSxDQUFDeEMsQ0FBRCxDQUFWLEdBQWMsS0FBS3VjLGVBQW5CLEdBQW1DLElBQXpIO0FBQThIOztBQUFBamlCLE1BQUFBLFFBQVEsQ0FBQ2lLLElBQVQsQ0FBY21HLFNBQWQsQ0FBd0JnQyxHQUF4QixDQUE0QjZPLEVBQTVCO0FBQWdDLEtBQTkyTCxFQUErMkxoYyxDQUFDLENBQUM4ZCxlQUFGLEdBQWtCLFlBQVU7QUFBQ3haLE1BQUFBLENBQUMsQ0FBQ3JKLENBQUMsQ0FBQ3lPLElBQUYsQ0FBTzBTLEVBQUUsQ0FBQ0ksYUFBVixDQUFELENBQUQsQ0FBNEI1YSxPQUE1QixDQUFxQyxVQUFTN0IsQ0FBVCxFQUFXO0FBQUMsWUFBSUMsQ0FBQyxHQUFDME4sRUFBRSxDQUFDSyxnQkFBSCxDQUFvQmhPLENBQXBCLEVBQXNCLGVBQXRCLENBQU47QUFBNkMsdUJBQWEsT0FBT0MsQ0FBcEIsS0FBd0IwTixFQUFFLENBQUNFLG1CQUFILENBQXVCN04sQ0FBdkIsRUFBeUIsZUFBekIsR0FBMENBLENBQUMsQ0FBQ3lFLEtBQUYsQ0FBUTZaLFlBQVIsR0FBcUJyZSxDQUF2RjtBQUEwRixPQUF4TCxHQUEyTHNFLENBQUMsQ0FBQ3JKLENBQUMsQ0FBQ3lPLElBQUYsQ0FBTyxLQUFHMFMsRUFBRSxDQUFDSyxjQUFiLENBQUQsQ0FBRCxDQUFnQzdhLE9BQWhDLENBQXlDLFVBQVM3QixDQUFULEVBQVc7QUFBQyxZQUFJQyxDQUFDLEdBQUMwTixFQUFFLENBQUNLLGdCQUFILENBQW9CaE8sQ0FBcEIsRUFBc0IsY0FBdEIsQ0FBTjtBQUE0Qyx1QkFBYSxPQUFPQyxDQUFwQixLQUF3QjBOLEVBQUUsQ0FBQ0UsbUJBQUgsQ0FBdUI3TixDQUF2QixFQUF5QixjQUF6QixHQUF5Q0EsQ0FBQyxDQUFDeUUsS0FBRixDQUFRK1osV0FBUixHQUFvQnZlLENBQXJGO0FBQXdGLE9BQXpMLENBQTNMO0FBQXVYLFVBQUlELENBQUMsR0FBQzJOLEVBQUUsQ0FBQ0ssZ0JBQUgsQ0FBb0JoVCxRQUFRLENBQUNpSyxJQUE3QixFQUFrQyxlQUFsQyxDQUFOO0FBQXlELHFCQUFhLE9BQU9qRixDQUFwQixHQUFzQmhGLFFBQVEsQ0FBQ2lLLElBQVQsQ0FBY1IsS0FBZCxDQUFvQjZaLFlBQXBCLEdBQWlDLEVBQXZELElBQTJEM1EsRUFBRSxDQUFDRSxtQkFBSCxDQUF1QjdTLFFBQVEsQ0FBQ2lLLElBQWhDLEVBQXFDLGVBQXJDLEdBQXNEakssUUFBUSxDQUFDaUssSUFBVCxDQUFjUixLQUFkLENBQW9CNlosWUFBcEIsR0FBaUN0ZSxDQUFsSjtBQUFxSixLQUFqOU0sRUFBazlNQyxDQUFDLENBQUNzZSxrQkFBRixHQUFxQixZQUFVO0FBQUMsVUFBSXZlLENBQUMsR0FBQ2hGLFFBQVEsQ0FBQzhMLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBTjtBQUFvQzlHLE1BQUFBLENBQUMsQ0FBQ2llLFNBQUYsR0FBWWxDLEVBQVosRUFBZS9nQixRQUFRLENBQUNpSyxJQUFULENBQWMwWSxXQUFkLENBQTBCM2QsQ0FBMUIsQ0FBZjtBQUE0QyxVQUFJQyxDQUFDLEdBQUNELENBQUMsQ0FBQ2lPLHFCQUFGLEdBQTBCd1EsS0FBMUIsR0FBZ0N6ZSxDQUFDLENBQUMwZSxXQUF4QztBQUFvRCxhQUFPMWpCLFFBQVEsQ0FBQ2lLLElBQVQsQ0FBY3VHLFdBQWQsQ0FBMEJ4TCxDQUExQixHQUE2QkMsQ0FBcEM7QUFBc0MsS0FBNXBOLEVBQTZwTkQsQ0FBQyxDQUFDeUwsZUFBRixHQUFrQixVQUFTeEwsQ0FBVCxFQUFXUyxDQUFYLEVBQWE7QUFBQyxhQUFPLEtBQUtqRixJQUFMLENBQVcsWUFBVTtBQUFDLFlBQUlzQixDQUFDLEdBQUN3SSxDQUFDLENBQUNFLE9BQUYsQ0FBVSxJQUFWLEVBQWV3VixFQUFmLENBQU47QUFBQSxZQUF5QjdaLENBQUMsR0FBQ08sQ0FBQyxDQUFDLEVBQUQsRUFBSXdaLEVBQUosRUFBTyxFQUFQLEVBQVV4TixFQUFFLENBQUNHLGlCQUFILENBQXFCLElBQXJCLENBQVYsRUFBcUMsRUFBckMsRUFBd0Msb0JBQWlCN04sQ0FBakIsS0FBb0JBLENBQXBCLEdBQXNCQSxDQUF0QixHQUF3QixFQUFoRSxDQUE1Qjs7QUFBZ0csWUFBR2xELENBQUMsS0FBR0EsQ0FBQyxHQUFDLElBQUlpRCxDQUFKLENBQU0sSUFBTixFQUFXb0IsQ0FBWCxDQUFMLENBQUQsRUFBcUIsWUFBVSxPQUFPbkIsQ0FBekMsRUFBMkM7QUFBQyxjQUFHLGVBQWEsT0FBT2xELENBQUMsQ0FBQ2tELENBQUQsQ0FBeEIsRUFBNEIsTUFBTSxJQUFJeVUsU0FBSixDQUFjLHNCQUFvQnpVLENBQXBCLEdBQXNCLEdBQXBDLENBQU47QUFBK0NsRCxVQUFBQSxDQUFDLENBQUNrRCxDQUFELENBQUQsQ0FBS1MsQ0FBTDtBQUFRLFNBQS9ILE1BQW9JVSxDQUFDLENBQUN1VixJQUFGLElBQVE1WixDQUFDLENBQUM0WixJQUFGLENBQU9qVyxDQUFQLENBQVI7QUFBa0IsT0FBNVEsQ0FBUDtBQUFzUixLQUFuOU4sRUFBbzlOVixDQUFDLENBQUMyTCxXQUFGLEdBQWMsVUFBUzNMLENBQVQsRUFBVztBQUFDLGFBQU91RixDQUFDLENBQUNFLE9BQUYsQ0FBVXpGLENBQVYsRUFBWWliLEVBQVosQ0FBUDtBQUF1QixLQUFyZ08sRUFBc2dPdmEsQ0FBQyxDQUFDVixDQUFELEVBQUcsSUFBSCxFQUFRLENBQUM7QUFBQ2lCLE1BQUFBLEdBQUcsRUFBQyxTQUFMO0FBQWVxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU0sT0FBTjtBQUFjO0FBQTVDLEtBQUQsRUFBK0M7QUFBQ3JFLE1BQUFBLEdBQUcsRUFBQyxTQUFMO0FBQWVxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU82VixFQUFQO0FBQVU7QUFBeEMsS0FBL0MsQ0FBUixDQUF2Z08sRUFBMG1PbmIsQ0FBam5PO0FBQW1uTyxHQUF0NU8sRUFBNzBCOztBQUFzdVFpSixFQUFBQSxDQUFDLENBQUNHLEVBQUYsQ0FBS3BPLFFBQUwsRUFBY3NnQixFQUFFLENBQUM3USxjQUFqQixFQUFnQzRSLEVBQUUsQ0FBQ25HLFdBQW5DLEVBQWdELFVBQVNsVyxDQUFULEVBQVc7QUFBQyxRQUFJQyxDQUFDLEdBQUMsSUFBTjtBQUFBLFFBQVdTLENBQUMsR0FBQ21DLENBQUMsQ0FBQyxJQUFELENBQWQ7QUFBcUIsWUFBTSxLQUFLbVIsT0FBWCxJQUFvQixXQUFTLEtBQUtBLE9BQWxDLElBQTJDaFUsQ0FBQyxDQUFDeUcsY0FBRixFQUEzQyxFQUE4RHdDLENBQUMsQ0FBQ0ksR0FBRixDQUFNM0ksQ0FBTixFQUFRNGEsRUFBRSxDQUFDaEcsSUFBWCxFQUFpQixVQUFTdFYsQ0FBVCxFQUFXO0FBQUNBLE1BQUFBLENBQUMsQ0FBQzBHLGdCQUFGLElBQW9CdUMsQ0FBQyxDQUFDSSxHQUFGLENBQU0zSSxDQUFOLEVBQVE0YSxFQUFFLENBQUM3RixNQUFYLEVBQW1CLFlBQVU7QUFBQzFYLFFBQUFBLENBQUMsQ0FBQ2tDLENBQUQsQ0FBRCxJQUFNQSxDQUFDLENBQUNpTixLQUFGLEVBQU47QUFBZ0IsT0FBOUMsQ0FBcEI7QUFBcUUsS0FBbEcsQ0FBOUQ7QUFBbUssUUFBSW5RLENBQUMsR0FBQ3dJLENBQUMsQ0FBQ0UsT0FBRixDQUFVL0UsQ0FBVixFQUFZdWEsRUFBWixDQUFOOztBQUFzQixRQUFHLENBQUNsZSxDQUFKLEVBQU07QUFBQyxVQUFJcUUsQ0FBQyxHQUFDTyxDQUFDLENBQUMsRUFBRCxFQUFJZ00sRUFBRSxDQUFDRyxpQkFBSCxDQUFxQnBOLENBQXJCLENBQUosRUFBNEIsRUFBNUIsRUFBK0JpTixFQUFFLENBQUNHLGlCQUFILENBQXFCLElBQXJCLENBQS9CLENBQVA7QUFBa0UvUSxNQUFBQSxDQUFDLEdBQUMsSUFBSTRmLEVBQUosQ0FBT2pjLENBQVAsRUFBU1UsQ0FBVCxDQUFGO0FBQWM7O0FBQUFyRSxJQUFBQSxDQUFDLENBQUM0WixJQUFGLENBQU8sSUFBUDtBQUFhLEdBQTlXO0FBQWlYLE1BQUlnSSxFQUFFLEdBQUMzWixDQUFDLEVBQVI7O0FBQVcsTUFBRzJaLEVBQUgsRUFBTTtBQUFDLFFBQUlDLEVBQUUsR0FBQ0QsRUFBRSxDQUFDN1MsRUFBSCxDQUFNK1MsS0FBYjtBQUFtQkYsSUFBQUEsRUFBRSxDQUFDN1MsRUFBSCxDQUFNK1MsS0FBTixHQUFZbEMsRUFBRSxDQUFDbFIsZUFBZixFQUErQmtULEVBQUUsQ0FBQzdTLEVBQUgsQ0FBTStTLEtBQU4sQ0FBWTdTLFdBQVosR0FBd0IyUSxFQUF2RCxFQUEwRGdDLEVBQUUsQ0FBQzdTLEVBQUgsQ0FBTStTLEtBQU4sQ0FBWTVTLFVBQVosR0FBdUIsWUFBVTtBQUFDLGFBQU8wUyxFQUFFLENBQUM3UyxFQUFILENBQU0rUyxLQUFOLEdBQVlELEVBQVosRUFBZWpDLEVBQUUsQ0FBQ2xSLGVBQXpCO0FBQXlDLEtBQXJJO0FBQXNJOztBQUFBLE1BQUlxVCxFQUFFLEdBQUMsQ0FBQyxZQUFELEVBQWMsTUFBZCxFQUFxQixNQUFyQixFQUE0QixVQUE1QixFQUF1QyxVQUF2QyxFQUFrRCxRQUFsRCxFQUEyRCxLQUEzRCxFQUFpRSxZQUFqRSxDQUFQO0FBQUEsTUFBc0ZDLEVBQUUsR0FBQyw2REFBekY7QUFBQSxNQUF1SkMsRUFBRSxHQUFDLHFJQUExSjtBQUFBLE1BQWdTNVYsRUFBRSxHQUFDLFNBQUhBLEVBQUcsQ0FBU3BKLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsUUFBSVMsQ0FBQyxHQUFDVixDQUFDLENBQUNpZixRQUFGLENBQVcvYSxXQUFYLEVBQU47QUFBK0IsUUFBRyxDQUFDLENBQUQsS0FBS2pFLENBQUMsQ0FBQzRJLE9BQUYsQ0FBVW5JLENBQVYsQ0FBUixFQUFxQixPQUFNLENBQUMsQ0FBRCxLQUFLb2UsRUFBRSxDQUFDalcsT0FBSCxDQUFXbkksQ0FBWCxDQUFMLElBQW9CeUcsT0FBTyxDQUFDbkgsQ0FBQyxDQUFDa2YsU0FBRixDQUFZamIsS0FBWixDQUFrQjhhLEVBQWxCLEtBQXVCL2UsQ0FBQyxDQUFDa2YsU0FBRixDQUFZamIsS0FBWixDQUFrQithLEVBQWxCLENBQXhCLENBQWpDOztBQUFnRixTQUFJLElBQUlqaUIsQ0FBQyxHQUFDa0QsQ0FBQyxDQUFDc0IsTUFBRixDQUFVLFVBQVN2QixDQUFULEVBQVc7QUFBQyxhQUFPQSxDQUFDLFlBQVltRSxNQUFwQjtBQUEyQixLQUFqRCxDQUFOLEVBQTBEL0MsQ0FBQyxHQUFDLENBQTVELEVBQThETyxDQUFDLEdBQUM1RSxDQUFDLENBQUM0RCxNQUF0RSxFQUE2RVMsQ0FBQyxHQUFDTyxDQUEvRSxFQUFpRlAsQ0FBQyxFQUFsRjtBQUFxRixVQUFHVixDQUFDLENBQUN1RCxLQUFGLENBQVFsSCxDQUFDLENBQUNxRSxDQUFELENBQVQsQ0FBSCxFQUFpQixPQUFNLENBQUMsQ0FBUDtBQUF0Rzs7QUFBK0csV0FBTSxDQUFDLENBQVA7QUFBUyxHQUE3aUI7QUFBQSxNQUE4aUIrZCxFQUFFLEdBQUM7QUFBQyxTQUFJLENBQUMsT0FBRCxFQUFTLEtBQVQsRUFBZSxJQUFmLEVBQW9CLE1BQXBCLEVBQTJCLE1BQTNCLEVBQWtDLGdCQUFsQyxDQUFMO0FBQXlEamQsSUFBQUEsQ0FBQyxFQUFDLENBQUMsUUFBRCxFQUFVLE1BQVYsRUFBaUIsT0FBakIsRUFBeUIsS0FBekIsQ0FBM0Q7QUFBMkZrZCxJQUFBQSxJQUFJLEVBQUMsRUFBaEc7QUFBbUd2YSxJQUFBQSxDQUFDLEVBQUMsRUFBckc7QUFBd0d3YSxJQUFBQSxFQUFFLEVBQUMsRUFBM0c7QUFBOEdDLElBQUFBLEdBQUcsRUFBQyxFQUFsSDtBQUFxSEMsSUFBQUEsSUFBSSxFQUFDLEVBQTFIO0FBQTZIQyxJQUFBQSxHQUFHLEVBQUMsRUFBakk7QUFBb0lDLElBQUFBLEVBQUUsRUFBQyxFQUF2STtBQUEwSUMsSUFBQUEsRUFBRSxFQUFDLEVBQTdJO0FBQWdKQyxJQUFBQSxFQUFFLEVBQUMsRUFBbko7QUFBc0pDLElBQUFBLEVBQUUsRUFBQyxFQUF6SjtBQUE0SkMsSUFBQUEsRUFBRSxFQUFDLEVBQS9KO0FBQWtLQyxJQUFBQSxFQUFFLEVBQUMsRUFBcks7QUFBd0tDLElBQUFBLEVBQUUsRUFBQyxFQUEzSztBQUE4S0MsSUFBQUEsRUFBRSxFQUFDLEVBQWpMO0FBQW9MampCLElBQUFBLENBQUMsRUFBQyxFQUF0TDtBQUF5TGtqQixJQUFBQSxHQUFHLEVBQUMsQ0FBQyxLQUFELEVBQU8sS0FBUCxFQUFhLE9BQWIsRUFBcUIsT0FBckIsRUFBNkIsUUFBN0IsQ0FBN0w7QUFBb09DLElBQUFBLEVBQUUsRUFBQyxFQUF2TztBQUEwT0MsSUFBQUEsRUFBRSxFQUFDLEVBQTdPO0FBQWdQL2MsSUFBQUEsQ0FBQyxFQUFDLEVBQWxQO0FBQXFQZ2QsSUFBQUEsR0FBRyxFQUFDLEVBQXpQO0FBQTRQbmUsSUFBQUEsQ0FBQyxFQUFDLEVBQTlQO0FBQWlRb2UsSUFBQUEsS0FBSyxFQUFDLEVBQXZRO0FBQTBRQyxJQUFBQSxJQUFJLEVBQUMsRUFBL1E7QUFBa1JDLElBQUFBLEdBQUcsRUFBQyxFQUF0UjtBQUF5UkMsSUFBQUEsR0FBRyxFQUFDLEVBQTdSO0FBQWdTQyxJQUFBQSxNQUFNLEVBQUMsRUFBdlM7QUFBMFNwZSxJQUFBQSxDQUFDLEVBQUMsRUFBNVM7QUFBK1NxZSxJQUFBQSxFQUFFLEVBQUM7QUFBbFQsR0FBampCOztBQUF1MkIsV0FBU0MsRUFBVCxDQUFZM2dCLENBQVosRUFBY0MsQ0FBZCxFQUFnQlMsQ0FBaEIsRUFBa0I7QUFBQyxRQUFHLENBQUNWLENBQUMsQ0FBQ1csTUFBTixFQUFhLE9BQU9YLENBQVA7QUFBUyxRQUFHVSxDQUFDLElBQUUsY0FBWSxPQUFPQSxDQUF6QixFQUEyQixPQUFPQSxDQUFDLENBQUNWLENBQUQsQ0FBUjs7QUFBWSxTQUFJLElBQUlqRCxDQUFDLEdBQUUsSUFBSTZCLE1BQU0sQ0FBQ2dpQixTQUFYLEVBQUQsQ0FBdUJDLGVBQXZCLENBQXVDN2dCLENBQXZDLEVBQXlDLFdBQXpDLENBQU4sRUFBNERvQixDQUFDLEdBQUNMLE1BQU0sQ0FBQ00sSUFBUCxDQUFZcEIsQ0FBWixDQUE5RCxFQUE2RTBCLENBQUMsR0FBQzRDLENBQUMsQ0FBQ3hILENBQUMsQ0FBQ2tJLElBQUYsQ0FBT3pKLGdCQUFQLENBQXdCLEdBQXhCLENBQUQsQ0FBaEYsRUFBK0d5RyxDQUFDLEdBQUMsV0FBU2pDLENBQVQsRUFBV1UsQ0FBWCxFQUFhO0FBQUMsVUFBSTNELENBQUMsR0FBQzRFLENBQUMsQ0FBQzNCLENBQUQsQ0FBUDtBQUFBLFVBQVdpQyxDQUFDLEdBQUNsRixDQUFDLENBQUNraUIsUUFBRixDQUFXL2EsV0FBWCxFQUFiO0FBQXNDLFVBQUcsQ0FBQyxDQUFELEtBQUs5QyxDQUFDLENBQUN5SCxPQUFGLENBQVU1RyxDQUFWLENBQVIsRUFBcUIsT0FBT2xGLENBQUMsQ0FBQzJILFVBQUYsQ0FBYThHLFdBQWIsQ0FBeUJ6TyxDQUF6QixHQUE0QixVQUFuQztBQUE4QyxVQUFJbUYsQ0FBQyxHQUFDcUMsQ0FBQyxDQUFDeEgsQ0FBQyxDQUFDK2pCLFVBQUgsQ0FBUDtBQUFBLFVBQXNCM2UsQ0FBQyxHQUFDLEdBQUc0ZSxNQUFILENBQVU5Z0IsQ0FBQyxDQUFDLEdBQUQsQ0FBRCxJQUFRLEVBQWxCLEVBQXFCQSxDQUFDLENBQUNnQyxDQUFELENBQUQsSUFBTSxFQUEzQixDQUF4QjtBQUF1REMsTUFBQUEsQ0FBQyxDQUFDTCxPQUFGLENBQVcsVUFBUzdCLENBQVQsRUFBVztBQUFDb0osUUFBQUEsRUFBRSxDQUFDcEosQ0FBRCxFQUFHbUMsQ0FBSCxDQUFGLElBQVNwRixDQUFDLENBQUNzSyxlQUFGLENBQWtCckgsQ0FBQyxDQUFDaWYsUUFBcEIsQ0FBVDtBQUF1QyxPQUE5RDtBQUFpRSxLQUFoVyxFQUFpVy9jLENBQUMsR0FBQyxDQUFuVyxFQUFxV0MsQ0FBQyxHQUFDUixDQUFDLENBQUNoQixNQUE3VyxFQUFvWHVCLENBQUMsR0FBQ0MsQ0FBdFgsRUFBd1hELENBQUMsRUFBelg7QUFBNFhELE1BQUFBLENBQUMsQ0FBQ0MsQ0FBRCxDQUFEO0FBQTVYOztBQUFpWSxXQUFPbkYsQ0FBQyxDQUFDa0ksSUFBRixDQUFPK2IsU0FBZDtBQUF3Qjs7QUFBQSxNQUFJQyxFQUFFLEdBQUMsU0FBUDtBQUFBLE1BQWlCQyxFQUFFLEdBQUMsSUFBSS9jLE1BQUosQ0FBVyx1QkFBWCxFQUFtQyxHQUFuQyxDQUFwQjtBQUFBLE1BQTREZ2QsRUFBRSxHQUFDLENBQUMsVUFBRCxFQUFZLFdBQVosRUFBd0IsWUFBeEIsQ0FBL0Q7QUFBQSxNQUFxR0MsRUFBRSxHQUFDO0FBQUNDLElBQUFBLFNBQVMsRUFBQyxTQUFYO0FBQXFCQyxJQUFBQSxRQUFRLEVBQUMsUUFBOUI7QUFBdUNDLElBQUFBLEtBQUssRUFBQywyQkFBN0M7QUFBeUVoWSxJQUFBQSxPQUFPLEVBQUMsUUFBakY7QUFBMEZqTSxJQUFBQSxLQUFLLEVBQUMsaUJBQWhHO0FBQWtIa2tCLElBQUFBLElBQUksRUFBQyxTQUF2SDtBQUFpSUMsSUFBQUEsUUFBUSxFQUFDLGtCQUExSTtBQUE2Sm5ILElBQUFBLFNBQVMsRUFBQyxtQkFBdks7QUFBMkxyZSxJQUFBQSxNQUFNLEVBQUMsMEJBQWxNO0FBQTZOeWxCLElBQUFBLFNBQVMsRUFBQywwQkFBdk87QUFBa1FDLElBQUFBLGlCQUFpQixFQUFDLGdCQUFwUjtBQUFxUzdJLElBQUFBLFFBQVEsRUFBQyxrQkFBOVM7QUFBaVU4SSxJQUFBQSxRQUFRLEVBQUMsU0FBMVU7QUFBb1ZDLElBQUFBLFVBQVUsRUFBQyxpQkFBL1Y7QUFBaVhDLElBQUFBLFNBQVMsRUFBQyxRQUEzWDtBQUFvWTlJLElBQUFBLFlBQVksRUFBQztBQUFqWixHQUF4RztBQUFBLE1BQTBnQmxOLEVBQUUsR0FBQztBQUFDaVcsSUFBQUEsSUFBSSxFQUFDLE1BQU47QUFBYUMsSUFBQUEsR0FBRyxFQUFDLEtBQWpCO0FBQXVCQyxJQUFBQSxLQUFLLEVBQUMsT0FBN0I7QUFBcUNDLElBQUFBLE1BQU0sRUFBQyxRQUE1QztBQUFxREMsSUFBQUEsSUFBSSxFQUFDO0FBQTFELEdBQTdnQjtBQUFBLE1BQStrQkMsRUFBRSxHQUFDO0FBQUNmLElBQUFBLFNBQVMsRUFBQyxDQUFDLENBQVo7QUFBY0MsSUFBQUEsUUFBUSxFQUFDLDhHQUF2QjtBQUFzSS9YLElBQUFBLE9BQU8sRUFBQyxhQUE5STtBQUE0SmdZLElBQUFBLEtBQUssRUFBQyxFQUFsSztBQUFxS2prQixJQUFBQSxLQUFLLEVBQUMsQ0FBM0s7QUFBNktra0IsSUFBQUEsSUFBSSxFQUFDLENBQUMsQ0FBbkw7QUFBcUxDLElBQUFBLFFBQVEsRUFBQyxDQUFDLENBQS9MO0FBQWlNbkgsSUFBQUEsU0FBUyxFQUFDLEtBQTNNO0FBQWlOcmUsSUFBQUEsTUFBTSxFQUFDLENBQXhOO0FBQTBOeWxCLElBQUFBLFNBQVMsRUFBQyxDQUFDLENBQXJPO0FBQXVPQyxJQUFBQSxpQkFBaUIsRUFBQyxNQUF6UDtBQUFnUTdJLElBQUFBLFFBQVEsRUFBQyxjQUF6UTtBQUF3UjhJLElBQUFBLFFBQVEsRUFBQyxDQUFDLENBQWxTO0FBQW9TQyxJQUFBQSxVQUFVLEVBQUMsSUFBL1M7QUFBb1RDLElBQUFBLFNBQVMsRUFBQzNDLEVBQTlUO0FBQWlVbkcsSUFBQUEsWUFBWSxFQUFDO0FBQTlVLEdBQWxsQjtBQUFBLE1BQXM2QnFKLEVBQUUsR0FBQyxNQUF6NkI7QUFBQSxNQUFnN0JDLEVBQUUsR0FBQyxLQUFuN0I7QUFBQSxNQUF5N0JDLEVBQUUsR0FBQztBQUFDL00sSUFBQUEsSUFBSSxFQUFDLGlCQUFOO0FBQXdCQyxJQUFBQSxNQUFNLEVBQUMsbUJBQS9CO0FBQW1ESCxJQUFBQSxJQUFJLEVBQUMsaUJBQXhEO0FBQTBFQyxJQUFBQSxLQUFLLEVBQUMsa0JBQWhGO0FBQW1HaU4sSUFBQUEsUUFBUSxFQUFDLHFCQUE1RztBQUFrSWpMLElBQUFBLEtBQUssRUFBQyxrQkFBeEk7QUFBMkppRSxJQUFBQSxPQUFPLEVBQUMsb0JBQW5LO0FBQXdMaUgsSUFBQUEsUUFBUSxFQUFDLHFCQUFqTTtBQUF1TjVTLElBQUFBLFVBQVUsRUFBQyx1QkFBbE87QUFBMFBDLElBQUFBLFVBQVUsRUFBQztBQUFyUSxHQUE1N0I7QUFBQSxNQUEwdEM0UyxFQUFFLEdBQUMsTUFBN3RDO0FBQUEsTUFBb3VDQyxFQUFFLEdBQUMsTUFBdnVDO0FBQUEsTUFBOHVDQyxFQUFFLEdBQUMsZ0JBQWp2QztBQUFBLE1BQWt3Q0MsRUFBRSxHQUFDLE9BQXJ3QztBQUFBLE1BQTZ3Q0MsRUFBRSxHQUFDLE9BQWh4QztBQUFBLE1BQXd4Q0MsRUFBRSxHQUFDLE9BQTN4QztBQUFBLE1BQW15Q0MsRUFBRSxHQUFDLFFBQXR5QztBQUFBLE1BQSt5Q0MsRUFBRSxHQUFDLFlBQVU7QUFBQyxhQUFTaGpCLENBQVQsQ0FBV0EsQ0FBWCxFQUFhUyxDQUFiLEVBQWU7QUFBQyxVQUFHLGVBQWEsT0FBT1YsQ0FBdkIsRUFBeUIsTUFBTSxJQUFJMFUsU0FBSixDQUFjLGdFQUFkLENBQU47QUFBc0YsV0FBS3dPLFVBQUwsR0FBZ0IsQ0FBQyxDQUFqQixFQUFtQixLQUFLQyxRQUFMLEdBQWMsQ0FBakMsRUFBbUMsS0FBS0MsV0FBTCxHQUFpQixFQUFwRCxFQUF1RCxLQUFLQyxjQUFMLEdBQW9CLEVBQTNFLEVBQThFLEtBQUtsSyxPQUFMLEdBQWEsSUFBM0YsRUFBZ0csS0FBS21LLE9BQUwsR0FBYXJqQixDQUE3RyxFQUErRyxLQUFLWCxNQUFMLEdBQVksS0FBSzhTLFVBQUwsQ0FBZ0IxUixDQUFoQixDQUEzSCxFQUE4SSxLQUFLNmlCLEdBQUwsR0FBUyxJQUF2SixFQUE0SixLQUFLQyxhQUFMLEVBQTVKLEVBQWlMamUsQ0FBQyxDQUFDQyxPQUFGLENBQVV2RixDQUFWLEVBQVksS0FBSytaLFdBQUwsQ0FBaUJ5SixRQUE3QixFQUFzQyxJQUF0QyxDQUFqTDtBQUE2Tjs7QUFBQSxRQUFJMW1CLENBQUMsR0FBQ2tELENBQUMsQ0FBQ2lCLFNBQVI7QUFBa0IsV0FBT25FLENBQUMsQ0FBQzJtQixNQUFGLEdBQVMsWUFBVTtBQUFDLFdBQUtSLFVBQUwsR0FBZ0IsQ0FBQyxDQUFqQjtBQUFtQixLQUF2QyxFQUF3Q25tQixDQUFDLENBQUM0bUIsT0FBRixHQUFVLFlBQVU7QUFBQyxXQUFLVCxVQUFMLEdBQWdCLENBQUMsQ0FBakI7QUFBbUIsS0FBaEYsRUFBaUZubUIsQ0FBQyxDQUFDNm1CLGFBQUYsR0FBZ0IsWUFBVTtBQUFDLFdBQUtWLFVBQUwsR0FBZ0IsQ0FBQyxLQUFLQSxVQUF0QjtBQUFpQyxLQUE3SSxFQUE4SW5tQixDQUFDLENBQUNpUSxNQUFGLEdBQVMsVUFBU2hOLENBQVQsRUFBVztBQUFDLFVBQUcsS0FBS2tqQixVQUFSLEVBQW1CLElBQUdsakIsQ0FBSCxFQUFLO0FBQUMsWUFBSUMsQ0FBQyxHQUFDLEtBQUsrWixXQUFMLENBQWlCeUosUUFBdkI7QUFBQSxZQUFnQy9pQixDQUFDLEdBQUM2RSxDQUFDLENBQUNFLE9BQUYsQ0FBVXpGLENBQUMsQ0FBQ3dJLGNBQVosRUFBMkJ2SSxDQUEzQixDQUFsQztBQUFnRVMsUUFBQUEsQ0FBQyxLQUFHQSxDQUFDLEdBQUMsSUFBSSxLQUFLc1osV0FBVCxDQUFxQmhhLENBQUMsQ0FBQ3dJLGNBQXZCLEVBQXNDLEtBQUtxYixrQkFBTCxFQUF0QyxDQUFGLEVBQW1FdGUsQ0FBQyxDQUFDQyxPQUFGLENBQVV4RixDQUFDLENBQUN3SSxjQUFaLEVBQTJCdkksQ0FBM0IsRUFBNkJTLENBQTdCLENBQXRFLENBQUQsRUFBd0dBLENBQUMsQ0FBQzJpQixjQUFGLENBQWlCUyxLQUFqQixHQUF1QixDQUFDcGpCLENBQUMsQ0FBQzJpQixjQUFGLENBQWlCUyxLQUFqSixFQUF1SnBqQixDQUFDLENBQUNxakIsb0JBQUYsS0FBeUJyakIsQ0FBQyxDQUFDc2pCLE1BQUYsQ0FBUyxJQUFULEVBQWN0akIsQ0FBZCxDQUF6QixHQUEwQ0EsQ0FBQyxDQUFDdWpCLE1BQUYsQ0FBUyxJQUFULEVBQWN2akIsQ0FBZCxDQUFqTTtBQUFrTixPQUF4UixNQUE0UjtBQUFDLFlBQUcsS0FBS3dqQixhQUFMLEdBQXFCOVksU0FBckIsQ0FBK0JFLFFBQS9CLENBQXdDcVgsRUFBeEMsQ0FBSCxFQUErQyxPQUFPLEtBQUssS0FBS3NCLE1BQUwsQ0FBWSxJQUFaLEVBQWlCLElBQWpCLENBQVo7O0FBQW1DLGFBQUtELE1BQUwsQ0FBWSxJQUFaLEVBQWlCLElBQWpCO0FBQXVCO0FBQUMsS0FBN2pCLEVBQThqQmpuQixDQUFDLENBQUNvTyxPQUFGLEdBQVUsWUFBVTtBQUFDNEksTUFBQUEsWUFBWSxDQUFDLEtBQUtvUCxRQUFOLENBQVosRUFBNEI1ZCxDQUFDLENBQUNHLFVBQUYsQ0FBYSxLQUFLNGQsT0FBbEIsRUFBMEIsS0FBS3RKLFdBQUwsQ0FBaUJ5SixRQUEzQyxDQUE1QixFQUFpRnhhLENBQUMsQ0FBQ0MsR0FBRixDQUFNLEtBQUtvYSxPQUFYLEVBQW1CLEtBQUt0SixXQUFMLENBQWlCbUssU0FBcEMsQ0FBakYsRUFBZ0lsYixDQUFDLENBQUNDLEdBQUYsQ0FBTWhPLENBQUMsQ0FBQzhLLE9BQUYsQ0FBVSxLQUFLc2QsT0FBZixFQUF1QixRQUF2QixDQUFOLEVBQXVDLGVBQXZDLEVBQXVELEtBQUtjLGlCQUE1RCxDQUFoSSxFQUErTSxLQUFLYixHQUFMLElBQVUsS0FBS0EsR0FBTCxDQUFTN2UsVUFBVCxDQUFvQjhHLFdBQXBCLENBQWdDLEtBQUsrWCxHQUFyQyxDQUF6TixFQUFtUSxLQUFLTCxVQUFMLEdBQWdCLElBQW5SLEVBQXdSLEtBQUtDLFFBQUwsR0FBYyxJQUF0UyxFQUEyUyxLQUFLQyxXQUFMLEdBQWlCLElBQTVULEVBQWlVLEtBQUtDLGNBQUwsR0FBb0IsSUFBclYsRUFBMFYsS0FBS2xLLE9BQUwsSUFBYyxLQUFLQSxPQUFMLENBQWFTLE9BQWIsRUFBeFcsRUFBK1gsS0FBS1QsT0FBTCxHQUFhLElBQTVZLEVBQWlaLEtBQUttSyxPQUFMLEdBQWEsSUFBOVosRUFBbWEsS0FBS2hrQixNQUFMLEdBQVksSUFBL2EsRUFBb2IsS0FBS2lrQixHQUFMLEdBQVMsSUFBN2I7QUFBa2MsS0FBcmhDLEVBQXNoQ3htQixDQUFDLENBQUM0WixJQUFGLEdBQU8sWUFBVTtBQUFDLFVBQUkxVyxDQUFDLEdBQUMsSUFBTjtBQUFXLFVBQUcsV0FBUyxLQUFLcWpCLE9BQUwsQ0FBYTdlLEtBQWIsQ0FBbUJFLE9BQS9CLEVBQXVDLE1BQU0sSUFBSU4sS0FBSixDQUFVLHFDQUFWLENBQU47O0FBQXVELFVBQUcsS0FBS2dnQixhQUFMLE1BQXNCLEtBQUtuQixVQUE5QixFQUF5QztBQUFDLFlBQUl4aUIsQ0FBQyxHQUFDdUksQ0FBQyxDQUFDTSxPQUFGLENBQVUsS0FBSytaLE9BQWYsRUFBdUIsS0FBS3RKLFdBQUwsQ0FBaUJwVCxLQUFqQixDQUF1QjBPLElBQTlDLENBQU47QUFBQSxZQUEwRHZZLENBQUMsR0FBQyxTQUFTaUQsQ0FBVCxDQUFXQyxDQUFYLEVBQWE7QUFBQyxjQUFHLENBQUNqRixRQUFRLENBQUM0TyxlQUFULENBQXlCMGEsWUFBN0IsRUFBMEMsT0FBTyxJQUFQOztBQUFZLGNBQUcsY0FBWSxPQUFPcmtCLENBQUMsQ0FBQ3NrQixXQUF4QixFQUFvQztBQUFDLGdCQUFJN2pCLENBQUMsR0FBQ1QsQ0FBQyxDQUFDc2tCLFdBQUYsRUFBTjtBQUFzQixtQkFBTzdqQixDQUFDLFlBQVk4akIsVUFBYixHQUF3QjlqQixDQUF4QixHQUEwQixJQUFqQztBQUFzQzs7QUFBQSxpQkFBT1QsQ0FBQyxZQUFZdWtCLFVBQWIsR0FBd0J2a0IsQ0FBeEIsR0FBMEJBLENBQUMsQ0FBQ3lFLFVBQUYsR0FBYTFFLENBQUMsQ0FBQ0MsQ0FBQyxDQUFDeUUsVUFBSCxDQUFkLEdBQTZCLElBQTlEO0FBQW1FLFNBQXhPLENBQXlPLEtBQUs0ZSxPQUE5TyxDQUE1RDtBQUFBLFlBQW1UbGlCLENBQUMsR0FBQyxTQUFPckUsQ0FBUCxHQUFTLEtBQUt1bUIsT0FBTCxDQUFhbUIsYUFBYixDQUEyQjdhLGVBQTNCLENBQTJDMEIsUUFBM0MsQ0FBb0QsS0FBS2dZLE9BQXpELENBQVQsR0FBMkV2bUIsQ0FBQyxDQUFDdU8sUUFBRixDQUFXLEtBQUtnWSxPQUFoQixDQUFoWTs7QUFBeVosWUFBRzVpQixDQUFDLENBQUNnRyxnQkFBRixJQUFvQixDQUFDdEYsQ0FBeEIsRUFBMEI7QUFBTyxZQUFJTyxDQUFDLEdBQUMsS0FBS3VpQixhQUFMLEVBQU47QUFBQSxZQUEyQmppQixDQUFDLEdBQUNJLENBQUMsQ0FBQyxLQUFLMlgsV0FBTCxDQUFpQjBLLElBQWxCLENBQTlCO0FBQXNEL2lCLFFBQUFBLENBQUMsQ0FBQ3dMLFlBQUYsQ0FBZSxJQUFmLEVBQW9CbEwsQ0FBcEIsR0FBdUIsS0FBS3FoQixPQUFMLENBQWFuVyxZQUFiLENBQTBCLGtCQUExQixFQUE2Q2xMLENBQTdDLENBQXZCLEVBQXVFLEtBQUswaUIsVUFBTCxFQUF2RSxFQUF5RixLQUFLcmxCLE1BQUwsQ0FBWStoQixTQUFaLElBQXVCMWYsQ0FBQyxDQUFDeUosU0FBRixDQUFZZ0MsR0FBWixDQUFnQnNWLEVBQWhCLENBQWhIOztBQUFvSSxZQUFJeGdCLENBQUMsR0FBQyxjQUFZLE9BQU8sS0FBSzVDLE1BQUwsQ0FBWWdiLFNBQS9CLEdBQXlDLEtBQUtoYixNQUFMLENBQVlnYixTQUFaLENBQXNCdFcsSUFBdEIsQ0FBMkIsSUFBM0IsRUFBZ0NyQyxDQUFoQyxFQUFrQyxLQUFLMmhCLE9BQXZDLENBQXpDLEdBQXlGLEtBQUtoa0IsTUFBTCxDQUFZZ2IsU0FBM0c7QUFBQSxZQUFxSG5ZLENBQUMsR0FBQyxLQUFLeWlCLGNBQUwsQ0FBb0IxaUIsQ0FBcEIsQ0FBdkg7O0FBQThJLGFBQUsyaUIsbUJBQUwsQ0FBeUIxaUIsQ0FBekI7O0FBQTRCLFlBQUlNLENBQUMsR0FBQyxLQUFLcWlCLGFBQUwsRUFBTjs7QUFBMkJ2ZixRQUFBQSxDQUFDLENBQUNDLE9BQUYsQ0FBVTdELENBQVYsRUFBWSxLQUFLcVksV0FBTCxDQUFpQnlKLFFBQTdCLEVBQXNDLElBQXRDLEdBQTRDLEtBQUtILE9BQUwsQ0FBYW1CLGFBQWIsQ0FBMkI3YSxlQUEzQixDQUEyQzBCLFFBQTNDLENBQW9ELEtBQUtpWSxHQUF6RCxLQUErRDlnQixDQUFDLENBQUNrYixXQUFGLENBQWNoYyxDQUFkLENBQTNHLEVBQTRIc0gsQ0FBQyxDQUFDTSxPQUFGLENBQVUsS0FBSytaLE9BQWYsRUFBdUIsS0FBS3RKLFdBQUwsQ0FBaUJwVCxLQUFqQixDQUF1QjRiLFFBQTlDLENBQTVILEVBQW9MLEtBQUtySixPQUFMLEdBQWEsSUFBSW5aLENBQUosQ0FBTSxLQUFLc2pCLE9BQVgsRUFBbUIzaEIsQ0FBbkIsRUFBcUIsS0FBS2dZLGdCQUFMLENBQXNCeFgsQ0FBdEIsQ0FBckIsQ0FBak0sRUFBZ1BSLENBQUMsQ0FBQ3lKLFNBQUYsQ0FBWWdDLEdBQVosQ0FBZ0J1VixFQUFoQixDQUFoUCxFQUFvUSxrQkFBaUIzbkIsUUFBUSxDQUFDNE8sZUFBMUIsSUFBMkNyRixDQUFDLENBQUN2SixRQUFRLENBQUNpSyxJQUFULENBQWM2RSxRQUFmLENBQUQsQ0FBMEJqSSxPQUExQixDQUFtQyxVQUFTN0IsQ0FBVCxFQUFXO0FBQUNpSixVQUFBQSxDQUFDLENBQUNHLEVBQUYsQ0FBS3BKLENBQUwsRUFBTyxXQUFQLEVBQW9CLFlBQVUsQ0FBRSxDQUFoQztBQUFtQyxTQUFsRixDQUEvUzs7QUFBb1ksWUFBSTRDLENBQUMsR0FBQyxTQUFGQSxDQUFFLEdBQVU7QUFBQzNDLFVBQUFBLENBQUMsQ0FBQ1gsTUFBRixDQUFTK2hCLFNBQVQsSUFBb0JwaEIsQ0FBQyxDQUFDOGtCLGNBQUYsRUFBcEI7QUFBdUMsY0FBSS9rQixDQUFDLEdBQUNDLENBQUMsQ0FBQ21qQixXQUFSO0FBQW9CbmpCLFVBQUFBLENBQUMsQ0FBQ21qQixXQUFGLEdBQWMsSUFBZCxFQUFtQm5hLENBQUMsQ0FBQ00sT0FBRixDQUFVdEosQ0FBQyxDQUFDcWpCLE9BQVosRUFBb0JyakIsQ0FBQyxDQUFDK1osV0FBRixDQUFjcFQsS0FBZCxDQUFvQjJPLEtBQXhDLENBQW5CLEVBQWtFdlYsQ0FBQyxLQUFHc2lCLEVBQUosSUFBUXJpQixDQUFDLENBQUNna0IsTUFBRixDQUFTLElBQVQsRUFBY2hrQixDQUFkLENBQTFFO0FBQTJGLFNBQXZLOztBQUF3SyxZQUFHLEtBQUtzakIsR0FBTCxDQUFTblksU0FBVCxDQUFtQkUsUUFBbkIsQ0FBNEJvWCxFQUE1QixDQUFILEVBQW1DO0FBQUMsY0FBSTdmLENBQUMsR0FBQ0MsQ0FBQyxDQUFDLEtBQUt5Z0IsR0FBTixDQUFQO0FBQWtCdGEsVUFBQUEsQ0FBQyxDQUFDSSxHQUFGLENBQU0sS0FBS2thLEdBQVgsRUFBZW5oQixDQUFmLEVBQWlCUSxDQUFqQixHQUFvQmMsQ0FBQyxDQUFDLEtBQUs2ZixHQUFOLEVBQVUxZ0IsQ0FBVixDQUFyQjtBQUFrQyxTQUF4RixNQUE2RkQsQ0FBQztBQUFHO0FBQUMsS0FBbG9GLEVBQW1vRjdGLENBQUMsQ0FBQzJaLElBQUYsR0FBTyxZQUFVO0FBQUMsVUFBSTFXLENBQUMsR0FBQyxJQUFOO0FBQUEsVUFBV0MsQ0FBQyxHQUFDLEtBQUtpa0IsYUFBTCxFQUFiO0FBQUEsVUFBa0N4akIsQ0FBQyxHQUFDLFNBQUZBLENBQUUsR0FBVTtBQUFDVixRQUFBQSxDQUFDLENBQUNvakIsV0FBRixLQUFnQmYsRUFBaEIsSUFBb0JwaUIsQ0FBQyxDQUFDeUUsVUFBdEIsSUFBa0N6RSxDQUFDLENBQUN5RSxVQUFGLENBQWE4RyxXQUFiLENBQXlCdkwsQ0FBekIsQ0FBbEMsRUFBOERELENBQUMsQ0FBQ2dsQixjQUFGLEVBQTlELEVBQWlGaGxCLENBQUMsQ0FBQ3NqQixPQUFGLENBQVVqYyxlQUFWLENBQTBCLGtCQUExQixDQUFqRixFQUErSDRCLENBQUMsQ0FBQ00sT0FBRixDQUFVdkosQ0FBQyxDQUFDc2pCLE9BQVosRUFBb0J0akIsQ0FBQyxDQUFDZ2EsV0FBRixDQUFjcFQsS0FBZCxDQUFvQjZPLE1BQXhDLENBQS9ILEVBQStLelYsQ0FBQyxDQUFDbVosT0FBRixDQUFVUyxPQUFWLEVBQS9LO0FBQW1NLE9BQWxQOztBQUFtUCxVQUFHLENBQUMzUSxDQUFDLENBQUNNLE9BQUYsQ0FBVSxLQUFLK1osT0FBZixFQUF1QixLQUFLdEosV0FBTCxDQUFpQnBULEtBQWpCLENBQXVCNE8sSUFBOUMsRUFBb0Q5TyxnQkFBeEQsRUFBeUU7QUFBQyxZQUFHekcsQ0FBQyxDQUFDbUwsU0FBRixDQUFZQyxNQUFaLENBQW1Cc1gsRUFBbkIsR0FBdUIsa0JBQWlCM25CLFFBQVEsQ0FBQzRPLGVBQTFCLElBQTJDckYsQ0FBQyxDQUFDdkosUUFBUSxDQUFDaUssSUFBVCxDQUFjNkUsUUFBZixDQUFELENBQTBCakksT0FBMUIsQ0FBbUMsVUFBUzdCLENBQVQsRUFBVztBQUFDLGlCQUFPaUosQ0FBQyxDQUFDQyxHQUFGLENBQU1sSixDQUFOLEVBQVEsV0FBUixFQUFvQjZFLENBQXBCLENBQVA7QUFBOEIsU0FBN0UsQ0FBbEUsRUFBa0osS0FBS3dlLGNBQUwsQ0FBb0JOLEVBQXBCLElBQXdCLENBQUMsQ0FBM0ssRUFBNkssS0FBS00sY0FBTCxDQUFvQlAsRUFBcEIsSUFBd0IsQ0FBQyxDQUF0TSxFQUF3TSxLQUFLTyxjQUFMLENBQW9CUixFQUFwQixJQUF3QixDQUFDLENBQWpPLEVBQW1PLEtBQUtVLEdBQUwsQ0FBU25ZLFNBQVQsQ0FBbUJFLFFBQW5CLENBQTRCb1gsRUFBNUIsQ0FBdE8sRUFBc1E7QUFBQyxjQUFJM2xCLENBQUMsR0FBQytGLENBQUMsQ0FBQzdDLENBQUQsQ0FBUDtBQUFXZ0osVUFBQUEsQ0FBQyxDQUFDSSxHQUFGLENBQU1wSixDQUFOLEVBQVFtQyxDQUFSLEVBQVUxQixDQUFWLEdBQWFnRCxDQUFDLENBQUN6RCxDQUFELEVBQUdsRCxDQUFILENBQWQ7QUFBb0IsU0FBdFMsTUFBMlMyRCxDQUFDOztBQUFHLGFBQUswaUIsV0FBTCxHQUFpQixFQUFqQjtBQUFvQjtBQUFDLEtBQXR4RyxFQUF1eEdybUIsQ0FBQyxDQUFDOGMsTUFBRixHQUFTLFlBQVU7QUFBQyxlQUFPLEtBQUtWLE9BQVosSUFBcUIsS0FBS0EsT0FBTCxDQUFhVyxjQUFiLEVBQXJCO0FBQW1ELEtBQTkxRyxFQUErMUcvYyxDQUFDLENBQUNzbkIsYUFBRixHQUFnQixZQUFVO0FBQUMsYUFBT2xkLE9BQU8sQ0FBQyxLQUFLOGQsUUFBTCxFQUFELENBQWQ7QUFBZ0MsS0FBMTVHLEVBQTI1R2xvQixDQUFDLENBQUNtbkIsYUFBRixHQUFnQixZQUFVO0FBQUMsVUFBRyxLQUFLWCxHQUFSLEVBQVksT0FBTyxLQUFLQSxHQUFaO0FBQWdCLFVBQUl2akIsQ0FBQyxHQUFDaEYsUUFBUSxDQUFDOEwsYUFBVCxDQUF1QixLQUF2QixDQUFOO0FBQW9DLGFBQU85RyxDQUFDLENBQUNnaEIsU0FBRixHQUFZLEtBQUsxaEIsTUFBTCxDQUFZZ2lCLFFBQXhCLEVBQWlDLEtBQUtpQyxHQUFMLEdBQVN2akIsQ0FBQyxDQUFDOEosUUFBRixDQUFXLENBQVgsQ0FBMUMsRUFBd0QsS0FBS3laLEdBQXBFO0FBQXdFLEtBQTlqSCxFQUErakh4bUIsQ0FBQyxDQUFDNG5CLFVBQUYsR0FBYSxZQUFVO0FBQUMsVUFBSTNrQixDQUFDLEdBQUMsS0FBS2trQixhQUFMLEVBQU47QUFBMkIsV0FBS2dCLGlCQUFMLENBQXVCaHFCLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVStZLEVBQVYsRUFBYTVpQixDQUFiLENBQXZCLEVBQXVDLEtBQUtpbEIsUUFBTCxFQUF2QyxHQUF3RGpsQixDQUFDLENBQUNvTCxTQUFGLENBQVlDLE1BQVosQ0FBbUJxWCxFQUFuQixDQUF4RCxFQUErRTFpQixDQUFDLENBQUNvTCxTQUFGLENBQVlDLE1BQVosQ0FBbUJzWCxFQUFuQixDQUEvRTtBQUFzRyxLQUF4dEgsRUFBeXRINWxCLENBQUMsQ0FBQ21vQixpQkFBRixHQUFvQixVQUFTbGxCLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsVUFBRyxTQUFPRCxDQUFWLEVBQVksT0FBTSxvQkFBaUJDLENBQWpCLEtBQW9CdUQsQ0FBQyxDQUFDdkQsQ0FBRCxDQUFyQixJQUEwQkEsQ0FBQyxDQUFDOFcsTUFBRixLQUFXOVcsQ0FBQyxHQUFDQSxDQUFDLENBQUMsQ0FBRCxDQUFkLEdBQW1CLE1BQUssS0FBS1gsTUFBTCxDQUFZa2lCLElBQVosR0FBaUJ2aEIsQ0FBQyxDQUFDeUUsVUFBRixLQUFlMUUsQ0FBZixLQUFtQkEsQ0FBQyxDQUFDZ2hCLFNBQUYsR0FBWSxFQUFaLEVBQWVoaEIsQ0FBQyxDQUFDMmQsV0FBRixDQUFjMWQsQ0FBZCxDQUFsQyxDQUFqQixHQUFxRUQsQ0FBQyxDQUFDbWxCLFNBQUYsR0FBWWxsQixDQUFDLENBQUNtbEIsV0FBeEYsQ0FBN0MsSUFBbUosTUFBSyxLQUFLOWxCLE1BQUwsQ0FBWWtpQixJQUFaLElBQWtCLEtBQUtsaUIsTUFBTCxDQUFZc2lCLFFBQVosS0FBdUIzaEIsQ0FBQyxHQUFDMGdCLEVBQUUsQ0FBQzFnQixDQUFELEVBQUcsS0FBS1gsTUFBTCxDQUFZd2lCLFNBQWYsRUFBeUIsS0FBS3hpQixNQUFMLENBQVl1aUIsVUFBckMsQ0FBM0IsR0FBNkU3aEIsQ0FBQyxDQUFDZ2hCLFNBQUYsR0FBWS9nQixDQUEzRyxJQUE4R0QsQ0FBQyxDQUFDbWxCLFNBQUYsR0FBWWxsQixDQUEvSCxDQUF6SjtBQUEyUixLQUFsaUksRUFBbWlJbEQsQ0FBQyxDQUFDa29CLFFBQUYsR0FBVyxZQUFVO0FBQUMsVUFBSWpsQixDQUFDLEdBQUMsS0FBS3NqQixPQUFMLENBQWE1Z0IsWUFBYixDQUEwQixxQkFBMUIsQ0FBTjtBQUF1RCxhQUFPMUMsQ0FBQyxLQUFHQSxDQUFDLEdBQUMsY0FBWSxPQUFPLEtBQUtWLE1BQUwsQ0FBWWlpQixLQUEvQixHQUFxQyxLQUFLamlCLE1BQUwsQ0FBWWlpQixLQUFaLENBQWtCdmQsSUFBbEIsQ0FBdUIsS0FBS3NmLE9BQTVCLENBQXJDLEdBQTBFLEtBQUtoa0IsTUFBTCxDQUFZaWlCLEtBQTNGLENBQUQsRUFBbUd2aEIsQ0FBMUc7QUFBNEcsS0FBNXRJLEVBQTZ0SWpELENBQUMsQ0FBQzRjLGdCQUFGLEdBQW1CLFVBQVMzWixDQUFULEVBQVc7QUFBQyxVQUFJQyxDQUFDLEdBQUMsSUFBTjtBQUFXLGFBQU8wQixDQUFDLENBQUMsRUFBRCxFQUFJO0FBQUMyWSxRQUFBQSxTQUFTLEVBQUN0YSxDQUFYO0FBQWF1YSxRQUFBQSxTQUFTLEVBQUM7QUFBQ3RlLFVBQUFBLE1BQU0sRUFBQyxLQUFLbWUsVUFBTCxFQUFSO0FBQTBCdkIsVUFBQUEsSUFBSSxFQUFDO0FBQUN3TSxZQUFBQSxRQUFRLEVBQUMsS0FBSy9sQixNQUFMLENBQVlxaUI7QUFBdEIsV0FBL0I7QUFBd0UyRCxVQUFBQSxLQUFLLEVBQUM7QUFBQ2hDLFlBQUFBLE9BQU8sRUFBQyxNQUFJLEtBQUt0SixXQUFMLENBQWlCMEssSUFBckIsR0FBMEI7QUFBbkMsV0FBOUU7QUFBMkhqSyxVQUFBQSxlQUFlLEVBQUM7QUFBQ0MsWUFBQUEsaUJBQWlCLEVBQUMsS0FBS3BiLE1BQUwsQ0FBWXdaO0FBQS9CO0FBQTNJLFNBQXZCO0FBQTRNeU0sUUFBQUEsUUFBUSxFQUFDLGtCQUFTdmxCLENBQVQsRUFBVztBQUFDQSxVQUFBQSxDQUFDLENBQUN3bEIsaUJBQUYsS0FBc0J4bEIsQ0FBQyxDQUFDc2EsU0FBeEIsSUFBbUNyYSxDQUFDLENBQUN3bEIsNEJBQUYsQ0FBK0J6bEIsQ0FBL0IsQ0FBbkM7QUFBcUUsU0FBdFM7QUFBdVMwbEIsUUFBQUEsUUFBUSxFQUFDLGtCQUFTMWxCLENBQVQsRUFBVztBQUFDLGlCQUFPQyxDQUFDLENBQUN3bEIsNEJBQUYsQ0FBK0J6bEIsQ0FBL0IsQ0FBUDtBQUF5QztBQUFyVyxPQUFKLEVBQTJXLEVBQTNXLEVBQThXLEtBQUtWLE1BQUwsQ0FBWTBaLFlBQTFYLENBQVI7QUFBZ1osS0FBdnBKLEVBQXdwSmpjLENBQUMsQ0FBQzhuQixtQkFBRixHQUFzQixVQUFTN2tCLENBQVQsRUFBVztBQUFDLFdBQUtra0IsYUFBTCxHQUFxQjlZLFNBQXJCLENBQStCZ0MsR0FBL0IsQ0FBbUMsZ0JBQWNwTixDQUFqRDtBQUFvRCxLQUE5dUosRUFBK3VKakQsQ0FBQyxDQUFDcWQsVUFBRixHQUFhLFlBQVU7QUFBQyxVQUFJcGEsQ0FBQyxHQUFDLElBQU47QUFBQSxVQUFXQyxDQUFDLEdBQUMsRUFBYjtBQUFnQixhQUFNLGNBQVksT0FBTyxLQUFLWCxNQUFMLENBQVlyRCxNQUEvQixHQUFzQ2dFLENBQUMsQ0FBQzZMLEVBQUYsR0FBSyxVQUFTN0wsQ0FBVCxFQUFXO0FBQUMsZUFBT0EsQ0FBQyxDQUFDb2EsT0FBRixHQUFVMVksQ0FBQyxDQUFDLEVBQUQsRUFBSTFCLENBQUMsQ0FBQ29hLE9BQU4sRUFBYyxFQUFkLEVBQWlCcmEsQ0FBQyxDQUFDVixNQUFGLENBQVNyRCxNQUFULENBQWdCZ0UsQ0FBQyxDQUFDb2EsT0FBbEIsRUFBMEJyYSxDQUFDLENBQUNzakIsT0FBNUIsS0FBc0MsRUFBdkQsQ0FBWCxFQUFzRXJqQixDQUE3RTtBQUErRSxPQUF0SSxHQUF1SUEsQ0FBQyxDQUFDaEUsTUFBRixHQUFTLEtBQUtxRCxNQUFMLENBQVlyRCxNQUE1SixFQUFtS2dFLENBQXpLO0FBQTJLLEtBQWw4SixFQUFtOEpsRCxDQUFDLENBQUMrbkIsYUFBRixHQUFnQixZQUFVO0FBQUMsYUFBTSxDQUFDLENBQUQsS0FBSyxLQUFLeGxCLE1BQUwsQ0FBWW9pQixTQUFqQixHQUEyQjFtQixRQUFRLENBQUNpSyxJQUFwQyxHQUF5Q3pCLENBQUMsQ0FBQyxLQUFLbEUsTUFBTCxDQUFZb2lCLFNBQWIsQ0FBRCxHQUF5QixLQUFLcGlCLE1BQUwsQ0FBWW9pQixTQUFyQyxHQUErQ3htQixDQUFDLENBQUMyTyxPQUFGLENBQVUsS0FBS3ZLLE1BQUwsQ0FBWW9pQixTQUF0QixDQUE5RjtBQUErSCxLQUE3bEssRUFBOGxLM2tCLENBQUMsQ0FBQzZuQixjQUFGLEdBQWlCLFVBQVM1a0IsQ0FBVCxFQUFXO0FBQUMsYUFBTzhMLEVBQUUsQ0FBQzlMLENBQUMsQ0FBQ3NFLFdBQUYsRUFBRCxDQUFUO0FBQTJCLEtBQXRwSyxFQUF1cEt2SCxDQUFDLENBQUN5bUIsYUFBRixHQUFnQixZQUFVO0FBQUMsVUFBSXhqQixDQUFDLEdBQUMsSUFBTjtBQUFXLFdBQUtWLE1BQUwsQ0FBWWlLLE9BQVosQ0FBb0JwRyxLQUFwQixDQUEwQixHQUExQixFQUErQnRCLE9BQS9CLENBQXdDLFVBQVM1QixDQUFULEVBQVc7QUFBQyxZQUFHLFlBQVVBLENBQWIsRUFBZWdKLENBQUMsQ0FBQ0csRUFBRixDQUFLcEosQ0FBQyxDQUFDc2pCLE9BQVAsRUFBZXRqQixDQUFDLENBQUNnYSxXQUFGLENBQWNwVCxLQUFkLENBQW9CMlEsS0FBbkMsRUFBeUN2WCxDQUFDLENBQUNWLE1BQUYsQ0FBU21pQixRQUFsRCxFQUE0RCxVQUFTeGhCLENBQVQsRUFBVztBQUFDLGlCQUFPRCxDQUFDLENBQUNnTixNQUFGLENBQVMvTSxDQUFULENBQVA7QUFBbUIsU0FBM0YsRUFBZixLQUFrSCxJQUFHQSxDQUFDLEtBQUcraUIsRUFBUCxFQUFVO0FBQUMsY0FBSXRpQixDQUFDLEdBQUNULENBQUMsS0FBRzRpQixFQUFKLEdBQU83aUIsQ0FBQyxDQUFDZ2EsV0FBRixDQUFjcFQsS0FBZCxDQUFvQmlKLFVBQTNCLEdBQXNDN1AsQ0FBQyxDQUFDZ2EsV0FBRixDQUFjcFQsS0FBZCxDQUFvQjRVLE9BQWhFO0FBQUEsY0FBd0V6ZSxDQUFDLEdBQUNrRCxDQUFDLEtBQUc0aUIsRUFBSixHQUFPN2lCLENBQUMsQ0FBQ2dhLFdBQUYsQ0FBY3BULEtBQWQsQ0FBb0JrSixVQUEzQixHQUFzQzlQLENBQUMsQ0FBQ2dhLFdBQUYsQ0FBY3BULEtBQWQsQ0FBb0I2YixRQUFwSTtBQUE2SXhaLFVBQUFBLENBQUMsQ0FBQ0csRUFBRixDQUFLcEosQ0FBQyxDQUFDc2pCLE9BQVAsRUFBZTVpQixDQUFmLEVBQWlCVixDQUFDLENBQUNWLE1BQUYsQ0FBU21pQixRQUExQixFQUFvQyxVQUFTeGhCLENBQVQsRUFBVztBQUFDLG1CQUFPRCxDQUFDLENBQUNna0IsTUFBRixDQUFTL2pCLENBQVQsQ0FBUDtBQUFtQixXQUFuRSxHQUFzRWdKLENBQUMsQ0FBQ0csRUFBRixDQUFLcEosQ0FBQyxDQUFDc2pCLE9BQVAsRUFBZXZtQixDQUFmLEVBQWlCaUQsQ0FBQyxDQUFDVixNQUFGLENBQVNtaUIsUUFBMUIsRUFBb0MsVUFBU3hoQixDQUFULEVBQVc7QUFBQyxtQkFBT0QsQ0FBQyxDQUFDaWtCLE1BQUYsQ0FBU2hrQixDQUFULENBQVA7QUFBbUIsV0FBbkUsQ0FBdEU7QUFBNEk7QUFBQyxPQUEzYyxHQUE4YyxLQUFLbWtCLGlCQUFMLEdBQXVCLFlBQVU7QUFBQ3BrQixRQUFBQSxDQUFDLENBQUNzakIsT0FBRixJQUFXdGpCLENBQUMsQ0FBQzBXLElBQUYsRUFBWDtBQUFvQixPQUFwZ0IsRUFBcWdCek4sQ0FBQyxDQUFDRyxFQUFGLENBQUtsTyxDQUFDLENBQUM4SyxPQUFGLENBQVUsS0FBS3NkLE9BQWYsRUFBdUIsUUFBdkIsQ0FBTCxFQUFzQyxlQUF0QyxFQUFzRCxLQUFLYyxpQkFBM0QsQ0FBcmdCLEVBQW1sQixLQUFLOWtCLE1BQUwsQ0FBWW1pQixRQUFaLEdBQXFCLEtBQUtuaUIsTUFBTCxHQUFZcUMsQ0FBQyxDQUFDLEVBQUQsRUFBSSxLQUFLckMsTUFBVCxFQUFnQjtBQUFDaUssUUFBQUEsT0FBTyxFQUFDLFFBQVQ7QUFBa0JrWSxRQUFBQSxRQUFRLEVBQUM7QUFBM0IsT0FBaEIsQ0FBbEMsR0FBa0YsS0FBS2tFLFNBQUwsRUFBcnFCO0FBQXNyQixLQUFuM0wsRUFBbzNMNW9CLENBQUMsQ0FBQzRvQixTQUFGLEdBQVksWUFBVTtBQUFDLFVBQUkzbEIsQ0FBQyxXQUFRLEtBQUtzakIsT0FBTCxDQUFhNWdCLFlBQWIsQ0FBMEIscUJBQTFCLENBQVIsQ0FBTDs7QUFBOEQsT0FBQyxLQUFLNGdCLE9BQUwsQ0FBYTVnQixZQUFiLENBQTBCLE9BQTFCLEtBQW9DLGFBQVcxQyxDQUFoRCxNQUFxRCxLQUFLc2pCLE9BQUwsQ0FBYW5XLFlBQWIsQ0FBMEIscUJBQTFCLEVBQWdELEtBQUttVyxPQUFMLENBQWE1Z0IsWUFBYixDQUEwQixPQUExQixLQUFvQyxFQUFwRixHQUF3RixLQUFLNGdCLE9BQUwsQ0FBYW5XLFlBQWIsQ0FBMEIsT0FBMUIsRUFBa0MsRUFBbEMsQ0FBN0k7QUFBb0wsS0FBN25NLEVBQThuTXBRLENBQUMsQ0FBQ2luQixNQUFGLEdBQVMsVUFBU2hrQixDQUFULEVBQVdDLENBQVgsRUFBYTtBQUFDLFVBQUlTLENBQUMsR0FBQyxLQUFLc1osV0FBTCxDQUFpQnlKLFFBQXZCO0FBQWdDLE9BQUN4akIsQ0FBQyxHQUFDQSxDQUFDLElBQUVzRixDQUFDLENBQUNFLE9BQUYsQ0FBVXpGLENBQUMsQ0FBQ3dJLGNBQVosRUFBMkI5SCxDQUEzQixDQUFOLE1BQXVDVCxDQUFDLEdBQUMsSUFBSSxLQUFLK1osV0FBVCxDQUFxQmhhLENBQUMsQ0FBQ3dJLGNBQXZCLEVBQXNDLEtBQUtxYixrQkFBTCxFQUF0QyxDQUFGLEVBQW1FdGUsQ0FBQyxDQUFDQyxPQUFGLENBQVV4RixDQUFDLENBQUN3SSxjQUFaLEVBQTJCOUgsQ0FBM0IsRUFBNkJULENBQTdCLENBQTFHLEdBQTJJRCxDQUFDLEtBQUdDLENBQUMsQ0FBQ29qQixjQUFGLENBQWlCLGNBQVlyakIsQ0FBQyxDQUFDcUksSUFBZCxHQUFtQnlhLEVBQW5CLEdBQXNCRCxFQUF2QyxJQUEyQyxDQUFDLENBQS9DLENBQTVJLEVBQThMNWlCLENBQUMsQ0FBQ2lrQixhQUFGLEdBQWtCOVksU0FBbEIsQ0FBNEJFLFFBQTVCLENBQXFDcVgsRUFBckMsS0FBMEMxaUIsQ0FBQyxDQUFDbWpCLFdBQUYsS0FBZ0JmLEVBQTFELEdBQTZEcGlCLENBQUMsQ0FBQ21qQixXQUFGLEdBQWNmLEVBQTNFLElBQStFdE8sWUFBWSxDQUFDOVQsQ0FBQyxDQUFDa2pCLFFBQUgsQ0FBWixFQUF5QmxqQixDQUFDLENBQUNtakIsV0FBRixHQUFjZixFQUF2QyxFQUEwQ3BpQixDQUFDLENBQUNYLE1BQUYsQ0FBU2hDLEtBQVQsSUFBZ0IyQyxDQUFDLENBQUNYLE1BQUYsQ0FBU2hDLEtBQVQsQ0FBZXFaLElBQS9CLEdBQW9DMVcsQ0FBQyxDQUFDa2pCLFFBQUYsR0FBV3RmLFVBQVUsQ0FBRSxZQUFVO0FBQUM1RCxRQUFBQSxDQUFDLENBQUNtakIsV0FBRixLQUFnQmYsRUFBaEIsSUFBb0JwaUIsQ0FBQyxDQUFDMFcsSUFBRixFQUFwQjtBQUE2QixPQUExQyxFQUE0QzFXLENBQUMsQ0FBQ1gsTUFBRixDQUFTaEMsS0FBVCxDQUFlcVosSUFBM0QsQ0FBekQsR0FBMEgxVyxDQUFDLENBQUMwVyxJQUFGLEVBQW5QLENBQTlMO0FBQTJiLEtBQWhuTixFQUFpbk41WixDQUFDLENBQUNrbkIsTUFBRixHQUFTLFVBQVNqa0IsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxVQUFJUyxDQUFDLEdBQUMsS0FBS3NaLFdBQUwsQ0FBaUJ5SixRQUF2QjtBQUFnQyxPQUFDeGpCLENBQUMsR0FBQ0EsQ0FBQyxJQUFFc0YsQ0FBQyxDQUFDRSxPQUFGLENBQVV6RixDQUFDLENBQUN3SSxjQUFaLEVBQTJCOUgsQ0FBM0IsQ0FBTixNQUF1Q1QsQ0FBQyxHQUFDLElBQUksS0FBSytaLFdBQVQsQ0FBcUJoYSxDQUFDLENBQUN3SSxjQUF2QixFQUFzQyxLQUFLcWIsa0JBQUwsRUFBdEMsQ0FBRixFQUFtRXRlLENBQUMsQ0FBQ0MsT0FBRixDQUFVeEYsQ0FBQyxDQUFDd0ksY0FBWixFQUEyQjlILENBQTNCLEVBQTZCVCxDQUE3QixDQUExRyxHQUEySUQsQ0FBQyxLQUFHQyxDQUFDLENBQUNvakIsY0FBRixDQUFpQixlQUFhcmpCLENBQUMsQ0FBQ3FJLElBQWYsR0FBb0J5YSxFQUFwQixHQUF1QkQsRUFBeEMsSUFBNEMsQ0FBQyxDQUFoRCxDQUE1SSxFQUErTDVpQixDQUFDLENBQUM4akIsb0JBQUYsT0FBMkJoUSxZQUFZLENBQUM5VCxDQUFDLENBQUNrakIsUUFBSCxDQUFaLEVBQXlCbGpCLENBQUMsQ0FBQ21qQixXQUFGLEdBQWNkLEVBQXZDLEVBQTBDcmlCLENBQUMsQ0FBQ1gsTUFBRixDQUFTaEMsS0FBVCxJQUFnQjJDLENBQUMsQ0FBQ1gsTUFBRixDQUFTaEMsS0FBVCxDQUFlb1osSUFBL0IsR0FBb0N6VyxDQUFDLENBQUNrakIsUUFBRixHQUFXdGYsVUFBVSxDQUFFLFlBQVU7QUFBQzVELFFBQUFBLENBQUMsQ0FBQ21qQixXQUFGLEtBQWdCZCxFQUFoQixJQUFvQnJpQixDQUFDLENBQUN5VyxJQUFGLEVBQXBCO0FBQTZCLE9BQTFDLEVBQTRDelcsQ0FBQyxDQUFDWCxNQUFGLENBQVNoQyxLQUFULENBQWVvWixJQUEzRCxDQUF6RCxHQUEwSHpXLENBQUMsQ0FBQ3lXLElBQUYsRUFBL0wsQ0FBL0w7QUFBd1ksS0FBaGpPLEVBQWlqTzNaLENBQUMsQ0FBQ2duQixvQkFBRixHQUF1QixZQUFVO0FBQUMsV0FBSSxJQUFJL2pCLENBQVIsSUFBYSxLQUFLcWpCLGNBQWxCO0FBQWlDLFlBQUcsS0FBS0EsY0FBTCxDQUFvQnJqQixDQUFwQixDQUFILEVBQTBCLE9BQU0sQ0FBQyxDQUFQO0FBQTNEOztBQUFvRSxhQUFNLENBQUMsQ0FBUDtBQUFTLEtBQWhxTyxFQUFpcU9qRCxDQUFDLENBQUNxVixVQUFGLEdBQWEsVUFBU3BTLENBQVQsRUFBVztBQUFDLFVBQUlDLENBQUMsR0FBQzBOLEVBQUUsQ0FBQ0csaUJBQUgsQ0FBcUIsS0FBS3dWLE9BQTFCLENBQU47QUFBeUMsYUFBT3ZpQixNQUFNLENBQUNNLElBQVAsQ0FBWXBCLENBQVosRUFBZTRCLE9BQWYsQ0FBd0IsVUFBUzdCLENBQVQsRUFBVztBQUFDLFNBQUMsQ0FBRCxLQUFLbWhCLEVBQUUsQ0FBQ3RZLE9BQUgsQ0FBVzdJLENBQVgsQ0FBTCxJQUFvQixPQUFPQyxDQUFDLENBQUNELENBQUQsQ0FBNUI7QUFBZ0MsT0FBcEUsR0FBdUVBLENBQUMsSUFBRSxvQkFBaUJBLENBQUMsQ0FBQzBoQixTQUFuQixDQUFILElBQWlDMWhCLENBQUMsQ0FBQzBoQixTQUFGLENBQVkzSyxNQUE3QyxLQUFzRC9XLENBQUMsQ0FBQzBoQixTQUFGLEdBQVkxaEIsQ0FBQyxDQUFDMGhCLFNBQUYsQ0FBWSxDQUFaLENBQWxFLENBQXZFLEVBQXlKLFlBQVUsT0FBTSxDQUFDMWhCLENBQUMsR0FBQzJCLENBQUMsQ0FBQyxFQUFELEVBQUksS0FBS3FZLFdBQUwsQ0FBaUJDLE9BQXJCLEVBQTZCLEVBQTdCLEVBQWdDaGEsQ0FBaEMsRUFBa0MsRUFBbEMsRUFBcUMsb0JBQWlCRCxDQUFqQixLQUFvQkEsQ0FBcEIsR0FBc0JBLENBQXRCLEdBQXdCLEVBQTdELENBQUosRUFBc0UxQyxLQUF0RixLQUE4RjBDLENBQUMsQ0FBQzFDLEtBQUYsR0FBUTtBQUFDcVosUUFBQUEsSUFBSSxFQUFDM1csQ0FBQyxDQUFDMUMsS0FBUjtBQUFjb1osUUFBQUEsSUFBSSxFQUFDMVcsQ0FBQyxDQUFDMUM7QUFBckIsT0FBdEcsQ0FBekosRUFBNFIsWUFBVSxPQUFPMEMsQ0FBQyxDQUFDdWhCLEtBQW5CLEtBQTJCdmhCLENBQUMsQ0FBQ3VoQixLQUFGLEdBQVF2aEIsQ0FBQyxDQUFDdWhCLEtBQUYsQ0FBUXhkLFFBQVIsRUFBbkMsQ0FBNVIsRUFBbVYsWUFBVSxPQUFPL0QsQ0FBQyxDQUFDNGxCLE9BQW5CLEtBQTZCNWxCLENBQUMsQ0FBQzRsQixPQUFGLEdBQVU1bEIsQ0FBQyxDQUFDNGxCLE9BQUYsQ0FBVTdoQixRQUFWLEVBQXZDLENBQW5WLEVBQWdaRCxDQUFDLENBQUNtZCxFQUFELEVBQUlqaEIsQ0FBSixFQUFNLEtBQUtnYSxXQUFMLENBQWlCRSxXQUF2QixDQUFqWixFQUFxYmxhLENBQUMsQ0FBQzRoQixRQUFGLEtBQWE1aEIsQ0FBQyxDQUFDc2hCLFFBQUYsR0FBV1gsRUFBRSxDQUFDM2dCLENBQUMsQ0FBQ3NoQixRQUFILEVBQVl0aEIsQ0FBQyxDQUFDOGhCLFNBQWQsRUFBd0I5aEIsQ0FBQyxDQUFDNmhCLFVBQTFCLENBQTFCLENBQXJiLEVBQXNmN2hCLENBQTdmO0FBQStmLEtBQWx1UCxFQUFtdVBqRCxDQUFDLENBQUM4bUIsa0JBQUYsR0FBcUIsWUFBVTtBQUFDLFVBQUk3akIsQ0FBQyxHQUFDLEVBQU47QUFBUyxVQUFHLEtBQUtWLE1BQVIsRUFBZSxLQUFJLElBQUlXLENBQVIsSUFBYSxLQUFLWCxNQUFsQjtBQUF5QixhQUFLMGEsV0FBTCxDQUFpQkMsT0FBakIsQ0FBeUJoYSxDQUF6QixNQUE4QixLQUFLWCxNQUFMLENBQVlXLENBQVosQ0FBOUIsS0FBK0NELENBQUMsQ0FBQ0MsQ0FBRCxDQUFELEdBQUssS0FBS1gsTUFBTCxDQUFZVyxDQUFaLENBQXBEO0FBQXpCO0FBQTZGLGFBQU9ELENBQVA7QUFBUyxLQUFqNFAsRUFBazRQakQsQ0FBQyxDQUFDaW9CLGNBQUYsR0FBaUIsWUFBVTtBQUFDLFVBQUlobEIsQ0FBQyxHQUFDLEtBQUtra0IsYUFBTCxFQUFOO0FBQUEsVUFBMkJqa0IsQ0FBQyxHQUFDRCxDQUFDLENBQUMwQyxZQUFGLENBQWUsT0FBZixFQUF3QnVCLEtBQXhCLENBQThCaWQsRUFBOUIsQ0FBN0I7QUFBK0QsZUFBT2poQixDQUFQLElBQVVBLENBQUMsQ0FBQ1UsTUFBWixJQUFvQlYsQ0FBQyxDQUFDNGxCLEdBQUYsQ0FBTyxVQUFTN2xCLENBQVQsRUFBVztBQUFDLGVBQU9BLENBQUMsQ0FBQzJDLElBQUYsRUFBUDtBQUFnQixPQUFuQyxFQUFzQ2QsT0FBdEMsQ0FBK0MsVUFBUzVCLENBQVQsRUFBVztBQUFDLGVBQU9ELENBQUMsQ0FBQ29MLFNBQUYsQ0FBWUMsTUFBWixDQUFtQnBMLENBQW5CLENBQVA7QUFBNkIsT0FBeEYsQ0FBcEI7QUFBK0csS0FBNWtRLEVBQTZrUWxELENBQUMsQ0FBQzBvQiw0QkFBRixHQUErQixVQUFTemxCLENBQVQsRUFBVztBQUFDLFVBQUlDLENBQUMsR0FBQ0QsQ0FBQyxDQUFDOGxCLFFBQVI7QUFBaUIsV0FBS3ZDLEdBQUwsR0FBU3RqQixDQUFDLENBQUM4bEIsTUFBWCxFQUFrQixLQUFLZixjQUFMLEVBQWxCLEVBQXdDLEtBQUtILG1CQUFMLENBQXlCLEtBQUtELGNBQUwsQ0FBb0I1a0IsQ0FBQyxDQUFDc2EsU0FBdEIsQ0FBekIsQ0FBeEM7QUFBbUcsS0FBNXVRLEVBQTZ1UXZkLENBQUMsQ0FBQ2dvQixjQUFGLEdBQWlCLFlBQVU7QUFBQyxVQUFJL2tCLENBQUMsR0FBQyxLQUFLa2tCLGFBQUwsRUFBTjtBQUFBLFVBQTJCamtCLENBQUMsR0FBQyxLQUFLWCxNQUFMLENBQVkraEIsU0FBekM7QUFBbUQsZUFBT3JoQixDQUFDLENBQUMwQyxZQUFGLENBQWUsYUFBZixDQUFQLEtBQXVDMUMsQ0FBQyxDQUFDb0wsU0FBRixDQUFZQyxNQUFaLENBQW1CcVgsRUFBbkIsR0FBdUIsS0FBS3BqQixNQUFMLENBQVkraEIsU0FBWixHQUFzQixDQUFDLENBQTlDLEVBQWdELEtBQUszSyxJQUFMLEVBQWhELEVBQTRELEtBQUtDLElBQUwsRUFBNUQsRUFBd0UsS0FBS3JYLE1BQUwsQ0FBWStoQixTQUFaLEdBQXNCcGhCLENBQXJJO0FBQXdJLEtBQXA4USxFQUFxOFFBLENBQUMsQ0FBQ3dMLGVBQUYsR0FBa0IsVUFBU3pMLENBQVQsRUFBVztBQUFDLGFBQU8sS0FBS3ZFLElBQUwsQ0FBVyxZQUFVO0FBQUMsWUFBSWlGLENBQUMsR0FBQzZFLENBQUMsQ0FBQ0UsT0FBRixDQUFVLElBQVYsRUFBZSxZQUFmLENBQU47QUFBQSxZQUFtQzFJLENBQUMsR0FBQyxvQkFBaUJpRCxDQUFqQixLQUFvQkEsQ0FBekQ7O0FBQTJELFlBQUcsQ0FBQ1UsQ0FBQyxJQUFFLENBQUMsZUFBZTBELElBQWYsQ0FBb0JwRSxDQUFwQixDQUFMLE1BQStCVSxDQUFDLEtBQUdBLENBQUMsR0FBQyxJQUFJVCxDQUFKLENBQU0sSUFBTixFQUFXbEQsQ0FBWCxDQUFMLENBQUQsRUFBcUIsWUFBVSxPQUFPaUQsQ0FBckUsQ0FBSCxFQUEyRTtBQUFDLGNBQUcsZUFBYSxPQUFPVSxDQUFDLENBQUNWLENBQUQsQ0FBeEIsRUFBNEIsTUFBTSxJQUFJMFUsU0FBSixDQUFjLHNCQUFvQjFVLENBQXBCLEdBQXNCLEdBQXBDLENBQU47QUFBK0NVLFVBQUFBLENBQUMsQ0FBQ1YsQ0FBRCxDQUFEO0FBQU87QUFBQyxPQUFoUCxDQUFQO0FBQTBQLEtBQTd0UixFQUE4dFJDLENBQUMsQ0FBQzBMLFdBQUYsR0FBYyxVQUFTM0wsQ0FBVCxFQUFXO0FBQUMsYUFBT3VGLENBQUMsQ0FBQ0UsT0FBRixDQUFVekYsQ0FBVixFQUFZLFlBQVosQ0FBUDtBQUFpQyxLQUF6eFIsRUFBMHhSVSxDQUFDLENBQUNULENBQUQsRUFBRyxJQUFILEVBQVEsQ0FBQztBQUFDZ0IsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTSxPQUFOO0FBQWM7QUFBNUMsS0FBRCxFQUErQztBQUFDckUsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTzhjLEVBQVA7QUFBVTtBQUF4QyxLQUEvQyxFQUF5RjtBQUFDbmhCLE1BQUFBLEdBQUcsRUFBQyxNQUFMO0FBQVlxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU8yYixFQUFQO0FBQVU7QUFBckMsS0FBekYsRUFBZ0k7QUFBQ2hnQixNQUFBQSxHQUFHLEVBQUMsVUFBTDtBQUFnQnFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTSxZQUFOO0FBQW1CO0FBQWxELEtBQWhJLEVBQW9MO0FBQUNyRSxNQUFBQSxHQUFHLEVBQUMsT0FBTDtBQUFhcUUsTUFBQUEsR0FBRyxFQUFDLGVBQVU7QUFBQyxlQUFPaWQsRUFBUDtBQUFVO0FBQXRDLEtBQXBMLEVBQTROO0FBQUN0aEIsTUFBQUEsR0FBRyxFQUFDLFdBQUw7QUFBaUJxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU0sYUFBTjtBQUFvQjtBQUFwRCxLQUE1TixFQUFrUjtBQUFDckUsTUFBQUEsR0FBRyxFQUFDLGFBQUw7QUFBbUJxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU84YixFQUFQO0FBQVU7QUFBNUMsS0FBbFIsQ0FBUixDQUEzeFIsRUFBcW1TbmhCLENBQTVtUztBQUE4bVMsR0FBditTLEVBQWx6QztBQUFBLE1BQTR4VitsQixFQUFFLEdBQUNoaEIsQ0FBQyxFQUFoeVY7O0FBQW15VixNQUFHZ2hCLEVBQUgsRUFBTTtBQUFDLFFBQUlDLEVBQUUsR0FBQ0QsRUFBRSxDQUFDbGEsRUFBSCxDQUFNb2EsT0FBYjtBQUFxQkYsSUFBQUEsRUFBRSxDQUFDbGEsRUFBSCxDQUFNb2EsT0FBTixHQUFjakQsRUFBRSxDQUFDeFgsZUFBakIsRUFBaUN1YSxFQUFFLENBQUNsYSxFQUFILENBQU1vYSxPQUFOLENBQWNsYSxXQUFkLEdBQTBCaVgsRUFBM0QsRUFBOEQrQyxFQUFFLENBQUNsYSxFQUFILENBQU1vYSxPQUFOLENBQWNqYSxVQUFkLEdBQXlCLFlBQVU7QUFBQyxhQUFPK1osRUFBRSxDQUFDbGEsRUFBSCxDQUFNb2EsT0FBTixHQUFjRCxFQUFkLEVBQWlCaEQsRUFBRSxDQUFDeFgsZUFBM0I7QUFBMkMsS0FBN0k7QUFBOEk7O0FBQUEsTUFBSTBhLEVBQUUsR0FBQyxTQUFQO0FBQUEsTUFBaUJDLEVBQUUsR0FBQyxJQUFJamlCLE1BQUosQ0FBVyx1QkFBWCxFQUFtQyxHQUFuQyxDQUFwQjtBQUFBLE1BQTREa2lCLEVBQUUsR0FBQzFrQixDQUFDLENBQUMsRUFBRCxFQUFJc2hCLEVBQUUsQ0FBQ2hKLE9BQVAsRUFBZTtBQUFDSyxJQUFBQSxTQUFTLEVBQUMsT0FBWDtBQUFtQi9RLElBQUFBLE9BQU8sRUFBQyxPQUEzQjtBQUFtQ3FjLElBQUFBLE9BQU8sRUFBQyxFQUEzQztBQUE4Q3RFLElBQUFBLFFBQVEsRUFBQztBQUF2RCxHQUFmLENBQWhFO0FBQUEsTUFBc1JnRixFQUFFLEdBQUMza0IsQ0FBQyxDQUFDLEVBQUQsRUFBSXNoQixFQUFFLENBQUMvSSxXQUFQLEVBQW1CO0FBQUMwTCxJQUFBQSxPQUFPLEVBQUM7QUFBVCxHQUFuQixDQUExUjtBQUFBLE1BQW9WVyxFQUFFLEdBQUMsTUFBdlY7QUFBQSxNQUE4VkMsRUFBRSxHQUFDLE1BQWpXO0FBQUEsTUFBd1dDLEVBQUUsR0FBQyxpQkFBM1c7QUFBQSxNQUE2WEMsRUFBRSxHQUFDLGVBQWhZO0FBQUEsTUFBZ1pDLEVBQUUsR0FBQztBQUFDblIsSUFBQUEsSUFBSSxFQUFDLGlCQUFOO0FBQXdCQyxJQUFBQSxNQUFNLEVBQUMsbUJBQS9CO0FBQW1ESCxJQUFBQSxJQUFJLEVBQUMsaUJBQXhEO0FBQTBFQyxJQUFBQSxLQUFLLEVBQUMsa0JBQWhGO0FBQW1HaU4sSUFBQUEsUUFBUSxFQUFDLHFCQUE1RztBQUFrSWpMLElBQUFBLEtBQUssRUFBQyxrQkFBeEk7QUFBMkppRSxJQUFBQSxPQUFPLEVBQUMsb0JBQW5LO0FBQXdMaUgsSUFBQUEsUUFBUSxFQUFDLHFCQUFqTTtBQUF1TjVTLElBQUFBLFVBQVUsRUFBQyx1QkFBbE87QUFBMFBDLElBQUFBLFVBQVUsRUFBQztBQUFyUSxHQUFuWjtBQUFBLE1BQWlyQjhXLEVBQUUsR0FBQyxVQUFTNW1CLENBQVQsRUFBVztBQUFDLFFBQUlDLENBQUosRUFBTWxELENBQU47O0FBQVEsYUFBU3FFLENBQVQsR0FBWTtBQUFDLGFBQU9wQixDQUFDLENBQUMwQixLQUFGLENBQVEsSUFBUixFQUFhRSxTQUFiLEtBQXlCLElBQWhDO0FBQXFDOztBQUFBN0UsSUFBQUEsQ0FBQyxHQUFDaUQsQ0FBRixFQUFJLENBQUNDLENBQUMsR0FBQ21CLENBQUgsRUFBTUYsU0FBTixHQUFnQkgsTUFBTSxDQUFDOGxCLE1BQVAsQ0FBYzlwQixDQUFDLENBQUNtRSxTQUFoQixDQUFwQixFQUErQ2pCLENBQUMsQ0FBQ2lCLFNBQUYsQ0FBWThZLFdBQVosR0FBd0IvWixDQUF2RSxFQUF5RUEsQ0FBQyxDQUFDNm1CLFNBQUYsR0FBWS9wQixDQUFyRjtBQUF1RixRQUFJNEUsQ0FBQyxHQUFDUCxDQUFDLENBQUNGLFNBQVI7QUFBa0IsV0FBT1MsQ0FBQyxDQUFDMGlCLGFBQUYsR0FBZ0IsWUFBVTtBQUFDLGFBQU8sS0FBS1ksUUFBTCxNQUFpQixLQUFLOEIsV0FBTCxFQUF4QjtBQUEyQyxLQUF0RSxFQUF1RXBsQixDQUFDLENBQUNnakIsVUFBRixHQUFhLFlBQVU7QUFBQyxVQUFJM2tCLENBQUMsR0FBQyxLQUFLa2tCLGFBQUwsRUFBTjtBQUEyQixXQUFLZ0IsaUJBQUwsQ0FBdUJocUIsQ0FBQyxDQUFDMk8sT0FBRixDQUFVNGMsRUFBVixFQUFhem1CLENBQWIsQ0FBdkIsRUFBdUMsS0FBS2lsQixRQUFMLEVBQXZDOztBQUF3RCxVQUFJaGxCLENBQUMsR0FBQyxLQUFLOG1CLFdBQUwsRUFBTjs7QUFBeUIsb0JBQVksT0FBTzltQixDQUFuQixLQUF1QkEsQ0FBQyxHQUFDQSxDQUFDLENBQUMrRCxJQUFGLENBQU8sS0FBS3NmLE9BQVosQ0FBekIsR0FBK0MsS0FBSzRCLGlCQUFMLENBQXVCaHFCLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVTZjLEVBQVYsRUFBYTFtQixDQUFiLENBQXZCLEVBQXVDQyxDQUF2QyxDQUEvQyxFQUF5RkQsQ0FBQyxDQUFDb0wsU0FBRixDQUFZQyxNQUFaLENBQW1Ca2IsRUFBbkIsQ0FBekYsRUFBZ0h2bUIsQ0FBQyxDQUFDb0wsU0FBRixDQUFZQyxNQUFaLENBQW1CbWIsRUFBbkIsQ0FBaEg7QUFBdUksS0FBbFYsRUFBbVY3a0IsQ0FBQyxDQUFDa2pCLG1CQUFGLEdBQXNCLFVBQVM3a0IsQ0FBVCxFQUFXO0FBQUMsV0FBS2trQixhQUFMLEdBQXFCOVksU0FBckIsQ0FBK0JnQyxHQUEvQixDQUFtQyxnQkFBY3BOLENBQWpEO0FBQW9ELEtBQXphLEVBQTBhMkIsQ0FBQyxDQUFDb2xCLFdBQUYsR0FBYyxZQUFVO0FBQUMsYUFBTyxLQUFLekQsT0FBTCxDQUFhNWdCLFlBQWIsQ0FBMEIsY0FBMUIsS0FBMkMsS0FBS3BELE1BQUwsQ0FBWXNtQixPQUE5RDtBQUFzRSxLQUF6Z0IsRUFBMGdCamtCLENBQUMsQ0FBQ3FqQixjQUFGLEdBQWlCLFlBQVU7QUFBQyxVQUFJaGxCLENBQUMsR0FBQyxLQUFLa2tCLGFBQUwsRUFBTjtBQUFBLFVBQTJCamtCLENBQUMsR0FBQ0QsQ0FBQyxDQUFDMEMsWUFBRixDQUFlLE9BQWYsRUFBd0J1QixLQUF4QixDQUE4Qm1pQixFQUE5QixDQUE3QjtBQUErRCxlQUFPbm1CLENBQVAsSUFBVUEsQ0FBQyxDQUFDVSxNQUFGLEdBQVMsQ0FBbkIsSUFBc0JWLENBQUMsQ0FBQzRsQixHQUFGLENBQU8sVUFBUzdsQixDQUFULEVBQVc7QUFBQyxlQUFPQSxDQUFDLENBQUMyQyxJQUFGLEVBQVA7QUFBZ0IsT0FBbkMsRUFBc0NkLE9BQXRDLENBQStDLFVBQVM1QixDQUFULEVBQVc7QUFBQyxlQUFPRCxDQUFDLENBQUNvTCxTQUFGLENBQVlDLE1BQVosQ0FBbUJwTCxDQUFuQixDQUFQO0FBQTZCLE9BQXhGLENBQXRCO0FBQWlILEtBQXR0QixFQUF1dEJtQixDQUFDLENBQUNxSyxlQUFGLEdBQWtCLFVBQVN6TCxDQUFULEVBQVc7QUFBQyxhQUFPLEtBQUt2RSxJQUFMLENBQVcsWUFBVTtBQUFDLFlBQUl3RSxDQUFDLEdBQUNzRixDQUFDLENBQUNFLE9BQUYsQ0FBVSxJQUFWLEVBQWUsWUFBZixDQUFOO0FBQUEsWUFBbUMvRSxDQUFDLEdBQUMsb0JBQWlCVixDQUFqQixJQUFtQkEsQ0FBbkIsR0FBcUIsSUFBMUQ7O0FBQStELFlBQUcsQ0FBQ0MsQ0FBQyxJQUFFLENBQUMsZUFBZW1FLElBQWYsQ0FBb0JwRSxDQUFwQixDQUFMLE1BQStCQyxDQUFDLEtBQUdBLENBQUMsR0FBQyxJQUFJbUIsQ0FBSixDQUFNLElBQU4sRUFBV1YsQ0FBWCxDQUFGLEVBQWdCNkUsQ0FBQyxDQUFDQyxPQUFGLENBQVUsSUFBVixFQUFlLFlBQWYsRUFBNEJ2RixDQUE1QixDQUFuQixDQUFELEVBQW9ELFlBQVUsT0FBT0QsQ0FBcEcsQ0FBSCxFQUEwRztBQUFDLGNBQUcsZUFBYSxPQUFPQyxDQUFDLENBQUNELENBQUQsQ0FBeEIsRUFBNEIsTUFBTSxJQUFJMFUsU0FBSixDQUFjLHNCQUFvQjFVLENBQXBCLEdBQXNCLEdBQXBDLENBQU47QUFBK0NDLFVBQUFBLENBQUMsQ0FBQ0QsQ0FBRCxDQUFEO0FBQU87QUFBQyxPQUFuUixDQUFQO0FBQTZSLEtBQWxoQyxFQUFtaENvQixDQUFDLENBQUN1SyxXQUFGLEdBQWMsVUFBUzNMLENBQVQsRUFBVztBQUFDLGFBQU91RixDQUFDLENBQUNFLE9BQUYsQ0FBVXpGLENBQVYsRUFBWSxZQUFaLENBQVA7QUFBaUMsS0FBOWtDLEVBQStrQ1UsQ0FBQyxDQUFDVSxDQUFELEVBQUcsSUFBSCxFQUFRLENBQUM7QUFBQ0gsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTSxPQUFOO0FBQWM7QUFBNUMsS0FBRCxFQUErQztBQUFDckUsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTytnQixFQUFQO0FBQVU7QUFBeEMsS0FBL0MsRUFBeUY7QUFBQ3BsQixNQUFBQSxHQUFHLEVBQUMsTUFBTDtBQUFZcUUsTUFBQUEsR0FBRyxFQUFDLGVBQVU7QUFBQyxlQUFPNmdCLEVBQVA7QUFBVTtBQUFyQyxLQUF6RixFQUFnSTtBQUFDbGxCLE1BQUFBLEdBQUcsRUFBQyxVQUFMO0FBQWdCcUUsTUFBQUEsR0FBRyxFQUFDLGVBQVU7QUFBQyxlQUFNLFlBQU47QUFBbUI7QUFBbEQsS0FBaEksRUFBb0w7QUFBQ3JFLE1BQUFBLEdBQUcsRUFBQyxPQUFMO0FBQWFxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU9xaEIsRUFBUDtBQUFVO0FBQXRDLEtBQXBMLEVBQTROO0FBQUMxbEIsTUFBQUEsR0FBRyxFQUFDLFdBQUw7QUFBaUJxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU0sYUFBTjtBQUFvQjtBQUFwRCxLQUE1TixFQUFrUjtBQUFDckUsTUFBQUEsR0FBRyxFQUFDLGFBQUw7QUFBbUJxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU9naEIsRUFBUDtBQUFVO0FBQTVDLEtBQWxSLENBQVIsQ0FBaGxDLEVBQTA1Q2xsQixDQUFqNkM7QUFBbTZDLEdBQWxsRCxDQUFtbEQ2aEIsRUFBbmxELENBQXByQjtBQUFBLE1BQTJ3RStELEVBQUUsR0FBQ2hpQixDQUFDLEVBQS93RTs7QUFBa3hFLE1BQUdnaUIsRUFBSCxFQUFNO0FBQUMsUUFBSUMsRUFBRSxHQUFDRCxFQUFFLENBQUNsYixFQUFILENBQU1vYixPQUFiO0FBQXFCRixJQUFBQSxFQUFFLENBQUNsYixFQUFILENBQU1vYixPQUFOLEdBQWNOLEVBQUUsQ0FBQ25iLGVBQWpCLEVBQWlDdWIsRUFBRSxDQUFDbGIsRUFBSCxDQUFNb2IsT0FBTixDQUFjbGIsV0FBZCxHQUEwQjRhLEVBQTNELEVBQThESSxFQUFFLENBQUNsYixFQUFILENBQU1vYixPQUFOLENBQWNqYixVQUFkLEdBQXlCLFlBQVU7QUFBQyxhQUFPK2EsRUFBRSxDQUFDbGIsRUFBSCxDQUFNb2IsT0FBTixHQUFjRCxFQUFkLEVBQWlCTCxFQUFFLENBQUNuYixlQUEzQjtBQUEyQyxLQUE3STtBQUE4STs7QUFBQSxNQUFJMGIsRUFBRSxHQUFDLFdBQVA7QUFBQSxNQUFtQkMsRUFBRSxHQUFDLGNBQXRCO0FBQUEsTUFBcUNDLEVBQUUsR0FBQyxNQUFJRCxFQUE1QztBQUFBLE1BQStDRSxFQUFFLEdBQUM7QUFBQ3JyQixJQUFBQSxNQUFNLEVBQUMsRUFBUjtBQUFXc3JCLElBQUFBLE1BQU0sRUFBQyxNQUFsQjtBQUF5QnZlLElBQUFBLE1BQU0sRUFBQztBQUFoQyxHQUFsRDtBQUFBLE1BQXNGd2UsRUFBRSxHQUFDO0FBQUN2ckIsSUFBQUEsTUFBTSxFQUFDLFFBQVI7QUFBaUJzckIsSUFBQUEsTUFBTSxFQUFDLFFBQXhCO0FBQWlDdmUsSUFBQUEsTUFBTSxFQUFDO0FBQXhDLEdBQXpGO0FBQUEsTUFBcUp5ZSxFQUFFLEdBQUM7QUFBQ0MsSUFBQUEsUUFBUSxFQUFDLGFBQVdMLEVBQXJCO0FBQXdCTSxJQUFBQSxNQUFNLEVBQUMsV0FBU04sRUFBeEM7QUFBMkNoWCxJQUFBQSxhQUFhLEVBQUMsU0FBT2dYLEVBQVAsR0FBVTtBQUFuRSxHQUF4SjtBQUFBLE1BQXdPTyxFQUFFLEdBQUM7QUFBQ0MsSUFBQUEsYUFBYSxFQUFDLGVBQWY7QUFBK0I5VyxJQUFBQSxNQUFNLEVBQUM7QUFBdEMsR0FBM087QUFBQSxNQUEyUitXLEVBQUUsR0FBQztBQUFDQyxJQUFBQSxRQUFRLEVBQUMscUJBQVY7QUFBZ0NDLElBQUFBLGNBQWMsRUFBQyxtQkFBL0M7QUFBbUVDLElBQUFBLFNBQVMsRUFBQyxXQUE3RTtBQUF5RkMsSUFBQUEsU0FBUyxFQUFDLFdBQW5HO0FBQStHQyxJQUFBQSxVQUFVLEVBQUMsa0JBQTFIO0FBQTZJQyxJQUFBQSxRQUFRLEVBQUMsV0FBdEo7QUFBa0tDLElBQUFBLGVBQWUsRUFBQztBQUFsTCxHQUE5UjtBQUFBLE1BQW9lQyxFQUFFLEdBQUMsUUFBdmU7QUFBQSxNQUFnZkMsRUFBRSxHQUFDLFVBQW5mO0FBQUEsTUFBOGZDLEVBQUUsR0FBQyxZQUFVO0FBQUMsYUFBU3hvQixDQUFULENBQVdBLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsVUFBSVMsQ0FBQyxHQUFDLElBQU47QUFBVyxXQUFLb0ssUUFBTCxHQUFjOUssQ0FBZCxFQUFnQixLQUFLeW9CLGNBQUwsR0FBb0IsV0FBU3pvQixDQUFDLENBQUNnVSxPQUFYLEdBQW1CcFYsTUFBbkIsR0FBMEJvQixDQUE5RCxFQUFnRSxLQUFLbVMsT0FBTCxHQUFhLEtBQUtDLFVBQUwsQ0FBZ0JuUyxDQUFoQixDQUE3RSxFQUFnRyxLQUFLcVcsU0FBTCxHQUFlLEtBQUtuRSxPQUFMLENBQWFuSixNQUFiLEdBQW9CLEdBQXBCLEdBQXdCOGUsRUFBRSxDQUFDRyxTQUEzQixHQUFxQyxHQUFyQyxHQUF5QyxLQUFLOVYsT0FBTCxDQUFhbkosTUFBdEQsR0FBNkQsR0FBN0QsR0FBaUU4ZSxFQUFFLENBQUNLLFVBQXBFLEdBQStFLEdBQS9FLEdBQW1GLEtBQUtoVyxPQUFMLENBQWFuSixNQUFoRyxHQUF1RyxJQUF2RyxHQUE0RzRlLEVBQUUsQ0FBQ0MsYUFBOU4sRUFBNE8sS0FBS2EsUUFBTCxHQUFjLEVBQTFQLEVBQTZQLEtBQUtDLFFBQUwsR0FBYyxFQUEzUSxFQUE4USxLQUFLQyxhQUFMLEdBQW1CLElBQWpTLEVBQXNTLEtBQUtDLGFBQUwsR0FBbUIsQ0FBelQsRUFBMlQ1ZixDQUFDLENBQUNHLEVBQUYsQ0FBSyxLQUFLcWYsY0FBVixFQUF5QmhCLEVBQUUsQ0FBQ0UsTUFBNUIsRUFBb0MsVUFBUzNuQixDQUFULEVBQVc7QUFBQyxlQUFPVSxDQUFDLENBQUNvb0IsUUFBRixDQUFXOW9CLENBQVgsQ0FBUDtBQUFxQixPQUFyRSxDQUEzVCxFQUFtWSxLQUFLK29CLE9BQUwsRUFBblksRUFBa1osS0FBS0QsUUFBTCxFQUFsWixFQUFrYXZqQixDQUFDLENBQUNDLE9BQUYsQ0FBVXhGLENBQVYsRUFBWW9uQixFQUFaLEVBQWUsSUFBZixDQUFsYTtBQUF1Yjs7QUFBQSxRQUFJbm5CLENBQUMsR0FBQ0QsQ0FBQyxDQUFDa0IsU0FBUjtBQUFrQixXQUFPakIsQ0FBQyxDQUFDOG9CLE9BQUYsR0FBVSxZQUFVO0FBQUMsVUFBSS9vQixDQUFDLEdBQUMsSUFBTjtBQUFBLFVBQVdDLENBQUMsR0FBQyxLQUFLd29CLGNBQUwsS0FBc0IsS0FBS0EsY0FBTCxDQUFvQjdwQixNQUExQyxHQUFpRDBwQixFQUFqRCxHQUFvREMsRUFBakU7QUFBQSxVQUFvRTduQixDQUFDLEdBQUMsV0FBUyxLQUFLeVIsT0FBTCxDQUFhb1YsTUFBdEIsR0FBNkJ0bkIsQ0FBN0IsR0FBK0IsS0FBS2tTLE9BQUwsQ0FBYW9WLE1BQWxIO0FBQUEsVUFBeUh4cUIsQ0FBQyxHQUFDMkQsQ0FBQyxLQUFHNm5CLEVBQUosR0FBTyxLQUFLUyxhQUFMLEVBQVAsR0FBNEIsQ0FBdko7QUFBeUosV0FBS04sUUFBTCxHQUFjLEVBQWQsRUFBaUIsS0FBS0MsUUFBTCxHQUFjLEVBQS9CLEVBQWtDLEtBQUtFLGFBQUwsR0FBbUIsS0FBS0ksZ0JBQUwsRUFBckQsRUFBNkUxa0IsQ0FBQyxDQUFDckosQ0FBQyxDQUFDeU8sSUFBRixDQUFPLEtBQUsyTSxTQUFaLENBQUQsQ0FBRCxDQUEwQnVQLEdBQTFCLENBQStCLFVBQVM3bEIsQ0FBVCxFQUFXO0FBQUMsWUFBSUMsQ0FBSjtBQUFBLFlBQU1tQixDQUFDLEdBQUN3QixDQUFDLENBQUM1QyxDQUFELENBQVQ7O0FBQWEsWUFBR29CLENBQUMsS0FBR25CLENBQUMsR0FBQy9FLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVXpJLENBQVYsQ0FBTCxDQUFELEVBQW9CbkIsQ0FBdkIsRUFBeUI7QUFBQyxjQUFJMEIsQ0FBQyxHQUFDMUIsQ0FBQyxDQUFDZ08scUJBQUYsRUFBTjtBQUFnQyxjQUFHdE0sQ0FBQyxDQUFDOGMsS0FBRixJQUFTOWMsQ0FBQyxDQUFDdW5CLE1BQWQsRUFBcUIsT0FBTSxDQUFDdmIsRUFBRSxDQUFDak4sQ0FBRCxDQUFGLENBQU1ULENBQU4sRUFBU2lPLEdBQVQsR0FBYW5SLENBQWQsRUFBZ0JxRSxDQUFoQixDQUFOO0FBQXlCOztBQUFBLGVBQU8sSUFBUDtBQUFZLE9BQTVLLEVBQStLRyxNQUEvSyxDQUF1TCxVQUFTdkIsQ0FBVCxFQUFXO0FBQUMsZUFBT0EsQ0FBUDtBQUFTLE9BQTVNLEVBQStNbXBCLElBQS9NLENBQXFOLFVBQVNucEIsQ0FBVCxFQUFXQyxDQUFYLEVBQWE7QUFBQyxlQUFPRCxDQUFDLENBQUMsQ0FBRCxDQUFELEdBQUtDLENBQUMsQ0FBQyxDQUFELENBQWI7QUFBaUIsT0FBcFAsRUFBdVA0QixPQUF2UCxDQUFnUSxVQUFTNUIsQ0FBVCxFQUFXO0FBQUNELFFBQUFBLENBQUMsQ0FBQzBvQixRQUFGLENBQVdqbkIsSUFBWCxDQUFnQnhCLENBQUMsQ0FBQyxDQUFELENBQWpCLEdBQXNCRCxDQUFDLENBQUMyb0IsUUFBRixDQUFXbG5CLElBQVgsQ0FBZ0J4QixDQUFDLENBQUMsQ0FBRCxDQUFqQixDQUF0QjtBQUE0QyxPQUF4VCxDQUE3RTtBQUF3WSxLQUF0akIsRUFBdWpCQSxDQUFDLENBQUNrTCxPQUFGLEdBQVUsWUFBVTtBQUFDNUYsTUFBQUEsQ0FBQyxDQUFDRyxVQUFGLENBQWEsS0FBS29GLFFBQWxCLEVBQTJCc2MsRUFBM0IsR0FBK0JuZSxDQUFDLENBQUNDLEdBQUYsQ0FBTSxLQUFLdWYsY0FBWCxFQUEwQnBCLEVBQTFCLENBQS9CLEVBQTZELEtBQUt2YyxRQUFMLEdBQWMsSUFBM0UsRUFBZ0YsS0FBSzJkLGNBQUwsR0FBb0IsSUFBcEcsRUFBeUcsS0FBS3RXLE9BQUwsR0FBYSxJQUF0SCxFQUEySCxLQUFLbUUsU0FBTCxHQUFlLElBQTFJLEVBQStJLEtBQUtvUyxRQUFMLEdBQWMsSUFBN0osRUFBa0ssS0FBS0MsUUFBTCxHQUFjLElBQWhMLEVBQXFMLEtBQUtDLGFBQUwsR0FBbUIsSUFBeE0sRUFBNk0sS0FBS0MsYUFBTCxHQUFtQixJQUFoTztBQUFxTyxLQUFqekIsRUFBa3pCNW9CLENBQUMsQ0FBQ21TLFVBQUYsR0FBYSxVQUFTcFMsQ0FBVCxFQUFXO0FBQUMsVUFBRyxZQUFVLE9BQU0sQ0FBQ0EsQ0FBQyxHQUFDMkIsQ0FBQyxDQUFDLEVBQUQsRUFBSTJsQixFQUFKLEVBQU8sRUFBUCxFQUFVLG9CQUFpQnRuQixDQUFqQixLQUFvQkEsQ0FBcEIsR0FBc0JBLENBQXRCLEdBQXdCLEVBQWxDLENBQUosRUFBMkNnSixNQUE5RCxFQUFxRTtBQUFDLFlBQUkvSSxDQUFDLEdBQUNELENBQUMsQ0FBQ2dKLE1BQUYsQ0FBUzNELEVBQWY7QUFBa0JwRixRQUFBQSxDQUFDLEtBQUdBLENBQUMsR0FBQ29DLENBQUMsQ0FBQzhrQixFQUFELENBQUgsRUFBUW5uQixDQUFDLENBQUNnSixNQUFGLENBQVMzRCxFQUFULEdBQVlwRixDQUF2QixDQUFELEVBQTJCRCxDQUFDLENBQUNnSixNQUFGLEdBQVMsTUFBSS9JLENBQXhDO0FBQTBDOztBQUFBLGFBQU82RCxDQUFDLENBQUNxakIsRUFBRCxFQUFJbm5CLENBQUosRUFBTXduQixFQUFOLENBQUQsRUFBV3huQixDQUFsQjtBQUFvQixLQUFqK0IsRUFBaytCQyxDQUFDLENBQUMrb0IsYUFBRixHQUFnQixZQUFVO0FBQUMsYUFBTyxLQUFLUCxjQUFMLEtBQXNCN3BCLE1BQXRCLEdBQTZCLEtBQUs2cEIsY0FBTCxDQUFvQlcsV0FBakQsR0FBNkQsS0FBS1gsY0FBTCxDQUFvQnRhLFNBQXhGO0FBQWtHLEtBQS9sQyxFQUFnbUNsTyxDQUFDLENBQUNncEIsZ0JBQUYsR0FBbUIsWUFBVTtBQUFDLGFBQU8sS0FBS1IsY0FBTCxDQUFvQnRLLFlBQXBCLElBQWtDN2IsSUFBSSxDQUFDK21CLEdBQUwsQ0FBU3J1QixRQUFRLENBQUNpSyxJQUFULENBQWNrWixZQUF2QixFQUFvQ25qQixRQUFRLENBQUM0TyxlQUFULENBQXlCdVUsWUFBN0QsQ0FBekM7QUFBb0gsS0FBbHZDLEVBQW12Q2xlLENBQUMsQ0FBQ3FwQixnQkFBRixHQUFtQixZQUFVO0FBQUMsYUFBTyxLQUFLYixjQUFMLEtBQXNCN3BCLE1BQXRCLEdBQTZCQSxNQUFNLENBQUMycUIsV0FBcEMsR0FBZ0QsS0FBS2QsY0FBTCxDQUFvQnhhLHFCQUFwQixHQUE0Q2liLE1BQW5HO0FBQTBHLEtBQTMzQyxFQUE0M0NqcEIsQ0FBQyxDQUFDNm9CLFFBQUYsR0FBVyxZQUFVO0FBQUMsVUFBSTlvQixDQUFDLEdBQUMsS0FBS2dwQixhQUFMLEtBQXFCLEtBQUs3VyxPQUFMLENBQWFsVyxNQUF4QztBQUFBLFVBQStDZ0UsQ0FBQyxHQUFDLEtBQUtncEIsZ0JBQUwsRUFBakQ7QUFBQSxVQUF5RXZvQixDQUFDLEdBQUMsS0FBS3lSLE9BQUwsQ0FBYWxXLE1BQWIsR0FBb0JnRSxDQUFwQixHQUFzQixLQUFLcXBCLGdCQUFMLEVBQWpHOztBQUF5SCxVQUFHLEtBQUtULGFBQUwsS0FBcUI1b0IsQ0FBckIsSUFBd0IsS0FBSzhvQixPQUFMLEVBQXhCLEVBQXVDL29CLENBQUMsSUFBRVUsQ0FBN0MsRUFBK0M7QUFBQyxZQUFJM0QsQ0FBQyxHQUFDLEtBQUs0ckIsUUFBTCxDQUFjLEtBQUtBLFFBQUwsQ0FBY2hvQixNQUFkLEdBQXFCLENBQW5DLENBQU47QUFBNEMsYUFBS2lvQixhQUFMLEtBQXFCN3JCLENBQXJCLElBQXdCLEtBQUt5c0IsU0FBTCxDQUFlenNCLENBQWYsQ0FBeEI7QUFBMEMsT0FBdEksTUFBMEk7QUFBQyxZQUFHLEtBQUs2ckIsYUFBTCxJQUFvQjVvQixDQUFDLEdBQUMsS0FBSzBvQixRQUFMLENBQWMsQ0FBZCxDQUF0QixJQUF3QyxLQUFLQSxRQUFMLENBQWMsQ0FBZCxJQUFpQixDQUE1RCxFQUE4RCxPQUFPLEtBQUtFLGFBQUwsR0FBbUIsSUFBbkIsRUFBd0IsS0FBSyxLQUFLYSxNQUFMLEVBQXBDOztBQUFrRCxhQUFJLElBQUlyb0IsQ0FBQyxHQUFDLEtBQUtzbkIsUUFBTCxDQUFjL25CLE1BQXhCLEVBQStCUyxDQUFDLEVBQWhDLEdBQW9DO0FBQUMsZUFBS3duQixhQUFMLEtBQXFCLEtBQUtELFFBQUwsQ0FBY3ZuQixDQUFkLENBQXJCLElBQXVDcEIsQ0FBQyxJQUFFLEtBQUswb0IsUUFBTCxDQUFjdG5CLENBQWQsQ0FBMUMsS0FBNkQsZUFBYSxPQUFPLEtBQUtzbkIsUUFBTCxDQUFjdG5CLENBQUMsR0FBQyxDQUFoQixDQUFwQixJQUF3Q3BCLENBQUMsR0FBQyxLQUFLMG9CLFFBQUwsQ0FBY3RuQixDQUFDLEdBQUMsQ0FBaEIsQ0FBdkcsS0FBNEgsS0FBS29vQixTQUFMLENBQWUsS0FBS2IsUUFBTCxDQUFjdm5CLENBQWQsQ0FBZixDQUE1SDtBQUE2SjtBQUFDO0FBQUMsS0FBMThELEVBQTI4RG5CLENBQUMsQ0FBQ3VwQixTQUFGLEdBQVksVUFBU3hwQixDQUFULEVBQVc7QUFBQyxXQUFLNG9CLGFBQUwsR0FBbUI1b0IsQ0FBbkIsRUFBcUIsS0FBS3lwQixNQUFMLEVBQXJCOztBQUFtQyxVQUFJeHBCLENBQUMsR0FBQyxLQUFLcVcsU0FBTCxDQUFlblQsS0FBZixDQUFxQixHQUFyQixFQUEwQjBpQixHQUExQixDQUErQixVQUFTNWxCLENBQVQsRUFBVztBQUFDLGVBQU9BLENBQUMsR0FBQyxnQkFBRixHQUFtQkQsQ0FBbkIsR0FBcUIsS0FBckIsR0FBMkJDLENBQTNCLEdBQTZCLFNBQTdCLEdBQXVDRCxDQUF2QyxHQUF5QyxJQUFoRDtBQUFxRCxPQUFoRyxDQUFOO0FBQUEsVUFBeUdVLENBQUMsR0FBQ3hGLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVTVKLENBQUMsQ0FBQ3lwQixJQUFGLENBQU8sR0FBUCxDQUFWLENBQTNHOztBQUFrSWhwQixNQUFBQSxDQUFDLENBQUMwSyxTQUFGLENBQVlFLFFBQVosQ0FBcUJzYyxFQUFFLENBQUNDLGFBQXhCLEtBQXdDM3NCLENBQUMsQ0FBQzJPLE9BQUYsQ0FBVWllLEVBQUUsQ0FBQ08sZUFBYixFQUE2Qm50QixDQUFDLENBQUM4SyxPQUFGLENBQVV0RixDQUFWLEVBQVlvbkIsRUFBRSxDQUFDTSxRQUFmLENBQTdCLEVBQXVEaGQsU0FBdkQsQ0FBaUVnQyxHQUFqRSxDQUFxRXdhLEVBQUUsQ0FBQzdXLE1BQXhFLEdBQWdGclEsQ0FBQyxDQUFDMEssU0FBRixDQUFZZ0MsR0FBWixDQUFnQndhLEVBQUUsQ0FBQzdXLE1BQW5CLENBQXhILEtBQXFKclEsQ0FBQyxDQUFDMEssU0FBRixDQUFZZ0MsR0FBWixDQUFnQndhLEVBQUUsQ0FBQzdXLE1BQW5CLEdBQTJCN1YsQ0FBQyxDQUFDNk8sT0FBRixDQUFVckosQ0FBVixFQUFZb25CLEVBQUUsQ0FBQ0UsY0FBZixFQUErQm5tQixPQUEvQixDQUF3QyxVQUFTN0IsQ0FBVCxFQUFXO0FBQUM5RSxRQUFBQSxDQUFDLENBQUNnUCxJQUFGLENBQU9sSyxDQUFQLEVBQVM4bkIsRUFBRSxDQUFDRyxTQUFILEdBQWEsSUFBYixHQUFrQkgsRUFBRSxDQUFDSyxVQUE5QixFQUEwQ3RtQixPQUExQyxDQUFtRCxVQUFTN0IsQ0FBVCxFQUFXO0FBQUMsaUJBQU9BLENBQUMsQ0FBQ29MLFNBQUYsQ0FBWWdDLEdBQVosQ0FBZ0J3YSxFQUFFLENBQUM3VyxNQUFuQixDQUFQO0FBQWtDLFNBQWpHLEdBQW9HN1YsQ0FBQyxDQUFDZ1AsSUFBRixDQUFPbEssQ0FBUCxFQUFTOG5CLEVBQUUsQ0FBQ0ksU0FBWixFQUF1QnJtQixPQUF2QixDQUFnQyxVQUFTN0IsQ0FBVCxFQUFXO0FBQUM5RSxVQUFBQSxDQUFDLENBQUM0TyxRQUFGLENBQVc5SixDQUFYLEVBQWE4bkIsRUFBRSxDQUFDRyxTQUFoQixFQUEyQnBtQixPQUEzQixDQUFvQyxVQUFTN0IsQ0FBVCxFQUFXO0FBQUMsbUJBQU9BLENBQUMsQ0FBQ29MLFNBQUYsQ0FBWWdDLEdBQVosQ0FBZ0J3YSxFQUFFLENBQUM3VyxNQUFuQixDQUFQO0FBQWtDLFdBQWxGO0FBQXFGLFNBQWpJLENBQXBHO0FBQXdPLE9BQTVSLENBQWhMLEdBQWdkOUgsQ0FBQyxDQUFDTSxPQUFGLENBQVUsS0FBS2tmLGNBQWYsRUFBOEJoQixFQUFFLENBQUNDLFFBQWpDLEVBQTBDO0FBQUN2VCxRQUFBQSxhQUFhLEVBQUNuVTtBQUFmLE9BQTFDLENBQWhkO0FBQTZnQixLQUFycEYsRUFBc3BGQyxDQUFDLENBQUN3cEIsTUFBRixHQUFTLFlBQVU7QUFBQ2xsQixNQUFBQSxDQUFDLENBQUNySixDQUFDLENBQUN5TyxJQUFGLENBQU8sS0FBSzJNLFNBQVosQ0FBRCxDQUFELENBQTBCL1UsTUFBMUIsQ0FBa0MsVUFBU3ZCLENBQVQsRUFBVztBQUFDLGVBQU9BLENBQUMsQ0FBQ29MLFNBQUYsQ0FBWUUsUUFBWixDQUFxQnNjLEVBQUUsQ0FBQzdXLE1BQXhCLENBQVA7QUFBdUMsT0FBckYsRUFBd0ZsUCxPQUF4RixDQUFpRyxVQUFTN0IsQ0FBVCxFQUFXO0FBQUMsZUFBT0EsQ0FBQyxDQUFDb0wsU0FBRixDQUFZQyxNQUFaLENBQW1CdWMsRUFBRSxDQUFDN1csTUFBdEIsQ0FBUDtBQUFxQyxPQUFsSjtBQUFxSixLQUEvekYsRUFBZzBGL1EsQ0FBQyxDQUFDeUwsZUFBRixHQUFrQixVQUFTeEwsQ0FBVCxFQUFXO0FBQUMsYUFBTyxLQUFLeEUsSUFBTCxDQUFXLFlBQVU7QUFBQyxZQUFJaUYsQ0FBQyxHQUFDNkUsQ0FBQyxDQUFDRSxPQUFGLENBQVUsSUFBVixFQUFlMmhCLEVBQWYsQ0FBTjs7QUFBeUIsWUFBRzFtQixDQUFDLEtBQUdBLENBQUMsR0FBQyxJQUFJVixDQUFKLENBQU0sSUFBTixFQUFXLG9CQUFpQkMsQ0FBakIsS0FBb0JBLENBQS9CLENBQUwsQ0FBRCxFQUF5QyxZQUFVLE9BQU9BLENBQTdELEVBQStEO0FBQUMsY0FBRyxlQUFhLE9BQU9TLENBQUMsQ0FBQ1QsQ0FBRCxDQUF4QixFQUE0QixNQUFNLElBQUl5VSxTQUFKLENBQWMsc0JBQW9CelUsQ0FBcEIsR0FBc0IsR0FBcEMsQ0FBTjtBQUErQ1MsVUFBQUEsQ0FBQyxDQUFDVCxDQUFELENBQUQ7QUFBTztBQUFDLE9BQWxNLENBQVA7QUFBNE0sS0FBMWlHLEVBQTJpR0QsQ0FBQyxDQUFDMkwsV0FBRixHQUFjLFVBQVMzTCxDQUFULEVBQVc7QUFBQyxhQUFPdUYsQ0FBQyxDQUFDRSxPQUFGLENBQVV6RixDQUFWLEVBQVlvbkIsRUFBWixDQUFQO0FBQXVCLEtBQTVsRyxFQUE2bEcxbUIsQ0FBQyxDQUFDVixDQUFELEVBQUcsSUFBSCxFQUFRLENBQUM7QUFBQ2lCLE1BQUFBLEdBQUcsRUFBQyxTQUFMO0FBQWVxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU0sT0FBTjtBQUFjO0FBQTVDLEtBQUQsRUFBK0M7QUFBQ3JFLE1BQUFBLEdBQUcsRUFBQyxTQUFMO0FBQWVxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU9naUIsRUFBUDtBQUFVO0FBQXhDLEtBQS9DLENBQVIsQ0FBOWxHLEVBQWlzR3RuQixDQUF4c0c7QUFBMHNHLEdBQXpySCxFQUFqZ0I7O0FBQTZySWlKLEVBQUFBLENBQUMsQ0FBQ0csRUFBRixDQUFLeEssTUFBTCxFQUFZNm9CLEVBQUUsQ0FBQ3BYLGFBQWYsRUFBOEIsWUFBVTtBQUFDOUwsSUFBQUEsQ0FBQyxDQUFDckosQ0FBQyxDQUFDeU8sSUFBRixDQUFPbWUsRUFBRSxDQUFDQyxRQUFWLENBQUQsQ0FBRCxDQUF1QmxtQixPQUF2QixDQUFnQyxVQUFTN0IsQ0FBVCxFQUFXO0FBQUMsYUFBTyxJQUFJd29CLEVBQUosQ0FBT3hvQixDQUFQLEVBQVMyTixFQUFFLENBQUNHLGlCQUFILENBQXFCOU4sQ0FBckIsQ0FBVCxDQUFQO0FBQXlDLEtBQXJGO0FBQXdGLEdBQWpJO0FBQW9JLE1BQUkycEIsRUFBRSxHQUFDM2tCLENBQUMsRUFBUjs7QUFBVyxNQUFHMmtCLEVBQUgsRUFBTTtBQUFDLFFBQUlDLEVBQUUsR0FBQ0QsRUFBRSxDQUFDN2QsRUFBSCxDQUFNcWIsRUFBTixDQUFQO0FBQWlCd0MsSUFBQUEsRUFBRSxDQUFDN2QsRUFBSCxDQUFNcWIsRUFBTixJQUFVcUIsRUFBRSxDQUFDL2MsZUFBYixFQUE2QmtlLEVBQUUsQ0FBQzdkLEVBQUgsQ0FBTXFiLEVBQU4sRUFBVW5iLFdBQVYsR0FBc0J3YyxFQUFuRCxFQUFzRG1CLEVBQUUsQ0FBQzdkLEVBQUgsQ0FBTXFiLEVBQU4sRUFBVWxiLFVBQVYsR0FBcUIsWUFBVTtBQUFDLGFBQU8wZCxFQUFFLENBQUM3ZCxFQUFILENBQU1xYixFQUFOLElBQVV5QyxFQUFWLEVBQWFwQixFQUFFLENBQUMvYyxlQUF2QjtBQUF1QyxLQUE3SDtBQUE4SDs7QUFBQSxNQUFJb2UsRUFBRSxHQUFDLFFBQVA7QUFBQSxNQUFnQkMsRUFBRSxHQUFDLE1BQUlELEVBQXZCO0FBQUEsTUFBMEJFLEVBQUUsR0FBQztBQUFDdlUsSUFBQUEsSUFBSSxFQUFDLFNBQU9zVSxFQUFiO0FBQWdCclUsSUFBQUEsTUFBTSxFQUFDLFdBQVNxVSxFQUFoQztBQUFtQ3hVLElBQUFBLElBQUksRUFBQyxTQUFPd1UsRUFBL0M7QUFBa0R2VSxJQUFBQSxLQUFLLEVBQUMsVUFBUXVVLEVBQWhFO0FBQW1FcmYsSUFBQUEsY0FBYyxFQUFDLFVBQVFxZixFQUFSLEdBQVc7QUFBN0YsR0FBN0I7QUFBQSxNQUF1SUUsRUFBRSxHQUFDLGVBQTFJO0FBQUEsTUFBMEpDLEVBQUUsR0FBQyxRQUE3SjtBQUFBLE1BQXNLQyxFQUFFLEdBQUMsVUFBeks7QUFBQSxNQUFvTEMsRUFBRSxHQUFDLE1BQXZMO0FBQUEsTUFBOExDLEVBQUUsR0FBQyxNQUFqTTtBQUFBLE1BQXdNQyxFQUFFLEdBQUMsV0FBM007QUFBQSxNQUF1TkMsRUFBRSxHQUFDLG1CQUExTjtBQUFBLE1BQThPQyxFQUFFLEdBQUMsU0FBalA7QUFBQSxNQUEyUHJLLEVBQUUsR0FBQyx1QkFBOVA7QUFBQSxNQUFzUnNLLEVBQUUsR0FBQyxpRUFBelI7QUFBQSxNQUEyVkMsRUFBRSxHQUFDLGtCQUE5VjtBQUFBLE1BQWlYQyxFQUFFLEdBQUMsaUNBQXBYO0FBQUEsTUFBc1pDLEVBQUUsR0FBQyxZQUFVO0FBQUMsYUFBUzNxQixDQUFULENBQVdBLENBQVgsRUFBYTtBQUFDLFdBQUs4SyxRQUFMLEdBQWM5SyxDQUFkLEVBQWdCdUYsQ0FBQyxDQUFDQyxPQUFGLENBQVUsS0FBS3NGLFFBQWYsRUFBd0IrZSxFQUF4QixFQUEyQixJQUEzQixDQUFoQjtBQUFpRDs7QUFBQSxRQUFJNXBCLENBQUMsR0FBQ0QsQ0FBQyxDQUFDa0IsU0FBUjtBQUFrQixXQUFPakIsQ0FBQyxDQUFDMFcsSUFBRixHQUFPLFlBQVU7QUFBQyxVQUFJM1csQ0FBQyxHQUFDLElBQU47O0FBQVcsVUFBRyxFQUFFLEtBQUs4SyxRQUFMLENBQWNwRyxVQUFkLElBQTBCLEtBQUtvRyxRQUFMLENBQWNwRyxVQUFkLENBQXlCakIsUUFBekIsS0FBb0N1RyxJQUFJLENBQUNDLFlBQW5FLElBQWlGLEtBQUthLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkUsUUFBeEIsQ0FBaUMyZSxFQUFqQyxDQUFqRixJQUF1SCxLQUFLbmYsUUFBTCxDQUFjTSxTQUFkLENBQXdCRSxRQUF4QixDQUFpQzRlLEVBQWpDLENBQXpILENBQUgsRUFBa0s7QUFBQyxZQUFJanFCLENBQUo7QUFBQSxZQUFNUyxDQUFDLEdBQUNtQyxDQUFDLENBQUMsS0FBS2lJLFFBQU4sQ0FBVDtBQUFBLFlBQXlCL04sQ0FBQyxHQUFDN0IsQ0FBQyxDQUFDOEssT0FBRixDQUFVLEtBQUs4RSxRQUFmLEVBQXdCd2YsRUFBeEIsQ0FBM0I7O0FBQXVELFlBQUd2dEIsQ0FBSCxFQUFLO0FBQUMsY0FBSXFFLENBQUMsR0FBQyxTQUFPckUsQ0FBQyxDQUFDa2lCLFFBQVQsSUFBbUIsU0FBT2xpQixDQUFDLENBQUNraUIsUUFBNUIsR0FBcUNpQixFQUFyQyxHQUF3Q3FLLEVBQTlDO0FBQWlEdHFCLFVBQUFBLENBQUMsR0FBQyxDQUFDQSxDQUFDLEdBQUNzRSxDQUFDLENBQUNySixDQUFDLENBQUN5TyxJQUFGLENBQU92SSxDQUFQLEVBQVNyRSxDQUFULENBQUQsQ0FBSixFQUFtQmtELENBQUMsQ0FBQ1UsTUFBRixHQUFTLENBQTVCLENBQUY7QUFBaUM7O0FBQUEsWUFBSWdCLENBQUMsR0FBQyxJQUFOOztBQUFXLFlBQUcxQixDQUFDLEtBQUcwQixDQUFDLEdBQUNzSCxDQUFDLENBQUNNLE9BQUYsQ0FBVXRKLENBQVYsRUFBWThwQixFQUFFLENBQUN2VSxJQUFmLEVBQW9CO0FBQUNyQixVQUFBQSxhQUFhLEVBQUMsS0FBS3JKO0FBQXBCLFNBQXBCLENBQUwsQ0FBRCxFQUEwRCxFQUFFN0IsQ0FBQyxDQUFDTSxPQUFGLENBQVUsS0FBS3VCLFFBQWYsRUFBd0JpZixFQUFFLENBQUN6VSxJQUEzQixFQUFnQztBQUFDbkIsVUFBQUEsYUFBYSxFQUFDbFU7QUFBZixTQUFoQyxFQUFtRHlHLGdCQUFuRCxJQUFxRSxTQUFPL0UsQ0FBUCxJQUFVQSxDQUFDLENBQUMrRSxnQkFBbkYsQ0FBN0QsRUFBa0s7QUFBQyxlQUFLOGlCLFNBQUwsQ0FBZSxLQUFLMWUsUUFBcEIsRUFBNkIvTixDQUE3Qjs7QUFBZ0MsY0FBSWtGLENBQUMsR0FBQyxTQUFGQSxDQUFFLEdBQVU7QUFBQ2dILFlBQUFBLENBQUMsQ0FBQ00sT0FBRixDQUFVdEosQ0FBVixFQUFZOHBCLEVBQUUsQ0FBQ3RVLE1BQWYsRUFBc0I7QUFBQ3RCLGNBQUFBLGFBQWEsRUFBQ25VLENBQUMsQ0FBQzhLO0FBQWpCLGFBQXRCLEdBQWtEN0IsQ0FBQyxDQUFDTSxPQUFGLENBQVV2SixDQUFDLENBQUM4SyxRQUFaLEVBQXFCaWYsRUFBRSxDQUFDeFUsS0FBeEIsRUFBOEI7QUFBQ3BCLGNBQUFBLGFBQWEsRUFBQ2xVO0FBQWYsYUFBOUIsQ0FBbEQ7QUFBbUcsV0FBcEg7O0FBQXFIUyxVQUFBQSxDQUFDLEdBQUMsS0FBSzhvQixTQUFMLENBQWU5b0IsQ0FBZixFQUFpQkEsQ0FBQyxDQUFDZ0UsVUFBbkIsRUFBOEJ6QyxDQUE5QixDQUFELEdBQWtDQSxDQUFDLEVBQXBDO0FBQXVDO0FBQUM7QUFBQyxLQUEzckIsRUFBNHJCaEMsQ0FBQyxDQUFDa0wsT0FBRixHQUFVLFlBQVU7QUFBQzVGLE1BQUFBLENBQUMsQ0FBQ0csVUFBRixDQUFhLEtBQUtvRixRQUFsQixFQUEyQitlLEVBQTNCLEdBQStCLEtBQUsvZSxRQUFMLEdBQWMsSUFBN0M7QUFBa0QsS0FBbndCLEVBQW93QjdLLENBQUMsQ0FBQ3VwQixTQUFGLEdBQVksVUFBU3hwQixDQUFULEVBQVdDLENBQVgsRUFBYVMsQ0FBYixFQUFlO0FBQUMsVUFBSTNELENBQUMsR0FBQyxJQUFOO0FBQUEsVUFBV3FFLENBQUMsR0FBQyxDQUFDLENBQUNuQixDQUFELElBQUksU0FBT0EsQ0FBQyxDQUFDZ2YsUUFBVCxJQUFtQixTQUFPaGYsQ0FBQyxDQUFDZ2YsUUFBaEMsR0FBeUMvakIsQ0FBQyxDQUFDNE8sUUFBRixDQUFXN0osQ0FBWCxFQUFhc3FCLEVBQWIsQ0FBekMsR0FBMERydkIsQ0FBQyxDQUFDeU8sSUFBRixDQUFPdVcsRUFBUCxFQUFVamdCLENBQVYsQ0FBM0QsRUFBeUUsQ0FBekUsQ0FBYjtBQUFBLFVBQXlGMEIsQ0FBQyxHQUFDakIsQ0FBQyxJQUFFVSxDQUFILElBQU1BLENBQUMsQ0FBQ2dLLFNBQUYsQ0FBWUUsUUFBWixDQUFxQjZlLEVBQXJCLENBQWpHO0FBQUEsVUFBMEhsb0IsQ0FBQyxHQUFDLFNBQUZBLENBQUUsR0FBVTtBQUFDLGVBQU9sRixDQUFDLENBQUM2dEIsbUJBQUYsQ0FBc0I1cUIsQ0FBdEIsRUFBd0JvQixDQUF4QixFQUEwQlYsQ0FBMUIsQ0FBUDtBQUFvQyxPQUEzSzs7QUFBNEssVUFBR1UsQ0FBQyxJQUFFTyxDQUFOLEVBQVE7QUFBQyxZQUFJTyxDQUFDLEdBQUNZLENBQUMsQ0FBQzFCLENBQUQsQ0FBUDtBQUFXQSxRQUFBQSxDQUFDLENBQUNnSyxTQUFGLENBQVlDLE1BQVosQ0FBbUIrZSxFQUFuQixHQUF1Qm5oQixDQUFDLENBQUNJLEdBQUYsQ0FBTWpJLENBQU4sRUFBUWdCLENBQVIsRUFBVUgsQ0FBVixDQUF2QixFQUFvQ3lCLENBQUMsQ0FBQ3RDLENBQUQsRUFBR2MsQ0FBSCxDQUFyQztBQUEyQyxPQUEvRCxNQUFvRUQsQ0FBQztBQUFHLEtBQXBoQyxFQUFxaENoQyxDQUFDLENBQUMycUIsbUJBQUYsR0FBc0IsVUFBUzVxQixDQUFULEVBQVdDLENBQVgsRUFBYVMsQ0FBYixFQUFlO0FBQUMsVUFBR1QsQ0FBSCxFQUFLO0FBQUNBLFFBQUFBLENBQUMsQ0FBQ21MLFNBQUYsQ0FBWUMsTUFBWixDQUFtQjRlLEVBQW5CO0FBQXVCLFlBQUlsdEIsQ0FBQyxHQUFDN0IsQ0FBQyxDQUFDMk8sT0FBRixDQUFVNmdCLEVBQVYsRUFBYXpxQixDQUFDLENBQUN5RSxVQUFmLENBQU47QUFBaUMzSCxRQUFBQSxDQUFDLElBQUVBLENBQUMsQ0FBQ3FPLFNBQUYsQ0FBWUMsTUFBWixDQUFtQjRlLEVBQW5CLENBQUgsRUFBMEIsVUFBUWhxQixDQUFDLENBQUN5QyxZQUFGLENBQWUsTUFBZixDQUFSLElBQWdDekMsQ0FBQyxDQUFDa04sWUFBRixDQUFlLGVBQWYsRUFBK0IsQ0FBQyxDQUFoQyxDQUExRDtBQUE2Rjs7QUFBQSxPQUFDbk4sQ0FBQyxDQUFDb0wsU0FBRixDQUFZZ0MsR0FBWixDQUFnQjZjLEVBQWhCLEdBQW9CLFVBQVFqcUIsQ0FBQyxDQUFDMEMsWUFBRixDQUFlLE1BQWYsQ0FBUixJQUFnQzFDLENBQUMsQ0FBQ21OLFlBQUYsQ0FBZSxlQUFmLEVBQStCLENBQUMsQ0FBaEMsQ0FBcEQsRUFBdUZySSxDQUFDLENBQUM5RSxDQUFELENBQXhGLEVBQTRGQSxDQUFDLENBQUNvTCxTQUFGLENBQVlFLFFBQVosQ0FBcUI2ZSxFQUFyQixLQUEwQm5xQixDQUFDLENBQUNvTCxTQUFGLENBQVlnQyxHQUFaLENBQWdCZ2QsRUFBaEIsQ0FBdEgsRUFBMElwcUIsQ0FBQyxDQUFDMEUsVUFBRixJQUFjMUUsQ0FBQyxDQUFDMEUsVUFBRixDQUFhMEcsU0FBYixDQUF1QkUsUUFBdkIsQ0FBZ0MwZSxFQUFoQyxDQUF6SixNQUFnTTl1QixDQUFDLENBQUM4SyxPQUFGLENBQVVoRyxDQUFWLEVBQVlxcUIsRUFBWixLQUFpQjlsQixDQUFDLENBQUNySixDQUFDLENBQUN5TyxJQUFGLENBQU84Z0IsRUFBUCxDQUFELENBQUQsQ0FBYzVvQixPQUFkLENBQXVCLFVBQVM3QixDQUFULEVBQVc7QUFBQyxlQUFPQSxDQUFDLENBQUNvTCxTQUFGLENBQVlnQyxHQUFaLENBQWdCNmMsRUFBaEIsQ0FBUDtBQUEyQixPQUE5RCxDQUFqQixFQUFrRmpxQixDQUFDLENBQUNtTixZQUFGLENBQWUsZUFBZixFQUErQixDQUFDLENBQWhDLENBQWxSO0FBQXNUek0sTUFBQUEsQ0FBQyxJQUFFQSxDQUFDLEVBQUo7QUFBTyxLQUFuaEQsRUFBb2hEVixDQUFDLENBQUN5TCxlQUFGLEdBQWtCLFVBQVN4TCxDQUFULEVBQVc7QUFBQyxhQUFPLEtBQUt4RSxJQUFMLENBQVcsWUFBVTtBQUFDLFlBQUlpRixDQUFDLEdBQUM2RSxDQUFDLENBQUNFLE9BQUYsQ0FBVSxJQUFWLEVBQWVva0IsRUFBZixLQUFvQixJQUFJN3BCLENBQUosQ0FBTSxJQUFOLENBQTFCOztBQUFzQyxZQUFHLFlBQVUsT0FBT0MsQ0FBcEIsRUFBc0I7QUFBQyxjQUFHLGVBQWEsT0FBT1MsQ0FBQyxDQUFDVCxDQUFELENBQXhCLEVBQTRCLE1BQU0sSUFBSXlVLFNBQUosQ0FBYyxzQkFBb0J6VSxDQUFwQixHQUFzQixHQUFwQyxDQUFOO0FBQStDUyxVQUFBQSxDQUFDLENBQUNULENBQUQsQ0FBRDtBQUFPO0FBQUMsT0FBdEssQ0FBUDtBQUFnTCxLQUFsdUQsRUFBbXVERCxDQUFDLENBQUMyTCxXQUFGLEdBQWMsVUFBUzNMLENBQVQsRUFBVztBQUFDLGFBQU91RixDQUFDLENBQUNFLE9BQUYsQ0FBVXpGLENBQVYsRUFBWTZwQixFQUFaLENBQVA7QUFBdUIsS0FBcHhELEVBQXF4RG5wQixDQUFDLENBQUNWLENBQUQsRUFBRyxJQUFILEVBQVEsQ0FBQztBQUFDaUIsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTSxPQUFOO0FBQWM7QUFBNUMsS0FBRCxDQUFSLENBQXR4RCxFQUErMER0RixDQUF0MUQ7QUFBdzFELEdBQXA3RCxFQUF6Wjs7QUFBZzFFaUosRUFBQUEsQ0FBQyxDQUFDRyxFQUFGLENBQUtwTyxRQUFMLEVBQWMrdUIsRUFBRSxDQUFDdGYsY0FBakIsRUFBZ0MrZixFQUFoQyxFQUFvQyxVQUFTeHFCLENBQVQsRUFBVztBQUFDQSxJQUFBQSxDQUFDLENBQUN5RyxjQUFGLElBQW1CLENBQUNsQixDQUFDLENBQUNFLE9BQUYsQ0FBVSxJQUFWLEVBQWVva0IsRUFBZixLQUFvQixJQUFJYyxFQUFKLENBQU8sSUFBUCxDQUFyQixFQUFtQ2hVLElBQW5DLEVBQW5CO0FBQTZELEdBQTdHO0FBQWdILE1BQUlrVSxFQUFFLEdBQUM3bEIsQ0FBQyxFQUFSOztBQUFXLE1BQUc2bEIsRUFBSCxFQUFNO0FBQUMsUUFBSUMsRUFBRSxHQUFDRCxFQUFFLENBQUMvZSxFQUFILENBQU1pZixHQUFiO0FBQWlCRixJQUFBQSxFQUFFLENBQUMvZSxFQUFILENBQU1pZixHQUFOLEdBQVVKLEVBQUUsQ0FBQ2xmLGVBQWIsRUFBNkJvZixFQUFFLENBQUMvZSxFQUFILENBQU1pZixHQUFOLENBQVUvZSxXQUFWLEdBQXNCMmUsRUFBbkQsRUFBc0RFLEVBQUUsQ0FBQy9lLEVBQUgsQ0FBTWlmLEdBQU4sQ0FBVTllLFVBQVYsR0FBcUIsWUFBVTtBQUFDLGFBQU80ZSxFQUFFLENBQUMvZSxFQUFILENBQU1pZixHQUFOLEdBQVVELEVBQVYsRUFBYUgsRUFBRSxDQUFDbGYsZUFBdkI7QUFBdUMsS0FBN0g7QUFBOEg7O0FBQUEsTUFBSXVmLEVBQUUsR0FBQyxVQUFQO0FBQUEsTUFBa0JDLEVBQUUsR0FBQyxNQUFJRCxFQUF6QjtBQUFBLE1BQTRCRSxFQUFFLEdBQUM7QUFBQ3hQLElBQUFBLGFBQWEsRUFBQyxrQkFBZ0J1UCxFQUEvQjtBQUFrQ3pWLElBQUFBLElBQUksRUFBQyxTQUFPeVYsRUFBOUM7QUFBaUR4VixJQUFBQSxNQUFNLEVBQUMsV0FBU3dWLEVBQWpFO0FBQW9FM1YsSUFBQUEsSUFBSSxFQUFDLFNBQU8yVixFQUFoRjtBQUFtRjFWLElBQUFBLEtBQUssRUFBQyxVQUFRMFY7QUFBakcsR0FBL0I7QUFBQSxNQUFvSUUsRUFBRSxHQUFDLE1BQXZJO0FBQUEsTUFBOElDLEVBQUUsR0FBQyxNQUFqSjtBQUFBLE1BQXdKQyxFQUFFLEdBQUMsTUFBM0o7QUFBQSxNQUFrS0MsRUFBRSxHQUFDLFNBQXJLO0FBQUEsTUFBK0tDLEVBQUUsR0FBQztBQUFDbEssSUFBQUEsU0FBUyxFQUFDLFNBQVg7QUFBcUJtSyxJQUFBQSxRQUFRLEVBQUMsU0FBOUI7QUFBd0NsdUIsSUFBQUEsS0FBSyxFQUFDO0FBQTlDLEdBQWxMO0FBQUEsTUFBME9tdUIsRUFBRSxHQUFDO0FBQUNwSyxJQUFBQSxTQUFTLEVBQUMsQ0FBQyxDQUFaO0FBQWNtSyxJQUFBQSxRQUFRLEVBQUMsQ0FBQyxDQUF4QjtBQUEwQmx1QixJQUFBQSxLQUFLLEVBQUM7QUFBaEMsR0FBN087QUFBQSxNQUFrUm91QixFQUFFLEdBQUMsd0JBQXJSO0FBQUEsTUFBOFNDLEVBQUUsR0FBQyxZQUFVO0FBQUMsYUFBUzNyQixDQUFULENBQVdBLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsV0FBSzZLLFFBQUwsR0FBYzlLLENBQWQsRUFBZ0IsS0FBS21TLE9BQUwsR0FBYSxLQUFLQyxVQUFMLENBQWdCblMsQ0FBaEIsQ0FBN0IsRUFBZ0QsS0FBS2tqQixRQUFMLEdBQWMsSUFBOUQsRUFBbUUsS0FBS0ssYUFBTCxFQUFuRSxFQUF3RmplLENBQUMsQ0FBQ0MsT0FBRixDQUFVeEYsQ0FBVixFQUFZZ3JCLEVBQVosRUFBZSxJQUFmLENBQXhGO0FBQTZHOztBQUFBLFFBQUkvcUIsQ0FBQyxHQUFDRCxDQUFDLENBQUNrQixTQUFSO0FBQWtCLFdBQU9qQixDQUFDLENBQUMwVyxJQUFGLEdBQU8sWUFBVTtBQUFDLFVBQUkzVyxDQUFDLEdBQUMsSUFBTjs7QUFBVyxVQUFHLENBQUNpSixDQUFDLENBQUNNLE9BQUYsQ0FBVSxLQUFLdUIsUUFBZixFQUF3Qm9nQixFQUFFLENBQUM1VixJQUEzQixFQUFpQzVPLGdCQUFyQyxFQUFzRDtBQUFDLGFBQUt5TCxPQUFMLENBQWFrUCxTQUFiLElBQXdCLEtBQUt2VyxRQUFMLENBQWNNLFNBQWQsQ0FBd0JnQyxHQUF4QixDQUE0QitkLEVBQTVCLENBQXhCOztBQUF3RCxZQUFJbHJCLENBQUMsR0FBQyxTQUFGQSxDQUFFLEdBQVU7QUFBQ0QsVUFBQUEsQ0FBQyxDQUFDOEssUUFBRixDQUFXTSxTQUFYLENBQXFCQyxNQUFyQixDQUE0QmlnQixFQUE1QixHQUFnQ3RyQixDQUFDLENBQUM4SyxRQUFGLENBQVdNLFNBQVgsQ0FBcUJnQyxHQUFyQixDQUF5QmllLEVBQXpCLENBQWhDLEVBQTZEcGlCLENBQUMsQ0FBQ00sT0FBRixDQUFVdkosQ0FBQyxDQUFDOEssUUFBWixFQUFxQm9nQixFQUFFLENBQUMzVixLQUF4QixDQUE3RCxFQUE0RnZWLENBQUMsQ0FBQ21TLE9BQUYsQ0FBVXFaLFFBQVYsS0FBcUJ4ckIsQ0FBQyxDQUFDbWpCLFFBQUYsR0FBV3RmLFVBQVUsQ0FBRSxZQUFVO0FBQUM3RCxZQUFBQSxDQUFDLENBQUMwVyxJQUFGO0FBQVMsV0FBdEIsRUFBd0IxVyxDQUFDLENBQUNtUyxPQUFGLENBQVU3VSxLQUFsQyxDQUExQyxDQUE1RjtBQUFnTCxTQUFqTTs7QUFBa00sWUFBRyxLQUFLd04sUUFBTCxDQUFjTSxTQUFkLENBQXdCQyxNQUF4QixDQUErQitmLEVBQS9CLEdBQW1DdG1CLENBQUMsQ0FBQyxLQUFLZ0csUUFBTixDQUFwQyxFQUFvRCxLQUFLQSxRQUFMLENBQWNNLFNBQWQsQ0FBd0JnQyxHQUF4QixDQUE0QmtlLEVBQTVCLENBQXBELEVBQW9GLEtBQUtuWixPQUFMLENBQWFrUCxTQUFwRyxFQUE4RztBQUFDLGNBQUkzZ0IsQ0FBQyxHQUFDb0MsQ0FBQyxDQUFDLEtBQUtnSSxRQUFOLENBQVA7QUFBdUI3QixVQUFBQSxDQUFDLENBQUNJLEdBQUYsQ0FBTSxLQUFLeUIsUUFBWCxFQUFvQjFJLENBQXBCLEVBQXNCbkMsQ0FBdEIsR0FBeUJ5RCxDQUFDLENBQUMsS0FBS29ILFFBQU4sRUFBZXBLLENBQWYsQ0FBMUI7QUFBNEMsU0FBbEwsTUFBdUxULENBQUM7QUFBRztBQUFDLEtBQTFnQixFQUEyZ0JBLENBQUMsQ0FBQ3lXLElBQUYsR0FBTyxZQUFVO0FBQUMsVUFBSTFXLENBQUMsR0FBQyxJQUFOOztBQUFXLFVBQUcsS0FBSzhLLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkUsUUFBeEIsQ0FBaUMrZixFQUFqQyxLQUFzQyxDQUFDcGlCLENBQUMsQ0FBQ00sT0FBRixDQUFVLEtBQUt1QixRQUFmLEVBQXdCb2dCLEVBQUUsQ0FBQzFWLElBQTNCLEVBQWlDOU8sZ0JBQTNFLEVBQTRGO0FBQUMsWUFBSXpHLENBQUMsR0FBQyxTQUFGQSxDQUFFLEdBQVU7QUFBQ0QsVUFBQUEsQ0FBQyxDQUFDOEssUUFBRixDQUFXTSxTQUFYLENBQXFCZ0MsR0FBckIsQ0FBeUJnZSxFQUF6QixHQUE2Qm5pQixDQUFDLENBQUNNLE9BQUYsQ0FBVXZKLENBQUMsQ0FBQzhLLFFBQVosRUFBcUJvZ0IsRUFBRSxDQUFDelYsTUFBeEIsQ0FBN0I7QUFBNkQsU0FBOUU7O0FBQStFLFlBQUcsS0FBSzNLLFFBQUwsQ0FBY00sU0FBZCxDQUF3QkMsTUFBeEIsQ0FBK0JnZ0IsRUFBL0IsR0FBbUMsS0FBS2xaLE9BQUwsQ0FBYWtQLFNBQW5ELEVBQTZEO0FBQUMsY0FBSTNnQixDQUFDLEdBQUNvQyxDQUFDLENBQUMsS0FBS2dJLFFBQU4sQ0FBUDtBQUF1QjdCLFVBQUFBLENBQUMsQ0FBQ0ksR0FBRixDQUFNLEtBQUt5QixRQUFYLEVBQW9CMUksQ0FBcEIsRUFBc0JuQyxDQUF0QixHQUF5QnlELENBQUMsQ0FBQyxLQUFLb0gsUUFBTixFQUFlcEssQ0FBZixDQUExQjtBQUE0QyxTQUFqSSxNQUFzSVQsQ0FBQztBQUFHO0FBQUMsS0FBLzFCLEVBQWcyQkEsQ0FBQyxDQUFDa0wsT0FBRixHQUFVLFlBQVU7QUFBQzRJLE1BQUFBLFlBQVksQ0FBQyxLQUFLb1AsUUFBTixDQUFaLEVBQTRCLEtBQUtBLFFBQUwsR0FBYyxJQUExQyxFQUErQyxLQUFLclksUUFBTCxDQUFjTSxTQUFkLENBQXdCRSxRQUF4QixDQUFpQytmLEVBQWpDLEtBQXNDLEtBQUt2Z0IsUUFBTCxDQUFjTSxTQUFkLENBQXdCQyxNQUF4QixDQUErQmdnQixFQUEvQixDQUFyRixFQUF3SHBpQixDQUFDLENBQUNDLEdBQUYsQ0FBTSxLQUFLNEIsUUFBWCxFQUFvQm9nQixFQUFFLENBQUN4UCxhQUF2QixDQUF4SCxFQUE4Sm5XLENBQUMsQ0FBQ0csVUFBRixDQUFhLEtBQUtvRixRQUFsQixFQUEyQmtnQixFQUEzQixDQUE5SixFQUE2TCxLQUFLbGdCLFFBQUwsR0FBYyxJQUEzTSxFQUFnTixLQUFLcUgsT0FBTCxHQUFhLElBQTdOO0FBQWtPLEtBQXZsQyxFQUF3bENsUyxDQUFDLENBQUNtUyxVQUFGLEdBQWEsVUFBU3BTLENBQVQsRUFBVztBQUFDLGFBQU9BLENBQUMsR0FBQzJCLENBQUMsQ0FBQyxFQUFELEVBQUk4cEIsRUFBSixFQUFPLEVBQVAsRUFBVTlkLEVBQUUsQ0FBQ0csaUJBQUgsQ0FBcUIsS0FBS2hELFFBQTFCLENBQVYsRUFBOEMsRUFBOUMsRUFBaUQsb0JBQWlCOUssQ0FBakIsS0FBb0JBLENBQXBCLEdBQXNCQSxDQUF0QixHQUF3QixFQUF6RSxDQUFILEVBQWdGOEQsQ0FBQyxDQUFDLE9BQUQsRUFBUzlELENBQVQsRUFBVyxLQUFLZ2EsV0FBTCxDQUFpQkUsV0FBNUIsQ0FBakYsRUFBMEhsYSxDQUFqSTtBQUFtSSxLQUFwdkMsRUFBcXZDQyxDQUFDLENBQUN1akIsYUFBRixHQUFnQixZQUFVO0FBQUMsVUFBSXhqQixDQUFDLEdBQUMsSUFBTjtBQUFXaUosTUFBQUEsQ0FBQyxDQUFDRyxFQUFGLENBQUssS0FBSzBCLFFBQVYsRUFBbUJvZ0IsRUFBRSxDQUFDeFAsYUFBdEIsRUFBb0NnUSxFQUFwQyxFQUF3QyxZQUFVO0FBQUMsZUFBTzFyQixDQUFDLENBQUMwVyxJQUFGLEVBQVA7QUFBZ0IsT0FBbkU7QUFBc0UsS0FBajJDLEVBQWsyQzFXLENBQUMsQ0FBQ3lMLGVBQUYsR0FBa0IsVUFBU3hMLENBQVQsRUFBVztBQUFDLGFBQU8sS0FBS3hFLElBQUwsQ0FBVyxZQUFVO0FBQUMsWUFBSWlGLENBQUMsR0FBQzZFLENBQUMsQ0FBQ0UsT0FBRixDQUFVLElBQVYsRUFBZXVsQixFQUFmLENBQU47O0FBQXlCLFlBQUd0cUIsQ0FBQyxLQUFHQSxDQUFDLEdBQUMsSUFBSVYsQ0FBSixDQUFNLElBQU4sRUFBVyxvQkFBaUJDLENBQWpCLEtBQW9CQSxDQUEvQixDQUFMLENBQUQsRUFBeUMsWUFBVSxPQUFPQSxDQUE3RCxFQUErRDtBQUFDLGNBQUcsZUFBYSxPQUFPUyxDQUFDLENBQUNULENBQUQsQ0FBeEIsRUFBNEIsTUFBTSxJQUFJeVUsU0FBSixDQUFjLHNCQUFvQnpVLENBQXBCLEdBQXNCLEdBQXBDLENBQU47QUFBK0NTLFVBQUFBLENBQUMsQ0FBQ1QsQ0FBRCxDQUFELENBQUssSUFBTDtBQUFXO0FBQUMsT0FBdE0sQ0FBUDtBQUFnTixLQUFobEQsRUFBaWxERCxDQUFDLENBQUMyTCxXQUFGLEdBQWMsVUFBUzNMLENBQVQsRUFBVztBQUFDLGFBQU91RixDQUFDLENBQUNFLE9BQUYsQ0FBVXpGLENBQVYsRUFBWWdyQixFQUFaLENBQVA7QUFBdUIsS0FBbG9ELEVBQW1vRHRxQixDQUFDLENBQUNWLENBQUQsRUFBRyxJQUFILEVBQVEsQ0FBQztBQUFDaUIsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBTSxPQUFOO0FBQWM7QUFBNUMsS0FBRCxFQUErQztBQUFDckUsTUFBQUEsR0FBRyxFQUFDLGFBQUw7QUFBbUJxRSxNQUFBQSxHQUFHLEVBQUMsZUFBVTtBQUFDLGVBQU9pbUIsRUFBUDtBQUFVO0FBQTVDLEtBQS9DLEVBQTZGO0FBQUN0cUIsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZXFFLE1BQUFBLEdBQUcsRUFBQyxlQUFVO0FBQUMsZUFBT21tQixFQUFQO0FBQVU7QUFBeEMsS0FBN0YsQ0FBUixDQUFwb0QsRUFBcXhEenJCLENBQTV4RDtBQUE4eEQsR0FBeDdELEVBQWpUO0FBQUEsTUFBNHVFNHJCLEVBQUUsR0FBQzVtQixDQUFDLEVBQWh2RTs7QUFBbXZFLE1BQUc0bUIsRUFBSCxFQUFNO0FBQUMsUUFBSUMsRUFBRSxHQUFDRCxFQUFFLENBQUM5ZixFQUFILENBQU1nZ0IsS0FBYjtBQUFtQkYsSUFBQUEsRUFBRSxDQUFDOWYsRUFBSCxDQUFNZ2dCLEtBQU4sR0FBWUgsRUFBRSxDQUFDbGdCLGVBQWYsRUFBK0JtZ0IsRUFBRSxDQUFDOWYsRUFBSCxDQUFNZ2dCLEtBQU4sQ0FBWTlmLFdBQVosR0FBd0IyZixFQUF2RCxFQUEwREMsRUFBRSxDQUFDOWYsRUFBSCxDQUFNZ2dCLEtBQU4sQ0FBWTdmLFVBQVosR0FBdUIsWUFBVTtBQUFDLGFBQU8yZixFQUFFLENBQUM5ZixFQUFILENBQU1nZ0IsS0FBTixHQUFZRCxFQUFaLEVBQWVGLEVBQUUsQ0FBQ2xnQixlQUF6QjtBQUF5QyxLQUFySTtBQUFzSTs7QUFBQSxTQUFNO0FBQUNzZ0IsSUFBQUEsS0FBSyxFQUFDbGhCLEVBQVA7QUFBVW1oQixJQUFBQSxNQUFNLEVBQUNqZixFQUFqQjtBQUFvQmtmLElBQUFBLFFBQVEsRUFBQ3ZhLEVBQTdCO0FBQWdDd2EsSUFBQUEsUUFBUSxFQUFDL1YsRUFBekM7QUFBNENnVyxJQUFBQSxRQUFRLEVBQUNqVCxFQUFyRDtBQUF3RGtULElBQUFBLEtBQUssRUFBQ3pQLEVBQTlEO0FBQWlFMFAsSUFBQUEsT0FBTyxFQUFDekYsRUFBekU7QUFBNEUwRixJQUFBQSxTQUFTLEVBQUM5RCxFQUF0RjtBQUF5RitELElBQUFBLEdBQUcsRUFBQzVCLEVBQTdGO0FBQWdHNkIsSUFBQUEsS0FBSyxFQUFDYixFQUF0RztBQUF5R2MsSUFBQUEsT0FBTyxFQUFDeEo7QUFBakgsR0FBTjtBQUEySCxDQUFuMGhFLENBQUQ7Ozs7Ozs7OztBQ0pBOzs7Ozs7Ozs7O0lBV015SixLOzs7QUFDSixtQkFBYztBQUFBOztBQUVaLFNBQUtDLFVBQUw7QUFDQSxTQUFLQyxVQUFMO0FBQ0Q7Ozs7aUNBRVk7QUFBQTs7QUFBQSxvQkFDTWh1QixNQUROO0FBQUEsVUFDSGl1QixJQURHLFdBQ0hBLElBREc7QUFFWCxXQUFLQyxXQUFMLEdBQW1COXhCLFFBQVEsQ0FBQ2lDLGFBQVQsQ0FBdUIsdUJBQXZCLENBQW5CO0FBQ0EsV0FBSzh2QixXQUFMLEdBQW1CL3hCLFFBQVEsQ0FBQ2lDLGFBQVQsQ0FBdUIsdUJBQXZCLENBQW5CO0FBQ0EsV0FBSyt2QixjQUFMLEdBQXNCLEtBQUtGLFdBQUwsQ0FBaUI3ZSxxQkFBakIsRUFBdEI7QUFDQSxXQUFLZ2YsZ0JBQUwsR0FBd0IsQ0FBeEI7QUFDQSxXQUFLQyxNQUFMLEdBQWNMLElBQUksQ0FBQ3B1QixPQUFMLENBQWFhLE1BQWIsQ0FBb0IsR0FBcEIsQ0FBZDtBQUNBLFdBQUt1VSxPQUFMLEdBQWUsQ0FBQyxHQUFoQjtBQUNBLFdBQUtzWixPQUFMLEdBQWUsQ0FBQyxHQUFoQjtBQUNBLFdBQUtDLFVBQUwsR0FBa0IsS0FBbEI7O0FBRUEsVUFBTUMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsR0FBTTtBQUN6QjV3QixRQUFBQSxRQUFRLENBQUMySSxHQUFULENBQWEsS0FBSSxDQUFDMm5CLFdBQWxCLEVBQStCO0FBQzdCN3RCLFVBQUFBLENBQUMsRUFBRSxLQUFJLENBQUMyVSxPQURxQjtBQUU3QjlWLFVBQUFBLENBQUMsRUFBRSxLQUFJLENBQUNvdkI7QUFGcUIsU0FBL0I7QUFJQTF3QixRQUFBQSxRQUFRLENBQUMySSxHQUFULENBQWEsS0FBSSxDQUFDMG5CLFdBQWxCLEVBQStCO0FBQzdCNXRCLFVBQUFBLENBQUMsRUFBRSxLQUFJLENBQUMyVSxPQUFMLEdBQWUsS0FBSSxDQUFDbVosY0FBTCxDQUFvQnZPLEtBQXBCLEdBQTRCLENBRGpCO0FBRTdCMWdCLFVBQUFBLENBQUMsRUFBRSxLQUFJLENBQUNvdkIsT0FBTCxHQUFlLEtBQUksQ0FBQ0gsY0FBTCxDQUFvQjlELE1BQXBCLEdBQTZCO0FBRmxCLFNBQS9CO0FBSUFybEIsUUFBQUEsVUFBVSxDQUFDLFlBQU07QUFDZixVQUFBLEtBQUksQ0FBQ29wQixnQkFBTCxHQUF3QixHQUF4QjtBQUNELFNBRlMsRUFFUCxHQUZPLENBQVY7QUFHQSxRQUFBLEtBQUksQ0FBQ0csVUFBTCxHQUFrQixJQUFsQjtBQUNELE9BYkQ7O0FBY0FweUIsTUFBQUEsUUFBUSxDQUFDMkksZ0JBQVQsQ0FBMEIsV0FBMUIsRUFBdUMwcEIsWUFBdkM7QUFFQXJ5QixNQUFBQSxRQUFRLENBQUMySSxnQkFBVCxDQUEwQixXQUExQixFQUF1QyxVQUFBMUQsQ0FBQyxFQUFJO0FBQzFDLFFBQUEsS0FBSSxDQUFDNFQsT0FBTCxHQUFlNVQsQ0FBQyxDQUFDNFQsT0FBakI7QUFDQSxRQUFBLEtBQUksQ0FBQ3NaLE9BQUwsR0FBZWx0QixDQUFDLENBQUNrdEIsT0FBakI7QUFDRCxPQUhEOztBQUtBLFVBQU1HLE1BQU0sR0FBRyxTQUFUQSxNQUFTLEdBQU07QUFDbkI3d0IsUUFBQUEsUUFBUSxDQUFDMkksR0FBVCxDQUFhLEtBQUksQ0FBQzJuQixXQUFsQixFQUErQjtBQUM3Qjd0QixVQUFBQSxDQUFDLEVBQUUsS0FBSSxDQUFDMlUsT0FEcUI7QUFFN0I5VixVQUFBQSxDQUFDLEVBQUUsS0FBSSxDQUFDb3ZCO0FBRnFCLFNBQS9COztBQUlBLFlBQUksQ0FBQyxLQUFJLENBQUNJLE9BQVYsRUFBbUI7QUFDakI5d0IsVUFBQUEsUUFBUSxDQUFDNlcsRUFBVCxDQUFZLEtBQUksQ0FBQ3daLFdBQWpCLEVBQThCLEtBQUksQ0FBQ0csZ0JBQW5DLEVBQXFEO0FBQ25EL3RCLFlBQUFBLENBQUMsRUFBRSxLQUFJLENBQUMyVSxPQUFMLEdBQWUsS0FBSSxDQUFDbVosY0FBTCxDQUFvQnZPLEtBQXBCLEdBQTRCLENBREs7QUFFbkQxZ0IsWUFBQUEsQ0FBQyxFQUFFLEtBQUksQ0FBQ292QixPQUFMLEdBQWUsS0FBSSxDQUFDSCxjQUFMLENBQW9COUQsTUFBcEIsR0FBNkI7QUFGSSxXQUFyRDtBQUlEOztBQUNELFlBQUksS0FBSSxDQUFDa0UsVUFBVCxFQUFxQjtBQUNuQnB5QixVQUFBQSxRQUFRLENBQUM0SSxtQkFBVCxDQUE2QixXQUE3QixFQUEwQ3lwQixZQUExQztBQUNEOztBQUNERyxRQUFBQSxxQkFBcUIsQ0FBQ0YsTUFBRCxDQUFyQjtBQUNELE9BZkQ7O0FBZ0JBRSxNQUFBQSxxQkFBcUIsQ0FBQ0YsTUFBRCxDQUFyQjtBQUNEOzs7aUNBRVk7QUFBQTs7QUFDWCxVQUFNRyxnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQUF4dEIsQ0FBQyxFQUFJO0FBQzVCLFFBQUEsTUFBSSxDQUFDc3RCLE9BQUwsR0FBZSxJQUFmO0FBQ0EsWUFBTXZrQixNQUFNLEdBQUcvSSxDQUFDLENBQUNpZSxhQUFqQjtBQUNBLFlBQU13UCxHQUFHLEdBQUcxa0IsTUFBTSxDQUFDaUYscUJBQVAsRUFBWjtBQUNBLFFBQUEsTUFBSSxDQUFDMGYsb0JBQUwsR0FBNEI7QUFDMUJsUCxVQUFBQSxLQUFLLEVBQUUsTUFBSSxDQUFDdU8sY0FBTCxDQUFvQnZPLEtBREQ7QUFFMUJ5SyxVQUFBQSxNQUFNLEVBQUUsTUFBSSxDQUFDOEQsY0FBTCxDQUFvQjlEO0FBRkYsU0FBNUI7QUFLQXpzQixRQUFBQSxRQUFRLENBQUMySSxHQUFULENBQWEsTUFBSSxDQUFDMm5CLFdBQWxCLEVBQStCO0FBQUUvdUIsVUFBQUEsT0FBTyxFQUFFO0FBQVgsU0FBL0I7QUFFQXZCLFFBQUFBLFFBQVEsQ0FBQzZXLEVBQVQsQ0FBWSxNQUFJLENBQUN3WixXQUFqQixFQUE4QixHQUE5QixFQUFtQztBQUNqQzV0QixVQUFBQSxDQUFDLEVBQUV3dUIsR0FBRyxDQUFDdGYsSUFBSixHQUFXLEVBRG1CO0FBRWpDclEsVUFBQUEsQ0FBQyxFQUFFMnZCLEdBQUcsQ0FBQ3RmLElBQUosR0FBVyxFQUZtQjtBQUdqQ3FRLFVBQUFBLEtBQUssRUFBRSxFQUgwQjtBQUlqQ3lLLFVBQUFBLE1BQU0sRUFBQyxFQUowQjtBQUtqQ2xyQixVQUFBQSxPQUFPLEVBQUU7QUFMd0IsU0FBbkM7QUFPRCxPQWxCRDs7QUFvQkEsVUFBTTR2QixnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLEdBQU07QUFDN0IsUUFBQSxNQUFJLENBQUNMLE9BQUwsR0FBZSxLQUFmO0FBQ0E5d0IsUUFBQUEsUUFBUSxDQUFDMkksR0FBVCxDQUFhLE1BQUksQ0FBQzJuQixXQUFsQixFQUErQjtBQUFFL3VCLFVBQUFBLE9BQU8sRUFBRTtBQUFYLFNBQS9CO0FBRUF2QixRQUFBQSxRQUFRLENBQUM2VyxFQUFULENBQVksTUFBSSxDQUFDd1osV0FBakIsRUFBOEIsR0FBOUIsRUFBbUM7QUFDakNyTyxVQUFBQSxLQUFLLEVBQUUsTUFBSSxDQUFDa1Asb0JBQUwsQ0FBMEJsUCxLQURBO0FBRWpDeUssVUFBQUEsTUFBTSxFQUFFLE1BQUksQ0FBQ3lFLG9CQUFMLENBQTBCekUsTUFGRDtBQUdqQ2xyQixVQUFBQSxPQUFPLEVBQUUsR0FId0I7QUFJakM2dkIsVUFBQUEsV0FBVyxFQUFFLFNBSm9CO0FBS2pDQyxVQUFBQSxXQUFXLEVBQUUsQ0FMb0I7QUFNakNDLFVBQUFBLFlBQVksRUFBRSxLQU5tQjtBQU9qQ3B4QixVQUFBQSxlQUFlLEVBQUU7QUFQZ0IsU0FBbkM7QUFVRCxPQWREOztBQWdCQSxVQUFNcXhCLFNBQVMsR0FBR2h6QixRQUFRLENBQUNRLGdCQUFULENBQTBCLFdBQTFCLENBQWxCO0FBQ0F3eUIsTUFBQUEsU0FBUyxDQUFDbnNCLE9BQVYsQ0FBa0IsVUFBQXhELElBQUksRUFBSTtBQUN4QkEsUUFBQUEsSUFBSSxDQUFDc0YsZ0JBQUwsQ0FBc0IsWUFBdEIsRUFBb0M4cEIsZ0JBQXBDO0FBQ0FwdkIsUUFBQUEsSUFBSSxDQUFDc0YsZ0JBQUwsQ0FBc0IsWUFBdEIsRUFBb0NpcUIsZ0JBQXBDO0FBQ0QsT0FIRDtBQUtBLFVBQU1LLGlCQUFpQixHQUFHeHhCLFFBQVEsQ0FBQzZXLEVBQVQsQ0FBWSxLQUFLd1osV0FBakIsRUFBOEIsR0FBOUIsRUFBbUM7QUFDM0Rud0IsUUFBQUEsZUFBZSxFQUFFLFNBRDBDO0FBRTNEZ0IsUUFBQUEsSUFBSSxFQUFFLEtBQUt1dkIsTUFGZ0Q7QUFHM0RnQixRQUFBQSxNQUFNLEVBQUUsSUFIbUQ7QUFJM0R6UCxRQUFBQSxLQUFLLEVBQUUsRUFKb0Q7QUFLM0R5SyxRQUFBQSxNQUFNLEVBQUMsRUFMb0Q7QUFNM0RsckIsUUFBQUEsT0FBTyxFQUFFLEdBTmtEO0FBTzNENnZCLFFBQUFBLFdBQVcsRUFBRTtBQVA4QyxPQUFuQyxDQUExQjs7QUFVQSxVQUFNTSxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLEdBQU07QUFDOUIsUUFBQSxNQUFJLENBQUNsQixnQkFBTCxHQUF3QixDQUF4QjtBQUNBeHdCLFFBQUFBLFFBQVEsQ0FBQzJJLEdBQVQsQ0FBYSxNQUFJLENBQUMybkIsV0FBbEIsRUFBK0I7QUFBRXB3QixVQUFBQSxlQUFlLEVBQUUsU0FBbkI7QUFBOEJxQixVQUFBQSxPQUFPLEVBQUU7QUFBdkMsU0FBL0I7QUFDQWl3QixRQUFBQSxpQkFBaUIsQ0FBQ0csSUFBbEI7QUFDRCxPQUpEOztBQU1BLFVBQU1DLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsR0FBTTtBQUM5QixRQUFBLE1BQUksQ0FBQ3BCLGdCQUFMLEdBQXdCLEdBQXhCO0FBQ0F4d0IsUUFBQUEsUUFBUSxDQUFDMkksR0FBVCxDQUFhLE1BQUksQ0FBQzJuQixXQUFsQixFQUErQjtBQUFFL3VCLFVBQUFBLE9BQU8sRUFBRTtBQUFYLFNBQS9CO0FBQ0Fpd0IsUUFBQUEsaUJBQWlCLENBQUNseUIsT0FBbEI7QUFDRCxPQUpEOztBQU1BLFVBQU11eUIsWUFBWSxHQUFHdHpCLFFBQVEsQ0FBQ1EsZ0JBQVQsQ0FBMEIsWUFBMUIsQ0FBckI7QUFDQTh5QixNQUFBQSxZQUFZLENBQUN6c0IsT0FBYixDQUFxQixVQUFBeEQsSUFBSSxFQUFJO0FBQzNCQSxRQUFBQSxJQUFJLENBQUNzRixnQkFBTCxDQUFzQixZQUF0QixFQUFvQ3dxQixpQkFBcEM7QUFDQTl2QixRQUFBQSxJQUFJLENBQUNzRixnQkFBTCxDQUFzQixZQUF0QixFQUFvQzBxQixpQkFBcEM7QUFDRCxPQUhEO0FBU0EsVUFBTUUsZUFBZSxHQUFHOXhCLFFBQVEsQ0FBQzZXLEVBQVQsQ0FBWSxLQUFLd1osV0FBakIsRUFBOEIsR0FBOUIsRUFBbUM7QUFDekQ5dUIsUUFBQUEsT0FBTyxFQUFFLENBRGdEO0FBRXZENnZCLFFBQUFBLFdBQVcsRUFBRSxTQUYwQztBQUd6RGx3QixRQUFBQSxJQUFJLEVBQUUsS0FBS3V2QixNQUg4QztBQUl6RGdCLFFBQUFBLE1BQU0sRUFBRSxJQUppRDtBQUt6RHZ4QixRQUFBQSxlQUFlLEVBQUU7QUFMd0MsT0FBbkMsQ0FBeEI7O0FBVUEsVUFBTTZ4QixnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQUF2dUIsQ0FBQyxFQUFJO0FBQzdCLFFBQUEsTUFBSSxDQUFDZ3RCLGdCQUFMLEdBQXdCLENBQXhCO0FBQ0t4d0IsUUFBQUEsUUFBUSxDQUFDMkksR0FBVCxDQUFhLE1BQUksQ0FBQzJuQixXQUFsQixFQUErQjtBQUFFcHdCLFVBQUFBLGVBQWUsRUFBRSxTQUFuQjtBQUE2QnFCLFVBQUFBLE9BQU8sRUFBRTtBQUF0QyxTQUEvQjtBQUNBdXdCLFFBQUFBLGVBQWUsQ0FBQ0gsSUFBaEI7QUFDTCxPQUpEOztBQU1BLFVBQU1LLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsR0FBTTtBQUM3QixRQUFBLE1BQUksQ0FBQ3hCLGdCQUFMLEdBQXdCLEdBQXhCO0FBQ0F4d0IsUUFBQUEsUUFBUSxDQUFDMkksR0FBVCxDQUFhLE1BQUksQ0FBQzJuQixXQUFsQixFQUErQjtBQUFHcHdCLFVBQUFBLGVBQWUsRUFBRSxNQUFwQjtBQUE0QnFCLFVBQUFBLE9BQU8sRUFBRTtBQUFyQyxTQUEvQjtBQUNBdXdCLFFBQUFBLGVBQWUsQ0FBQ3h5QixPQUFoQjtBQUNELE9BSkQ7O0FBTUEsVUFBTTJ5QixNQUFNLEdBQUcxekIsUUFBUSxDQUFDUSxnQkFBVCxDQUEwQixZQUExQixDQUFmO0FBQ0FrekIsTUFBQUEsTUFBTSxDQUFDN3NCLE9BQVAsQ0FBZSxVQUFBeEQsSUFBSSxFQUFJO0FBQ3JCQSxRQUFBQSxJQUFJLENBQUNzRixnQkFBTCxDQUFzQixZQUF0QixFQUFvQzZxQixnQkFBcEM7QUFDQW53QixRQUFBQSxJQUFJLENBQUNzRixnQkFBTCxDQUFzQixZQUF0QixFQUFvQzhxQixnQkFBcEM7QUFDRCxPQUhEO0FBUUEsVUFBTUUsY0FBYyxHQUFHbHlCLFFBQVEsQ0FBQzZXLEVBQVQsQ0FBWSxLQUFLd1osV0FBakIsRUFBOEIsR0FBOUIsRUFBbUM7QUFDMUQ7QUFDRW52QixRQUFBQSxJQUFJLEVBQUUsS0FBS3V2QixNQUY2QztBQUd4RGdCLFFBQUFBLE1BQU0sRUFBRSxJQUhnRDtBQUl4RGx3QixRQUFBQSxPQUFPLEVBQUUsQ0FKK0M7QUFLeEQ2dkIsUUFBQUEsV0FBVyxFQUFFO0FBTDJDLE9BQW5DLENBQXZCOztBQVFBLFVBQU1lLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsR0FBTTtBQUMzQixRQUFBLE1BQUksQ0FBQzNCLGdCQUFMLEdBQXdCLENBQXhCO0FBQ0F4d0IsUUFBQUEsUUFBUSxDQUFDMkksR0FBVCxDQUFhLE1BQUksQ0FBQzJuQixXQUFsQixFQUErQjtBQUFFcHdCLFVBQUFBLGVBQWUsRUFBRSxNQUFuQjtBQUEyQnFCLFVBQUFBLE9BQU8sRUFBRTtBQUFwQyxTQUEvQjtBQUNBMndCLFFBQUFBLGNBQWMsQ0FBQ1AsSUFBZjtBQUNELE9BSkQ7O0FBTUEsVUFBTVMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixHQUFNO0FBQzNCLFFBQUEsTUFBSSxDQUFDNUIsZ0JBQUwsR0FBd0IsR0FBeEI7QUFDQXh3QixRQUFBQSxRQUFRLENBQUMySSxHQUFULENBQWEsTUFBSSxDQUFDMm5CLFdBQWxCLEVBQStCO0FBQUdwd0IsVUFBQUEsZUFBZSxFQUFFLFNBQXBCO0FBQStCcUIsVUFBQUEsT0FBTyxFQUFFO0FBQXhDLFNBQS9CO0FBRUEyd0IsUUFBQUEsY0FBYyxDQUFDNXlCLE9BQWY7QUFDRCxPQUxEOztBQU9BLFVBQU0reUIsT0FBTyxHQUFHOXpCLFFBQVEsQ0FBQ1EsZ0JBQVQsQ0FBMEIsVUFBMUIsQ0FBaEI7QUFDQXN6QixNQUFBQSxPQUFPLENBQUNqdEIsT0FBUixDQUFnQixVQUFBeEQsSUFBSSxFQUFJO0FBQ3RCQSxRQUFBQSxJQUFJLENBQUNzRixnQkFBTCxDQUFzQixZQUF0QixFQUFvQ2lyQixjQUFwQztBQUNBdndCLFFBQUFBLElBQUksQ0FBQ3NGLGdCQUFMLENBQXNCLFlBQXRCLEVBQW9Da3JCLGNBQXBDO0FBQ0QsT0FIRDtBQUtEOzs7Ozs7QUFHSCxJQUFNRSxJQUFJLEdBQUcsSUFBSXJDLEtBQUosRUFBYjs7O0FDOU1BLENBQUMsVUFBUzFzQixDQUFULEVBQVc7QUFBQyxXQUFTQyxDQUFULENBQVdBLENBQVgsRUFBYVMsQ0FBYixFQUFlM0QsQ0FBZixFQUFpQjRFLENBQWpCLEVBQW1CO0FBQUMsUUFBSU8sQ0FBQyxHQUFDakMsQ0FBQyxDQUFDK3VCLElBQUYsRUFBTjtBQUFBLFFBQWU1c0IsQ0FBQyxHQUFDRixDQUFDLENBQUNpQixLQUFGLENBQVF6QyxDQUFSLENBQWpCO0FBQUEsUUFBNEJ1QixDQUFDLEdBQUMsRUFBOUI7QUFBaUNHLElBQUFBLENBQUMsQ0FBQ3pCLE1BQUYsS0FBV1gsQ0FBQyxDQUFDb0MsQ0FBRCxDQUFELENBQUszRyxJQUFMLENBQVUsVUFBU3VFLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUNnQyxNQUFBQSxDQUFDLElBQUUsa0JBQWdCbEYsQ0FBaEIsSUFBbUJpRCxDQUFDLEdBQUMsQ0FBckIsSUFBd0IsdUJBQXhCLEdBQWdEQyxDQUFoRCxHQUFrRCxTQUFsRCxHQUE0RDBCLENBQS9EO0FBQWlFLEtBQXpGLEdBQTJGMUIsQ0FBQyxDQUFDMUQsSUFBRixDQUFPLFlBQVAsRUFBb0IyRixDQUFwQixFQUF1QitzQixLQUF2QixHQUErQkMsTUFBL0IsQ0FBc0NqdEIsQ0FBdEMsQ0FBdEc7QUFBZ0o7O0FBQUEsTUFBSXZCLENBQUMsR0FBQztBQUFDeXVCLElBQUFBLElBQUksRUFBQyxnQkFBVTtBQUFDLGFBQU8sS0FBSzF6QixJQUFMLENBQVUsWUFBVTtBQUFDd0UsUUFBQUEsQ0FBQyxDQUFDRCxDQUFDLENBQUMsSUFBRCxDQUFGLEVBQVMsRUFBVCxFQUFZLE1BQVosRUFBbUIsRUFBbkIsQ0FBRDtBQUF3QixPQUE3QyxDQUFQO0FBQXNELEtBQXZFO0FBQXdFb3ZCLElBQUFBLEtBQUssRUFBQyxpQkFBVTtBQUFDLGFBQU8sS0FBSzN6QixJQUFMLENBQVUsWUFBVTtBQUFDd0UsUUFBQUEsQ0FBQyxDQUFDRCxDQUFDLENBQUMsSUFBRCxDQUFGLEVBQVMsR0FBVCxFQUFhLE1BQWIsRUFBb0IsR0FBcEIsQ0FBRDtBQUEwQixPQUEvQyxDQUFQO0FBQXdELEtBQWpKO0FBQWtKcXZCLElBQUFBLEtBQUssRUFBQyxpQkFBVTtBQUFDLGFBQU8sS0FBSzV6QixJQUFMLENBQVUsWUFBVTtBQUFDLFlBQUlpRixDQUFDLEdBQUMsa0NBQU47QUFBeUNULFFBQUFBLENBQUMsQ0FBQ0QsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFROEosUUFBUixDQUFpQixJQUFqQixFQUF1QndsQixXQUF2QixDQUFtQzV1QixDQUFuQyxFQUFzQzZ1QixHQUF0QyxFQUFELEVBQTZDN3VCLENBQTdDLEVBQStDLE1BQS9DLEVBQXNELEVBQXRELENBQUQ7QUFBMkQsT0FBekgsQ0FBUDtBQUFrSTtBQUFyUyxHQUFOOztBQUE2U1YsRUFBQUEsQ0FBQyxDQUFDOEwsRUFBRixDQUFLMGpCLFNBQUwsR0FBZSxVQUFTdnZCLENBQVQsRUFBVztBQUFDLFdBQU9BLENBQUMsSUFBRVMsQ0FBQyxDQUFDVCxDQUFELENBQUosR0FBUVMsQ0FBQyxDQUFDVCxDQUFELENBQUQsQ0FBS3lCLEtBQUwsQ0FBVyxJQUFYLEVBQWdCLEdBQUc4QyxLQUFILENBQVNSLElBQVQsQ0FBY3BDLFNBQWQsRUFBd0IsQ0FBeEIsQ0FBaEIsQ0FBUixHQUFvRCxjQUFZM0IsQ0FBWixJQUFlQSxDQUFmLElBQWtCRCxDQUFDLENBQUN5dkIsS0FBRixDQUFRLFlBQVV4dkIsQ0FBVixHQUFZLHFDQUFwQixHQUEyRCxJQUE3RSxJQUFtRlMsQ0FBQyxDQUFDeXVCLElBQUYsQ0FBT3p0QixLQUFQLENBQWEsSUFBYixFQUFrQixHQUFHOEMsS0FBSCxDQUFTUixJQUFULENBQWNwQyxTQUFkLEVBQXdCLENBQXhCLENBQWxCLENBQTlJO0FBQTRMLEdBQXZOO0FBQXdOLENBQXR0QixDQUF1dEI3RyxNQUF2dEIsQ0FBRDs7O0FDQUE7Ozs7OztBQU1DQyxRQUFRLENBQUM0TyxlQUFULENBQXlCcVUsU0FBekIsR0FBbUMsSUFBbkM7QUFDQXlSLFNBQVMsQ0FBQ0MsZ0JBQVYsR0FBNkIsS0FBN0I7QUFDQSxJQUFJQyxhQUFhLEdBQUc7QUFDbEJDLEVBQUFBLE9BQU8sRUFBRTtBQUNQeHFCLElBQUFBLEVBQUUsRUFBRTtBQURHO0FBRFMsQ0FBcEI7QUFLQXlxQixPQUFPLENBQUNDLElBQVIsQ0FBY0gsYUFBZDtBQUdBLElBQUlJLGdCQUFnQixHQUFHLElBQUlDLFFBQUosQ0FBYTtBQUNoQ0MsRUFBQUEsaUJBQWlCLEVBQUUsV0FEYSxDQUVoQzs7QUFGZ0MsQ0FBYixDQUF2Qjs7QUFPRCxDQUFFLFVBQVVoMUIsQ0FBVixFQUFjO0FBSWYsTUFBSWkxQixRQUFRLEdBQUdqMUIsQ0FBQyxDQUFDLElBQUQsQ0FBaEI7QUFDQSxNQUFNazFCLEtBQUksR0FBRyxpQkFBYjtBQUNBLE1BQUk3aUIsTUFBTSxHQUFHclMsQ0FBQyxDQUFDLFlBQUQsQ0FBZDtBQUNBLE1BQUltMUIsT0FBTyxHQUFHcjFCLFFBQVEsQ0FBQ2lDLGFBQVQsQ0FBdUIsU0FBdkIsQ0FBZDtBQUFBLE1BQ0NxekIsV0FBVyxHQUFHcDFCLENBQUMsQ0FBQyxlQUFELENBRGhCO0FBQUEsTUFFQ3ExQixXQUFXLEdBQUcsS0FGZjtBQUdDLE1BQU1DLEtBQUssR0FBR3QxQixDQUFDLENBQUMsVUFBRCxDQUFmLENBVmMsQ0FjZjs7QUFDQWkxQixFQUFBQSxRQUFRLENBQ0xYLFNBREgsQ0FDYSxPQURiLEVBRUcxbEIsUUFGSCxDQUVZLE1BRlosRUFFb0IwbEIsU0FGcEIsR0FmZSxDQXFCZjs7QUFDQSxNQUFNaUIsR0FBRyxHQUFHdjFCLENBQUMsQ0FBQyxRQUFELENBQWI7QUFDQSxNQUFNNEIsUUFBUSxHQUFHNUIsQ0FBQyxDQUFDLGFBQUQsQ0FBbEI7QUFDQSxNQUFNdzFCLFNBQVMsR0FBR3gxQixDQUFDLENBQUMsWUFBRCxDQUFuQixDQXhCZSxDQTBCZjs7QUFDQSxNQUFNeTFCLElBQUksR0FBRyxJQUFJdHpCLFdBQUosQ0FBZ0I7QUFDM0I2d0IsSUFBQUEsTUFBTSxFQUFFLENBQUMsQ0FEa0I7QUFFM0I7QUFDQTBDLElBQUFBLFVBQVUsRUFBRSxzQkFBTTtBQUNoQnJqQixNQUFBQSxNQUFNLENBQUNzakIsUUFBUCxDQUFnQixXQUFoQixFQURnQixDQUVoQjtBQUNEO0FBTjBCLEdBQWhCLENBQWI7O0FBVUEsTUFBRzMxQixDQUFDLENBQUMsb0JBQUQsQ0FBRCxDQUF3QnlGLE1BQXhCLEdBQWlDLENBQXBDLEVBQXVDO0FBR3ZDZ3dCLElBQUFBLElBQUksQ0FBQ3ZqQixHQUFMLENBQVMsT0FBVCxFQUVDa0csRUFGRCxDQUVJb2QsU0FGSixFQUVlLEdBRmYsRUFFb0I7QUFDSTN5QixNQUFBQSxDQUFDLEVBQUUsT0FEUDtBQUVJSixNQUFBQSxJQUFJLEVBQUVtekIsSUFBSSxDQUFDcnlCLE9BRmYsQ0FFc0I7O0FBRnRCLEtBRnBCLEVBS3VCLEtBTHZCLEVBUUMvQixNQVJELENBUVErekIsR0FSUixFQVFhLEdBUmIsRUFRa0I7QUFDaEJNLE1BQUFBLFNBQVMsRUFBRTtBQURLLEtBUmxCLEVBVUc7QUFDREEsTUFBQUEsU0FBUyxFQUFFLENBRFY7QUFFRHB6QixNQUFBQSxJQUFJLEVBQUVxekIsTUFBTSxDQUFDdnlCO0FBRlosS0FWSCxFQWFHLE9BYkgsRUFlQy9CLE1BZkQsQ0FlUSt6QixHQWZSLEVBZWEsR0FmYixFQWVrQjtBQUNoQlEsTUFBQUEsS0FBSyxFQUFFLENBRFM7QUFFaEIveEIsTUFBQUEsQ0FBQyxFQUFFO0FBRmEsS0FmbEIsRUFrQkc7QUFDRCt4QixNQUFBQSxLQUFLLEVBQUUsQ0FETjtBQUVEL3hCLE1BQUFBLENBQUMsRUFBRSxJQUZGO0FBR0R2QixNQUFBQSxJQUFJLEVBQUVxekIsTUFBTSxDQUFDdnlCO0FBSFosS0FsQkgsRUFzQkcsT0F0QkgsRUEwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBL0JBLEtBa0NDWCxXQWxDRCxDQWtDYTB5QixLQWxDYixFQWtDb0IsR0FsQ3BCLEVBa0N5QjtBQUN0Qnh5QixNQUFBQSxPQUFPLEVBQUUsQ0FEYTtBQUV0QkwsTUFBQUEsSUFBSSxFQUFFcXpCLE1BQU0sQ0FBQ3Z5QixPQUZTO0FBR3RCVixNQUFBQSxDQUFDLEVBQUU7QUFIbUIsS0FsQ3pCLEVBc0NJLE1BdENKLEVBd0NDRCxXQXhDRCxDQXdDYXN5QixLQXhDYixFQXdDbUIsR0F4Q25CLEVBd0N3QjtBQUFDcnlCLE1BQUFBLENBQUMsRUFBRSxFQUFKO0FBQVFDLE1BQUFBLE9BQU8sRUFBRSxDQUFqQjtBQUFvQkwsTUFBQUEsSUFBSSxFQUFFYSxPQUFPLENBQUNDO0FBQWxDLEtBeEN4QixFQXdDb0UsSUF4Q3BFLEVBSHVDLENBK0N2QztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQU1BdkQsSUFBQUEsQ0FBQyxDQUFDLFVBQUQsQ0FBRCxDQUFjZzJCLFlBQWQsQ0FBNEI7QUFBRUMsTUFBQUEsVUFBVSxFQUFFO0FBQWQsS0FBNUIsRUFBa0UsWUFBVztBQUMzRW4yQixNQUFBQSxRQUFRLENBQUNpSyxJQUFULENBQWNtRyxTQUFkLENBQXdCQyxNQUF4QixDQUErQixTQUEvQjtBQUNEc2xCLE1BQUFBLElBQUksQ0FBQ3ZDLElBQUw7QUFFQSxLQUpEO0FBTUMsR0EvREQsTUFpRUs7QUFDSixRQUFJM3hCLFFBQVEsQ0FBQzZXLEVBQWIsQ0FBZ0JvZCxTQUFoQixFQUEyQixHQUEzQixFQUFnQztBQUNSM3lCLE1BQUFBLENBQUMsRUFBRSxPQURLO0FBRVJKLE1BQUFBLElBQUksRUFBRW16QixJQUFJLENBQUNyeUIsT0FGSCxDQUVVOztBQUZWLEtBQWhDLEVBR3VCLEtBSHZCO0FBSUE7O0FBSUYxRCxFQUFBQSxNQUFNLENBQUNDLFFBQUQsQ0FBTixDQUFpQkMsS0FBakIsQ0FBdUIsVUFBU0MsQ0FBVCxFQUFZO0FBSWxDQSxJQUFBQSxDQUFDLENBQUMsU0FBRCxDQUFELENBQWFrTyxFQUFiLENBQWdCLE9BQWhCLEVBQXlCLFdBQXpCLEVBQXNDLFVBQVNuSixDQUFULEVBQVk7QUFDakRBLE1BQUFBLENBQUMsQ0FBQ3dHLGNBQUY7QUFHQyxVQUFJMnFCLElBQUksR0FBR2wyQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFxQixJQUFSLENBQWEsTUFBYixDQUFYO0FBQ0EsVUFBSTgwQixRQUFRLEdBQUd6eUIsTUFBTSxDQUFDeXlCLFFBQVAsQ0FBZ0JDLFFBQWhCLENBQXlCdnRCLFFBQXpCLEVBQWY7QUFDQSxVQUFJd3RCLFFBQVEsR0FBR3IyQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVE4ekIsSUFBUixHQUFlanJCLFFBQWYsR0FBMEJHLFdBQTFCLEVBQWYsQ0FOZ0QsQ0FTOUM7O0FBQ0MsVUFBSW10QixRQUFRLENBQUN4b0IsT0FBVCxDQUFpQjBvQixRQUFqQixNQUErQixDQUFDLENBQXBDLEVBQXVDO0FBQ3ZDQyxRQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxtQkFBWixFQUFpQ0YsUUFBakMsRUFBMkNILElBQTNDLEVBQWlEQyxRQUFqRDtBQUNBenlCLFFBQUFBLE1BQU0sQ0FBQ3l5QixRQUFQLENBQWdCSyxNQUFoQixDQUF1QixJQUF2QjtBQUNBLGVBQU8sS0FBUDtBQUVBOztBQUVIckIsTUFBQUEsT0FBTyxDQUFDamxCLFNBQVIsQ0FBa0JnQyxHQUFsQixDQUFzQixnQkFBdEI7QUFHQSxVQUFJdWtCLEtBQUssR0FBR3oyQixDQUFDLENBQUMsSUFBRCxDQUFiO0FBQ0F5MkIsTUFBQUEsS0FBSyxDQUFDeGMsTUFBTixHQUFleWMsUUFBZixHQUEwQkMsV0FBMUIsQ0FBc0MsUUFBdEMsRUFBZ0R0QyxHQUFoRCxHQUFzRHNCLFFBQXRELENBQStELFFBQS9EO0FBR0EsVUFBSWlCLE9BQU8sR0FBRzUyQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFxQixJQUFSLENBQWEsTUFBYixDQUFkO0FBQ0EsVUFBSXcxQixXQUFXLEdBQUc3MkIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRcUIsSUFBUixDQUFhLE9BQWIsQ0FBbEI7QUFDQSxVQUFJeTFCLFFBQVEsR0FBRzkyQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFxQixJQUFSLENBQWEsV0FBYixDQUFmO0FBRUYwMUIsTUFBQUEsUUFBUSxDQUFDSCxPQUFELEVBQVVFLFFBQVYsRUFBb0JELFdBQXBCLENBQVI7QUFFQyxLQTlCRDs7QUFrQ0RuekIsSUFBQUEsTUFBTSxDQUFDc3pCLFVBQVAsR0FBb0IsVUFBU2p5QixDQUFULEVBQVk7QUFDM0I7QUFDQWd5QixNQUFBQSxRQUFRLENBQUNoeUIsQ0FBQyxDQUFDa3lCLEtBQUYsR0FBVWx5QixDQUFDLENBQUNreUIsS0FBRixDQUFRQyxHQUFsQixHQUF3QixJQUF6QixDQUFSO0FBQ0gsS0FIRjs7QUFVQSxhQUFTSCxRQUFULENBQWtCSSxZQUFsQixFQUFnQ0wsUUFBaEMsRUFBMENNLGdCQUExQyxFQUEyRDtBQUMxRDF6QixNQUFBQSxNQUFNLENBQUMyekIsT0FBUCxDQUFlQyxTQUFmLENBQXlCO0FBQUNKLFFBQUFBLEdBQUcsRUFBRSxLQUFLQyxZQUFMLEdBQW9CO0FBQTFCLE9BQXpCLEVBQXdEQyxnQkFBeEQsRUFBMEVELFlBQTFFO0FBRUEsVUFBSVYsS0FBSyxHQUFHejJCLENBQUMsQ0FBQyxJQUFELENBQWI7QUFDQXkyQixNQUFBQSxLQUFLLENBQUNDLFFBQU4sR0FBaUJDLFdBQWpCLENBQTZCLFFBQTdCLEVBQXVDdEMsR0FBdkMsR0FBNkNzQixRQUE3QyxDQUFzRCxRQUF0RDtBQUNDLFVBQUk0QixVQUFVLEdBQUdULFFBQWpCO0FBRUEsVUFBSVUsT0FBTyxHQUFHeDNCLENBQUMsQ0FBQyxhQUFELENBQWY7QUFFQXczQixNQUFBQSxPQUFPLENBQUMzQyxJQUFSLENBQWEwQyxVQUFiLEVBQXlCLFVBQVN4eUIsQ0FBVCxFQUFXO0FBQ3BDLFlBQUlELENBQUMsR0FBR2hGLFFBQVEsQ0FBQzhMLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBUjtBQUNBLFlBQUk2ckIsV0FBVyxHQUFHMXlCLENBQWxCO0FBQ29CRCxRQUFBQSxDQUFDLENBQUNnaEIsU0FBRixHQUFjL2dCLENBQWQ7QUFDQSxZQUFJbEQsQ0FBQyxHQUFHaUQsQ0FBQyxDQUFDL0MsYUFBRixDQUFnQixPQUFoQixDQUFSO0FBQ0FqQyxRQUFBQSxRQUFRLENBQUN1bUIsS0FBVCxHQUFpQnhrQixDQUFDLENBQUNxb0IsV0FBbkI7QUFFSCxPQVBqQixFQVR5RCxDQW9CMUQ7O0FBQ0EsVUFBSXdOLE9BQU8sR0FBRzEzQixDQUFDLENBQUMsMENBQXdDdTNCLFVBQXhDLEdBQW1ELFdBQXBELENBQUQsQ0FBa0VJLFFBQWxFLENBQTJFdkMsV0FBM0UsQ0FBZCxDQXJCMEQsQ0FzQjFEOztBQUNBc0MsTUFBQUEsT0FBTyxDQUFDN0MsSUFBUixDQUFhMEMsVUFBVSxHQUFDLGVBQXhCLEVBQXlDLFVBQVNLLEtBQVQsRUFBZTtBQUV2REYsUUFBQUEsT0FBTyxDQUFDMW9CLElBQVIsQ0FBYSxVQUFiLEVBQXlCMm5CLFdBQXpCLENBQXFDLFNBQXJDLEVBQWdEdEMsR0FBaEQsR0FBc0RzQixRQUF0RCxDQUErRCxTQUEvRDtBQUNBa0MsUUFBQUEsbUJBQW1CLENBQUNILE9BQUQsQ0FBbkI7QUFHQSxPQU5EO0FBU0U7O0FBSUQsYUFBU0csbUJBQVQsQ0FBNkJOLFVBQTdCLEVBQXlDO0FBQ3ZDdjNCLE1BQUFBLENBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JpRCxPQUFoQixDQUF3QjtBQUFFZ1EsUUFBQUEsU0FBUyxFQUFFO0FBQWIsT0FBeEIsRUFBMEMsR0FBMUMsRUFEdUMsQ0FJdkM7O0FBQ0Fza0IsTUFBQUEsVUFBVSxDQUFDWixXQUFYLENBQXVCLGlCQUF2QixFQUEwQzNuQixJQUExQyxDQUErQyxVQUEvQyxFQUEyRG1CLE1BQTNEO0FBQ0FrbEIsTUFBQUEsV0FBVyxHQUFJLEtBQWY7QUFFRXBCLE1BQUFBLElBQUksR0FSaUMsQ0FTdkM7QUFFQTs7QUFDQSxVQUFNc0IsR0FBRyxHQUFHdjFCLENBQUMsQ0FBQyxRQUFELENBQWI7QUFDQSxVQUFNNEIsUUFBUSxHQUFHNUIsQ0FBQyxDQUFDLGFBQUQsQ0FBbEI7QUFDQSxVQUFNODNCLEtBQUssR0FBRyxpQkFBZDtBQUNBLFVBQU03QyxRQUFRLEdBQUdqMUIsQ0FBQyxDQUFDLElBQUQsQ0FBbEI7QUFDQSxVQUFNczFCLEtBQUssR0FBR3QxQixDQUFDLENBQUMsVUFBRCxDQUFmO0FBR0FpMUIsTUFBQUEsUUFBUSxDQUNMWCxTQURILENBQ2EsT0FEYixFQW5CdUMsQ0F1QnZDOztBQUNBLFVBQU1tQixJQUFJLEdBQUcsSUFBSXR6QixXQUFKLENBQWdCO0FBQzNCO0FBQ0E7QUFDQXV6QixRQUFBQSxVQUFVLEVBQUUsc0JBQU07QUFDaEIxMUIsVUFBQUEsQ0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQm1RLE1BQWhCO0FBQ0Q7QUFMMEIsT0FBaEIsQ0FBYjs7QUFXQSxVQUFHblEsQ0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0J5RixNQUF4QixHQUFpQyxDQUFwQyxFQUF1QztBQUd2Q2d3QixRQUFBQSxJQUFJLENBQUN2akIsR0FBTCxDQUFTLE9BQVQsRUFFQ2tHLEVBRkQsQ0FFSW9kLFNBRkosRUFFZSxHQUZmLEVBRW9CO0FBQ3JCM3lCLFVBQUFBLENBQUMsRUFBRSxPQURrQjtBQUVyQkosVUFBQUEsSUFBSSxFQUFFbXpCLElBQUksQ0FBQ3J5QixPQUZVLENBRUg7O0FBRkcsU0FGcEIsRUFLdUIsS0FMdkIsRUFRQy9CLE1BUkQsQ0FRUSt6QixHQVJSLEVBUWEsR0FSYixFQVFrQjtBQUNoQk0sVUFBQUEsU0FBUyxFQUFFO0FBREssU0FSbEIsRUFVRztBQUNEQSxVQUFBQSxTQUFTLEVBQUUsQ0FEVjtBQUVEcHpCLFVBQUFBLElBQUksRUFBRXF6QixNQUFNLENBQUN2eUI7QUFGWixTQVZILEVBYUcsT0FiSCxFQWVDL0IsTUFmRCxDQWVRK3pCLEdBZlIsRUFlYSxHQWZiLEVBZWtCO0FBQ2hCUSxVQUFBQSxLQUFLLEVBQUUsQ0FEUztBQUVoQi94QixVQUFBQSxDQUFDLEVBQUU7QUFGYSxTQWZsQixFQWtCRztBQUNEK3hCLFVBQUFBLEtBQUssRUFBRSxDQUROO0FBRUQveEIsVUFBQUEsQ0FBQyxFQUFFLElBRkY7QUFHRHZCLFVBQUFBLElBQUksRUFBRXF6QixNQUFNLENBQUN2eUI7QUFIWixTQWxCSCxFQXNCRyxPQXRCSCxFQTJCQy9CLE1BM0JELENBMkJRSSxRQTNCUixFQTJCa0IsR0EzQmxCLEVBMkJ1QjtBQUNyQmtCLFVBQUFBLE9BQU8sRUFBRTtBQURZLFNBM0J2QixFQTZCRztBQUNEQSxVQUFBQSxPQUFPLEVBQUUsQ0FEUjtBQUVETCxVQUFBQSxJQUFJLEVBQUVhLE9BQU8sQ0FBQ0M7QUFGYixTQTdCSCxFQWdDRyxPQWhDSCxFQW1DQ1gsV0FuQ0QsQ0FtQ2EweUIsS0FuQ2IsRUFtQ29CLEdBbkNwQixFQW1DeUI7QUFDdEJ4eUIsVUFBQUEsT0FBTyxFQUFFLENBRGE7QUFFdEJMLFVBQUFBLElBQUksRUFBRWEsT0FBTyxDQUFDQyxPQUZRO0FBR3RCVixVQUFBQSxDQUFDLEVBQUU7QUFIbUIsU0FuQ3pCLEVBdUNJLE1BdkNKLEVBd0NDRCxXQXhDRCxDQXdDYWsxQixLQXhDYixFQXdDb0IsQ0F4Q3BCLEVBd0N1QjtBQUNwQmgxQixVQUFBQSxPQUFPLEVBQUUsQ0FEVztBQUVwQkwsVUFBQUEsSUFBSSxFQUFFYSxPQUFPLENBQUNDLE9BRk07QUFHcEJWLFVBQUFBLENBQUMsRUFBRTtBQUhpQixTQXhDdkIsRUE0Q0ksTUE1Q0o7QUFtREFtekIsUUFBQUEsWUFBWSxDQUFFbDJCLFFBQVEsQ0FBQ2lDLGFBQVQsQ0FBdUIsb0JBQXZCLENBQUYsRUFBZ0Q7QUFBRWswQixVQUFBQSxVQUFVLEVBQUU7QUFBZCxTQUFoRCxFQUFzRSxVQUFVckwsUUFBVixFQUFxQjtBQUNuR3VLLFVBQUFBLE9BQU8sQ0FBQ2psQixTQUFSLENBQWtCQyxNQUFsQixDQUF5QixnQkFBekI7QUFDRHNsQixVQUFBQSxJQUFJLENBQUN2QyxJQUFMO0FBQ0YsU0FIVyxDQUFaO0FBS0EsT0EzREEsTUE4REQ7QUFFS2lDLFFBQUFBLE9BQU8sQ0FBQ2psQixTQUFSLENBQWtCQyxNQUFsQixDQUF5QixnQkFBekI7QUFFSjtBQUdBO0FBS0MsR0FqTUw7QUF3TUMsQ0F2VEQsRUF1VEt0USxNQXZUTCxFLENBNFRBOzs7QUFDQSxTQUFTazRCLE9BQVQsR0FBbUI7QUFDZi8zQixFQUFBQSxDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQk8sSUFBckIsQ0FBMEIsWUFBVztBQUNqQyxRQUFJeTNCLEtBQUssR0FBR2g0QixDQUFDLENBQUMsSUFBRCxDQUFELENBQVF5TyxJQUFSLENBQWEsR0FBYixFQUFrQndwQixJQUFsQixDQUF1QixLQUF2QixDQUFaO0FBQ0FqNEIsSUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFReU8sSUFBUixDQUFhLHdCQUFiLEVBQXVDNUosR0FBdkMsQ0FBMkM7QUFBRSwwQkFBb0IsU0FBU216QixLQUFULEdBQWlCLEdBQXZDO0FBQTRDLHlCQUFtQixPQUEvRDtBQUF3RSw2QkFBdUI7QUFBL0YsS0FBM0M7QUFDSCxHQUhEO0FBSUgsQyxDQUdEOzs7QUFDQSxTQUFTRSxVQUFULEdBQXNCO0FBQ2xCLE1BQUlsNEIsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJ5RixNQUFyQixHQUE4QixDQUFsQyxFQUFxQztBQUNqQ3pGLElBQUFBLENBQUMsQ0FBQyxzQkFBRCxDQUFELENBQTBCbTRCLGtCQUExQixDQUE2QztBQUN6Q1AsTUFBQUEsS0FBSyxFQUFFLFlBRGtDO0FBRXpDUSxNQUFBQSxjQUFjLEVBQUUsT0FGeUI7QUFHekNDLE1BQUFBLGdCQUFnQixFQUFFO0FBSHVCLEtBQTdDO0FBTUg7QUFDSjs7QUFHRCxTQUFTQyxpQkFBVCxHQUE2QjtBQUMzQixNQUFJQyxJQUFJLEdBQUd6NEIsUUFBUSxDQUFDaUMsYUFBVCxDQUF1QixPQUF2QixDQUFYOztBQUVBLE1BQUd3MkIsSUFBSCxFQUFTO0FBR1QsUUFBSUMsS0FBSyxHQUFHeDRCLENBQUMsQ0FBQyxPQUFELENBQUQsQ0FBV3k0QixPQUFYLENBQW1CO0FBQzdCQyxNQUFBQSxZQUFZLEVBQUUsYUFEZTtBQUVoQztBQUNJcnlCLE1BQUFBLE1BQU0sRUFBRSxHQUhvQjtBQUkzQnN5QixNQUFBQSxZQUFZLEVBQUU7QUFBRUMsUUFBQUEsU0FBUyxFQUFFLGVBQWI7QUFBOEI5MUIsUUFBQUEsT0FBTyxFQUFFO0FBQXZDLE9BSmE7QUFLM0IrMUIsTUFBQUEsV0FBVyxFQUFFO0FBQUVELFFBQUFBLFNBQVMsRUFBRSxtQkFBYjtBQUFrQzkxQixRQUFBQSxPQUFPLEVBQUU7QUFBM0MsT0FMYztBQU85QmcyQixNQUFBQSxPQUFPLEVBQUU7QUFDTEMsUUFBQUEsV0FBVyxFQUFFO0FBRFI7QUFQcUIsS0FBbkIsQ0FBWjtBQWdCQVAsSUFBQUEsS0FBSyxDQUFDdHFCLEVBQU4sQ0FBVSxnQkFBVixFQUE0QixZQUFXO0FBQ3JDbE8sTUFBQUEsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFld2IsSUFBZjtBQUNBeGIsTUFBQUEsQ0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0I2RSxHQUF4QixDQUE0QixTQUE1QixFQUF1QyxHQUF2QztBQUVELEtBSkQ7QUFNQTJ6QixJQUFBQSxLQUFLLENBQUN4QyxZQUFOLEdBQXFCZ0QsUUFBckIsQ0FBK0IsWUFBVztBQUN4Q1QsTUFBQUEsSUFBSSxDQUFDcm9CLFNBQUwsQ0FBZUMsTUFBZixDQUFzQixxQkFBdEI7QUFDQXFvQixNQUFBQSxLQUFLLENBQUNDLE9BQU4sQ0FBYyxRQUFkO0FBQ0QsS0FIRDtBQU1BLFFBQUlRLFlBQVksR0FBR2o1QixDQUFDLENBQUMseUJBQUQsQ0FBcEI7QUFHQ2k1QixJQUFBQSxZQUFZLENBQUNyUSxLQUFiLENBQW1CLFlBQVU7QUFDMUIsVUFBSTZOLEtBQUssR0FBR3oyQixDQUFDLENBQUMsSUFBRCxDQUFiO0FBRURpNUIsTUFBQUEsWUFBWSxDQUFDdEMsV0FBYixDQUF5QixRQUF6QjtBQUNDMzJCLE1BQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTIxQixRQUFSLENBQWlCLFFBQWpCO0FBRUEsVUFBSXBQLFFBQVEsR0FBR2tRLEtBQUssQ0FBQ3dCLElBQU4sQ0FBVyxRQUFYLENBQWY7QUFDQ08sTUFBQUEsS0FBSyxDQUFDQyxPQUFOLENBQWM7QUFDYnB5QixRQUFBQSxNQUFNLEVBQUVrZ0I7QUFESyxPQUFkO0FBSUYsS0FYRjtBQWNBO0FBQ0Y7O0FBR0QsU0FBUzJTLFlBQVQsR0FBdUI7QUFFckJsNUIsRUFBQUEsQ0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQnMwQixTQUFsQixDQUE0QixTQUE1QjtBQUVBdDBCLEVBQUFBLENBQUMsQ0FBQ08sSUFBRixDQUFPUCxDQUFDLENBQUMsY0FBRCxDQUFSLEVBQTBCLFVBQVNRLEtBQVQsRUFBZ0JDLFdBQWhCLEVBQTZCO0FBRXJEVCxJQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVE0TyxRQUFSLEdBQW1CdXFCLE9BQW5CLENBQTJCLGVBQTNCO0FBQ0EsUUFBSS9ULElBQUksR0FBR3BsQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVE0TyxRQUFSLEVBQVg7QUFDRHdXLElBQUFBLElBQUksQ0FBQ2dVLEtBQUwsR0FBYXpCLFFBQWIsQ0FBc0IsSUFBdEI7QUFHQSxHQVBELEVBSnFCLENBWXBCOztBQUdELE1BQUluMEIsS0FBSyxHQUFHMUQsUUFBUSxDQUFDUSxnQkFBVCxDQUEwQiwwQkFBMUIsQ0FBWjtBQUNDTixFQUFBQSxDQUFDLENBQUNPLElBQUYsQ0FBT2lELEtBQVAsRUFBYyxVQUFTaEQsS0FBVCxFQUFnQkMsV0FBaEIsRUFBNkI7QUFFMUMsUUFBSTQ0QixRQUFRLEdBQUdyNUIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMlgsSUFBUixDQUFhLE9BQWIsRUFBc0IyaEIsTUFBdEIsRUFBZjtBQUNNdDVCLElBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWcwQixNQUFSLENBQWVxRixRQUFmO0FBRUQsR0FMTjtBQU9EcjVCLEVBQUFBLENBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0J1NUIsUUFBbEIsQ0FBMkIsWUFBVztBQUNuQyxRQUFJdjVCLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXc1QixHQUFSLE1BQWlCLEVBQXJCLEVBQXlCO0FBQ3ZCeDVCLE1BQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTIxQixRQUFSLENBQWlCLFdBQWpCO0FBQ0QsS0FGRCxNQUVPO0FBQ0wzMUIsTUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMjJCLFdBQVIsQ0FBb0IsV0FBcEI7QUFDRDtBQUNGLEdBTkY7QUFPRDs7QUFFRCxTQUFTOEMsYUFBVCxHQUF5QjtBQUV2QixNQUFJMTBCLENBQUMsR0FBQ2pGLFFBQVEsQ0FBQ3dILGNBQVQsQ0FBd0IsS0FBeEIsQ0FBTjs7QUFDQSxNQUFHdkMsQ0FBSCxFQUFLO0FBRUwsUUFBSTIwQixJQUFJLEdBQUcsSUFBSXY1QixXQUFXLENBQUNDLFVBQWhCLEVBQVg7QUFDQSxRQUFJdTVCLEtBQUssR0FBRyxJQUFJcDRCLFFBQVEsQ0FBQ0MsTUFBYixDQUFvQnVELENBQXBCLEVBQXNCLENBQXRCLEVBQXdCO0FBQUN0QyxNQUFBQSxJQUFJLEVBQUNtM0IsSUFBSSxDQUFDcjJCLE9BQVg7QUFBbUJULE1BQUFBLE9BQU8sRUFBQztBQUEzQixLQUF4QixFQUFzRDtBQUFDTCxNQUFBQSxJQUFJLEVBQUNtM0IsSUFBSSxDQUFDcjJCLE9BQVg7QUFBbUJULE1BQUFBLE9BQU8sRUFBQyxDQUEzQjtBQUE2QlYsTUFBQUEsS0FBSyxFQUFDO0FBQW5DLEtBQXRELENBQVosQ0FISyxDQUtEOztBQUNBLFFBQUlqQyxXQUFXLENBQUNPLEtBQWhCLENBQXNCO0FBQ2RDLE1BQUFBLGNBQWMsRUFBRW9FLENBREY7QUFFZG5FLE1BQUFBLFdBQVcsRUFBRSxTQUZDO0FBR2RDLE1BQUFBLE9BQU8sRUFBRSxLQUhLO0FBSWRDLE1BQUFBLFFBQVEsRUFBRSxHQUpJLENBS2Y7O0FBTGUsS0FBdEIsRUFPS2EsUUFQTCxDQU9jZzRCLEtBUGQsRUFRSzE0QixLQVJMLENBUVd5NEIsSUFSWCxFQU5DLENBZUQ7O0FBTUYsUUFBSWwwQixDQUFDLEdBQUNULENBQUMsQ0FBQ2dPLHFCQUFGLEdBQTBCQyxHQUFoQztBQUFBLFFBQ0FsTyxDQUFDLEdBQUMsU0FBRkEsQ0FBRSxHQUFVO0FBQUMsYUFBT0MsQ0FBQyxDQUFDZ0gsYUFBRixDQUFnQmdILHFCQUFoQixHQUF3Q2liLE1BQXhDLEdBQStDdHFCLE1BQU0sQ0FBQzJxQixXQUF0RCxJQUFtRTNxQixNQUFNLENBQUNDLFVBQVAsR0FBa0IsR0FBNUY7QUFBZ0csS0FEN0c7O0FBR0E3RCxJQUFBQSxRQUFRLENBQUMySSxnQkFBVCxDQUEwQixRQUExQixFQUFtQyxZQUFVO0FBQzdDLFVBQUk1RyxDQUFDLEdBQUNrRCxDQUFDLENBQUNnSCxhQUFGLENBQWdCZ0gscUJBQWhCLEdBQXdDaWIsTUFBeEMsR0FBK0N4b0IsQ0FBL0MsR0FBaUQ5QixNQUFNLENBQUMycUIsV0FBOUQ7QUFBQSxVQUNBMW1CLENBQUMsR0FBQ2pFLE1BQU0sQ0FBQ3dxQixXQUFQLElBQW9CcHVCLFFBQVEsQ0FBQzRPLGVBQVQsQ0FBeUJ1RSxTQUQvQztBQUVBbk8sTUFBQUEsQ0FBQyxLQUFHNkMsQ0FBQyxJQUFFOUYsQ0FBSCxJQUFNa0QsQ0FBQyxDQUFDbUwsU0FBRixDQUFZZ0MsR0FBWixDQUFnQixRQUFoQixHQUEwQm5OLENBQUMsQ0FBQ21MLFNBQUYsQ0FBWUMsTUFBWixDQUFtQixPQUFuQixDQUFoQyxJQUNIeEksQ0FBQyxJQUFFbkMsQ0FBSCxJQUFNVCxDQUFDLENBQUNtTCxTQUFGLENBQVlDLE1BQVosQ0FBbUIsUUFBbkIsR0FBNkJwTCxDQUFDLENBQUNtTCxTQUFGLENBQVlnQyxHQUFaLENBQWdCLE9BQWhCLENBQW5DLEtBQ0NuTixDQUFDLENBQUNtTCxTQUFGLENBQVlDLE1BQVosQ0FBbUIsUUFBbkIsR0FBNkJwTCxDQUFDLENBQUNtTCxTQUFGLENBQVlDLE1BQVosQ0FBbUIsT0FBbkIsQ0FEOUIsQ0FEQSxHQUUyRHBMLENBQUMsQ0FBQ21MLFNBQUYsQ0FBWWdDLEdBQVosQ0FBZ0IsVUFBaEIsQ0FGNUQ7QUFJRCxLQVBDO0FBUUQ7QUFDRjs7QUFNRCxTQUFTK2hCLElBQVQsR0FBZ0I7QUFDWmlGLEVBQUFBLFlBQVk7QUFDWlosRUFBQUEsaUJBQWlCO0FBQ2pCUCxFQUFBQSxPQUFPO0FBQ1AwQixFQUFBQSxhQUFhO0FBR2hCOztBQVFEejVCLENBQUMsQ0FBQ0YsUUFBRCxDQUFELENBQVlDLEtBQVosQ0FBa0IsWUFBVztBQUV6Qjs7QUFDQWswQixFQUFBQSxJQUFJO0FBQ1AsQ0FKRDs7Ozs7Ozs7O0FDemZBOzs7Ozs7Ozs7O0FBVUE7QUFDSTtBQURKLE1BRVU0RixJQUZWO0FBQUE7QUFBQTtBQUdRLGtCQUFZQyxFQUFaLEVBQWdCO0FBQUE7O0FBQUE7O0FBQ1osV0FBS0MsR0FBTCxHQUFXO0FBQUNELFFBQUFBLEVBQUUsRUFBRUE7QUFBTCxPQUFYLENBRFksQ0FFWjs7QUFDQSxXQUFLQyxHQUFMLENBQVNDLFFBQVQsR0FBb0JsNkIsUUFBUSxDQUFDaUMsYUFBVCxDQUF1QixlQUF2QixDQUFwQjtBQUNBLFdBQUtnNEIsR0FBTCxDQUFTRSxTQUFULEdBQXFCLEtBQUtGLEdBQUwsQ0FBU0QsRUFBVCxDQUFZLzNCLGFBQVosQ0FBMEIsZ0JBQTFCLENBQXJCO0FBQ0EsV0FBS2c0QixHQUFMLENBQVNHLFNBQVQsR0FBcUJwNkIsUUFBUSxDQUFDaUMsYUFBVCxDQUF1QixnQkFBdkIsQ0FBckI7QUFHQSxXQUFLZzRCLEdBQUwsQ0FBU0MsUUFBVCxDQUFrQnZ4QixnQkFBbEIsQ0FBbUMsT0FBbkMsRUFBNEM7QUFBQSxlQUFNLEtBQUksQ0FBQzB4QixJQUFMLEVBQU47QUFBQSxPQUE1Qzs7QUFHQSxVQUFHLEtBQUtKLEdBQUwsQ0FBU0csU0FBWixFQUF1QjtBQUFDLGFBQUtILEdBQUwsQ0FBU0csU0FBVCxDQUFtQkUsT0FBbkIsR0FBNkIsWUFBWTtBQUFFLGVBQUtELElBQUw7QUFBWSxTQUF2RDtBQUF5RCxPQVhyRSxDQWNiOzs7QUFHQyxXQUFLSixHQUFMLENBQVNFLFNBQVQsQ0FBbUJ4eEIsZ0JBQW5CLENBQW9DLE9BQXBDLEVBQTZDO0FBQUEsZUFBTSxLQUFJLENBQUNvSCxLQUFMLEVBQU47QUFBQSxPQUE3QztBQUVBL1AsTUFBQUEsUUFBUSxDQUFDaUMsYUFBVCxDQUF1QixnQkFBdkIsRUFBeUMwRyxnQkFBekMsQ0FBMEQsT0FBMUQsRUFBbUU7QUFBQSxlQUFNLEtBQUksQ0FBQ29ILEtBQUwsRUFBTjtBQUFBLE9BQW5FO0FBRUEsV0FBS2txQixHQUFMLENBQVNNLFFBQVQsR0FBb0J2NkIsUUFBUSxDQUFDaUMsYUFBVCxDQUF1QixVQUF2QixDQUFwQixDQXJCWSxDQXdCWjs7QUFDQSxXQUFLZzRCLEdBQUwsQ0FBUzUxQixLQUFULEdBQWlCbTJCLEtBQUssQ0FBQ25oQixJQUFOLENBQVcsS0FBSzRnQixHQUFMLENBQVNELEVBQVQsQ0FBWXg1QixnQkFBWixDQUE2QixhQUE3QixDQUFYLENBQWpCLENBekJZLENBMEJaOztBQUNBLFdBQUtpNkIsVUFBTCxHQUFrQixLQUFLUixHQUFMLENBQVM1MUIsS0FBVCxDQUFlc0IsTUFBakMsQ0EzQlksQ0E2Qlo7O0FBQ0EsV0FBS3MwQixHQUFMLENBQVNTLFNBQVQsR0FBcUIsS0FBS1QsR0FBTCxDQUFTRCxFQUFULENBQVl4NUIsZ0JBQVosQ0FBNkIsOEJBQTdCLENBQXJCO0FBQ0gsS0FsQ1QsQ0FtQ1E7OztBQW5DUjtBQUFBO0FBQUEsNkJBb0NlO0FBQ0gsYUFBS3dSLE1BQUwsQ0FBWSxNQUFaO0FBQ0gsT0F0Q1QsQ0F1Q1E7O0FBdkNSO0FBQUE7QUFBQSw4QkF3Q2dCO0FBQ0osYUFBS0EsTUFBTCxDQUFZLE9BQVo7QUFDSDtBQTFDVDtBQUFBO0FBQUEsNkJBMkNlMm9CLE1BM0NmLEVBMkN1QjtBQUFBOztBQUNYLFlBQUssS0FBS3BGLFdBQVYsRUFBd0IsT0FEYixDQUVYOztBQUNBLGFBQUtBLFdBQUwsR0FBbUIsSUFBbkIsQ0FIVyxDQUlYOztBQUNBLGFBQUswRSxHQUFMLENBQVNELEVBQVQsQ0FBWTVwQixTQUFaLENBQXNCdXFCLE1BQU0sS0FBSyxNQUFYLEdBQW9CLEtBQXBCLEdBQTRCLFFBQWxELEVBQTRELFlBQTVEO0FBRUEzNkIsUUFBQUEsUUFBUSxDQUFDaUssSUFBVCxDQUFjbUcsU0FBZCxDQUF3QnVxQixNQUFNLEtBQUssTUFBWCxHQUFvQixLQUFwQixHQUE0QixRQUFwRCxFQUE4RCxjQUE5RCxFQVBXLENBUVg7O0FBQ0EsWUFBTUMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ0MsR0FBRCxFQUFTO0FBQzFCLGNBQUtBLEdBQUcsS0FBSyxNQUFJLENBQUNKLFVBQUwsR0FBZ0IsQ0FBN0IsRUFBaUM7QUFDN0IsWUFBQSxNQUFJLENBQUNsRixXQUFMLEdBQW1CLEtBQW5COztBQUVBLGdCQUFLb0YsTUFBTSxLQUFLLE9BQWhCLEVBQTBCO0FBRTFCbDVCLGNBQUFBLFFBQVEsQ0FBQzZXLEVBQVQsQ0FBWSxNQUFJLENBQUMyaEIsR0FBTCxDQUFTTSxRQUFyQixFQUErQixHQUEvQixFQUFvQztBQUNoQzUzQixnQkFBQUEsSUFBSSxFQUFFbTRCLEtBQUssQ0FBQzkyQixTQURvQjtBQUVoQ2pCLGdCQUFBQSxDQUFDLEVBQUUsT0FGNkIsQ0FHakM7O0FBSGlDLGVBQXBDO0FBT0g7QUFJQTtBQUtKLFNBdEJEOztBQTBCQSxZQUFLNDNCLE1BQU0sS0FBSyxNQUFoQixFQUF5QjtBQUNyQixjQUFNSSxRQUFRLEdBQUcsRUFBakI7QUFFQUEsVUFBQUEsUUFBUSxDQUFDaDRCLENBQVQsR0FBYSxPQUFiO0FBQ0FnNEIsVUFBQUEsUUFBUSxDQUFDNzJCLENBQVQsR0FBYSxJQUFiLENBSnFCLENBS3JCOztBQUNBekMsVUFBQUEsUUFBUSxDQUFDMkksR0FBVCxDQUFhLEtBQUs2dkIsR0FBTCxDQUFTTSxRQUF0QixFQUFnQ1EsUUFBaEMsRUFOcUIsQ0FRckI7O0FBQ0F0NUIsVUFBQUEsUUFBUSxDQUFDNlcsRUFBVCxDQUFZLEtBQUsyaEIsR0FBTCxDQUFTTSxRQUFyQixFQUErQixFQUEvQixFQUFtQztBQUMvQjUzQixZQUFBQSxJQUFJLEVBQUVxNEIsS0FBSyxDQUFDdjNCLE9BRG1CO0FBRS9CUyxZQUFBQSxDQUFDLEVBQUUsSUFGNEI7QUFHL0JuQixZQUFBQSxDQUFDLEVBQUUsSUFINEI7QUFJL0I2eUIsWUFBQUEsVUFBVSxFQUFFO0FBQUEscUJBQU1xRixjQUFjLEVBQXBCO0FBQUE7QUFKbUIsV0FBbkM7QUFNSCxTQWZELE1BbUJLO0FBQ0R4NUIsVUFBQUEsUUFBUSxDQUFDNlcsRUFBVCxDQUFZLEtBQUsyaEIsR0FBTCxDQUFTTSxRQUFyQixFQUErQixHQUEvQixFQUFvQztBQUNoQzUzQixZQUFBQSxJQUFJLEVBQUVtNEIsS0FBSyxDQUFDOTJCLFNBRG9CO0FBRWpDO0FBQ0E7QUFDQzR4QixZQUFBQSxVQUFVLEVBQUU7QUFBQSxxQkFBTXFGLGNBQWMsRUFBcEI7QUFBQTtBQUpvQixXQUFwQztBQVFIOztBQU1ELFlBQU1BLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsR0FBTTtBQUU3QjtBQUNBLFVBQUEsTUFBSSxDQUFDaEIsR0FBTCxDQUFTNTFCLEtBQVQsQ0FBZXdDLE9BQWYsQ0FBdUIsVUFBQ216QixFQUFELEVBQUthLEdBQUwsRUFBYTtBQUNoQztBQUNBLGdCQUFNSyxPQUFPLEdBQUdsQixFQUFFLENBQUMvM0IsYUFBSCxDQUFpQixtQkFBakIsQ0FBaEIsQ0FGZ0MsQ0FHaEM7O0FBQ0EsZ0JBQU1xQyxNQUFNLEdBQUcsRUFBZjtBQUNBLGdCQUFNNjJCLFdBQVcsR0FBRyxFQUFwQixDQUxnQyxDQU1oQztBQUNBOztBQUNBLGdCQUFNL2hCLFNBQVMsR0FBRzRnQixFQUFFLENBQUNqbkIsT0FBSCxDQUFXcUcsU0FBN0IsQ0FSZ0MsQ0FTaEM7QUFDQTs7QUFDQSxnQkFBS0EsU0FBUyxLQUFLLElBQW5CLEVBQTBCO0FBQ3RCOVUsY0FBQUEsTUFBTSxDQUFDdkIsQ0FBUCxHQUFXLE1BQVg7QUFDQW80QixjQUFBQSxXQUFXLENBQUNwNEIsQ0FBWixHQUFnQixPQUFoQjtBQUNBbzRCLGNBQUFBLFdBQVcsQ0FBQ2ozQixDQUFaLEdBQWdCLElBQWhCO0FBQ0gsYUFKRCxNQUtLLElBQUtrVixTQUFTLEtBQUssSUFBbkIsRUFBMEI7QUFDM0I5VSxjQUFBQSxNQUFNLENBQUN2QixDQUFQLEdBQVcsT0FBWDtBQUNBbzRCLGNBQUFBLFdBQVcsQ0FBQ3A0QixDQUFaLEdBQWdCLE1BQWhCO0FBQ0FvNEIsY0FBQUEsV0FBVyxDQUFDajNCLENBQVosR0FBZ0IsSUFBaEI7QUFDSCxhQUpJLE1BS0EsSUFBS2tWLFNBQVMsS0FBSyxJQUFuQixFQUEwQjtBQUMzQjlVLGNBQUFBLE1BQU0sQ0FBQ0osQ0FBUCxHQUFXLE9BQVg7QUFDQWkzQixjQUFBQSxXQUFXLENBQUNqM0IsQ0FBWixHQUFnQixNQUFoQjtBQUNBSSxjQUFBQSxNQUFNLENBQUN2QixDQUFQLEdBQVcsT0FBWDtBQUdILGFBTkksTUFPQSxJQUFLcVcsU0FBUyxLQUFLLElBQW5CLEVBQTBCO0FBQzNCOVUsY0FBQUEsTUFBTSxDQUFDSixDQUFQLEdBQVcsTUFBWDtBQUNBaTNCLGNBQUFBLFdBQVcsQ0FBQ2ozQixDQUFaLEdBQWdCLE9BQWhCO0FBQ0FJLGNBQUFBLE1BQU0sQ0FBQ3ZCLENBQVAsR0FBVyxPQUFYO0FBR0gsYUFOSSxNQU9BO0FBQ0R1QixjQUFBQSxNQUFNLENBQUNKLENBQVAsR0FBVyxNQUFYO0FBQ0FJLGNBQUFBLE1BQU0sQ0FBQ3ZCLENBQVAsR0FBVyxNQUFYO0FBQ0FvNEIsY0FBQUEsV0FBVyxDQUFDajNCLENBQVosR0FBZ0IsT0FBaEI7QUFDQWkzQixjQUFBQSxXQUFXLENBQUNwNEIsQ0FBWixHQUFnQixPQUFoQjtBQUNIOztBQUVELGdCQUFLNDNCLE1BQU0sS0FBSyxNQUFoQixFQUF5QjtBQUNyQjtBQUNBbDVCLGNBQUFBLFFBQVEsQ0FBQzJJLEdBQVQsQ0FBYTR2QixFQUFiLEVBQWlCMTFCLE1BQWpCO0FBQ0E3QyxjQUFBQSxRQUFRLENBQUMySSxHQUFULENBQWE4d0IsT0FBYixFQUFzQkMsV0FBdEIsRUFIcUIsQ0FLckI7O0FBQ0ExNUIsY0FBQUEsUUFBUSxDQUFDNlcsRUFBVCxDQUFZLENBQUMwaEIsRUFBRCxFQUFJa0IsT0FBSixDQUFaLEVBQTBCLEVBQTFCLEVBQThCO0FBQzFCdjRCLGdCQUFBQSxJQUFJLEVBQUVxNEIsS0FBSyxDQUFDdjNCLE9BRGM7QUFFMUJTLGdCQUFBQSxDQUFDLEVBQUUsSUFGdUI7QUFHMUJuQixnQkFBQUEsQ0FBQyxFQUFFLElBSHVCO0FBSTFCNnlCLGdCQUFBQSxVQUFVLEVBQUU7QUFBQSx5QkFBTWdGLFlBQVksQ0FBQ0MsR0FBRCxDQUFsQjtBQUFBO0FBSmMsZUFBOUI7QUFNSCxhQVpELE1BYUs7QUFDRHA1QixjQUFBQSxRQUFRLENBQUM2VyxFQUFULENBQVkwaEIsRUFBWixFQUFnQixHQUFoQixFQUFxQjtBQUNqQnIzQixnQkFBQUEsSUFBSSxFQUFFbTRCLEtBQUssQ0FBQzkyQixTQURLO0FBRWpCRSxnQkFBQUEsQ0FBQyxFQUFFSSxNQUFNLENBQUNKLENBQVAsSUFBWSxDQUZFO0FBR2pCbkIsZ0JBQUFBLENBQUMsRUFBRXVCLE1BQU0sQ0FBQ3ZCLENBQVAsSUFBWTtBQUhFLGVBQXJCO0FBS0F0QixjQUFBQSxRQUFRLENBQUM2VyxFQUFULENBQVk0aUIsT0FBWixFQUFxQixHQUFyQixFQUEwQjtBQUN0QnY0QixnQkFBQUEsSUFBSSxFQUFFbTRCLEtBQUssQ0FBQzkyQixTQURVO0FBRXRCRSxnQkFBQUEsQ0FBQyxFQUFFaTNCLFdBQVcsQ0FBQ2ozQixDQUFaLElBQWlCLENBRkU7QUFHdEJuQixnQkFBQUEsQ0FBQyxFQUFFbzRCLFdBQVcsQ0FBQ3A0QixDQUFaLElBQWlCLENBSEU7QUFJdEI2eUIsZ0JBQUFBLFVBQVUsRUFBRTtBQUFBLHlCQUFNZ0YsWUFBWSxDQUFDQyxHQUFELENBQWxCO0FBQUE7QUFKVSxlQUExQjtBQU1IO0FBQ0osV0FwRUQ7QUF1RUMsU0ExRUQsQ0FyRVcsQ0FrSlg7OztBQUNBcDVCLFFBQUFBLFFBQVEsQ0FBQzZXLEVBQVQsQ0FBWSxLQUFLMmhCLEdBQUwsQ0FBU0UsU0FBckIsRUFBZ0MsR0FBaEMsRUFBcUM7QUFDakN4M0IsVUFBQUEsSUFBSSxFQUFFZzRCLE1BQU0sS0FBSyxNQUFYLEdBQW9CSyxLQUFLLENBQUN2M0IsT0FBMUIsR0FBb0NxM0IsS0FBSyxDQUFDOTJCLFNBRGY7QUFFakNvM0IsVUFBQUEsT0FBTyxFQUFFVCxNQUFNLEtBQUssTUFBWCxHQUFvQjtBQUFDVSxZQUFBQSxRQUFRLEVBQUU7QUFBWCxXQUFwQixHQUFvQyxJQUZaO0FBR2pDcjRCLFVBQUFBLE9BQU8sRUFBRTIzQixNQUFNLEtBQUssTUFBWCxHQUFvQixDQUFwQixHQUF3QixDQUhBO0FBSWpDVSxVQUFBQSxRQUFRLEVBQUVWLE1BQU0sS0FBSyxNQUFYLEdBQW9CLEdBQXBCLEdBQTBCLEdBSkg7QUFLakNyNEIsVUFBQUEsS0FBSyxFQUFFcTRCLE1BQU0sS0FBSyxNQUFYLEdBQW9CLEdBQXBCLEdBQTBCO0FBTEEsU0FBckM7QUFXSGw1QixRQUFBQSxRQUFRLENBQUM2VyxFQUFULENBQVksS0FBSzJoQixHQUFMLENBQVNDLFFBQXJCLEVBQStCUyxNQUFNLEtBQUssTUFBWCxHQUFvQixHQUFwQixHQUEwQixHQUF6RCxFQUE4RDtBQUNqRDtBQUNBaDRCLFVBQUFBLElBQUksRUFBRWc0QixNQUFNLEtBQUssTUFBWCxHQUFvQkssS0FBSyxDQUFDdjNCLE9BQTFCLEdBQW9DNjNCLElBQUksQ0FBQzczQixPQUZFLENBR2xEOztBQUhrRCxTQUE5RCxFQTlKYyxDQW9LWDs7QUFDQWhDLFFBQUFBLFFBQVEsQ0FBQzg1QixTQUFULENBQW1CLEtBQUt0QixHQUFMLENBQVNTLFNBQTVCLEVBQXVDQyxNQUFNLEtBQUssTUFBWCxHQUFvQixHQUFwQixHQUEwQixHQUFqRSxFQUFzRTtBQUNsRWg0QixVQUFBQSxJQUFJLEVBQUVnNEIsTUFBTSxLQUFLLE1BQVgsR0FBb0JLLEtBQUssQ0FBQ3YzQixPQUExQixHQUFvQ3EzQixLQUFLLENBQUM5MkIsU0FEa0I7QUFFbEVvM0IsVUFBQUEsT0FBTyxFQUFFVCxNQUFNLEtBQUssTUFBWCxHQUFvQjtBQUFDNTNCLFlBQUFBLENBQUMsRUFBRSxLQUFKO0FBQVdDLFlBQUFBLE9BQU8sRUFBRTtBQUFwQixXQUFwQixHQUE2QyxJQUZZO0FBR2xFRCxVQUFBQSxDQUFDLEVBQUU0M0IsTUFBTSxLQUFLLE1BQVgsR0FBb0IsSUFBcEIsR0FBMkIsS0FIb0M7QUFJbEUzM0IsVUFBQUEsT0FBTyxFQUFFMjNCLE1BQU0sS0FBSyxNQUFYLEdBQW9CLENBQXBCLEdBQXdCO0FBSmlDLFNBQXRFLEVBS0dBLE1BQU0sS0FBSyxNQUFYLEdBQW9CLEdBQXBCLEdBQTBCLENBQUMsR0FMOUI7QUFTSDtBQXpOVDs7QUFBQTtBQUFBLE9BMk5DOzs7QUFDRyxNQUFNNUcsSUFBSSxHQUFHLElBQUlnRyxJQUFKLENBQVMvNUIsUUFBUSxDQUFDaUMsYUFBVCxDQUF1QixVQUF2QixDQUFULENBQWI7QUFDQSxNQUFJdTVCLFNBQVMsR0FBRyxJQUFoQjtBQUdIOzs7QUMxT0Q7Ozs7Ozs7QUFPQSxDQUFFLFlBQVc7QUFDWixNQUFJQyxJQUFJLEdBQUcsa0JBQWtCcnlCLElBQWxCLENBQXdCbU8sU0FBUyxDQUFDbWtCLFNBQWxDLENBQVg7O0FBRUEsTUFBS0QsSUFBSSxJQUFJejdCLFFBQVEsQ0FBQ3dILGNBQWpCLElBQW1DNUQsTUFBTSxDQUFDK0UsZ0JBQS9DLEVBQWtFO0FBQ2pFL0UsSUFBQUEsTUFBTSxDQUFDK0UsZ0JBQVAsQ0FBeUIsWUFBekIsRUFBdUMsWUFBVztBQUNqRCxVQUFJMEIsRUFBRSxHQUFHZ3NCLFFBQVEsQ0FBQ3NGLElBQVQsQ0FBY0MsU0FBZCxDQUF5QixDQUF6QixDQUFUO0FBQUEsVUFDQ3RULE9BREQ7O0FBR0EsVUFBSyxDQUFJLGdCQUFnQmxmLElBQWhCLENBQXNCaUIsRUFBdEIsQ0FBVCxFQUF3QztBQUN2QztBQUNBOztBQUVEaWUsTUFBQUEsT0FBTyxHQUFHdG9CLFFBQVEsQ0FBQ3dILGNBQVQsQ0FBeUI2QyxFQUF6QixDQUFWOztBQUVBLFVBQUtpZSxPQUFMLEVBQWU7QUFDZCxZQUFLLENBQUksd0NBQXdDbGYsSUFBeEMsQ0FBOENrZixPQUFPLENBQUN0UCxPQUF0RCxDQUFULEVBQTZFO0FBQzVFc1AsVUFBQUEsT0FBTyxDQUFDdVQsUUFBUixHQUFtQixDQUFDLENBQXBCO0FBQ0E7O0FBRUR2VCxRQUFBQSxPQUFPLENBQUNwVyxLQUFSO0FBQ0E7QUFDRCxLQWpCRCxFQWlCRyxLQWpCSDtBQWtCQTtBQUNELENBdkJEIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbImpRdWVyeShkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oJCkge1xuXG4gIHNjcm9sbEFuaW0oKVxufSk7XG5cblxuXG5cbmZ1bmN0aW9uIHNjcm9sbEFuaW0oKSB7XG5cbiAgICAvLyBpbml0IFNjcm9sbE1hZ2ljXG4gICAgdmFyIHNjcm9sbE1hZ2ljID0gbmV3IFNjcm9sbE1hZ2ljLkNvbnRyb2xsZXIoKTtcblxuICAgIC8vIFNjYWxlIEFuaW1hdGlvbiBTZXR1cFxuICAgIC8vIC50bygnQHRhcmdldCcsIEBsZW5ndGgsIHtAb2JqZWN0fSlcbiAgICAvLyB2YXIgc2NhbGVfdHdlZW4gPSBUd2Vlbk1heC50bygnI3NjYWxlLWFuaW1hdGlvbicsIDEsIHtcbiAgICAvLyAgIHRyYW5zZm9ybTogJ3NjYWxlKC43NSknLFxuICAgIC8vICAgZWFzZTogTGluZWFyLmVhc2VOb25lXG4gICAgLy8gfSk7XG5cbiAgICAvLyAvLyBCRyBBbmltYXRpb24gU2V0dXBcbiAgICAvLyAvLyAudG8oJ0B0YXJnZXQnLCBAbGVuZ3RoLCB7QG9iamVjdH0pXG4gICAgLy8gdmFyIGJnX3R3ZWVuID0gVHdlZW5NYXgudG8oJyNiZy10cmlnZ2VyJywgMSwge1xuICAgIC8vICAgYmFja2dyb3VuZENvbG9yOiAnI0ZGQTUwMCcsXG4gICAgLy8gICBlYXNlOiBMaW5lYXIuZWFzZU5vbmVcbiAgICAvLyB9KTtcblxuICAgIC8vIC8vIFlvWW8gQW5pbWF0aW9uIFNldHVwXG4gICAgLy8gLy8gLnRvKEB0YXJnZXQsIEBsZW5ndGgsIEBvYmplY3QpXG4gICAgLy8gdmFyIHlveW9fdHdlZW4gPSBUd2Vlbk1heC50bygnI3lveW8tYW5pbWF0aW9uJywgMSwge1xuICAgIC8vICAgdHJhbnNmb3JtOiAnc2NhbGUoMiknLFxuICAgIC8vICAgZWFzZTogQ3ViaWMuZWFzZU91dCxcbiAgICAvLyAgIHJlcGVhdDogLTEsIC8vIHRoaXMgbmVnYXRpdmUgdmFsdWUgcmVwZWF0cyB0aGUgYW5pbWF0aW9uXG4gICAgLy8gICB5b3lvOiB0cnVlIC8vIG1ha2UgaXQgYm91bmNl4oCmeW8hXG4gICAgLy8gfSk7XG5cbiAgICAvLyBSZXZlYWxcbiAgICB2YXIgcmV2ZWFsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnJldmVhbC1lZmZlY3QnKTtcblxuICAgICQuZWFjaChyZXZlYWwsIGZ1bmN0aW9uKGluZGV4LCByZXZlYWxfaXRlbSkge1xuICAgICAgICAvLyBpZiAoIWlzTW9iaWxlKSB7XG4gICAgICAgICAgbmV3IFNjcm9sbE1hZ2ljLlNjZW5lKHtcbiAgICAgICAgICAgIHRyaWdnZXJFbGVtZW50OiByZXZlYWxfaXRlbSxcbiAgICAgICAgICAgIHRyaWdnZXJIb29rOiAnb25FbnRlcicsXG4gICAgICAgICAgICByZXZlcnNlOiBmYWxzZSxcbiAgICAgICAgICAgIGR1cmF0aW9uOiAwLFxuICAgICAgICAgICAgb2Zmc2V0OiAyXG4gICAgICAgICAgfSlcbiAgICAgICAgICAuc2V0Q2xhc3NUb2dnbGUocmV2ZWFsX2l0ZW0sICdhbmltYXRlJylcbiAgICAgICAgICAuYWRkVG8oc2Nyb2xsTWFnaWMpO1xuICAgICAgICAvLyB9XG4gICAgICB9KTtcblxuXG4gICAgLy8gVW5mb2xkXG4gICAgdmFyIHVuZm9sZCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy51bmZvbGQnKTtcblxuICAgICQuZWFjaCh1bmZvbGQsIGZ1bmN0aW9uKGluZGV4LCByZXZlYWxfaXRlbSkge1xuICAgICAgICAvLyBpZiAoIWlzTW9iaWxlKSB7XG4gICAgICAgICAgbmV3IFNjcm9sbE1hZ2ljLlNjZW5lKHtcbiAgICAgICAgICAgIHRyaWdnZXJFbGVtZW50OiByZXZlYWxfaXRlbSxcbiAgICAgICAgICAgIHRyaWdnZXJIb29rOiAnb25FbnRlcicsXG4gICAgICAgICAgICByZXZlcnNlOiBmYWxzZSxcbiAgICAgICAgICAgIG9mZnNldDogMCxcbiAgICAgICAgICAgIGR1cmF0aW9uOiAwXG4gICAgICAgICAgfSlcbiAgICAgICAgICAuc2V0Q2xhc3NUb2dnbGUocmV2ZWFsX2l0ZW0sICdhbmltYXRlJylcbiAgICAgICAgICAuYWRkVG8oc2Nyb2xsTWFnaWMpO1xuICAgICAgICAvLyB9XG4gICAgICB9KTtcblxuXG4gICAgdmFyICRiZ19zd2l0Y2ggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuYmctc3dpdGNoJyk7XG5cbiAgICAkLmVhY2goJGJnX3N3aXRjaCwgZnVuY3Rpb24oaW5kZXgsIHJldmVhbF9pdGVtKSB7XG4gICAgICB2YXIgY2hpbGQgPSAkKHRoaXMpLmF0dHIoXCJkYXRhLWJnLWNvbG9yXCIpO1xuICAvLyAgdmFyIHByZXYgPSAkYmdfc3dpdGNoW2luZGV4XS5wcmV2aW91c1NpYmxpbmcoKTtcblxuICB2YXIgdGhpc3R3ZWVuID0gVHdlZW5NYXguZnJvbVRvKCRiZ19zd2l0Y2hbaW5kZXhdLCAxLCB7XG4gICBiYWNrZ3JvdW5kQ29sb3I6JyNmZmYnLFxuICAgfSx7XG4gICAgIGJhY2tncm91bmRDb2xvcjpjaGlsZCxcbiAgICAgaW1tZWRpYXRlUmVuZGVyOiBmYWxzZVxuICAgfSk7XG4gICAgICAgICAvLyBtYWtlIHNjZW5lXG4gICAgICAgICBuZXcgU2Nyb2xsTWFnaWMuU2NlbmUoe1xuICAgICAgICAgICB0cmlnZ2VyRWxlbWVudDogIHRoaXMsXG4gICAgICAgICAgIG9mZnNldDogMCxcbiAgICAgICAgICAgZHVyYXRpb246ICcxMDAlJyxcbiAgICAgICAgICAgdHJpZ2dlckhvb2s6ICdvbkVudGVyJyxcblxuICAgICAgICAgfSlcbiAgICAgICAgIC5zZXRUd2Vlbih0aGlzdHdlZW4pXG4gICAgICAgICAuYWRkVG8oc2Nyb2xsTWFnaWMpO1xuXG4gICAgICAgfSk7XG5cblxuICAgIHZhciAkaGVhZGluZyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5zZWN0aW9uX19oZWFkaW5nJyk7XG4gICAgJC5lYWNoKCRoZWFkaW5nLCBmdW5jdGlvbihpLCByZXZlYWxfaXRlbSkge1xuXG5cbiAgICAgIHZhciBzcGxpdG9uZSA9ICRoZWFkaW5nW2ldLnF1ZXJ5U2VsZWN0b3IoJ2gzJyk7XG5cbiAgICAgIHZhciBzcGxpdHR3byA9ICRoZWFkaW5nW2ldLnF1ZXJ5U2VsZWN0b3IoJ3NwYW4jc2VjdGlvbi10aXRsZScpO1xuXG4gICAgICB2YXIgc3BsaXR0aHJlZSA9ICRoZWFkaW5nW2ldLnF1ZXJ5U2VsZWN0b3IoJ2RpdicpO1xuXG4gICAgICB2YXIgdHdlZW5MaW5lID0gbmV3IFRpbWVsaW5lTWF4KHtcbiAgICAgICAgZGVsYXk6IDAuM1xuICAgICAgfSk7XG5cbiAgICAgXHQvL2lmICghaXNNb2JpbGUpIHtcblxuICAgICAgICB2YXIgJHdvcmQgPSAkKFwicGF0aCNoLWxpbmVcIik7XG4gICAgICAgIHZhciAkZG90ID0gJChcInBhdGgjaC1zcXVhcmVcIik7XG5cbiAgICAgICAgLy8gcHJlcGFyZSBTVkdcbiAgICAgICAgcGF0aFByZXBhcmUoJHdvcmQpO1xuICAgICAgICBwYXRoUHJlcGFyZSgkZG90KTtcblxuXG5cbiAgICAgICAgdHdlZW5MaW5lLmZyb21Ubygkd29yZCwgMC45LHtzdHJva2VEYXNob2Zmc2V0OiAwfSwgeyBlYXNlOkxpbmVhci5lYXNlTm9uZSwgaW1tZWRpYXRlUmVuZGVyOiBmYWxzZX0sIDEpO1xuICAgICAgICB0d2VlbkxpbmUuZnJvbVRvKCRkb3QsIDAuMSwge3N0cm9rZURhc2hvZmZzZXQ6IDB9LCB7ICBlYXNlOkxpbmVhci5lYXNlTm9uZSwgaW1tZWRpYXRlUmVuZGVyOiBmYWxzZX0sIDEpO1xuXG4gICAgICAgIC8vIHR3ZWVuTGluZS5zdGFnZ2VyRnJvbShzcGxpdG9uZSwgLjYsIHtcbiAgICAgICAgLy8gICB5OiAxMDAsXG4gICAgICAgIC8vICAgb3BhY2l0eTogMCxcbiAgICAgICAgLy8gICBlYXNlOiAnQ2lyYy5lYXNlT3V0J1xuICAgICAgICAvLyB9LCAwLjIpO1xuXG4gICAgICAgIHR3ZWVuTGluZS5zdGFnZ2VyRnJvbShzcGxpdHR3bywgMS42LCB7XG4gICAgICAgICAgeTogNTAsXG4gICAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgICBlYXNlOiAnQ2lyYy5lYXNlT3V0JyxcblxuICAgICAgICB9LCAwLjcyKTtcblxuICAgICAgICB0d2VlbkxpbmUuc3RhZ2dlckZyb20oc3BsaXR0aHJlZSwgLjYsIHtcbiAgICAgICAgICB5OiA1MCxcbiAgICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICAgIGVhc2U6ICdFbGFzdGljLmVhc2VPdXQnLFxuXG4gICAgICAgIH0sIDAuOTIpO1xuXG5cblxuICAgICAgICBuZXcgU2Nyb2xsTWFnaWMuU2NlbmUoe1xuICAgICAgICAgIHRyaWdnZXJFbGVtZW50OiB0aGlzLFxuICAgICAgICAgIHRyaWdnZXJIb29rOiAnb25FbnRlcicsXG4gICAgICAgICAgcmV2ZXJzZTogZmFsc2UsXG4gICAgICAgICAgb2Zmc2V0OiAxMDAsXG4gICAgICAgICAgZHVyYXRpb246IDIwMCwgdHdlZW5DaGFuZ2VzOiB0cnVlXG4gICAgICAgIH0pXG4gICAgICAgIC5zZXRUd2Vlbih0d2VlbkxpbmUpXG4gICAgICAgIC5zZXRDbGFzc1RvZ2dsZSgkaGVhZGluZ1tpXSwgJ2FuaW1hdGUnKVxuICAgICAgICAuYWRkVG8oc2Nyb2xsTWFnaWMpO1xuXG4gICAgICB9KTtcblxuXG5cblxuXG4gICAgLyoqIEZhZGUgQW5pbWF0aW9uICoqL1xuXG4gICAgdmFyIGFsbF9hbmltYXRpb25zID0gWydmYWRlSW5VcCcsICdib3VuY2VJblVwJ107XG4gICAgJC5lYWNoKGFsbF9hbmltYXRpb25zLCBmdW5jdGlvbihpbmRleCwgYW5pbWF0ZSkge1xuICAgICB2YXIgJGZhZGVJbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy4nK2FsbF9hbmltYXRpb25zW2luZGV4XSk7XG4gICAgICQuZWFjaCgkZmFkZUluLCBmdW5jdGlvbihpLCBpdGVtKSB7XG5cbiAgICAgIG5ldyBTY3JvbGxNYWdpYy5TY2VuZSh7XG4gICAgICAgIHRyaWdnZXJFbGVtZW50OiB0aGlzLFxuICAgICAgICB0cmlnZ2VySG9vazogJ29uRW50ZXInLFxuICAgICAgICByZXZlcnNlOiBmYWxzZSxcbiAgICAgICAgb2Zmc2V0OiAwLFxuICAgICAgICBkdXJhdGlvbjogMjBcbiAgICAgIH0pXG4gICAgICAuc2V0Q2xhc3NUb2dnbGUoaXRlbSwgJ2FuaW1hdGVkJylcbiAgICAgIC5hZGRUbyhzY3JvbGxNYWdpYyk7XG5cbiAgICB9KTtcbiAgIH0pO1xuXG5cbiAgICB2YXIgJHNsaWRlUmlnaHQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuc2xpZGVJblJpZ2h0Jyk7XG4gICAgJC5lYWNoKCRzbGlkZVJpZ2h0LCBmdW5jdGlvbihpLCBpdGVtKSB7XG5cblxuXG4gICAgIHZhciB0d2VlbkZhZGUgPSBuZXcgVHdlZW5NYXguZnJvbVRvKGl0ZW0sIDEsIHtvcGFjaXR5OiAwLCB5OiA1MH0sIHtcbiAgICAgIG9wYWNpdHk6IDEsXG4gICAgICB5OiAwLFxuICAgICAgZWFzZTogRWxhc3RpYy5lYXNlT3V0XG4gICAgfSk7XG5cbiAgICAgbmV3IFNjcm9sbE1hZ2ljLlNjZW5lKHtcbiAgICAgICB0cmlnZ2VyRWxlbWVudDogdGhpcyxcbiAgICAgICB0cmlnZ2VySG9vazogJ29uRW50ZXInLFxuICAgICAgIHJldmVyc2U6IGZhbHNlLFxuICAgICAgIG9mZnNldDogMCxcbiAgICAgICBkdXJhdGlvbjogMTVcbiAgICAgfSlcbiAgICAgICAgICAgICAgICAgXHQvLy5zZXRDbGFzc1RvZ2dsZShpdGVtLCAnYW5pbWF0ZWQnKVxuXG4gICAgICAgICAgICAgICAgICAgLnNldFR3ZWVuKHR3ZWVuRmFkZSlcbiAgICAgICAgICAgICAgICAgICAuYWRkVG8oc2Nyb2xsTWFnaWMpO1xuXG4gICAgICAgICAgICAgICAgIH0pO1xuXG5cblxuXG4gICAgLyoqIFNsaWRpbmcgVGV4dCAqKi9cblxuICAgIHZhciAkdHRfaCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy50dC1oJyk7XG4gICAgdmFyIGR1cmF0aW9uID0gODAwMDtcbiAgICB2YXIgaGVyZSA9IChkdXJhdGlvbiAtIHdpbmRvdy5pbm5lcldpZHRoKTtcblxuICAgICQuZWFjaCgkdHRfaCwgZnVuY3Rpb24oaW5kZXgsIHJldmVhbF9pdGVtKSB7XG5cbiAgICAgbmV3IFNjcm9sbE1hZ2ljLlNjZW5lKHtcbiAgICAgICB0cmlnZ2VyRWxlbWVudDogICcuc2VjdGlvbi1wb3J0Zm9saW8nLFxuICAgICAgIHRyaWdnZXJIb29rOiAnb25MZWF2ZScsXG4gICAgICAgb2Zmc2V0OiAwLFxuICAgICAgIGR1cmF0aW9uOiAyNTAwLFxuICAgICAgIHJldmVyc2U6IHRydWVcblxuXG4gICAgIH0pXG4gICAgIC5zZXRUd2VlbihUd2Vlbk1heC5mcm9tVG8ocmV2ZWFsX2l0ZW0sIDEsIHtyaWdodDogJy0xNTB2dyd9LCB7cmlnaHQ6IDMwLCBpbW1lZGlhdGVSZW5kZXI6IGZhbHNlLCBlYXNlOiBQb3dlcjQuZWFzZUluT3V0fSkpXG5cbiAgICAgLy8gIC5hZGRJbmRpY2F0b3JzKHRydWUpXG4gICAgIC5hZGRUbyhzY3JvbGxNYWdpYyk7XG5cbiAgIH0pO1xuXG5cbiAgICAvKiogU2xpZGVMZWZ0SW4gKiovXG5cblxuICAgIHZhciAkc2xpZGVJbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5zbGlkZWluJyk7XG4gICAgJC5lYWNoKCRzbGlkZUluLCBmdW5jdGlvbihpLCBpdGVtKSB7XG5cblxuICAgICAgbmV3IFNjcm9sbE1hZ2ljLlNjZW5lKHtcbiAgICAgICAgdHJpZ2dlckVsZW1lbnQ6IHRoaXMsXG4gICAgICAgIHRyaWdnZXJIb29rOiAnb25FbnRlcicsXG4gICAgICAgIHJldmVyc2U6IGZhbHNlLFxuICAgICAgICBvZmZzZXQ6IDAsXG4gICAgICAgIGR1cmF0aW9uOiAwLFxuXG4gICAgICB9KVxuXG4gICAgICAuc2V0VHdlZW4oVHdlZW5NYXguZnJvbVRvKGl0ZW0sIDEuMiwge3g6LTEwMCwgb3BhY2l0eTogMH0sIHt4OicwJywgb3BhY2l0eTogMSAsZWFzZTpFbGFzdGljLmVhc2VJbk91dCwgaW1tZWRpYXRlUmVuZGVyOiB0cnVlfSwnLT0xLjgnKSkgXG4gICAgICAuYWRkVG8oc2Nyb2xsTWFnaWMpO1xuXG4gICAgfSk7XG5cblxuICAgIC8qKiBTY3JvbGwgVXAgLyBEb3duICoqL1xuXG4gICAgdmFyIHNjcm9sbEFuaW1hdGlvbnMgPSBbJ3Njcm9sbFVwJywgJ3Njcm9sbERvd24nXTtcblxuICAgICQuZWFjaChzY3JvbGxBbmltYXRpb25zLCBmdW5jdGlvbihpLCBpdGVtKSB7XG4gICAgICBjb25zdCAkZWZmZWN0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLicraXRlbSk7XG5cbiAgICAgIGlmKCRlZmZlY3QpIHtcblxuICAgICAgY29uc3QgaXRlbXMgPSAkZWZmZWN0LnF1ZXJ5U2VsZWN0b3JBbGwoJ3NwYW4nKTtcblxuXG4gICAgICAkLmVhY2goaXRlbXMsIGZ1bmN0aW9uKGksIGFuaW1hdGUpIHtcblxuXG4gICAgICAgIGNvbnN0IGNvbmZpZyA9IHt9O1xuXG5cblxuICAgICAgICBpZiAoaXRlbSA9PSAnc2Nyb2xsVXAnKXtcbiAgICAgICAgICBjb25maWcueVN0YXJ0ID0gLTYwO1xuICAgICAgICAgIGNvbmZpZy55RW5kID0gLTUwMDtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBjb25maWcueVN0YXJ0ID0gLTMwO1xuICAgICAgICAgIGNvbmZpZy55RW5kID0gNTAwO1xuXG4gICAgICAgIH1cblxuXG4gICAgXG5cblxuICAgICAgICBuZXcgU2Nyb2xsTWFnaWMuU2NlbmUoe1xuICAgICAgICAgICAgICB0cmlnZ2VyRWxlbWVudDogKCcuc2VnbWVudGVkLWNvbnRyb2wnKSxcbiAgICAgICAgICAgICAgdHJpZ2dlckhvb2s6ICAnb25FbnRlcicsXG4gICAgICAgICAgICAgIHJldmVyc2U6IHRydWUsXG4gICAgICAgICAgICAgIG9mZnNldDogNTAsXG4gICAgICAgICAgICAgIGR1cmF0aW9uOiAnMTAwJScsXG5cbiAgICAgICB9KVxuICAgICAgICAuc2V0VHdlZW4oIFR3ZWVuTWF4LmZyb21Ubyh0aGlzLCAwLjUsIHsgeDogY29uZmlnLnlTdGFydH0sIHtcbiAgICAgICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgICAgICB4OiBjb25maWcueUVuZCxcbiAgICAgICAgICAgIGVhc2U6IFBvd2VyMi5lYXNlSW5PdXRcbiAgICAgICAgICB9KSlcbiAgICAgLy8gIC5zZXRQaW4odGhpcylcbiAgICAgIC5hZGRJbmRpY2F0b3JzKHsgbmFtZTogXCJIaSAoZHVyYXRpb246IDUwMClcIisgY29uZmlnLm9mZnNldCB9KVxuICAgICAgLmFkZFRvKHNjcm9sbE1hZ2ljKTtcblxuXG5cbiAgICAgIH0pOyAgXG5cbn1cblxuICAgIH0pO1xuXG5cblxuXG5cbiAgfVxuXG5cblxuICBmdW5jdGlvbiBwYXRoUHJlcGFyZSAoJGVsKSB7XG4gICAgICB2YXIgbGluZUxlbmd0aCA9ICRlbFswXS5nZXRUb3RhbExlbmd0aCgpO1xuICAgICAgJGVsLmNzcyhcInN0cm9rZS1kYXNoYXJyYXlcIiwgbGluZUxlbmd0aCk7XG4gICAgICAkZWwuY3NzKFwic3Ryb2tlLWRhc2hvZmZzZXRcIiwgbGluZUxlbmd0aCk7XG4gICAgfVxuXG4iLCIvKiFcbiAgKiBCb290c3RyYXAgdjQuMy4xIChodHRwczovL2dldGJvb3RzdHJhcC5jb20vKVxuICAqIENvcHlyaWdodCAyMDExLTIwMjAgVGhlIEJvb3RzdHJhcCBBdXRob3JzIChodHRwczovL2dpdGh1Yi5jb20vdHdicy9ib290c3RyYXAvZ3JhcGhzL2NvbnRyaWJ1dG9ycylcbiAgKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90d2JzL2Jvb3RzdHJhcC9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICAqL1xuIWZ1bmN0aW9uKHQsZSl7XCJvYmplY3RcIj09dHlwZW9mIGV4cG9ydHMmJlwidW5kZWZpbmVkXCIhPXR5cGVvZiBtb2R1bGU/bW9kdWxlLmV4cG9ydHM9ZShyZXF1aXJlKFwicG9wcGVyLmpzXCIpKTpcImZ1bmN0aW9uXCI9PXR5cGVvZiBkZWZpbmUmJmRlZmluZS5hbWQ/ZGVmaW5lKFtcInBvcHBlci5qc1wiXSxlKToodD10fHxzZWxmKS5ib290c3RyYXA9ZSh0LlBvcHBlcil9KHRoaXMsKGZ1bmN0aW9uKHQpe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIGUodCxlKXtmb3IodmFyIG49MDtuPGUubGVuZ3RoO24rKyl7dmFyIGk9ZVtuXTtpLmVudW1lcmFibGU9aS5lbnVtZXJhYmxlfHwhMSxpLmNvbmZpZ3VyYWJsZT0hMCxcInZhbHVlXCJpbiBpJiYoaS53cml0YWJsZT0hMCksT2JqZWN0LmRlZmluZVByb3BlcnR5KHQsaS5rZXksaSl9fWZ1bmN0aW9uIG4odCxuLGkpe3JldHVybiBuJiZlKHQucHJvdG90eXBlLG4pLGkmJmUodCxpKSx0fWZ1bmN0aW9uIGkodCxlLG4pe3JldHVybiBlIGluIHQ/T2JqZWN0LmRlZmluZVByb3BlcnR5KHQsZSx7dmFsdWU6bixlbnVtZXJhYmxlOiEwLGNvbmZpZ3VyYWJsZTohMCx3cml0YWJsZTohMH0pOnRbZV09bix0fWZ1bmN0aW9uIG8odCxlKXt2YXIgbj1PYmplY3Qua2V5cyh0KTtpZihPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKXt2YXIgaT1PYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHQpO2UmJihpPWkuZmlsdGVyKChmdW5jdGlvbihlKXtyZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0LGUpLmVudW1lcmFibGV9KSkpLG4ucHVzaC5hcHBseShuLGkpfXJldHVybiBufWZ1bmN0aW9uIHIodCl7Zm9yKHZhciBlPTE7ZTxhcmd1bWVudHMubGVuZ3RoO2UrKyl7dmFyIG49bnVsbCE9YXJndW1lbnRzW2VdP2FyZ3VtZW50c1tlXTp7fTtlJTI/byhuLCEwKS5mb3JFYWNoKChmdW5jdGlvbihlKXtpKHQsZSxuW2VdKX0pKTpPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycz9PYmplY3QuZGVmaW5lUHJvcGVydGllcyh0LE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKG4pKTpvKG4pLmZvckVhY2goKGZ1bmN0aW9uKGUpe09iamVjdC5kZWZpbmVQcm9wZXJ0eSh0LGUsT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihuLGUpKX0pKX1yZXR1cm4gdH10PXQmJnQuaGFzT3duUHJvcGVydHkoXCJkZWZhdWx0XCIpP3QuZGVmYXVsdDp0O3ZhciBzLGEsbCxjPVwidHJhbnNpdGlvbmVuZFwiLHU9ZnVuY3Rpb24odCl7ZG97dCs9fn4oMWU2Kk1hdGgucmFuZG9tKCkpfXdoaWxlKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHQpKTtyZXR1cm4gdH0sZj1mdW5jdGlvbih0KXt2YXIgZT10LmdldEF0dHJpYnV0ZShcImRhdGEtdGFyZ2V0XCIpO2lmKCFlfHxcIiNcIj09PWUpe3ZhciBuPXQuZ2V0QXR0cmlidXRlKFwiaHJlZlwiKTtlPW4mJlwiI1wiIT09bj9uLnRyaW0oKTpudWxsfXJldHVybiBlfSxoPWZ1bmN0aW9uKHQpe3ZhciBlPWYodCk7cmV0dXJuIGUmJmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZSk/ZTpudWxsfSxkPWZ1bmN0aW9uKHQpe3ZhciBlPWYodCk7cmV0dXJuIGU/ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlKTpudWxsfSxnPWZ1bmN0aW9uKHQpe2lmKCF0KXJldHVybiAwO3ZhciBlPXdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKHQpLG49ZS50cmFuc2l0aW9uRHVyYXRpb24saT1lLnRyYW5zaXRpb25EZWxheSxvPXBhcnNlRmxvYXQobikscj1wYXJzZUZsb2F0KGkpO3JldHVybiBvfHxyPyhuPW4uc3BsaXQoXCIsXCIpWzBdLGk9aS5zcGxpdChcIixcIilbMF0sMWUzKihwYXJzZUZsb2F0KG4pK3BhcnNlRmxvYXQoaSkpKTowfSxwPWZ1bmN0aW9uKHQpe3ZhciBlPWRvY3VtZW50LmNyZWF0ZUV2ZW50KFwiSFRNTEV2ZW50c1wiKTtlLmluaXRFdmVudChjLCEwLCEwKSx0LmRpc3BhdGNoRXZlbnQoZSl9LF89ZnVuY3Rpb24odCl7cmV0dXJuKHRbMF18fHQpLm5vZGVUeXBlfSxtPWZ1bmN0aW9uKHQsZSl7dmFyIG49ITEsaT1lKzU7dC5hZGRFdmVudExpc3RlbmVyKGMsKGZ1bmN0aW9uIGUoKXtuPSEwLHQucmVtb3ZlRXZlbnRMaXN0ZW5lcihjLGUpfSkpLHNldFRpbWVvdXQoKGZ1bmN0aW9uKCl7bnx8cCh0KX0pLGkpfSx2PWZ1bmN0aW9uKHQsZSxuKXtPYmplY3Qua2V5cyhuKS5mb3JFYWNoKChmdW5jdGlvbihpKXt2YXIgbyxyPW5baV0scz1lW2ldLGE9cyYmXyhzKT9cImVsZW1lbnRcIjoobz1zLHt9LnRvU3RyaW5nLmNhbGwobykubWF0Y2goL1xccyhbYS16XSspL2kpWzFdLnRvTG93ZXJDYXNlKCkpO2lmKCFuZXcgUmVnRXhwKHIpLnRlc3QoYSkpdGhyb3cgbmV3IEVycm9yKHQudG9VcHBlckNhc2UoKSsnOiBPcHRpb24gXCInK2krJ1wiIHByb3ZpZGVkIHR5cGUgXCInK2ErJ1wiIGJ1dCBleHBlY3RlZCB0eXBlIFwiJytyKydcIi4nKX0pKX0sRT1mdW5jdGlvbih0KXtyZXR1cm4gdD9bXS5zbGljZS5jYWxsKHQpOltdfSx5PWZ1bmN0aW9uKHQpe2lmKCF0KXJldHVybiExO2lmKHQuc3R5bGUmJnQucGFyZW50Tm9kZSYmdC5wYXJlbnROb2RlLnN0eWxlKXt2YXIgZT1nZXRDb21wdXRlZFN0eWxlKHQpLG49Z2V0Q29tcHV0ZWRTdHlsZSh0LnBhcmVudE5vZGUpO3JldHVyblwibm9uZVwiIT09ZS5kaXNwbGF5JiZcIm5vbmVcIiE9PW4uZGlzcGxheSYmXCJoaWRkZW5cIiE9PWUudmlzaWJpbGl0eX1yZXR1cm4hMX0sYj1mdW5jdGlvbigpe3JldHVybiBmdW5jdGlvbigpe319LEQ9ZnVuY3Rpb24odCl7cmV0dXJuIHQub2Zmc2V0SGVpZ2h0fSxUPWZ1bmN0aW9uKCl7dmFyIHQ9d2luZG93LmpRdWVyeTtyZXR1cm4gdCYmIWRvY3VtZW50LmJvZHkuaGFzQXR0cmlidXRlKFwiZGF0YS1uby1qcXVlcnlcIik/dDpudWxsfSxJPShzPXt9LGE9MSx7c2V0OmZ1bmN0aW9uKHQsZSxuKXtcInVuZGVmaW5lZFwiPT10eXBlb2YgdC5rZXkmJih0LmtleT17a2V5OmUsaWQ6YX0sYSsrKSxzW3Qua2V5LmlkXT1ufSxnZXQ6ZnVuY3Rpb24odCxlKXtpZighdHx8XCJ1bmRlZmluZWRcIj09dHlwZW9mIHQua2V5KXJldHVybiBudWxsO3ZhciBuPXQua2V5O3JldHVybiBuLmtleT09PWU/c1tuLmlkXTpudWxsfSxkZWxldGU6ZnVuY3Rpb24odCxlKXtpZihcInVuZGVmaW5lZFwiIT10eXBlb2YgdC5rZXkpe3ZhciBuPXQua2V5O24ua2V5PT09ZSYmKGRlbGV0ZSBzW24uaWRdLGRlbGV0ZSB0LmtleSl9fX0pLEE9e3NldERhdGE6ZnVuY3Rpb24odCxlLG4pe0kuc2V0KHQsZSxuKX0sZ2V0RGF0YTpmdW5jdGlvbih0LGUpe3JldHVybiBJLmdldCh0LGUpfSxyZW1vdmVEYXRhOmZ1bmN0aW9uKHQsZSl7SS5kZWxldGUodCxlKX19LFM9RWxlbWVudC5wcm90b3R5cGUsdz1TLm1hdGNoZXMsQz1TLmNsb3Nlc3QsTD1FbGVtZW50LnByb3RvdHlwZS5xdWVyeVNlbGVjdG9yQWxsLE89RWxlbWVudC5wcm90b3R5cGUucXVlcnlTZWxlY3RvcixOPWZ1bmN0aW9uKHQsZSl7cmV0dXJuIG5ldyBDdXN0b21FdmVudCh0LGUpfTtpZihcImZ1bmN0aW9uXCIhPXR5cGVvZiB3aW5kb3cuQ3VzdG9tRXZlbnQmJihOPWZ1bmN0aW9uKHQsZSl7ZT1lfHx7YnViYmxlczohMSxjYW5jZWxhYmxlOiExLGRldGFpbDpudWxsfTt2YXIgbj1kb2N1bWVudC5jcmVhdGVFdmVudChcIkN1c3RvbUV2ZW50XCIpO3JldHVybiBuLmluaXRDdXN0b21FdmVudCh0LGUuYnViYmxlcyxlLmNhbmNlbGFibGUsZS5kZXRhaWwpLG59KSwhKChsPWRvY3VtZW50LmNyZWF0ZUV2ZW50KFwiQ3VzdG9tRXZlbnRcIikpLmluaXRFdmVudChcIkJvb3RzdHJhcFwiLCEwLCEwKSxsLnByZXZlbnREZWZhdWx0KCksbC5kZWZhdWx0UHJldmVudGVkKSl7dmFyIGs9RXZlbnQucHJvdG90eXBlLnByZXZlbnREZWZhdWx0O0V2ZW50LnByb3RvdHlwZS5wcmV2ZW50RGVmYXVsdD1mdW5jdGlvbigpe3RoaXMuY2FuY2VsYWJsZSYmKGsuY2FsbCh0aGlzKSxPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcyxcImRlZmF1bHRQcmV2ZW50ZWRcIix7Z2V0OmZ1bmN0aW9uKCl7cmV0dXJuITB9LGNvbmZpZ3VyYWJsZTohMH0pKX19dmFyIFA9ZnVuY3Rpb24oKXt2YXIgdD1OKFwiQm9vdHN0cmFwXCIse2NhbmNlbGFibGU6ITB9KSxlPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7cmV0dXJuIGUuYWRkRXZlbnRMaXN0ZW5lcihcIkJvb3RzdHJhcFwiLChmdW5jdGlvbigpe3JldHVybiBudWxsfSkpLHQucHJldmVudERlZmF1bHQoKSxlLmRpc3BhdGNoRXZlbnQodCksdC5kZWZhdWx0UHJldmVudGVkfSgpO3d8fCh3PUVsZW1lbnQucHJvdG90eXBlLm1zTWF0Y2hlc1NlbGVjdG9yfHxFbGVtZW50LnByb3RvdHlwZS53ZWJraXRNYXRjaGVzU2VsZWN0b3IpLEN8fChDPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXM7ZG97aWYody5jYWxsKGUsdCkpcmV0dXJuIGU7ZT1lLnBhcmVudEVsZW1lbnR8fGUucGFyZW50Tm9kZX13aGlsZShudWxsIT09ZSYmMT09PWUubm9kZVR5cGUpO3JldHVybiBudWxsfSk7dmFyIEg9LzpzY29wZVxcYi87KGZ1bmN0aW9uKCl7dmFyIHQ9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTt0cnl7dC5xdWVyeVNlbGVjdG9yQWxsKFwiOnNjb3BlICpcIil9Y2F0Y2godCl7cmV0dXJuITF9cmV0dXJuITB9KSgpfHwoTD1mdW5jdGlvbih0KXtpZighSC50ZXN0KHQpKXJldHVybiB0aGlzLnF1ZXJ5U2VsZWN0b3JBbGwodCk7dmFyIGU9Qm9vbGVhbih0aGlzLmlkKTtlfHwodGhpcy5pZD11KFwic2NvcGVcIikpO3ZhciBuPW51bGw7dHJ5e3Q9dC5yZXBsYWNlKEgsXCIjXCIrdGhpcy5pZCksbj10aGlzLnF1ZXJ5U2VsZWN0b3JBbGwodCl9ZmluYWxseXtlfHx0aGlzLnJlbW92ZUF0dHJpYnV0ZShcImlkXCIpfXJldHVybiBufSxPPWZ1bmN0aW9uKHQpe2lmKCFILnRlc3QodCkpcmV0dXJuIHRoaXMucXVlcnlTZWxlY3Rvcih0KTt2YXIgZT1MLmNhbGwodGhpcyx0KTtyZXR1cm5cInVuZGVmaW5lZFwiIT10eXBlb2YgZVswXT9lWzBdOm51bGx9KTt2YXIgaj1UKCksTT0vW14uXSooPz1cXC4uKilcXC58LiovLFI9L1xcLi4qLyx4PS9ea2V5LyxXPS86OlxcZCskLyxVPXt9LEs9MSxWPXttb3VzZWVudGVyOlwibW91c2VvdmVyXCIsbW91c2VsZWF2ZTpcIm1vdXNlb3V0XCJ9LEI9W1wiY2xpY2tcIixcImRibGNsaWNrXCIsXCJtb3VzZXVwXCIsXCJtb3VzZWRvd25cIixcImNvbnRleHRtZW51XCIsXCJtb3VzZXdoZWVsXCIsXCJET01Nb3VzZVNjcm9sbFwiLFwibW91c2VvdmVyXCIsXCJtb3VzZW91dFwiLFwibW91c2Vtb3ZlXCIsXCJzZWxlY3RzdGFydFwiLFwic2VsZWN0ZW5kXCIsXCJrZXlkb3duXCIsXCJrZXlwcmVzc1wiLFwia2V5dXBcIixcIm9yaWVudGF0aW9uY2hhbmdlXCIsXCJ0b3VjaHN0YXJ0XCIsXCJ0b3VjaG1vdmVcIixcInRvdWNoZW5kXCIsXCJ0b3VjaGNhbmNlbFwiLFwicG9pbnRlcmRvd25cIixcInBvaW50ZXJtb3ZlXCIsXCJwb2ludGVydXBcIixcInBvaW50ZXJsZWF2ZVwiLFwicG9pbnRlcmNhbmNlbFwiLFwiZ2VzdHVyZXN0YXJ0XCIsXCJnZXN0dXJlY2hhbmdlXCIsXCJnZXN0dXJlZW5kXCIsXCJmb2N1c1wiLFwiYmx1clwiLFwiY2hhbmdlXCIsXCJyZXNldFwiLFwic2VsZWN0XCIsXCJzdWJtaXRcIixcImZvY3VzaW5cIixcImZvY3Vzb3V0XCIsXCJsb2FkXCIsXCJ1bmxvYWRcIixcImJlZm9yZXVubG9hZFwiLFwicmVzaXplXCIsXCJtb3ZlXCIsXCJET01Db250ZW50TG9hZGVkXCIsXCJyZWFkeXN0YXRlY2hhbmdlXCIsXCJlcnJvclwiLFwiYWJvcnRcIixcInNjcm9sbFwiXTtmdW5jdGlvbiBGKHQsZSl7cmV0dXJuIGUmJmUrXCI6OlwiK0srK3x8dC51aWRFdmVudHx8SysrfWZ1bmN0aW9uIFEodCl7dmFyIGU9Rih0KTtyZXR1cm4gdC51aWRFdmVudD1lLFVbZV09VVtlXXx8e30sVVtlXX1mdW5jdGlvbiBZKHQsZSl7bnVsbD09PXQud2hpY2gmJngudGVzdCh0LnR5cGUpJiYodC53aGljaD1udWxsPT09dC5jaGFyQ29kZT90LmtleUNvZGU6dC5jaGFyQ29kZSksdC5kZWxlZ2F0ZVRhcmdldD1lfWZ1bmN0aW9uIEcodCxlLG4pe3ZvaWQgMD09PW4mJihuPW51bGwpO2Zvcih2YXIgaT1PYmplY3Qua2V5cyh0KSxvPTAscj1pLmxlbmd0aDtvPHI7bysrKXt2YXIgcz10W2lbb11dO2lmKHMub3JpZ2luYWxIYW5kbGVyPT09ZSYmcy5kZWxlZ2F0aW9uU2VsZWN0b3I9PT1uKXJldHVybiBzfXJldHVybiBudWxsfWZ1bmN0aW9uIFgodCxlLG4pe3ZhciBpPVwic3RyaW5nXCI9PXR5cGVvZiBlLG89aT9uOmUscj10LnJlcGxhY2UoUixcIlwiKSxzPVZbcl07cmV0dXJuIHMmJihyPXMpLEIuaW5kZXhPZihyKT4tMXx8KHI9dCksW2ksbyxyXX1mdW5jdGlvbiBxKHQsZSxuLGksbyl7aWYoXCJzdHJpbmdcIj09dHlwZW9mIGUmJnQpe258fChuPWksaT1udWxsKTt2YXIgcj1YKGUsbixpKSxzPXJbMF0sYT1yWzFdLGw9clsyXSxjPVEodCksdT1jW2xdfHwoY1tsXT17fSksZj1HKHUsYSxzP246bnVsbCk7aWYoZilmLm9uZU9mZj1mLm9uZU9mZiYmbztlbHNle3ZhciBoPUYoYSxlLnJlcGxhY2UoTSxcIlwiKSksZD1zP2Z1bmN0aW9uKHQsZSxuKXtyZXR1cm4gZnVuY3Rpb24gaShvKXtmb3IodmFyIHI9dC5xdWVyeVNlbGVjdG9yQWxsKGUpLHM9by50YXJnZXQ7cyYmcyE9PXRoaXM7cz1zLnBhcmVudE5vZGUpZm9yKHZhciBhPXIubGVuZ3RoO2EtLTspaWYoclthXT09PXMpcmV0dXJuIFkobyxzKSxpLm9uZU9mZiYmWi5vZmYodCxvLnR5cGUsbiksbi5hcHBseShzLFtvXSk7cmV0dXJuIG51bGx9fSh0LG4saSk6ZnVuY3Rpb24odCxlKXtyZXR1cm4gZnVuY3Rpb24gbihpKXtyZXR1cm4gWShpLHQpLG4ub25lT2ZmJiZaLm9mZih0LGkudHlwZSxlKSxlLmFwcGx5KHQsW2ldKX19KHQsbik7ZC5kZWxlZ2F0aW9uU2VsZWN0b3I9cz9uOm51bGwsZC5vcmlnaW5hbEhhbmRsZXI9YSxkLm9uZU9mZj1vLGQudWlkRXZlbnQ9aCx1W2hdPWQsdC5hZGRFdmVudExpc3RlbmVyKGwsZCxzKX19fWZ1bmN0aW9uIHoodCxlLG4saSxvKXt2YXIgcj1HKGVbbl0saSxvKTtyJiYodC5yZW1vdmVFdmVudExpc3RlbmVyKG4scixCb29sZWFuKG8pKSxkZWxldGUgZVtuXVtyLnVpZEV2ZW50XSl9dmFyIFo9e29uOmZ1bmN0aW9uKHQsZSxuLGkpe3EodCxlLG4saSwhMSl9LG9uZTpmdW5jdGlvbih0LGUsbixpKXtxKHQsZSxuLGksITApfSxvZmY6ZnVuY3Rpb24odCxlLG4saSl7aWYoXCJzdHJpbmdcIj09dHlwZW9mIGUmJnQpe3ZhciBvPVgoZSxuLGkpLHI9b1swXSxzPW9bMV0sYT1vWzJdLGw9YSE9PWUsYz1RKHQpLHU9XCIuXCI9PT1lLmNoYXJBdCgwKTtpZihcInVuZGVmaW5lZFwiPT10eXBlb2Ygcyl7dSYmT2JqZWN0LmtleXMoYykuZm9yRWFjaCgoZnVuY3Rpb24obil7IWZ1bmN0aW9uKHQsZSxuLGkpe3ZhciBvPWVbbl18fHt9O09iamVjdC5rZXlzKG8pLmZvckVhY2goKGZ1bmN0aW9uKHIpe2lmKHIuaW5kZXhPZihpKT4tMSl7dmFyIHM9b1tyXTt6KHQsZSxuLHMub3JpZ2luYWxIYW5kbGVyLHMuZGVsZWdhdGlvblNlbGVjdG9yKX19KSl9KHQsYyxuLGUuc2xpY2UoMSkpfSkpO3ZhciBmPWNbYV18fHt9O09iamVjdC5rZXlzKGYpLmZvckVhY2goKGZ1bmN0aW9uKG4pe3ZhciBpPW4ucmVwbGFjZShXLFwiXCIpO2lmKCFsfHxlLmluZGV4T2YoaSk+LTEpe3ZhciBvPWZbbl07eih0LGMsYSxvLm9yaWdpbmFsSGFuZGxlcixvLmRlbGVnYXRpb25TZWxlY3Rvcil9fSkpfWVsc2V7aWYoIWN8fCFjW2FdKXJldHVybjt6KHQsYyxhLHMscj9uOm51bGwpfX19LHRyaWdnZXI6ZnVuY3Rpb24odCxlLG4pe2lmKFwic3RyaW5nXCIhPXR5cGVvZiBlfHwhdClyZXR1cm4gbnVsbDt2YXIgaSxvPWUucmVwbGFjZShSLFwiXCIpLHI9ZSE9PW8scz1CLmluZGV4T2Yobyk+LTEsYT0hMCxsPSEwLGM9ITEsdT1udWxsO3JldHVybiByJiZqJiYoaT1qLkV2ZW50KGUsbiksaih0KS50cmlnZ2VyKGkpLGE9IWkuaXNQcm9wYWdhdGlvblN0b3BwZWQoKSxsPSFpLmlzSW1tZWRpYXRlUHJvcGFnYXRpb25TdG9wcGVkKCksYz1pLmlzRGVmYXVsdFByZXZlbnRlZCgpKSxzPyh1PWRvY3VtZW50LmNyZWF0ZUV2ZW50KFwiSFRNTEV2ZW50c1wiKSkuaW5pdEV2ZW50KG8sYSwhMCk6dT1OKGUse2J1YmJsZXM6YSxjYW5jZWxhYmxlOiEwfSksXCJ1bmRlZmluZWRcIiE9dHlwZW9mIG4mJk9iamVjdC5rZXlzKG4pLmZvckVhY2goKGZ1bmN0aW9uKHQpe09iamVjdC5kZWZpbmVQcm9wZXJ0eSh1LHQse2dldDpmdW5jdGlvbigpe3JldHVybiBuW3RdfX0pfSkpLGMmJih1LnByZXZlbnREZWZhdWx0KCksUHx8T2JqZWN0LmRlZmluZVByb3BlcnR5KHUsXCJkZWZhdWx0UHJldmVudGVkXCIse2dldDpmdW5jdGlvbigpe3JldHVybiEwfX0pKSxsJiZ0LmRpc3BhdGNoRXZlbnQodSksdS5kZWZhdWx0UHJldmVudGVkJiZcInVuZGVmaW5lZFwiIT10eXBlb2YgaSYmaS5wcmV2ZW50RGVmYXVsdCgpLHV9fSwkPXttYXRjaGVzOmZ1bmN0aW9uKHQsZSl7cmV0dXJuIHcuY2FsbCh0LGUpfSxmaW5kOmZ1bmN0aW9uKHQsZSl7cmV0dXJuIHZvaWQgMD09PWUmJihlPWRvY3VtZW50LmRvY3VtZW50RWxlbWVudCksTC5jYWxsKGUsdCl9LGZpbmRPbmU6ZnVuY3Rpb24odCxlKXtyZXR1cm4gdm9pZCAwPT09ZSYmKGU9ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KSxPLmNhbGwoZSx0KX0sY2hpbGRyZW46ZnVuY3Rpb24odCxlKXt2YXIgbj10aGlzLGk9RSh0LmNoaWxkcmVuKTtyZXR1cm4gaS5maWx0ZXIoKGZ1bmN0aW9uKHQpe3JldHVybiBuLm1hdGNoZXModCxlKX0pKX0scGFyZW50czpmdW5jdGlvbih0LGUpe2Zvcih2YXIgbj1bXSxpPXQucGFyZW50Tm9kZTtpJiZpLm5vZGVUeXBlPT09Tm9kZS5FTEVNRU5UX05PREUmJjMhPT1pLm5vZGVUeXBlOyl0aGlzLm1hdGNoZXMoaSxlKSYmbi5wdXNoKGkpLGk9aS5wYXJlbnROb2RlO3JldHVybiBufSxjbG9zZXN0OmZ1bmN0aW9uKHQsZSl7cmV0dXJuIEMuY2FsbCh0LGUpfSxwcmV2OmZ1bmN0aW9uKHQsZSl7Zm9yKHZhciBuPVtdLGk9dC5wcmV2aW91c1NpYmxpbmc7aSYmaS5ub2RlVHlwZT09PU5vZGUuRUxFTUVOVF9OT0RFJiYzIT09aS5ub2RlVHlwZTspdGhpcy5tYXRjaGVzKGksZSkmJm4ucHVzaChpKSxpPWkucHJldmlvdXNTaWJsaW5nO3JldHVybiBufX0sSj1cImJzLmFsZXJ0XCIsdHQ9XCIuXCIrSixldD17Q0xPU0U6XCJjbG9zZVwiK3R0LENMT1NFRDpcImNsb3NlZFwiK3R0LENMSUNLX0RBVEFfQVBJOlwiY2xpY2tcIit0dCtcIi5kYXRhLWFwaVwifSxudD1cImFsZXJ0XCIsaXQ9XCJmYWRlXCIsb3Q9XCJzaG93XCIscnQ9ZnVuY3Rpb24oKXtmdW5jdGlvbiB0KHQpe3RoaXMuX2VsZW1lbnQ9dCx0aGlzLl9lbGVtZW50JiZBLnNldERhdGEodCxKLHRoaXMpfXZhciBlPXQucHJvdG90eXBlO3JldHVybiBlLmNsb3NlPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMuX2VsZW1lbnQ7dCYmKGU9dGhpcy5fZ2V0Um9vdEVsZW1lbnQodCkpO3ZhciBuPXRoaXMuX3RyaWdnZXJDbG9zZUV2ZW50KGUpO251bGw9PT1ufHxuLmRlZmF1bHRQcmV2ZW50ZWR8fHRoaXMuX3JlbW92ZUVsZW1lbnQoZSl9LGUuZGlzcG9zZT1mdW5jdGlvbigpe0EucmVtb3ZlRGF0YSh0aGlzLl9lbGVtZW50LEopLHRoaXMuX2VsZW1lbnQ9bnVsbH0sZS5fZ2V0Um9vdEVsZW1lbnQ9ZnVuY3Rpb24odCl7dmFyIGU9ZCh0KTtyZXR1cm4gZXx8KGU9JC5jbG9zZXN0KHQsXCIuXCIrbnQpKSxlfSxlLl90cmlnZ2VyQ2xvc2VFdmVudD1mdW5jdGlvbih0KXtyZXR1cm4gWi50cmlnZ2VyKHQsZXQuQ0xPU0UpfSxlLl9yZW1vdmVFbGVtZW50PWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXM7aWYodC5jbGFzc0xpc3QucmVtb3ZlKG90KSx0LmNsYXNzTGlzdC5jb250YWlucyhpdCkpe3ZhciBuPWcodCk7Wi5vbmUodCxjLChmdW5jdGlvbigpe3JldHVybiBlLl9kZXN0cm95RWxlbWVudCh0KX0pKSxtKHQsbil9ZWxzZSB0aGlzLl9kZXN0cm95RWxlbWVudCh0KX0sZS5fZGVzdHJveUVsZW1lbnQ9ZnVuY3Rpb24odCl7dC5wYXJlbnROb2RlJiZ0LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodCksWi50cmlnZ2VyKHQsZXQuQ0xPU0VEKX0sdC5qUXVlcnlJbnRlcmZhY2U9ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMuZWFjaCgoZnVuY3Rpb24oKXt2YXIgbj1BLmdldERhdGEodGhpcyxKKTtufHwobj1uZXcgdCh0aGlzKSksXCJjbG9zZVwiPT09ZSYmbltlXSh0aGlzKX0pKX0sdC5oYW5kbGVEaXNtaXNzPWZ1bmN0aW9uKHQpe3JldHVybiBmdW5jdGlvbihlKXtlJiZlLnByZXZlbnREZWZhdWx0KCksdC5jbG9zZSh0aGlzKX19LHQuZ2V0SW5zdGFuY2U9ZnVuY3Rpb24odCl7cmV0dXJuIEEuZ2V0RGF0YSh0LEopfSxuKHQsbnVsbCxbe2tleTpcIlZFUlNJT05cIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm5cIjQuMy4xXCJ9fV0pLHR9KCk7Wi5vbihkb2N1bWVudCxldC5DTElDS19EQVRBX0FQSSwnW2RhdGEtZGlzbWlzcz1cImFsZXJ0XCJdJyxydC5oYW5kbGVEaXNtaXNzKG5ldyBydCkpO3ZhciBzdD1UKCk7aWYoc3Qpe3ZhciBhdD1zdC5mbi5hbGVydDtzdC5mbi5hbGVydD1ydC5qUXVlcnlJbnRlcmZhY2Usc3QuZm4uYWxlcnQuQ29uc3RydWN0b3I9cnQsc3QuZm4uYWxlcnQubm9Db25mbGljdD1mdW5jdGlvbigpe3JldHVybiBzdC5mbi5hbGVydD1hdCxydC5qUXVlcnlJbnRlcmZhY2V9fXZhciBsdD1cImJzLmJ1dHRvblwiLGN0PVwiLlwiK2x0LHV0PVwiYWN0aXZlXCIsZnQ9XCJidG5cIixodD1cImZvY3VzXCIsZHQ9J1tkYXRhLXRvZ2dsZV49XCJidXR0b25cIl0nLGd0PSdbZGF0YS10b2dnbGU9XCJidXR0b25zXCJdJyxwdD0naW5wdXQ6bm90KFt0eXBlPVwiaGlkZGVuXCJdKScsX3Q9XCIuYWN0aXZlXCIsbXQ9XCIuYnRuXCIsdnQ9e0NMSUNLX0RBVEFfQVBJOlwiY2xpY2tcIitjdCtcIi5kYXRhLWFwaVwiLEZPQ1VTX0RBVEFfQVBJOlwiZm9jdXNcIitjdCtcIi5kYXRhLWFwaVwiLEJMVVJfREFUQV9BUEk6XCJibHVyXCIrY3QrXCIuZGF0YS1hcGlcIn0sRXQ9ZnVuY3Rpb24oKXtmdW5jdGlvbiB0KHQpe3RoaXMuX2VsZW1lbnQ9dCxBLnNldERhdGEodCxsdCx0aGlzKX12YXIgZT10LnByb3RvdHlwZTtyZXR1cm4gZS50b2dnbGU9ZnVuY3Rpb24oKXt2YXIgdD0hMCxlPSEwLG49JC5jbG9zZXN0KHRoaXMuX2VsZW1lbnQsZ3QpO2lmKG4pe3ZhciBpPSQuZmluZE9uZShwdCx0aGlzLl9lbGVtZW50KTtpZihpJiZcInJhZGlvXCI9PT1pLnR5cGUpe2lmKGkuY2hlY2tlZCYmdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnModXQpKXQ9ITE7ZWxzZXt2YXIgbz0kLmZpbmRPbmUoX3Qsbik7byYmby5jbGFzc0xpc3QucmVtb3ZlKHV0KX1pZih0KXtpZihpLmhhc0F0dHJpYnV0ZShcImRpc2FibGVkXCIpfHxuLmhhc0F0dHJpYnV0ZShcImRpc2FibGVkXCIpfHxpLmNsYXNzTGlzdC5jb250YWlucyhcImRpc2FibGVkXCIpfHxuLmNsYXNzTGlzdC5jb250YWlucyhcImRpc2FibGVkXCIpKXJldHVybjtpLmNoZWNrZWQ9IXRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKHV0KSxaLnRyaWdnZXIoaSxcImNoYW5nZVwiKX1pLmZvY3VzKCksZT0hMX19ZSYmdGhpcy5fZWxlbWVudC5zZXRBdHRyaWJ1dGUoXCJhcmlhLXByZXNzZWRcIiwhdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnModXQpKSx0JiZ0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC50b2dnbGUodXQpfSxlLmRpc3Bvc2U9ZnVuY3Rpb24oKXtBLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCxsdCksdGhpcy5fZWxlbWVudD1udWxsfSx0LmpRdWVyeUludGVyZmFjZT1mdW5jdGlvbihlKXtyZXR1cm4gdGhpcy5lYWNoKChmdW5jdGlvbigpe3ZhciBuPUEuZ2V0RGF0YSh0aGlzLGx0KTtufHwobj1uZXcgdCh0aGlzKSksXCJ0b2dnbGVcIj09PWUmJm5bZV0oKX0pKX0sdC5nZXRJbnN0YW5jZT1mdW5jdGlvbih0KXtyZXR1cm4gQS5nZXREYXRhKHQsbHQpfSxuKHQsbnVsbCxbe2tleTpcIlZFUlNJT05cIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm5cIjQuMy4xXCJ9fV0pLHR9KCk7Wi5vbihkb2N1bWVudCx2dC5DTElDS19EQVRBX0FQSSxkdCwoZnVuY3Rpb24odCl7dC5wcmV2ZW50RGVmYXVsdCgpO3ZhciBlPXQudGFyZ2V0O2UuY2xhc3NMaXN0LmNvbnRhaW5zKGZ0KXx8KGU9JC5jbG9zZXN0KGUsbXQpKTt2YXIgbj1BLmdldERhdGEoZSxsdCk7bnx8KG49bmV3IEV0KGUpKSxuLnRvZ2dsZSgpfSkpLFoub24oZG9jdW1lbnQsdnQuRk9DVVNfREFUQV9BUEksZHQsKGZ1bmN0aW9uKHQpe3ZhciBlPSQuY2xvc2VzdCh0LnRhcmdldCxtdCk7ZSYmZS5jbGFzc0xpc3QuYWRkKGh0KX0pKSxaLm9uKGRvY3VtZW50LHZ0LkJMVVJfREFUQV9BUEksZHQsKGZ1bmN0aW9uKHQpe3ZhciBlPSQuY2xvc2VzdCh0LnRhcmdldCxtdCk7ZSYmZS5jbGFzc0xpc3QucmVtb3ZlKGh0KX0pKTt2YXIgeXQ9VCgpO2lmKHl0KXt2YXIgYnQ9eXQuZm4uYnV0dG9uO3l0LmZuLmJ1dHRvbj1FdC5qUXVlcnlJbnRlcmZhY2UseXQuZm4uYnV0dG9uLkNvbnN0cnVjdG9yPUV0LHl0LmZuLmJ1dHRvbi5ub0NvbmZsaWN0PWZ1bmN0aW9uKCl7cmV0dXJuIHl0LmZuLmJ1dHRvbj1idCxFdC5qUXVlcnlJbnRlcmZhY2V9fWZ1bmN0aW9uIER0KHQpe3JldHVyblwidHJ1ZVwiPT09dHx8XCJmYWxzZVwiIT09dCYmKHQ9PT1OdW1iZXIodCkudG9TdHJpbmcoKT9OdW1iZXIodCk6XCJcIj09PXR8fFwibnVsbFwiPT09dD9udWxsOnQpfWZ1bmN0aW9uIFR0KHQpe3JldHVybiB0LnJlcGxhY2UoL1tBLVpdL2csKGZ1bmN0aW9uKHQpe3JldHVyblwiLVwiK3QudG9Mb3dlckNhc2UoKX0pKX12YXIgSXQ9e3NldERhdGFBdHRyaWJ1dGU6ZnVuY3Rpb24odCxlLG4pe3Quc2V0QXR0cmlidXRlKFwiZGF0YS1cIitUdChlKSxuKX0scmVtb3ZlRGF0YUF0dHJpYnV0ZTpmdW5jdGlvbih0LGUpe3QucmVtb3ZlQXR0cmlidXRlKFwiZGF0YS1cIitUdChlKSl9LGdldERhdGFBdHRyaWJ1dGVzOmZ1bmN0aW9uKHQpe2lmKCF0KXJldHVybnt9O3ZhciBlPXIoe30sdC5kYXRhc2V0KTtyZXR1cm4gT2JqZWN0LmtleXMoZSkuZm9yRWFjaCgoZnVuY3Rpb24odCl7ZVt0XT1EdChlW3RdKX0pKSxlfSxnZXREYXRhQXR0cmlidXRlOmZ1bmN0aW9uKHQsZSl7cmV0dXJuIER0KHQuZ2V0QXR0cmlidXRlKFwiZGF0YS1cIitUdChlKSkpfSxvZmZzZXQ6ZnVuY3Rpb24odCl7dmFyIGU9dC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtyZXR1cm57dG9wOmUudG9wK2RvY3VtZW50LmJvZHkuc2Nyb2xsVG9wLGxlZnQ6ZS5sZWZ0K2RvY3VtZW50LmJvZHkuc2Nyb2xsTGVmdH19LHBvc2l0aW9uOmZ1bmN0aW9uKHQpe3JldHVybnt0b3A6dC5vZmZzZXRUb3AsbGVmdDp0Lm9mZnNldExlZnR9fSx0b2dnbGVDbGFzczpmdW5jdGlvbih0LGUpe3QmJih0LmNsYXNzTGlzdC5jb250YWlucyhlKT90LmNsYXNzTGlzdC5yZW1vdmUoZSk6dC5jbGFzc0xpc3QuYWRkKGUpKX19LEF0PVwiY2Fyb3VzZWxcIixTdD1cImJzLmNhcm91c2VsXCIsd3Q9XCIuXCIrU3QsQ3Q9e2ludGVydmFsOjVlMyxrZXlib2FyZDohMCxzbGlkZTohMSxwYXVzZTpcImhvdmVyXCIsd3JhcDohMCx0b3VjaDohMH0sTHQ9e2ludGVydmFsOlwiKG51bWJlcnxib29sZWFuKVwiLGtleWJvYXJkOlwiYm9vbGVhblwiLHNsaWRlOlwiKGJvb2xlYW58c3RyaW5nKVwiLHBhdXNlOlwiKHN0cmluZ3xib29sZWFuKVwiLHdyYXA6XCJib29sZWFuXCIsdG91Y2g6XCJib29sZWFuXCJ9LE90PVwibmV4dFwiLE50PVwicHJldlwiLGt0PVwibGVmdFwiLFB0PVwicmlnaHRcIixIdD17U0xJREU6XCJzbGlkZVwiK3d0LFNMSUQ6XCJzbGlkXCIrd3QsS0VZRE9XTjpcImtleWRvd25cIit3dCxNT1VTRUVOVEVSOlwibW91c2VlbnRlclwiK3d0LE1PVVNFTEVBVkU6XCJtb3VzZWxlYXZlXCIrd3QsVE9VQ0hTVEFSVDpcInRvdWNoc3RhcnRcIit3dCxUT1VDSE1PVkU6XCJ0b3VjaG1vdmVcIit3dCxUT1VDSEVORDpcInRvdWNoZW5kXCIrd3QsUE9JTlRFUkRPV046XCJwb2ludGVyZG93blwiK3d0LFBPSU5URVJVUDpcInBvaW50ZXJ1cFwiK3d0LERSQUdfU1RBUlQ6XCJkcmFnc3RhcnRcIit3dCxMT0FEX0RBVEFfQVBJOlwibG9hZFwiK3d0K1wiLmRhdGEtYXBpXCIsQ0xJQ0tfREFUQV9BUEk6XCJjbGlja1wiK3d0K1wiLmRhdGEtYXBpXCJ9LGp0PVwiY2Fyb3VzZWxcIixNdD1cImFjdGl2ZVwiLFJ0PVwic2xpZGVcIix4dD1cImNhcm91c2VsLWl0ZW0tcmlnaHRcIixXdD1cImNhcm91c2VsLWl0ZW0tbGVmdFwiLFV0PVwiY2Fyb3VzZWwtaXRlbS1uZXh0XCIsS3Q9XCJjYXJvdXNlbC1pdGVtLXByZXZcIixWdD1cInBvaW50ZXItZXZlbnRcIixCdD17QUNUSVZFOlwiLmFjdGl2ZVwiLEFDVElWRV9JVEVNOlwiLmFjdGl2ZS5jYXJvdXNlbC1pdGVtXCIsSVRFTTpcIi5jYXJvdXNlbC1pdGVtXCIsSVRFTV9JTUc6XCIuY2Fyb3VzZWwtaXRlbSBpbWdcIixORVhUX1BSRVY6XCIuY2Fyb3VzZWwtaXRlbS1uZXh0LCAuY2Fyb3VzZWwtaXRlbS1wcmV2XCIsSU5ESUNBVE9SUzpcIi5jYXJvdXNlbC1pbmRpY2F0b3JzXCIsREFUQV9TTElERTpcIltkYXRhLXNsaWRlXSwgW2RhdGEtc2xpZGUtdG9dXCIsREFUQV9SSURFOidbZGF0YS1yaWRlPVwiY2Fyb3VzZWxcIl0nfSxGdD17VE9VQ0g6XCJ0b3VjaFwiLFBFTjpcInBlblwifSxRdD1mdW5jdGlvbigpe2Z1bmN0aW9uIHQodCxlKXt0aGlzLl9pdGVtcz1udWxsLHRoaXMuX2ludGVydmFsPW51bGwsdGhpcy5fYWN0aXZlRWxlbWVudD1udWxsLHRoaXMuX2lzUGF1c2VkPSExLHRoaXMuX2lzU2xpZGluZz0hMSx0aGlzLnRvdWNoVGltZW91dD1udWxsLHRoaXMudG91Y2hTdGFydFg9MCx0aGlzLnRvdWNoRGVsdGFYPTAsdGhpcy5fY29uZmlnPXRoaXMuX2dldENvbmZpZyhlKSx0aGlzLl9lbGVtZW50PXQsdGhpcy5faW5kaWNhdG9yc0VsZW1lbnQ9JC5maW5kT25lKEJ0LklORElDQVRPUlMsdGhpcy5fZWxlbWVudCksdGhpcy5fdG91Y2hTdXBwb3J0ZWQ9XCJvbnRvdWNoc3RhcnRcImluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudHx8bmF2aWdhdG9yLm1heFRvdWNoUG9pbnRzPjAsdGhpcy5fcG9pbnRlckV2ZW50PUJvb2xlYW4od2luZG93LlBvaW50ZXJFdmVudHx8d2luZG93Lk1TUG9pbnRlckV2ZW50KSx0aGlzLl9hZGRFdmVudExpc3RlbmVycygpLEEuc2V0RGF0YSh0LFN0LHRoaXMpfXZhciBlPXQucHJvdG90eXBlO3JldHVybiBlLm5leHQ9ZnVuY3Rpb24oKXt0aGlzLl9pc1NsaWRpbmd8fHRoaXMuX3NsaWRlKE90KX0sZS5uZXh0V2hlblZpc2libGU9ZnVuY3Rpb24oKXshZG9jdW1lbnQuaGlkZGVuJiZ5KHRoaXMuX2VsZW1lbnQpJiZ0aGlzLm5leHQoKX0sZS5wcmV2PWZ1bmN0aW9uKCl7dGhpcy5faXNTbGlkaW5nfHx0aGlzLl9zbGlkZShOdCl9LGUucGF1c2U9ZnVuY3Rpb24odCl7dHx8KHRoaXMuX2lzUGF1c2VkPSEwKSwkLmZpbmRPbmUoQnQuTkVYVF9QUkVWLHRoaXMuX2VsZW1lbnQpJiYocCh0aGlzLl9lbGVtZW50KSx0aGlzLmN5Y2xlKCEwKSksY2xlYXJJbnRlcnZhbCh0aGlzLl9pbnRlcnZhbCksdGhpcy5faW50ZXJ2YWw9bnVsbH0sZS5jeWNsZT1mdW5jdGlvbih0KXt0fHwodGhpcy5faXNQYXVzZWQ9ITEpLHRoaXMuX2ludGVydmFsJiYoY2xlYXJJbnRlcnZhbCh0aGlzLl9pbnRlcnZhbCksdGhpcy5faW50ZXJ2YWw9bnVsbCksdGhpcy5fY29uZmlnJiZ0aGlzLl9jb25maWcuaW50ZXJ2YWwmJiF0aGlzLl9pc1BhdXNlZCYmKHRoaXMuX2ludGVydmFsPXNldEludGVydmFsKChkb2N1bWVudC52aXNpYmlsaXR5U3RhdGU/dGhpcy5uZXh0V2hlblZpc2libGU6dGhpcy5uZXh0KS5iaW5kKHRoaXMpLHRoaXMuX2NvbmZpZy5pbnRlcnZhbCkpfSxlLnRvPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXM7dGhpcy5fYWN0aXZlRWxlbWVudD0kLmZpbmRPbmUoQnQuQUNUSVZFX0lURU0sdGhpcy5fZWxlbWVudCk7dmFyIG49dGhpcy5fZ2V0SXRlbUluZGV4KHRoaXMuX2FjdGl2ZUVsZW1lbnQpO2lmKCEodD50aGlzLl9pdGVtcy5sZW5ndGgtMXx8dDwwKSlpZih0aGlzLl9pc1NsaWRpbmcpWi5vbmUodGhpcy5fZWxlbWVudCxIdC5TTElELChmdW5jdGlvbigpe3JldHVybiBlLnRvKHQpfSkpO2Vsc2V7aWYobj09PXQpcmV0dXJuIHRoaXMucGF1c2UoKSx2b2lkIHRoaXMuY3ljbGUoKTt2YXIgaT10Pm4/T3Q6TnQ7dGhpcy5fc2xpZGUoaSx0aGlzLl9pdGVtc1t0XSl9fSxlLmRpc3Bvc2U9ZnVuY3Rpb24oKXtaLm9mZih0aGlzLl9lbGVtZW50LHd0KSxBLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCxTdCksdGhpcy5faXRlbXM9bnVsbCx0aGlzLl9jb25maWc9bnVsbCx0aGlzLl9lbGVtZW50PW51bGwsdGhpcy5faW50ZXJ2YWw9bnVsbCx0aGlzLl9pc1BhdXNlZD1udWxsLHRoaXMuX2lzU2xpZGluZz1udWxsLHRoaXMuX2FjdGl2ZUVsZW1lbnQ9bnVsbCx0aGlzLl9pbmRpY2F0b3JzRWxlbWVudD1udWxsfSxlLl9nZXRDb25maWc9ZnVuY3Rpb24odCl7cmV0dXJuIHQ9cih7fSxDdCx7fSx0KSx2KEF0LHQsTHQpLHR9LGUuX2hhbmRsZVN3aXBlPWZ1bmN0aW9uKCl7dmFyIHQ9TWF0aC5hYnModGhpcy50b3VjaERlbHRhWCk7aWYoISh0PD00MCkpe3ZhciBlPXQvdGhpcy50b3VjaERlbHRhWDt0aGlzLnRvdWNoRGVsdGFYPTAsZT4wJiZ0aGlzLnByZXYoKSxlPDAmJnRoaXMubmV4dCgpfX0sZS5fYWRkRXZlbnRMaXN0ZW5lcnM9ZnVuY3Rpb24oKXt2YXIgdD10aGlzO3RoaXMuX2NvbmZpZy5rZXlib2FyZCYmWi5vbih0aGlzLl9lbGVtZW50LEh0LktFWURPV04sKGZ1bmN0aW9uKGUpe3JldHVybiB0Ll9rZXlkb3duKGUpfSkpLFwiaG92ZXJcIj09PXRoaXMuX2NvbmZpZy5wYXVzZSYmKFoub24odGhpcy5fZWxlbWVudCxIdC5NT1VTRUVOVEVSLChmdW5jdGlvbihlKXtyZXR1cm4gdC5wYXVzZShlKX0pKSxaLm9uKHRoaXMuX2VsZW1lbnQsSHQuTU9VU0VMRUFWRSwoZnVuY3Rpb24oZSl7cmV0dXJuIHQuY3ljbGUoZSl9KSkpLHRoaXMuX2NvbmZpZy50b3VjaCYmdGhpcy5fdG91Y2hTdXBwb3J0ZWQmJnRoaXMuX2FkZFRvdWNoRXZlbnRMaXN0ZW5lcnMoKX0sZS5fYWRkVG91Y2hFdmVudExpc3RlbmVycz1mdW5jdGlvbigpe3ZhciB0PXRoaXMsZT1mdW5jdGlvbihlKXt0Ll9wb2ludGVyRXZlbnQmJkZ0W2UucG9pbnRlclR5cGUudG9VcHBlckNhc2UoKV0/dC50b3VjaFN0YXJ0WD1lLmNsaWVudFg6dC5fcG9pbnRlckV2ZW50fHwodC50b3VjaFN0YXJ0WD1lLnRvdWNoZXNbMF0uY2xpZW50WCl9LG49ZnVuY3Rpb24oZSl7dC5fcG9pbnRlckV2ZW50JiZGdFtlLnBvaW50ZXJUeXBlLnRvVXBwZXJDYXNlKCldJiYodC50b3VjaERlbHRhWD1lLmNsaWVudFgtdC50b3VjaFN0YXJ0WCksdC5faGFuZGxlU3dpcGUoKSxcImhvdmVyXCI9PT10Ll9jb25maWcucGF1c2UmJih0LnBhdXNlKCksdC50b3VjaFRpbWVvdXQmJmNsZWFyVGltZW91dCh0LnRvdWNoVGltZW91dCksdC50b3VjaFRpbWVvdXQ9c2V0VGltZW91dCgoZnVuY3Rpb24oZSl7cmV0dXJuIHQuY3ljbGUoZSl9KSw1MDArdC5fY29uZmlnLmludGVydmFsKSl9O0UoJC5maW5kKEJ0LklURU1fSU1HLHRoaXMuX2VsZW1lbnQpKS5mb3JFYWNoKChmdW5jdGlvbih0KXtaLm9uKHQsSHQuRFJBR19TVEFSVCwoZnVuY3Rpb24odCl7cmV0dXJuIHQucHJldmVudERlZmF1bHQoKX0pKX0pKSx0aGlzLl9wb2ludGVyRXZlbnQ/KFoub24odGhpcy5fZWxlbWVudCxIdC5QT0lOVEVSRE9XTiwoZnVuY3Rpb24odCl7cmV0dXJuIGUodCl9KSksWi5vbih0aGlzLl9lbGVtZW50LEh0LlBPSU5URVJVUCwoZnVuY3Rpb24odCl7cmV0dXJuIG4odCl9KSksdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuYWRkKFZ0KSk6KFoub24odGhpcy5fZWxlbWVudCxIdC5UT1VDSFNUQVJULChmdW5jdGlvbih0KXtyZXR1cm4gZSh0KX0pKSxaLm9uKHRoaXMuX2VsZW1lbnQsSHQuVE9VQ0hNT1ZFLChmdW5jdGlvbihlKXtyZXR1cm4gZnVuY3Rpb24oZSl7ZS50b3VjaGVzJiZlLnRvdWNoZXMubGVuZ3RoPjE/dC50b3VjaERlbHRhWD0wOnQudG91Y2hEZWx0YVg9ZS50b3VjaGVzWzBdLmNsaWVudFgtdC50b3VjaFN0YXJ0WH0oZSl9KSksWi5vbih0aGlzLl9lbGVtZW50LEh0LlRPVUNIRU5ELChmdW5jdGlvbih0KXtyZXR1cm4gbih0KX0pKSl9LGUuX2tleWRvd249ZnVuY3Rpb24odCl7aWYoIS9pbnB1dHx0ZXh0YXJlYS9pLnRlc3QodC50YXJnZXQudGFnTmFtZSkpc3dpdGNoKHQud2hpY2gpe2Nhc2UgMzc6dC5wcmV2ZW50RGVmYXVsdCgpLHRoaXMucHJldigpO2JyZWFrO2Nhc2UgMzk6dC5wcmV2ZW50RGVmYXVsdCgpLHRoaXMubmV4dCgpfX0sZS5fZ2V0SXRlbUluZGV4PWZ1bmN0aW9uKHQpe3JldHVybiB0aGlzLl9pdGVtcz10JiZ0LnBhcmVudE5vZGU/RSgkLmZpbmQoQnQuSVRFTSx0LnBhcmVudE5vZGUpKTpbXSx0aGlzLl9pdGVtcy5pbmRleE9mKHQpfSxlLl9nZXRJdGVtQnlEaXJlY3Rpb249ZnVuY3Rpb24odCxlKXt2YXIgbj10PT09T3QsaT10PT09TnQsbz10aGlzLl9nZXRJdGVtSW5kZXgoZSkscj10aGlzLl9pdGVtcy5sZW5ndGgtMTtpZigoaSYmMD09PW98fG4mJm89PT1yKSYmIXRoaXMuX2NvbmZpZy53cmFwKXJldHVybiBlO3ZhciBzPShvKyh0PT09TnQ/LTE6MSkpJXRoaXMuX2l0ZW1zLmxlbmd0aDtyZXR1cm4tMT09PXM/dGhpcy5faXRlbXNbdGhpcy5faXRlbXMubGVuZ3RoLTFdOnRoaXMuX2l0ZW1zW3NdfSxlLl90cmlnZ2VyU2xpZGVFdmVudD1mdW5jdGlvbih0LGUpe3ZhciBuPXRoaXMuX2dldEl0ZW1JbmRleCh0KSxpPXRoaXMuX2dldEl0ZW1JbmRleCgkLmZpbmRPbmUoQnQuQUNUSVZFX0lURU0sdGhpcy5fZWxlbWVudCkpO3JldHVybiBaLnRyaWdnZXIodGhpcy5fZWxlbWVudCxIdC5TTElERSx7cmVsYXRlZFRhcmdldDp0LGRpcmVjdGlvbjplLGZyb206aSx0bzpufSl9LGUuX3NldEFjdGl2ZUluZGljYXRvckVsZW1lbnQ9ZnVuY3Rpb24odCl7aWYodGhpcy5faW5kaWNhdG9yc0VsZW1lbnQpe2Zvcih2YXIgZT0kLmZpbmQoQnQuQUNUSVZFLHRoaXMuX2luZGljYXRvcnNFbGVtZW50KSxuPTA7bjxlLmxlbmd0aDtuKyspZVtuXS5jbGFzc0xpc3QucmVtb3ZlKE10KTt2YXIgaT10aGlzLl9pbmRpY2F0b3JzRWxlbWVudC5jaGlsZHJlblt0aGlzLl9nZXRJdGVtSW5kZXgodCldO2kmJmkuY2xhc3NMaXN0LmFkZChNdCl9fSxlLl9zbGlkZT1mdW5jdGlvbih0LGUpe3ZhciBuLGksbyxyPXRoaXMscz0kLmZpbmRPbmUoQnQuQUNUSVZFX0lURU0sdGhpcy5fZWxlbWVudCksYT10aGlzLl9nZXRJdGVtSW5kZXgocyksbD1lfHxzJiZ0aGlzLl9nZXRJdGVtQnlEaXJlY3Rpb24odCxzKSx1PXRoaXMuX2dldEl0ZW1JbmRleChsKSxmPUJvb2xlYW4odGhpcy5faW50ZXJ2YWwpO2lmKHQ9PT1PdD8obj1XdCxpPVV0LG89a3QpOihuPXh0LGk9S3Qsbz1QdCksbCYmbC5jbGFzc0xpc3QuY29udGFpbnMoTXQpKXRoaXMuX2lzU2xpZGluZz0hMTtlbHNlIGlmKCF0aGlzLl90cmlnZ2VyU2xpZGVFdmVudChsLG8pLmRlZmF1bHRQcmV2ZW50ZWQmJnMmJmwpe2lmKHRoaXMuX2lzU2xpZGluZz0hMCxmJiZ0aGlzLnBhdXNlKCksdGhpcy5fc2V0QWN0aXZlSW5kaWNhdG9yRWxlbWVudChsKSx0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5jb250YWlucyhSdCkpe2wuY2xhc3NMaXN0LmFkZChpKSxEKGwpLHMuY2xhc3NMaXN0LmFkZChuKSxsLmNsYXNzTGlzdC5hZGQobik7dmFyIGg9cGFyc2VJbnQobC5nZXRBdHRyaWJ1dGUoXCJkYXRhLWludGVydmFsXCIpLDEwKTtoPyh0aGlzLl9jb25maWcuZGVmYXVsdEludGVydmFsPXRoaXMuX2NvbmZpZy5kZWZhdWx0SW50ZXJ2YWx8fHRoaXMuX2NvbmZpZy5pbnRlcnZhbCx0aGlzLl9jb25maWcuaW50ZXJ2YWw9aCk6dGhpcy5fY29uZmlnLmludGVydmFsPXRoaXMuX2NvbmZpZy5kZWZhdWx0SW50ZXJ2YWx8fHRoaXMuX2NvbmZpZy5pbnRlcnZhbDt2YXIgZD1nKHMpO1oub25lKHMsYywoZnVuY3Rpb24oKXtsLmNsYXNzTGlzdC5yZW1vdmUobiksbC5jbGFzc0xpc3QucmVtb3ZlKGkpLGwuY2xhc3NMaXN0LmFkZChNdCkscy5jbGFzc0xpc3QucmVtb3ZlKE10KSxzLmNsYXNzTGlzdC5yZW1vdmUoaSkscy5jbGFzc0xpc3QucmVtb3ZlKG4pLHIuX2lzU2xpZGluZz0hMSxzZXRUaW1lb3V0KChmdW5jdGlvbigpe1oudHJpZ2dlcihyLl9lbGVtZW50LEh0LlNMSUQse3JlbGF0ZWRUYXJnZXQ6bCxkaXJlY3Rpb246byxmcm9tOmEsdG86dX0pfSksMCl9KSksbShzLGQpfWVsc2Ugcy5jbGFzc0xpc3QucmVtb3ZlKE10KSxsLmNsYXNzTGlzdC5hZGQoTXQpLHRoaXMuX2lzU2xpZGluZz0hMSxaLnRyaWdnZXIodGhpcy5fZWxlbWVudCxIdC5TTElELHtyZWxhdGVkVGFyZ2V0OmwsZGlyZWN0aW9uOm8sZnJvbTphLHRvOnV9KTtmJiZ0aGlzLmN5Y2xlKCl9fSx0LmNhcm91c2VsSW50ZXJmYWNlPWZ1bmN0aW9uKGUsbil7dmFyIGk9QS5nZXREYXRhKGUsU3QpLG89cih7fSxDdCx7fSxJdC5nZXREYXRhQXR0cmlidXRlcyhlKSk7XCJvYmplY3RcIj09dHlwZW9mIG4mJihvPXIoe30sbyx7fSxuKSk7dmFyIHM9XCJzdHJpbmdcIj09dHlwZW9mIG4/bjpvLnNsaWRlO2lmKGl8fChpPW5ldyB0KGUsbykpLFwibnVtYmVyXCI9PXR5cGVvZiBuKWkudG8obik7ZWxzZSBpZihcInN0cmluZ1wiPT10eXBlb2Ygcyl7aWYoXCJ1bmRlZmluZWRcIj09dHlwZW9mIGlbc10pdGhyb3cgbmV3IFR5cGVFcnJvcignTm8gbWV0aG9kIG5hbWVkIFwiJytzKydcIicpO2lbc10oKX1lbHNlIG8uaW50ZXJ2YWwmJm8ucmlkZSYmKGkucGF1c2UoKSxpLmN5Y2xlKCkpfSx0LmpRdWVyeUludGVyZmFjZT1mdW5jdGlvbihlKXtyZXR1cm4gdGhpcy5lYWNoKChmdW5jdGlvbigpe3QuY2Fyb3VzZWxJbnRlcmZhY2UodGhpcyxlKX0pKX0sdC5kYXRhQXBpQ2xpY2tIYW5kbGVyPWZ1bmN0aW9uKGUpe3ZhciBuPWQodGhpcyk7aWYobiYmbi5jbGFzc0xpc3QuY29udGFpbnMoanQpKXt2YXIgaT1yKHt9LEl0LmdldERhdGFBdHRyaWJ1dGVzKG4pLHt9LEl0LmdldERhdGFBdHRyaWJ1dGVzKHRoaXMpKSxvPXRoaXMuZ2V0QXR0cmlidXRlKFwiZGF0YS1zbGlkZS10b1wiKTtvJiYoaS5pbnRlcnZhbD0hMSksdC5jYXJvdXNlbEludGVyZmFjZShuLGkpLG8mJkEuZ2V0RGF0YShuLFN0KS50byhvKSxlLnByZXZlbnREZWZhdWx0KCl9fSx0LmdldEluc3RhbmNlPWZ1bmN0aW9uKHQpe3JldHVybiBBLmdldERhdGEodCxTdCl9LG4odCxudWxsLFt7a2V5OlwiVkVSU0lPTlwiLGdldDpmdW5jdGlvbigpe3JldHVyblwiNC4zLjFcIn19LHtrZXk6XCJEZWZhdWx0XCIsZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuIEN0fX1dKSx0fSgpO1oub24oZG9jdW1lbnQsSHQuQ0xJQ0tfREFUQV9BUEksQnQuREFUQV9TTElERSxRdC5kYXRhQXBpQ2xpY2tIYW5kbGVyKSxaLm9uKHdpbmRvdyxIdC5MT0FEX0RBVEFfQVBJLChmdW5jdGlvbigpe2Zvcih2YXIgdD1FKCQuZmluZChCdC5EQVRBX1JJREUpKSxlPTAsbj10Lmxlbmd0aDtlPG47ZSsrKVF0LmNhcm91c2VsSW50ZXJmYWNlKHRbZV0sQS5nZXREYXRhKHRbZV0sU3QpKX0pKTt2YXIgWXQ9VCgpO2lmKFl0KXt2YXIgR3Q9WXQuZm5bQXRdO1l0LmZuW0F0XT1RdC5qUXVlcnlJbnRlcmZhY2UsWXQuZm5bQXRdLkNvbnN0cnVjdG9yPVF0LFl0LmZuW0F0XS5ub0NvbmZsaWN0PWZ1bmN0aW9uKCl7cmV0dXJuIFl0LmZuW0F0XT1HdCxRdC5qUXVlcnlJbnRlcmZhY2V9fXZhciBYdD1cImNvbGxhcHNlXCIscXQ9XCJicy5jb2xsYXBzZVwiLHp0PVwiLlwiK3F0LFp0PXt0b2dnbGU6ITAscGFyZW50OlwiXCJ9LCR0PXt0b2dnbGU6XCJib29sZWFuXCIscGFyZW50OlwiKHN0cmluZ3xlbGVtZW50KVwifSxKdD17U0hPVzpcInNob3dcIit6dCxTSE9XTjpcInNob3duXCIrenQsSElERTpcImhpZGVcIit6dCxISURERU46XCJoaWRkZW5cIit6dCxDTElDS19EQVRBX0FQSTpcImNsaWNrXCIrenQrXCIuZGF0YS1hcGlcIn0sdGU9XCJzaG93XCIsZWU9XCJjb2xsYXBzZVwiLG5lPVwiY29sbGFwc2luZ1wiLGllPVwiY29sbGFwc2VkXCIsb2U9XCJ3aWR0aFwiLHJlPVwiaGVpZ2h0XCIsc2U9e0FDVElWRVM6XCIuc2hvdywgLmNvbGxhcHNpbmdcIixEQVRBX1RPR0dMRTonW2RhdGEtdG9nZ2xlPVwiY29sbGFwc2VcIl0nfSxhZT1mdW5jdGlvbigpe2Z1bmN0aW9uIHQodCxlKXt0aGlzLl9pc1RyYW5zaXRpb25pbmc9ITEsdGhpcy5fZWxlbWVudD10LHRoaXMuX2NvbmZpZz10aGlzLl9nZXRDb25maWcoZSksdGhpcy5fdHJpZ2dlckFycmF5PUUoJC5maW5kKCdbZGF0YS10b2dnbGU9XCJjb2xsYXBzZVwiXVtocmVmPVwiIycrdC5pZCsnXCJdLFtkYXRhLXRvZ2dsZT1cImNvbGxhcHNlXCJdW2RhdGEtdGFyZ2V0PVwiIycrdC5pZCsnXCJdJykpO2Zvcih2YXIgbj1FKCQuZmluZChzZS5EQVRBX1RPR0dMRSkpLGk9MCxvPW4ubGVuZ3RoO2k8bztpKyspe3ZhciByPW5baV0scz1oKHIpLGE9RSgkLmZpbmQocykpLmZpbHRlcigoZnVuY3Rpb24oZSl7cmV0dXJuIGU9PT10fSkpO251bGwhPT1zJiZhLmxlbmd0aCYmKHRoaXMuX3NlbGVjdG9yPXMsdGhpcy5fdHJpZ2dlckFycmF5LnB1c2gocikpfXRoaXMuX3BhcmVudD10aGlzLl9jb25maWcucGFyZW50P3RoaXMuX2dldFBhcmVudCgpOm51bGwsdGhpcy5fY29uZmlnLnBhcmVudHx8dGhpcy5fYWRkQXJpYUFuZENvbGxhcHNlZENsYXNzKHRoaXMuX2VsZW1lbnQsdGhpcy5fdHJpZ2dlckFycmF5KSx0aGlzLl9jb25maWcudG9nZ2xlJiZ0aGlzLnRvZ2dsZSgpLEEuc2V0RGF0YSh0LHF0LHRoaXMpfXZhciBlPXQucHJvdG90eXBlO3JldHVybiBlLnRvZ2dsZT1mdW5jdGlvbigpe3RoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKHRlKT90aGlzLmhpZGUoKTp0aGlzLnNob3coKX0sZS5zaG93PWZ1bmN0aW9uKCl7dmFyIGU9dGhpcztpZighdGhpcy5faXNUcmFuc2l0aW9uaW5nJiYhdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnModGUpKXt2YXIgbixpO3RoaXMuX3BhcmVudCYmMD09PShuPUUoJC5maW5kKHNlLkFDVElWRVMsdGhpcy5fcGFyZW50KSkuZmlsdGVyKChmdW5jdGlvbih0KXtyZXR1cm5cInN0cmluZ1wiPT10eXBlb2YgZS5fY29uZmlnLnBhcmVudD90LmdldEF0dHJpYnV0ZShcImRhdGEtcGFyZW50XCIpPT09ZS5fY29uZmlnLnBhcmVudDp0LmNsYXNzTGlzdC5jb250YWlucyhlZSl9KSkpLmxlbmd0aCYmKG49bnVsbCk7dmFyIG89JC5maW5kT25lKHRoaXMuX3NlbGVjdG9yKTtpZihuKXt2YXIgcj1uLmZpbHRlcigoZnVuY3Rpb24odCl7cmV0dXJuIG8hPT10fSkpO2lmKChpPXJbMF0/QS5nZXREYXRhKHJbMF0scXQpOm51bGwpJiZpLl9pc1RyYW5zaXRpb25pbmcpcmV0dXJufWlmKCFaLnRyaWdnZXIodGhpcy5fZWxlbWVudCxKdC5TSE9XKS5kZWZhdWx0UHJldmVudGVkKXtuJiZuLmZvckVhY2goKGZ1bmN0aW9uKGUpe28hPT1lJiZ0LmNvbGxhcHNlSW50ZXJmYWNlKGUsXCJoaWRlXCIpLGl8fEEuc2V0RGF0YShlLHF0LG51bGwpfSkpO3ZhciBzPXRoaXMuX2dldERpbWVuc2lvbigpO3RoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShlZSksdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuYWRkKG5lKSx0aGlzLl9lbGVtZW50LnN0eWxlW3NdPTAsdGhpcy5fdHJpZ2dlckFycmF5Lmxlbmd0aCYmdGhpcy5fdHJpZ2dlckFycmF5LmZvckVhY2goKGZ1bmN0aW9uKHQpe3QuY2xhc3NMaXN0LnJlbW92ZShpZSksdC5zZXRBdHRyaWJ1dGUoXCJhcmlhLWV4cGFuZGVkXCIsITApfSkpLHRoaXMuc2V0VHJhbnNpdGlvbmluZyghMCk7dmFyIGE9XCJzY3JvbGxcIisoc1swXS50b1VwcGVyQ2FzZSgpK3Muc2xpY2UoMSkpLGw9Zyh0aGlzLl9lbGVtZW50KTtaLm9uZSh0aGlzLl9lbGVtZW50LGMsKGZ1bmN0aW9uKCl7ZS5fZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKG5lKSxlLl9lbGVtZW50LmNsYXNzTGlzdC5hZGQoZWUpLGUuX2VsZW1lbnQuY2xhc3NMaXN0LmFkZCh0ZSksZS5fZWxlbWVudC5zdHlsZVtzXT1cIlwiLGUuc2V0VHJhbnNpdGlvbmluZyghMSksWi50cmlnZ2VyKGUuX2VsZW1lbnQsSnQuU0hPV04pfSkpLG0odGhpcy5fZWxlbWVudCxsKSx0aGlzLl9lbGVtZW50LnN0eWxlW3NdPXRoaXMuX2VsZW1lbnRbYV0rXCJweFwifX19LGUuaGlkZT1mdW5jdGlvbigpe3ZhciB0PXRoaXM7aWYoIXRoaXMuX2lzVHJhbnNpdGlvbmluZyYmdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnModGUpJiYhWi50cmlnZ2VyKHRoaXMuX2VsZW1lbnQsSnQuSElERSkuZGVmYXVsdFByZXZlbnRlZCl7dmFyIGU9dGhpcy5fZ2V0RGltZW5zaW9uKCk7dGhpcy5fZWxlbWVudC5zdHlsZVtlXT10aGlzLl9lbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpW2VdK1wicHhcIixEKHRoaXMuX2VsZW1lbnQpLHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmFkZChuZSksdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKGVlKSx0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUodGUpO3ZhciBuPXRoaXMuX3RyaWdnZXJBcnJheS5sZW5ndGg7aWYobj4wKWZvcih2YXIgaT0wO2k8bjtpKyspe3ZhciBvPXRoaXMuX3RyaWdnZXJBcnJheVtpXSxyPWQobyk7ciYmIXIuY2xhc3NMaXN0LmNvbnRhaW5zKHRlKSYmKG8uY2xhc3NMaXN0LmFkZChpZSksby5zZXRBdHRyaWJ1dGUoXCJhcmlhLWV4cGFuZGVkXCIsITEpKX10aGlzLnNldFRyYW5zaXRpb25pbmcoITApO3RoaXMuX2VsZW1lbnQuc3R5bGVbZV09XCJcIjt2YXIgcz1nKHRoaXMuX2VsZW1lbnQpO1oub25lKHRoaXMuX2VsZW1lbnQsYywoZnVuY3Rpb24oKXt0LnNldFRyYW5zaXRpb25pbmcoITEpLHQuX2VsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShuZSksdC5fZWxlbWVudC5jbGFzc0xpc3QuYWRkKGVlKSxaLnRyaWdnZXIodC5fZWxlbWVudCxKdC5ISURERU4pfSkpLG0odGhpcy5fZWxlbWVudCxzKX19LGUuc2V0VHJhbnNpdGlvbmluZz1mdW5jdGlvbih0KXt0aGlzLl9pc1RyYW5zaXRpb25pbmc9dH0sZS5kaXNwb3NlPWZ1bmN0aW9uKCl7QS5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQscXQpLHRoaXMuX2NvbmZpZz1udWxsLHRoaXMuX3BhcmVudD1udWxsLHRoaXMuX2VsZW1lbnQ9bnVsbCx0aGlzLl90cmlnZ2VyQXJyYXk9bnVsbCx0aGlzLl9pc1RyYW5zaXRpb25pbmc9bnVsbH0sZS5fZ2V0Q29uZmlnPWZ1bmN0aW9uKHQpe3JldHVybih0PXIoe30sWnQse30sdCkpLnRvZ2dsZT1Cb29sZWFuKHQudG9nZ2xlKSx2KFh0LHQsJHQpLHR9LGUuX2dldERpbWVuc2lvbj1mdW5jdGlvbigpe3JldHVybiB0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5jb250YWlucyhvZSk/b2U6cmV9LGUuX2dldFBhcmVudD1mdW5jdGlvbigpe3ZhciB0PXRoaXMsZT10aGlzLl9jb25maWcucGFyZW50O18oZSk/XCJ1bmRlZmluZWRcIj09dHlwZW9mIGUuanF1ZXJ5JiZcInVuZGVmaW5lZFwiPT10eXBlb2YgZVswXXx8KGU9ZVswXSk6ZT0kLmZpbmRPbmUoZSk7dmFyIG49J1tkYXRhLXRvZ2dsZT1cImNvbGxhcHNlXCJdW2RhdGEtcGFyZW50PVwiJytlKydcIl0nO3JldHVybiBFKCQuZmluZChuLGUpKS5mb3JFYWNoKChmdW5jdGlvbihlKXt2YXIgbj1kKGUpO3QuX2FkZEFyaWFBbmRDb2xsYXBzZWRDbGFzcyhuLFtlXSl9KSksZX0sZS5fYWRkQXJpYUFuZENvbGxhcHNlZENsYXNzPWZ1bmN0aW9uKHQsZSl7aWYodCl7dmFyIG49dC5jbGFzc0xpc3QuY29udGFpbnModGUpO2UubGVuZ3RoJiZlLmZvckVhY2goKGZ1bmN0aW9uKHQpe24/dC5jbGFzc0xpc3QucmVtb3ZlKGllKTp0LmNsYXNzTGlzdC5hZGQoaWUpLHQuc2V0QXR0cmlidXRlKFwiYXJpYS1leHBhbmRlZFwiLG4pfSkpfX0sdC5jb2xsYXBzZUludGVyZmFjZT1mdW5jdGlvbihlLG4pe3ZhciBpPUEuZ2V0RGF0YShlLHF0KSxvPXIoe30sWnQse30sSXQuZ2V0RGF0YUF0dHJpYnV0ZXMoZSkse30sXCJvYmplY3RcIj09dHlwZW9mIG4mJm4/bjp7fSk7aWYoIWkmJm8udG9nZ2xlJiYvc2hvd3xoaWRlLy50ZXN0KG4pJiYoby50b2dnbGU9ITEpLGl8fChpPW5ldyB0KGUsbykpLFwic3RyaW5nXCI9PXR5cGVvZiBuKXtpZihcInVuZGVmaW5lZFwiPT10eXBlb2YgaVtuXSl0aHJvdyBuZXcgVHlwZUVycm9yKCdObyBtZXRob2QgbmFtZWQgXCInK24rJ1wiJyk7aVtuXSgpfX0sdC5qUXVlcnlJbnRlcmZhY2U9ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMuZWFjaCgoZnVuY3Rpb24oKXt0LmNvbGxhcHNlSW50ZXJmYWNlKHRoaXMsZSl9KSl9LHQuZ2V0SW5zdGFuY2U9ZnVuY3Rpb24odCl7cmV0dXJuIEEuZ2V0RGF0YSh0LHF0KX0sbih0LG51bGwsW3trZXk6XCJWRVJTSU9OXCIsZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuXCI0LjMuMVwifX0se2tleTpcIkRlZmF1bHRcIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gWnR9fV0pLHR9KCk7Wi5vbihkb2N1bWVudCxKdC5DTElDS19EQVRBX0FQSSxzZS5EQVRBX1RPR0dMRSwoZnVuY3Rpb24odCl7XCJBXCI9PT10LnRhcmdldC50YWdOYW1lJiZ0LnByZXZlbnREZWZhdWx0KCk7dmFyIGU9SXQuZ2V0RGF0YUF0dHJpYnV0ZXModGhpcyksbj1oKHRoaXMpO0UoJC5maW5kKG4pKS5mb3JFYWNoKChmdW5jdGlvbih0KXt2YXIgbixpPUEuZ2V0RGF0YSh0LHF0KTtpPyhudWxsPT09aS5fcGFyZW50JiZcInN0cmluZ1wiPT10eXBlb2YgZS5wYXJlbnQmJihpLl9jb25maWcucGFyZW50PWUucGFyZW50LGkuX3BhcmVudD1pLl9nZXRQYXJlbnQoKSksbj1cInRvZ2dsZVwiKTpuPWUsYWUuY29sbGFwc2VJbnRlcmZhY2UodCxuKX0pKX0pKTt2YXIgbGU9VCgpO2lmKGxlKXt2YXIgY2U9bGUuZm5bWHRdO2xlLmZuW1h0XT1hZS5qUXVlcnlJbnRlcmZhY2UsbGUuZm5bWHRdLkNvbnN0cnVjdG9yPWFlLGxlLmZuW1h0XS5ub0NvbmZsaWN0PWZ1bmN0aW9uKCl7cmV0dXJuIGxlLmZuW1h0XT1jZSxhZS5qUXVlcnlJbnRlcmZhY2V9fXZhciB1ZT1cImRyb3Bkb3duXCIsZmU9XCJicy5kcm9wZG93blwiLGhlPVwiLlwiK2ZlLGRlPW5ldyBSZWdFeHAoXCIzOHw0MHwyN1wiKSxnZT17SElERTpcImhpZGVcIitoZSxISURERU46XCJoaWRkZW5cIitoZSxTSE9XOlwic2hvd1wiK2hlLFNIT1dOOlwic2hvd25cIitoZSxDTElDSzpcImNsaWNrXCIraGUsQ0xJQ0tfREFUQV9BUEk6XCJjbGlja1wiK2hlK1wiLmRhdGEtYXBpXCIsS0VZRE9XTl9EQVRBX0FQSTpcImtleWRvd25cIitoZStcIi5kYXRhLWFwaVwiLEtFWVVQX0RBVEFfQVBJOlwia2V5dXBcIitoZStcIi5kYXRhLWFwaVwifSxwZT1cImRpc2FibGVkXCIsX2U9XCJzaG93XCIsbWU9XCJkcm9wdXBcIix2ZT1cImRyb3ByaWdodFwiLEVlPVwiZHJvcGxlZnRcIix5ZT1cImRyb3Bkb3duLW1lbnUtcmlnaHRcIixiZT1cInBvc2l0aW9uLXN0YXRpY1wiLERlPSdbZGF0YS10b2dnbGU9XCJkcm9wZG93blwiXScsVGU9XCIuZHJvcGRvd24gZm9ybVwiLEllPVwiLmRyb3Bkb3duLW1lbnVcIixBZT1cIi5uYXZiYXItbmF2XCIsU2U9XCIuZHJvcGRvd24tbWVudSAuZHJvcGRvd24taXRlbTpub3QoLmRpc2FibGVkKTpub3QoOmRpc2FibGVkKVwiLHdlPVwidG9wLXN0YXJ0XCIsQ2U9XCJ0b3AtZW5kXCIsTGU9XCJib3R0b20tc3RhcnRcIixPZT1cImJvdHRvbS1lbmRcIixOZT1cInJpZ2h0LXN0YXJ0XCIsa2U9XCJsZWZ0LXN0YXJ0XCIsUGU9e29mZnNldDowLGZsaXA6ITAsYm91bmRhcnk6XCJzY3JvbGxQYXJlbnRcIixyZWZlcmVuY2U6XCJ0b2dnbGVcIixkaXNwbGF5OlwiZHluYW1pY1wiLHBvcHBlckNvbmZpZzpudWxsfSxIZT17b2Zmc2V0OlwiKG51bWJlcnxzdHJpbmd8ZnVuY3Rpb24pXCIsZmxpcDpcImJvb2xlYW5cIixib3VuZGFyeTpcIihzdHJpbmd8ZWxlbWVudClcIixyZWZlcmVuY2U6XCIoc3RyaW5nfGVsZW1lbnQpXCIsZGlzcGxheTpcInN0cmluZ1wiLHBvcHBlckNvbmZpZzpcIihudWxsfG9iamVjdClcIn0samU9ZnVuY3Rpb24oKXtmdW5jdGlvbiBlKHQsZSl7dGhpcy5fZWxlbWVudD10LHRoaXMuX3BvcHBlcj1udWxsLHRoaXMuX2NvbmZpZz10aGlzLl9nZXRDb25maWcoZSksdGhpcy5fbWVudT10aGlzLl9nZXRNZW51RWxlbWVudCgpLHRoaXMuX2luTmF2YmFyPXRoaXMuX2RldGVjdE5hdmJhcigpLHRoaXMuX2FkZEV2ZW50TGlzdGVuZXJzKCksQS5zZXREYXRhKHQsZmUsdGhpcyl9dmFyIGk9ZS5wcm90b3R5cGU7cmV0dXJuIGkudG9nZ2xlPWZ1bmN0aW9uKCl7aWYoIXRoaXMuX2VsZW1lbnQuZGlzYWJsZWQmJiF0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5jb250YWlucyhwZSkpe3ZhciB0PXRoaXMuX21lbnUuY2xhc3NMaXN0LmNvbnRhaW5zKF9lKTtlLmNsZWFyTWVudXMoKSx0fHx0aGlzLnNob3coKX19LGkuc2hvdz1mdW5jdGlvbigpe2lmKCEodGhpcy5fZWxlbWVudC5kaXNhYmxlZHx8dGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMocGUpfHx0aGlzLl9tZW51LmNsYXNzTGlzdC5jb250YWlucyhfZSkpKXt2YXIgbj1lLmdldFBhcmVudEZyb21FbGVtZW50KHRoaXMuX2VsZW1lbnQpLGk9e3JlbGF0ZWRUYXJnZXQ6dGhpcy5fZWxlbWVudH07aWYoIVoudHJpZ2dlcihuLGdlLlNIT1csaSkuZGVmYXVsdFByZXZlbnRlZCl7aWYoIXRoaXMuX2luTmF2YmFyKXtpZihcInVuZGVmaW5lZFwiPT10eXBlb2YgdCl0aHJvdyBuZXcgVHlwZUVycm9yKFwiQm9vdHN0cmFwJ3MgZHJvcGRvd25zIHJlcXVpcmUgUG9wcGVyLmpzIChodHRwczovL3BvcHBlci5qcy5vcmcpXCIpO3ZhciBvPXRoaXMuX2VsZW1lbnQ7XCJwYXJlbnRcIj09PXRoaXMuX2NvbmZpZy5yZWZlcmVuY2U/bz1uOl8odGhpcy5fY29uZmlnLnJlZmVyZW5jZSkmJihvPXRoaXMuX2NvbmZpZy5yZWZlcmVuY2UsXCJ1bmRlZmluZWRcIiE9dHlwZW9mIHRoaXMuX2NvbmZpZy5yZWZlcmVuY2UuanF1ZXJ5JiYobz10aGlzLl9jb25maWcucmVmZXJlbmNlWzBdKSksXCJzY3JvbGxQYXJlbnRcIiE9PXRoaXMuX2NvbmZpZy5ib3VuZGFyeSYmbi5jbGFzc0xpc3QuYWRkKGJlKSx0aGlzLl9wb3BwZXI9bmV3IHQobyx0aGlzLl9tZW51LHRoaXMuX2dldFBvcHBlckNvbmZpZygpKX1cIm9udG91Y2hzdGFydFwiaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50JiYhRSgkLmNsb3Nlc3QobixBZSkpLmxlbmd0aCYmRShkb2N1bWVudC5ib2R5LmNoaWxkcmVuKS5mb3JFYWNoKChmdW5jdGlvbih0KXtyZXR1cm4gWi5vbih0LFwibW91c2VvdmVyXCIsbnVsbCwoZnVuY3Rpb24oKXt9KSl9KSksdGhpcy5fZWxlbWVudC5mb2N1cygpLHRoaXMuX2VsZW1lbnQuc2V0QXR0cmlidXRlKFwiYXJpYS1leHBhbmRlZFwiLCEwKSxJdC50b2dnbGVDbGFzcyh0aGlzLl9tZW51LF9lKSxJdC50b2dnbGVDbGFzcyhuLF9lKSxaLnRyaWdnZXIobixnZS5TSE9XTixpKX19fSxpLmhpZGU9ZnVuY3Rpb24oKXtpZighdGhpcy5fZWxlbWVudC5kaXNhYmxlZCYmIXRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKHBlKSYmdGhpcy5fbWVudS5jbGFzc0xpc3QuY29udGFpbnMoX2UpKXt2YXIgdD1lLmdldFBhcmVudEZyb21FbGVtZW50KHRoaXMuX2VsZW1lbnQpLG49e3JlbGF0ZWRUYXJnZXQ6dGhpcy5fZWxlbWVudH07Wi50cmlnZ2VyKHQsZ2UuSElERSxuKS5kZWZhdWx0UHJldmVudGVkfHwodGhpcy5fcG9wcGVyJiZ0aGlzLl9wb3BwZXIuZGVzdHJveSgpLEl0LnRvZ2dsZUNsYXNzKHRoaXMuX21lbnUsX2UpLEl0LnRvZ2dsZUNsYXNzKHQsX2UpLFoudHJpZ2dlcih0LGdlLkhJRERFTixuKSl9fSxpLmRpc3Bvc2U9ZnVuY3Rpb24oKXtBLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCxmZSksWi5vZmYodGhpcy5fZWxlbWVudCxoZSksdGhpcy5fZWxlbWVudD1udWxsLHRoaXMuX21lbnU9bnVsbCx0aGlzLl9wb3BwZXImJih0aGlzLl9wb3BwZXIuZGVzdHJveSgpLHRoaXMuX3BvcHBlcj1udWxsKX0saS51cGRhdGU9ZnVuY3Rpb24oKXt0aGlzLl9pbk5hdmJhcj10aGlzLl9kZXRlY3ROYXZiYXIoKSx0aGlzLl9wb3BwZXImJnRoaXMuX3BvcHBlci5zY2hlZHVsZVVwZGF0ZSgpfSxpLl9hZGRFdmVudExpc3RlbmVycz1mdW5jdGlvbigpe3ZhciB0PXRoaXM7Wi5vbih0aGlzLl9lbGVtZW50LGdlLkNMSUNLLChmdW5jdGlvbihlKXtlLnByZXZlbnREZWZhdWx0KCksZS5zdG9wUHJvcGFnYXRpb24oKSx0LnRvZ2dsZSgpfSkpfSxpLl9nZXRDb25maWc9ZnVuY3Rpb24odCl7cmV0dXJuIHQ9cih7fSx0aGlzLmNvbnN0cnVjdG9yLkRlZmF1bHQse30sSXQuZ2V0RGF0YUF0dHJpYnV0ZXModGhpcy5fZWxlbWVudCkse30sdCksdih1ZSx0LHRoaXMuY29uc3RydWN0b3IuRGVmYXVsdFR5cGUpLHR9LGkuX2dldE1lbnVFbGVtZW50PWZ1bmN0aW9uKCl7dmFyIHQ9ZS5nZXRQYXJlbnRGcm9tRWxlbWVudCh0aGlzLl9lbGVtZW50KTtyZXR1cm4gJC5maW5kT25lKEllLHQpfSxpLl9nZXRQbGFjZW1lbnQ9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLl9lbGVtZW50LnBhcmVudE5vZGUsZT1MZTtyZXR1cm4gdC5jbGFzc0xpc3QuY29udGFpbnMobWUpPyhlPXdlLHRoaXMuX21lbnUuY2xhc3NMaXN0LmNvbnRhaW5zKHllKSYmKGU9Q2UpKTp0LmNsYXNzTGlzdC5jb250YWlucyh2ZSk/ZT1OZTp0LmNsYXNzTGlzdC5jb250YWlucyhFZSk/ZT1rZTp0aGlzLl9tZW51LmNsYXNzTGlzdC5jb250YWlucyh5ZSkmJihlPU9lKSxlfSxpLl9kZXRlY3ROYXZiYXI9ZnVuY3Rpb24oKXtyZXR1cm4gQm9vbGVhbigkLmNsb3Nlc3QodGhpcy5fZWxlbWVudCxcIi5uYXZiYXJcIikpfSxpLl9nZXRPZmZzZXQ9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLGU9e307cmV0dXJuXCJmdW5jdGlvblwiPT10eXBlb2YgdGhpcy5fY29uZmlnLm9mZnNldD9lLmZuPWZ1bmN0aW9uKGUpe3JldHVybiBlLm9mZnNldHM9cih7fSxlLm9mZnNldHMse30sdC5fY29uZmlnLm9mZnNldChlLm9mZnNldHMsdC5fZWxlbWVudCl8fHt9KSxlfTplLm9mZnNldD10aGlzLl9jb25maWcub2Zmc2V0LGV9LGkuX2dldFBvcHBlckNvbmZpZz1mdW5jdGlvbigpe3ZhciB0PXtwbGFjZW1lbnQ6dGhpcy5fZ2V0UGxhY2VtZW50KCksbW9kaWZpZXJzOntvZmZzZXQ6dGhpcy5fZ2V0T2Zmc2V0KCksZmxpcDp7ZW5hYmxlZDp0aGlzLl9jb25maWcuZmxpcH0scHJldmVudE92ZXJmbG93Ontib3VuZGFyaWVzRWxlbWVudDp0aGlzLl9jb25maWcuYm91bmRhcnl9fX07cmV0dXJuXCJzdGF0aWNcIj09PXRoaXMuX2NvbmZpZy5kaXNwbGF5JiYodC5tb2RpZmllcnMuYXBwbHlTdHlsZT17ZW5hYmxlZDohMX0pLHIoe30sdCx7fSx0aGlzLl9jb25maWcucG9wcGVyQ29uZmlnKX0sZS5kcm9wZG93bkludGVyZmFjZT1mdW5jdGlvbih0LG4pe3ZhciBpPUEuZ2V0RGF0YSh0LGZlKTtpZihpfHwoaT1uZXcgZSh0LFwib2JqZWN0XCI9PXR5cGVvZiBuP246bnVsbCkpLFwic3RyaW5nXCI9PXR5cGVvZiBuKXtpZihcInVuZGVmaW5lZFwiPT10eXBlb2YgaVtuXSl0aHJvdyBuZXcgVHlwZUVycm9yKCdObyBtZXRob2QgbmFtZWQgXCInK24rJ1wiJyk7aVtuXSgpfX0sZS5qUXVlcnlJbnRlcmZhY2U9ZnVuY3Rpb24odCl7cmV0dXJuIHRoaXMuZWFjaCgoZnVuY3Rpb24oKXtlLmRyb3Bkb3duSW50ZXJmYWNlKHRoaXMsdCl9KSl9LGUuY2xlYXJNZW51cz1mdW5jdGlvbih0KXtpZighdHx8MyE9PXQud2hpY2gmJihcImtleXVwXCIhPT10LnR5cGV8fDk9PT10LndoaWNoKSlmb3IodmFyIG49RSgkLmZpbmQoRGUpKSxpPTAsbz1uLmxlbmd0aDtpPG87aSsrKXt2YXIgcj1lLmdldFBhcmVudEZyb21FbGVtZW50KG5baV0pLHM9QS5nZXREYXRhKG5baV0sZmUpLGE9e3JlbGF0ZWRUYXJnZXQ6bltpXX07aWYodCYmXCJjbGlja1wiPT09dC50eXBlJiYoYS5jbGlja0V2ZW50PXQpLHMpe3ZhciBsPXMuX21lbnU7aWYoci5jbGFzc0xpc3QuY29udGFpbnMoX2UpKWlmKCEodCYmKFwiY2xpY2tcIj09PXQudHlwZSYmL2lucHV0fHRleHRhcmVhL2kudGVzdCh0LnRhcmdldC50YWdOYW1lKXx8XCJrZXl1cFwiPT09dC50eXBlJiY5PT09dC53aGljaCkmJnIuY29udGFpbnModC50YXJnZXQpKSlaLnRyaWdnZXIocixnZS5ISURFLGEpLmRlZmF1bHRQcmV2ZW50ZWR8fChcIm9udG91Y2hzdGFydFwiaW4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50JiZFKGRvY3VtZW50LmJvZHkuY2hpbGRyZW4pLmZvckVhY2goKGZ1bmN0aW9uKHQpe3JldHVybiBaLm9mZih0LFwibW91c2VvdmVyXCIsbnVsbCwoZnVuY3Rpb24oKXt9KSl9KSksbltpXS5zZXRBdHRyaWJ1dGUoXCJhcmlhLWV4cGFuZGVkXCIsXCJmYWxzZVwiKSxzLl9wb3BwZXImJnMuX3BvcHBlci5kZXN0cm95KCksbC5jbGFzc0xpc3QucmVtb3ZlKF9lKSxyLmNsYXNzTGlzdC5yZW1vdmUoX2UpLFoudHJpZ2dlcihyLGdlLkhJRERFTixhKSl9fX0sZS5nZXRQYXJlbnRGcm9tRWxlbWVudD1mdW5jdGlvbih0KXtyZXR1cm4gZCh0KXx8dC5wYXJlbnROb2RlfSxlLmRhdGFBcGlLZXlkb3duSGFuZGxlcj1mdW5jdGlvbih0KXtpZigoL2lucHV0fHRleHRhcmVhL2kudGVzdCh0LnRhcmdldC50YWdOYW1lKT8hKDMyPT09dC53aGljaHx8MjchPT10LndoaWNoJiYoNDAhPT10LndoaWNoJiYzOCE9PXQud2hpY2h8fCQuY2xvc2VzdCh0LnRhcmdldCxJZSkpKTpkZS50ZXN0KHQud2hpY2gpKSYmKHQucHJldmVudERlZmF1bHQoKSx0LnN0b3BQcm9wYWdhdGlvbigpLCF0aGlzLmRpc2FibGVkJiYhdGhpcy5jbGFzc0xpc3QuY29udGFpbnMocGUpKSl7dmFyIG49ZS5nZXRQYXJlbnRGcm9tRWxlbWVudCh0aGlzKSxpPW4uY2xhc3NMaXN0LmNvbnRhaW5zKF9lKTtpZighaXx8aSYmKDI3PT09dC53aGljaHx8MzI9PT10LndoaWNoKSlyZXR1cm4gMjc9PT10LndoaWNoJiYkLmZpbmRPbmUoRGUsbikuZm9jdXMoKSx2b2lkIGUuY2xlYXJNZW51cygpO3ZhciBvPUUoJC5maW5kKFNlLG4pKS5maWx0ZXIoeSk7aWYoby5sZW5ndGgpe3ZhciByPW8uaW5kZXhPZih0LnRhcmdldCk7Mzg9PT10LndoaWNoJiZyPjAmJnItLSw0MD09PXQud2hpY2gmJnI8by5sZW5ndGgtMSYmcisrLHI8MCYmKHI9MCksb1tyXS5mb2N1cygpfX19LGUuZ2V0SW5zdGFuY2U9ZnVuY3Rpb24odCl7cmV0dXJuIEEuZ2V0RGF0YSh0LGZlKX0sbihlLG51bGwsW3trZXk6XCJWRVJTSU9OXCIsZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuXCI0LjMuMVwifX0se2tleTpcIkRlZmF1bHRcIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gUGV9fSx7a2V5OlwiRGVmYXVsdFR5cGVcIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gSGV9fV0pLGV9KCk7Wi5vbihkb2N1bWVudCxnZS5LRVlET1dOX0RBVEFfQVBJLERlLGplLmRhdGFBcGlLZXlkb3duSGFuZGxlciksWi5vbihkb2N1bWVudCxnZS5LRVlET1dOX0RBVEFfQVBJLEllLGplLmRhdGFBcGlLZXlkb3duSGFuZGxlciksWi5vbihkb2N1bWVudCxnZS5DTElDS19EQVRBX0FQSSxqZS5jbGVhck1lbnVzKSxaLm9uKGRvY3VtZW50LGdlLktFWVVQX0RBVEFfQVBJLGplLmNsZWFyTWVudXMpLFoub24oZG9jdW1lbnQsZ2UuQ0xJQ0tfREFUQV9BUEksRGUsKGZ1bmN0aW9uKHQpe3QucHJldmVudERlZmF1bHQoKSx0LnN0b3BQcm9wYWdhdGlvbigpLGplLmRyb3Bkb3duSW50ZXJmYWNlKHRoaXMsXCJ0b2dnbGVcIil9KSksWi5vbihkb2N1bWVudCxnZS5DTElDS19EQVRBX0FQSSxUZSwoZnVuY3Rpb24odCl7cmV0dXJuIHQuc3RvcFByb3BhZ2F0aW9uKCl9KSk7dmFyIE1lPVQoKTtpZihNZSl7dmFyIFJlPU1lLmZuW3VlXTtNZS5mblt1ZV09amUualF1ZXJ5SW50ZXJmYWNlLE1lLmZuW3VlXS5Db25zdHJ1Y3Rvcj1qZSxNZS5mblt1ZV0ubm9Db25mbGljdD1mdW5jdGlvbigpe3JldHVybiBNZS5mblt1ZV09UmUsamUualF1ZXJ5SW50ZXJmYWNlfX12YXIgeGU9XCJicy5tb2RhbFwiLFdlPVwiLlwiK3hlLFVlPXtiYWNrZHJvcDohMCxrZXlib2FyZDohMCxmb2N1czohMCxzaG93OiEwfSxLZT17YmFja2Ryb3A6XCIoYm9vbGVhbnxzdHJpbmcpXCIsa2V5Ym9hcmQ6XCJib29sZWFuXCIsZm9jdXM6XCJib29sZWFuXCIsc2hvdzpcImJvb2xlYW5cIn0sVmU9e0hJREU6XCJoaWRlXCIrV2UsSElERV9QUkVWRU5URUQ6XCJoaWRlUHJldmVudGVkXCIrV2UsSElEREVOOlwiaGlkZGVuXCIrV2UsU0hPVzpcInNob3dcIitXZSxTSE9XTjpcInNob3duXCIrV2UsRk9DVVNJTjpcImZvY3VzaW5cIitXZSxSRVNJWkU6XCJyZXNpemVcIitXZSxDTElDS19ESVNNSVNTOlwiY2xpY2suZGlzbWlzc1wiK1dlLEtFWURPV05fRElTTUlTUzpcImtleWRvd24uZGlzbWlzc1wiK1dlLE1PVVNFVVBfRElTTUlTUzpcIm1vdXNldXAuZGlzbWlzc1wiK1dlLE1PVVNFRE9XTl9ESVNNSVNTOlwibW91c2Vkb3duLmRpc21pc3NcIitXZSxDTElDS19EQVRBX0FQSTpcImNsaWNrXCIrV2UrXCIuZGF0YS1hcGlcIn0sQmU9XCJtb2RhbC1kaWFsb2ctc2Nyb2xsYWJsZVwiLEZlPVwibW9kYWwtc2Nyb2xsYmFyLW1lYXN1cmVcIixRZT1cIm1vZGFsLWJhY2tkcm9wXCIsWWU9XCJtb2RhbC1vcGVuXCIsR2U9XCJmYWRlXCIsWGU9XCJzaG93XCIscWU9XCJtb2RhbC1zdGF0aWNcIix6ZT17RElBTE9HOlwiLm1vZGFsLWRpYWxvZ1wiLE1PREFMX0JPRFk6XCIubW9kYWwtYm9keVwiLERBVEFfVE9HR0xFOidbZGF0YS10b2dnbGU9XCJtb2RhbFwiXScsREFUQV9ESVNNSVNTOidbZGF0YS1kaXNtaXNzPVwibW9kYWxcIl0nLEZJWEVEX0NPTlRFTlQ6XCIuZml4ZWQtdG9wLCAuZml4ZWQtYm90dG9tLCAuaXMtZml4ZWQsIC5zdGlja3ktdG9wXCIsU1RJQ0tZX0NPTlRFTlQ6XCIuc3RpY2t5LXRvcFwifSxaZT1mdW5jdGlvbigpe2Z1bmN0aW9uIHQodCxlKXt0aGlzLl9jb25maWc9dGhpcy5fZ2V0Q29uZmlnKGUpLHRoaXMuX2VsZW1lbnQ9dCx0aGlzLl9kaWFsb2c9JC5maW5kT25lKHplLkRJQUxPRyx0KSx0aGlzLl9iYWNrZHJvcD1udWxsLHRoaXMuX2lzU2hvd249ITEsdGhpcy5faXNCb2R5T3ZlcmZsb3dpbmc9ITEsdGhpcy5faWdub3JlQmFja2Ryb3BDbGljaz0hMSx0aGlzLl9pc1RyYW5zaXRpb25pbmc9ITEsdGhpcy5fc2Nyb2xsYmFyV2lkdGg9MCxBLnNldERhdGEodCx4ZSx0aGlzKX12YXIgZT10LnByb3RvdHlwZTtyZXR1cm4gZS50b2dnbGU9ZnVuY3Rpb24odCl7cmV0dXJuIHRoaXMuX2lzU2hvd24/dGhpcy5oaWRlKCk6dGhpcy5zaG93KHQpfSxlLnNob3c9ZnVuY3Rpb24odCl7dmFyIGU9dGhpcztpZighdGhpcy5faXNTaG93biYmIXRoaXMuX2lzVHJhbnNpdGlvbmluZyl7dGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoR2UpJiYodGhpcy5faXNUcmFuc2l0aW9uaW5nPSEwKTt2YXIgbj1aLnRyaWdnZXIodGhpcy5fZWxlbWVudCxWZS5TSE9XLHtyZWxhdGVkVGFyZ2V0OnR9KTt0aGlzLl9pc1Nob3dufHxuLmRlZmF1bHRQcmV2ZW50ZWR8fCh0aGlzLl9pc1Nob3duPSEwLHRoaXMuX2NoZWNrU2Nyb2xsYmFyKCksdGhpcy5fc2V0U2Nyb2xsYmFyKCksdGhpcy5fYWRqdXN0RGlhbG9nKCksdGhpcy5fc2V0RXNjYXBlRXZlbnQoKSx0aGlzLl9zZXRSZXNpemVFdmVudCgpLFoub24odGhpcy5fZWxlbWVudCxWZS5DTElDS19ESVNNSVNTLHplLkRBVEFfRElTTUlTUywoZnVuY3Rpb24odCl7cmV0dXJuIGUuaGlkZSh0KX0pKSxaLm9uKHRoaXMuX2RpYWxvZyxWZS5NT1VTRURPV05fRElTTUlTUywoZnVuY3Rpb24oKXtaLm9uZShlLl9lbGVtZW50LFZlLk1PVVNFVVBfRElTTUlTUywoZnVuY3Rpb24odCl7dC50YXJnZXQ9PT1lLl9lbGVtZW50JiYoZS5faWdub3JlQmFja2Ryb3BDbGljaz0hMCl9KSl9KSksdGhpcy5fc2hvd0JhY2tkcm9wKChmdW5jdGlvbigpe3JldHVybiBlLl9zaG93RWxlbWVudCh0KX0pKSl9fSxlLmhpZGU9ZnVuY3Rpb24odCl7dmFyIGU9dGhpcztpZigodCYmdC5wcmV2ZW50RGVmYXVsdCgpLHRoaXMuX2lzU2hvd24mJiF0aGlzLl9pc1RyYW5zaXRpb25pbmcpJiYhWi50cmlnZ2VyKHRoaXMuX2VsZW1lbnQsVmUuSElERSkuZGVmYXVsdFByZXZlbnRlZCl7dGhpcy5faXNTaG93bj0hMTt2YXIgbj10aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5jb250YWlucyhHZSk7aWYobiYmKHRoaXMuX2lzVHJhbnNpdGlvbmluZz0hMCksdGhpcy5fc2V0RXNjYXBlRXZlbnQoKSx0aGlzLl9zZXRSZXNpemVFdmVudCgpLFoub2ZmKGRvY3VtZW50LFZlLkZPQ1VTSU4pLHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShYZSksWi5vZmYodGhpcy5fZWxlbWVudCxWZS5DTElDS19ESVNNSVNTKSxaLm9mZih0aGlzLl9kaWFsb2csVmUuTU9VU0VET1dOX0RJU01JU1MpLG4pe3ZhciBpPWcodGhpcy5fZWxlbWVudCk7Wi5vbmUodGhpcy5fZWxlbWVudCxjLChmdW5jdGlvbih0KXtyZXR1cm4gZS5faGlkZU1vZGFsKHQpfSkpLG0odGhpcy5fZWxlbWVudCxpKX1lbHNlIHRoaXMuX2hpZGVNb2RhbCgpfX0sZS5kaXNwb3NlPWZ1bmN0aW9uKCl7W3dpbmRvdyx0aGlzLl9lbGVtZW50LHRoaXMuX2RpYWxvZ10uZm9yRWFjaCgoZnVuY3Rpb24odCl7cmV0dXJuIFoub2ZmKHQsV2UpfSkpLFoub2ZmKGRvY3VtZW50LFZlLkZPQ1VTSU4pLEEucmVtb3ZlRGF0YSh0aGlzLl9lbGVtZW50LHhlKSx0aGlzLl9jb25maWc9bnVsbCx0aGlzLl9lbGVtZW50PW51bGwsdGhpcy5fZGlhbG9nPW51bGwsdGhpcy5fYmFja2Ryb3A9bnVsbCx0aGlzLl9pc1Nob3duPW51bGwsdGhpcy5faXNCb2R5T3ZlcmZsb3dpbmc9bnVsbCx0aGlzLl9pZ25vcmVCYWNrZHJvcENsaWNrPW51bGwsdGhpcy5faXNUcmFuc2l0aW9uaW5nPW51bGwsdGhpcy5fc2Nyb2xsYmFyV2lkdGg9bnVsbH0sZS5oYW5kbGVVcGRhdGU9ZnVuY3Rpb24oKXt0aGlzLl9hZGp1c3REaWFsb2coKX0sZS5fZ2V0Q29uZmlnPWZ1bmN0aW9uKHQpe3JldHVybiB0PXIoe30sVWUse30sdCksdihcIm1vZGFsXCIsdCxLZSksdH0sZS5fc2hvd0VsZW1lbnQ9ZnVuY3Rpb24odCl7dmFyIGU9dGhpcyxuPXRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKEdlKSxpPSQuZmluZE9uZSh6ZS5NT0RBTF9CT0RZLHRoaXMuX2RpYWxvZyk7dGhpcy5fZWxlbWVudC5wYXJlbnROb2RlJiZ0aGlzLl9lbGVtZW50LnBhcmVudE5vZGUubm9kZVR5cGU9PT1Ob2RlLkVMRU1FTlRfTk9ERXx8ZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCh0aGlzLl9lbGVtZW50KSx0aGlzLl9lbGVtZW50LnN0eWxlLmRpc3BsYXk9XCJibG9ja1wiLHRoaXMuX2VsZW1lbnQucmVtb3ZlQXR0cmlidXRlKFwiYXJpYS1oaWRkZW5cIiksdGhpcy5fZWxlbWVudC5zZXRBdHRyaWJ1dGUoXCJhcmlhLW1vZGFsXCIsITApLHRoaXMuX2RpYWxvZy5jbGFzc0xpc3QuY29udGFpbnMoQmUpJiZpP2kuc2Nyb2xsVG9wPTA6dGhpcy5fZWxlbWVudC5zY3JvbGxUb3A9MCxuJiZEKHRoaXMuX2VsZW1lbnQpLHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmFkZChYZSksdGhpcy5fY29uZmlnLmZvY3VzJiZ0aGlzLl9lbmZvcmNlRm9jdXMoKTt2YXIgbz1mdW5jdGlvbigpe2UuX2NvbmZpZy5mb2N1cyYmZS5fZWxlbWVudC5mb2N1cygpLGUuX2lzVHJhbnNpdGlvbmluZz0hMSxaLnRyaWdnZXIoZS5fZWxlbWVudCxWZS5TSE9XTix7cmVsYXRlZFRhcmdldDp0fSl9O2lmKG4pe3ZhciByPWcodGhpcy5fZGlhbG9nKTtaLm9uZSh0aGlzLl9kaWFsb2csYyxvKSxtKHRoaXMuX2RpYWxvZyxyKX1lbHNlIG8oKX0sZS5fZW5mb3JjZUZvY3VzPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcztaLm9mZihkb2N1bWVudCxWZS5GT0NVU0lOKSxaLm9uKGRvY3VtZW50LFZlLkZPQ1VTSU4sKGZ1bmN0aW9uKGUpe2RvY3VtZW50PT09ZS50YXJnZXR8fHQuX2VsZW1lbnQ9PT1lLnRhcmdldHx8dC5fZWxlbWVudC5jb250YWlucyhlLnRhcmdldCl8fHQuX2VsZW1lbnQuZm9jdXMoKX0pKX0sZS5fc2V0RXNjYXBlRXZlbnQ9ZnVuY3Rpb24oKXt2YXIgdD10aGlzO3RoaXMuX2lzU2hvd24mJnRoaXMuX2NvbmZpZy5rZXlib2FyZD9aLm9uKHRoaXMuX2VsZW1lbnQsVmUuS0VZRE9XTl9ESVNNSVNTLChmdW5jdGlvbihlKXsyNz09PWUud2hpY2gmJnQuX3RyaWdnZXJCYWNrZHJvcFRyYW5zaXRpb24oKX0pKTpaLm9mZih0aGlzLl9lbGVtZW50LFZlLktFWURPV05fRElTTUlTUyl9LGUuX3NldFJlc2l6ZUV2ZW50PWZ1bmN0aW9uKCl7dmFyIHQ9dGhpczt0aGlzLl9pc1Nob3duP1oub24od2luZG93LFZlLlJFU0laRSwoZnVuY3Rpb24oKXtyZXR1cm4gdC5fYWRqdXN0RGlhbG9nKCl9KSk6Wi5vZmYod2luZG93LFZlLlJFU0laRSl9LGUuX2hpZGVNb2RhbD1mdW5jdGlvbigpe3ZhciB0PXRoaXM7dGhpcy5fZWxlbWVudC5zdHlsZS5kaXNwbGF5PVwibm9uZVwiLHRoaXMuX2VsZW1lbnQuc2V0QXR0cmlidXRlKFwiYXJpYS1oaWRkZW5cIiwhMCksdGhpcy5fZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoXCJhcmlhLW1vZGFsXCIpLHRoaXMuX2lzVHJhbnNpdGlvbmluZz0hMSx0aGlzLl9zaG93QmFja2Ryb3AoKGZ1bmN0aW9uKCl7ZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QucmVtb3ZlKFllKSx0Ll9yZXNldEFkanVzdG1lbnRzKCksdC5fcmVzZXRTY3JvbGxiYXIoKSxaLnRyaWdnZXIodC5fZWxlbWVudCxWZS5ISURERU4pfSkpfSxlLl9yZW1vdmVCYWNrZHJvcD1mdW5jdGlvbigpe3RoaXMuX2JhY2tkcm9wLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5fYmFja2Ryb3ApLHRoaXMuX2JhY2tkcm9wPW51bGx9LGUuX3Nob3dCYWNrZHJvcD1mdW5jdGlvbih0KXt2YXIgZT10aGlzLG49dGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoR2UpP0dlOlwiXCI7aWYodGhpcy5faXNTaG93biYmdGhpcy5fY29uZmlnLmJhY2tkcm9wKXtpZih0aGlzLl9iYWNrZHJvcD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpLHRoaXMuX2JhY2tkcm9wLmNsYXNzTmFtZT1RZSxuJiZ0aGlzLl9iYWNrZHJvcC5jbGFzc0xpc3QuYWRkKG4pLGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5fYmFja2Ryb3ApLFoub24odGhpcy5fZWxlbWVudCxWZS5DTElDS19ESVNNSVNTLChmdW5jdGlvbih0KXtlLl9pZ25vcmVCYWNrZHJvcENsaWNrP2UuX2lnbm9yZUJhY2tkcm9wQ2xpY2s9ITE6dC50YXJnZXQ9PT10LmN1cnJlbnRUYXJnZXQmJmUuX3RyaWdnZXJCYWNrZHJvcFRyYW5zaXRpb24oKX0pKSxuJiZEKHRoaXMuX2JhY2tkcm9wKSx0aGlzLl9iYWNrZHJvcC5jbGFzc0xpc3QuYWRkKFhlKSwhbilyZXR1cm4gdm9pZCB0KCk7dmFyIGk9Zyh0aGlzLl9iYWNrZHJvcCk7Wi5vbmUodGhpcy5fYmFja2Ryb3AsYyx0KSxtKHRoaXMuX2JhY2tkcm9wLGkpfWVsc2UgaWYoIXRoaXMuX2lzU2hvd24mJnRoaXMuX2JhY2tkcm9wKXt0aGlzLl9iYWNrZHJvcC5jbGFzc0xpc3QucmVtb3ZlKFhlKTt2YXIgbz1mdW5jdGlvbigpe2UuX3JlbW92ZUJhY2tkcm9wKCksdCgpfTtpZih0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5jb250YWlucyhHZSkpe3ZhciByPWcodGhpcy5fYmFja2Ryb3ApO1oub25lKHRoaXMuX2JhY2tkcm9wLGMsbyksbSh0aGlzLl9iYWNrZHJvcCxyKX1lbHNlIG8oKX1lbHNlIHQoKX0sZS5fdHJpZ2dlckJhY2tkcm9wVHJhbnNpdGlvbj1mdW5jdGlvbigpe3ZhciB0PXRoaXM7aWYoXCJzdGF0aWNcIj09PXRoaXMuX2NvbmZpZy5iYWNrZHJvcCl7aWYoWi50cmlnZ2VyKHRoaXMuX2VsZW1lbnQsVmUuSElERV9QUkVWRU5URUQpLmRlZmF1bHRQcmV2ZW50ZWQpcmV0dXJuO3RoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmFkZChxZSk7dmFyIGU9Zyh0aGlzLl9lbGVtZW50KTtaLm9uZSh0aGlzLl9lbGVtZW50LGMsKGZ1bmN0aW9uKCl7dC5fZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKHFlKX0pKSxtKHRoaXMuX2VsZW1lbnQsZSksdGhpcy5fZWxlbWVudC5mb2N1cygpfWVsc2UgdGhpcy5oaWRlKCl9LGUuX2FkanVzdERpYWxvZz1mdW5jdGlvbigpe3ZhciB0PXRoaXMuX2VsZW1lbnQuc2Nyb2xsSGVpZ2h0PmRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQ7IXRoaXMuX2lzQm9keU92ZXJmbG93aW5nJiZ0JiYodGhpcy5fZWxlbWVudC5zdHlsZS5wYWRkaW5nTGVmdD10aGlzLl9zY3JvbGxiYXJXaWR0aCtcInB4XCIpLHRoaXMuX2lzQm9keU92ZXJmbG93aW5nJiYhdCYmKHRoaXMuX2VsZW1lbnQuc3R5bGUucGFkZGluZ1JpZ2h0PXRoaXMuX3Njcm9sbGJhcldpZHRoK1wicHhcIil9LGUuX3Jlc2V0QWRqdXN0bWVudHM9ZnVuY3Rpb24oKXt0aGlzLl9lbGVtZW50LnN0eWxlLnBhZGRpbmdMZWZ0PVwiXCIsdGhpcy5fZWxlbWVudC5zdHlsZS5wYWRkaW5nUmlnaHQ9XCJcIn0sZS5fY2hlY2tTY3JvbGxiYXI9ZnVuY3Rpb24oKXt2YXIgdD1kb2N1bWVudC5ib2R5LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO3RoaXMuX2lzQm9keU92ZXJmbG93aW5nPXQubGVmdCt0LnJpZ2h0PHdpbmRvdy5pbm5lcldpZHRoLHRoaXMuX3Njcm9sbGJhcldpZHRoPXRoaXMuX2dldFNjcm9sbGJhcldpZHRoKCl9LGUuX3NldFNjcm9sbGJhcj1mdW5jdGlvbigpe3ZhciB0PXRoaXM7aWYodGhpcy5faXNCb2R5T3ZlcmZsb3dpbmcpe0UoJC5maW5kKHplLkZJWEVEX0NPTlRFTlQpKS5mb3JFYWNoKChmdW5jdGlvbihlKXt2YXIgbj1lLnN0eWxlLnBhZGRpbmdSaWdodCxpPXdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGUpW1wicGFkZGluZy1yaWdodFwiXTtJdC5zZXREYXRhQXR0cmlidXRlKGUsXCJwYWRkaW5nLXJpZ2h0XCIsbiksZS5zdHlsZS5wYWRkaW5nUmlnaHQ9cGFyc2VGbG9hdChpKSt0Ll9zY3JvbGxiYXJXaWR0aCtcInB4XCJ9KSksRSgkLmZpbmQoemUuU1RJQ0tZX0NPTlRFTlQpKS5mb3JFYWNoKChmdW5jdGlvbihlKXt2YXIgbj1lLnN0eWxlLm1hcmdpblJpZ2h0LGk9d2luZG93LmdldENvbXB1dGVkU3R5bGUoZSlbXCJtYXJnaW4tcmlnaHRcIl07SXQuc2V0RGF0YUF0dHJpYnV0ZShlLFwibWFyZ2luLXJpZ2h0XCIsbiksZS5zdHlsZS5tYXJnaW5SaWdodD1wYXJzZUZsb2F0KGkpLXQuX3Njcm9sbGJhcldpZHRoK1wicHhcIn0pKTt2YXIgZT1kb2N1bWVudC5ib2R5LnN0eWxlLnBhZGRpbmdSaWdodCxuPXdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGRvY3VtZW50LmJvZHkpW1wicGFkZGluZy1yaWdodFwiXTtJdC5zZXREYXRhQXR0cmlidXRlKGRvY3VtZW50LmJvZHksXCJwYWRkaW5nLXJpZ2h0XCIsZSksZG9jdW1lbnQuYm9keS5zdHlsZS5wYWRkaW5nUmlnaHQ9cGFyc2VGbG9hdChuKSt0aGlzLl9zY3JvbGxiYXJXaWR0aCtcInB4XCJ9ZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QuYWRkKFllKX0sZS5fcmVzZXRTY3JvbGxiYXI9ZnVuY3Rpb24oKXtFKCQuZmluZCh6ZS5GSVhFRF9DT05URU5UKSkuZm9yRWFjaCgoZnVuY3Rpb24odCl7dmFyIGU9SXQuZ2V0RGF0YUF0dHJpYnV0ZSh0LFwicGFkZGluZy1yaWdodFwiKTtcInVuZGVmaW5lZFwiIT10eXBlb2YgZSYmKEl0LnJlbW92ZURhdGFBdHRyaWJ1dGUodCxcInBhZGRpbmctcmlnaHRcIiksdC5zdHlsZS5wYWRkaW5nUmlnaHQ9ZSl9KSksRSgkLmZpbmQoXCJcIit6ZS5TVElDS1lfQ09OVEVOVCkpLmZvckVhY2goKGZ1bmN0aW9uKHQpe3ZhciBlPUl0LmdldERhdGFBdHRyaWJ1dGUodCxcIm1hcmdpbi1yaWdodFwiKTtcInVuZGVmaW5lZFwiIT10eXBlb2YgZSYmKEl0LnJlbW92ZURhdGFBdHRyaWJ1dGUodCxcIm1hcmdpbi1yaWdodFwiKSx0LnN0eWxlLm1hcmdpblJpZ2h0PWUpfSkpO3ZhciB0PUl0LmdldERhdGFBdHRyaWJ1dGUoZG9jdW1lbnQuYm9keSxcInBhZGRpbmctcmlnaHRcIik7XCJ1bmRlZmluZWRcIj09dHlwZW9mIHQ/ZG9jdW1lbnQuYm9keS5zdHlsZS5wYWRkaW5nUmlnaHQ9XCJcIjooSXQucmVtb3ZlRGF0YUF0dHJpYnV0ZShkb2N1bWVudC5ib2R5LFwicGFkZGluZy1yaWdodFwiKSxkb2N1bWVudC5ib2R5LnN0eWxlLnBhZGRpbmdSaWdodD10KX0sZS5fZ2V0U2Nyb2xsYmFyV2lkdGg9ZnVuY3Rpb24oKXt2YXIgdD1kb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO3QuY2xhc3NOYW1lPUZlLGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodCk7dmFyIGU9dC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aC10LmNsaWVudFdpZHRoO3JldHVybiBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHQpLGV9LHQualF1ZXJ5SW50ZXJmYWNlPWZ1bmN0aW9uKGUsbil7cmV0dXJuIHRoaXMuZWFjaCgoZnVuY3Rpb24oKXt2YXIgaT1BLmdldERhdGEodGhpcyx4ZSksbz1yKHt9LFVlLHt9LEl0LmdldERhdGFBdHRyaWJ1dGVzKHRoaXMpLHt9LFwib2JqZWN0XCI9PXR5cGVvZiBlJiZlP2U6e30pO2lmKGl8fChpPW5ldyB0KHRoaXMsbykpLFwic3RyaW5nXCI9PXR5cGVvZiBlKXtpZihcInVuZGVmaW5lZFwiPT10eXBlb2YgaVtlXSl0aHJvdyBuZXcgVHlwZUVycm9yKCdObyBtZXRob2QgbmFtZWQgXCInK2UrJ1wiJyk7aVtlXShuKX1lbHNlIG8uc2hvdyYmaS5zaG93KG4pfSkpfSx0LmdldEluc3RhbmNlPWZ1bmN0aW9uKHQpe3JldHVybiBBLmdldERhdGEodCx4ZSl9LG4odCxudWxsLFt7a2V5OlwiVkVSU0lPTlwiLGdldDpmdW5jdGlvbigpe3JldHVyblwiNC4zLjFcIn19LHtrZXk6XCJEZWZhdWx0XCIsZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuIFVlfX1dKSx0fSgpO1oub24oZG9jdW1lbnQsVmUuQ0xJQ0tfREFUQV9BUEksemUuREFUQV9UT0dHTEUsKGZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMsbj1kKHRoaXMpO1wiQVwiIT09dGhpcy50YWdOYW1lJiZcIkFSRUFcIiE9PXRoaXMudGFnTmFtZXx8dC5wcmV2ZW50RGVmYXVsdCgpLFoub25lKG4sVmUuU0hPVywoZnVuY3Rpb24odCl7dC5kZWZhdWx0UHJldmVudGVkfHxaLm9uZShuLFZlLkhJRERFTiwoZnVuY3Rpb24oKXt5KGUpJiZlLmZvY3VzKCl9KSl9KSk7dmFyIGk9QS5nZXREYXRhKG4seGUpO2lmKCFpKXt2YXIgbz1yKHt9LEl0LmdldERhdGFBdHRyaWJ1dGVzKG4pLHt9LEl0LmdldERhdGFBdHRyaWJ1dGVzKHRoaXMpKTtpPW5ldyBaZShuLG8pfWkuc2hvdyh0aGlzKX0pKTt2YXIgJGU9VCgpO2lmKCRlKXt2YXIgSmU9JGUuZm4ubW9kYWw7JGUuZm4ubW9kYWw9WmUualF1ZXJ5SW50ZXJmYWNlLCRlLmZuLm1vZGFsLkNvbnN0cnVjdG9yPVplLCRlLmZuLm1vZGFsLm5vQ29uZmxpY3Q9ZnVuY3Rpb24oKXtyZXR1cm4gJGUuZm4ubW9kYWw9SmUsWmUualF1ZXJ5SW50ZXJmYWNlfX12YXIgdG49W1wiYmFja2dyb3VuZFwiLFwiY2l0ZVwiLFwiaHJlZlwiLFwiaXRlbXR5cGVcIixcImxvbmdkZXNjXCIsXCJwb3N0ZXJcIixcInNyY1wiLFwieGxpbms6aHJlZlwiXSxlbj0vXig/Oig/Omh0dHBzP3xtYWlsdG98ZnRwfHRlbHxmaWxlKTp8W14mOi8/I10qKD86Wy8/I118JCkpL2dpLG5uPS9eZGF0YTooPzppbWFnZVxcLyg/OmJtcHxnaWZ8anBlZ3xqcGd8cG5nfHRpZmZ8d2VicCl8dmlkZW9cXC8oPzptcGVnfG1wNHxvZ2d8d2VibSl8YXVkaW9cXC8oPzptcDN8b2dhfG9nZ3xvcHVzKSk7YmFzZTY0LFthLXowLTkrL10rPSokL2ksb249ZnVuY3Rpb24odCxlKXt2YXIgbj10Lm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk7aWYoLTEhPT1lLmluZGV4T2YobikpcmV0dXJuLTE9PT10bi5pbmRleE9mKG4pfHxCb29sZWFuKHQubm9kZVZhbHVlLm1hdGNoKGVuKXx8dC5ub2RlVmFsdWUubWF0Y2gobm4pKTtmb3IodmFyIGk9ZS5maWx0ZXIoKGZ1bmN0aW9uKHQpe3JldHVybiB0IGluc3RhbmNlb2YgUmVnRXhwfSkpLG89MCxyPWkubGVuZ3RoO288cjtvKyspaWYobi5tYXRjaChpW29dKSlyZXR1cm4hMDtyZXR1cm4hMX0scm49e1wiKlwiOltcImNsYXNzXCIsXCJkaXJcIixcImlkXCIsXCJsYW5nXCIsXCJyb2xlXCIsL15hcmlhLVtcXHctXSokL2ldLGE6W1widGFyZ2V0XCIsXCJocmVmXCIsXCJ0aXRsZVwiLFwicmVsXCJdLGFyZWE6W10sYjpbXSxicjpbXSxjb2w6W10sY29kZTpbXSxkaXY6W10sZW06W10saHI6W10saDE6W10saDI6W10saDM6W10saDQ6W10saDU6W10saDY6W10saTpbXSxpbWc6W1wic3JjXCIsXCJhbHRcIixcInRpdGxlXCIsXCJ3aWR0aFwiLFwiaGVpZ2h0XCJdLGxpOltdLG9sOltdLHA6W10scHJlOltdLHM6W10sc21hbGw6W10sc3BhbjpbXSxzdWI6W10sc3VwOltdLHN0cm9uZzpbXSx1OltdLHVsOltdfTtmdW5jdGlvbiBzbih0LGUsbil7aWYoIXQubGVuZ3RoKXJldHVybiB0O2lmKG4mJlwiZnVuY3Rpb25cIj09dHlwZW9mIG4pcmV0dXJuIG4odCk7Zm9yKHZhciBpPShuZXcgd2luZG93LkRPTVBhcnNlcikucGFyc2VGcm9tU3RyaW5nKHQsXCJ0ZXh0L2h0bWxcIiksbz1PYmplY3Qua2V5cyhlKSxyPUUoaS5ib2R5LnF1ZXJ5U2VsZWN0b3JBbGwoXCIqXCIpKSxzPWZ1bmN0aW9uKHQsbil7dmFyIGk9clt0XSxzPWkubm9kZU5hbWUudG9Mb3dlckNhc2UoKTtpZigtMT09PW8uaW5kZXhPZihzKSlyZXR1cm4gaS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGkpLFwiY29udGludWVcIjt2YXIgYT1FKGkuYXR0cmlidXRlcyksbD1bXS5jb25jYXQoZVtcIipcIl18fFtdLGVbc118fFtdKTthLmZvckVhY2goKGZ1bmN0aW9uKHQpe29uKHQsbCl8fGkucmVtb3ZlQXR0cmlidXRlKHQubm9kZU5hbWUpfSkpfSxhPTAsbD1yLmxlbmd0aDthPGw7YSsrKXMoYSk7cmV0dXJuIGkuYm9keS5pbm5lckhUTUx9dmFyIGFuPVwidG9vbHRpcFwiLGxuPW5ldyBSZWdFeHAoXCIoXnxcXFxccylicy10b29sdGlwXFxcXFMrXCIsXCJnXCIpLGNuPVtcInNhbml0aXplXCIsXCJ3aGl0ZUxpc3RcIixcInNhbml0aXplRm5cIl0sdW49e2FuaW1hdGlvbjpcImJvb2xlYW5cIix0ZW1wbGF0ZTpcInN0cmluZ1wiLHRpdGxlOlwiKHN0cmluZ3xlbGVtZW50fGZ1bmN0aW9uKVwiLHRyaWdnZXI6XCJzdHJpbmdcIixkZWxheTpcIihudW1iZXJ8b2JqZWN0KVwiLGh0bWw6XCJib29sZWFuXCIsc2VsZWN0b3I6XCIoc3RyaW5nfGJvb2xlYW4pXCIscGxhY2VtZW50OlwiKHN0cmluZ3xmdW5jdGlvbilcIixvZmZzZXQ6XCIobnVtYmVyfHN0cmluZ3xmdW5jdGlvbilcIixjb250YWluZXI6XCIoc3RyaW5nfGVsZW1lbnR8Ym9vbGVhbilcIixmYWxsYmFja1BsYWNlbWVudDpcIihzdHJpbmd8YXJyYXkpXCIsYm91bmRhcnk6XCIoc3RyaW5nfGVsZW1lbnQpXCIsc2FuaXRpemU6XCJib29sZWFuXCIsc2FuaXRpemVGbjpcIihudWxsfGZ1bmN0aW9uKVwiLHdoaXRlTGlzdDpcIm9iamVjdFwiLHBvcHBlckNvbmZpZzpcIihudWxsfG9iamVjdClcIn0sZm49e0FVVE86XCJhdXRvXCIsVE9QOlwidG9wXCIsUklHSFQ6XCJyaWdodFwiLEJPVFRPTTpcImJvdHRvbVwiLExFRlQ6XCJsZWZ0XCJ9LGhuPXthbmltYXRpb246ITAsdGVtcGxhdGU6JzxkaXYgY2xhc3M9XCJ0b29sdGlwXCIgcm9sZT1cInRvb2x0aXBcIj48ZGl2IGNsYXNzPVwidG9vbHRpcC1hcnJvd1wiPjwvZGl2PjxkaXYgY2xhc3M9XCJ0b29sdGlwLWlubmVyXCI+PC9kaXY+PC9kaXY+Jyx0cmlnZ2VyOlwiaG92ZXIgZm9jdXNcIix0aXRsZTpcIlwiLGRlbGF5OjAsaHRtbDohMSxzZWxlY3RvcjohMSxwbGFjZW1lbnQ6XCJ0b3BcIixvZmZzZXQ6MCxjb250YWluZXI6ITEsZmFsbGJhY2tQbGFjZW1lbnQ6XCJmbGlwXCIsYm91bmRhcnk6XCJzY3JvbGxQYXJlbnRcIixzYW5pdGl6ZTohMCxzYW5pdGl6ZUZuOm51bGwsd2hpdGVMaXN0OnJuLHBvcHBlckNvbmZpZzpudWxsfSxkbj1cInNob3dcIixnbj1cIm91dFwiLHBuPXtISURFOlwiaGlkZS5icy50b29sdGlwXCIsSElEREVOOlwiaGlkZGVuLmJzLnRvb2x0aXBcIixTSE9XOlwic2hvdy5icy50b29sdGlwXCIsU0hPV046XCJzaG93bi5icy50b29sdGlwXCIsSU5TRVJURUQ6XCJpbnNlcnRlZC5icy50b29sdGlwXCIsQ0xJQ0s6XCJjbGljay5icy50b29sdGlwXCIsRk9DVVNJTjpcImZvY3VzaW4uYnMudG9vbHRpcFwiLEZPQ1VTT1VUOlwiZm9jdXNvdXQuYnMudG9vbHRpcFwiLE1PVVNFRU5URVI6XCJtb3VzZWVudGVyLmJzLnRvb2x0aXBcIixNT1VTRUxFQVZFOlwibW91c2VsZWF2ZS5icy50b29sdGlwXCJ9LF9uPVwiZmFkZVwiLG1uPVwic2hvd1wiLHZuPVwiLnRvb2x0aXAtaW5uZXJcIixFbj1cImhvdmVyXCIseW49XCJmb2N1c1wiLGJuPVwiY2xpY2tcIixEbj1cIm1hbnVhbFwiLFRuPWZ1bmN0aW9uKCl7ZnVuY3Rpb24gZShlLG4pe2lmKFwidW5kZWZpbmVkXCI9PXR5cGVvZiB0KXRocm93IG5ldyBUeXBlRXJyb3IoXCJCb290c3RyYXAncyB0b29sdGlwcyByZXF1aXJlIFBvcHBlci5qcyAoaHR0cHM6Ly9wb3BwZXIuanMub3JnKVwiKTt0aGlzLl9pc0VuYWJsZWQ9ITAsdGhpcy5fdGltZW91dD0wLHRoaXMuX2hvdmVyU3RhdGU9XCJcIix0aGlzLl9hY3RpdmVUcmlnZ2VyPXt9LHRoaXMuX3BvcHBlcj1udWxsLHRoaXMuZWxlbWVudD1lLHRoaXMuY29uZmlnPXRoaXMuX2dldENvbmZpZyhuKSx0aGlzLnRpcD1udWxsLHRoaXMuX3NldExpc3RlbmVycygpLEEuc2V0RGF0YShlLHRoaXMuY29uc3RydWN0b3IuREFUQV9LRVksdGhpcyl9dmFyIGk9ZS5wcm90b3R5cGU7cmV0dXJuIGkuZW5hYmxlPWZ1bmN0aW9uKCl7dGhpcy5faXNFbmFibGVkPSEwfSxpLmRpc2FibGU9ZnVuY3Rpb24oKXt0aGlzLl9pc0VuYWJsZWQ9ITF9LGkudG9nZ2xlRW5hYmxlZD1mdW5jdGlvbigpe3RoaXMuX2lzRW5hYmxlZD0hdGhpcy5faXNFbmFibGVkfSxpLnRvZ2dsZT1mdW5jdGlvbih0KXtpZih0aGlzLl9pc0VuYWJsZWQpaWYodCl7dmFyIGU9dGhpcy5jb25zdHJ1Y3Rvci5EQVRBX0tFWSxuPUEuZ2V0RGF0YSh0LmRlbGVnYXRlVGFyZ2V0LGUpO258fChuPW5ldyB0aGlzLmNvbnN0cnVjdG9yKHQuZGVsZWdhdGVUYXJnZXQsdGhpcy5fZ2V0RGVsZWdhdGVDb25maWcoKSksQS5zZXREYXRhKHQuZGVsZWdhdGVUYXJnZXQsZSxuKSksbi5fYWN0aXZlVHJpZ2dlci5jbGljaz0hbi5fYWN0aXZlVHJpZ2dlci5jbGljayxuLl9pc1dpdGhBY3RpdmVUcmlnZ2VyKCk/bi5fZW50ZXIobnVsbCxuKTpuLl9sZWF2ZShudWxsLG4pfWVsc2V7aWYodGhpcy5nZXRUaXBFbGVtZW50KCkuY2xhc3NMaXN0LmNvbnRhaW5zKG1uKSlyZXR1cm4gdm9pZCB0aGlzLl9sZWF2ZShudWxsLHRoaXMpO3RoaXMuX2VudGVyKG51bGwsdGhpcyl9fSxpLmRpc3Bvc2U9ZnVuY3Rpb24oKXtjbGVhclRpbWVvdXQodGhpcy5fdGltZW91dCksQS5yZW1vdmVEYXRhKHRoaXMuZWxlbWVudCx0aGlzLmNvbnN0cnVjdG9yLkRBVEFfS0VZKSxaLm9mZih0aGlzLmVsZW1lbnQsdGhpcy5jb25zdHJ1Y3Rvci5FVkVOVF9LRVkpLFoub2ZmKCQuY2xvc2VzdCh0aGlzLmVsZW1lbnQsXCIubW9kYWxcIiksXCJoaWRlLmJzLm1vZGFsXCIsdGhpcy5faGlkZU1vZGFsSGFuZGxlciksdGhpcy50aXAmJnRoaXMudGlwLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy50aXApLHRoaXMuX2lzRW5hYmxlZD1udWxsLHRoaXMuX3RpbWVvdXQ9bnVsbCx0aGlzLl9ob3ZlclN0YXRlPW51bGwsdGhpcy5fYWN0aXZlVHJpZ2dlcj1udWxsLHRoaXMuX3BvcHBlciYmdGhpcy5fcG9wcGVyLmRlc3Ryb3koKSx0aGlzLl9wb3BwZXI9bnVsbCx0aGlzLmVsZW1lbnQ9bnVsbCx0aGlzLmNvbmZpZz1udWxsLHRoaXMudGlwPW51bGx9LGkuc2hvdz1mdW5jdGlvbigpe3ZhciBlPXRoaXM7aWYoXCJub25lXCI9PT10aGlzLmVsZW1lbnQuc3R5bGUuZGlzcGxheSl0aHJvdyBuZXcgRXJyb3IoXCJQbGVhc2UgdXNlIHNob3cgb24gdmlzaWJsZSBlbGVtZW50c1wiKTtpZih0aGlzLmlzV2l0aENvbnRlbnQoKSYmdGhpcy5faXNFbmFibGVkKXt2YXIgbj1aLnRyaWdnZXIodGhpcy5lbGVtZW50LHRoaXMuY29uc3RydWN0b3IuRXZlbnQuU0hPVyksaT1mdW5jdGlvbiB0KGUpe2lmKCFkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuYXR0YWNoU2hhZG93KXJldHVybiBudWxsO2lmKFwiZnVuY3Rpb25cIj09dHlwZW9mIGUuZ2V0Um9vdE5vZGUpe3ZhciBuPWUuZ2V0Um9vdE5vZGUoKTtyZXR1cm4gbiBpbnN0YW5jZW9mIFNoYWRvd1Jvb3Q/bjpudWxsfXJldHVybiBlIGluc3RhbmNlb2YgU2hhZG93Um9vdD9lOmUucGFyZW50Tm9kZT90KGUucGFyZW50Tm9kZSk6bnVsbH0odGhpcy5lbGVtZW50KSxvPW51bGw9PT1pP3RoaXMuZWxlbWVudC5vd25lckRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jb250YWlucyh0aGlzLmVsZW1lbnQpOmkuY29udGFpbnModGhpcy5lbGVtZW50KTtpZihuLmRlZmF1bHRQcmV2ZW50ZWR8fCFvKXJldHVybjt2YXIgcj10aGlzLmdldFRpcEVsZW1lbnQoKSxzPXUodGhpcy5jb25zdHJ1Y3Rvci5OQU1FKTtyLnNldEF0dHJpYnV0ZShcImlkXCIscyksdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZShcImFyaWEtZGVzY3JpYmVkYnlcIixzKSx0aGlzLnNldENvbnRlbnQoKSx0aGlzLmNvbmZpZy5hbmltYXRpb24mJnIuY2xhc3NMaXN0LmFkZChfbik7dmFyIGE9XCJmdW5jdGlvblwiPT10eXBlb2YgdGhpcy5jb25maWcucGxhY2VtZW50P3RoaXMuY29uZmlnLnBsYWNlbWVudC5jYWxsKHRoaXMscix0aGlzLmVsZW1lbnQpOnRoaXMuY29uZmlnLnBsYWNlbWVudCxsPXRoaXMuX2dldEF0dGFjaG1lbnQoYSk7dGhpcy5fYWRkQXR0YWNobWVudENsYXNzKGwpO3ZhciBmPXRoaXMuX2dldENvbnRhaW5lcigpO0Euc2V0RGF0YShyLHRoaXMuY29uc3RydWN0b3IuREFUQV9LRVksdGhpcyksdGhpcy5lbGVtZW50Lm93bmVyRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNvbnRhaW5zKHRoaXMudGlwKXx8Zi5hcHBlbmRDaGlsZChyKSxaLnRyaWdnZXIodGhpcy5lbGVtZW50LHRoaXMuY29uc3RydWN0b3IuRXZlbnQuSU5TRVJURUQpLHRoaXMuX3BvcHBlcj1uZXcgdCh0aGlzLmVsZW1lbnQscix0aGlzLl9nZXRQb3BwZXJDb25maWcobCkpLHIuY2xhc3NMaXN0LmFkZChtbiksXCJvbnRvdWNoc3RhcnRcImluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCYmRShkb2N1bWVudC5ib2R5LmNoaWxkcmVuKS5mb3JFYWNoKChmdW5jdGlvbih0KXtaLm9uKHQsXCJtb3VzZW92ZXJcIiwoZnVuY3Rpb24oKXt9KSl9KSk7dmFyIGg9ZnVuY3Rpb24oKXtlLmNvbmZpZy5hbmltYXRpb24mJmUuX2ZpeFRyYW5zaXRpb24oKTt2YXIgdD1lLl9ob3ZlclN0YXRlO2UuX2hvdmVyU3RhdGU9bnVsbCxaLnRyaWdnZXIoZS5lbGVtZW50LGUuY29uc3RydWN0b3IuRXZlbnQuU0hPV04pLHQ9PT1nbiYmZS5fbGVhdmUobnVsbCxlKX07aWYodGhpcy50aXAuY2xhc3NMaXN0LmNvbnRhaW5zKF9uKSl7dmFyIGQ9Zyh0aGlzLnRpcCk7Wi5vbmUodGhpcy50aXAsYyxoKSxtKHRoaXMudGlwLGQpfWVsc2UgaCgpfX0saS5oaWRlPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcyxlPXRoaXMuZ2V0VGlwRWxlbWVudCgpLG49ZnVuY3Rpb24oKXt0Ll9ob3ZlclN0YXRlIT09ZG4mJmUucGFyZW50Tm9kZSYmZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGUpLHQuX2NsZWFuVGlwQ2xhc3MoKSx0LmVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKFwiYXJpYS1kZXNjcmliZWRieVwiKSxaLnRyaWdnZXIodC5lbGVtZW50LHQuY29uc3RydWN0b3IuRXZlbnQuSElEREVOKSx0Ll9wb3BwZXIuZGVzdHJveSgpfTtpZighWi50cmlnZ2VyKHRoaXMuZWxlbWVudCx0aGlzLmNvbnN0cnVjdG9yLkV2ZW50LkhJREUpLmRlZmF1bHRQcmV2ZW50ZWQpe2lmKGUuY2xhc3NMaXN0LnJlbW92ZShtbiksXCJvbnRvdWNoc3RhcnRcImluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCYmRShkb2N1bWVudC5ib2R5LmNoaWxkcmVuKS5mb3JFYWNoKChmdW5jdGlvbih0KXtyZXR1cm4gWi5vZmYodCxcIm1vdXNlb3ZlclwiLGIpfSkpLHRoaXMuX2FjdGl2ZVRyaWdnZXJbYm5dPSExLHRoaXMuX2FjdGl2ZVRyaWdnZXJbeW5dPSExLHRoaXMuX2FjdGl2ZVRyaWdnZXJbRW5dPSExLHRoaXMudGlwLmNsYXNzTGlzdC5jb250YWlucyhfbikpe3ZhciBpPWcoZSk7Wi5vbmUoZSxjLG4pLG0oZSxpKX1lbHNlIG4oKTt0aGlzLl9ob3ZlclN0YXRlPVwiXCJ9fSxpLnVwZGF0ZT1mdW5jdGlvbigpe251bGwhPT10aGlzLl9wb3BwZXImJnRoaXMuX3BvcHBlci5zY2hlZHVsZVVwZGF0ZSgpfSxpLmlzV2l0aENvbnRlbnQ9ZnVuY3Rpb24oKXtyZXR1cm4gQm9vbGVhbih0aGlzLmdldFRpdGxlKCkpfSxpLmdldFRpcEVsZW1lbnQ9ZnVuY3Rpb24oKXtpZih0aGlzLnRpcClyZXR1cm4gdGhpcy50aXA7dmFyIHQ9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtyZXR1cm4gdC5pbm5lckhUTUw9dGhpcy5jb25maWcudGVtcGxhdGUsdGhpcy50aXA9dC5jaGlsZHJlblswXSx0aGlzLnRpcH0saS5zZXRDb250ZW50PWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5nZXRUaXBFbGVtZW50KCk7dGhpcy5zZXRFbGVtZW50Q29udGVudCgkLmZpbmRPbmUodm4sdCksdGhpcy5nZXRUaXRsZSgpKSx0LmNsYXNzTGlzdC5yZW1vdmUoX24pLHQuY2xhc3NMaXN0LnJlbW92ZShtbil9LGkuc2V0RWxlbWVudENvbnRlbnQ9ZnVuY3Rpb24odCxlKXtpZihudWxsIT09dClyZXR1cm5cIm9iamVjdFwiPT10eXBlb2YgZSYmXyhlKT8oZS5qcXVlcnkmJihlPWVbMF0pLHZvaWQodGhpcy5jb25maWcuaHRtbD9lLnBhcmVudE5vZGUhPT10JiYodC5pbm5lckhUTUw9XCJcIix0LmFwcGVuZENoaWxkKGUpKTp0LmlubmVyVGV4dD1lLnRleHRDb250ZW50KSk6dm9pZCh0aGlzLmNvbmZpZy5odG1sPyh0aGlzLmNvbmZpZy5zYW5pdGl6ZSYmKGU9c24oZSx0aGlzLmNvbmZpZy53aGl0ZUxpc3QsdGhpcy5jb25maWcuc2FuaXRpemVGbikpLHQuaW5uZXJIVE1MPWUpOnQuaW5uZXJUZXh0PWUpfSxpLmdldFRpdGxlPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcImRhdGEtb3JpZ2luYWwtdGl0bGVcIik7cmV0dXJuIHR8fCh0PVwiZnVuY3Rpb25cIj09dHlwZW9mIHRoaXMuY29uZmlnLnRpdGxlP3RoaXMuY29uZmlnLnRpdGxlLmNhbGwodGhpcy5lbGVtZW50KTp0aGlzLmNvbmZpZy50aXRsZSksdH0saS5fZ2V0UG9wcGVyQ29uZmlnPWZ1bmN0aW9uKHQpe3ZhciBlPXRoaXM7cmV0dXJuIHIoe30se3BsYWNlbWVudDp0LG1vZGlmaWVyczp7b2Zmc2V0OnRoaXMuX2dldE9mZnNldCgpLGZsaXA6e2JlaGF2aW9yOnRoaXMuY29uZmlnLmZhbGxiYWNrUGxhY2VtZW50fSxhcnJvdzp7ZWxlbWVudDpcIi5cIit0aGlzLmNvbnN0cnVjdG9yLk5BTUUrXCItYXJyb3dcIn0scHJldmVudE92ZXJmbG93Ontib3VuZGFyaWVzRWxlbWVudDp0aGlzLmNvbmZpZy5ib3VuZGFyeX19LG9uQ3JlYXRlOmZ1bmN0aW9uKHQpe3Qub3JpZ2luYWxQbGFjZW1lbnQhPT10LnBsYWNlbWVudCYmZS5faGFuZGxlUG9wcGVyUGxhY2VtZW50Q2hhbmdlKHQpfSxvblVwZGF0ZTpmdW5jdGlvbih0KXtyZXR1cm4gZS5faGFuZGxlUG9wcGVyUGxhY2VtZW50Q2hhbmdlKHQpfX0se30sdGhpcy5jb25maWcucG9wcGVyQ29uZmlnKX0saS5fYWRkQXR0YWNobWVudENsYXNzPWZ1bmN0aW9uKHQpe3RoaXMuZ2V0VGlwRWxlbWVudCgpLmNsYXNzTGlzdC5hZGQoXCJicy10b29sdGlwLVwiK3QpfSxpLl9nZXRPZmZzZXQ9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLGU9e307cmV0dXJuXCJmdW5jdGlvblwiPT10eXBlb2YgdGhpcy5jb25maWcub2Zmc2V0P2UuZm49ZnVuY3Rpb24oZSl7cmV0dXJuIGUub2Zmc2V0cz1yKHt9LGUub2Zmc2V0cyx7fSx0LmNvbmZpZy5vZmZzZXQoZS5vZmZzZXRzLHQuZWxlbWVudCl8fHt9KSxlfTplLm9mZnNldD10aGlzLmNvbmZpZy5vZmZzZXQsZX0saS5fZ2V0Q29udGFpbmVyPWZ1bmN0aW9uKCl7cmV0dXJuITE9PT10aGlzLmNvbmZpZy5jb250YWluZXI/ZG9jdW1lbnQuYm9keTpfKHRoaXMuY29uZmlnLmNvbnRhaW5lcik/dGhpcy5jb25maWcuY29udGFpbmVyOiQuZmluZE9uZSh0aGlzLmNvbmZpZy5jb250YWluZXIpfSxpLl9nZXRBdHRhY2htZW50PWZ1bmN0aW9uKHQpe3JldHVybiBmblt0LnRvVXBwZXJDYXNlKCldfSxpLl9zZXRMaXN0ZW5lcnM9ZnVuY3Rpb24oKXt2YXIgdD10aGlzO3RoaXMuY29uZmlnLnRyaWdnZXIuc3BsaXQoXCIgXCIpLmZvckVhY2goKGZ1bmN0aW9uKGUpe2lmKFwiY2xpY2tcIj09PWUpWi5vbih0LmVsZW1lbnQsdC5jb25zdHJ1Y3Rvci5FdmVudC5DTElDSyx0LmNvbmZpZy5zZWxlY3RvciwoZnVuY3Rpb24oZSl7cmV0dXJuIHQudG9nZ2xlKGUpfSkpO2Vsc2UgaWYoZSE9PURuKXt2YXIgbj1lPT09RW4/dC5jb25zdHJ1Y3Rvci5FdmVudC5NT1VTRUVOVEVSOnQuY29uc3RydWN0b3IuRXZlbnQuRk9DVVNJTixpPWU9PT1Fbj90LmNvbnN0cnVjdG9yLkV2ZW50Lk1PVVNFTEVBVkU6dC5jb25zdHJ1Y3Rvci5FdmVudC5GT0NVU09VVDtaLm9uKHQuZWxlbWVudCxuLHQuY29uZmlnLnNlbGVjdG9yLChmdW5jdGlvbihlKXtyZXR1cm4gdC5fZW50ZXIoZSl9KSksWi5vbih0LmVsZW1lbnQsaSx0LmNvbmZpZy5zZWxlY3RvciwoZnVuY3Rpb24oZSl7cmV0dXJuIHQuX2xlYXZlKGUpfSkpfX0pKSx0aGlzLl9oaWRlTW9kYWxIYW5kbGVyPWZ1bmN0aW9uKCl7dC5lbGVtZW50JiZ0LmhpZGUoKX0sWi5vbigkLmNsb3Nlc3QodGhpcy5lbGVtZW50LFwiLm1vZGFsXCIpLFwiaGlkZS5icy5tb2RhbFwiLHRoaXMuX2hpZGVNb2RhbEhhbmRsZXIpLHRoaXMuY29uZmlnLnNlbGVjdG9yP3RoaXMuY29uZmlnPXIoe30sdGhpcy5jb25maWcse3RyaWdnZXI6XCJtYW51YWxcIixzZWxlY3RvcjpcIlwifSk6dGhpcy5fZml4VGl0bGUoKX0saS5fZml4VGl0bGU9ZnVuY3Rpb24oKXt2YXIgdD10eXBlb2YgdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcImRhdGEtb3JpZ2luYWwtdGl0bGVcIik7KHRoaXMuZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJ0aXRsZVwiKXx8XCJzdHJpbmdcIiE9PXQpJiYodGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZShcImRhdGEtb3JpZ2luYWwtdGl0bGVcIix0aGlzLmVsZW1lbnQuZ2V0QXR0cmlidXRlKFwidGl0bGVcIil8fFwiXCIpLHRoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGUoXCJ0aXRsZVwiLFwiXCIpKX0saS5fZW50ZXI9ZnVuY3Rpb24odCxlKXt2YXIgbj10aGlzLmNvbnN0cnVjdG9yLkRBVEFfS0VZOyhlPWV8fEEuZ2V0RGF0YSh0LmRlbGVnYXRlVGFyZ2V0LG4pKXx8KGU9bmV3IHRoaXMuY29uc3RydWN0b3IodC5kZWxlZ2F0ZVRhcmdldCx0aGlzLl9nZXREZWxlZ2F0ZUNvbmZpZygpKSxBLnNldERhdGEodC5kZWxlZ2F0ZVRhcmdldCxuLGUpKSx0JiYoZS5fYWN0aXZlVHJpZ2dlcltcImZvY3VzaW5cIj09PXQudHlwZT95bjpFbl09ITApLGUuZ2V0VGlwRWxlbWVudCgpLmNsYXNzTGlzdC5jb250YWlucyhtbil8fGUuX2hvdmVyU3RhdGU9PT1kbj9lLl9ob3ZlclN0YXRlPWRuOihjbGVhclRpbWVvdXQoZS5fdGltZW91dCksZS5faG92ZXJTdGF0ZT1kbixlLmNvbmZpZy5kZWxheSYmZS5jb25maWcuZGVsYXkuc2hvdz9lLl90aW1lb3V0PXNldFRpbWVvdXQoKGZ1bmN0aW9uKCl7ZS5faG92ZXJTdGF0ZT09PWRuJiZlLnNob3coKX0pLGUuY29uZmlnLmRlbGF5LnNob3cpOmUuc2hvdygpKX0saS5fbGVhdmU9ZnVuY3Rpb24odCxlKXt2YXIgbj10aGlzLmNvbnN0cnVjdG9yLkRBVEFfS0VZOyhlPWV8fEEuZ2V0RGF0YSh0LmRlbGVnYXRlVGFyZ2V0LG4pKXx8KGU9bmV3IHRoaXMuY29uc3RydWN0b3IodC5kZWxlZ2F0ZVRhcmdldCx0aGlzLl9nZXREZWxlZ2F0ZUNvbmZpZygpKSxBLnNldERhdGEodC5kZWxlZ2F0ZVRhcmdldCxuLGUpKSx0JiYoZS5fYWN0aXZlVHJpZ2dlcltcImZvY3Vzb3V0XCI9PT10LnR5cGU/eW46RW5dPSExKSxlLl9pc1dpdGhBY3RpdmVUcmlnZ2VyKCl8fChjbGVhclRpbWVvdXQoZS5fdGltZW91dCksZS5faG92ZXJTdGF0ZT1nbixlLmNvbmZpZy5kZWxheSYmZS5jb25maWcuZGVsYXkuaGlkZT9lLl90aW1lb3V0PXNldFRpbWVvdXQoKGZ1bmN0aW9uKCl7ZS5faG92ZXJTdGF0ZT09PWduJiZlLmhpZGUoKX0pLGUuY29uZmlnLmRlbGF5LmhpZGUpOmUuaGlkZSgpKX0saS5faXNXaXRoQWN0aXZlVHJpZ2dlcj1mdW5jdGlvbigpe2Zvcih2YXIgdCBpbiB0aGlzLl9hY3RpdmVUcmlnZ2VyKWlmKHRoaXMuX2FjdGl2ZVRyaWdnZXJbdF0pcmV0dXJuITA7cmV0dXJuITF9LGkuX2dldENvbmZpZz1mdW5jdGlvbih0KXt2YXIgZT1JdC5nZXREYXRhQXR0cmlidXRlcyh0aGlzLmVsZW1lbnQpO3JldHVybiBPYmplY3Qua2V5cyhlKS5mb3JFYWNoKChmdW5jdGlvbih0KXstMSE9PWNuLmluZGV4T2YodCkmJmRlbGV0ZSBlW3RdfSkpLHQmJlwib2JqZWN0XCI9PXR5cGVvZiB0LmNvbnRhaW5lciYmdC5jb250YWluZXIuanF1ZXJ5JiYodC5jb250YWluZXI9dC5jb250YWluZXJbMF0pLFwibnVtYmVyXCI9PXR5cGVvZih0PXIoe30sdGhpcy5jb25zdHJ1Y3Rvci5EZWZhdWx0LHt9LGUse30sXCJvYmplY3RcIj09dHlwZW9mIHQmJnQ/dDp7fSkpLmRlbGF5JiYodC5kZWxheT17c2hvdzp0LmRlbGF5LGhpZGU6dC5kZWxheX0pLFwibnVtYmVyXCI9PXR5cGVvZiB0LnRpdGxlJiYodC50aXRsZT10LnRpdGxlLnRvU3RyaW5nKCkpLFwibnVtYmVyXCI9PXR5cGVvZiB0LmNvbnRlbnQmJih0LmNvbnRlbnQ9dC5jb250ZW50LnRvU3RyaW5nKCkpLHYoYW4sdCx0aGlzLmNvbnN0cnVjdG9yLkRlZmF1bHRUeXBlKSx0LnNhbml0aXplJiYodC50ZW1wbGF0ZT1zbih0LnRlbXBsYXRlLHQud2hpdGVMaXN0LHQuc2FuaXRpemVGbikpLHR9LGkuX2dldERlbGVnYXRlQ29uZmlnPWZ1bmN0aW9uKCl7dmFyIHQ9e307aWYodGhpcy5jb25maWcpZm9yKHZhciBlIGluIHRoaXMuY29uZmlnKXRoaXMuY29uc3RydWN0b3IuRGVmYXVsdFtlXSE9PXRoaXMuY29uZmlnW2VdJiYodFtlXT10aGlzLmNvbmZpZ1tlXSk7cmV0dXJuIHR9LGkuX2NsZWFuVGlwQ2xhc3M9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLmdldFRpcEVsZW1lbnQoKSxlPXQuZ2V0QXR0cmlidXRlKFwiY2xhc3NcIikubWF0Y2gobG4pO251bGwhPT1lJiZlLmxlbmd0aCYmZS5tYXAoKGZ1bmN0aW9uKHQpe3JldHVybiB0LnRyaW0oKX0pKS5mb3JFYWNoKChmdW5jdGlvbihlKXtyZXR1cm4gdC5jbGFzc0xpc3QucmVtb3ZlKGUpfSkpfSxpLl9oYW5kbGVQb3BwZXJQbGFjZW1lbnRDaGFuZ2U9ZnVuY3Rpb24odCl7dmFyIGU9dC5pbnN0YW5jZTt0aGlzLnRpcD1lLnBvcHBlcix0aGlzLl9jbGVhblRpcENsYXNzKCksdGhpcy5fYWRkQXR0YWNobWVudENsYXNzKHRoaXMuX2dldEF0dGFjaG1lbnQodC5wbGFjZW1lbnQpKX0saS5fZml4VHJhbnNpdGlvbj1mdW5jdGlvbigpe3ZhciB0PXRoaXMuZ2V0VGlwRWxlbWVudCgpLGU9dGhpcy5jb25maWcuYW5pbWF0aW9uO251bGw9PT10LmdldEF0dHJpYnV0ZShcIngtcGxhY2VtZW50XCIpJiYodC5jbGFzc0xpc3QucmVtb3ZlKF9uKSx0aGlzLmNvbmZpZy5hbmltYXRpb249ITEsdGhpcy5oaWRlKCksdGhpcy5zaG93KCksdGhpcy5jb25maWcuYW5pbWF0aW9uPWUpfSxlLmpRdWVyeUludGVyZmFjZT1mdW5jdGlvbih0KXtyZXR1cm4gdGhpcy5lYWNoKChmdW5jdGlvbigpe3ZhciBuPUEuZ2V0RGF0YSh0aGlzLFwiYnMudG9vbHRpcFwiKSxpPVwib2JqZWN0XCI9PXR5cGVvZiB0JiZ0O2lmKChufHwhL2Rpc3Bvc2V8aGlkZS8udGVzdCh0KSkmJihufHwobj1uZXcgZSh0aGlzLGkpKSxcInN0cmluZ1wiPT10eXBlb2YgdCkpe2lmKFwidW5kZWZpbmVkXCI9PXR5cGVvZiBuW3RdKXRocm93IG5ldyBUeXBlRXJyb3IoJ05vIG1ldGhvZCBuYW1lZCBcIicrdCsnXCInKTtuW3RdKCl9fSkpfSxlLmdldEluc3RhbmNlPWZ1bmN0aW9uKHQpe3JldHVybiBBLmdldERhdGEodCxcImJzLnRvb2x0aXBcIil9LG4oZSxudWxsLFt7a2V5OlwiVkVSU0lPTlwiLGdldDpmdW5jdGlvbigpe3JldHVyblwiNC4zLjFcIn19LHtrZXk6XCJEZWZhdWx0XCIsZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuIGhufX0se2tleTpcIk5BTUVcIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gYW59fSx7a2V5OlwiREFUQV9LRVlcIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm5cImJzLnRvb2x0aXBcIn19LHtrZXk6XCJFdmVudFwiLGdldDpmdW5jdGlvbigpe3JldHVybiBwbn19LHtrZXk6XCJFVkVOVF9LRVlcIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm5cIi5icy50b29sdGlwXCJ9fSx7a2V5OlwiRGVmYXVsdFR5cGVcIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gdW59fV0pLGV9KCksSW49VCgpO2lmKEluKXt2YXIgQW49SW4uZm4udG9vbHRpcDtJbi5mbi50b29sdGlwPVRuLmpRdWVyeUludGVyZmFjZSxJbi5mbi50b29sdGlwLkNvbnN0cnVjdG9yPVRuLEluLmZuLnRvb2x0aXAubm9Db25mbGljdD1mdW5jdGlvbigpe3JldHVybiBJbi5mbi50b29sdGlwPUFuLFRuLmpRdWVyeUludGVyZmFjZX19dmFyIFNuPVwicG9wb3ZlclwiLHduPW5ldyBSZWdFeHAoXCIoXnxcXFxccylicy1wb3BvdmVyXFxcXFMrXCIsXCJnXCIpLENuPXIoe30sVG4uRGVmYXVsdCx7cGxhY2VtZW50OlwicmlnaHRcIix0cmlnZ2VyOlwiY2xpY2tcIixjb250ZW50OlwiXCIsdGVtcGxhdGU6JzxkaXYgY2xhc3M9XCJwb3BvdmVyXCIgcm9sZT1cInRvb2x0aXBcIj48ZGl2IGNsYXNzPVwicG9wb3Zlci1hcnJvd1wiPjwvZGl2PjxoMyBjbGFzcz1cInBvcG92ZXItaGVhZGVyXCI+PC9oMz48ZGl2IGNsYXNzPVwicG9wb3Zlci1ib2R5XCI+PC9kaXY+PC9kaXY+J30pLExuPXIoe30sVG4uRGVmYXVsdFR5cGUse2NvbnRlbnQ6XCIoc3RyaW5nfGVsZW1lbnR8ZnVuY3Rpb24pXCJ9KSxPbj1cImZhZGVcIixObj1cInNob3dcIixrbj1cIi5wb3BvdmVyLWhlYWRlclwiLFBuPVwiLnBvcG92ZXItYm9keVwiLEhuPXtISURFOlwiaGlkZS5icy5wb3BvdmVyXCIsSElEREVOOlwiaGlkZGVuLmJzLnBvcG92ZXJcIixTSE9XOlwic2hvdy5icy5wb3BvdmVyXCIsU0hPV046XCJzaG93bi5icy5wb3BvdmVyXCIsSU5TRVJURUQ6XCJpbnNlcnRlZC5icy5wb3BvdmVyXCIsQ0xJQ0s6XCJjbGljay5icy5wb3BvdmVyXCIsRk9DVVNJTjpcImZvY3VzaW4uYnMucG9wb3ZlclwiLEZPQ1VTT1VUOlwiZm9jdXNvdXQuYnMucG9wb3ZlclwiLE1PVVNFRU5URVI6XCJtb3VzZWVudGVyLmJzLnBvcG92ZXJcIixNT1VTRUxFQVZFOlwibW91c2VsZWF2ZS5icy5wb3BvdmVyXCJ9LGpuPWZ1bmN0aW9uKHQpe3ZhciBlLGk7ZnVuY3Rpb24gbygpe3JldHVybiB0LmFwcGx5KHRoaXMsYXJndW1lbnRzKXx8dGhpc31pPXQsKGU9bykucHJvdG90eXBlPU9iamVjdC5jcmVhdGUoaS5wcm90b3R5cGUpLGUucHJvdG90eXBlLmNvbnN0cnVjdG9yPWUsZS5fX3Byb3RvX189aTt2YXIgcj1vLnByb3RvdHlwZTtyZXR1cm4gci5pc1dpdGhDb250ZW50PWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuZ2V0VGl0bGUoKXx8dGhpcy5fZ2V0Q29udGVudCgpfSxyLnNldENvbnRlbnQ9ZnVuY3Rpb24oKXt2YXIgdD10aGlzLmdldFRpcEVsZW1lbnQoKTt0aGlzLnNldEVsZW1lbnRDb250ZW50KCQuZmluZE9uZShrbix0KSx0aGlzLmdldFRpdGxlKCkpO3ZhciBlPXRoaXMuX2dldENvbnRlbnQoKTtcImZ1bmN0aW9uXCI9PXR5cGVvZiBlJiYoZT1lLmNhbGwodGhpcy5lbGVtZW50KSksdGhpcy5zZXRFbGVtZW50Q29udGVudCgkLmZpbmRPbmUoUG4sdCksZSksdC5jbGFzc0xpc3QucmVtb3ZlKE9uKSx0LmNsYXNzTGlzdC5yZW1vdmUoTm4pfSxyLl9hZGRBdHRhY2htZW50Q2xhc3M9ZnVuY3Rpb24odCl7dGhpcy5nZXRUaXBFbGVtZW50KCkuY2xhc3NMaXN0LmFkZChcImJzLXBvcG92ZXItXCIrdCl9LHIuX2dldENvbnRlbnQ9ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcImRhdGEtY29udGVudFwiKXx8dGhpcy5jb25maWcuY29udGVudH0sci5fY2xlYW5UaXBDbGFzcz1mdW5jdGlvbigpe3ZhciB0PXRoaXMuZ2V0VGlwRWxlbWVudCgpLGU9dC5nZXRBdHRyaWJ1dGUoXCJjbGFzc1wiKS5tYXRjaCh3bik7bnVsbCE9PWUmJmUubGVuZ3RoPjAmJmUubWFwKChmdW5jdGlvbih0KXtyZXR1cm4gdC50cmltKCl9KSkuZm9yRWFjaCgoZnVuY3Rpb24oZSl7cmV0dXJuIHQuY2xhc3NMaXN0LnJlbW92ZShlKX0pKX0sby5qUXVlcnlJbnRlcmZhY2U9ZnVuY3Rpb24odCl7cmV0dXJuIHRoaXMuZWFjaCgoZnVuY3Rpb24oKXt2YXIgZT1BLmdldERhdGEodGhpcyxcImJzLnBvcG92ZXJcIiksbj1cIm9iamVjdFwiPT10eXBlb2YgdD90Om51bGw7aWYoKGV8fCEvZGlzcG9zZXxoaWRlLy50ZXN0KHQpKSYmKGV8fChlPW5ldyBvKHRoaXMsbiksQS5zZXREYXRhKHRoaXMsXCJicy5wb3BvdmVyXCIsZSkpLFwic3RyaW5nXCI9PXR5cGVvZiB0KSl7aWYoXCJ1bmRlZmluZWRcIj09dHlwZW9mIGVbdF0pdGhyb3cgbmV3IFR5cGVFcnJvcignTm8gbWV0aG9kIG5hbWVkIFwiJyt0KydcIicpO2VbdF0oKX19KSl9LG8uZ2V0SW5zdGFuY2U9ZnVuY3Rpb24odCl7cmV0dXJuIEEuZ2V0RGF0YSh0LFwiYnMucG9wb3ZlclwiKX0sbihvLG51bGwsW3trZXk6XCJWRVJTSU9OXCIsZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuXCI0LjMuMVwifX0se2tleTpcIkRlZmF1bHRcIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gQ259fSx7a2V5OlwiTkFNRVwiLGdldDpmdW5jdGlvbigpe3JldHVybiBTbn19LHtrZXk6XCJEQVRBX0tFWVwiLGdldDpmdW5jdGlvbigpe3JldHVyblwiYnMucG9wb3ZlclwifX0se2tleTpcIkV2ZW50XCIsZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuIEhufX0se2tleTpcIkVWRU5UX0tFWVwiLGdldDpmdW5jdGlvbigpe3JldHVyblwiLmJzLnBvcG92ZXJcIn19LHtrZXk6XCJEZWZhdWx0VHlwZVwiLGdldDpmdW5jdGlvbigpe3JldHVybiBMbn19XSksb30oVG4pLE1uPVQoKTtpZihNbil7dmFyIFJuPU1uLmZuLnBvcG92ZXI7TW4uZm4ucG9wb3Zlcj1qbi5qUXVlcnlJbnRlcmZhY2UsTW4uZm4ucG9wb3Zlci5Db25zdHJ1Y3Rvcj1qbixNbi5mbi5wb3BvdmVyLm5vQ29uZmxpY3Q9ZnVuY3Rpb24oKXtyZXR1cm4gTW4uZm4ucG9wb3Zlcj1Sbixqbi5qUXVlcnlJbnRlcmZhY2V9fXZhciB4bj1cInNjcm9sbHNweVwiLFduPVwiYnMuc2Nyb2xsc3B5XCIsVW49XCIuXCIrV24sS249e29mZnNldDoxMCxtZXRob2Q6XCJhdXRvXCIsdGFyZ2V0OlwiXCJ9LFZuPXtvZmZzZXQ6XCJudW1iZXJcIixtZXRob2Q6XCJzdHJpbmdcIix0YXJnZXQ6XCIoc3RyaW5nfGVsZW1lbnQpXCJ9LEJuPXtBQ1RJVkFURTpcImFjdGl2YXRlXCIrVW4sU0NST0xMOlwic2Nyb2xsXCIrVW4sTE9BRF9EQVRBX0FQSTpcImxvYWRcIitVbitcIi5kYXRhLWFwaVwifSxGbj17RFJPUERPV05fSVRFTTpcImRyb3Bkb3duLWl0ZW1cIixBQ1RJVkU6XCJhY3RpdmVcIn0sUW49e0RBVEFfU1BZOidbZGF0YS1zcHk9XCJzY3JvbGxcIl0nLE5BVl9MSVNUX0dST1VQOlwiLm5hdiwgLmxpc3QtZ3JvdXBcIixOQVZfTElOS1M6XCIubmF2LWxpbmtcIixOQVZfSVRFTVM6XCIubmF2LWl0ZW1cIixMSVNUX0lURU1TOlwiLmxpc3QtZ3JvdXAtaXRlbVwiLERST1BET1dOOlwiLmRyb3Bkb3duXCIsRFJPUERPV05fVE9HR0xFOlwiLmRyb3Bkb3duLXRvZ2dsZVwifSxZbj1cIm9mZnNldFwiLEduPVwicG9zaXRpb25cIixYbj1mdW5jdGlvbigpe2Z1bmN0aW9uIHQodCxlKXt2YXIgbj10aGlzO3RoaXMuX2VsZW1lbnQ9dCx0aGlzLl9zY3JvbGxFbGVtZW50PVwiQk9EWVwiPT09dC50YWdOYW1lP3dpbmRvdzp0LHRoaXMuX2NvbmZpZz10aGlzLl9nZXRDb25maWcoZSksdGhpcy5fc2VsZWN0b3I9dGhpcy5fY29uZmlnLnRhcmdldCtcIiBcIitRbi5OQVZfTElOS1MrXCIsXCIrdGhpcy5fY29uZmlnLnRhcmdldCtcIiBcIitRbi5MSVNUX0lURU1TK1wiLFwiK3RoaXMuX2NvbmZpZy50YXJnZXQrXCIgLlwiK0ZuLkRST1BET1dOX0lURU0sdGhpcy5fb2Zmc2V0cz1bXSx0aGlzLl90YXJnZXRzPVtdLHRoaXMuX2FjdGl2ZVRhcmdldD1udWxsLHRoaXMuX3Njcm9sbEhlaWdodD0wLFoub24odGhpcy5fc2Nyb2xsRWxlbWVudCxCbi5TQ1JPTEwsKGZ1bmN0aW9uKHQpe3JldHVybiBuLl9wcm9jZXNzKHQpfSkpLHRoaXMucmVmcmVzaCgpLHRoaXMuX3Byb2Nlc3MoKSxBLnNldERhdGEodCxXbix0aGlzKX12YXIgZT10LnByb3RvdHlwZTtyZXR1cm4gZS5yZWZyZXNoPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcyxlPXRoaXMuX3Njcm9sbEVsZW1lbnQ9PT10aGlzLl9zY3JvbGxFbGVtZW50LndpbmRvdz9ZbjpHbixuPVwiYXV0b1wiPT09dGhpcy5fY29uZmlnLm1ldGhvZD9lOnRoaXMuX2NvbmZpZy5tZXRob2QsaT1uPT09R24/dGhpcy5fZ2V0U2Nyb2xsVG9wKCk6MDt0aGlzLl9vZmZzZXRzPVtdLHRoaXMuX3RhcmdldHM9W10sdGhpcy5fc2Nyb2xsSGVpZ2h0PXRoaXMuX2dldFNjcm9sbEhlaWdodCgpLEUoJC5maW5kKHRoaXMuX3NlbGVjdG9yKSkubWFwKChmdW5jdGlvbih0KXt2YXIgZSxvPWgodCk7aWYobyYmKGU9JC5maW5kT25lKG8pKSxlKXt2YXIgcj1lLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO2lmKHIud2lkdGh8fHIuaGVpZ2h0KXJldHVybltJdFtuXShlKS50b3AraSxvXX1yZXR1cm4gbnVsbH0pKS5maWx0ZXIoKGZ1bmN0aW9uKHQpe3JldHVybiB0fSkpLnNvcnQoKGZ1bmN0aW9uKHQsZSl7cmV0dXJuIHRbMF0tZVswXX0pKS5mb3JFYWNoKChmdW5jdGlvbihlKXt0Ll9vZmZzZXRzLnB1c2goZVswXSksdC5fdGFyZ2V0cy5wdXNoKGVbMV0pfSkpfSxlLmRpc3Bvc2U9ZnVuY3Rpb24oKXtBLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCxXbiksWi5vZmYodGhpcy5fc2Nyb2xsRWxlbWVudCxVbiksdGhpcy5fZWxlbWVudD1udWxsLHRoaXMuX3Njcm9sbEVsZW1lbnQ9bnVsbCx0aGlzLl9jb25maWc9bnVsbCx0aGlzLl9zZWxlY3Rvcj1udWxsLHRoaXMuX29mZnNldHM9bnVsbCx0aGlzLl90YXJnZXRzPW51bGwsdGhpcy5fYWN0aXZlVGFyZ2V0PW51bGwsdGhpcy5fc2Nyb2xsSGVpZ2h0PW51bGx9LGUuX2dldENvbmZpZz1mdW5jdGlvbih0KXtpZihcInN0cmluZ1wiIT10eXBlb2YodD1yKHt9LEtuLHt9LFwib2JqZWN0XCI9PXR5cGVvZiB0JiZ0P3Q6e30pKS50YXJnZXQpe3ZhciBlPXQudGFyZ2V0LmlkO2V8fChlPXUoeG4pLHQudGFyZ2V0LmlkPWUpLHQudGFyZ2V0PVwiI1wiK2V9cmV0dXJuIHYoeG4sdCxWbiksdH0sZS5fZ2V0U2Nyb2xsVG9wPWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuX3Njcm9sbEVsZW1lbnQ9PT13aW5kb3c/dGhpcy5fc2Nyb2xsRWxlbWVudC5wYWdlWU9mZnNldDp0aGlzLl9zY3JvbGxFbGVtZW50LnNjcm9sbFRvcH0sZS5fZ2V0U2Nyb2xsSGVpZ2h0PWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuX3Njcm9sbEVsZW1lbnQuc2Nyb2xsSGVpZ2h0fHxNYXRoLm1heChkb2N1bWVudC5ib2R5LnNjcm9sbEhlaWdodCxkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsSGVpZ2h0KX0sZS5fZ2V0T2Zmc2V0SGVpZ2h0PWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuX3Njcm9sbEVsZW1lbnQ9PT13aW5kb3c/d2luZG93LmlubmVySGVpZ2h0OnRoaXMuX3Njcm9sbEVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0fSxlLl9wcm9jZXNzPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy5fZ2V0U2Nyb2xsVG9wKCkrdGhpcy5fY29uZmlnLm9mZnNldCxlPXRoaXMuX2dldFNjcm9sbEhlaWdodCgpLG49dGhpcy5fY29uZmlnLm9mZnNldCtlLXRoaXMuX2dldE9mZnNldEhlaWdodCgpO2lmKHRoaXMuX3Njcm9sbEhlaWdodCE9PWUmJnRoaXMucmVmcmVzaCgpLHQ+PW4pe3ZhciBpPXRoaXMuX3RhcmdldHNbdGhpcy5fdGFyZ2V0cy5sZW5ndGgtMV07dGhpcy5fYWN0aXZlVGFyZ2V0IT09aSYmdGhpcy5fYWN0aXZhdGUoaSl9ZWxzZXtpZih0aGlzLl9hY3RpdmVUYXJnZXQmJnQ8dGhpcy5fb2Zmc2V0c1swXSYmdGhpcy5fb2Zmc2V0c1swXT4wKXJldHVybiB0aGlzLl9hY3RpdmVUYXJnZXQ9bnVsbCx2b2lkIHRoaXMuX2NsZWFyKCk7Zm9yKHZhciBvPXRoaXMuX29mZnNldHMubGVuZ3RoO28tLTspe3RoaXMuX2FjdGl2ZVRhcmdldCE9PXRoaXMuX3RhcmdldHNbb10mJnQ+PXRoaXMuX29mZnNldHNbb10mJihcInVuZGVmaW5lZFwiPT10eXBlb2YgdGhpcy5fb2Zmc2V0c1tvKzFdfHx0PHRoaXMuX29mZnNldHNbbysxXSkmJnRoaXMuX2FjdGl2YXRlKHRoaXMuX3RhcmdldHNbb10pfX19LGUuX2FjdGl2YXRlPWZ1bmN0aW9uKHQpe3RoaXMuX2FjdGl2ZVRhcmdldD10LHRoaXMuX2NsZWFyKCk7dmFyIGU9dGhpcy5fc2VsZWN0b3Iuc3BsaXQoXCIsXCIpLm1hcCgoZnVuY3Rpb24oZSl7cmV0dXJuIGUrJ1tkYXRhLXRhcmdldD1cIicrdCsnXCJdLCcrZSsnW2hyZWY9XCInK3QrJ1wiXSd9KSksbj0kLmZpbmRPbmUoZS5qb2luKFwiLFwiKSk7bi5jbGFzc0xpc3QuY29udGFpbnMoRm4uRFJPUERPV05fSVRFTSk/KCQuZmluZE9uZShRbi5EUk9QRE9XTl9UT0dHTEUsJC5jbG9zZXN0KG4sUW4uRFJPUERPV04pKS5jbGFzc0xpc3QuYWRkKEZuLkFDVElWRSksbi5jbGFzc0xpc3QuYWRkKEZuLkFDVElWRSkpOihuLmNsYXNzTGlzdC5hZGQoRm4uQUNUSVZFKSwkLnBhcmVudHMobixRbi5OQVZfTElTVF9HUk9VUCkuZm9yRWFjaCgoZnVuY3Rpb24odCl7JC5wcmV2KHQsUW4uTkFWX0xJTktTK1wiLCBcIitRbi5MSVNUX0lURU1TKS5mb3JFYWNoKChmdW5jdGlvbih0KXtyZXR1cm4gdC5jbGFzc0xpc3QuYWRkKEZuLkFDVElWRSl9KSksJC5wcmV2KHQsUW4uTkFWX0lURU1TKS5mb3JFYWNoKChmdW5jdGlvbih0KXskLmNoaWxkcmVuKHQsUW4uTkFWX0xJTktTKS5mb3JFYWNoKChmdW5jdGlvbih0KXtyZXR1cm4gdC5jbGFzc0xpc3QuYWRkKEZuLkFDVElWRSl9KSl9KSl9KSkpLFoudHJpZ2dlcih0aGlzLl9zY3JvbGxFbGVtZW50LEJuLkFDVElWQVRFLHtyZWxhdGVkVGFyZ2V0OnR9KX0sZS5fY2xlYXI9ZnVuY3Rpb24oKXtFKCQuZmluZCh0aGlzLl9zZWxlY3RvcikpLmZpbHRlcigoZnVuY3Rpb24odCl7cmV0dXJuIHQuY2xhc3NMaXN0LmNvbnRhaW5zKEZuLkFDVElWRSl9KSkuZm9yRWFjaCgoZnVuY3Rpb24odCl7cmV0dXJuIHQuY2xhc3NMaXN0LnJlbW92ZShGbi5BQ1RJVkUpfSkpfSx0LmpRdWVyeUludGVyZmFjZT1mdW5jdGlvbihlKXtyZXR1cm4gdGhpcy5lYWNoKChmdW5jdGlvbigpe3ZhciBuPUEuZ2V0RGF0YSh0aGlzLFduKTtpZihufHwobj1uZXcgdCh0aGlzLFwib2JqZWN0XCI9PXR5cGVvZiBlJiZlKSksXCJzdHJpbmdcIj09dHlwZW9mIGUpe2lmKFwidW5kZWZpbmVkXCI9PXR5cGVvZiBuW2VdKXRocm93IG5ldyBUeXBlRXJyb3IoJ05vIG1ldGhvZCBuYW1lZCBcIicrZSsnXCInKTtuW2VdKCl9fSkpfSx0LmdldEluc3RhbmNlPWZ1bmN0aW9uKHQpe3JldHVybiBBLmdldERhdGEodCxXbil9LG4odCxudWxsLFt7a2V5OlwiVkVSU0lPTlwiLGdldDpmdW5jdGlvbigpe3JldHVyblwiNC4zLjFcIn19LHtrZXk6XCJEZWZhdWx0XCIsZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuIEtufX1dKSx0fSgpO1oub24od2luZG93LEJuLkxPQURfREFUQV9BUEksKGZ1bmN0aW9uKCl7RSgkLmZpbmQoUW4uREFUQV9TUFkpKS5mb3JFYWNoKChmdW5jdGlvbih0KXtyZXR1cm4gbmV3IFhuKHQsSXQuZ2V0RGF0YUF0dHJpYnV0ZXModCkpfSkpfSkpO3ZhciBxbj1UKCk7aWYocW4pe3ZhciB6bj1xbi5mblt4bl07cW4uZm5beG5dPVhuLmpRdWVyeUludGVyZmFjZSxxbi5mblt4bl0uQ29uc3RydWN0b3I9WG4scW4uZm5beG5dLm5vQ29uZmxpY3Q9ZnVuY3Rpb24oKXtyZXR1cm4gcW4uZm5beG5dPXpuLFhuLmpRdWVyeUludGVyZmFjZX19dmFyIFpuPVwiYnMudGFiXCIsJG49XCIuXCIrWm4sSm49e0hJREU6XCJoaWRlXCIrJG4sSElEREVOOlwiaGlkZGVuXCIrJG4sU0hPVzpcInNob3dcIiskbixTSE9XTjpcInNob3duXCIrJG4sQ0xJQ0tfREFUQV9BUEk6XCJjbGlja1wiKyRuK1wiLmRhdGEtYXBpXCJ9LHRpPVwiZHJvcGRvd24tbWVudVwiLGVpPVwiYWN0aXZlXCIsbmk9XCJkaXNhYmxlZFwiLGlpPVwiZmFkZVwiLG9pPVwic2hvd1wiLHJpPVwiLmRyb3Bkb3duXCIsc2k9XCIubmF2LCAubGlzdC1ncm91cFwiLGFpPVwiLmFjdGl2ZVwiLGxpPVwiOnNjb3BlID4gbGkgPiAuYWN0aXZlXCIsY2k9J1tkYXRhLXRvZ2dsZT1cInRhYlwiXSwgW2RhdGEtdG9nZ2xlPVwicGlsbFwiXSwgW2RhdGEtdG9nZ2xlPVwibGlzdFwiXScsdWk9XCIuZHJvcGRvd24tdG9nZ2xlXCIsZmk9XCI6c2NvcGUgPiAuZHJvcGRvd24tbWVudSAuYWN0aXZlXCIsaGk9ZnVuY3Rpb24oKXtmdW5jdGlvbiB0KHQpe3RoaXMuX2VsZW1lbnQ9dCxBLnNldERhdGEodGhpcy5fZWxlbWVudCxabix0aGlzKX12YXIgZT10LnByb3RvdHlwZTtyZXR1cm4gZS5zaG93PWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcztpZighKHRoaXMuX2VsZW1lbnQucGFyZW50Tm9kZSYmdGhpcy5fZWxlbWVudC5wYXJlbnROb2RlLm5vZGVUeXBlPT09Tm9kZS5FTEVNRU5UX05PREUmJnRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKGVpKXx8dGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMobmkpKSl7dmFyIGUsbj1kKHRoaXMuX2VsZW1lbnQpLGk9JC5jbG9zZXN0KHRoaXMuX2VsZW1lbnQsc2kpO2lmKGkpe3ZhciBvPVwiVUxcIj09PWkubm9kZU5hbWV8fFwiT0xcIj09PWkubm9kZU5hbWU/bGk6YWk7ZT0oZT1FKCQuZmluZChvLGkpKSlbZS5sZW5ndGgtMV19dmFyIHI9bnVsbDtpZihlJiYocj1aLnRyaWdnZXIoZSxKbi5ISURFLHtyZWxhdGVkVGFyZ2V0OnRoaXMuX2VsZW1lbnR9KSksIShaLnRyaWdnZXIodGhpcy5fZWxlbWVudCxKbi5TSE9XLHtyZWxhdGVkVGFyZ2V0OmV9KS5kZWZhdWx0UHJldmVudGVkfHxudWxsIT09ciYmci5kZWZhdWx0UHJldmVudGVkKSl7dGhpcy5fYWN0aXZhdGUodGhpcy5fZWxlbWVudCxpKTt2YXIgcz1mdW5jdGlvbigpe1oudHJpZ2dlcihlLEpuLkhJRERFTix7cmVsYXRlZFRhcmdldDp0Ll9lbGVtZW50fSksWi50cmlnZ2VyKHQuX2VsZW1lbnQsSm4uU0hPV04se3JlbGF0ZWRUYXJnZXQ6ZX0pfTtuP3RoaXMuX2FjdGl2YXRlKG4sbi5wYXJlbnROb2RlLHMpOnMoKX19fSxlLmRpc3Bvc2U9ZnVuY3Rpb24oKXtBLnJlbW92ZURhdGEodGhpcy5fZWxlbWVudCxabiksdGhpcy5fZWxlbWVudD1udWxsfSxlLl9hY3RpdmF0ZT1mdW5jdGlvbih0LGUsbil7dmFyIGk9dGhpcyxvPSghZXx8XCJVTFwiIT09ZS5ub2RlTmFtZSYmXCJPTFwiIT09ZS5ub2RlTmFtZT8kLmNoaWxkcmVuKGUsYWkpOiQuZmluZChsaSxlKSlbMF0scj1uJiZvJiZvLmNsYXNzTGlzdC5jb250YWlucyhpaSkscz1mdW5jdGlvbigpe3JldHVybiBpLl90cmFuc2l0aW9uQ29tcGxldGUodCxvLG4pfTtpZihvJiZyKXt2YXIgYT1nKG8pO28uY2xhc3NMaXN0LnJlbW92ZShvaSksWi5vbmUobyxjLHMpLG0obyxhKX1lbHNlIHMoKX0sZS5fdHJhbnNpdGlvbkNvbXBsZXRlPWZ1bmN0aW9uKHQsZSxuKXtpZihlKXtlLmNsYXNzTGlzdC5yZW1vdmUoZWkpO3ZhciBpPSQuZmluZE9uZShmaSxlLnBhcmVudE5vZGUpO2kmJmkuY2xhc3NMaXN0LnJlbW92ZShlaSksXCJ0YWJcIj09PWUuZ2V0QXR0cmlidXRlKFwicm9sZVwiKSYmZS5zZXRBdHRyaWJ1dGUoXCJhcmlhLXNlbGVjdGVkXCIsITEpfSh0LmNsYXNzTGlzdC5hZGQoZWkpLFwidGFiXCI9PT10LmdldEF0dHJpYnV0ZShcInJvbGVcIikmJnQuc2V0QXR0cmlidXRlKFwiYXJpYS1zZWxlY3RlZFwiLCEwKSxEKHQpLHQuY2xhc3NMaXN0LmNvbnRhaW5zKGlpKSYmdC5jbGFzc0xpc3QuYWRkKG9pKSx0LnBhcmVudE5vZGUmJnQucGFyZW50Tm9kZS5jbGFzc0xpc3QuY29udGFpbnModGkpKSYmKCQuY2xvc2VzdCh0LHJpKSYmRSgkLmZpbmQodWkpKS5mb3JFYWNoKChmdW5jdGlvbih0KXtyZXR1cm4gdC5jbGFzc0xpc3QuYWRkKGVpKX0pKSx0LnNldEF0dHJpYnV0ZShcImFyaWEtZXhwYW5kZWRcIiwhMCkpO24mJm4oKX0sdC5qUXVlcnlJbnRlcmZhY2U9ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMuZWFjaCgoZnVuY3Rpb24oKXt2YXIgbj1BLmdldERhdGEodGhpcyxabil8fG5ldyB0KHRoaXMpO2lmKFwic3RyaW5nXCI9PXR5cGVvZiBlKXtpZihcInVuZGVmaW5lZFwiPT10eXBlb2YgbltlXSl0aHJvdyBuZXcgVHlwZUVycm9yKCdObyBtZXRob2QgbmFtZWQgXCInK2UrJ1wiJyk7bltlXSgpfX0pKX0sdC5nZXRJbnN0YW5jZT1mdW5jdGlvbih0KXtyZXR1cm4gQS5nZXREYXRhKHQsWm4pfSxuKHQsbnVsbCxbe2tleTpcIlZFUlNJT05cIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm5cIjQuMy4xXCJ9fV0pLHR9KCk7Wi5vbihkb2N1bWVudCxKbi5DTElDS19EQVRBX0FQSSxjaSwoZnVuY3Rpb24odCl7dC5wcmV2ZW50RGVmYXVsdCgpLChBLmdldERhdGEodGhpcyxabil8fG5ldyBoaSh0aGlzKSkuc2hvdygpfSkpO3ZhciBkaT1UKCk7aWYoZGkpe3ZhciBnaT1kaS5mbi50YWI7ZGkuZm4udGFiPWhpLmpRdWVyeUludGVyZmFjZSxkaS5mbi50YWIuQ29uc3RydWN0b3I9aGksZGkuZm4udGFiLm5vQ29uZmxpY3Q9ZnVuY3Rpb24oKXtyZXR1cm4gZGkuZm4udGFiPWdpLGhpLmpRdWVyeUludGVyZmFjZX19dmFyIHBpPVwiYnMudG9hc3RcIixfaT1cIi5cIitwaSxtaT17Q0xJQ0tfRElTTUlTUzpcImNsaWNrLmRpc21pc3NcIitfaSxISURFOlwiaGlkZVwiK19pLEhJRERFTjpcImhpZGRlblwiK19pLFNIT1c6XCJzaG93XCIrX2ksU0hPV046XCJzaG93blwiK19pfSx2aT1cImZhZGVcIixFaT1cImhpZGVcIix5aT1cInNob3dcIixiaT1cInNob3dpbmdcIixEaT17YW5pbWF0aW9uOlwiYm9vbGVhblwiLGF1dG9oaWRlOlwiYm9vbGVhblwiLGRlbGF5OlwibnVtYmVyXCJ9LFRpPXthbmltYXRpb246ITAsYXV0b2hpZGU6ITAsZGVsYXk6NTAwfSxJaT0nW2RhdGEtZGlzbWlzcz1cInRvYXN0XCJdJyxBaT1mdW5jdGlvbigpe2Z1bmN0aW9uIHQodCxlKXt0aGlzLl9lbGVtZW50PXQsdGhpcy5fY29uZmlnPXRoaXMuX2dldENvbmZpZyhlKSx0aGlzLl90aW1lb3V0PW51bGwsdGhpcy5fc2V0TGlzdGVuZXJzKCksQS5zZXREYXRhKHQscGksdGhpcyl9dmFyIGU9dC5wcm90b3R5cGU7cmV0dXJuIGUuc2hvdz1mdW5jdGlvbigpe3ZhciB0PXRoaXM7aWYoIVoudHJpZ2dlcih0aGlzLl9lbGVtZW50LG1pLlNIT1cpLmRlZmF1bHRQcmV2ZW50ZWQpe3RoaXMuX2NvbmZpZy5hbmltYXRpb24mJnRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmFkZCh2aSk7dmFyIGU9ZnVuY3Rpb24oKXt0Ll9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoYmkpLHQuX2VsZW1lbnQuY2xhc3NMaXN0LmFkZCh5aSksWi50cmlnZ2VyKHQuX2VsZW1lbnQsbWkuU0hPV04pLHQuX2NvbmZpZy5hdXRvaGlkZSYmKHQuX3RpbWVvdXQ9c2V0VGltZW91dCgoZnVuY3Rpb24oKXt0LmhpZGUoKX0pLHQuX2NvbmZpZy5kZWxheSkpfTtpZih0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoRWkpLEQodGhpcy5fZWxlbWVudCksdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuYWRkKGJpKSx0aGlzLl9jb25maWcuYW5pbWF0aW9uKXt2YXIgbj1nKHRoaXMuX2VsZW1lbnQpO1oub25lKHRoaXMuX2VsZW1lbnQsYyxlKSxtKHRoaXMuX2VsZW1lbnQsbil9ZWxzZSBlKCl9fSxlLmhpZGU9ZnVuY3Rpb24oKXt2YXIgdD10aGlzO2lmKHRoaXMuX2VsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKHlpKSYmIVoudHJpZ2dlcih0aGlzLl9lbGVtZW50LG1pLkhJREUpLmRlZmF1bHRQcmV2ZW50ZWQpe3ZhciBlPWZ1bmN0aW9uKCl7dC5fZWxlbWVudC5jbGFzc0xpc3QuYWRkKEVpKSxaLnRyaWdnZXIodC5fZWxlbWVudCxtaS5ISURERU4pfTtpZih0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoeWkpLHRoaXMuX2NvbmZpZy5hbmltYXRpb24pe3ZhciBuPWcodGhpcy5fZWxlbWVudCk7Wi5vbmUodGhpcy5fZWxlbWVudCxjLGUpLG0odGhpcy5fZWxlbWVudCxuKX1lbHNlIGUoKX19LGUuZGlzcG9zZT1mdW5jdGlvbigpe2NsZWFyVGltZW91dCh0aGlzLl90aW1lb3V0KSx0aGlzLl90aW1lb3V0PW51bGwsdGhpcy5fZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoeWkpJiZ0aGlzLl9lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoeWkpLFoub2ZmKHRoaXMuX2VsZW1lbnQsbWkuQ0xJQ0tfRElTTUlTUyksQS5yZW1vdmVEYXRhKHRoaXMuX2VsZW1lbnQscGkpLHRoaXMuX2VsZW1lbnQ9bnVsbCx0aGlzLl9jb25maWc9bnVsbH0sZS5fZ2V0Q29uZmlnPWZ1bmN0aW9uKHQpe3JldHVybiB0PXIoe30sVGkse30sSXQuZ2V0RGF0YUF0dHJpYnV0ZXModGhpcy5fZWxlbWVudCkse30sXCJvYmplY3RcIj09dHlwZW9mIHQmJnQ/dDp7fSksdihcInRvYXN0XCIsdCx0aGlzLmNvbnN0cnVjdG9yLkRlZmF1bHRUeXBlKSx0fSxlLl9zZXRMaXN0ZW5lcnM9ZnVuY3Rpb24oKXt2YXIgdD10aGlzO1oub24odGhpcy5fZWxlbWVudCxtaS5DTElDS19ESVNNSVNTLElpLChmdW5jdGlvbigpe3JldHVybiB0LmhpZGUoKX0pKX0sdC5qUXVlcnlJbnRlcmZhY2U9ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMuZWFjaCgoZnVuY3Rpb24oKXt2YXIgbj1BLmdldERhdGEodGhpcyxwaSk7aWYobnx8KG49bmV3IHQodGhpcyxcIm9iamVjdFwiPT10eXBlb2YgZSYmZSkpLFwic3RyaW5nXCI9PXR5cGVvZiBlKXtpZihcInVuZGVmaW5lZFwiPT10eXBlb2YgbltlXSl0aHJvdyBuZXcgVHlwZUVycm9yKCdObyBtZXRob2QgbmFtZWQgXCInK2UrJ1wiJyk7bltlXSh0aGlzKX19KSl9LHQuZ2V0SW5zdGFuY2U9ZnVuY3Rpb24odCl7cmV0dXJuIEEuZ2V0RGF0YSh0LHBpKX0sbih0LG51bGwsW3trZXk6XCJWRVJTSU9OXCIsZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuXCI0LjMuMVwifX0se2tleTpcIkRlZmF1bHRUeXBlXCIsZ2V0OmZ1bmN0aW9uKCl7cmV0dXJuIERpfX0se2tleTpcIkRlZmF1bHRcIixnZXQ6ZnVuY3Rpb24oKXtyZXR1cm4gVGl9fV0pLHR9KCksU2k9VCgpO2lmKFNpKXt2YXIgd2k9U2kuZm4udG9hc3Q7U2kuZm4udG9hc3Q9QWkualF1ZXJ5SW50ZXJmYWNlLFNpLmZuLnRvYXN0LkNvbnN0cnVjdG9yPUFpLFNpLmZuLnRvYXN0Lm5vQ29uZmxpY3Q9ZnVuY3Rpb24oKXtyZXR1cm4gU2kuZm4udG9hc3Q9d2ksQWkualF1ZXJ5SW50ZXJmYWNlfX1yZXR1cm57QWxlcnQ6cnQsQnV0dG9uOkV0LENhcm91c2VsOlF0LENvbGxhcHNlOmFlLERyb3Bkb3duOmplLE1vZGFsOlplLFBvcG92ZXI6am4sU2Nyb2xsU3B5OlhuLFRhYjpoaSxUb2FzdDpBaSxUb29sdGlwOlRufX0pKTtcbiIsIlxuLyoqXG4gKiBkZW1vLmpzXG4gKiBodHRwOi8vd3d3LmNvZHJvcHMuY29tXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlLlxuICogaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAqXG4gKiBDb3B5cmlnaHQgMjAxOSwgQ29kcm9wc1xuICogaHR0cDovL3d3dy5jb2Ryb3BzLmNvbVxuICovXG5cbmNsYXNzIERlbW8zIHtcbiAgY29uc3RydWN0b3IoKSB7XG5cbiAgICB0aGlzLmluaXRDdXJzb3IoKTtcbiAgICB0aGlzLmluaXRIb3ZlcnMoKTtcbiAgfVxuXG4gIGluaXRDdXJzb3IoKSB7XG4gICAgY29uc3QgeyBCYWNrIH0gPSB3aW5kb3c7XG4gICAgdGhpcy5vdXRlckN1cnNvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuY2lyY2xlLWN1cnNvci0tb3V0ZXJcIik7XG4gICAgdGhpcy5pbm5lckN1cnNvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuY2lyY2xlLWN1cnNvci0taW5uZXJcIik7XG4gICAgdGhpcy5vdXRlckN1cnNvckJveCA9IHRoaXMub3V0ZXJDdXJzb3IuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgdGhpcy5vdXRlckN1cnNvclNwZWVkID0gMDtcbiAgICB0aGlzLmVhc2luZyA9IEJhY2suZWFzZU91dC5jb25maWcoMS43KTtcbiAgICB0aGlzLmNsaWVudFggPSAtMTAwO1xuICAgIHRoaXMuY2xpZW50WSA9IC0xMDA7XG4gICAgdGhpcy5zaG93Q3Vyc29yID0gZmFsc2U7XG5cbiAgICBjb25zdCB1bnZlaWxDdXJzb3IgPSAoKSA9PiB7XG4gICAgICBUd2Vlbk1heC5zZXQodGhpcy5pbm5lckN1cnNvciwge1xuICAgICAgICB4OiB0aGlzLmNsaWVudFgsXG4gICAgICAgIHk6IHRoaXMuY2xpZW50WVxuICAgICAgfSk7XG4gICAgICBUd2Vlbk1heC5zZXQodGhpcy5vdXRlckN1cnNvciwge1xuICAgICAgICB4OiB0aGlzLmNsaWVudFggLSB0aGlzLm91dGVyQ3Vyc29yQm94LndpZHRoIC8gMixcbiAgICAgICAgeTogdGhpcy5jbGllbnRZIC0gdGhpcy5vdXRlckN1cnNvckJveC5oZWlnaHQgLyAyXG4gICAgICB9KTtcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLm91dGVyQ3Vyc29yU3BlZWQgPSAwLjI7XG4gICAgICB9LCAxMDApO1xuICAgICAgdGhpcy5zaG93Q3Vyc29yID0gdHJ1ZTtcbiAgICB9O1xuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZW1vdmVcIiwgdW52ZWlsQ3Vyc29yKTtcblxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZW1vdmVcIiwgZSA9PiB7XG4gICAgICB0aGlzLmNsaWVudFggPSBlLmNsaWVudFg7XG4gICAgICB0aGlzLmNsaWVudFkgPSBlLmNsaWVudFk7XG4gICAgfSk7XG5cbiAgICBjb25zdCByZW5kZXIgPSAoKSA9PiB7XG4gICAgICBUd2Vlbk1heC5zZXQodGhpcy5pbm5lckN1cnNvciwge1xuICAgICAgICB4OiB0aGlzLmNsaWVudFgsXG4gICAgICAgIHk6IHRoaXMuY2xpZW50WVxuICAgICAgfSk7XG4gICAgICBpZiAoIXRoaXMuaXNTdHVjaykge1xuICAgICAgICBUd2Vlbk1heC50byh0aGlzLm91dGVyQ3Vyc29yLCB0aGlzLm91dGVyQ3Vyc29yU3BlZWQsIHtcbiAgICAgICAgICB4OiB0aGlzLmNsaWVudFggLSB0aGlzLm91dGVyQ3Vyc29yQm94LndpZHRoIC8gMixcbiAgICAgICAgICB5OiB0aGlzLmNsaWVudFkgLSB0aGlzLm91dGVyQ3Vyc29yQm94LmhlaWdodCAvIDJcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5zaG93Q3Vyc29yKSB7XG4gICAgICAgIGRvY3VtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZW1vdmVcIiwgdW52ZWlsQ3Vyc29yKTtcbiAgICAgIH1cbiAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShyZW5kZXIpO1xuICAgIH07XG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKHJlbmRlcik7XG4gIH1cblxuICBpbml0SG92ZXJzKCkge1xuICAgIGNvbnN0IGhhbmRsZU1vdXNlRW50ZXIgPSBlID0+IHtcbiAgICAgIHRoaXMuaXNTdHVjayA9IHRydWU7XG4gICAgICBjb25zdCB0YXJnZXQgPSBlLmN1cnJlbnRUYXJnZXQ7XG4gICAgICBjb25zdCBib3ggPSB0YXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICB0aGlzLm91dGVyQ3Vyc29yT3JpZ2luYWxzID0ge1xuICAgICAgICB3aWR0aDogdGhpcy5vdXRlckN1cnNvckJveC53aWR0aCxcbiAgICAgICAgaGVpZ2h0OiB0aGlzLm91dGVyQ3Vyc29yQm94LmhlaWdodFxuICAgICAgfTtcblxuICAgICAgVHdlZW5NYXguc2V0KHRoaXMuaW5uZXJDdXJzb3IsIHsgb3BhY2l0eTogMC43IH0pO1xuXG4gICAgICBUd2Vlbk1heC50byh0aGlzLm91dGVyQ3Vyc29yLCAwLjIsIHtcbiAgICAgICAgeDogYm94LmxlZnQgKyA0MCxcbiAgICAgICAgeTogYm94LmxlZnQgLSAyMixcbiAgICAgICAgd2lkdGg6IDQwLFxuICAgICAgICBoZWlnaHQ6NDAsXG4gICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgY29uc3QgaGFuZGxlTW91c2VMZWF2ZSA9ICgpID0+IHtcbiAgICAgIHRoaXMuaXNTdHVjayA9IGZhbHNlO1xuICAgICAgVHdlZW5NYXguc2V0KHRoaXMuaW5uZXJDdXJzb3IsIHsgb3BhY2l0eTogMSB9KTtcblxuICAgICAgVHdlZW5NYXgudG8odGhpcy5vdXRlckN1cnNvciwgMC4yLCB7XG4gICAgICAgIHdpZHRoOiB0aGlzLm91dGVyQ3Vyc29yT3JpZ2luYWxzLndpZHRoLFxuICAgICAgICBoZWlnaHQ6IHRoaXMub3V0ZXJDdXJzb3JPcmlnaW5hbHMuaGVpZ2h0LFxuICAgICAgICBvcGFjaXR5OiAwLjIsXG4gICAgICAgIGJvcmRlckNvbG9yOiBcIiNmZmZmZmZcIixcbiAgICAgICAgYm9yZGVyV2lkdGg6IDEsXG4gICAgICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogXCJ0cmFuc3BhcmVudFwiLFxuXG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgY29uc3QgbGlua0l0ZW1zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5uYXYtbGlua1wiKTtcbiAgICBsaW5rSXRlbXMuZm9yRWFjaChpdGVtID0+IHtcbiAgICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZW50ZXJcIiwgaGFuZGxlTW91c2VFbnRlcik7XG4gICAgICBpdGVtLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWxlYXZlXCIsIGhhbmRsZU1vdXNlTGVhdmUpO1xuICAgIH0pO1xuXG4gICAgY29uc3QgbWFpbk5hdkhvdmVyVHdlZW4gPSBUd2Vlbk1heC50byh0aGlzLm91dGVyQ3Vyc29yLCAwLjMsIHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogXCIjMDgxODMxXCIsXG4gICAgICBlYXNlOiB0aGlzLmVhc2luZyxcbiAgICAgIHBhdXNlZDogdHJ1ZSxcbiAgICAgIHdpZHRoOiA1MCxcbiAgICAgIGhlaWdodDo1MCxcbiAgICAgIG9wYWNpdHk6IDAuNixcbiAgICAgIGJvcmRlckNvbG9yOiBcIiMwODE4MzFcIlxuICAgIH0pO1xuXG4gICAgY29uc3QgbWFpbk5hdk1vdXNlRW50ZXIgPSAoKSA9PiB7XG4gICAgICB0aGlzLm91dGVyQ3Vyc29yU3BlZWQgPSAwO1xuICAgICAgVHdlZW5NYXguc2V0KHRoaXMuaW5uZXJDdXJzb3IsIHsgYmFja2dyb3VuZENvbG9yOiAnIzA4MTgzMScsIG9wYWNpdHk6IDAgfSk7XG4gICAgICBtYWluTmF2SG92ZXJUd2Vlbi5wbGF5KCk7XG4gICAgfTtcblxuICAgIGNvbnN0IG1haW5OYXZNb3VzZUxlYXZlID0gKCkgPT4ge1xuICAgICAgdGhpcy5vdXRlckN1cnNvclNwZWVkID0gMC4yO1xuICAgICAgVHdlZW5NYXguc2V0KHRoaXMuaW5uZXJDdXJzb3IsIHsgb3BhY2l0eTogMSB9KTtcbiAgICAgIG1haW5OYXZIb3ZlclR3ZWVuLnJldmVyc2UoKTtcbiAgICB9O1xuXG4gICAgY29uc3QgbWFpbk5hdkxpbmtzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5hamF4LWxpbmtcIik7XG4gICAgbWFpbk5hdkxpbmtzLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICBpdGVtLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWVudGVyXCIsIG1haW5OYXZNb3VzZUVudGVyKTtcbiAgICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbGVhdmVcIiwgbWFpbk5hdk1vdXNlTGVhdmUpO1xuICAgIH0pO1xuXG5cbiAgIFxuXG5cbiAgICBjb25zdCB3aGl0ZUhvdmVyVHdlZW4gPSBUd2Vlbk1heC50byh0aGlzLm91dGVyQ3Vyc29yLCAwLjMsIHtcbiAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgIGJvcmRlckNvbG9yOiBcIiMwODE4MzFcIixcbiAgICAgIGVhc2U6IHRoaXMuZWFzaW5nLFxuICAgICAgcGF1c2VkOiB0cnVlLFxuICAgICAgYmFja2dyb3VuZENvbG9yOiBcInJnYmEoMCwwLDAsMClcIlxuXG4gICAgfSk7XG5cblxuICAgIGNvbnN0IGhhbmRsZVdoaXRlRW50ZXIgPSBlID0+IHtcbiAgICAgdGhpcy5vdXRlckN1cnNvclNwZWVkID0gMDtcbiAgICAgICAgICBUd2Vlbk1heC5zZXQodGhpcy5pbm5lckN1cnNvciwgeyBiYWNrZ3JvdW5kQ29sb3I6ICcjMDgxODMxJyxvcGFjaXR5OiAxIH0pO1xuICAgICAgICAgIHdoaXRlSG92ZXJUd2Vlbi5wbGF5KCk7XG4gICAgfTtcblxuICAgIGNvbnN0IGhhbmRsZVdoaXRlTGVhdmUgPSAoKSA9PiB7XG4gICAgICB0aGlzLm91dGVyQ3Vyc29yU3BlZWQgPSAwLjI7XG4gICAgICBUd2Vlbk1heC5zZXQodGhpcy5pbm5lckN1cnNvciwgeyAgYmFja2dyb3VuZENvbG9yOiAnI2ZmZicsIG9wYWNpdHk6IDEgfSk7XG4gICAgICB3aGl0ZUhvdmVyVHdlZW4ucmV2ZXJzZSgpO1xuICAgIH07XG5cbiAgICBjb25zdCBicmlnaHQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmJnLWJyaWdodFwiKTtcbiAgICBicmlnaHQuZm9yRWFjaChpdGVtID0+IHtcbiAgICAgIGl0ZW0uYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZW50ZXJcIiwgaGFuZGxlV2hpdGVFbnRlcik7XG4gICAgICBpdGVtLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWxlYXZlXCIsIGhhbmRsZVdoaXRlTGVhdmUpO1xuICAgIH0pO1xuXG5cblxuXG4gICAgY29uc3QgYmx1ZUhvdmVyVHdlZW4gPSBUd2Vlbk1heC50byh0aGlzLm91dGVyQ3Vyc29yLCAwLjMsIHtcbiAgICAvLyAgYmFja2dyb3VuZENvbG9yOiBcIiNmZmZmZmZcIixcbiAgICAgIGVhc2U6IHRoaXMuZWFzaW5nLFxuICAgICAgcGF1c2VkOiB0cnVlLFxuICAgICAgb3BhY2l0eTogMSxcbiAgICAgIGJvcmRlckNvbG9yOiBcIiNmZmZcIlxuICAgIH0pO1xuXG4gICAgY29uc3QgYmx1ZU1vdXNlRW50ZXIgPSAoKSA9PiB7XG4gICAgICB0aGlzLm91dGVyQ3Vyc29yU3BlZWQgPSAwO1xuICAgICAgVHdlZW5NYXguc2V0KHRoaXMuaW5uZXJDdXJzb3IsIHsgYmFja2dyb3VuZENvbG9yOiAnI2ZmZicsIG9wYWNpdHk6IDEgfSk7XG4gICAgICBibHVlSG92ZXJUd2Vlbi5wbGF5KCk7XG4gICAgfTtcblxuICAgIGNvbnN0IGJsdWVNb3VzZUxlYXZlID0gKCkgPT4ge1xuICAgICAgdGhpcy5vdXRlckN1cnNvclNwZWVkID0gMC4yO1xuICAgICAgVHdlZW5NYXguc2V0KHRoaXMuaW5uZXJDdXJzb3IsIHsgIGJhY2tncm91bmRDb2xvcjogJyMwODE4MzEnLCBvcGFjaXR5OiAxIH0pO1xuXG4gICAgICBibHVlSG92ZXJUd2Vlbi5yZXZlcnNlKCk7XG4gICAgfTtcblxuICAgIGNvbnN0IGJsdWVfYmcgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmJnLWJsdWVcIik7XG4gICAgYmx1ZV9iZy5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgaXRlbS5hZGRFdmVudExpc3RlbmVyKFwibW91c2VlbnRlclwiLCBibHVlTW91c2VFbnRlcik7XG4gICAgICBpdGVtLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWxlYXZlXCIsIGJsdWVNb3VzZUxlYXZlKTtcbiAgICB9KTtcblxuICB9XG59XG5cbmNvbnN0IG1lbnUgPSBuZXcgRGVtbzMoKTtcbiIsIiFmdW5jdGlvbih0KXtmdW5jdGlvbiBlKGUsbixpLHIpe3ZhciBhPWUudGV4dCgpLGM9YS5zcGxpdChuKSxzPVwiXCI7Yy5sZW5ndGgmJih0KGMpLmVhY2goZnVuY3Rpb24odCxlKXtzKz0nPHNwYW4gY2xhc3M9XCInK2krKHQrMSkrJ1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPicrZStcIjwvc3Bhbj5cIityfSksZS5hdHRyKFwiYXJpYS1sYWJlbFwiLGEpLmVtcHR5KCkuYXBwZW5kKHMpKX12YXIgbj17aW5pdDpmdW5jdGlvbigpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtlKHQodGhpcyksXCJcIixcImNoYXJcIixcIlwiKX0pfSx3b3JkczpmdW5jdGlvbigpe3JldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKXtlKHQodGhpcyksXCIgXCIsXCJ3b3JkXCIsXCIgXCIpfSl9LGxpbmVzOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpe3ZhciBuPVwiZWVmZWMzMDMwNzlhZDE3NDA1Yzg4OWUwOTJlMTA1YjBcIjtlKHQodGhpcykuY2hpbGRyZW4oXCJiclwiKS5yZXBsYWNlV2l0aChuKS5lbmQoKSxuLFwibGluZVwiLFwiXCIpfSl9fTt0LmZuLmxldHRlcmluZz1mdW5jdGlvbihlKXtyZXR1cm4gZSYmbltlXT9uW2VdLmFwcGx5KHRoaXMsW10uc2xpY2UuY2FsbChhcmd1bWVudHMsMSkpOlwibGV0dGVyc1wiIT09ZSYmZT8odC5lcnJvcihcIk1ldGhvZCBcIitlK1wiIGRvZXMgbm90IGV4aXN0IG9uIGpRdWVyeS5sZXR0ZXJpbmdcIiksdGhpcyk6bi5pbml0LmFwcGx5KHRoaXMsW10uc2xpY2UuY2FsbChhcmd1bWVudHMsMCkpfX0oalF1ZXJ5KTsiLCIvKipcbiAqXG4gKiBUaGVtZSBDdXN0b21pemVyIGVuaGFuY2VtZW50cyBmb3IgYSBiZXR0ZXIgdXNlciBleHBlcmllbmNlLlxuICpcbiAqIENvbnRhaW5zIGhhbmRsZXJzIHRvIG1ha2UgVGhlbWUgQ3VzdG9taXplciBwcmV2aWV3IHJlbG9hZCBjaGFuZ2VzIGFzeW5jaHJvbm91c2x5LlxuICovXG4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTmFtZT1cImpzXCI7XG4gVHdlZW5MaXRlLmRlZmF1bHRPdmVyd3JpdGUgPSBmYWxzZTtcbiB2YXIgV2ViRm9udENvbmZpZyA9IHtcbiAgIHR5cGVraXQ6IHtcbiAgICAgaWQ6ICdjaW0wZWp1J1xuICAgfVxuIH07XG4gV2ViRm9udC5sb2FkKCBXZWJGb250Q29uZmlnICk7XG5cblxuIHZhciBsYXp5TG9hZEluc3RhbmNlID0gbmV3IExhenlMb2FkKHtcbiAgICAgZWxlbWVudHNfc2VsZWN0b3I6IFwiLmxhenlsb2FkXCJcbiAgICAgLy8gLi4uIG1vcmUgY3VzdG9tIHNldHRpbmdzP1xuIH0pO1xuICAgIFxuXG5cbiggZnVuY3Rpb24oICQgKSB7XG5cblxuXG5cdHZhciBoZWFkbGluZSA9ICQoXCJoMVwiKTtcblx0Y29uc3QgY2hhciA9ICdbY2xhc3MqPVwiY2hhclwiXSc7XG5cdHZhciBidXR0b24gPSAkKCcuYnRuLXF1b3RlJyk7XG5cdHZhciAkbG9hZGVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxvYWRlcicpLFxuXHQgbWFpbkNvbnRlbnQgPSAkKCcuc2l0ZS1jb250ZW50JyksXG5cdCBpc0FuaW1hdGluZyA9IGZhbHNlO1xuXHQgY29uc3QgJHNwYW4gPSAkKCcuanMtc3BhbicpO1xuXG5cblxuXHQvLyBVc2luZyBsZXR0ZXJpbmcuanMgdG8gd3JhcCBhIDxzcGFuPiBhcm91bmQgZWFjaCB3b3JkIGFuZCBhIDxzcGFuPiBhcm91bmQgZWFjaCBjaGFyYWN0ZXIuXG5cdGhlYWRsaW5lXG5cdCAgLmxldHRlcmluZygnd29yZHMnKVxuXHQgIC5jaGlsZHJlbignc3BhbicpLmxldHRlcmluZygpO1xuXG5cblxuXHQvL0VMJ3Ncblx0Y29uc3QgJGJnID0gJCgnLmpzLWJnJyk7XG5cdGNvbnN0ICRoZWFkaW5nID0gJCgnLmpzLWhlYWRpbmcnKTtcblx0Y29uc3QgcHJlbG9hZGVyID0gJCgnI3ByZWxvYWRlcicpO1xuXG5cdC8vVElNRUxJTkVcblx0Y29uc3QgcGxUbCA9IG5ldyBUaW1lbGluZU1heCh7XG5cdCAgcGF1c2VkOiAhMCxcblx0ICAvL2RlbGF5OiAzLFxuXHQgIG9uQ29tcGxldGU6ICgpID0+IHtcblx0ICAgIGJ1dHRvbi5hZGRDbGFzcygnaXMtYWN0aXZlJyk7XG5cdCAgICAvLyQoXCIjcHJlbG9hZGVyXCIpLnJlbW92ZSgpXG5cdCAgfVxuXHR9KTtcblxuXG5cdGlmKCQoJy5wYWdlLWhlYWRlci1pbm5lcicpLmxlbmd0aCA+IDApIHtcblxuXG5cdHBsVGwuYWRkKCdzdGFydCcpXG5cblx0LnRvKHByZWxvYWRlciwgMS4yLCB7XG5cdCAgICAgICAgICAgICAgICAgICAgICAgIHk6IFwiLTEwMCVcIixcblx0ICAgICAgICAgICAgICAgICAgICAgICAgZWFzZTogQ2lyYy5lYXNlT3V0Ly9DdXN0b21FYXNlLmNyZWF0ZShcImN1c3RvbVwiLCBcIk0wLDAgQzAuODYsMCAwLjA3LDEgMSwxXCIpXG5cdCAgICAgICAgICAgICAgICAgICAgfSwgXCItPTBcIilcblx0XG5cblx0LmZyb21UbygkYmcsIDAuNCwge1xuXHQgIGF1dG9BbHBoYTogMCxcblx0fSwge1xuXHQgIGF1dG9BbHBoYTogMSxcblx0ICBlYXNlOiBQb3dlcjMuZWFzZU91dFxuXHR9LCAnc3RhcnQnKVxuXG5cdC5mcm9tVG8oJGJnLCAxLjgsIHtcblx0ICBzY2FsZTogMixcblx0ICB4OiAnLTcwJSdcblx0fSwge1xuXHQgIHNjYWxlOiAxLFxuXHQgIHg6ICcwJScsXG5cdCAgZWFzZTogUG93ZXIzLmVhc2VPdXRcblx0fSwgJ3N0YXJ0JylcblxuXG5cblx0Ly8gLmZyb21UbygkaGVhZGluZywgMC40LCB7XG5cdC8vICAgYXV0b0FscGhhOiAwLFxuXHQvLyB9LCB7XG5cdC8vICAgYXV0b0FscGhhOiAxLFxuXHQvLyAgIGVhc2U6IFBvd2VyMy5lYXNlT3V0XG5cdC8vIH0sICdzdGFydCcpXG5cblxuXHQuc3RhZ2dlckZyb20oJHNwYW4sIDAuNCwge1xuXHRcdCAgb3BhY2l0eTogMCxcblx0XHQgIGVhc2U6IFBvd2VyMy5lYXNlT3V0LFxuXHRcdCAgeTogJzEwMCUnXG5cdFx0fSwgJzAuMDUnKVxuXG5cdC5zdGFnZ2VyRnJvbShjaGFyLCAwLjUsIHt5OiA4MCwgb3BhY2l0eTogMCwgZWFzZTogRWxhc3RpYy5lYXNlT3V0fSwgMC4wMSk7XG5cblxuXG5cdC8vIC5zdGFnZ2VyRnJvbShjaGFyLCAxLCB7XG5cdC8vIFx0ICBvcGFjaXR5OiAwLFxuXHQvLyBcdCAgZWFzZTogRWxhc3RpYy5lYXNlT3V0LFxuXHQvLyBcdCAgeTogJzEwMCUnXG5cdC8vIFx0fSwgJzAuMDUnKTtcblxuXG5cdFxuXG5cblx0JCgnLndyYXBwZXInKS5pbWFnZXNMb2FkZWQoIHsgYmFja2dyb3VuZDogJy5wYWdlLWhlYWRlci1pbm5lcicgfSwgZnVuY3Rpb24oKSB7XG5cdCAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QucmVtb3ZlKCdsb2FkaW5nJyk7XG5cdFx0cGxUbC5wbGF5KClcblxuXHR9KTtcblxuXHR9XG5cblx0ZWxzZSB7XG5cdFx0bmV3IFR3ZWVuTWF4LnRvKHByZWxvYWRlciwgMS4yLCB7XG5cdFx0ICAgICAgICAgICAgICAgICAgICAgICAgeTogXCItMTAwJVwiLFxuXHRcdCAgICAgICAgICAgICAgICAgICAgICAgIGVhc2U6IENpcmMuZWFzZU91dC8vQ3VzdG9tRWFzZS5jcmVhdGUoXCJjdXN0b21cIiwgXCJNMCwwIEMwLjg2LDAgMC4wNywxIDEsMVwiKVxuXHRcdCAgICAgICAgICAgICAgICAgICAgfSwgXCItPTBcIilcblx0fVxuXG5cblxualF1ZXJ5KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigkKSB7XG5cblxuXG5cdCQoJy5uYXZiYXInKS5vbignY2xpY2snLCAnLm5hdi1saW5rJywgZnVuY3Rpb24oZSkge1xuXHRcdGUucHJldmVudERlZmF1bHQoKTtcblxuXG5cdFx0XHR2YXIgcGF0aCA9ICQodGhpcykuYXR0cignaHJlZicpO1xuXHRcdFx0dmFyIGxvY2F0aW9uID0gd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lLnRvU3RyaW5nKCk7XG5cdFx0XHR2YXIgcGF0aE5hbWUgPSAkKHRoaXMpLnRleHQoKS50b1N0cmluZygpLnRvTG93ZXJDYXNlKCk7XG5cblxuXHRcdFx0XHRcdC8vIGVsc2UgY2hlY2sgaWYgd2UgYXJlIGluc2lkZSBhIG1ham9yIHNlY3Rpb24gKGkuZS4gYmxvZylcblx0XHRcdFx0XHQgaWYgKGxvY2F0aW9uLmluZGV4T2YocGF0aE5hbWUpICE9PSAtMSkge1xuXHRcdFx0XHRcdFx0Y29uc29sZS5sb2coJ3NvbWV3aGVyZSBpbnNpZGUgJywgcGF0aE5hbWUsIHBhdGgsIGxvY2F0aW9uKTtcblx0XHRcdFx0XHRcdHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQodHJ1ZSk7XG5cdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cblx0XHRcdFx0XHR9XG5cblx0ICAkbG9hZGVyLmNsYXNzTGlzdC5hZGQoJ2xvYWRlci0tYWN0aXZlJylcblxuXG5cdCAgdmFyICR0aGlzID0gJCh0aGlzKTtcblx0ICAkdGhpcy5wYXJlbnQoKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKS5lbmQoKS5hZGRDbGFzcygnYWN0aXZlJyk7XG5cblxuXHQgIHZhciBwYWdlUmVmID0gJCh0aGlzKS5hdHRyKCdocmVmJyk7XG5cdCAgdmFyIHRhcmdldFRpdGxlID0gJCh0aGlzKS5hdHRyKCd0aXRsZScpO1xuXHQgIHZhciBwYWdlU2x1ZyA9ICQodGhpcykuYXR0cignZGF0YS1tZW51Jyk7XG5cblx0Y2FsbFBhZ2UocGFnZVJlZiwgcGFnZVNsdWcsIHRhcmdldFRpdGxlKTtcblxuXHR9KTtcblxuXG5cbndpbmRvdy5vbnBvcHN0YXRlID0gZnVuY3Rpb24oZSkge1xuICAgICAvLyQoXCIubmF2LWxpbmtcIikuZmFkZVRvKCdmYXN0JywgMS4wKTtcbiAgICAgY2FsbFBhZ2UoZS5zdGF0ZSA/IGUuc3RhdGUudXJsIDogbnVsbCk7XG4gfTtcblxuXG5cblxuXG5cbmZ1bmN0aW9uIGNhbGxQYWdlKHBhZ2VSZWZJbnB1dCwgcGFnZVNsdWcsIHRhcmdldFRpdGxlSW5wdXQpe1xuXHR3aW5kb3cuaGlzdG9yeS5wdXNoU3RhdGUoe3VybDogXCJcIiArIHBhZ2VSZWZJbnB1dCArIFwiXCJ9LCB0YXJnZXRUaXRsZUlucHV0LCBwYWdlUmVmSW5wdXQpO1xuXG5cdHZhciAkdGhpcyA9ICQodGhpcyk7XG5cdCR0aGlzLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpLmVuZCgpLmFkZENsYXNzKCdhY3RpdmUnKTtcblx0IHZhciBuZXdTZWN0aW9uID0gcGFnZVNsdWc7XG5cblx0IHZhciBwcmltZXJvID0gJCgnPGRpdj48L2Rpdj4nKTtcblxuXHQgcHJpbWVyby5sb2FkKG5ld1NlY3Rpb24sIGZ1bmN0aW9uKGUpe1xuXHQgdmFyIHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuXHQgdmFyIGN1cnJlbnRIVE1MID0gZTtcblx0ICAgICAgICAgICAgICAgICAgICAgdC5pbm5lckhUTUwgPSBlO1xuXHQgICAgICAgICAgICAgICAgICAgICB2YXIgaSA9IHQucXVlcnlTZWxlY3RvcihcInRpdGxlXCIpO1xuXHQgICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC50aXRsZSA9IGkudGV4dENvbnRlbnRcblxuXHQgICAgICAgICAgICAgICAgIH0pO1xuXG5cdFxuXG5cdC8vY3JlYXRlIGEgbmV3IHNlY3Rpb24gZWxlbWVudCBhbmQgaW5zZXJ0IGl0IGludG8gdGhlIERPTVxuXHR2YXIgc2VjdGlvbiA9ICQoJzxtYWluIGNsYXNzPVwiY2QtbWFpbiBvdmVyZmxvdy1oaWRkZW4gJytuZXdTZWN0aW9uKydcIj48L21haW4+JykuYXBwZW5kVG8obWFpbkNvbnRlbnQpO1xuXHQvL2xvYWQgdGhlIG5ldyBjb250ZW50IGZyb20gdGhlIHByb3BlciBodG1sIGZpbGVcblx0c2VjdGlvbi5sb2FkKG5ld1NlY3Rpb24rJyAuY2QtbWFpbiA+IConLCBmdW5jdGlvbihldmVudCl7XG5cblx0XHRzZWN0aW9uLnByZXYoJy52aXNpYmxlJykucmVtb3ZlQ2xhc3MoJ3Zpc2libGUnKS5lbmQoKS5hZGRDbGFzcygndmlzaWJsZScpO1xuXHRcdHJlc2V0QWZ0ZXJBbmltYXRpb24oc2VjdGlvbik7XG5cblxuXHR9KTtcblxuXG4gIH1cblxuIFxuXG4gIGZ1bmN0aW9uIHJlc2V0QWZ0ZXJBbmltYXRpb24obmV3U2VjdGlvbikge1xuICAgICQoJ2h0bWwsIGJvZHknKS5hbmltYXRlKHsgc2Nyb2xsVG9wOiAwIH0sIDgwMCk7XG5cblxuICBcdFx0Ly9vbmNlIHRoZSBuZXcgc2VjdGlvbiBhbmltYXRpb24gaXMgb3ZlciwgcmVtb3ZlIHRoZSBvbGQgc2VjdGlvbiBhbmQgbWFrZSB0aGUgbmV3IG9uZSBzY3JvbGxhYmxlXG4gIFx0XHRuZXdTZWN0aW9uLnJlbW92ZUNsYXNzKCdvdmVyZmxvdy1oaWRkZW4nKS5wcmV2KCcuY2QtbWFpbicpLnJlbW92ZSgpO1xuICBcdFx0aXNBbmltYXRpbmcgPSAgZmFsc2U7XG5cbiAgICAgIGluaXQoKTtcbiAgXHRcdC8vcmVzZXQgeW91ciBsb2FkaW5nIGJhclxuXG4gIFx0XHQvL0VMJ3NcbiAgXHRcdGNvbnN0ICRiZyA9ICQoJy5qcy1iZycpO1xuICBcdFx0Y29uc3QgJGhlYWRpbmcgPSAkKCcuanMtaGVhZGluZycpO1xuICBcdFx0Y29uc3QgJGNoYXIgPSAnW2NsYXNzKj1cIndvcmRcIl0nO1xuICBcdFx0Y29uc3QgaGVhZGxpbmUgPSAkKFwiaDFcIik7XG4gIFx0XHRjb25zdCAkc3BhbiA9ICQoJy5qcy1zcGFuJyk7XG5cblxuICBcdFx0aGVhZGxpbmVcbiAgXHRcdCAgLmxldHRlcmluZygnd29yZHMnKVxuXG5cbiAgXHRcdC8vVElNRUxJTkVcbiAgXHRcdGNvbnN0IHBsVGwgPSBuZXcgVGltZWxpbmVNYXgoe1xuICBcdFx0ICAvL3BhdXNlZDogdHJ1ZSxcbiAgXHRcdCAgLy9kZWxheTogMyxcbiAgXHRcdCAgb25Db21wbGV0ZTogKCkgPT4ge1xuICBcdFx0ICAgICQoJyNwcmVsb2FkZXInKS5yZW1vdmUoKTtcbiAgXHRcdCAgfVxuICBcdFx0fSk7XG5cblxuXG5cbiAgXHRcdGlmKCQoJy5wYWdlLWhlYWRlci1pbm5lcicpLmxlbmd0aCA+IDApIHtcblxuXG4gIFx0XHRwbFRsLmFkZCgnc3RhcnQnKVxuXG4gIFx0XHQudG8ocHJlbG9hZGVyLCAxLjIsIHtcblx0XHRcdHk6IFwiLTEwMCVcIixcblx0XHRcdGVhc2U6IENpcmMuZWFzZU91dC8vQ3VzdG9tRWFzZS5jcmVhdGUoXCJjdXN0b21cIiwgXCJNMCwwIEMwLjg2LDAgMC4wNywxIDEsMVwiKVxuICBcdFx0ICAgICAgICAgICAgICAgICAgICB9LCBcIi09MFwiKVxuICBcdFx0XG5cbiAgXHRcdC5mcm9tVG8oJGJnLCAwLjQsIHtcbiAgXHRcdCAgYXV0b0FscGhhOiAwLFxuICBcdFx0fSwge1xuICBcdFx0ICBhdXRvQWxwaGE6IDEsXG4gIFx0XHQgIGVhc2U6IFBvd2VyMy5lYXNlT3V0XG4gIFx0XHR9LCAnc3RhcnQnKVxuXG4gIFx0XHQuZnJvbVRvKCRiZywgMS44LCB7XG4gIFx0XHQgIHNjYWxlOiAyLFxuICBcdFx0ICB4OiAnLTcwJSdcbiAgXHRcdH0sIHtcbiAgXHRcdCAgc2NhbGU6IDEsXG4gIFx0XHQgIHg6ICcwJScsXG4gIFx0XHQgIGVhc2U6IFBvd2VyMy5lYXNlT3V0XG4gIFx0XHR9LCAnc3RhcnQnKVxuXG4gIFx0XHRcblxuXG4gIFx0XHQuZnJvbVRvKCRoZWFkaW5nLCAwLjQsIHtcbiAgXHRcdCAgb3BhY2l0eTogMCxcbiAgXHRcdH0sIHtcbiAgXHRcdCAgb3BhY2l0eTogMSxcbiAgXHRcdCAgZWFzZTogRWxhc3RpYy5lYXNlT3V0XG4gIFx0XHR9LCAnc3RhcnQnKVxuXG5cbiAgXHRcdC5zdGFnZ2VyRnJvbSgkc3BhbiwgMC40LCB7XG4gIFx0XHRcdCAgb3BhY2l0eTogMCxcbiAgXHRcdFx0ICBlYXNlOiBFbGFzdGljLmVhc2VPdXQsXG4gIFx0XHRcdCAgeTogJzEwMCUnXG4gIFx0XHRcdH0sICcwLjA1JylcbiAgXHRcdC5zdGFnZ2VyRnJvbSgkY2hhciwgMSwge1xuICBcdFx0XHQgIG9wYWNpdHk6IDAsXG4gIFx0XHRcdCAgZWFzZTogRWxhc3RpYy5lYXNlT3V0LFxuICBcdFx0XHQgIHk6ICcxMDAlJ1xuICBcdFx0XHR9LCAnMC4wNScpXG5cbiAgXHRcdDtcblxuXG5cblxuICBcdFx0aW1hZ2VzTG9hZGVkKCBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucGFnZS1oZWFkZXItaW5uZXInKSwgeyBiYWNrZ3JvdW5kOiB0cnVlIH0sIGZ1bmN0aW9uKCBpbnN0YW5jZSApIHtcbiAgXHRcdCAgICAkbG9hZGVyLmNsYXNzTGlzdC5yZW1vdmUoJ2xvYWRlci0tYWN0aXZlJylcbiAgXHRcdCAgXHRwbFRsLnBsYXkoKVxuICBcdFx0fSk7XG5cbiAgXHR9XG5cbiAgXHRlbHNlIFxuICBcdHtcblxuICBcdFx0ICAgICRsb2FkZXIuY2xhc3NMaXN0LnJlbW92ZSgnbG9hZGVyLS1hY3RpdmUnKVxuXG4gIFx0fVxuXG5cbiAgXHR9XG5cblxuXG5cbiAgXHRcdH0pO1xuXG5cblxuXG5cblxufSApKCBqUXVlcnkgKTtcblxuXG5cblxuLy9JbWcgbG9hZFxuZnVuY3Rpb24gaW1nTG9hZCgpIHtcbiAgICAkKCcucG9ydGZvbGlvLWl0ZW0nKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgaW1hZ2UgPSAkKHRoaXMpLmZpbmQoJ2EnKS5kYXRhKCdzcmMnKTtcbiAgICAgICAgJCh0aGlzKS5maW5kKCcucG9ydGZvbGlvLWltZy1jb250ZW50JykuY3NzKHsgJ2JhY2tncm91bmQtaW1hZ2UnOiAndXJsKCcgKyBpbWFnZSArICcpJywgJ2JhY2tncm91bmQtc2l6ZSc6ICdjb3ZlcicsICdiYWNrZ3JvdW5kLXBvc2l0aW9uJzogJ2NlbnRlcicgfSk7XG4gICAgfSk7XG59XG5cblxuLy9QYXJhbGxheCBFZmZlY3RcbmZ1bmN0aW9uIHBhcmFsbGF4YmcoKSB7XG4gICAgaWYgKCQoJy5wYXJhbGxheC1pbm5lcicpLmxlbmd0aCA+IDApIHtcbiAgICAgICAgJCgnLnBhcmFsbGF4LWJhY2tncm91bmQnKS5wYXJhbGxheEJhY2tncm91bmQoe1xuICAgICAgICAgICAgZXZlbnQ6ICdtb3VzZV9tb3ZlJyxcbiAgICAgICAgICAgIGFuaW1hdGlvbl90eXBlOiAnc2hpZnQnLFxuICAgICAgICAgICAgYW5pbWF0ZV9kdXJhdGlvbjogMyxcbiAgICAgICAgfSk7XG5cbiAgICB9XG59XG5cblxuZnVuY3Rpb24gcG9ydGZvbGlvU2V0dGluZ3MoKSB7XG4gIHZhciBncmlkID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmdyaWQnKTtcblxuICBpZihncmlkKSB7XG5cblxuICB2YXIgJGdyaWQgPSAkKCcuZ3JpZCcpLmlzb3RvcGUoe1xuICAgIGl0ZW1TZWxlY3RvcjogJy5ncmlkX19pdGVtJyxcbiAvLyAgIHBlcmNlbnRQb3NpdGlvbjogdHJ1ZSxcbiAgICAgZmlsdGVyOiAnKicsXG4gICAgICB2aXNpYmxlU3R5bGU6IHsgdHJhbnNmb3JtOiAndHJhbnNsYXRlWSgwKScsIG9wYWNpdHk6IDEgfSxcbiAgICAgIGhpZGRlblN0eWxlOiB7IHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoMTAwcHgpJywgb3BhY2l0eTogMCB9LFxuXG4gICBtYXNvbnJ5OiB7XG4gICAgICAgY29sdW1uV2lkdGg6ICcuZ3JpZF9faXRlbSdcbiAgIH0sXG5cblxuICAgICB9KTtcblxuXG5cbiAgJGdyaWQub24oICdsYXlvdXRDb21wbGV0ZScsIGZ1bmN0aW9uKCkge1xuICAgICQoJy5sb2FkZXItNScpLmhpZGUoKTtcbiAgICAkKCcuc2VnbWVudGVkLWNvbnRyb2wnKS5jc3MoJ29wYWNpdHknLCAnMScpO1xuXG4gIH0pO1xuXG4gICRncmlkLmltYWdlc0xvYWRlZCgpLnByb2dyZXNzKCBmdW5jdGlvbigpIHtcbiAgICBncmlkLmNsYXNzTGlzdC5yZW1vdmUoJ2FyZS1pbWFnZXMtdW5sb2FkZWQnKTtcbiAgICAkZ3JpZC5pc290b3BlKCdsYXlvdXQnKTtcbiAgfSk7XG5cblxuICB2YXIgJGZpbHRlckxpbmtzID0gJCgnLnNlZ21lbnRlZC1jb250cm9sID4gbGknKTtcblxuXG4gICAkZmlsdGVyTGlua3MuY2xpY2soZnVuY3Rpb24oKXtcbiAgICAgIHZhciAkdGhpcyA9ICQodGhpcyk7XG5cbiAgICAgJGZpbHRlckxpbmtzLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICQodGhpcykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXG4gICAgICB2YXIgc2VsZWN0b3IgPSAkdGhpcy5kYXRhKCdmaWx0ZXInKTtcbiAgICAgICAkZ3JpZC5pc290b3BlKHtcbiAgICAgICAgZmlsdGVyOiBzZWxlY3RvclxuICAgICAgfSk7XG5cbiAgICB9KTtcblxuXG4gIH1cbn1cblxuXG5mdW5jdGlvbiBpbnB1dEVmZmVjdHMoKXtcblxuICAkKCcuYnRuLW91dGxpbmUnKS5sZXR0ZXJpbmcoJ2xldHRlcnMnKTtcblxuICAkLmVhY2goJCgnLmJ0bi1vdXRsaW5lJyksIGZ1bmN0aW9uKGluZGV4LCByZXZlYWxfaXRlbSkge1xuXG4gICAgJCh0aGlzKS5jaGlsZHJlbigpLndyYXBBbGwoXCI8c3Bhbj48L3NwYW4+XCIpO1xuICAgIHZhciBzcGFuID0gJCh0aGlzKS5jaGlsZHJlbigpO1xuICAgc3Bhbi5jbG9uZSgpLmFwcGVuZFRvKHRoaXMpXG5cblxuICB9KTtcbiAgIC8vIC5jaGlsZHJlbignc3BhbicpLmxldHRlcmluZygpO1xuXG5cbiAgdmFyICR0dF9oID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi53cGNmNy1mb3JtLWNvbnRyb2wtd3JhcFwiKTtcbiAgICQuZWFjaCgkdHRfaCwgZnVuY3Rpb24oaW5kZXgsIHJldmVhbF9pdGVtKSB7XG5cbiAgICB2YXIgZ2V0TGFiZWwgPSAkKHRoaXMpLm5leHQoXCJsYWJlbFwiKS5kZXRhY2goKTtcbiAgICAgICAgICAkKHRoaXMpLmFwcGVuZChnZXRMYWJlbCk7XG5cbiAgICAgICAgfSk7XG5cbiAgJChcIi5pbnB1dCBpbnB1dFwiKS5mb2N1c291dChmdW5jdGlvbigpIHtcbiAgICAgaWYgKCQodGhpcykudmFsKCkgIT0gXCJcIikge1xuICAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJub3QtZW1wdHlcIik7XG4gICAgIH0gZWxzZSB7XG4gICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcyhcIm5vdC1lbXB0eVwiKTtcbiAgICAgfVxuICAgfSlcbn1cblxuZnVuY3Rpb24gc2VydmljZVRoaW5ncygpIHtcblxuICB2YXIgZT1kb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInN2NFwiKTtcbiAgaWYoZSl7XG5cbiAgdmFyIGN0cmwgPSBuZXcgU2Nyb2xsTWFnaWMuQ29udHJvbGxlcigpO1xuICB2YXIgdHdlZW4gPSBuZXcgVHdlZW5NYXguZnJvbVRvKGUsMSx7ZWFzZTpTaW5lLmVhc2VPdXQsb3BhY2l0eTowfSx7ZWFzZTpTaW5lLmVhc2VPdXQsb3BhY2l0eToxLGRlbGF5Oi42fSlcblxuICAgICAgLy8gaWYgKCFpc01vYmlsZSkge1xuICAgICAgbmV3IFNjcm9sbE1hZ2ljLlNjZW5lKHtcbiAgICAgICAgICAgICAgdHJpZ2dlckVsZW1lbnQ6IGUsXG4gICAgICAgICAgICAgIHRyaWdnZXJIb29rOiAnb25FbnRlcicsXG4gICAgICAgICAgICAgIHJldmVyc2U6IGZhbHNlLFxuICAgICAgICAgICAgICBkdXJhdGlvbjogMTIwLFxuICAgICAgICAgICAgIC8vIG9mZnNldDogMlxuICAgICAgICAgIH0pXG4gICAgICAgICAgLnNldFR3ZWVuKHR3ZWVuKVxuICAgICAgICAgIC5hZGRUbyhjdHJsKTtcbiAgICAgIC8vIH1cblxuXG5cblxuICBcbiAgICB2YXIgbj1lLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcCxcbiAgICB0PWZ1bmN0aW9uKCl7cmV0dXJuIGUucGFyZW50RWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5oZWlnaHQ+d2luZG93LmlubmVySGVpZ2h0JiZ3aW5kb3cuaW5uZXJXaWR0aD45OTJ9O1xuXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLGZ1bmN0aW9uKCl7XG4gICAgdmFyIGk9ZS5wYXJlbnRFbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLmhlaWdodCtuLXdpbmRvdy5pbm5lckhlaWdodCxcbiAgICBkPXdpbmRvdy5wYWdlWU9mZnNldHx8ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcDtcbiAgICB0KCk/ZD49aT8oZS5jbGFzc0xpc3QuYWRkKFwicGlubmVkXCIpLGUuY2xhc3NMaXN0LnJlbW92ZShcImZpeGVkXCIpKVxuICAgIDpkPj1uPyhlLmNsYXNzTGlzdC5yZW1vdmUoXCJwaW5uZWRcIiksZS5jbGFzc0xpc3QuYWRkKFwiZml4ZWRcIikpXG4gICAgOihlLmNsYXNzTGlzdC5yZW1vdmUoXCJwaW5uZWRcIiksZS5jbGFzc0xpc3QucmVtb3ZlKFwiZml4ZWRcIikpOmUuY2xhc3NMaXN0LmFkZChcIm5vLWZpeGVkXCIpXG5cbiAgfSlcbiAgfVxufVxuXG5cblxuXG5cbmZ1bmN0aW9uIGluaXQoKSB7XG4gICAgaW5wdXRFZmZlY3RzKCk7XG4gICAgcG9ydGZvbGlvU2V0dGluZ3MoKTtcbiAgICBpbWdMb2FkKCk7XG4gICAgc2VydmljZVRoaW5ncygpO1xuXG5cbn1cblxuXG5cblxuXG5cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG5cbiAgICBcInVzZSBzdHJpY3RcIjtcbiAgICBpbml0KCk7XG59KTtcbiIsIi8qKlxuICogZGVtby5qc1xuICogaHR0cDovL3d3dy5jb2Ryb3BzLmNvbVxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZS5cbiAqIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXG4gKiBcbiAqIENvcHlyaWdodCAyMDE4LCBDb2Ryb3BzXG4gKiBodHRwOi8vd3d3LmNvZHJvcHMuY29tXG4gKi9cbntcbiAgICAvLyBDbGFzcyBNZW51LlxuICAgIGNsYXNzIE1lbnUge1xuICAgICAgICBjb25zdHJ1Y3RvcihlbCkge1xuICAgICAgICAgICAgdGhpcy5ET00gPSB7ZWw6IGVsfTtcbiAgICAgICAgICAgIC8vIE9wZW4gYW5kIGNsb3NlIGN0bHMuXG4gICAgICAgICAgICB0aGlzLkRPTS5vcGVuQ3RybCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5hY3Rpb24tLW1lbnUnKTtcbiAgICAgICAgICAgIHRoaXMuRE9NLmNsb3NlQ3RybCA9IHRoaXMuRE9NLmVsLnF1ZXJ5U2VsZWN0b3IoJy5hY3Rpb24tLWNsb3NlJyk7XG4gICAgICAgICAgICB0aGlzLkRPTS5xdW90ZUN0cmwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYWN0aW9uLS1xdW90ZScpO1xuICAgICAgICAgICAgXG5cbiAgICAgICAgICAgIHRoaXMuRE9NLm9wZW5DdHJsLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4gdGhpcy5vcGVuKCkpO1xuICAgICAgICAgICAgXG5cbiAgICAgICAgICAgIGlmKHRoaXMuRE9NLnF1b3RlQ3RybCkge3RoaXMuRE9NLnF1b3RlQ3RybC5vbmNsaWNrID0gZnVuY3Rpb24gKCkgeyB0aGlzLm9wZW4oKX0gfVxuXG5cbiAgICAgICAgICAgLy8gaWYoJCgpKXt0aGlzLkRPTS5xdW90ZUN0cmwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB0aGlzLm9wZW4oKSk7fVxuXG5cbiAgICAgICAgICAgIHRoaXMuRE9NLmNsb3NlQ3RybC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHRoaXMuY2xvc2UoKSk7XG5cbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tZW51X19pdGVtLS0yJykuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB0aGlzLmNsb3NlKCkpO1xuIFxuICAgICAgICAgICAgdGhpcy5ET00ubWFpbkN0cmwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCduYXYubWVudScpO1xuXG5cbiAgICAgICAgICAgIC8vIFRoZSBtZW51IGl0ZW1zLlxuICAgICAgICAgICAgdGhpcy5ET00uaXRlbXMgPSBBcnJheS5mcm9tKHRoaXMuRE9NLmVsLnF1ZXJ5U2VsZWN0b3JBbGwoJy5tZW51X19pdGVtJykpO1xuICAgICAgICAgICAgLy8gVGhlIHRvdGFsIG51bWJlciBvZiBpdGVtcy5cbiAgICAgICAgICAgIHRoaXMuaXRlbXNUb3RhbCA9IHRoaXMuRE9NLml0ZW1zLmxlbmd0aDtcblxuICAgICAgICAgICAgLy8gQ3VzdG9tIGVsZW1lbnRzIHRoYXQgd2lsbCBiZSBhbmltYXRlZC5cbiAgICAgICAgICAgIHRoaXMuRE9NLm1haW5MaW5rcyA9IHRoaXMuRE9NLmVsLnF1ZXJ5U2VsZWN0b3JBbGwoJy5tYWlubWVudSA+IGEubWFpbm1lbnVfX2l0ZW0nKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBPcGVuIHRoZSBtZW51LlxuICAgICAgICBvcGVuKCkge1xuICAgICAgICAgICAgdGhpcy50b2dnbGUoJ29wZW4nKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBDbG9zZSB0aGUgbWVudS5cbiAgICAgICAgY2xvc2UoKSB7XG4gICAgICAgICAgICB0aGlzLnRvZ2dsZSgnY2xvc2UnKTtcbiAgICAgICAgfVxuICAgICAgICB0b2dnbGUoYWN0aW9uKSB7XG4gICAgICAgICAgICBpZiAoIHRoaXMuaXNBbmltYXRpbmcgKSByZXR1cm47XG4gICAgICAgICAgICAvLyAoZGlzKWFsbG93IHRoZSBtYWluIGltYWdlIHRpbHQgZWZmZWN0LlxuICAgICAgICAgICAgdGhpcy5pc0FuaW1hdGluZyA9IHRydWU7XG4gICAgICAgICAgICAvLyBUb2dnbGluZyB0aGUgb3BlbiBzdGF0ZSBjbGFzcy5cbiAgICAgICAgICAgIHRoaXMuRE9NLmVsLmNsYXNzTGlzdFthY3Rpb24gPT09ICdvcGVuJyA/ICdhZGQnIDogJ3JlbW92ZSddKCdtZW51LS1vcGVuJyk7XG5cbiAgICAgICAgICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0W2FjdGlvbiA9PT0gJ29wZW4nID8gJ2FkZCcgOiAncmVtb3ZlJ10oJ21lbnUtLWFjdGl2ZScpO1xuICAgICAgICAgICAgLy8gQWZ0ZXIgYWxsIGlzIGFuaW1hdGVkLi5cbiAgICAgICAgICAgIGNvbnN0IGFuaW1hdGlvbkVuZCA9IChwb3MpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoIHBvcyA9PT0gdGhpcy5pdGVtc1RvdGFsLTEgKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBbmltYXRpbmcgPSBmYWxzZTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAoIGFjdGlvbiA9PT0gJ2Nsb3NlJyApIHtcblxuICAgICAgICAgICAgICAgICAgICBUd2Vlbk1heC50byh0aGlzLkRPTS5tYWluQ3RybCwgMC42LCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlYXNlOiBRdWFydC5lYXNlSW5PdXQsXG4gICAgICAgICAgICAgICAgICAgICAgICB5OiAnLTEwMSUnLFxuICAgICAgICAgICAgICAgICAgICAgICAvLyBvbkNvbXBsZXRlOiAoKSA9PiBhbmltYXRpb25TdGFydCgpXG5cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICB9XG5cblxuXG4gICAgICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfTtcblxuXG5cbiAgICAgICAgICAgIGlmICggYWN0aW9uID09PSAnb3BlbicgKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgY29uZmlnZWQgPSB7fTtcblxuICAgICAgICAgICAgICAgIGNvbmZpZ2VkLnkgPSAnLTEwMSUnO1xuICAgICAgICAgICAgICAgIGNvbmZpZ2VkLnggPSAnMCUnO1xuICAgICAgICAgICAgICAgIC8vIFNldHRpbmcgdGhlIGluaXRpYWwgdmFsdWVzLlxuICAgICAgICAgICAgICAgIFR3ZWVuTWF4LnNldCh0aGlzLkRPTS5tYWluQ3RybCwgY29uZmlnZWQpO1xuXG4gICAgICAgICAgICAgICAgLy8gQW5pbWF0ZS5cbiAgICAgICAgICAgICAgICBUd2Vlbk1heC50byh0aGlzLkRPTS5tYWluQ3RybCwgLjksIHtcbiAgICAgICAgICAgICAgICAgICAgZWFzZTogUXVpbnQuZWFzZU91dCxcbiAgICAgICAgICAgICAgICAgICAgeDogJzAlJyxcbiAgICAgICAgICAgICAgICAgICAgeTogJzAlJyxcbiAgICAgICAgICAgICAgICAgICAgb25Db21wbGV0ZTogKCkgPT4gYW5pbWF0aW9uU3RhcnQoKVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG5cblxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgVHdlZW5NYXgudG8odGhpcy5ET00ubWFpbkN0cmwsIDAuNiwge1xuICAgICAgICAgICAgICAgICAgICBlYXNlOiBRdWFydC5lYXNlSW5PdXQsXG4gICAgICAgICAgICAgICAgICAgLy8geDogY29uZmlnLnggfHwgMCxcbiAgICAgICAgICAgICAgICAgICAvLyB5OiAnLTEwMSUnLFxuICAgICAgICAgICAgICAgICAgICBvbkNvbXBsZXRlOiAoKSA9PiBhbmltYXRpb25TdGFydCgpXG5cbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgfVxuXG5cblxuXG5cbiAgICAgICAgICAgIGNvbnN0IGFuaW1hdGlvblN0YXJ0ID0gKCkgPT4ge1xuXG4gICAgICAgICAgICAvLyBHb2luZyB0aHJvdWdoIGVhY2ggbWVudcK0cyBpdGVtLlxuICAgICAgICAgICAgdGhpcy5ET00uaXRlbXMuZm9yRWFjaCgoZWwsIHBvcykgPT4ge1xuICAgICAgICAgICAgICAgIC8vIFRoZSBpbm5lciB3cmFwcGVyLlxuICAgICAgICAgICAgICAgIGNvbnN0IGlubmVyRWwgPSBlbC5xdWVyeVNlbGVjdG9yKCcubWVudV9faXRlbS1pbm5lcicpO1xuICAgICAgICAgICAgICAgIC8vIGNvbmZpZyBhbmQgaW5uZXIgY29uZmlnIHdpbGwgaGF2ZSB0aGUgc3RhcnRpbmcgdHJhbnNmb3JtIHZhbHVlcyAod2hlbiBvcGVuaW5nKSBhbmQgdGhlIGVuZCBvbmVzICh3aGVuIGNsb3NpbmcpIGZvciBib3RoIHRoZSBpdGVtIGFuZCBpdHMgaW5uZXIgZWxlbWVudC5cbiAgICAgICAgICAgICAgICBjb25zdCBjb25maWcgPSB7fTtcbiAgICAgICAgICAgICAgICBjb25zdCBjb25maWdJbm5lciA9IHt9O1xuICAgICAgICAgICAgICAgIC8vIERpcmVjdGlvbiBkZWZpbmVkIGluIHRoZSBIVE1MIGRhdGEtZGlyZWN0aW9uLlxuICAgICAgICAgICAgICAgIC8vIGJ0IChib3R0b20gdG8gdG9wKSB8fCB0YiAodG9wIHRvIGJvdHRvbSkgfHwgbHIgKGxlZnQgdG8gcmlnaHQpIHx8IHJsIChyaWdodCB0byBsZWZ0KVxuICAgICAgICAgICAgICAgIGNvbnN0IGRpcmVjdGlvbiA9IGVsLmRhdGFzZXQuZGlyZWN0aW9uO1xuICAgICAgICAgICAgICAgIC8vIFVzaW5nIDEwMSUgaW5zdGVhZCBvZiAxMDAlIHRvIGF2b2lkIHJlbmRlcmluZyBwcm9ibGVtcy5cbiAgICAgICAgICAgICAgICAvLyBJbiBvcmRlciB0byBjcmVhdGUgdGhlIFwicmV2ZWFsXCIgZWZmZWN0LCB0aGUgaXRlbSBzbGlkZXMgbW92ZXMgaW4gb25lIGRpcmVjdGlvbiBhbmQgaXRzIGlubmVyIGVsZW1lbnQgaW4gdGhlIG9wcG9zaXRlIGRpcmVjdGlvbi5cbiAgICAgICAgICAgICAgICBpZiAoIGRpcmVjdGlvbiA9PT0gJ2J0JyApIHtcbiAgICAgICAgICAgICAgICAgICAgY29uZmlnLnkgPSAnMTAxJSc7XG4gICAgICAgICAgICAgICAgICAgIGNvbmZpZ0lubmVyLnkgPSAnLTEwMSUnO1xuICAgICAgICAgICAgICAgICAgICBjb25maWdJbm5lci54ID0gJzAlJztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSBpZiAoIGRpcmVjdGlvbiA9PT0gJ3RiJyApIHtcbiAgICAgICAgICAgICAgICAgICAgY29uZmlnLnkgPSAnLTEwMSUnO1xuICAgICAgICAgICAgICAgICAgICBjb25maWdJbm5lci55ID0gJzEwMSUnO1xuICAgICAgICAgICAgICAgICAgICBjb25maWdJbm5lci54ID0gJzAlJztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSBpZiAoIGRpcmVjdGlvbiA9PT0gJ2xyJyApIHtcbiAgICAgICAgICAgICAgICAgICAgY29uZmlnLnggPSAnLTEwMSUnO1xuICAgICAgICAgICAgICAgICAgICBjb25maWdJbm5lci54ID0gJzEwMSUnO1xuICAgICAgICAgICAgICAgICAgICBjb25maWcueSA9ICctMTAxJSc7XG5cblxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmICggZGlyZWN0aW9uID09PSAncmwnICkge1xuICAgICAgICAgICAgICAgICAgICBjb25maWcueCA9ICcxMDElJztcbiAgICAgICAgICAgICAgICAgICAgY29uZmlnSW5uZXIueCA9ICctMTAxJSc7XG4gICAgICAgICAgICAgICAgICAgIGNvbmZpZy55ID0gJy0xMDElJztcblxuXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjb25maWcueCA9ICcxMDElJztcbiAgICAgICAgICAgICAgICAgICAgY29uZmlnLnkgPSAnMTAxJSc7XG4gICAgICAgICAgICAgICAgICAgIGNvbmZpZ0lubmVyLnggPSAnLTEwMSUnO1xuICAgICAgICAgICAgICAgICAgICBjb25maWdJbm5lci55ID0gJy0xMDElJztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgaWYgKCBhY3Rpb24gPT09ICdvcGVuJyApIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gU2V0dGluZyB0aGUgaW5pdGlhbCB2YWx1ZXMuXG4gICAgICAgICAgICAgICAgICAgIFR3ZWVuTWF4LnNldChlbCwgY29uZmlnKTtcbiAgICAgICAgICAgICAgICAgICAgVHdlZW5NYXguc2V0KGlubmVyRWwsIGNvbmZpZ0lubmVyKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBBbmltYXRlLlxuICAgICAgICAgICAgICAgICAgICBUd2Vlbk1heC50byhbZWwsaW5uZXJFbF0sIC45LCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlYXNlOiBRdWludC5lYXNlT3V0LFxuICAgICAgICAgICAgICAgICAgICAgICAgeDogJzAlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHk6ICcwJScsXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNvbXBsZXRlOiAoKSA9PiBhbmltYXRpb25FbmQocG9zKVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIFR3ZWVuTWF4LnRvKGVsLCAwLjYsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVhc2U6IFF1YXJ0LmVhc2VJbk91dCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHg6IGNvbmZpZy54IHx8IDAsXG4gICAgICAgICAgICAgICAgICAgICAgICB5OiBjb25maWcueSB8fCAwXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBUd2Vlbk1heC50byhpbm5lckVsLCAwLjYsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVhc2U6IFF1YXJ0LmVhc2VJbk91dCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHg6IGNvbmZpZ0lubmVyLnggfHwgMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHk6IGNvbmZpZ0lubmVyLnkgfHwgMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ29tcGxldGU6ICgpID0+IGFuaW1hdGlvbkVuZChwb3MpXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuXG5cbiAgICAgICAgICAgIH07XG5cblxuICAgICAgICAgICAgLy8gU2hvdy9IaWRlIG9wZW4gYW5kIGNsb3NlIGN0cmxzLlxuICAgICAgICAgICAgVHdlZW5NYXgudG8odGhpcy5ET00uY2xvc2VDdHJsLCAwLjYsIHtcbiAgICAgICAgICAgICAgICBlYXNlOiBhY3Rpb24gPT09ICdvcGVuJyA/IFF1aW50LmVhc2VPdXQgOiBRdWFydC5lYXNlSW5PdXQsXG4gICAgICAgICAgICAgICAgc3RhcnRBdDogYWN0aW9uID09PSAnb3BlbicgPyB7cm90YXRpb246IDB9IDogbnVsbCxcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiBhY3Rpb24gPT09ICdvcGVuJyA/IDEgOiAwLFxuICAgICAgICAgICAgICAgIHJvdGF0aW9uOiBhY3Rpb24gPT09ICdvcGVuJyA/IDE4MCA6IDI3MCxcbiAgICAgICAgICAgICAgICBkZWxheTogYWN0aW9uID09PSAnb3BlbicgPyAxLjUgOiAwLjMsXG5cbiAgICAgICAgICAgIH0pO1xuXG5cbiAgICAgICAgIFxuICAgICAgICAgVHdlZW5NYXgudG8odGhpcy5ET00ub3BlbkN0cmwsIGFjdGlvbiA9PT0gJ29wZW4nID8gMC42IDogMC4zLCB7XG4gICAgICAgICAgICAgICAgICAgICAgLy9kZWxheTogYWN0aW9uID09PSAnb3BlbicgPyAwIDogMC4zLFxuICAgICAgICAgICAgICAgICAgICAgIGVhc2U6IGFjdGlvbiA9PT0gJ29wZW4nID8gUXVpbnQuZWFzZU91dCA6IFF1YWQuZWFzZU91dCxcbiAgICAgICAgICAgICAgICAgICAgIC8vIG9wYWNpdHk6IGFjdGlvbiA9PT0gJ29wZW4nID8gMCA6IDFcbiAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAvLyBNYWluIGxpbmtzIGFuaW1hdGlvbi5cbiAgICAgICAgICAgIFR3ZWVuTWF4LnN0YWdnZXJUbyh0aGlzLkRPTS5tYWluTGlua3MsIGFjdGlvbiA9PT0gJ29wZW4nID8gMC45IDogMC4yLCB7XG4gICAgICAgICAgICAgICAgZWFzZTogYWN0aW9uID09PSAnb3BlbicgPyBRdWludC5lYXNlT3V0IDogUXVhcnQuZWFzZUluT3V0LFxuICAgICAgICAgICAgICAgIHN0YXJ0QXQ6IGFjdGlvbiA9PT0gJ29wZW4nID8ge3k6ICc1MCUnLCBvcGFjaXR5OiAwfSA6IG51bGwsXG4gICAgICAgICAgICAgICAgeTogYWN0aW9uID09PSAnb3BlbicgPyAnMCUnIDogJzUwJScsXG4gICAgICAgICAgICAgICAgb3BhY2l0eTogYWN0aW9uID09PSAnb3BlbicgPyAxIDogMFxuICAgICAgICAgICAgfSwgYWN0aW9uID09PSAnb3BlbicgPyAwLjEgOiAtMC4xKTtcblxuXG5cbiAgICAgICAgfVxuICAgIH1cblx0Ly8gSW5pdGlhbGl6ZSB0aGUgTWVudS5cbiAgICBjb25zdCBtZW51ID0gbmV3IE1lbnUoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignbmF2Lm1lbnUnKSk7XG4gICAgbGV0IGFsbG93VGlsdCA9IHRydWU7XG5cblxufSIsIi8qKlxuICogRmlsZSBza2lwLWxpbmstZm9jdXMtZml4LmpzLlxuICpcbiAqIEhlbHBzIHdpdGggYWNjZXNzaWJpbGl0eSBmb3Iga2V5Ym9hcmQgb25seSB1c2Vycy5cbiAqXG4gKiBMZWFybiBtb3JlOiBodHRwczovL2dpdC5pby92V2RyMlxuICovXG4oIGZ1bmN0aW9uKCkge1xuXHR2YXIgaXNJZSA9IC8odHJpZGVudHxtc2llKS9pLnRlc3QoIG5hdmlnYXRvci51c2VyQWdlbnQgKTtcblxuXHRpZiAoIGlzSWUgJiYgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQgJiYgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIgKSB7XG5cdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoICdoYXNoY2hhbmdlJywgZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgaWQgPSBsb2NhdGlvbi5oYXNoLnN1YnN0cmluZyggMSApLFxuXHRcdFx0XHRlbGVtZW50O1xuXG5cdFx0XHRpZiAoICEgKCAvXltBLXowLTlfLV0rJC8udGVzdCggaWQgKSApICkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblx0XHRcdGVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggaWQgKTtcblxuXHRcdFx0aWYgKCBlbGVtZW50ICkge1xuXHRcdFx0XHRpZiAoICEgKCAvXig/OmF8c2VsZWN0fGlucHV0fGJ1dHRvbnx0ZXh0YXJlYSkkL2kudGVzdCggZWxlbWVudC50YWdOYW1lICkgKSApIHtcblx0XHRcdFx0XHRlbGVtZW50LnRhYkluZGV4ID0gLTE7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRlbGVtZW50LmZvY3VzKCk7XG5cdFx0XHR9XG5cdFx0fSwgZmFsc2UgKTtcblx0fVxufSApKCk7XG4iXX0=