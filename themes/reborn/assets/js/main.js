/**
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */
 document.documentElement.className="js";
 TweenLite.defaultOverwrite = false;
 var WebFontConfig = {
   typekit: {
     id: 'cim0eju'
   }
 };
 WebFont.load( WebFontConfig );


 var lazyLoadInstance = new LazyLoad({
     elements_selector: ".lazyload"
     // ... more custom settings?
 });
    


( function( $ ) {



	var headline = $("h1");
	const char = '[class*="char"]';
	var button = $('.btn-quote');
	var $loader = document.querySelector('.loader'),
	 mainContent = $('.site-content'),
	 isAnimating = false;
	 const $span = $('.js-span');



	// Using lettering.js to wrap a <span> around each word and a <span> around each character.
	headline
	  .lettering('words')
	  .children('span').lettering();



	//EL's
	const $bg = $('.js-bg');
	const $heading = $('.js-heading');
	const preloader = $('#preloader');

	//TIMELINE
	const plTl = new TimelineMax({
	  paused: !0,
	  //delay: 3,
	  onComplete: () => {
	    button.addClass('is-active');
	    //$("#preloader").remove()
	  }
	});


	if($('.page-header-inner').length > 0) {


	plTl.add('start')

	.to(preloader, 1.2, {
	                        y: "-100%",
	                        ease: Circ.easeOut//CustomEase.create("custom", "M0,0 C0.86,0 0.07,1 1,1")
	                    }, "-=0")
	

	.fromTo($bg, 0.4, {
	  autoAlpha: 0,
	}, {
	  autoAlpha: 1,
	  ease: Power3.easeOut
	}, 'start')

	.fromTo($bg, 1.8, {
	  scale: 2,
	  x: '-70%'
	}, {
	  scale: 1,
	  x: '0%',
	  ease: Power3.easeOut
	}, 'start')



	// .fromTo($heading, 0.4, {
	//   autoAlpha: 0,
	// }, {
	//   autoAlpha: 1,
	//   ease: Power3.easeOut
	// }, 'start')


	.staggerFrom($span, 0.4, {
		  opacity: 0,
		  ease: Power3.easeOut,
		  y: '100%'
		}, '0.05')

	.staggerFrom(char, 0.5, {y: 80, opacity: 0, ease: Elastic.easeOut}, 0.01);



	// .staggerFrom(char, 1, {
	// 	  opacity: 0,
	// 	  ease: Elastic.easeOut,
	// 	  y: '100%'
	// 	}, '0.05');


	


	$('.wrapper').imagesLoaded( { background: '.page-header-inner' }, function() {
	  document.body.classList.remove('loading');
		plTl.play()

	});

	}

	else {
		new TweenMax.to(preloader, 1.2, {
		                        y: "-100%",
		                        ease: Circ.easeOut//CustomEase.create("custom", "M0,0 C0.86,0 0.07,1 1,1")
		                    }, "-=0")
	}



jQuery(document).ready(function($) {



	$('.navbar').on('click', '.nav-link', function(e) {
		e.preventDefault();


			var path = $(this).attr('href');
			var location = window.location.pathname.toString();
			var pathName = $(this).text().toString().toLowerCase();


					// else check if we are inside a major section (i.e. blog)
					 if (location.indexOf(pathName) !== -1) {
						console.log('somewhere inside ', pathName, path, location);
						window.location.reload(true);
						return false;

					}

	  $loader.classList.add('loader--active')


	  var $this = $(this);
	  $this.parent().siblings().removeClass('active').end().addClass('active');


	  var pageRef = $(this).attr('href');
	  var targetTitle = $(this).attr('title');
	  var pageSlug = $(this).attr('data-menu');

	callPage(pageRef, pageSlug, targetTitle);

	});



window.onpopstate = function(e) {
     //$(".nav-link").fadeTo('fast', 1.0);
     callPage(e.state ? e.state.url : null);
 };






function callPage(pageRefInput, pageSlug, targetTitleInput){
	window.history.pushState({url: "" + pageRefInput + ""}, targetTitleInput, pageRefInput);

	var $this = $(this);
	$this.siblings().removeClass('active').end().addClass('active');
	 var newSection = pageSlug;

	 var primero = $('<div></div>');

	 primero.load(newSection, function(e){
	 var t = document.createElement("div");
	 var currentHTML = e;
	                     t.innerHTML = e;
	                     var i = t.querySelector("title");
	                     document.title = i.textContent

	                 });

	

	//create a new section element and insert it into the DOM
	var section = $('<main class="cd-main overflow-hidden '+newSection+'"></main>').appendTo(mainContent);
	//load the new content from the proper html file
	section.load(newSection+' .cd-main > *', function(event){

		section.prev('.visible').removeClass('visible').end().addClass('visible');
		resetAfterAnimation(section);


	});


  }

 

  function resetAfterAnimation(newSection) {
    $('html, body').animate({ scrollTop: 0 }, 800);


  		//once the new section animation is over, remove the old section and make the new one scrollable
  		newSection.removeClass('overflow-hidden').prev('.cd-main').remove();
  		isAnimating =  false;

      init();
  		//reset your loading bar

  		//EL's
  		const $bg = $('.js-bg');
  		const $heading = $('.js-heading');
  		const $char = '[class*="word"]';
  		const headline = $("h1");
  		const $span = $('.js-span');


  		headline
  		  .lettering('words')


  		//TIMELINE
  		const plTl = new TimelineMax({
  		  //paused: true,
  		  //delay: 3,
  		  onComplete: () => {
  		    $('#preloader').remove();
  		  }
  		});




  		if($('.page-header-inner').length > 0) {


  		plTl.add('start')

  		.to(preloader, 1.2, {
			y: "-100%",
			ease: Circ.easeOut//CustomEase.create("custom", "M0,0 C0.86,0 0.07,1 1,1")
  		                    }, "-=0")
  		

  		.fromTo($bg, 0.4, {
  		  autoAlpha: 0,
  		}, {
  		  autoAlpha: 1,
  		  ease: Power3.easeOut
  		}, 'start')

  		.fromTo($bg, 1.8, {
  		  scale: 2,
  		  x: '-70%'
  		}, {
  		  scale: 1,
  		  x: '0%',
  		  ease: Power3.easeOut
  		}, 'start')

  		


  		.fromTo($heading, 0.4, {
  		  opacity: 0,
  		}, {
  		  opacity: 1,
  		  ease: Elastic.easeOut
  		}, 'start')


  		.staggerFrom($span, 0.4, {
  			  opacity: 0,
  			  ease: Elastic.easeOut,
  			  y: '100%'
  			}, '0.05')
  		.staggerFrom($char, 1, {
  			  opacity: 0,
  			  ease: Elastic.easeOut,
  			  y: '100%'
  			}, '0.05')

  		;




  		imagesLoaded( document.querySelector('.page-header-inner'), { background: true }, function( instance ) {
  		    $loader.classList.remove('loader--active')
  		  	plTl.play()
  		});

  	}

  	else 
  	{

  		    $loader.classList.remove('loader--active')

  	}


  	}




  		});






} )( jQuery );




//Img load
function imgLoad() {
    $('.portfolio-item').each(function() {
        var image = $(this).find('a').data('src');
        $(this).find('.portfolio-img-content').css({ 'background-image': 'url(' + image + ')', 'background-size': 'cover', 'background-position': 'center' });
    });
}


//Parallax Effect
function parallaxbg() {
    if ($('.parallax-inner').length > 0) {
        $('.parallax-background').parallaxBackground({
            event: 'mouse_move',
            animation_type: 'shift',
            animate_duration: 3,
        });

    }
}


function portfolioSettings() {
  var grid = document.querySelector('.grid');

  if(grid) {


  var $grid = $('.grid').isotope({
    itemSelector: '.grid__item',
 //   percentPosition: true,
     filter: '*',
      visibleStyle: { transform: 'translateY(0)', opacity: 1 },
      hiddenStyle: { transform: 'translateY(100px)', opacity: 0 },

   masonry: {
       columnWidth: '.grid__item'
   },


     });



  $grid.on( 'layoutComplete', function() {
    $('.loader-5').hide();
    $('.segmented-control').css('opacity', '1');

  });

  $grid.imagesLoaded().progress( function() {
    grid.classList.remove('are-images-unloaded');
    $grid.isotope('layout');
  });


  var $filterLinks = $('.segmented-control > li');


   $filterLinks.click(function(){
      var $this = $(this);

     $filterLinks.removeClass('active');
      $(this).addClass('active');

      var selector = $this.data('filter');
       $grid.isotope({
        filter: selector
      });

    });


  }
}


function inputEffects(){

  $('.btn-outline').lettering('letters');

  $.each($('.btn-outline'), function(index, reveal_item) {

    $(this).children().wrapAll("<span></span>");
    var span = $(this).children();
   span.clone().appendTo(this)


  });
   // .children('span').lettering();


  var $tt_h = document.querySelectorAll(".wpcf7-form-control-wrap");
   $.each($tt_h, function(index, reveal_item) {

    var getLabel = $(this).next("label").detach();
          $(this).append(getLabel);

        });

  $(".input input").focusout(function() {
     if ($(this).val() != "") {
       $(this).addClass("not-empty");
     } else {
       $(this).removeClass("not-empty");
     }
   })
}

function serviceThings() {

  var e=document.getElementById("sv4");
  if(e){

  var ctrl = new ScrollMagic.Controller();
  var tween = new TweenMax.fromTo(e,1,{ease:Sine.easeOut,opacity:0},{ease:Sine.easeOut,opacity:1,delay:.6})

      // if (!isMobile) {
      new ScrollMagic.Scene({
              triggerElement: e,
              triggerHook: 'onEnter',
              reverse: false,
              duration: 120,
             // offset: 2
          })
          .setTween(tween)
          .addTo(ctrl);
      // }




  
    var n=e.getBoundingClientRect().top,
    t=function(){return e.parentElement.getBoundingClientRect().height>window.innerHeight&&window.innerWidth>992};

    document.addEventListener("scroll",function(){
    var i=e.parentElement.getBoundingClientRect().height+n-window.innerHeight,
    d=window.pageYOffset||document.documentElement.scrollTop;
    t()?d>=i?(e.classList.add("pinned"),e.classList.remove("fixed"))
    :d>=n?(e.classList.remove("pinned"),e.classList.add("fixed"))
    :(e.classList.remove("pinned"),e.classList.remove("fixed")):e.classList.add("no-fixed")

  })
  }
}





function init() {
    inputEffects();
    portfolioSettings();
    imgLoad();
    serviceThings();


}







$(document).ready(function() {

    "use strict";
    init();
});
