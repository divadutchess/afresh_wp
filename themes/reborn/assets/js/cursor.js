
/**
 * demo.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2019, Codrops
 * http://www.codrops.com
 */

class Demo3 {
  constructor() {

    this.initCursor();
    this.initHovers();
  }

  initCursor() {
    const { Back } = window;
    this.outerCursor = document.querySelector(".circle-cursor--outer");
    this.innerCursor = document.querySelector(".circle-cursor--inner");
    this.outerCursorBox = this.outerCursor.getBoundingClientRect();
    this.outerCursorSpeed = 0;
    this.easing = Back.easeOut.config(1.7);
    this.clientX = -100;
    this.clientY = -100;
    this.showCursor = false;

    const unveilCursor = () => {
      TweenMax.set(this.innerCursor, {
        x: this.clientX,
        y: this.clientY
      });
      TweenMax.set(this.outerCursor, {
        x: this.clientX - this.outerCursorBox.width / 2,
        y: this.clientY - this.outerCursorBox.height / 2
      });
      setTimeout(() => {
        this.outerCursorSpeed = 0.2;
      }, 100);
      this.showCursor = true;
    };
    document.addEventListener("mousemove", unveilCursor);

    document.addEventListener("mousemove", e => {
      this.clientX = e.clientX;
      this.clientY = e.clientY;
    });

    const render = () => {
      TweenMax.set(this.innerCursor, {
        x: this.clientX,
        y: this.clientY
      });
      if (!this.isStuck) {
        TweenMax.to(this.outerCursor, this.outerCursorSpeed, {
          x: this.clientX - this.outerCursorBox.width / 2,
          y: this.clientY - this.outerCursorBox.height / 2
        });
      }
      if (this.showCursor) {
        document.removeEventListener("mousemove", unveilCursor);
      }
      requestAnimationFrame(render);
    };
    requestAnimationFrame(render);
  }

  initHovers() {
    const handleMouseEnter = e => {
      this.isStuck = true;
      const target = e.currentTarget;
      const box = target.getBoundingClientRect();
      this.outerCursorOriginals = {
        width: this.outerCursorBox.width,
        height: this.outerCursorBox.height
      };

      TweenMax.set(this.innerCursor, { opacity: 0.7 });

      TweenMax.to(this.outerCursor, 0.2, {
        x: box.left + 40,
        y: box.left - 22,
        width: 40,
        height:40,
        opacity: 1,
      });
    };

    const handleMouseLeave = () => {
      this.isStuck = false;
      TweenMax.set(this.innerCursor, { opacity: 1 });

      TweenMax.to(this.outerCursor, 0.2, {
        width: this.outerCursorOriginals.width,
        height: this.outerCursorOriginals.height,
        opacity: 0.2,
        borderColor: "#ffffff",
        borderWidth: 1,
        borderRadius: '50%',
        backgroundColor: "transparent",

      });
    };

    const linkItems = document.querySelectorAll(".nav-link");
    linkItems.forEach(item => {
      item.addEventListener("mouseenter", handleMouseEnter);
      item.addEventListener("mouseleave", handleMouseLeave);
    });

    const mainNavHoverTween = TweenMax.to(this.outerCursor, 0.3, {
      backgroundColor: "#081831",
      ease: this.easing,
      paused: true,
      width: 50,
      height:50,
      opacity: 0.6,
      borderColor: "#081831"
    });

    const mainNavMouseEnter = () => {
      this.outerCursorSpeed = 0;
      TweenMax.set(this.innerCursor, { backgroundColor: '#081831', opacity: 0 });
      mainNavHoverTween.play();
    };

    const mainNavMouseLeave = () => {
      this.outerCursorSpeed = 0.2;
      TweenMax.set(this.innerCursor, { opacity: 1 });
      mainNavHoverTween.reverse();
    };

    const mainNavLinks = document.querySelectorAll(".ajax-link");
    mainNavLinks.forEach(item => {
      item.addEventListener("mouseenter", mainNavMouseEnter);
      item.addEventListener("mouseleave", mainNavMouseLeave);
    });


   


    const whiteHoverTween = TweenMax.to(this.outerCursor, 0.3, {
      opacity: 1,
        borderColor: "#081831",
      ease: this.easing,
      paused: true,
      backgroundColor: "rgba(0,0,0,0)"

    });


    const handleWhiteEnter = e => {
     this.outerCursorSpeed = 0;
          TweenMax.set(this.innerCursor, { backgroundColor: '#081831',opacity: 1 });
          whiteHoverTween.play();
    };

    const handleWhiteLeave = () => {
      this.outerCursorSpeed = 0.2;
      TweenMax.set(this.innerCursor, {  backgroundColor: '#fff', opacity: 1 });
      whiteHoverTween.reverse();
    };

    const bright = document.querySelectorAll(".bg-bright");
    bright.forEach(item => {
      item.addEventListener("mouseenter", handleWhiteEnter);
      item.addEventListener("mouseleave", handleWhiteLeave);
    });




    const blueHoverTween = TweenMax.to(this.outerCursor, 0.3, {
    //  backgroundColor: "#ffffff",
      ease: this.easing,
      paused: true,
      opacity: 1,
      borderColor: "#fff"
    });

    const blueMouseEnter = () => {
      this.outerCursorSpeed = 0;
      TweenMax.set(this.innerCursor, { backgroundColor: '#fff', opacity: 1 });
      blueHoverTween.play();
    };

    const blueMouseLeave = () => {
      this.outerCursorSpeed = 0.2;
      TweenMax.set(this.innerCursor, {  backgroundColor: '#081831', opacity: 1 });

      blueHoverTween.reverse();
    };

    const blue_bg = document.querySelectorAll(".bg-blue");
    blue_bg.forEach(item => {
      item.addEventListener("mouseenter", blueMouseEnter);
      item.addEventListener("mouseleave", blueMouseLeave);
    });

  }
}

const menu = new Demo3();
