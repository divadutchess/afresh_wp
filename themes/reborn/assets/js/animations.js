jQuery(document).ready(function($) {

  scrollAnim()
});




function scrollAnim() {

    // init ScrollMagic
    var scrollMagic = new ScrollMagic.Controller();

    // Scale Animation Setup
    // .to('@target', @length, {@object})
    // var scale_tween = TweenMax.to('#scale-animation', 1, {
    //   transform: 'scale(.75)',
    //   ease: Linear.easeNone
    // });

    // // BG Animation Setup
    // // .to('@target', @length, {@object})
    // var bg_tween = TweenMax.to('#bg-trigger', 1, {
    //   backgroundColor: '#FFA500',
    //   ease: Linear.easeNone
    // });

    // // YoYo Animation Setup
    // // .to(@target, @length, @object)
    // var yoyo_tween = TweenMax.to('#yoyo-animation', 1, {
    //   transform: 'scale(2)',
    //   ease: Cubic.easeOut,
    //   repeat: -1, // this negative value repeats the animation
    //   yoyo: true // make it bounce…yo!
    // });

    // Reveal
    var reveal = document.querySelectorAll('.reveal-effect');

    $.each(reveal, function(index, reveal_item) {
        // if (!isMobile) {
          new ScrollMagic.Scene({
            triggerElement: reveal_item,
            triggerHook: 'onEnter',
            reverse: false,
            duration: 0,
            offset: 2
          })
          .setClassToggle(reveal_item, 'animate')
          .addTo(scrollMagic);
        // }
      });


    // Unfold
    var unfold = document.querySelectorAll('.unfold');

    $.each(unfold, function(index, reveal_item) {
        // if (!isMobile) {
          new ScrollMagic.Scene({
            triggerElement: reveal_item,
            triggerHook: 'onEnter',
            reverse: false,
            offset: 0,
            duration: 0
          })
          .setClassToggle(reveal_item, 'animate')
          .addTo(scrollMagic);
        // }
      });


    var $bg_switch = document.querySelectorAll('.bg-switch');

    $.each($bg_switch, function(index, reveal_item) {
      var child = $(this).attr("data-bg-color");
  //  var prev = $bg_switch[index].previousSibling();

  var thistween = TweenMax.fromTo($bg_switch[index], 1, {
   backgroundColor:'#fff',
   },{
     backgroundColor:child,
     immediateRender: false
   });
         // make scene
         new ScrollMagic.Scene({
           triggerElement:  this,
           offset: 0,
           duration: '100%',
           triggerHook: 'onEnter',

         })
         .setTween(thistween)
         .addTo(scrollMagic);

       });


    var $heading = document.querySelectorAll('.section__heading');
    $.each($heading, function(i, reveal_item) {


      var splitone = $heading[i].querySelector('h3');

      var splittwo = $heading[i].querySelector('span#section-title');

      var splitthree = $heading[i].querySelector('div');

      var tweenLine = new TimelineMax({
        delay: 0.3
      });

     	//if (!isMobile) {

        var $word = $("path#h-line");
        var $dot = $("path#h-square");

        // prepare SVG
        pathPrepare($word);
        pathPrepare($dot);



        tweenLine.fromTo($word, 0.9,{strokeDashoffset: 0}, { ease:Linear.easeNone, immediateRender: false}, 1);
        tweenLine.fromTo($dot, 0.1, {strokeDashoffset: 0}, {  ease:Linear.easeNone, immediateRender: false}, 1);

        // tweenLine.staggerFrom(splitone, .6, {
        //   y: 100,
        //   opacity: 0,
        //   ease: 'Circ.easeOut'
        // }, 0.2);

        tweenLine.staggerFrom(splittwo, 1.6, {
          y: 50,
          opacity: 0,
          ease: 'Circ.easeOut',

        }, 0.72);

        tweenLine.staggerFrom(splitthree, .6, {
          y: 50,
          opacity: 0,
          ease: 'Elastic.easeOut',

        }, 0.92);



        new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 'onEnter',
          reverse: false,
          offset: 100,
          duration: 200, tweenChanges: true
        })
        .setTween(tweenLine)
        .setClassToggle($heading[i], 'animate')
        .addTo(scrollMagic);

      });





    /** Fade Animation **/

    var all_animations = ['fadeInUp', 'bounceInUp'];
    $.each(all_animations, function(index, animate) {
     var $fadeIn = document.querySelectorAll('.'+all_animations[index]);
     $.each($fadeIn, function(i, item) {

      new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: 'onEnter',
        reverse: false,
        offset: 0,
        duration: 20
      })
      .setClassToggle(item, 'animated')
      .addTo(scrollMagic);

    });
   });


    var $slideRight = document.querySelectorAll('.slideInRight');
    $.each($slideRight, function(i, item) {



     var tweenFade = new TweenMax.fromTo(item, 1, {opacity: 0, y: 50}, {
      opacity: 1,
      y: 0,
      ease: Elastic.easeOut
    });

     new ScrollMagic.Scene({
       triggerElement: this,
       triggerHook: 'onEnter',
       reverse: false,
       offset: 0,
       duration: 15
     })
                 	//.setClassToggle(item, 'animated')

                   .setTween(tweenFade)
                   .addTo(scrollMagic);

                 });




    /** Sliding Text **/

    var $tt_h = document.querySelectorAll('.tt-h');
    var duration = 8000;
    var here = (duration - window.innerWidth);

    $.each($tt_h, function(index, reveal_item) {

     new ScrollMagic.Scene({
       triggerElement:  '.section-portfolio',
       triggerHook: 'onLeave',
       offset: 0,
       duration: 2500,
       reverse: true


     })
     .setTween(TweenMax.fromTo(reveal_item, 1, {right: '-150vw'}, {right: 30, immediateRender: false, ease: Power4.easeInOut}))

     //  .addIndicators(true)
     .addTo(scrollMagic);

   });


    /** SlideLeftIn **/


    var $slideIn = document.querySelectorAll('.slidein');
    $.each($slideIn, function(i, item) {


      new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: 'onEnter',
        reverse: false,
        offset: 0,
        duration: 0,

      })

      .setTween(TweenMax.fromTo(item, 1.2, {x:-100, opacity: 0}, {x:'0', opacity: 1 ,ease:Elastic.easeInOut, immediateRender: true},'-=1.8')) 
      .addTo(scrollMagic);

    });


    /** Scroll Up / Down **/

    var scrollAnimations = ['scrollUp', 'scrollDown'];

    $.each(scrollAnimations, function(i, item) {
      const $effect = document.querySelector('.'+item);

      if($effect) {

      const items = $effect.querySelectorAll('span');


      $.each(items, function(i, animate) {


        const config = {};



        if (item == 'scrollUp'){
          config.yStart = -60;
          config.yEnd = -500;
        }
        else {
          config.yStart = -30;
          config.yEnd = 500;

        }


    


        new ScrollMagic.Scene({
              triggerElement: ('.segmented-control'),
              triggerHook:  'onEnter',
              reverse: true,
              offset: 50,
              duration: '100%',

       })
        .setTween( TweenMax.fromTo(this, 0.5, { x: config.yStart}, {
            opacity: 1,
            x: config.yEnd,
            ease: Power2.easeInOut
          }))
     //  .setPin(this)
      .addIndicators({ name: "Hi (duration: 500)"+ config.offset })
      .addTo(scrollMagic);



      });  

}

    });





  }



  function pathPrepare ($el) {
      var lineLength = $el[0].getTotalLength();
      $el.css("stroke-dasharray", lineLength);
      $el.css("stroke-dashoffset", lineLength);
    }

