<?php
/**
 * Template Name: Front Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package reborn
 */

get_header();
?>

		<main id="main" class="cd-main visible">

		<?php
		while ( have_posts() ) :
			the_post();?>

		<?php
		/**
		 * Template part for displaying page content in page.php
		 *
		 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
		 *
		 * @package reborn
		 */

		?>

		<div id="post-<?php the_ID(); ?>" <?php post_class((is_page(get_the_ID())) ? 'cd-section visible' : 'cd-section'); ?>>
			
			<?php if(get_field('show_page_header') == true):

			$image_url = get_field('afresh_page_header_bg') ? get_field('afresh_page_header_bg'): 'https://source.unsplash.com/random/1400x600'; 
 ?>
			<div class="page-header pl">
				<div class="page-header--content">
					<?php if(get_post_meta(get_the_ID(), 'afresh_page_subtitle', true)): ?>
					<span class="intro-text"><div class="js-span"><?php echo get_post_meta(get_the_ID(), 'afresh_page_subtitle', true) ?></div></span>
					<?php endif; ?>
					<h1 class="pl__heading js-heading"><?php echo get_post_meta(get_the_ID(), 'afresh_page_title', true) ? get_post_meta(get_the_ID(), 'afresh_page_title', true) : get_the_title() ?></h1>
				</div>
				<div class="page-header-inner js-bg pl__bg" style="background-image: url('<?php echo $image_url;?>')"></div>

					<div class="page-header--secondary">
						<div class="bg-primary page-overflow">
						<div class="row fadeInUp">
							<div class="col-md-4">
								<h2 class="text-uppercase">What We Do</h2>
							</div>

							<div class="col-md-6">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam iste, qui unde eligendi magnam repudiandae consequuntur provident perferendis optio labore cumque sit veniam beatae libero rem recusandae nemo tempore quis.</p>
							</div>
						</div>
					</div></div>
					
				
			</div><!-- .entry-header -->

		<?php endif; ?>


		<section class="section-services bg-white bg-bright">

			<div class="container">
				<div class="section__heading">
					<h3 class="pinline">
						<span>Our Services</span>
					</h3>
				</div>
				<div class="row">

				<?php $the_query = new WP_Query( array( 'posts_per_page' => -1, 'post_type' => 'services', 'order_by' => 'ID' ));
				$count = 0;
				 if ( $the_query->have_posts() ) : ?>


					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $count++; ?>
				<div class="col-md-3 service-col">
				<div class="module fadeInUp">


				<?php echo get_field('afresh_service_img')?>
				<div class="module-text">
					<a href="<?php the_permalink(  ); ?>" title="<?php the_title( ); ?>" class="service-title"><?php the_title();?></a>

					<?php the_excerpt()?>
				</div>
				</div>

				</div>


				  	<?php endwhile; ?>
					<!-- end of the loop -->

					<!-- pagination here -->

					<?php wp_reset_postdata(); ?>


				<?php endif; ?>

				</div>
			</div>
			
		</section>
		<section class="section-portfolio bg-switch bg-bright" data-bg-color="#EAF0F8">
			<div class="container">
			<div class="section__heading">
			<h3>Recent Works</h3>
			<span>Inpired by you, always – #afreshcreative</span>
			</div>
			</div>

			<div class="container">
				

				<div id="portfolio-container" class="fadeInUp creative two-col title-tooltip row">

							  	<?php
							//portfolio_category
							global $post;
							$args = array( 'posts_per_page' => -1, 'post_type'=>'portfolio', 'tax_query' => array(
					        array(
					            'taxonomy' => 'portfolio_category',
					            'field' => 'slug',
					            'terms' => 'web-development'
					        ),
					    ) );

							$myposts = get_posts( $args );
							foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
				    
				    <div class="portfolio-item webdesign">
				    	<?php if(get_field('portfolio_img')): ?>

				    		<?php $image_url = get_field('portfolio_img');

				    	 else: ?>

				    		<?php $image_url = get_the_post_thumbnail_url( $post->ID, 'full', array( 'class' => 'portfolio-img' ));


				    		 endif; ?>
				        <a class="ajax-link" href="mark-one.html" data-src="<?php echo $image_url ?>">
				            <div class="portfolio-content">
				                <div class="portfolio-parallax">
				                    <div class="portfolio-img-content reveal-effect parallax-inner">
				                    </div>
				                </div>
				                <div class="portfolio-text-content ">
				                    <div class="portfolio-text">
				                        <h3><span><?php the_title() ?></span></h3>
				                        <span>Branding</span>
				                    </div>
				                </div>
				            </div>
				        </a>
				    </div>

				    <?php endforeach; 
				    wp_reset_postdata();?>
				</div>
			</div>

			
		</section>
		<section class="section-testimonials p-0">

			<div class="container">
			<div class="bg-square slidein">
				<div class="bg-square__inner bg-white">
				<h4>Des soins personnalisés d’excellence dans un environnement haut-de-gamme</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem, quae consectetur corporis. Officia, architecto dolores sit dignissimos, modi accusantium quas sed adipisci atque enim tempora minima, non accusamus beatae eligendi?</p>
			</div>
			</div>
		</div>

			<div class="bg-primary inner">
				<div class="tt-h d-flex">
				<span class="title-xxl tt-w text-stroke">Testimonials</span>
				<span class="title-xxl text-white">Testimonials</span>

			</div>
			</div>

			<div class="bg-background unfold">

				<div class="bg-background__inner">
					<div class="bg-background__bg cover cover--full">

						<?php echo wp_get_attachment_image(get_field('afresh_middle_image'), 'full') ?>

						
					</div>
				</div>
				
			</div>
			
		</section>

		<section class="section-coa bg-bright">
			<div class="container">

			<div class="coa__wrapper unfold">
			<div class="coa__square bg-blue bg-primary slidein">
				<h2><?php echo get_field('afresh_halfway_title') ?></h2>
				<div class="coa-btn">
				<button class="btn btn-outline btn-outline-light action--quote">Request A Quote</button>
			</div>
			</div>
			<div class="coa__inner">
				<figure class="coa-img cover--full">
				<img class="lazyload" <?php echo afresh_attachment_image(get_field('afresh_last_image'), 'blog-image', '600px') ?> alt="Request A Quote">
			</figure>

			</div>

			</div></div>
		</section>
	</div>
		</div><!-- #post-<?php the_ID(); ?> -->


		<?php endwhile; // End of the loop.
		?>

		</main><!-- #main -->

<?php
get_footer();
