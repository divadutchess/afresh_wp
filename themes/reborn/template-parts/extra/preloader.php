<?php 

/* 
* ========================
* PRELOADERS TEMPLATE
* ========================
*     
**/

?>
    <!-- ========== START PRELOADER ========== -->
    <div id="preloader">
        <div class="inner">
            <div class="loading_effect">
                <div class="object" id="object_one"></div>
            </div>
        </div>
    </div>
    <!-- ========== END PRELOADER ========== -->
