<?php
/**
 * Template part for displaying page content in showcase.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package reborn
 */

?>

<section class="section-services bg-white bg-bright pt-100">

	<div class="container w-850">
		<div class="section__heading text-center mb-0">

			<h3 class="pinline">

				<span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5 66.22">
					<path d="M3,60H2V0H3Z" id="h-line"/>
					<path d="M5,66.22H0v-5H5Z" id="h-square"/>

					

				</svg>

				<span id="section-title"><?php echo get_field('afresh_page_title') ?></span>
			</span>
			</h3>

			<div class="w-850"><?php the_content() ?></div>
		</div>


		</div>

		<div class="container w-850">


			<ul class="segmented-control text-right">
			               
			                  <li class="active" data-value="all" data-filter="*">All</li>
			                  <li data-value="Web-esque" data-filter=".web">Web Design</li>
			                  <li data-value="Graphic-esque" data-filter=".graphic">Graphic Design</li>
			              </ul>


			
			<div class="grid are-images-unloaded">


			  <?php

			global $post;
			$count= 0; 
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

			$args = array( 'posts_per_page' => 6, 'post_type'=>'portfolio', 'paged' => $paged);

			$myposts = get_posts( $args );
			foreach ( $myposts as $post ) : setup_postdata( $post ); $count++;  

			$terms = get_the_terms( get_the_ID(), 'portfolio_category' );

			  ?>

			  <div class="grid__item transition <?php echo (has_term('web-design', 'portfolio_category', null) == 1)  ? 'web' : 'graphic' ?>">
			      <a href="<?php echo get_the_permalink( $post->ID ); ?>" title="<?php echo $post->post_title ?>" class="ajax-link reveal unfold">
					<div class="reveal__inner">
			      	<figure class="cover--full showcase__img">

			        <?php if(get_field('portfolio_img')): ?>

			              <img class="lazyload" src="<?php echo get_field('portfolio_img') ?>" alt="<?php the_title() ?>">

			            <?php else: ?>

			           	 <?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'class' => 'portfolio-img lazyload' ) ); ?>

			          <?php endif; ?>

			          </figure>

			          <h3><?php the_title() ?></h3>
			      </div>



			    </a>
			      </div>
			<?php endforeach; 
			wp_reset_postdata();?>

			 
			</div>
		</div>

			<div class="upside-down scrollUp">
			<span class="title-xxl tt-b">Portfolio</span>
		</div>
		<div class="upside-down upside-down-right scrollDown">

			<span class="title-xxl tt-n">Portfolio</span>


			</div>
</section>