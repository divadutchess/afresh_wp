<?php
/**
 * Template part for contact page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package reborn
 */



?>

<section class="text-left contact__intro bg-bright">
	<div class="container-fluid">
		<?php if(get_post_meta(get_the_ID(), 'afresh_page_subtitle', true)): ?>
		<span class="intro-text text-uppercase"><div class="js-span mb-4"><?php echo get_post_meta(get_the_ID(), 'afresh_page_subtitle', true) ?></div></span>
		<?php endif; ?>
		<h1 class="pl__heading js-heading  p-0 m-0 text-primary text-left"><?php echo get_post_meta(get_the_ID(), 'afresh_page_title', true) ? get_post_meta(get_the_ID(), 'afresh_page_title', true) : get_the_title() ?></h1>

	</div>
</section>


<section class="bg-bright pt-0 contact__info">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">

				<div class="contact__photo">
						<?php echo wp_get_attachment_image(get_field('afresh_middle_image'), 'default-portfolio') ?>
						<?php echo wp_get_attachment_image(get_field('afresh_last_image'), 'default-portfolio') ?>
					
				</div>
				
			</div>
			<div class="col-md-6">
				<?php echo get_field('afresh_halfway_desc') ?>
			</div>
		</div>
	</div>
</section>

<section class="section-contact bg-switch bg-bright" data-bg-color="#EAF0F8">
	

	<div class="container-fluid">
		<div class="contact__form pl-xxl">
			<?php the_content() ?>
		</div>
	</div>

</section>