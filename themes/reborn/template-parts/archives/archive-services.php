<?php
/**
 * Template part for displaying archive content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package reborn
 */

?>

<div class="container-fluid p-0">
	<div class="service-slide">
		<div class="left__col">
			<div class="fedUp services__content bg-white bg-bright">

					<div class="tt-v">
					<span class="title-xxl tt-b text-stroke">Services</span>

					</div>


				<?php $object = get_queried_object(); ?>
				<div class="section__heading mb-5">

				<h2><?php echo get_field('afresh_halfway_title', $object->name) ?></h2>

			</div>

			<div class="fadeInUp">
				<?php echo get_field('afresh_halfway_desc', $object->name) ?>

			</div>

			<div class="services__inner">
		<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();?>

				<div id="post-<?php the_ID(); ?>" <?php post_class('row services__row'); ?>>

					<div class="service__type col-md-6">
						<div class="slidein">
						<?php reborn_post_thumbnail(); ?>
					</div>


					</div>
					
			<div class="col-md-6">
				<div class="slideInRight">
			
					<header class="services__header">
						<div class="entry-meta mb-4 d-none">
						</div><!-- .entry-meta -->
						<?php
						
							the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );

						if ( 'post' === get_post_type() ) :
							?>
							
						<?php endif; ?>
					</header><!-- .entry-header -->

					<?php the_excerpt() ?>

					<a href="<?php the_permalink() ?>" class="btn btn-arrow">Learn More</a>
</div></div>
				</div><!-- #post-<?php the_ID(); ?> -->

			<?php endwhile;

			the_posts_navigation();

		
		?>

	</div>	</div></div>

	<div class="co">


			<div id="sv4">
				<div class="services__block">

			<?php
			while ( have_posts() ) :
				the_post();?>

					<div class="service-group">
					<?php echo get_field('afresh_service_img')?>

					<span><?php echo get_field('afresh_cute_title') ?></span>
					<p><?php echo get_field('afresh_page_desc')?></p>
					<div class="services__img">
						<?php echo wp_get_attachment_image( get_field('afresh_page_image'), 'thumbnail' ) ?>
					</div>

				</div>

					
			<?php endwhile;?>
			</div>

			
		</div>
		
	</div>

	</div>

	</div>