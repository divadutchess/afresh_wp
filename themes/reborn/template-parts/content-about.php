<?php
/**
 * Template part for about page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package reborn
 */

?>

<section class="intro p-0">
	<div class="container-fluid is-fedUp p-0">
		<div class="row no-gutters">
			<div class="col-md-4 intro__heading">
				<div class="fedUp">
				<h2 class="intro__title"><?php echo get_field('afresh_secondary_text') ?></h2>

				<span class="hours">Opening Hours</span>
				<span class="decorative">9:00<small>am</small> – 9:00<small>pm</small></span>

				<button class="btn btn-arrow btn-arrow__big mt-2">Schedule A Call</button>

			</div>

				
			</div>

			<div class="col-md-8">
				<div class="about__content pt-sm pb-0">

					<?php the_content() ?>

			</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-switch bg-bright text-center" data-bg-color="#EAF0F8">
	<div class="container">

	<div class="w-850">

		<div class="why__inner  reveal unfold overlay">
			<div class="reveal__inner">
		<figure class="about__img cover--full">
			<img class="lazyload" <?php echo afresh_attachment_image(get_field('afresh_last_image'), 'full', '1000px') ?> alt="About">
		</figure>

	</div>

		</div>

	<div class="section__heading mb-5">
						<h3 class="pinline">
							<span><?php echo get_field('afresh_halfway_title') ?></span>
						</h3>
					</div>



	<div class="us__content fadeInUp">
		
	<?php echo get_field('afresh_halfway_desc') ?>

	</div>

	</div>

	</div>

</section>