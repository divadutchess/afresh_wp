<?php
/**
 * Template Name: Portfolio Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package reborn
 */

get_header();
?>

		<main id="main" class="cd-main visible">

		<?php
		while ( have_posts() ) :
			the_post();?>

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<?php if(get_field('show_page_header') == true): 
					$image_url = get_field('afresh_page_header_bg') ? get_field('afresh_page_header_bg'): 'https://source.unsplash.com/random/1400x600'; 
			?>
				<div class="page-header pl">
					<div class="page-header--content">
						<?php if(get_post_meta(get_the_ID(), 'afresh_page_subtitle', true)): ?>
						<span class="intro-text"><div class="js-span"><?php echo get_post_meta(get_the_ID(), 'afresh_page_subtitle', true) ?></div></span>
						<?php endif; ?>
						<h1 class="pl__heading js-heading"><?php echo get_post_meta(get_the_ID(), 'afresh_page_title', true) ? get_post_meta(get_the_ID(), 'afresh_page_title', true) : get_the_title() ?></h1>
					</div>
					<div class="page-header-inner js-bg pl__bg" style="background-image: url('<?php echo $image_url;?>')"></div>
						
					
				</div><!-- .entry-header -->

			<?php endif; ?>


			<?php if(is_page('about-us')):

			get_template_part( 'template-parts/content', 'about' );

			endif; ?>

			</div><!-- #post-<?php the_ID(); ?> -->
			
		<?php endwhile; // End of the loop.
		?>

		</main><!-- #main -->

<?php
get_footer();
