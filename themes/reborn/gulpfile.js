const { src, dest, series, watch } = require('gulp');
const sass = require('gulp-sass');
const minifyCSS = require('gulp-csso');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const plumber = require("gulp-plumber");
const uglify = require("gulp-uglify");
const beeper = require("beeper");
const log = require("fancy-log");
const autoprefixer = require('gulp-autoprefixer');


/* -------------------------------------------------------------------------------------------------
Header & Footer JavaScript Boundles
-------------------------------------------------------------------------------------------------- */
const headerJS = [
  "./node_modules/vanilla-lazyload/dist/lazyload.min.js",

  "./node_modules/jquery/dist/jquery.js",
  "./node_modules/webfontloader/webfontloader.js",

  "./node_modules/gsap/dist/gsap.js",
  "./node_modules/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js",
  "./node_modules/scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js",
  "./node_modules/scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js",
  "./node_modules/imagesloaded/imagesloaded.pkgd.min.js",
  "./node_modules/isotope-layout/dist/isotope.pkgd.min.js",



];

const footerJS = ["./assets/js/**"];


function css() {
    return src('./assets/sass/app.scss', { sourcemaps: true })
        .pipe(sass())
        .pipe(minifyCSS())
        .pipe(autoprefixer({
             grid: true,
             overrideBrowserslist: ["last 10 versions"]
           }))
        .pipe(dest('./dist/css'), { sourcemaps: true })
        .pipe(browserSync.stream());
}

function js() {
    return src('./assets/js/*.js', { sourcemaps: true })
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(concat('app.js'))
        .pipe(dest('./dist/js', { sourcemaps: true }));
}

function headerScriptsProd() {
  return src(headerJS)
    .pipe(plumber({ errorHandler: onError }))
    .pipe(concat("vendor.js"))
    .pipe(uglify())
    .pipe(dest("./dist/js"));
}

function browser() {
    browserSync.init({
        proxy: 'afresh.local',
        files: [
            './**/*.php'
        ]
    });

    watch('./assets/sass/**/*.scss', css);
    watch('./assets/js/*.js', js).on('change', browserSync.reload);
    watch("./assets/js/**", headerScriptsProd);

}




exports.css = css;
exports.js = js;

exports.headerScriptsProd = headerScriptsProd;
exports.default = browser;


/* -------------------------------------------------------------------------------------------------
Utility Tasks
-------------------------------------------------------------------------------------------------- */
const onError = err => {
  beeper();
  log(wpFy + " - " + errorMsg + " " + err.toString());
  this.emit("end");
};

/* -------------------------------------------------------------------------------------------------
Messages
-------------------------------------------------------------------------------------------------- */
const date = new Date().toLocaleDateString("en-GB").replace(/\//g, ".");
const errorMsg = "\x1b[41mError\x1b[0m";
const wpFy = "\x1b[42m\x1b[1mWordPressify\x1b[0m";
