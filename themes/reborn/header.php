<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package reborn
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class('loading'); ?>>
<div id="page" class="wrapper">

	<div class="circle-cursor circle-cursor--inner"></div>
	<div class="circle-cursor circle-cursor--outer"></div>

	<?php 
	//if (cariera_get_option( 'afresh_preloader' )) {
	    // If "cariera_preloader" is enabled load preloader template
	get_template_part( 'template-parts/extra/preloader' ); 
	//}?>


	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'reborn' ); ?></a>




	<header id="masthead" class="main--header">
		<?php the_custom_logo(  ) ?>

		<nav class="navbar navbar-expand-md bg-primary" role="navigation">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-main-navbar-collapse-1" aria-controls="bs-main-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
		        <div id="nav-icon">
		          <span></span>
		          <span></span>
		          <span></span>
		          <span></span>
		        </div>
		    </button>
		        <?php
		        wp_nav_menu( array(
		            'theme_location'    => 'menu-1',
		            'depth'             => 2,
		            'container'         => 'div',
		            'container_class'   => 'collapse navbar-collapse  justify-content-md-center',
		            'container_id'      => 'bs-main-navbar-collapse-1',
		            'menu_class'        => 'nav navbar-nav m-0',
		            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
		            'walker'            => new WP_Bootstrap_Navwalker(),
		        ) );
		        ?>
		    
		</nav><!-- #site-navigation -->

 
	<button class="action action--menu btn-quote"> Get A Free Estimate</button>
	
	</header><!-- #masthead -->


	<nav class="menu">
			<div class="menu__item menu__item--1" data-direction="rl">
				<div class="menu__item-inner">
					<div class="mainmenu">
						<a href="#" class="mainmenu__item">Story</a>
						<a href="#" class="mainmenu__item">Chronicles</a>
						<a href="#" class="mainmenu__item">Tour</a>
						<a href="#" class="mainmenu__item">Contact</a>
					</div>
					
				</div>
			</div>

			<div class="menu__item menu__item--2" data-direction="lr">
								<div class="menu__item-inner">
								</div>
							</div>



			<button class="action action--close"> Close</button>

		</nav>

	<div id="content" class="site-content">
