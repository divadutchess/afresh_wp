<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package reborn
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function reborn_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'reborn_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function reborn_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'reborn_pingback_header' );

/**
 * Changes the class on the custom logo in the header.php
 */
function helpwp_custom_logo_output( $html ) {
	$html = str_replace('custom-logo-link', 'navbar-brand', $html );
	return $html;
}
add_filter('get_custom_logo', 'helpwp_custom_logo_output', 10);


// Register Custom Post Type
function custom_portfolio_type() {

	$labels = array(
		'name'                  => _x( 'Portfolio', 'Post Type General Name', 'afresh' ),
		'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'afresh' ),
		'menu_name'             => __( 'Portfolio', 'afresh' ),
		'name_admin_bar'        => __( 'Portfolio', 'afresh' ),
		'archives'              => __( 'Item Archives', 'afresh' ),
		'attributes'            => __( 'Item Attributes', 'afresh' ),
		'parent_item_colon'     => __( 'Parent Item:', 'afresh' ),
		'all_items'             => __( 'All Items', 'afresh' ),
		'add_new_item'          => __( 'Add New Item', 'afresh' ),
		'add_new'               => __( 'Add New', 'afresh' ),
		'new_item'              => __( 'New Item', 'afresh' ),
		'edit_item'             => __( 'Edit Item', 'afresh' ),
		'update_item'           => __( 'Update Item', 'afresh' ),
		'view_item'             => __( 'View Item', 'afresh' ),
		'view_items'            => __( 'View Items', 'afresh' ),
		'search_items'          => __( 'Search Item', 'afresh' ),
		'not_found'             => __( 'Not found', 'afresh' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'afresh' ),
		'featured_image'        => __( 'Featured Image', 'afresh' ),
		'set_featured_image'    => __( 'Set featured image', 'afresh' ),
		'remove_featured_image' => __( 'Remove featured image', 'afresh' ),
		'use_featured_image'    => __( 'Use as featured image', 'afresh' ),
		'insert_into_item'      => __( 'Insert into item', 'afresh' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'afresh' ),
		'items_list'            => __( 'Items list', 'afresh' ),
		'items_list_navigation' => __( 'Items list navigation', 'afresh' ),
		'filter_items_list'     => __( 'Filter items list', 'afresh' ),
	);
	$args = array(
		'label'                 => __( 'Portfolio', 'afresh' ),
		'description'           => __( 'Custom Portfolio Type', 'afresh' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments' ),
		'taxonomies'            => array( 'portfolio_category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 54,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'       => 'dashicons-images-alt2',

	);
	register_post_type( 'portfolio', $args );

}
add_action( 'init', 'custom_portfolio_type', 0 );



// Register Custom Taxonomy
function portfolio_kind_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'afresh' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'afresh' ),
		'menu_name'                  => __( 'Category', 'afresh' ),
		'all_items'                  => __( 'All Categories', 'afresh' ),
		'parent_item'                => __( 'Parent Category', 'afresh' ),
		'parent_item_colon'          => __( 'Parent Category:', 'afresh' ),
		'new_item_name'              => __( 'New Category Name', 'afresh' ),
		'add_new_item'               => __( 'Add New Category', 'afresh' ),
		'edit_item'                  => __( 'Edit Category', 'afresh' ),
		'update_item'                => __( 'Update Category', 'afresh' ),
		'view_item'                  => __( 'View Item', 'afresh' ),
		'separate_items_with_commas' => __( 'Separate genres with commas', 'afresh' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'afresh' ),
		'choose_from_most_used'      => __( 'Choose from the most used genres', 'afresh' ),
		'popular_items'              => __( 'Popular Items', 'afresh' ),
		'search_items'               => __( 'Search categories', 'afresh' ),
		'not_found'                  => __( 'Not Found', 'afresh' ),
		'no_terms'                   => __( 'No items', 'afresh' ),
		'items_list'                 => __( 'Items list', 'afresh' ),
		'items_list_navigation'      => __( 'Items list navigation', 'afresh' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'portfolio_category', array( 'portfolio' ), $args );

}
add_action( 'init', 'portfolio_kind_taxonomy', 0 );


function create_posttype() {
  $args = array(
    'labels' => array(
      'name' => __('Testimonials'),
      'singular_name' => __('Testimonials'),
      'all_items' => __('All Testimonials'),
      'add_new_item' => __('Add New Testimonial'),
      'edit_item' => __('Edit Testimonial'),
      'view_item' => __('View Testimonial'),
		'not_found' => __( 'Not Found', 'afresh' ),

    ),
    'public' => true,
    'has_archive' => true,
    'rewrite' => array('slug' => 'testimonials'),
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'capability_type' => 'page',
    'supports' => array('title', 'editor', 'thumbnail'),
    'exclude_from_search' => true,
    'menu_position' => 60,
    'has_archive' => true,
    'menu_icon' => 'dashicons-format-status'
    );
  register_post_type('testimonials', $args);
}
add_action( 'init', 'create_posttype');



// Register Custom Post Type
function services_post_type() {

	$labels = array(
		'name'                  => _x( 'Services', 'Post Type General Name', 'afresh' ),
		'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'afresh' ),
		'menu_name'             => __( 'Services', 'afresh' ),
		'name_admin_bar'        => __( 'Service', 'afresh' ),
		'archives'              => __( 'Item Archives', 'afresh' ),
		'attributes'            => __( 'Item Attributes', 'afresh' ),
		'parent_item_colon'     => __( 'Parent Service:', 'afresh' ),
		'all_items'             => __( 'All Services', 'afresh' ),
		'add_new_item'          => __( 'Add New Service', 'afresh' ),
		'add_new'               => __( 'New Service', 'afresh' ),
		'new_item'              => __( 'New Item', 'afresh' ),
		'edit_item'             => __( 'Edit Service', 'afresh' ),
		'update_item'           => __( 'Update Service', 'afresh' ),
		'view_item'             => __( 'View Service', 'afresh' ),
		'view_items'            => __( 'View Items', 'afresh' ),
		'search_items'          => __( 'Search services', 'afresh' ),
		'not_found'             => __( 'No services found', 'afresh' ),
		'not_found_in_trash'    => __( 'No services found in Trash', 'afresh' ),
		'featured_image'        => __( 'Featured Image', 'afresh' ),
		'set_featured_image'    => __( 'Set featured image', 'afresh' ),
		'remove_featured_image' => __( 'Remove featured image', 'afresh' ),
		'use_featured_image'    => __( 'Use as featured image', 'afresh' ),
		'insert_into_item'      => __( 'Insert into item', 'afresh' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'afresh' ),
		'items_list'            => __( 'Items list', 'afresh' ),
		'items_list_navigation' => __( 'Items list navigation', 'afresh' ),
		'filter_items_list'     => __( 'Filter items list', 'afresh' ),
	);
	$args = array(
		'label'                 => __( 'Service', 'afresh' ),
		'description'           => __( 'An overview of afresh packages available to you.', 'afresh' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 45,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'menu_icon'       => 'dashicons-exerpt-view',

	);
	register_post_type( 'services', $args );

}
add_action( 'init', 'services_post_type', 0 );
