<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package reborn
 */

?>

	</div><!-- #content -->



	<footer id="colophon" class="footer">
		<div class="footer-form">
			<h3>Sign up for newsletter here</h3>
		</div>
		<div class="footer-grid">

			<div class="footer-widgets">

				    <?php dynamic_sidebar( 'footer-widget-area' ); ?>
				
			</div>

	</div>


	<div class="site-info">
		&copy; Copyright <?php the_date('Y') ?> afresh creative ~ All Rights Reserved.
	</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->



<!-- ========== START TRANSITION ========== -->
<div class="loader">

	  <div class="loader__tile"></div>
	  <div class="loader__tile"></div>
	  <div class="loader__tile"></div>
	  <div class="loader__tile"></div>
	  <div class="loader__tile"></div>
</div>
<!-- ========== END TRANSITION ========== -->

<?php wp_footer(); ?>

</body>
</html>
